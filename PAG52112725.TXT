OBJECT Page 52112725 Request List - Open
{
  OBJECT-PROPERTIES
  {
    Date=30/10/14;
    Time=08:30:27;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Requests;
               PTB=Requisi��es];
    SourceTable=Table52112723;
    SourceTableView=SORTING(No.)
                    WHERE(Status=FILTER(Open|Pending Approval|Inventory|Purchase));
    PageType=List;
    CardPageID=Request;
    OnOpenPage=BEGIN
                 SETRANGE("User ID", USERID);
               END;

    ActionList=ACTIONS
    {
      { 35000015;    ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 35000014;1   ;Action    ;
                      CaptionML=[ENU=Aprovals;
                                 PTB=Aprova��es];
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=Approvals;
                      PromotedCategory=Process;
                      OnAction=VAR
                                 approvEntries@35000001 : Page 658;
                                 approvEntry@35000000 : Record 454;
                               BEGIN
                                 approvEntries.Setfilters(DATABASE::Request, approvEntry."Document Type"::Request, "No.");
                                 approvEntries.RUN;
                               END;
                                }
      { 52006501;1   ;Action    ;
                      Name=OpenFiles;
                      CaptionML=[ENU=Files;
                                 PTB=Arquivos];
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=ExternalDocument;
                      PromotedCategory=Process;
                      OnAction=VAR
                                 docFileMgt@52006500 : Codeunit 52112428;
                               BEGIN
                                 docFileMgt.OpenFiles(Rec);
                               END;
                                }
      { 52006500;1   ;Action    ;
                      Name=Print;
                      Ellipsis=Yes;
                      CaptionML=[ENU=&Print;
                                 PTB=Im&primir];
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=Print;
                      PromotedCategory=Report;
                      OnAction=VAR
                                 req@52006501 : Record 52112723;
                                 report@52006500 : Report 52112723;
                               BEGIN
                                 req := Rec;
                                 req.SETRECFILTER;
                                 report.SETTABLEVIEW(req);
                                 report.RUN;
                               END;
                                }
      { 52006504;1   ;Action    ;
                      Name=Navigate;
                      CaptionML=[ENU=&Navigate;
                                 PTB=&Navegar];
                      Promoted=Yes;
                      Image=Navigate;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 Navigate;
                               END;
                                }
      { 35000012;    ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 35000011;1   ;Action    ;
                      CaptionML=[ENU=Send Approval Request;
                                 PTB=Enviar Requisi��o Aprova��o];
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=SendApprovalRequest;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 SendApprovalRequest;
                               END;
                                }
      { 35000010;1   ;Action    ;
                      CaptionML=[ENU=Cancel Approval Request;
                                 PTB=Cancelar Requisi��o Aprova��o];
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=Cancel;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 CancelApprovalRequest;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 35000000;0;Container;
                ContainerType=ContentArea }

    { 35000001;1;Group    ;
                Name=Group;
                GroupType=Repeater }

    { 35000002;2;Field    ;
                SourceExpr="No." }

    { 35000003;2;Field    ;
                SourceExpr=Description }

    { 35000004;2;Field    ;
                SourceExpr=Status }

    { 52006503;2;Field    ;
                SourceExpr="Release Date";
                Editable=FALSE }

    { 35000005;2;Field    ;
                SourceExpr="Priority Degree" }

    { 35000006;2;Field    ;
                SourceExpr=Date }

    { 35000007;2;Field    ;
                SourceExpr="User ID" }

    { 35000008;2;Field    ;
                SourceExpr="User Name" }

    { 35000009;2;Field    ;
                SourceExpr="Purchaser Code";
                Visible=FALSE }

    { 52006502;2;Field    ;
                CaptionML=[ENU=Approvers;
                           PTB=Aprovadores];
                SourceExpr=GetApprovers }

  }
  CODE
  {

    PROCEDURE GetApprovers@52006500() approvers : Text;
    VAR
      approvEntry@52006500 : Record 454;
      user@52006501 : Record 2000000120;
    BEGIN
      user.SETCURRENTKEY("User Name");
      WITH approvEntry DO BEGIN
        SETRANGE("Table ID", DATABASE::Request);
        SETRANGE("Document Type", approvEntry."Document Type"::Request);
        SETRANGE("Document No.", "No.");
        SETFILTER(Status, '%1|%2|%3', Status::Created, Status::Open, Status::Approved);
        IF FINDSET THEN
          REPEAT
            IF "Approver ID" <> "Sender ID" THEN BEGIN
              user.SETRANGE("User Name", "Approver ID");
              IF user.FINDFIRST THEN BEGIN
                approvers += user."Full Name" + ', ';
              END;
            END;
          UNTIL NEXT = 0;
      END;
      IF approvers <> '' THEN
        approvers := COPYSTR(approvers, 1, STRLEN(approvers) - 2);
    END;

    BEGIN
    END.
  }
}

