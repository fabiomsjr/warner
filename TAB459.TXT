OBJECT Table 459 Sales Prepayment %
{
  OBJECT-PROPERTIES
  {
    Date=05/11/08;
    Time=12:00:00;
    Version List=NAVW16.00;
  }
  PROPERTIES
  {
    OnInsert=BEGIN
               IF "Sales Type" = "Sales Type"::"All Customers" THEN
                 "Sales Code" := ''
               ELSE
                 TESTFIELD("Sales Code");
               TESTFIELD("Item No.");
             END;

    CaptionML=[ENU=Sales Prepayment %;
               PTB=% Adiantamento Vendas];
  }
  FIELDS
  {
    { 1   ;   ;Item No.            ;Code20        ;TableRelation=Item;
                                                   CaptionML=[ENU=Item No.;
                                                              PTB=No. Produto];
                                                   NotBlank=Yes }
    { 2   ;   ;Sales Type          ;Option        ;OnValidate=BEGIN
                                                                IF "Sales Type" <> xRec."Sales Type" THEN
                                                                  VALIDATE("Sales Code",'');
                                                              END;

                                                   CaptionML=[ENU=Sales Type;
                                                              PTB=Tipo Venda];
                                                   OptionCaptionML=[ENU=Customer,Customer Price Group,All Customers;
                                                                    PTB=Cliente,Grupo Pre�o Cliente,Todos Clientes];
                                                   OptionString=Customer,Customer Price Group,All Customers }
    { 3   ;   ;Sales Code          ;Code20        ;TableRelation=IF (Sales Type=CONST(Customer)) Customer
                                                                 ELSE IF (Sales Type=CONST(Customer Price Group)) "Customer Price Group";
                                                   OnValidate=BEGIN
                                                                IF "Sales Code" = '' THEN
                                                                  EXIT;

                                                                IF "Sales Type" = "Sales Type"::"All Customers" THEN
                                                                  ERROR(Text001,FIELDCAPTION("Sales Code"));
                                                              END;

                                                   CaptionML=[ENU=Sales Code;
                                                              PTB=Cod. Venda] }
    { 4   ;   ;Starting Date       ;Date          ;OnValidate=BEGIN
                                                                CheckDate;
                                                              END;

                                                   CaptionML=[ENU=Starting Date;
                                                              PTB=Data Inicial] }
    { 5   ;   ;Ending Date         ;Date          ;OnValidate=BEGIN
                                                                CheckDate;
                                                              END;

                                                   CaptionML=[ENU=Ending Date;
                                                              PTB=Data Final] }
    { 6   ;   ;Prepayment %        ;Decimal       ;CaptionML=[ENU=Prepayment %;
                                                              PTB=% Adiantamento];
                                                   DecimalPlaces=0:5;
                                                   MinValue=0;
                                                   MaxValue=100 }
  }
  KEYS
  {
    {    ;Item No.,Sales Type,Sales Code,Starting Date;
                                                   Clustered=Yes }
    {    ;Sales Type,Sales Code                    }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      Text000@1001 : TextConst 'ENU=%1 cannot be after %2.;PTB=%1 n�o pode ser depois de %2.';
      Text001@1000 : TextConst 'ENU=%1 must be blank.;PTB=%1 deve ser vazio.';

    LOCAL PROCEDURE CheckDate@5();
    BEGIN
      IF ("Starting Date" > "Ending Date") AND ("Ending Date" <> 0D) THEN
        ERROR(Text000,FIELDCAPTION("Starting Date"),FIELDCAPTION("Ending Date"));
    END;

    BEGIN
    END.
  }
}

