OBJECT Table 404 Change Log Setup (Field)
{
  OBJECT-PROPERTIES
  {
    Date=05/11/08;
    Time=12:00:00;
    Version List=NAVW16.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Change Log Setup (Field);
               PTB=Conf. Reg. Altera��es (Campo)];
  }
  FIELDS
  {
    { 1   ;   ;Table No.           ;Integer       ;TableRelation="Change Log Setup (Table)";
                                                   CaptionML=[ENU=Table No.;
                                                              PTB=N� Tabela] }
    { 2   ;   ;Field No.           ;Integer       ;TableRelation=Field.No. WHERE (TableNo=FIELD(Table No.));
                                                   CaptionML=[ENU=Field No.;
                                                              PTB=N� Campo] }
    { 3   ;   ;Field Caption       ;Text100       ;FieldClass=FlowField;
                                                   CalcFormula=Lookup(Field."Field Caption" WHERE (TableNo=FIELD(Table No.),
                                                                                                   No.=FIELD(Field No.)));
                                                   CaptionML=[ENU=Field Caption;
                                                              PTB=T�tulo Campo] }
    { 4   ;   ;Log Insertion       ;Boolean       ;CaptionML=[ENU=Log Insertion;
                                                              PTB=Registrar Inser��o] }
    { 5   ;   ;Log Modification    ;Boolean       ;CaptionML=[ENU=Log Modification;
                                                              PTB=Registrar Modifica��o] }
    { 6   ;   ;Log Deletion        ;Boolean       ;CaptionML=[ENU=Log Deletion;
                                                              PTB=Elimina��o do Registro] }
  }
  KEYS
  {
    {    ;Table No.,Field No.                     ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

