OBJECT Page 52112434 Partial Payments
{
  OBJECT-PROPERTIES
  {
    Date=30/04/13;
    Time=16:49:25;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Partial Payments;
               PTB=Prazos Pagamentos/Cobran�as];
    SourceTable=Table52112434;
    PageType=List;
    AutoSplitKey=Yes;
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1102300000;1;Group  ;
                GroupType=Repeater }

    { 1102300005;2;Field  ;
                SourceExpr="% of the Total" }

    { 1102300011;2;Field  ;
                SourceExpr="Due Period Calc." }

  }
  CODE
  {

    BEGIN
    END.
  }
}

