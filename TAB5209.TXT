OBJECT Table 5209 Union
{
  OBJECT-PROPERTIES
  {
    Date=23/09/13;
    Time=12:00:00;
    Version List=NAVW17.10,NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Union;
               PTB=Sindicato];
    LookupPageID=Page5213;
    DrillDownPageID=Page5213;
  }
  FIELDS
  {
    { 1   ;   ;Code                ;Code10        ;CaptionML=[ENU=Code;
                                                              PTB=C�digo];
                                                   NotBlank=Yes }
    { 2   ;   ;Name                ;Text50        ;CaptionML=[ENU=Name;
                                                              PTB=Nome] }
    { 3   ;   ;Address             ;Text50        ;CaptionML=[ENU=Address;
                                                              PTB=Endere�o] }
    { 4   ;   ;Post Code           ;Code20        ;TableRelation="Post Code";
                                                   OnValidate=BEGIN
                                                                // > NAVBR
                                                                //PostCode.ValidatePostCode(City,"Post Code",County,"Country/Region Code",(CurrFieldNo <> 0) AND GUIALLOWED);
                                                                PostCode.ValidatePostCodeBR("Post Code", City, Address, County, District, "Territory Code", "Country/Region Code");
                                                                // < NAVBR
                                                              END;

                                                   CaptionML=[ENU=Post Code;
                                                              PTB=CEP] }
    { 5   ;   ;City                ;Text30        ;TableRelation=IF (Country/Region Code=CONST()) "Post Code".City
                                                                 ELSE IF (Country/Region Code=FILTER(<>'')) "Post Code".City WHERE (Country/Region Code=FIELD(Country/Region Code));
                                                   OnValidate=BEGIN
                                                                // > NAVBR
                                                                //PostCode.ValidateCity(City,"Post Code",County,"Country/Region Code",(CurrFieldNo <> 0) AND GUIALLOWED);
                                                                // < NAVBR
                                                              END;

                                                   ValidateTableRelation=No;
                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=City;
                                                              PTB=Cidade] }
    { 6   ;   ;Phone No.           ;Text30        ;ExtendedDatatype=Phone No.;
                                                   CaptionML=[ENU=Phone No.;
                                                              PTB=Telefone] }
    { 7   ;   ;No. of Members Employed;Integer    ;FieldClass=FlowField;
                                                   CalcFormula=Count(Employee WHERE (Status=FILTER(<>Terminated),
                                                                                     Union Code=FIELD(Code)));
                                                   CaptionML=[ENU=No. of Members Employed;
                                                              PTB=N� Afiliados Empregados];
                                                   Editable=No }
    { 8   ;   ;Name 2              ;Text50        ;CaptionML=[ENU=Name 2;
                                                              PTB=Nome Complementar] }
    { 9   ;   ;Address 2           ;Text50        ;CaptionML=[ENU=Address 2;
                                                              PTB=Endere�o Complementar] }
    { 10  ;   ;County              ;Text30        ;CaptionML=[ENU=County;
                                                              PTB=Regi�o] }
    { 11  ;   ;Fax No.             ;Text30        ;CaptionML=[ENU=Fax No.;
                                                              PTB=N� Fax] }
    { 12  ;   ;E-Mail              ;Text80        ;ExtendedDatatype=E-Mail;
                                                   CaptionML=[ENU=E-Mail;
                                                              PTB=E-Mail] }
    { 13  ;   ;Home Page           ;Text80        ;ExtendedDatatype=URL;
                                                   CaptionML=[ENU=Home Page;
                                                              PTB=Home Page] }
    { 14  ;   ;Country/Region Code ;Code10        ;TableRelation=Country/Region;
                                                   CaptionML=[ENU=Country/Region Code;
                                                              PTB=C�d. Pa�s] }
    { 52112430;;District           ;Text30        ;CaptionML=[ENU=District;
                                                              PTB=Bairro] }
    { 52112440;;Territory Code     ;Code10        ;TableRelation=Territory;
                                                   CaptionML=[ENU=Territory Code;
                                                              PTB=C�d. Unidade Federal] }
  }
  KEYS
  {
    {    ;Code                                    ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      PostCode@1000 : Record 225;

    BEGIN
    END.
  }
}

