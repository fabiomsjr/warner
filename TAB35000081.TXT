OBJECT Table 35000081 NFe Field2
{
  OBJECT-PROPERTIES
  {
    Date=13/05/16;
    Time=09:14:03;
    Modified=Yes;
    Version List=;
  }
  PROPERTIES
  {
    DataPerCompany=No;
    LookupPageID=Page35000097;
    DrillDownPageID=Page35000097;
  }
  FIELDS
  {
    { 10  ;   ;NavNFe Code         ;Code20        ;TableRelation="NFe Layout2";
                                                   CaptionML=PTB=C�digo NavNFe }
    { 20  ;   ;Table Code          ;Code20        ;TableRelation="NFe Table2".Code WHERE (NavNFe Code=FIELD(NavNFe Code));
                                                   CaptionML=PTB=C�d. Tabela }
    { 25  ;   ;Table No.           ;Integer       ;FieldClass=FlowField;
                                                   CalcFormula=Lookup("NFe Table2"."Table No." WHERE (NavNFe Code=FIELD(NavNFe Code),
                                                                                                      Code=FIELD(Table Code)));
                                                   CaptionML=PTB=N� Tabela;
                                                   Editable=No }
    { 30  ;   ;Code                ;Code20        ;OnValidate=BEGIN
                                                                IF STRPOS(Code, '|') > 0 THEN
                                                                  ERROR(Text002);
                                                              END;

                                                   CaptionML=PTB=C�digo }
    { 40  ;   ;Field No.           ;Integer       ;TableRelation=Field.No. WHERE (TableNo=FIELD(Table No.));
                                                   OnLookup=VAR
                                                              table@1102300002 : Record 35000079;
                                                              field@1102300001 : Record 2000000041;
                                                            BEGIN
                                                            END;

                                                   CaptionML=PTB=N� Campo }
    { 45  ;   ;Field Name          ;Text250       ;FieldClass=FlowField;
                                                   CalcFormula=Lookup(Field.FieldName WHERE (TableNo=FIELD(Table No.),
                                                                                             No.=FIELD(Field No.)));
                                                   CaptionML=PTB=Nome Campo;
                                                   Editable=No }
    { 50  ;   ;Changes             ;Integer       ;FieldClass=FlowField;
                                                   CalcFormula=Count("NFe Field Change2" WHERE (NavNFe Code=FIELD(NavNFe Code),
                                                                                                Table Code=FIELD(Table Code),
                                                                                                Field Code=FIELD(Code)));
                                                   CaptionML=PTB=Modifica��es;
                                                   Editable=No }
    { 10001;  ;TempID              ;Integer        }
    { 10002;  ;TempInt             ;Integer        }
    { 10003;  ;TempDecimal         ;Decimal        }
    { 10004;  ;TempText            ;Text250        }
    { 10005;  ;TempDate            ;Date           }
    { 10006;  ;TempLineNo          ;Integer        }
    { 11001;  ;TempRecID           ;Text100        }
    { 11002;  ;TempParentRecID     ;Text100        }
  }
  KEYS
  {
    {    ;NavNFe Code,Table Code,Code,TempID      ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      Text002@1102300000 : TextConst 'PTB=O c�digo do campo n�o pode conter o caracter |';

    BEGIN
    {
      --- THBR177 ---
      rafaelr,050811,NF-E

      --- THBR191 ---
      rafaelr,311011,(00) New control fields
    }
    END.
  }
}

