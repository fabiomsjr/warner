OBJECT Table 52112596 EFD Tax Credit
{
  OBJECT-PROPERTIES
  {
    Date=25/10/13;
    Time=13:52:00;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=EFD Tax Credit;
               PTB=Cr�dito Imposto EFD];
  }
  FIELDS
  {
    { 10  ;   ;Settlement No.      ;Code20        ;TableRelation="Tax Settlement";
                                                   CaptionML=[ENU=Settlement No.;
                                                              PTB=N� Apura��o] }
    { 20  ;   ;Tax Identification  ;Option        ;CaptionML=[ENU=Tax Identification;
                                                              PTB=Identifica��o do imposto];
                                                   OptionCaptionML=[ENU=" ,IPI,ICMS,PIS,COFINS,II,IRRF,INSS,ISS,CSL Ret.,PIS Ret.,COFINS Ret.,ISS Ret.,INSS Ret.,Dif. Aliq. ICMS,ST,PCC,CIDE,Funrural,Simples";
                                                                    PTB=" ,IPI,ICMS,PIS,COFINS,II,IRRF,INSS,ISS,CSL Ret.,PIS Ret.,COFINS Ret.,ISS Ret.,INSS Ret.,Dif. Aliq. ICMS,ST,PCC,CIDE,Funrural,Simples"];
                                                   OptionString=[ ,IPI,ICMS,PIS,COFINS,II,IRRF,INSS,ISS,CSL Ret.,PIS Ret.,COFINS Ret.,ISS Ret.,INSS Ret.,Dif. Aliq. ICMS,ST,PCC,CIDE,Funrural,Simples] }
    { 25  ;   ;Importation Credit  ;Boolean       ;CaptionML=[ENU=Importation Credit;
                                                              PTB=Cr�dito Importa��o] }
    { 27  ;   ;Order No.           ;Integer       ;CaptionML=[ENU=Order No.;
                                                              PTB=N� Ordem] }
    { 30  ;   ;CST Code            ;Text10        ;CaptionML=[ENU=CST Code;
                                                              PTB=C�d. CST] }
    { 40  ;   ;Credit Code         ;Text10        ;CaptionML=[ENU=Credit Code;
                                                              PTB=C�d. Cr�dito] }
    { 50  ;   ;Base Amount         ;Decimal       ;CaptionML=[ENU=Base Amount;
                                                              PTB=Valor Base] }
    { 60  ;   ;Amount              ;Decimal       ;CaptionML=[ENU=Amount;
                                                              PTB=Valor] }
  }
  KEYS
  {
    {    ;Settlement No.,Tax Identification,CST Code,Credit Code,Importation Credit,Order No.;
                                                   Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

