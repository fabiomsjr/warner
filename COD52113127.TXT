OBJECT Codeunit 52113127 Sales Line Hook
{
  OBJECT-PROPERTIES
  {
    Date=09/09/15;
    Time=16:28:25;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    OnRun=BEGIN
          END;

  }
  CODE
  {
    VAR
      SalesLineCHook@52006500 : Codeunit 52113128;
      ApprovalDimChangeErr@52006501 : TextConst 'ENU=Dimensions involved in the approval process can not be modified.;PTB=Dimens�es envolvidas no processo de aprova��o n�o podem ser modificadas.';

    PROCEDURE OnInsert@52006501(VAR salesLine@52006500 : Record 37);
    BEGIN
      SalesLineCHook.OnInsert(salesLine);
    END;

    PROCEDURE OnModify@52006503(VAR salesLine@52006501 : Record 37;VAR xSalesLine@52006500 : Record 37);
    BEGIN
      SalesLineCHook.OnModify(salesLine, xSalesLine);
    END;

    PROCEDURE OnDelete@52006502(VAR salesLine@52006500 : Record 37);
    BEGIN
      DeleteRelatedTables(salesLine);

      SalesLineCHook.OnDelete(salesLine);
    END;

    PROCEDURE OnValidateNo@52006500(salesHeader@52006502 : Record 36;VAR salesLine@52006500 : Record 37;VAR xSalesLine@52006501 : Record 37);
    VAR
      item@52006504 : Record 27;
      resource@52006503 : Record 156;
      operationType@52006505 : Record 52112435;
      fiscalCodesMgt@52006506 : Codeunit 52112483;
    BEGIN
      WITH salesLine DO BEGIN
        CASE Type OF
          Type::Item:
            BEGIN
              item.GET("No.");
              "NCM Code" := item."NCM Code";
              "Origin Code" := item."Origin Code";
              "NCM Exception Code" := item."NCM Exception Code";
              "Tax Exception Code" := item."Tax Exception Code";
            END;
          Type::Resource: BEGIN
            resource.GET("No.");
            IF resource."Default Work Type Code" <> '' THEN
              VALIDATE("Work Type Code", resource."Default Work Type Code");
          END;
          Type::"Fixed Asset": BEGIN
            "Allow Item Charge Assignment" := TRUE
          END;
        END;

        "Operation Type" := salesHeader."Operation Type";
        "End User" := salesHeader."End User";
        "CFOP Code" := salesHeader."CFOP Code";
        "Customer Situation" := salesHeader."Customer Situation";
        "Branch Code" := salesHeader."Branch Code";

        IF operationType.GET("Operation Type") THEN BEGIN
          IF operationType."Gen. Prod. Posting Group" <> '' THEN
            "Gen. Prod. Posting Group" := operationType."Gen. Prod. Posting Group";
        END;

        "VAT Calculation Type" := "VAT Calculation Type"::"Sales Tax";
        "Prepmt. VAT Calc. Type" := "Prepmt. VAT Calc. Type"::"Sales Tax";

        fiscalCodesMgt.ApplySalesLineTaxesMatrix(salesHeader, salesLine);
      END;

      SalesLineCHook.OnValidateNo(salesHeader, salesLine, xSalesLine);
    END;

    PROCEDURE OnValidateOutstandingAmount@52006506(VAR SalesLine@52006500 : Record 37;VAR xSalesLine@52006501 : Record 37);
    VAR
      OperationType@52006505 : Record 52112435;
    BEGIN
      OperationType.SetColumnValuesFor(SalesLine."Operation Type");
      IF OperationType."Customer/Vendor Entry" <> OperationType."Customer/Vendor Entry"::"Amount Including Taxes" THEN
        SalesLine."Outstanding Amount" := 0;
    END;

    PROCEDURE OnUpdateAmounts@52006505(VAR SalesLine@52006500 : Record 37;VAR XSalesLine@52006503 : Record 37);
    VAR
      SalesMgt@52006502 : Codeunit 52112432;
      FiscalCodesMgt@52006501 : Codeunit 52112483;
      FCIMgt@52006505 : Codeunit 52112663;
      SalesHeader@52006504 : Record 36;
    BEGIN
      SalesMgt.CheckApprovedAmtChange(SalesLine, XSalesLine);
      SalesHeader.GET(SalesLine."Document Type", SalesLine."Document No.");
      FiscalCodesMgt.UpdateSalesLineCST(SalesHeader, SalesLine);
      IF SalesLine.Type = SalesLine.Type::Item THEN
        FCIMgt.ApplyFCINo(SalesLine."FCI No.", SalesHeader."Posting Date", SalesLine."No.", SalesHeader."Branch Code");

      SalesLineCHook.OnUpdateAmounts(SalesLine);
    END;

    PROCEDURE OnDimensionSetUpdate@52006507(SalesLine@52006500 : Record 37;xSalesLine@52006501 : Record 37);
    VAR
      SalesHeader@52006505 : Record 36;
      DimMgt@52006502 : Codeunit 408;
      ApprovMgt@52006504 : Codeunit 439;
      ApprovFlowMgt@52006503 : Codeunit 52113073;
      DocApprovDimMgt@52006506 : Codeunit 52113080;
    BEGIN
      WITH SalesLine DO BEGIN
        SalesHeader.GET("Document Type", "Document No.");
        DimMgt.CheckDocLineBranchCode(SalesHeader."Dimension Set ID", "Dimension Set ID");
        IF Status <> Status::Open THEN BEGIN
          IF NOT ApprovMgt.CheckDocDimChange(DATABASE::"Sales Line", "Document Type", "Dimension Set ID", xSalesLine."Dimension Set ID") THEN
            ERROR(ApprovalDimChangeErr);
          IF NOT DocApprovDimMgt.CheckPurchDimUpdate("Document Type", "Dimension Set ID", xSalesLine."Dimension Set ID") THEN
            ERROR(ApprovalDimChangeErr);
        END;
      END;

      SalesLineCHook.OnDimensionSetUpdate(SalesLine, xSalesLine);
    END;

    PROCEDURE _@52006504();
    BEGIN
    END;

    LOCAL PROCEDURE DeleteRelatedTables@35000050(VAR salesLine@52006500 : Record 37);
    VAR
      modTaxAmtLine@35000000 : Record 52112450;
    BEGIN
      WITH modTaxAmtLine DO BEGIN
        SETRANGE("Table ID", DATABASE::"Sales Line");
        SETRANGE("Document Type", salesLine."Document Type");
        SETRANGE("Document No.", salesLine."Document No.");
        SETRANGE("Document Line No.", salesLine."Line No.");
        DELETEALL;
      END;
    END;

    BEGIN
    END.
  }
}

