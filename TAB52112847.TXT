OBJECT Table 52112847 Memo Journal Entry
{
  OBJECT-PROPERTIES
  {
    Date=16/05/16;
    Time=08:39:28;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Memo Journal Entry;
               PTB=Memo Lan�amento Cont�bil];
  }
  FIELDS
  {
    { 1   ;   ;Document No.        ;Code20        ;CaptionML=[ENU=Document No.;
                                                              PTB=No. Documento] }
    { 2   ;   ;Release Date        ;Date          ;CaptionML=[ENU=Release Date;
                                                              PTB=Data Lan�amento] }
    { 3   ;   ;Release Amount      ;Decimal       ;CaptionML=[ENU=Release Amount;
                                                              PTB=Valor Lan�amento] }
    { 4   ;   ;Release Type        ;Option        ;CaptionML=[ENU=Release Type;
                                                              PTB=Tipo Lan�amento];
                                                   OptionCaptionML=[ENU=Debit,Credit;
                                                                    PTB=D�bito,Cr�dito];
                                                   OptionString=D�bito,Cr�dito }
    { 5   ;   ;Release Indicator   ;Option        ;CaptionML=[ENU=Release Indicator;
                                                              PTB=Indicador Lan�amento];
                                                   OptionCaptionML=[ENU=Normal,Closure;
                                                                    PTB=Normal,Encerramento];
                                                   OptionString=Normal,Encerramento }
    { 6   ;   ;Transaction No.     ;Integer       ;CaptionML=[ENU=Transaction No.;
                                                              PTB=No. Lan�amento] }
    { 1000;   ;Funct. Curr. Release Amount;Decimal;CaptionML=[ENU=Funct. Curr. Release Amount;
                                                              PTB=Valor Lan�amento Moeda Func.] }
    { 2000;   ;FCont Posting Type  ;Code20        ;CaptionML=[ENU=FCont Posting Type;
                                                              PTB=Tipo Lan�amento FCont] }
  }
  KEYS
  {
    {    ;Document No.,Release Date,Release Indicator;
                                                   Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

