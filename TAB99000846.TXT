OBJECT Table 99000846 Planning Buffer
{
  OBJECT-PROPERTIES
  {
    Date=07/09/12;
    Time=12:00:00;
    Version List=NAVW17.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Planning Buffer;
               PTB=Mem. Interna Planejamento];
  }
  FIELDS
  {
    { 1   ;   ;Buffer No.          ;Integer       ;CaptionML=[ENU=Buffer No.;
                                                              PTB=N� Mem. Int.] }
    { 2   ;   ;Date                ;Date          ;CaptionML=[ENU=Date;
                                                              PTB=Data] }
    { 3   ;   ;Document Type       ;Option        ;CaptionML=[ENU=Document Type;
                                                              PTB=Tipo Documento];
                                                   OptionCaptionML=[ENU=Requisition Line,Planned Prod. Order Comp.,Firm Planned Prod. Order Comp.,Released Prod. Order Comp.,Planning Comp.,Sales Order,Planned Prod. Order,Planning Line,Req. Worksheet Line,Firm Planned Prod. Order,Released Prod. Order,Purchase Order,Quantity at Inventory,Service Order,Transfer,Job Order,Assembly Order,Assembly Order Line,Production Forecast-Sales,Production Forecast-Component;
                                                                    PTB=Linha Requisi��o,Comp. Ped. Prod. Planejada,Comp. Ped. Prod. Planejada Firme,Comp. Ped. Prod. Lan�ada,Comp. Planejamento,Pedido Venda,Enc. Prod. Planejada,Linha Planejamento,Linha Folha Nec.,Enc. Prod. Planejada Firme,Ped. Prod. Lan�ada,Pedido Compra,Quant. em Invent�rio,Ordem Servi�o,Transfer�ncia];
                                                   OptionString=Requisition Line,Planned Prod. Order Comp.,Firm Planned Prod. Order Comp.,Released Prod. Order Comp.,Planning Comp.,Sales Order,Planned Prod. Order,Planning Line,Req. Worksheet Line,Firm Planned Prod. Order,Released Prod. Order,Purchase Order,Quantity at Inventory,Service Order,Transfer,Job Order,Assembly Order,Assembly Order Line,Production Forecast-Sales,Production Forecast-Component }
    { 4   ;   ;Document No.        ;Code20        ;CaptionML=[ENU=Document No.;
                                                              PTB=No. Documento] }
    { 6   ;   ;Item No.            ;Code20        ;TableRelation=Item;
                                                   CaptionML=[ENU=Item No.;
                                                              PTB=No. Produto] }
    { 7   ;   ;Description         ;Text50        ;CaptionML=[ENU=Description;
                                                              PTB=Descri��o] }
    { 8   ;   ;Gross Requirement   ;Decimal       ;CaptionML=[ENU=Gross Requirement;
                                                              PTB=Necessidades Brutas];
                                                   DecimalPlaces=0:5 }
    { 10  ;   ;Planned Receipts    ;Decimal       ;CaptionML=[ENU=Planned Receipts;
                                                              PTB=Recep��es Planejadas];
                                                   DecimalPlaces=0:5 }
    { 11  ;   ;Scheduled Receipts  ;Decimal       ;CaptionML=[ENU=Scheduled Receipts;
                                                              PTB=Recep��es Programadas];
                                                   DecimalPlaces=0:5 }
  }
  KEYS
  {
    {    ;Buffer No.                              ;Clustered=Yes }
    {    ;Item No.,Date                            }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

