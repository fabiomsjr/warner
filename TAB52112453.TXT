OBJECT Table 52112453 Accountant Data
{
  OBJECT-PROPERTIES
  {
    Date=30/04/13;
    Time=08:11:58;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Accountant Data;
               PTB=Dados Contabilista];
  }
  FIELDS
  {
    { 1   ;   ;Accountant CPF      ;Code11        ;CaptionML=PTB=CPF Contabilista }
    { 2   ;   ;Name                ;Text50        ;CaptionML=PTB=Nome }
    { 3   ;   ;Accountant CRC      ;Code11        ;CaptionML=PTB=CRC Contabilista }
    { 4   ;   ;CNPJ                ;Code14         }
    { 5   ;   ;CEP                 ;Code10        ;TableRelation="Post Code" }
    { 6   ;   ;Address             ;Text100       ;CaptionML=PTB=Endere�o }
    { 7   ;   ;Number              ;Code10        ;CaptionML=PTB=N�mero }
    { 8   ;   ;Address 2           ;Text100       ;CaptionML=PTB=Complemento }
    { 9   ;   ;District            ;Text50        ;CaptionML=PTB=Bairro }
    { 10  ;   ;Phone               ;Code10        ;CaptionML=PTB=Telefone }
    { 11  ;   ;Fax                 ;Code10        ;CaptionML=PTB=Fax }
    { 12  ;   ;e-Mail              ;Text100       ;CaptionML=PTB=e-Mail }
    { 14  ;   ;Service Start Date  ;Date          ;CaptionML=[ENU=Start Date Service;
                                                              PTB=Data In�cio Servi�o] }
    { 15  ;   ;Service End Date    ;Date          ;CaptionML=[ENU=End Date Service;
                                                              PTB=Data Final de Servi�o] }
    { 16  ;   ;PO Box              ;Text10        ;CaptionML=[ENU=PO Box;
                                                              PTB=Caixa Postal] }
    { 17  ;   ;PO Box CEP          ;Code10        ;TableRelation="Post Code";
                                                   CaptionML=[ENU=PO Box CEP;
                                                              PTB=CEP Caixa Postal] }
    { 18  ;   ;Contrib. SPED       ;Boolean       ;CaptionML=[ENU=Contrib. SPED;
                                                              PTB=SPED Contribui��es] }
    { 19  ;   ;EFD SPED            ;Boolean       ;CaptionML=[ENU=EFD SPED;
                                                              PTB=SPED Fiscal] }
    { 20  ;   ;ECD SPED            ;Boolean       ;CaptionML=[ENU=ECD SPED;
                                                              PTB=SPED Cont�bil] }
  }
  KEYS
  {
    {    ;Accountant CPF                          ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

