OBJECT Table 1014 Job G/L Account Price
{
  OBJECT-PROPERTIES
  {
    Date=07/09/12;
    Time=12:00:00;
    Version List=NAVW17.00;
  }
  PROPERTIES
  {
    OnInsert=BEGIN
               LOCKTABLE;
               Job.GET("Job No.");
               TESTFIELD("G/L Account No.");
             END;

    CaptionML=[ENU=Job G/L Account Price;
               PTB=Pre�o Tarefa Conta Cont�bil];
    LookupPageID=Page1013;
    DrillDownPageID=Page1013;
  }
  FIELDS
  {
    { 1   ;   ;Job No.             ;Code20        ;TableRelation=Job;
                                                   OnValidate=BEGIN
                                                                GetJob;
                                                                "Currency Code" := Job."Currency Code";
                                                              END;

                                                   CaptionML=[ENU=Job No.;
                                                              PTB=N� Projeto];
                                                   NotBlank=Yes }
    { 2   ;   ;Job Task No.        ;Code20        ;TableRelation="Job Task"."Job Task No." WHERE (Job No.=FIELD(Job No.));
                                                   OnValidate=BEGIN
                                                                IF "Job Task No." <> '' THEN BEGIN
                                                                  JT.GET("Job No.","Job Task No.");
                                                                  JT.TESTFIELD("Job Task Type",JT."Job Task Type"::Posting);
                                                                END;
                                                              END;

                                                   CaptionML=[ENU=Job Task No.;
                                                              PTB=No. Processamento Tarefa] }
    { 3   ;   ;G/L Account No.     ;Code20        ;TableRelation="G/L Account";
                                                   CaptionML=[ENU=G/L Account No.;
                                                              PTB=N� Conta] }
    { 5   ;   ;Unit Price          ;Decimal       ;OnValidate=BEGIN
                                                                "Unit Cost Factor" := 0;
                                                              END;

                                                   CaptionML=[ENU=Unit Price;
                                                              PTB=Unit Price];
                                                   AutoFormatType=2;
                                                   AutoFormatExpr="Currency Code" }
    { 6   ;   ;Currency Code       ;Code10        ;TableRelation=Currency;
                                                   OnValidate=BEGIN
                                                                IF "Currency Code" <> xRec."Currency Code" THEN BEGIN
                                                                  "Unit Cost Factor" := 0;
                                                                  "Line Discount %" := 0;
                                                                  "Unit Price" := 0;
                                                                END;
                                                              END;

                                                   CaptionML=[ENU=Currency Code;
                                                              PTB=Cod. Moeda] }
    { 7   ;   ;Unit Cost Factor    ;Decimal       ;OnValidate=BEGIN
                                                                "Unit Price" := 0;
                                                              END;

                                                   CaptionML=[ENU=Unit Cost Factor;
                                                              PTB=Fator Custo Unit�rio] }
    { 8   ;   ;Line Discount %     ;Decimal       ;CaptionML=[ENU=Line Discount %;
                                                              PTB=% Desconto Linha] }
    { 9   ;   ;Unit Cost           ;Decimal       ;CaptionML=[ENU=Unit Cost;
                                                              PTB=Custo Unit�rio] }
    { 10  ;   ;Description         ;Text50        ;FieldClass=FlowField;
                                                   CalcFormula=Lookup("G/L Account".Name WHERE (No.=FIELD(G/L Account No.)));
                                                   CaptionML=[ENU=Description;
                                                              PTB=Descri��o];
                                                   Editable=No }
  }
  KEYS
  {
    {    ;Job No.,Job Task No.,G/L Account No.,Currency Code;
                                                   Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      Job@1001 : Record 167;
      JT@1002 : Record 1001;

    PROCEDURE GetJob@16();
    BEGIN
      TESTFIELD("Job No.");
      Job.GET("Job No.");
    END;

    BEGIN
    END.
  }
}

