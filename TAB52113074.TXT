OBJECT Table 52113074 Approval Flow Entry
{
  OBJECT-PROPERTIES
  {
    Date=26/08/15;
    Time=13:24:06;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Approval Entry;
               PTB=Lan�amento Aprova��o];
  }
  FIELDS
  {
    { 10  ;   ;Approval Flow No.   ;Code20        ;TableRelation="Approval Flow";
                                                   CaptionML=[ENU=Approval Flow No.;
                                                              PTB=N� Fluxo Aprova��o] }
    { 20  ;   ;Sequence No.        ;Integer       ;CaptionML=[ENU=Sequence No.;
                                                              PTB=N� Sequ�ncia] }
    { 25  ;   ;Approval No.        ;Integer       ;CaptionML=[ENU=Approval No.;
                                                              PTB=N� Aprova��o];
                                                   BlankNumbers=BlankZero }
    { 30  ;   ;Sender ID           ;Code50        ;TableRelation=User."User Name";
                                                   CaptionML=[ENU=Sender;
                                                              PTB=Remetente] }
    { 40  ;   ;Approver ID         ;Code50        ;TableRelation="User Setup"."User ID";
                                                   CaptionML=[ENU=Approver;
                                                              PTB=Aprovador] }
    { 50  ;   ;Status              ;Option        ;CaptionML=[ENU=Status;
                                                              PTB=Status];
                                                   OptionCaptionML=[ENU=Created,Open,Canceled,Rejected,Approved;
                                                                    PTB=Criado,Aberto,Cancelado,Rejeitado,Aprovado];
                                                   OptionString=Created,Open,Canceled,Rejected,Approved }
    { 60  ;   ;Type                ;Option        ;CaptionML=[ENU=Type;
                                                              PTB=Tipo];
                                                   OptionCaptionML=[ENU=" ,General Journal,Purchase Document,Sales Document,Item Journal,Request,CNAB Payment";
                                                                    PTB=" ,Di�rio Geral,Documento Compra,Documento Venda,Di�rio Produto,Requisi��o,Pagamento CNAB"];
                                                   OptionString=[ ,GenJnl,PurchDoc,SalesDoc,ItemJnl,Request,CNABPayment] }
    { 70  ;   ;Last Date-Time Modified;DateTime   ;CaptionML=[ENU=Last Date-Time Modified;
                                                              PTB=�ltima Data-Hora Modificado] }
    { 80  ;   ;Last Modified By ID ;Code50        ;CaptionML=[ENU=Last Modified By;
                                                              PTB=�ltima Modifica��o Por] }
    { 84  ;   ;Delegated By ID     ;Code50        ;CaptionML=[ENU=Delegated By;
                                                              PTB=Delega��o Por] }
    { 90  ;   ;Due Date            ;Date          ;CaptionML=[ENU=Due Date;
                                                              PTB=Data Vencimento] }
    { 100 ;   ;Unlimited           ;Boolean       ;CaptionML=[ENU=Unlimited Approval;
                                                              PTB=Aprova��o Ilimitada] }
    { 110 ;   ;Maximum Amount      ;Decimal       ;CaptionML=[ENU=Maximum Amount;
                                                              PTB=Valor Limite];
                                                   MinValue=0;
                                                   BlankNumbers=BlankZero }
    { 120 ;   ;Rule Code           ;Code20        ;TableRelation="Approval Flow Rule";
                                                   CaptionML=[ENU=Rule Code;
                                                              PTB=C�d. Regra] }
    { 130 ;   ;Reason              ;Text250       ;CaptionML=[ENU=Reason;
                                                              PTB=Motivo] }
    { 140 ;   ;Additional Approver ;Boolean       ;CaptionML=[ENU=Additional Approver;
                                                              PTB=Aprovador Adicional] }
    { 200 ;   ;E-Mail Key          ;Text50        ;CaptionML=[ENU=E-Mail Key;
                                                              PTB=Chave E-Mail] }
    { 210 ;   ;Last E-Mail Date    ;Date          ;CaptionML=[ENU=Last E-Mail Date;
                                                              PTB=Data �ltimo E-Mail] }
    { 400 ;   ;Description         ;Text100       ;FieldClass=FlowField;
                                                   CalcFormula=Lookup("Approval Flow".Description WHERE (No.=FIELD(Approval Flow No.)));
                                                   CaptionML=[ENU=Description;
                                                              PTB=Descri��o];
                                                   Editable=No }
    { 410 ;   ;Amount              ;Decimal       ;CaptionML=[ENU=Amount;
                                                              PTB=Valor];
                                                   BlankNumbers=BlankZero;
                                                   Editable=No;
                                                   AutoFormatType=1 }
    { 420 ;   ;Comments            ;Boolean       ;FieldClass=FlowField;
                                                   CalcFormula=Exist("Approval Flow Comment" WHERE (Approval Flow No.=FIELD(Approval Flow No.)));
                                                   CaptionML=[ENU=Comments;
                                                              PTB=Coment�rios];
                                                   Editable=No }
  }
  KEYS
  {
    {    ;Approval Flow No.,Sequence No.          ;Clustered=Yes }
    {    ;Sender ID                                }
    {    ;Approver ID,Status                       }
    {    ;Status,E-Mail Key                        }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    PROCEDURE OpenDocument@35100000();
    VAR
      approvFlow@35100000 : Record 52113073;
    BEGIN
      approvFlow.GET("Approval Flow No.");
      approvFlow.OpenDocument;
    END;

    PROCEDURE OpenFlow@35100001();
    VAR
      approvFlow@35100000 : Record 52113073;
      approvFlowCard@35100001 : Page 52113073;
    BEGIN
      approvFlow.GET("Approval Flow No.");
      approvFlowCard.SETRECORD(approvFlow);
      approvFlowCard.RUN;
    END;

    PROCEDURE ResendEmail@52006502();
    VAR
      ApprovFlowEmailJob@52006500 : Codeunit 52113077;
    BEGIN
      ApprovFlowEmailJob.AddMail(Rec);
      ApprovFlowEmailJob.Send;
    END;

    PROCEDURE GetStyle@52006500() : Text;
    BEGIN
      IF NOT (Status IN [Status::Open,Status::Created]) THEN
        EXIT('');

      IF WORKDATE > "Due Date" THEN
        EXIT('StrongAccent')
      ELSE IF Status = Status::Open THEN
        EXIT('Strong')
      ELSE
        EXIT('');
    END;

    BEGIN
    END.
  }
}

