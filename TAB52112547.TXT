OBJECT Table 52112547 EFD C490
{
  OBJECT-PROPERTIES
  {
    Date=30/04/13;
    Time=11:59:13;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    LookupPageID=Page50022;
    DrillDownPageID=Page50022;
  }
  FIELDS
  {
    { 1   ;   ;Tax Settlement No.  ;Code20        ;TableRelation="Tax Settlement" }
    { 3   ;   ;Company ID          ;Text30         }
    { 10  ;   ;Start Date          ;Date           }
    { 15  ;   ;End Date            ;Date           }
    { 20  ;   ;Document Model      ;Code20         }
  }
  KEYS
  {
    {    ;Tax Settlement No.,Company ID,Start Date,End Date,Document Model;
                                                   Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    PROCEDURE RegCode@35000000() : Code[10];
    BEGIN
      EXIT('C490');
    END;

    BEGIN
    {
      --- FUM0089 ---
      rafaelr,281211,00,Corre��es SPED PC
    }
    END.
  }
}

