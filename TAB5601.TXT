OBJECT Table 5601 FA Ledger Entry
{
  OBJECT-PROPERTIES
  {
    Date=07/09/12;
    Time=12:00:00;
    Version List=NAVW17.00,NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=FA Ledger Entry;
               PTB=Mov. AF];
    LookupPageID=Page5604;
    DrillDownPageID=Page5604;
  }
  FIELDS
  {
    { 1   ;   ;Entry No.           ;Integer       ;CaptionML=[ENU=Entry No.;
                                                              PTB=N� Mov.] }
    { 2   ;   ;G/L Entry No.       ;Integer       ;TableRelation="G/L Entry";
                                                   CaptionML=[ENU=G/L Entry No.;
                                                              PTB=N� Mov. Cont�beis];
                                                   BlankZero=Yes }
    { 3   ;   ;FA No.              ;Code20        ;TableRelation="Fixed Asset";
                                                   CaptionML=[ENU=FA No.;
                                                              PTB=N� AF] }
    { 4   ;   ;FA Posting Date     ;Date          ;CaptionML=[ENU=FA Posting Date;
                                                              PTB=Data Registo AF] }
    { 5   ;   ;Posting Date        ;Date          ;CaptionML=[ENU=Posting Date;
                                                              PTB=Data Registo] }
    { 6   ;   ;Document Type       ;Option        ;CaptionML=[ENU=Document Type;
                                                              PTB=Tipo Documento];
                                                   OptionCaptionML=[ENU=" ,Payment,Invoice,Credit Memo,Finance Charge Memo,Reminder,Refund";
                                                                    PTB=" ,Pagamento,Nota Fiscal,Nota de Cr�dito,Nota de Encargo Financeiro,Lembrete,Reembolso"];
                                                   OptionString=[ ,Payment,Invoice,Credit Memo,Finance Charge Memo,Reminder,Refund] }
    { 7   ;   ;Document Date       ;Date          ;CaptionML=[ENU=Document Date;
                                                              PTB=Data Documento] }
    { 8   ;   ;Document No.        ;Code20        ;CaptionML=[ENU=Document No.;
                                                              PTB=No. Documento] }
    { 9   ;   ;External Document No.;Code35       ;CaptionML=[ENU=External Document No.;
                                                              PTB=N� Documento Externo] }
    { 10  ;   ;Description         ;Text50        ;CaptionML=[ENU=Description;
                                                              PTB=Descri��o] }
    { 11  ;   ;Depreciation Book Code;Code10      ;TableRelation="Depreciation Book";
                                                   CaptionML=[ENU=Depreciation Book Code;
                                                              PTB=Cod. Livro Deprecia��o] }
    { 12  ;   ;FA Posting Category ;Option        ;CaptionML=[ENU=FA Posting Category;
                                                              PTB=Categoria Reg. AF];
                                                   OptionCaptionML=[ENU=" ,Disposal,Bal. Disposal";
                                                                    PTB=" ,Descarte,Descarte Bal"];
                                                   OptionString=[ ,Disposal,Bal. Disposal] }
    { 13  ;   ;FA Posting Type     ;Option        ;CaptionML=[ENU=FA Posting Type;
                                                              PTB=Tipo Registo AF];
                                                   OptionCaptionML=[ENU=Acquisition Cost,Depreciation,Write-Down,Appreciation,Custom 1,Custom 2,Proceeds on Disposal,Salvage Value,Gain/Loss,Book Value on Disposal;
                                                                    PTB=Custo Aquisi��o,Deprecia��o,Redu��o,Aprecia��o,Personalizado 1,Personalizado 2,Provis�o Venda/Baixa,Valor Residual,Benef�cio/Preju�zo,Valor L�quido em Venda/Baixa];
                                                   OptionString=Acquisition Cost,Depreciation,Write-Down,Appreciation,Custom 1,Custom 2,Proceeds on Disposal,Salvage Value,Gain/Loss,Book Value on Disposal }
    { 14  ;   ;Amount              ;Decimal       ;CaptionML=[ENU=Amount;
                                                              PTB=Valor];
                                                   AutoFormatType=1 }
    { 15  ;   ;Debit Amount        ;Decimal       ;CaptionML=[ENU=Debit Amount;
                                                              PTB=Valor D�bito];
                                                   AutoFormatType=1 }
    { 16  ;   ;Credit Amount       ;Decimal       ;CaptionML=[ENU=Credit Amount;
                                                              PTB=Valor Cr�dito];
                                                   AutoFormatType=1 }
    { 17  ;   ;Reclassification Entry;Boolean     ;CaptionML=[ENU=Reclassification Entry;
                                                              PTB=Mov. Reclassifica��o] }
    { 18  ;   ;Part of Book Value  ;Boolean       ;CaptionML=[ENU=Part of Book Value;
                                                              PTB=Parte do Valor L�quido] }
    { 19  ;   ;Part of Depreciable Basis;Boolean  ;CaptionML=[ENU=Part of Depreciable Basis;
                                                              PTB=Parte da Base Deprecia��o] }
    { 20  ;   ;Disposal Calculation Method;Option ;CaptionML=[ENU=Disposal Calculation Method;
                                                              PTB=M�todo C�lculo Venda/Baixa];
                                                   OptionCaptionML=[ENU=" ,Net,Gross";
                                                                    PTB=" ,L�quido,Bruto"];
                                                   OptionString=[ ,Net,Gross] }
    { 21  ;   ;Disposal Entry No.  ;Integer       ;CaptionML=[ENU=Disposal Entry No.;
                                                              PTB=M�todo C�l. Venda/Baixa];
                                                   BlankZero=Yes }
    { 22  ;   ;No. of Depreciation Days;Integer   ;CaptionML=[ENU=No. of Depreciation Days;
                                                              PTB=N� Dias Deprecia��o] }
    { 23  ;   ;Quantity            ;Decimal       ;CaptionML=[ENU=Quantity;
                                                              PTB=Quantidade];
                                                   DecimalPlaces=0:5 }
    { 24  ;   ;FA No./Budgeted FA No.;Code20      ;TableRelation="Fixed Asset";
                                                   CaptionML=[ENU=FA No./Budgeted FA No.;
                                                              PTB=N� AF/N� AF Or�ado] }
    { 25  ;   ;FA Subclass Code    ;Code10        ;TableRelation="FA Subclass";
                                                   CaptionML=[ENU=FA Subclass Code;
                                                              PTB=Cod. Subclasse AF] }
    { 26  ;   ;FA Location Code    ;Code10        ;TableRelation="FA Location";
                                                   CaptionML=[ENU=FA Location Code;
                                                              PTB=Cod. Dep�sito AF] }
    { 27  ;   ;FA Posting Group    ;Code10        ;TableRelation="FA Posting Group";
                                                   CaptionML=[ENU=FA Posting Group;
                                                              PTB=Grupo Cont. AF] }
    { 28  ;   ;Global Dimension 1 Code;Code20     ;TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(1));
                                                   CaptionML=[ENU=Global Dimension 1 Code;
                                                              PTB=Cod. Dimens�o 1 Global];
                                                   CaptionClass='1,1,1' }
    { 29  ;   ;Global Dimension 2 Code;Code20     ;TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(2));
                                                   CaptionML=[ENU=Global Dimension 2 Code;
                                                              PTB=Cod. Dimens�o 2 Global];
                                                   CaptionClass='1,1,2' }
    { 30  ;   ;Location Code       ;Code10        ;TableRelation=Location WHERE (Use As In-Transit=CONST(No));
                                                   CaptionML=[ENU=Location Code;
                                                              PTB=Cod. Dep�sito] }
    { 32  ;   ;User ID             ;Code50        ;TableRelation=User."User Name";
                                                   OnLookup=VAR
                                                              UserMgt@1000 : Codeunit 418;
                                                            BEGIN
                                                              UserMgt.LookupUserID("User ID");
                                                            END;

                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=User ID;
                                                              PTB=ID Usu�rio] }
    { 33  ;   ;Depreciation Method ;Option        ;CaptionML=[ENU=Depreciation Method;
                                                              PTB=M�todo Deprecia��o];
                                                   OptionCaptionML=[ENU=Straight-Line,Declining-Balance 1,Declining-Balance 2,DB1/SL,DB2/SL,User-Defined,Manual;
                                                                    PTB=Constantes,Decrescente Saldo 1,Decrescente Saldo,Ds1/C,Ds2/C,Definido por Usu�rio,Manual];
                                                   OptionString=Straight-Line,Declining-Balance 1,Declining-Balance 2,DB1/SL,DB2/SL,User-Defined,Manual }
    { 34  ;   ;Depreciation Starting Date;Date    ;CaptionML=[ENU=Depreciation Starting Date;
                                                              PTB=Data �nicio Deprecia��o] }
    { 35  ;   ;Straight-Line %     ;Decimal       ;CaptionML=[ENU=Straight-Line %;
                                                              PTB=% constante];
                                                   DecimalPlaces=1:1 }
    { 36  ;   ;No. of Depreciation Years;Decimal  ;CaptionML=[ENU=No. of Depreciation Years;
                                                              PTB=N� Anos Deprecia��o];
                                                   DecimalPlaces=0:3 }
    { 37  ;   ;Fixed Depr. Amount  ;Decimal       ;CaptionML=[ENU=Fixed Depr. Amount;
                                                              PTB=Valor Deprecia��o Fixo];
                                                   AutoFormatType=1 }
    { 38  ;   ;Declining-Balance % ;Decimal       ;CaptionML=[ENU=Declining-Balance %;
                                                              PTB=% decrescente];
                                                   DecimalPlaces=1:1 }
    { 39  ;   ;Depreciation Table Code;Code10     ;TableRelation="Depreciation Table Header";
                                                   CaptionML=[ENU=Depreciation Table Code;
                                                              PTB=Cod. Tabela Deprecia��o] }
    { 40  ;   ;Journal Batch Name  ;Code10        ;CaptionML=[ENU=Journal Batch Name;
                                                              PTB=Nome Se��o Di�rio] }
    { 41  ;   ;Source Code         ;Code10        ;TableRelation="Source Code";
                                                   CaptionML=[ENU=Source Code;
                                                              PTB=Cod. Origem] }
    { 42  ;   ;Reason Code         ;Code10        ;TableRelation="Reason Code";
                                                   CaptionML=[ENU=Reason Code;
                                                              PTB=Cod. Raz�o] }
    { 43  ;   ;Transaction No.     ;Integer       ;CaptionML=[ENU=Transaction No.;
                                                              PTB=N� Lan�amento] }
    { 44  ;   ;Bal. Account Type   ;Option        ;CaptionML=[ENU=Bal. Account Type;
                                                              PTB=Tipo Contrapartida];
                                                   OptionCaptionML=[ENU=G/L Account,Customer,Vendor,Bank Account,Fixed Asset;
                                                                    PTB=Conta,Cliente,Fornecedor,Banco,Ativo Fixo];
                                                   OptionString=G/L Account,Customer,Vendor,Bank Account,Fixed Asset }
    { 45  ;   ;Bal. Account No.    ;Code20        ;TableRelation=IF (Bal. Account Type=CONST(G/L Account)) "G/L Account"
                                                                 ELSE IF (Bal. Account Type=CONST(Customer)) Customer
                                                                 ELSE IF (Bal. Account Type=CONST(Vendor)) Vendor
                                                                 ELSE IF (Bal. Account Type=CONST(Bank Account)) "Bank Account"
                                                                 ELSE IF (Bal. Account Type=CONST(Fixed Asset)) "Fixed Asset";
                                                   CaptionML=[ENU=Bal. Account No.;
                                                              PTB=N� Conta Contrap.] }
    { 46  ;   ;VAT Amount          ;Decimal       ;CaptionML=[ENU=VAT Amount;
                                                              PTB=Valor IPI];
                                                   AutoFormatType=1 }
    { 47  ;   ;Gen. Posting Type   ;Option        ;CaptionML=[ENU=Gen. Posting Type;
                                                              PTB=Tipo Registro Geral];
                                                   OptionCaptionML=[ENU=" ,Purchase,Sale,Settlement";
                                                                    PTB=,Compra,Venda,Ajuste];
                                                   OptionString=[ ,Purchase,Sale,Settlement] }
    { 48  ;   ;Gen. Bus. Posting Group;Code10     ;TableRelation="Gen. Business Posting Group";
                                                   CaptionML=[ENU=Gen. Bus. Posting Group;
                                                              PTB=Gr. Cont�bil Neg�cio] }
    { 49  ;   ;Gen. Prod. Posting Group;Code10    ;TableRelation="Gen. Product Posting Group";
                                                   CaptionML=[ENU=Gen. Prod. Posting Group;
                                                              PTB=Gr. Cont�bil Produto] }
    { 50  ;   ;FA Class Code       ;Code10        ;TableRelation="FA Class";
                                                   CaptionML=[ENU=FA Class Code;
                                                              PTB=Cod. Classe AF] }
    { 51  ;   ;FA Exchange Rate    ;Decimal       ;CaptionML=[ENU=FA Exchange Rate;
                                                              PTB=Taxa C�mbio AF];
                                                   AutoFormatType=1 }
    { 52  ;   ;Amount (LCY)        ;Decimal       ;CaptionML=[ENU=Amount (LCY);
                                                              PTB=Valor (ML)];
                                                   AutoFormatType=1 }
    { 53  ;   ;Result on Disposal  ;Option        ;CaptionML=[ENU=Result on Disposal;
                                                              PTB=Resultado venda/baixa];
                                                   OptionCaptionML=[ENU=" ,Gain,Loss";
                                                                    PTB=" ,Ganho,Perda"];
                                                   OptionString=[ ,Gain,Loss] }
    { 54  ;   ;Correction          ;Boolean       ;CaptionML=[ENU=Correction;
                                                              PTB=Corre��o] }
    { 55  ;   ;Index Entry         ;Boolean       ;CaptionML=[ENU=Index Entry;
                                                              PTB=Mov. Ajuste] }
    { 56  ;   ;Canceled from FA No.;Code20        ;TableRelation="Fixed Asset";
                                                   CaptionML=[ENU=Canceled from FA No.;
                                                              PTB=AF cancelado N�] }
    { 57  ;   ;Depreciation Ending Date;Date      ;CaptionML=[ENU=Depreciation Ending Date;
                                                              PTB=Data Final Deprecia��o] }
    { 58  ;   ;Use FA Ledger Check ;Boolean       ;CaptionML=[ENU=Use FA Ledger Check;
                                                              PTB=Usar Verif. Mov. AF] }
    { 59  ;   ;Automatic Entry     ;Boolean       ;CaptionML=[ENU=Automatic Entry;
                                                              PTB=Mov. autom�tico] }
    { 60  ;   ;Depr. Starting Date (Custom 1);Date;CaptionML=[ENU=Depr. Starting Date (Custom 1);
                                                              PTB=Data Inicial Deprec. Especial] }
    { 61  ;   ;Depr. Ending Date (Custom 1);Date  ;CaptionML=[ENU=Depr. Ending Date (Custom 1);
                                                              PTB=Data Final Deprec. Especial] }
    { 62  ;   ;Accum. Depr. % (Custom 1);Decimal  ;CaptionML=[ENU=Accum. Depr. % (Custom 1);
                                                              PTB=Deprecia��o Acum. Especial];
                                                   DecimalPlaces=1:1 }
    { 63  ;   ;Depr. % this year (Custom 1);Decimal;
                                                   CaptionML=[ENU=Depr. % this year (Custom 1);
                                                              PTB=% Deprecia��o Acum. Especial];
                                                   DecimalPlaces=1:1 }
    { 64  ;   ;Property Class (Custom 1);Option   ;CaptionML=[ENU=Property Class (Custom 1);
                                                              PTB=Tipo de Bem (Especial)];
                                                   OptionCaptionML=[ENU=" ,Personal Property,Real Property";
                                                                    PTB=" ,Propriedade Pessoal,Propriedade Real"];
                                                   OptionString=[ ,Personal Property,Real Property] }
    { 65  ;   ;No. Series          ;Code10        ;TableRelation="No. Series";
                                                   CaptionML=[ENU=No. Series;
                                                              PTB=N� S�ries] }
    { 66  ;   ;Tax Area Code       ;Code20        ;TableRelation="Tax Area";
                                                   CaptionML=[ENU=Tax Area Code;
                                                              PTB=C�d. �rea Imposto] }
    { 67  ;   ;Tax Liable          ;Boolean       ;CaptionML=[ENU=Tax Liable;
                                                              PTB=Sujeito a Imposto] }
    { 68  ;   ;Tax Group Code      ;Code10        ;TableRelation="Tax Group";
                                                   CaptionML=[ENU=Tax Group Code;
                                                              PTB=Cod. Gr. Imposto] }
    { 69  ;   ;Use Tax             ;Boolean       ;CaptionML=[ENU=Use Tax;
                                                              PTB=Utiliza Imposto] }
    { 70  ;   ;VAT Bus. Posting Group;Code10      ;TableRelation="VAT Business Posting Group";
                                                   CaptionML=[ENU=VAT Bus. Posting Group;
                                                              PTB=Gr. Registro Imp. Neg�cio] }
    { 71  ;   ;VAT Prod. Posting Group;Code10     ;TableRelation="VAT Product Posting Group";
                                                   CaptionML=[ENU=VAT Prod. Posting Group;
                                                              PTB=Gr. Registo Imp. Produto] }
    { 72  ;   ;Reversed            ;Boolean       ;CaptionML=[ENU=Reversed;
                                                              PTB=Revertido] }
    { 73  ;   ;Reversed by Entry No.;Integer      ;TableRelation="FA Ledger Entry";
                                                   CaptionML=[ENU=Reversed by Entry No.;
                                                              PTB=Revertido por N� Mov.];
                                                   BlankZero=Yes }
    { 74  ;   ;Reversed Entry No.  ;Integer       ;TableRelation="FA Ledger Entry";
                                                   CaptionML=[ENU=Reversed Entry No.;
                                                              PTB=N� Mov. Revertido];
                                                   BlankZero=Yes }
    { 480 ;   ;Dimension Set ID    ;Integer       ;TableRelation="Dimension Set Entry";
                                                   OnLookup=BEGIN
                                                              ShowDimensions;
                                                            END;

                                                   CaptionML=[ENU=Dimension Set ID;
                                                              PTB=ID Dimens�es];
                                                   Editable=No }
    { 52112430;;Branch Code (Old)  ;Code20        ;TableRelation="Branch Information" }
    { 52112440;;Branch Code        ;Code20        ;TableRelation="Branch Information";
                                                   CaptionML=[ENU=Branch Code;
                                                              PTB=C�d. Filial] }
  }
  KEYS
  {
    {    ;Entry No.                               ;Clustered=Yes }
    {    ;FA No.,Depreciation Book Code,FA Posting Date;
                                                   SumIndexFields=Amount }
    {    ;FA No.,Depreciation Book Code,FA Posting Category,FA Posting Type,FA Posting Date,Part of Book Value,Reclassification Entry;
                                                   SumIndexFields=Amount }
    {    ;FA No.,Depreciation Book Code,Part of Book Value,FA Posting Date;
                                                   SumIndexFields=Amount }
    {    ;FA No.,Depreciation Book Code,Part of Depreciable Basis,FA Posting Date;
                                                   SumIndexFields=Amount }
    {    ;FA No.,Depreciation Book Code,FA Posting Category,FA Posting Type,Posting Date;
                                                   SumIndexFields=Amount }
    {    ;Canceled from FA No.,Depreciation Book Code,FA Posting Date }
    {    ;Document No.,Posting Date                }
    {    ;G/L Entry No.                            }
    {    ;Document Type,Document No.               }
    {    ;Transaction No.                          }
    {    ;FA No.,Depreciation Book Code,FA Posting Category,FA Posting Type,Document No. }
  }
  FIELDGROUPS
  {
    { 1   ;DropDown            ;Entry No.,FA No.,FA Posting Date,FA Posting Type,Document Type }
  }
  CODE
  {
    VAR
      FAJnlSetup@1000 : Record 5605;
      DimMgt@1001 : Codeunit 408;
      NextLineNo@1002 : Integer;

    PROCEDURE MoveToGenJnl@3(VAR GenJnlLine@1000 : Record 81);
    BEGIN
      NextLineNo := GenJnlLine."Line No.";
      GenJnlLine."Line No." := 0;
      GenJnlLine.INIT;
      FAJnlSetup.SetGenJnlTrailCodes(GenJnlLine);
      GenJnlLine."FA Posting Type" := ConvertPostingType + 1;
      GenJnlLine."Posting Date" := "Posting Date";
      GenJnlLine."FA Posting Date" := "FA Posting Date";
      IF GenJnlLine."Posting Date" = GenJnlLine."FA Posting Date" THEN
        GenJnlLine."FA Posting Date" := 0D;
      GenJnlLine.VALIDATE("Account Type",GenJnlLine."Account Type"::"Fixed Asset");
      GenJnlLine.VALIDATE("Account No.","FA No.");
      GenJnlLine.VALIDATE("Depreciation Book Code","Depreciation Book Code");
      GenJnlLine.VALIDATE(Amount,Amount);
      GenJnlLine.VALIDATE(Correction,Correction);
      GenJnlLine."Document Type" := "Document Type";
      GenJnlLine."Document No." := "Document No.";
      GenJnlLine."Document Date" := "Document Date";
      GenJnlLine."External Document No." := "External Document No.";
      GenJnlLine.Quantity := Quantity;
      GenJnlLine."No. of Depreciation Days" := "No. of Depreciation Days";
      GenJnlLine."FA Reclassification Entry" := "Reclassification Entry";
      GenJnlLine."Index Entry" := "Index Entry";
      GenJnlLine."Line No." := NextLineNo;
      GenJnlLine."Dimension Set ID" := "Dimension Set ID";
    END;

    PROCEDURE MoveToFAJnl@2(VAR FAJnlLine@1000 : Record 5621);
    BEGIN
      NextLineNo := FAJnlLine."Line No.";
      FAJnlLine."Line No." := 0;
      FAJnlLine.INIT;
      FAJnlSetup.SetFAJnlTrailCodes(FAJnlLine);
      FAJnlLine."FA Posting Type" := ConvertPostingType;
      FAJnlLine."Posting Date" := "Posting Date";
      FAJnlLine."FA Posting Date" := "FA Posting Date";
      IF FAJnlLine."Posting Date" = FAJnlLine."FA Posting Date" THEN
        FAJnlLine."Posting Date" := 0D;
      FAJnlLine.VALIDATE("FA No.","FA No.");
      FAJnlLine.VALIDATE("Depreciation Book Code","Depreciation Book Code");
      FAJnlLine.VALIDATE(Amount,Amount);
      FAJnlLine.VALIDATE(Correction,Correction);
      FAJnlLine.Quantity := Quantity;
      FAJnlLine."Document Type" := "Document Type";
      FAJnlLine."Document No." := "Document No.";
      FAJnlLine."Document Date" := "Document Date";
      FAJnlLine."External Document No." := "External Document No.";
      FAJnlLine."No. of Depreciation Days" := "No. of Depreciation Days";
      FAJnlLine."FA Reclassification Entry" := "Reclassification Entry";
      FAJnlLine."Index Entry" := "Index Entry";
      FAJnlLine."Line No." := NextLineNo;
      FAJnlLine."Dimension Set ID" := "Dimension Set ID";
    END;

    PROCEDURE ConvertPostingType@1() : Option;
    VAR
      FAJnlLine@1000 : Record 5621;
    BEGIN
      CASE "FA Posting Type" OF
        "FA Posting Type"::"Acquisition Cost":
          FAJnlLine."FA Posting Type" := FAJnlLine."FA Posting Type"::"Acquisition Cost";
        "FA Posting Type"::Depreciation:
          FAJnlLine."FA Posting Type" := FAJnlLine."FA Posting Type"::Depreciation;
        "FA Posting Type"::"Write-Down":
          FAJnlLine."FA Posting Type" := FAJnlLine."FA Posting Type"::"Write-Down";
        "FA Posting Type"::Appreciation:
          FAJnlLine."FA Posting Type" := FAJnlLine."FA Posting Type"::Appreciation;
        "FA Posting Type"::"Custom 1":
          FAJnlLine."FA Posting Type" := FAJnlLine."FA Posting Type"::"Custom 1";
        "FA Posting Type"::"Custom 2":
          FAJnlLine."FA Posting Type" := FAJnlLine."FA Posting Type"::"Custom 2";
        "FA Posting Type"::"Proceeds on Disposal":
          FAJnlLine."FA Posting Type" := FAJnlLine."FA Posting Type"::Disposal;
        "FA Posting Type"::"Salvage Value":
          FAJnlLine."FA Posting Type" := FAJnlLine."FA Posting Type"::"Salvage Value";
      END;
      EXIT(FAJnlLine."FA Posting Type");
    END;

    PROCEDURE ShowDimensions@4();
    BEGIN
      DimMgt.ShowDimensionSet("Dimension Set ID",STRSUBSTNO('%1 %2',TABLECAPTION,"Entry No."));
    END;

    BEGIN
    END.
  }
}

