OBJECT Table 5790 Shipping Agent Services
{
  OBJECT-PROPERTIES
  {
    Date=07/09/12;
    Time=12:00:00;
    Version List=NAVW17.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Shipping Agent Services;
               PTB=Servi�os Transportador];
    LookupPageID=Page5790;
    DrillDownPageID=Page5790;
  }
  FIELDS
  {
    { 1   ;   ;Shipping Agent Code ;Code10        ;TableRelation="Shipping Agent";
                                                   CaptionML=[ENU=Shipping Agent Code;
                                                              PTB=Cod. Transportador];
                                                   NotBlank=Yes }
    { 2   ;   ;Code                ;Code10        ;CaptionML=[ENU=Code;
                                                              PTB=C�digo];
                                                   NotBlank=Yes }
    { 3   ;   ;Description         ;Text50        ;CaptionML=[ENU=Description;
                                                              PTB=Descri��o] }
    { 4   ;   ;Shipping Time       ;DateFormula   ;OnValidate=VAR
                                                                DateTest@1000 : Date;
                                                              BEGIN
                                                                DateTest := CALCDATE("Shipping Time",WORKDATE);
                                                                IF DateTest < WORKDATE THEN
                                                                  ERROR(Text000,FIELDCAPTION("Shipping Time"));
                                                              END;

                                                   CaptionML=[ENU=Shipping Time;
                                                              PTB=Tempo Expedi��o] }
    { 7600;   ;Base Calendar Code  ;Code10        ;TableRelation="Base Calendar";
                                                   CaptionML=[ENU=Base Calendar Code;
                                                              PTB=Cod. Calend�rio Base] }
  }
  KEYS
  {
    {    ;Shipping Agent Code,Code                ;Clustered=Yes }
  }
  FIELDGROUPS
  {
    { 1   ;DropDown            ;Code,Description,Shipping Time           }
  }
  CODE
  {
    VAR
      Text000@1000 : TextConst 'ENU=The %1 cannot be negative.;PTB=O %1 n�o pode ser negativo.';

    BEGIN
    END.
  }
}

