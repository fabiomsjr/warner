OBJECT Table 52112574 Model Fiscal Type.
{
  OBJECT-PROPERTIES
  {
    Date=30/04/13;
    Time=18:14:18;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Model Fiscal Type;
               PTB=Tipo Modelo Fiscal];
  }
  FIELDS
  {
    { 1   ;   ;Model Code          ;Code2         ;CaptionML=PTB=C�d. Modelo }
    { 2   ;   ;Description         ;Text100       ;CaptionML=PTB=Descri��o Modelo }
    { 3   ;   ;Reference Model Code;Code10        ;CaptionML=PTB=C�d. Modelo Referenciado }
  }
  KEYS
  {
    {    ;Model Code,Reference Model Code         ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

