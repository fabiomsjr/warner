OBJECT Table 52112557 SPED PC Consolidation
{
  OBJECT-PROPERTIES
  {
    Date=30/04/13;
    Time=16:53:57;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    OnDelete=VAR
               line@1102300000 : Record 52112558;
             BEGIN
               line.SETRANGE("Settlement No.", "No.");
               line.DELETEALL;
             END;

    CaptionML=PTB=Apura��o de Impostos;
    LookupPageID=Page52112544;
    DrillDownPageID=Page52112544;
  }
  FIELDS
  {
    { 1   ;   ;No.                 ;Code20        ;CaptionML=[ENU=No.;
                                                              PTB=N�] }
    { 3   ;   ;Start Date          ;Date          ;CaptionML=[ENU=Start Date;
                                                              PTB=Data Inicio] }
    { 4   ;   ;End Date            ;Date          ;CaptionML=[ENU=End Date;
                                                              PTB=Data Fim] }
    { 10  ;   ;File Type           ;Option        ;CaptionML=PTB=Tipo Arquivo;
                                                   OptionCaptionML=PTB=0-Remessa Arquivo Original,1-Remessa Arquivo Substituto;
                                                   OptionString=0-Remessa Arquivo Original,1-Remessa Arquivo Substituto }
  }
  KEYS
  {
    {    ;No.                                     ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      SetupGL@1102300001 : Record 98;
      NoSeriesMgt@1102300000 : Codeunit 396;
      Text001@1102300003 : TextConst 'ENU=When you mark %1, only the EDF will be processed. Continue?;PTB=Ao marcar %1, somente o EDF ser� processado. Deseja continuar?';
      Text011@1102300006 : TextConst 'ENU=If you change the contents of the %1 field, the analysis view entries will be deleted.;PTB=Se alterar o conte�do do campo %1, os movimentos dos dados an�lise ser�o eliminados.';
      Text012@1102300005 : TextConst 'ENU=\You will have to update again.\\Do you want to enter a new value in the %1 field?;PTB=\Ter� que atualizar novamente.\\Deseja introduzir um novo valor no campo %1?';
      Text013@1102300004 : TextConst 'ENU=The update has been interrupted in response to the warning.;PTB=A atualiza��o foi interrompida em resposta ao aviso.';

    PROCEDURE AssistEdit@2(OldApura��o@1000 : Record 52112523) : Boolean;
    BEGIN
    END;

    LOCAL PROCEDURE TestNoSeries@6() : Boolean;
    BEGIN
    END;

    LOCAL PROCEDURE GetNoSeriesCode@9() : Code[10];
    BEGIN
    END;

    PROCEDURE ValidateDelete@10(FieldName@1000 : Text[100]);
    VAR
      Question@1001 : Text[250];
    BEGIN
    END;

    PROCEDURE AnalysisViewReset@5();
    VAR
      AnalysisViewEntry@1000 : Record 365;
    BEGIN
    END;

    BEGIN
    END.
  }
}

