OBJECT Report 52113024 Update CNAB Layout Line Order
{
  OBJECT-PROPERTIES
  {
    Date=22/07/13;
    Time=17:33:27;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    ProcessingOnly=Yes;
  }
  DATASET
  {
    { 52006500;;DataItem;                    ;
               DataItemTable=Table52113024;
               OnAfterGetRecord=BEGIN
                                  UpdateOrder;
                                  MODIFY;
                                END;

               ReqFilterFields=Layout Code }

  }
  REQUESTPAGE
  {
    PROPERTIES
    {
    }
    CONTROLS
    {
    }
  }
  LABELS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
  RDLDATA
  {
  }
}

