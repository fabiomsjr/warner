OBJECT Table 5525 Manufacturing User Template
{
  OBJECT-PROPERTIES
  {
    Date=09/09/14;
    Time=12:00:00;
    Version List=NAVW18.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Manufacturing User Template;
               PTB=Modelo Usu�rio Manufatura];
  }
  FIELDS
  {
    { 1   ;   ;User ID             ;Code50        ;TableRelation=User."User Name";
                                                   OnValidate=VAR
                                                                UserMgt@1000 : Codeunit 418;
                                                              BEGIN
                                                                UserMgt.ValidateUserID("User ID");
                                                              END;

                                                   OnLookup=VAR
                                                              UserMgt@1000 : Codeunit 418;
                                                            BEGIN
                                                              UserMgt.LookupUserID("User ID");
                                                            END;

                                                   ValidateTableRelation=No;
                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=User ID;
                                                              PTB=ID Usu�rio] }
    { 2   ;   ;Create Purchase Order;Option       ;AccessByPermission=TableData 120=R;
                                                   CaptionML=[ENU=Create Purchase Order;
                                                              PTB=Criar Pedido de Compra];
                                                   OptionCaptionML=[ENU=" ,Make Purch. Orders,Make Purch. Orders & Print,Copy to Req. Wksh";
                                                                    PTB=" ,Fazer Ordens de Compra,Fazer Ordens de Compra &Imprimir,Copiar para Folha de Requisi��o"];
                                                   OptionString=[ ,Make Purch. Orders,Make Purch. Orders & Print,Copy to Req. Wksh] }
    { 3   ;   ;Create Production Order;Option     ;CaptionML=[ENU=Create Production Order;
                                                              PTB=Criar Ordem de Produ��o];
                                                   OptionCaptionML=[ENU=" ,Planned,Firm Planned,Firm Planned & Print,Copy to Req. Wksh";
                                                                    PTB=" ,Planejada,Planejada Firme,Planejada Firme & Impressa,Copiar para Folha de Requisi��o"];
                                                   OptionString=[ ,Planned,Firm Planned,Firm Planned & Print,Copy to Req. Wksh] }
    { 4   ;   ;Create Transfer Order;Option       ;AccessByPermission=TableData 5740=R;
                                                   CaptionML=[ENU=Create Transfer Order;
                                                              PTB=Criar Ordem de Transf.];
                                                   OptionCaptionML=[ENU=" ,Make Trans. Orders,Make Trans. Order & Print,Copy to Req. Wksh";
                                                                    PTB=" ,Fazer Ordens de Transf.,Fazer Ordens de Transf. & Imprimir,Copiar para Folha de Requisi��o"];
                                                   OptionString=[ ,Make Trans. Orders,Make Trans. Order & Print,Copy to Req. Wksh] }
    { 5   ;   ;Create Assembly Order;Option       ;AccessByPermission=TableData 90=R;
                                                   CaptionML=ENU=Create Assembly Order;
                                                   OptionCaptionML=ENU=" ,Make Assembly Orders,Make Assembly Orders & Print";
                                                   OptionString=[ ,Make Assembly Orders,Make Assembly Orders & Print] }
    { 11  ;   ;Purchase Req. Wksh. Template;Code10;TableRelation="Req. Wksh. Template";
                                                   CaptionML=[ENU=Purchase Req. Wksh. Template;
                                                              PTB=Modelo Folha de Requisi��o Compra] }
    { 12  ;   ;Purchase Wksh. Name ;Code10        ;TableRelation="Requisition Wksh. Name".Name WHERE (Worksheet Template Name=FIELD(Purchase Req. Wksh. Template));
                                                   CaptionML=[ENU=Purchase Wksh. Name;
                                                              PTB=Nome Folha Compra] }
    { 15  ;   ;Prod. Req. Wksh. Template;Code10   ;TableRelation="Req. Wksh. Template";
                                                   CaptionML=[ENU=Prod. Req. Wksh. Template;
                                                              PTB=Modelo Folha Requisi��o Produ��o] }
    { 16  ;   ;Prod. Wksh. Name    ;Code10        ;TableRelation="Requisition Wksh. Name".Name WHERE (Worksheet Template Name=FIELD(Prod. Req. Wksh. Template));
                                                   CaptionML=[ENU=Prod. Wksh. Name;
                                                              PTB=Nome Folha Produ��o] }
    { 19  ;   ;Transfer Req. Wksh. Template;Code10;TableRelation="Req. Wksh. Template";
                                                   CaptionML=[ENU=Transfer Req. Wksh. Template;
                                                              PTB=Modelo Folha Requisi��o Transfer] }
    { 20  ;   ;Transfer Wksh. Name ;Code10        ;TableRelation="Requisition Wksh. Name".Name WHERE (Worksheet Template Name=FIELD(Transfer Req. Wksh. Template));
                                                   CaptionML=[ENU=Transfer Wksh. Name;
                                                              PTB=Transferir Nome Folha] }
    { 21  ;   ;Make Orders         ;Option        ;CaptionML=[ENU=Make Orders;
                                                              PTB=Criar Pedido];
                                                   OptionCaptionML=[ENU=The Active Line,The Active Order,All Lines;
                                                                    PTB=A Linha Ativa,o Pedido Ativo,Todas Linhas];
                                                   OptionString=The Active Line,The Active Order,All Lines }
  }
  KEYS
  {
    {    ;User ID                                 ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

