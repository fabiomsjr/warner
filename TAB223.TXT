OBJECT Table 223 Drop Shpt. Post. Buffer
{
  OBJECT-PROPERTIES
  {
    Date=05/11/08;
    Time=12:00:00;
    Version List=NAVW16.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Drop Shpt. Post. Buffer;
               PTB=Mem. Int. Envio Endere�o];
  }
  FIELDS
  {
    { 1   ;   ;Order No.           ;Code20        ;CaptionML=[ENU=Order No.;
                                                              PTB=N� Pedido] }
    { 2   ;   ;Order Line No.      ;Integer       ;CaptionML=[ENU=Order Line No.;
                                                              PTB=N� Linha Pedido] }
    { 3   ;   ;Item Shpt. Entry No.;Integer       ;CaptionML=[ENU=Item Shpt. Entry No.;
                                                              PTB=N� Ordem Mov. Prod. Assoc.] }
    { 4   ;   ;Quantity            ;Decimal       ;CaptionML=[ENU=Quantity;
                                                              PTB=Quantidade];
                                                   DecimalPlaces=0:5 }
    { 5   ;   ;Quantity (Base)     ;Decimal       ;CaptionML=[ENU=Quantity (Base);
                                                              PTB=Qtd. (Base)];
                                                   DecimalPlaces=0:5 }
  }
  KEYS
  {
    {    ;Order No.,Order Line No.                ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

