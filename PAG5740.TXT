OBJECT Page 5740 Transfer Order
{
  OBJECT-PROPERTIES
  {
    Date=05/06/15;
    Time=13:46:58;
    Version List=NAVW18.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Transfer Order;
               PTB=Ordem Transfer�ncia];
    SourceTable=Table5740;
    PageType=Document;
    RefreshOnActivate=Yes;
    OnDeleteRecord=BEGIN
                     TESTFIELD(Status,Status::Open);
                   END;

    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 57      ;1   ;ActionGroup;
                      CaptionML=[ENU=O&rder;
                                 PTB=O&rdem];
                      Image=Order }
      { 27      ;2   ;Action    ;
                      ShortCutKey=F7;
                      CaptionML=[ENU=Statistics;
                                 PTB=Estat�sticas];
                      RunObject=Page 5755;
                      RunPageLink=No.=FIELD(No.);
                      Promoted=Yes;
                      Image=Statistics;
                      PromotedCategory=Process }
      { 28      ;2   ;Action    ;
                      CaptionML=[ENU=Co&mments;
                                 PTB=Co&ment�rios];
                      RunObject=Page 5750;
                      RunPageLink=Document Type=CONST(Transfer Order),
                                  No.=FIELD(No.);
                      Image=ViewComments }
      { 65      ;2   ;Action    ;
                      Name=Dimensions;
                      AccessByPermission=TableData 348=R;
                      ShortCutKey=Shift+Ctrl+D;
                      CaptionML=[ENU=Dimensions;
                                 PTB=Dimens�es];
                      Image=Dimensions;
                      OnAction=BEGIN
                                 ShowDocDim;
                                 CurrPage.SAVERECORD;
                               END;
                                }
      { 5       ;1   ;ActionGroup;
                      CaptionML=[ENU=Documents;
                                 PTB=Documentos];
                      Image=Documents }
      { 81      ;2   ;Action    ;
                      CaptionML=[ENU=S&hipments;
                                 PTB=Envios];
                      RunObject=Page 5752;
                      RunPageLink=Transfer Order No.=FIELD(No.);
                      Image=Shipment }
      { 82      ;2   ;Action    ;
                      CaptionML=[ENU=Re&ceipts;
                                 PTB=Recebimentos];
                      RunObject=Page 5753;
                      RunPageLink=Transfer Order No.=FIELD(No.);
                      Image=PostedReceipts }
      { 7       ;1   ;ActionGroup;
                      CaptionML=[ENU=Warehouse;
                                 PTB=Armaz�m];
                      Image=Warehouse }
      { 98      ;2   ;Action    ;
                      CaptionML=[ENU=Whse. Shi&pments;
                                 PTB=Envios Armaz�m];
                      RunObject=Page 7341;
                      RunPageView=SORTING(Source Type,Source Subtype,Source No.,Source Line No.);
                      RunPageLink=Source Type=CONST(5741),
                                  Source Subtype=CONST(0),
                                  Source No.=FIELD(No.);
                      Image=Shipment }
      { 97      ;2   ;Action    ;
                      CaptionML=[ENU=&Whse. Receipts;
                                 PTB=Recebimentos Armaz�m];
                      RunObject=Page 7342;
                      RunPageView=SORTING(Source Type,Source Subtype,Source No.,Source Line No.);
                      RunPageLink=Source Type=CONST(5741),
                                  Source Subtype=CONST(1),
                                  Source No.=FIELD(No.);
                      Image=Receipt }
      { 85      ;2   ;Action    ;
                      CaptionML=[ENU=In&vt. Put-away/Pick Lines;
                                 PTB=Linhas Separa��o/Picking Invent�rio];
                      RunObject=Page 5774;
                      RunPageView=SORTING(Source Document,Source No.,Location Code);
                      RunPageLink=Source Document=FILTER(Inbound Transfer|Outbound Transfer),
                                  Source No.=FIELD(No.);
                      Image=PickLines }
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 69      ;1   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[ENU=&Print;
                                 PTB=&Imprimir];
                      Promoted=Yes;
                      Image=Print;
                      PromotedCategory=Process;
                      OnAction=VAR
                                 DocPrint@1001 : Codeunit 229;
                               BEGIN
                                 DocPrint.PrintTransferHeader(Rec);
                               END;
                                }
      { 3       ;1   ;ActionGroup;
                      CaptionML=[ENU=Release;
                                 PTB=Liberar];
                      Image=ReleaseDoc }
      { 59      ;2   ;Action    ;
                      ShortCutKey=Ctrl+F9;
                      CaptionML=[ENU=Re&lease;
                                 PTB=&Liberar];
                      RunObject=Codeunit 5708;
                      Promoted=Yes;
                      Image=ReleaseDoc;
                      PromotedCategory=Process }
      { 48      ;2   ;Action    ;
                      CaptionML=[ENU=Reo&pen;
                                 PTB=Reabrir];
                      Promoted=Yes;
                      Image=ReOpen;
                      PromotedCategory=Process;
                      OnAction=VAR
                                 ReleaseTransferDoc@1001 : Codeunit 5708;
                               BEGIN
                                 ReleaseTransferDoc.Reopen(Rec);
                               END;
                                }
      { 58      ;1   ;ActionGroup;
                      CaptionML=[ENU=F&unctions;
                                 PTB=F&un��es];
                      Image=Action }
      { 5778    ;2   ;Action    ;
                      AccessByPermission=TableData 7320=R;
                      CaptionML=[ENU=Create Whse. S&hipment;
                                 PTB=Criar Envio Armaz�m];
                      Image=NewShipment;
                      OnAction=VAR
                                 GetSourceDocOutbound@1001 : Codeunit 5752;
                               BEGIN
                                 GetSourceDocOutbound.CreateFromOutbndTransferOrder(Rec);
                               END;
                                }
      { 84      ;2   ;Action    ;
                      AccessByPermission=TableData 7316=R;
                      CaptionML=[ENU=Create &Whse. Receipt;
                                 PTB=Criar Recebimento Armaz�m];
                      Image=NewReceipt;
                      OnAction=VAR
                                 GetSourceDocInbound@1001 : Codeunit 5751;
                               BEGIN
                                 GetSourceDocInbound.CreateFromInbndTransferOrder(Rec);
                               END;
                                }
      { 94      ;2   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[ENU=Create Inventor&y Put-away/Pick;
                                 PTB=Create Inventor&y Put-away / Pick];
                      Image=CreateInventoryPickup;
                      OnAction=BEGIN
                                 CreateInvtPutAwayPick;
                               END;
                                }
      { 95      ;2   ;Action    ;
                      AccessByPermission=TableData 7302=R;
                      Ellipsis=Yes;
                      CaptionML=[ENU=Get Bin Content;
                                 PTB=Get Bin Content];
                      Image=GetBinContent;
                      OnAction=VAR
                                 BinContent@1002 : Record 7302;
                                 GetBinContent@1000 : Report 7391;
                               BEGIN
                                 BinContent.SETRANGE("Location Code","Transfer-from Code");
                                 GetBinContent.SETTABLEVIEW(BinContent);
                                 GetBinContent.InitializeTransferHeader(Rec);
                                 GetBinContent.RUNMODAL;
                               END;
                                }
      { 62      ;1   ;ActionGroup;
                      CaptionML=[ENU=P&osting;
                                 PTB=Re&gistro];
                      Image=Post }
      { 66      ;2   ;Action    ;
                      ShortCutKey=F9;
                      Ellipsis=Yes;
                      CaptionML=[ENU=P&ost;
                                 PTB=Re&gistrar];
                      RunObject=Codeunit 5706;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=PostOrder;
                      PromotedCategory=Process }
      { 67      ;2   ;Action    ;
                      ShortCutKey=Shift+F9;
                      CaptionML=[ENU=Post and &Print;
                                 PTB=Registrar e &Imprimir];
                      RunObject=Codeunit 5707;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=PostPrint;
                      PromotedCategory=Process }
      { 1900000006;0 ;ActionContainer;
                      ActionContainerType=Reports }
      { 1901320106;1 ;Action    ;
                      CaptionML=[ENU=Inventory - Inbound Transfer;
                                 PTB=Invent�rio - Entrada Transfer�ncia];
                      RunObject=Report 5702;
                      Promoted=Yes;
                      Image=Report;
                      PromotedCategory=Report }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                CaptionML=[ENU=General;
                           PTB=Geral] }

    { 2   ;2   ;Field     ;
                SourceExpr="No.";
                Importance=Promoted;
                OnAssistEdit=BEGIN
                               IF AssistEdit(xRec) THEN
                                 CurrPage.UPDATE;
                             END;
                              }

    { 14  ;2   ;Field     ;
                SourceExpr="Transfer-from Code";
                Importance=Promoted }

    { 34  ;2   ;Field     ;
                SourceExpr="Transfer-to Code";
                Importance=Promoted }

    { 8   ;2   ;Field     ;
                SourceExpr="In-Transit Code" }

    { 4   ;2   ;Field     ;
                SourceExpr="Posting Date";
                OnValidate=BEGIN
                             PostingDateOnAfterValidate;
                           END;
                            }

    { 10  ;2   ;Field     ;
                SourceExpr="Shortcut Dimension 1 Code" }

    { 12  ;2   ;Field     ;
                SourceExpr="Shortcut Dimension 2 Code" }

    { 106 ;2   ;Field     ;
                SourceExpr="Assigned User ID" }

    { 6   ;2   ;Field     ;
                SourceExpr=Status;
                Importance=Promoted }

    { 55  ;1   ;Part      ;
                Name=TransferLines;
                SubPageLink=Document No.=FIELD(No.),
                            Derived From Line No.=CONST(0);
                PagePartID=Page5741 }

    { 1904655901;1;Group  ;
                CaptionML=[ENU=Transfer-from;
                           PTB=Transferir De];
                GroupType=Group }

    { 16  ;2   ;Field     ;
                SourceExpr="Transfer-from Name" }

    { 18  ;2   ;Field     ;
                SourceExpr="Transfer-from Name 2" }

    { 20  ;2   ;Field     ;
                SourceExpr="Transfer-from Address" }

    { 22  ;2   ;Field     ;
                SourceExpr="Transfer-from Address 2" }

    { 24  ;2   ;Field     ;
                SourceExpr="Transfer-from Post Code" }

    { 26  ;2   ;Field     ;
                SourceExpr="Transfer-from City" }

    { 49  ;2   ;Field     ;
                SourceExpr="Transfer-from Contact" }

    { 32  ;2   ;Field     ;
                SourceExpr="Shipment Date";
                Importance=Promoted;
                OnValidate=BEGIN
                             ShipmentDateOnAfterValidate;
                           END;
                            }

    { 89  ;2   ;Field     ;
                SourceExpr="Outbound Whse. Handling Time";
                OnValidate=BEGIN
                             OutboundWhseHandlingTimeOnAfte;
                           END;
                            }

    { 79  ;2   ;Field     ;
                SourceExpr="Shipment Method Code" }

    { 72  ;2   ;Field     ;
                SourceExpr="Shipping Agent Code";
                Importance=Promoted;
                OnValidate=BEGIN
                             ShippingAgentCodeOnAfterValida;
                           END;
                            }

    { 74  ;2   ;Field     ;
                SourceExpr="Shipping Agent Service Code";
                OnValidate=BEGIN
                             ShippingAgentServiceCodeOnAfte;
                           END;
                            }

    { 76  ;2   ;Field     ;
                SourceExpr="Shipping Time";
                OnValidate=BEGIN
                             ShippingTimeOnAfterValidate;
                           END;
                            }

    { 70  ;2   ;Field     ;
                SourceExpr="Shipping Advice";
                Importance=Promoted;
                OnValidate=BEGIN
                             IF "Shipping Advice" <> xRec."Shipping Advice" THEN
                               IF NOT CONFIRM(Text000,FALSE,FIELDCAPTION("Shipping Advice")) THEN
                                 ERROR('');
                           END;
                            }

    { 1901454601;1;Group  ;
                CaptionML=[ENU=Transfer-to;
                           PTB=Transferir Para];
                GroupType=Group }

    { 36  ;2   ;Field     ;
                SourceExpr="Transfer-to Name" }

    { 38  ;2   ;Field     ;
                SourceExpr="Transfer-to Name 2" }

    { 40  ;2   ;Field     ;
                SourceExpr="Transfer-to Address" }

    { 42  ;2   ;Field     ;
                SourceExpr="Transfer-to Address 2" }

    { 44  ;2   ;Field     ;
                SourceExpr="Transfer-to Post Code" }

    { 46  ;2   ;Field     ;
                SourceExpr="Transfer-to City" }

    { 51  ;2   ;Field     ;
                SourceExpr="Transfer-to Contact" }

    { 52  ;2   ;Field     ;
                SourceExpr="Receipt Date";
                OnValidate=BEGIN
                             ReceiptDateOnAfterValidate;
                           END;
                            }

    { 91  ;2   ;Field     ;
                SourceExpr="Inbound Whse. Handling Time";
                OnValidate=BEGIN
                             InboundWhseHandlingTimeOnAfter;
                           END;
                            }

    { 1907468901;1;Group  ;
                CaptionML=[ENU=Foreign Trade;
                           PTB=Internacional] }

    { 104 ;2   ;Field     ;
                SourceExpr="Transaction Type";
                Importance=Promoted }

    { 102 ;2   ;Field     ;
                SourceExpr="Transaction Specification" }

    { 100 ;2   ;Field     ;
                SourceExpr="Transport Method";
                Importance=Promoted }

    { 96  ;2   ;Field     ;
                SourceExpr=Area }

    { 83  ;2   ;Field     ;
                SourceExpr="Entry/Exit Point" }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=TRUE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      Text000@1000 : TextConst 'ENU=Do you want to change %1 in all related records in the warehouse?;PTB=Voc� quer alterar %1 em todos os registros relacionados no armazem?';

    LOCAL PROCEDURE PostingDateOnAfterValidate@19003005();
    BEGIN
      CurrPage.TransferLines.PAGE.UpdateForm(TRUE);
    END;

    LOCAL PROCEDURE ShipmentDateOnAfterValidate@19068710();
    BEGIN
      CurrPage.TransferLines.PAGE.UpdateForm(TRUE);
    END;

    LOCAL PROCEDURE ShippingAgentServiceCodeOnAfte@19046563();
    BEGIN
      CurrPage.TransferLines.PAGE.UpdateForm(TRUE);
    END;

    LOCAL PROCEDURE ShippingAgentCodeOnAfterValida@19008956();
    BEGIN
      CurrPage.TransferLines.PAGE.UpdateForm(TRUE);
    END;

    LOCAL PROCEDURE ShippingTimeOnAfterValidate@19029567();
    BEGIN
      CurrPage.TransferLines.PAGE.UpdateForm(TRUE);
    END;

    LOCAL PROCEDURE OutboundWhseHandlingTimeOnAfte@19078070();
    BEGIN
      CurrPage.TransferLines.PAGE.UpdateForm(TRUE);
    END;

    LOCAL PROCEDURE ReceiptDateOnAfterValidate@19074743();
    BEGIN
      CurrPage.TransferLines.PAGE.UpdateForm(TRUE);
    END;

    LOCAL PROCEDURE InboundWhseHandlingTimeOnAfter@19076948();
    BEGIN
      CurrPage.TransferLines.PAGE.UpdateForm(TRUE);
    END;

    BEGIN
    END.
  }
}

