OBJECT Table 52112874 ECF Excluded Entry
{
  OBJECT-PROPERTIES
  {
    Date=29/06/15;
    Time=10:15:29;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Excluded Entry;
               PTB=Mov. Exclu�do];
  }
  FIELDS
  {
    { 10  ;   ;Posting Code        ;Code10        ;TableRelation="ECF Posting Code".Code;
                                                   CaptionML=[ENU=Posting Code;
                                                              PTB=C�d. Lan�amento] }
    { 20  ;   ;Line No.            ;Integer       ;CaptionML=[ENU=Line No.;
                                                              PTB=N� Linha] }
    { 24  ;   ;Account Type        ;Option        ;CaptionML=[ENU=Account Type;
                                                              PTB=Tipo Conta];
                                                   OptionCaptionML=[ENU=G/L,B-Part;
                                                                    PTB=Cont�bil,Parte B];
                                                   OptionString=GL,BPart }
    { 30  ;   ;Entry No.           ;Integer       ;CaptionML=[ENU=Entry No.;
                                                              PTB=N� Mov.] }
  }
  KEYS
  {
    {    ;Posting Code,Line No.,Account Type,Entry No.;
                                                   Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    PROCEDURE ToggleSelected@52006500(PostingCode@52006502 : Code[20];LineNo@52006503 : Integer;AcctType@52006500 : Integer;EntryNo@52006504 : Integer;Selected@52006501 : Boolean);
    BEGIN
      "Posting Code" := PostingCode;
      "Line No." := LineNo;
      "Account Type" := AcctType;
      "Entry No." := EntryNo;
      IF Selected THEN BEGIN
        IF FIND THEN
          DELETE;
      END ELSE BEGIN
        IF NOT FIND THEN
          INSERT;
      END;
    END;

    BEGIN
    END.
  }
}

