OBJECT Table 35000760 EFD C4902
{
  OBJECT-PROPERTIES
  {
    Date=04/04/12;
    Time=11:59:13;
    Version List=FNX048.00;
  }
  PROPERTIES
  {
    LookupPageID=Page50022;
    DrillDownPageID=Page50022;
  }
  FIELDS
  {
    { 1   ;   ;Tax Settlement No.  ;Code20        ;TableRelation="Tax Settlement 20092" }
    { 3   ;   ;Company ID          ;Text30         }
    { 10  ;   ;Start Date          ;Date           }
    { 15  ;   ;End Date            ;Date           }
    { 20  ;   ;Document Model      ;Code20         }
  }
  KEYS
  {
    {    ;Tax Settlement No.,Company ID,Start Date,End Date,Document Model;
                                                   Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    PROCEDURE RegCode@35000000() : Code[10];
    BEGIN
      EXIT('C490');
    END;

    BEGIN
    {
      --- FUM0089 ---
      rafaelr,281211,00,Corre��es SPED PC
    }
    END.
  }
}

