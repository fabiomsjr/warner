OBJECT Table 73000 NAV Source Buddy
{
  OBJECT-PROPERTIES
  {
    Date=01/05/15;
    Time=17:50:25;
    Version List=;
  }
  PROPERTIES
  {
    DataPerCompany=No;
    CaptionML=[ENU=NAV Source Buddy;
               PTB=NAV Source Buddy];
  }
  FIELDS
  {
    { 10  ;   ;User ID             ;Code50        ;CaptionML=[ENU=User ID;
                                                              PTB=ID Usu�rio] }
    { 20  ;   ;Repository Path     ;Text250       ;CaptionML=[ENU=Repository Path;
                                                              PTB=Caminho Reposit�rio] }
    { 30  ;   ;Finsql Path         ;Text250       ;CaptionML=[ENU=Finsql Path;
                                                              PTB=Caminho Finsql] }
  }
  KEYS
  {
    {    ;User ID                                 ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      Dlg@52006500 : Dialog;
      ExportingObjects@52006501 : TextConst 'ENU=Exporting objects...;PTB=Exportando objetos...';
      SplittingObject@52006502 : TextConst 'ENU=Splitting objects: #1###########################################################################;PTB=Dividindo objetos: #1###########################################################################';
      ExportingObject@52006505 : TextConst 'ENU=Exporting object: #1###########;PTB=Exportando objeto: #1###########';
      SelectRepFolder@52006503 : TextConst 'ENU=Select the repository folder.;PTB=Selecione a pasta do reposit�rio.';
      InvalidObjectType@52006504 : TextConst 'ENU=Invalid object type: %1.;PTB=Tipo de objeto inv�lido: %1.';
      SelectObjectsToImport@52006506 : TextConst 'ENU=Select the objects to be imported.;PTB=Selecione os objetos a serem importados.';

    PROCEDURE Setup@52006504();
    VAR
      fileMgt@52006500 : Codeunit 419;
      appDomain@52006501 : DotNet "'mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.AppDomain" RUNONCLIENT;
    BEGIN
      INIT;
      "User ID" := USERID;
      IF NOT FIND THEN
        INSERT;
      IF "Repository Path" = '' THEN BEGIN
        "Repository Path" := fileMgt.BrowseForFolderDialog(SelectRepFolder, '', TRUE);
        MODIFY;
      END;
      IF "Finsql Path" = '' THEN BEGIN
        "Finsql Path" := DELCHR(appDomain.CurrentDomain.BaseDirectory, '>', '\') + '\finsql.exe';
      END;
      MODIFY;
    END;

    PROCEDURE GetSetup@52006501(VAR repositoryPath@52006514 : Text;VAR databaseName@52006503 : Text;VAR databaseServer@52006502 : Text;VAR databaseInstance@52006501 : Text;VAR finsqlPath@52006500 : Text);
    VAR
      serverFile@52006513 : DotNet "'mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.IO.File";
      xmlDoc@52006512 : DotNet "'System.Xml, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.Xml.XmlDocument";
      xmlNode@52006511 : DotNet "'System.Xml, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.Xml.XmlNode";
      environment@52006510 : DotNet "'mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.Environment" RUNONCLIENT;
      environmentSpecialFolder@52006509 : DotNet "'mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.Environment+SpecialFolder" RUNONCLIENT;
      systemIOFile@52006508 : DotNet "'mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.IO.File" RUNONCLIENT;
      httpUtility@52006507 : DotNet "'System.Web, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'.System.Web.HttpUtility";
      session@52006505 : Record 2000000009;
      activeSession@52006504 : Record 2000000110;
    BEGIN
      GET(USERID);
      TESTFIELD("Repository Path");
      repositoryPath := "Repository Path";
      IF "Finsql Path" = '' THEN
        Setup;
      finsqlPath := "Finsql Path";

      activeSession.SETRANGE("Session ID", SESSIONID);
      activeSession.FINDFIRST;
      xmlDoc := xmlDoc.XmlDocument;
      IF serverFile.Exists(APPLICATIONPATH + 'Instances\' + activeSession."Server Instance Name" + '\CustomSettings.config') THEN
        xmlDoc.Load(APPLICATIONPATH + 'Instances\' + activeSession."Server Instance Name" + '\CustomSettings.config')
      ELSE
        xmlDoc.Load(APPLICATIONPATH + 'CustomSettings.config');
      xmlNode := xmlDoc.SelectSingleNode('//appSettings/add[@key=''DatabaseServer'']');
      databaseServer := xmlNode.Attributes.Item(1).InnerText;
      xmlNode := xmlDoc.SelectSingleNode('//appSettings/add[@key=''DatabaseInstance'']');
      IF NOT ISNULL(xmlNode) THEN
        databaseInstance := xmlNode.Attributes.Item(1).InnerText;
      IF databaseInstance <> '' THEN
        databaseServer := databaseServer + '\' + databaseInstance;

      session.SETRANGE("My Session",TRUE);
      session.FINDFIRST;
      databaseName := session."Database Name";
    END;

    PROCEDURE ExportEach@1000(VAR object2@52006518 : Record 2000000001);
    VAR
      object@52006506 : Record 2000000001;
      repositoryPath@52006504 : Text;
      databaseName@52006515 : Text;
      databaseServer@52006503 : Text;
      databaseInstance@52006502 : Text;
      filename@52006519 : Text;
      logFilename@52006505 : Text;
      finsqlPath@52006500 : Text;
      finsqlParams@52006516 : Text;
      finsqlFilters@52006517 : Text;
      finsqlPSI@52006501 : DotNet "'System, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.Diagnostics.ProcessStartInfo" RUNONCLIENT;
      finsqlProcess@52006514 : DotNet "'System, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.Diagnostics.Process" RUNONCLIENT;
    BEGIN
      object.COPY(object2);
      GetSetup(repositoryPath, databaseName, databaseServer, databaseInstance, finsqlPath);
      Dlg.OPEN(ExportingObject);
      IF object.FINDSET THEN
        REPEAT
          filename := GetObjFilenameFromObject(object);
          Dlg.UPDATE(1, filename);
          filename := STRSUBSTNO('%1\%2', repositoryPath, filename);
          logFilename := GetTempFile;
          finsqlFilters := STRSUBSTNO('Type=%1;ID=%2', FORMAT(object.Type), object.ID);
          finsqlParams := STRSUBSTNO('command=exportobjects, file="%1", servername="%2", database="%3", filter"=%4", logfile="%5", ntauthentication=yes',
                                    filename, databaseServer, databaseName, finsqlFilters, logFilename);
          finsqlPSI := finsqlPSI.ProcessStartInfo(finsqlPath, finsqlParams);
          finsqlPSI.CreateNoWindow  := TRUE;
          finsqlProcess := finsqlProcess.Start(finsqlPSI);
          finsqlProcess.WaitForExit;
          CheckLogfile(filename, logFilename);
        UNTIL object.NEXT = 0;
      Dlg.CLOSE;
    END;

    PROCEDURE ExportFiltered@52006511(VAR object@52006518 : Record 2000000001);
    VAR
      repositoryPath@52006504 : Text;
      databaseName@52006515 : Text;
      databaseServer@52006503 : Text;
      databaseInstance@52006502 : Text;
      filename@52006519 : Text;
      logFilename@52006506 : Text;
      finsqlPath@52006500 : Text;
      finsqlParams@52006516 : Text;
      finsqlFilters@52006517 : Text;
      finsqlPSI@52006501 : DotNet "'System, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.Diagnostics.ProcessStartInfo" RUNONCLIENT;
      finsqlProcess@52006514 : DotNet "'System, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.Diagnostics.Process" RUNONCLIENT;
      textFunc@52006505 : Codeunit 52006530;
      systemIOFile@52006507 : DotNet "'mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.IO.File";
    BEGIN
      GetSetup(repositoryPath, databaseName, databaseServer, databaseInstance, finsqlPath);
      filename := GetTempFile;
      logFilename := GetTempFile;
      finsqlFilters := textFunc.Replace(object.GETFILTERS, ': ', '=');
      finsqlFilters := textFunc.Replace(finsqlFilters, ', ', ';');
      finsqlParams := STRSUBSTNO('command=exportobjects, file=%1, servername=%2, database=%3, filter="%4", logfile="%5", ntauthentication=yes',
                                filename, databaseServer, databaseName, finsqlFilters, logFilename);
      Dlg.OPEN(ExportingObjects);
      finsqlPSI := finsqlPSI.ProcessStartInfo(finsqlPath, finsqlParams);
      finsqlPSI.CreateNoWindow  := TRUE;
      finsqlProcess := finsqlProcess.Start(finsqlPSI);
      finsqlProcess.WaitForExit;
      CheckLogfile(filename, logFilename);
      Dlg.CLOSE;
      SplitObjectFile(filename, repositoryPath);
      systemIOFile.Delete(filename);
    END;

    PROCEDURE ImportEach@52006510(VAR object2@52006518 : Record 2000000001);
    VAR
      object@52006506 : Record 2000000001;
      repositoryPath@52006504 : Text;
      databaseName@52006515 : Text;
      databaseServer@52006503 : Text;
      databaseInstance@52006502 : Text;
      filename@52006519 : Text;
      logFilename@52006505 : Text;
      finsqlPath@52006500 : Text;
      finsqlParams@52006516 : Text;
      finsqlPSI@52006501 : DotNet "'System, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.Diagnostics.ProcessStartInfo" RUNONCLIENT;
      finsqlProcess@52006514 : DotNet "'System, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.Diagnostics.Process" RUNONCLIENT;
    BEGIN
      object.COPY(object2);
      GetSetup(repositoryPath, databaseName, databaseServer, databaseInstance, finsqlPath);
      IF object.FINDSET THEN
        REPEAT
          filename := GetObjFilenameFromObject(object);
          filename := STRSUBSTNO('%1\%2', repositoryPath, GetObjFilenameFromObject(object));
          logFilename := GetTempFile;
          finsqlParams := STRSUBSTNO('command=importobjects, file="%1", servername="%2", database="%3", logfile="%4", ntauthentication=yes',
                                    filename, databaseServer, databaseName, logFilename);
          finsqlPSI := finsqlPSI.ProcessStartInfo(finsqlPath, finsqlParams);
          finsqlPSI.CreateNoWindow  := TRUE;
          finsqlProcess := finsqlProcess.Start(finsqlPSI);
          finsqlProcess.WaitForExit;
          CheckLogfile(filename, logFilename);
        UNTIL object.NEXT = 0;
    END;

    PROCEDURE CompileEach@52006516(VAR object2@52006518 : Record 2000000001);
    VAR
      object@52006506 : Record 2000000001;
      repositoryPath@52006504 : Text;
      databaseName@52006515 : Text;
      databaseServer@52006503 : Text;
      databaseInstance@52006502 : Text;
      logFilename@52006505 : Text;
      finsqlPath@52006500 : Text;
      finsqlParams@52006516 : Text;
      finsqlFilters@52006517 : Text;
      finsqlPSI@52006507 : DotNet "'System, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.Diagnostics.ProcessStartInfo" RUNONCLIENT;
      finsqlProcess@52006501 : DotNet "'System, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.Diagnostics.Process" RUNONCLIENT;
    BEGIN
      object.COPY(object2);
      GetSetup(repositoryPath, databaseName, databaseServer, databaseInstance, finsqlPath);
      IF object.FINDSET THEN
        REPEAT
          logFilename := GetTempFile;
          finsqlFilters := STRSUBSTNO('Type=%1;ID=%2', FORMAT(object.Type), object.ID);
          finsqlParams := STRSUBSTNO('command=compileobjects, servername="%1", database="%2", filter"=%3", logfile="%4", ntauthentication=yes',
                                     databaseServer, databaseName, finsqlFilters, logFilename);
          finsqlPSI := finsqlPSI.ProcessStartInfo(finsqlPath, finsqlParams);
          finsqlPSI.CreateNoWindow  := TRUE;
          finsqlProcess := finsqlProcess.Start(finsqlPSI);
          finsqlProcess.WaitForExit;
          CheckLogfile(STRSUBSTNO('%1 %2', object.Type, object.ID), logFilename);
        UNTIL object.NEXT = 0;
    END;

    PROCEDURE ServerExportEach@52006505(VAR object2@52006518 : Record 2000000001);
    VAR
      object@52006506 : Record 2000000001;
      repositoryPath@52006504 : Text;
      databaseName@52006515 : Text;
      databaseServer@52006503 : Text;
      databaseInstance@52006502 : Text;
      filename@52006519 : Text;
      logFilename@52006505 : Text;
      finsqlPath@52006500 : Text;
      finsqlParams@52006516 : Text;
      finsqlFilters@52006517 : Text;
      finsqlPSI@52006501 : DotNet "'System, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.Diagnostics.ProcessStartInfo";
      finsqlProcess@52006514 : DotNet "'System, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.Diagnostics.Process";
    BEGIN
      object.COPY(object2);
      GetSetup(repositoryPath, databaseName, databaseServer, databaseInstance, finsqlPath);
      IF object.FINDSET THEN
        REPEAT
          filename := GetObjFilenameFromObject(object);
          filename := STRSUBSTNO('%1\%2', repositoryPath, filename);
          logFilename := ServerGetTempFile;
          finsqlFilters := STRSUBSTNO('Type=%1;ID=%2', FORMAT(object.Type), object.ID);
          finsqlParams := STRSUBSTNO('command=exportobjects, file="%1", servername="%2", database="%3", filter"=%4", logfile="%5", ntauthentication=yes',
                                    filename, databaseServer, databaseName, finsqlFilters, logFilename);
          finsqlPSI := finsqlPSI.ProcessStartInfo(finsqlPath, finsqlParams);
          finsqlPSI.CreateNoWindow  := TRUE;
          finsqlProcess := finsqlProcess.Start(finsqlPSI);
          finsqlProcess.WaitForExit;
          CheckLogfile(filename, logFilename);
        UNTIL object.NEXT = 0;
    END;

    PROCEDURE ImportObject@52006500(object@52006518 : Record 2000000001);
    VAR
      repositoryPath@52006504 : Text;
      databaseName@52006515 : Text;
      databaseServer@52006503 : Text;
      databaseInstance@52006502 : Text;
      finsqlPath@52006500 : Text;
      filename@52006505 : Text;
      logFilename@52006506 : Text;
      finsqlParams@52006516 : Text;
      finsqlPSI@52006501 : DotNet "'System, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.Diagnostics.ProcessStartInfo" RUNONCLIENT;
      finsqlProcess@52006514 : DotNet "'System, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.Diagnostics.Process" RUNONCLIENT;
    BEGIN
      GetSetup(repositoryPath, databaseName, databaseServer, databaseInstance, finsqlPath);
      filename := STRSUBSTNO('%1\%2', repositoryPath, GetObjFilenameFromObject(object));
      ImportFile(filename);
    END;

    PROCEDURE Compile@52006542(object@52006518 : Record 2000000001);
    VAR
      repositoryPath@52006504 : Text;
      databaseName@52006515 : Text;
      databaseServer@52006503 : Text;
      databaseInstance@52006502 : Text;
      finsqlPath@52006500 : Text;
      finsqlFilters@52006507 : Text;
      filename@52006505 : Text;
      logFilename@52006506 : Text;
      finsqlParams@52006516 : Text;
      finsqlPSI@52006501 : DotNet "'System, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.Diagnostics.ProcessStartInfo" RUNONCLIENT;
      finsqlProcess@52006514 : DotNet "'System, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.Diagnostics.Process" RUNONCLIENT;
    BEGIN
      GetSetup(repositoryPath, databaseName, databaseServer, databaseInstance, finsqlPath);
      logFilename := GetTempFile;
      finsqlFilters := STRSUBSTNO('Type=%1;ID=%2', FORMAT(object.Type), object.ID);
      finsqlParams := STRSUBSTNO('command=compileobjects, servername=%1, database=%2, filter="%3", logfile="%4", ntauthentication=yes',
                                databaseServer, databaseName, finsqlFilters, logFilename);
      finsqlPSI := finsqlPSI.ProcessStartInfo(finsqlPath, finsqlParams);
      finsqlPSI.CreateNoWindow  := TRUE;
      finsqlProcess := finsqlProcess.Start(finsqlPSI);
      finsqlProcess.WaitForExit;
      CheckLogfile(STRSUBSTNO('%1 %2', object.Type, object.ID), logFilename);
    END;

    PROCEDURE ImportFile@52006514(filename@52006505 : Text);
    VAR
      repositoryPath@52006504 : Text;
      databaseName@52006515 : Text;
      databaseServer@52006503 : Text;
      databaseInstance@52006502 : Text;
      finsqlPath@52006500 : Text;
      logFilename@52006506 : Text;
      finsqlParams@52006516 : Text;
      finsqlPSI@52006501 : DotNet "'System, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.Diagnostics.ProcessStartInfo" RUNONCLIENT;
      finsqlProcess@52006514 : DotNet "'System, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.Diagnostics.Process" RUNONCLIENT;
    BEGIN
      GetSetup(repositoryPath, databaseName, databaseServer, databaseInstance, finsqlPath);
      logFilename := GetTempFile;
      finsqlParams := STRSUBSTNO('command=importobjects, file="%1", servername="%2", database="%3", logfile="%4", ntauthentication=yes',
                                filename, databaseServer, databaseName, logFilename);
      finsqlPSI := finsqlPSI.ProcessStartInfo(finsqlPath, finsqlParams);
      finsqlPSI.CreateNoWindow  := TRUE;
      finsqlProcess := finsqlProcess.Start(finsqlPSI);
      finsqlProcess.WaitForExit;
      CheckLogfile(filename, logFilename);
    END;

    PROCEDURE OpenFileFromObject@52006515(object@52006500 : Record 2000000001);
    VAR
      filename@52006501 : Text;
      psi@52006503 : DotNet "'System, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.Diagnostics.ProcessStartInfo" RUNONCLIENT;
      process@52006502 : DotNet "'System, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.Diagnostics.Process" RUNONCLIENT;
    BEGIN
      GET(USERID);
      TESTFIELD("Repository Path");
      filename := STRSUBSTNO('%1\%2', "Repository Path", GetObjFilenameFromObject(object));
      psi := psi.ProcessStartInfo(filename);
      process.Start(psi);
    END;

    PROCEDURE MarkModifiedInRepository@52006503(VAR object@52006500 : Record 2000000001);
    VAR
      repositoryPath@52006507 : Text;
      databaseName@52006506 : Text;
      databaseServer@52006505 : Text;
      databaseInstance@52006504 : Text;
      finsqlPath@52006503 : Text;
      gitPSI@52006502 : DotNet "'System, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.Diagnostics.ProcessStartInfo" RUNONCLIENT;
      gitProcess@52006501 : DotNet "'System, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.Diagnostics.Process" RUNONCLIENT;
      line@52006509 : Text;
      filename@52006510 : Text;
    BEGIN
      GetSetup(repositoryPath, databaseName, databaseServer, databaseInstance, finsqlPath);
      gitPSI := gitPSI.ProcessStartInfo('git', 'status --porcelain');
      gitPSI.WorkingDirectory := repositoryPath;
      gitPSI.CreateNoWindow  := TRUE;
      gitPSI.RedirectStandardOutput := TRUE;
      gitPSI.UseShellExecute := FALSE;
      gitProcess := gitProcess.Start(gitPSI);
      gitProcess.WaitForExit(10000);
      WHILE NOT gitProcess.StandardOutput.EndOfStream DO BEGIN
        line := gitProcess.StandardOutput.ReadLine;
        filename := COPYSTR(line, 4, STRLEN(line));
        IF GetObjectFromFilename(object, filename) THEN
          object.MARK(TRUE);
      END;
    END;

    PROCEDURE MarkAllSinceGitObject@52006508(VAR object@52006500 : Record 2000000001;objName@52006501 : Text);
    VAR
      repositoryPath@52006510 : Text;
      databaseName@52006509 : Text;
      databaseServer@52006508 : Text;
      databaseInstance@52006507 : Text;
      finsqlPath@52006506 : Text;
      gitPSI@52006505 : DotNet "'System, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.Diagnostics.ProcessStartInfo" RUNONCLIENT;
      gitProcess@52006504 : DotNet "'System, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.Diagnostics.Process" RUNONCLIENT;
      filename@52006502 : Text;
    BEGIN
      GetSetup(repositoryPath, databaseName, databaseServer, databaseInstance, finsqlPath);
      gitPSI := gitPSI.ProcessStartInfo('git', STRSUBSTNO('diff --name-only %1', objName));
      gitPSI.WorkingDirectory := repositoryPath;
      gitPSI.CreateNoWindow  := TRUE;
      gitPSI.RedirectStandardOutput := TRUE;
      gitPSI.UseShellExecute := FALSE;
      gitProcess := gitProcess.Start(gitPSI);
      gitProcess.WaitForExit(10000);
      WHILE NOT gitProcess.StandardOutput.EndOfStream DO BEGIN
        filename := gitProcess.StandardOutput.ReadLine;
        IF GetObjectFromFilename(object, filename) THEN
          object.MARK(TRUE);
      END;
    END;

    PROCEDURE ImportAllSinceGitObject@52006513(objName@52006501 : Text);
    VAR
      repositoryPath@52006510 : Text;
      databaseName@52006509 : Text;
      databaseServer@52006508 : Text;
      databaseInstance@52006507 : Text;
      finsqlPath@52006506 : Text;
      gitPSI@52006505 : DotNet "'System, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.Diagnostics.ProcessStartInfo" RUNONCLIENT;
      gitProcess@52006504 : DotNet "'System, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.Diagnostics.Process" RUNONCLIENT;
      file@52006511 : DotNet "'mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.IO.File" RUNONCLIENT;
      filename@52006502 : Text;
      nsbFileList@52006500 : Page 73002;
      tmpObj@52006503 : TEMPORARY Record 2000000001;
    BEGIN
      GetSetup(repositoryPath, databaseName, databaseServer, databaseInstance, finsqlPath);
      gitPSI := gitPSI.ProcessStartInfo('git', STRSUBSTNO('diff --name-only %1', objName));
      gitPSI.WorkingDirectory := repositoryPath;
      gitPSI.CreateNoWindow  := TRUE;
      gitPSI.RedirectStandardOutput := TRUE;
      gitPSI.UseShellExecute := FALSE;
      gitProcess := gitProcess.Start(gitPSI);
      gitProcess.WaitForExit(10000);
      WHILE NOT gitProcess.StandardOutput.EndOfStream DO BEGIN
        filename := gitProcess.StandardOutput.ReadLine;
        IF STRPOS(filename, '.TXT') <> 0 THEN
          nsbFileList.AddFile(filename);
      END;
      nsbFileList.LOOKUPMODE(TRUE);
      nsbFileList.CAPTION(SelectObjectsToImport);
      IF nsbFileList.RUNMODAL <> ACTION::LookupOK THEN
        EXIT;
      nsbFileList.SetObjects(tmpObj);
      IF tmpObj.FINDSET THEN
        REPEAT
          filename := STRSUBSTNO('%1\%2', repositoryPath, GetObjFilenameFromObject(tmpObj));
          IF file.Exists(filename) THEN
            ImportFile(filename);
        UNTIL tmpObj.NEXT = 0;
    END;

    PROCEDURE GetGitBranchName@52006506() : Text;
    VAR
      repositoryPath@52006510 : Text;
      databaseName@52006509 : Text;
      databaseServer@52006508 : Text;
      databaseInstance@52006507 : Text;
      finsqlPath@52006506 : Text;
      gitPSI@52006505 : DotNet "'System, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.Diagnostics.ProcessStartInfo" RUNONCLIENT;
      gitProcess@52006504 : DotNet "'System, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.Diagnostics.Process" RUNONCLIENT;
    BEGIN
      GetSetup(repositoryPath, databaseName, databaseServer, databaseInstance, finsqlPath);
      gitPSI := gitPSI.ProcessStartInfo('git', 'rev-parse --abbrev-ref HEAD');
      gitPSI.WorkingDirectory := repositoryPath;
      gitPSI.CreateNoWindow  := TRUE;
      gitPSI.RedirectStandardOutput := TRUE;
      gitPSI.UseShellExecute := FALSE;
      gitProcess := gitProcess.Start(gitPSI);
      gitProcess.WaitForExit(10000);
      WHILE NOT gitProcess.StandardOutput.EndOfStream DO BEGIN
        EXIT(gitProcess.StandardOutput.ReadLine);
      END;
    END;

    PROCEDURE ServerGetGitBranchName@52006507() : Text;
    VAR
      repositoryPath@52006510 : Text;
      databaseName@52006509 : Text;
      databaseServer@52006508 : Text;
      databaseInstance@52006507 : Text;
      finsqlPath@52006506 : Text;
      gitPSI@52006505 : DotNet "'System, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.Diagnostics.ProcessStartInfo";
      gitProcess@52006504 : DotNet "'System, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.Diagnostics.Process";
    BEGIN
      GetSetup(repositoryPath, databaseName, databaseServer, databaseInstance, finsqlPath);
      gitPSI := gitPSI.ProcessStartInfo('git', 'rev-parse --abbrev-ref HEAD');
      gitPSI.WorkingDirectory := repositoryPath;
      gitPSI.CreateNoWindow  := TRUE;
      gitPSI.RedirectStandardOutput := TRUE;
      gitPSI.UseShellExecute := FALSE;
      gitProcess := gitProcess.Start(gitPSI);
      gitProcess.WaitForExit(10000);
      WHILE NOT gitProcess.StandardOutput.EndOfStream DO BEGIN
        EXIT(gitProcess.StandardOutput.ReadLine);
      END;
    END;

    PROCEDURE GetObjFilenameFromObject@52006502(object@52006500 : Record 2000000001) : Text;
    BEGIN
      EXIT(STRSUBSTNO('%1%2.TXT', COPYSTR(UPPERCASE(FORMAT(object.Type)), 1, 3), object.ID));
    END;

    PROCEDURE GetObjFilenameFromTextHeader@52006591(textHeader@52006500 : Text) : Text;
    VAR
      type@52006501 : Text;
      i@52006502 : Integer;
    BEGIN
      textHeader := COPYSTR(textHeader, 8, STRLEN(textHeader));
      type := UPPERCASE(COPYSTR(textHeader, 1, 3));
      i := STRPOS(textHeader, ' ');
      textHeader := COPYSTR(textHeader, i + 1, STRLEN(textHeader));
      i := STRPOS(textHeader, ' ');
      EXIT(STRSUBSTNO('%1%2.TXT', type, COPYSTR(textHeader, 1, i - 1)));
    END;

    PROCEDURE GetObjectFromFilename@52006512(VAR object@52006500 : Record 2000000001;filename@52006501 : Text) : Boolean;
    VAR
      type@52006502 : Text;
      id@52006503 : Text;
      textFunc@52006504 : Codeunit 52006530;
    BEGIN
      type := COPYSTR(filename, 1, 3);
      id := textFunc.RemoveAllButNumeric(filename);
      IF (type = '') OR (id = '') THEN
        EXIT;
      object.Type := GetObjectTypeFromTypeText(type);
      EVALUATE(object.ID, id);
      EXIT(object.GET(object.Type, '', object.ID));
    END;

    PROCEDURE GetObjectTypeFromTypeText@52006555(typeText@52006501 : Text) : Integer;
    VAR
      object@52006500 : Record 2000000001;
    BEGIN
      CASE typeText OF
        'TAB': EXIT(object.Type::Table);
        'COD': EXIT(object.Type::Codeunit);
        'PAG': EXIT(object.Type::Page);
        'REP': EXIT(object.Type::Report);
        'XML': EXIT(object.Type::XMLport);
        'MEN': EXIT(object.Type::MenuSuite);
        'QUE': EXIT(object.Type::Query);
      ELSE
        ERROR(InvalidObjectType, typeText);
      END;
    END;

    PROCEDURE GetTempFile@52006529() : Text;
    VAR
      systemIOPath@52006500 : DotNet "'mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.IO.Path" RUNONCLIENT;
    BEGIN
      EXIT(STRSUBSTNO('%1%2.TXT', systemIOPath.GetTempPath, CREATEGUID));
    END;

    PROCEDURE ServerGetTempFile@52006509() : Text;
    VAR
      systemIOPath@52006500 : DotNet "'mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.IO.Path";
    BEGIN
      EXIT(STRSUBSTNO('%1%2.TXT', systemIOPath.GetTempPath, CREATEGUID));
    END;

    PROCEDURE SplitObjectFile@52006547(filename@52006500 : Text;destination@52006508 : Text);
    VAR
      inputFile@52006502 : File;
      outputFile@52006505 : File;
      inStream@52006501 : InStream;
      outStream@52006506 : OutStream;
      line@52006504 : Text;
      outFilename@52006507 : Text;
      objInProgress@52006503 : Boolean;
    BEGIN
      Dlg.OPEN(SplittingObject);
      inputFile.OPEN(filename);
      inputFile.CREATEINSTREAM(inStream);
      WHILE NOT inStream.EOS DO BEGIN
        inStream.READTEXT(line);
        IF NOT objInProgress THEN BEGIN
          IF COPYSTR(line, 1, 7) = 'OBJECT ' THEN BEGIN
            objInProgress := TRUE;
            outFilename := GetObjFilenameFromTextHeader(line);
            Dlg.UPDATE(1, outFilename);
            outFilename := STRSUBSTNO('%1\%2', destination, outFilename);
            outputFile.CREATE(outFilename);
            outputFile.TEXTMODE(TRUE);
            outputFile.CREATEOUTSTREAM(outStream);
            outStream.WRITETEXT(line);
            outStream.WRITETEXT;
          END;
        END ELSE BEGIN
          outStream.WRITETEXT(line);
          outStream.WRITETEXT;
          IF COPYSTR(line, 1, STRLEN(line)) = '}' THEN BEGIN
            objInProgress := FALSE;
            outStream.WRITETEXT;
            outputFile.CLOSE;
          END;
        END;
      END;
      inputFile.CLOSE;
      Dlg.CLOSE;
    END;

    PROCEDURE CheckLogfile@52006622(objFilename@52006504 : Text;logFilename@52006500 : Text);
    VAR
      textFunc@52006503 : Codeunit 52006530;
      systemIOFile@52006502 : DotNet "'mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.IO.File" RUNONCLIENT;
      systemIOPath@52006505 : DotNet "'mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.IO.Path" RUNONCLIENT;
      log@52006501 : Text;
    BEGIN
      IF NOT systemIOFile.Exists(logFilename) THEN
        EXIT;

      log := systemIOFile.ReadAllText(logFilename);
      systemIOFile.Delete(logFilename);
      IF objFilename <> '' THEN
        objFilename := systemIOPath.GetFileName(objFilename);
      log := STRSUBSTNO('%1\\%2', objFilename, log);
      ERROR(log);
    END;

    BEGIN
    END.
  }
}

