OBJECT Table 35000073 DIRF Lines2
{
  OBJECT-PROPERTIES
  {
    Date=13/05/16;
    Time=09:13:38;
    Modified=Yes;
    Version List=NAVBR5.01.0109,DIRF11;
  }
  PROPERTIES
  {
    PasteIsValid=Yes;
  }
  FIELDS
  {
    { 1   ;   ;DIRF Header Code    ;Code10        ;CaptionML=[ENU=DIRF Header Code;
                                                              PTB=C�digo Cab. DIRF];
                                                   Editable=No }
    { 2   ;   ;Beneficiary No.     ;Code20        ;TableRelation=Vendor;
                                                   CaptionML=[ENU=Beneficiary No.;
                                                              PTB=No. Benefici�rio];
                                                   Description=CADIA C�digo }
    { 3   ;   ;Retention Code      ;Code4         ;CaptionML=[ENU=Retention Code;
                                                              PTB=C�d. de Reten��o] }
    { 4   ;   ;Beneficiary Category;Option        ;CaptionML=[ENU=Beneficiary Category;
                                                              PTB=Categor�a Benefici�rio];
                                                   OptionCaptionML=[ENU=" ,1.- Person,2.- Company,3.- Foreing";
                                                                    PTB=" ,1.- Pessoa F�sica,2.- Pessoa Jur�dica,3.- Estrangeiro,5.- Estrangeiro Pessoa Fisica"];
                                                   OptionString=[ ,1.- Person,2.- Company,3.- Foreing,5.- Foreing Person] }
    { 5   ;   ;Beneficiary CPF/CNPJ;Code14        ;CaptionML=[ENU=Beneficiary CPF/CNPJ;
                                                              PTB=CPF/CNPJ Benefici�rio] }
    { 6   ;   ;Beneficiary Name    ;Text60        ;CaptionML=[ENU=Beneficiary Name;
                                                              PTB=Nome Benefici�rio] }
    { 7   ;   ;Taxable Incomes (January);Decimal  ;CaptionML=[ENU=Taxable Incomes (January);
                                                              PTB=Rend. Tribut�veis (Janeiro)] }
    { 8   ;   ;Deductions (January);Decimal       ;CaptionML=[ENU=Deductions (January);
                                                              PTB=Dedu�oes (Janeiro)] }
    { 9   ;   ;Restrained Tax (January);Decimal   ;CaptionML=[ENU=Restrained Tax (January);
                                                              PTB=Imposto Retido (Janeiro)] }
    { 10  ;   ;Taxable Incomes (February);Decimal ;CaptionML=[ENU=Taxable Incomes (February);
                                                              PTB=Rend. Tribut�veis (Fevereiro)] }
    { 11  ;   ;Deductions (February);Decimal      ;CaptionML=[ENU=Deductions (February);
                                                              PTB=Dedu�oes (Fevereiro)] }
    { 12  ;   ;Restrained Tax (February);Decimal  ;CaptionML=[ENU=Restrained Tax (February);
                                                              PTB=Imposto Retido (Fevereiro)] }
    { 13  ;   ;Taxable Incomes (March);Decimal    ;CaptionML=[ENU=Taxable Incomes (March);
                                                              PTB=Rend. Tribut�veis (Mar�o)] }
    { 14  ;   ;Deductions (March)  ;Decimal       ;CaptionML=[ENU=Deductions (March);
                                                              PTB=Dedu�oes (Mar�o)] }
    { 15  ;   ;Restrained Tax (March);Decimal     ;CaptionML=[ENU=Restrained Tax (March);
                                                              PTB=Imposto Retido (Mar�o)] }
    { 16  ;   ;Taxable Incomes (April);Decimal    ;CaptionML=[ENU=Taxable Incomes (April);
                                                              PTB=Rend. Tribut�veis (Abril)] }
    { 17  ;   ;Deductions (April)  ;Decimal       ;CaptionML=[ENU=Deductions (April);
                                                              PTB=Dedu�oes (Abril)] }
    { 18  ;   ;Restrained Tax (April);Decimal     ;CaptionML=[ENU=Restrained Tax (April);
                                                              PTB=Imposto Retido (Abril)] }
    { 19  ;   ;Taxable Incomes (May);Decimal      ;CaptionML=[ENU=Taxable Incomes (May);
                                                              PTB=Rend. Tribut�veis (Maio)] }
    { 20  ;   ;Deductions (May)    ;Decimal       ;CaptionML=[ENU=Deductions (May);
                                                              PTB=Dedu�oes (Maio)] }
    { 21  ;   ;Restrained Tax (May);Decimal       ;CaptionML=[ENU=Restrained Tax (May);
                                                              PTB=Imposto Retido (Maio)] }
    { 22  ;   ;Taxable Incomes (June);Decimal     ;CaptionML=[ENU=Taxable Incomes (June);
                                                              PTB=Rend. Tribut�veis (Junho)] }
    { 23  ;   ;Deductions (June)   ;Decimal       ;CaptionML=[ENU=Deductions (June);
                                                              PTB=Dedu�oes (Junho)] }
    { 24  ;   ;Restrained Tax (June);Decimal      ;CaptionML=[ENU=Restrained Tax (June);
                                                              PTB=Imposto Retido (Junho)] }
    { 25  ;   ;Taxable Incomes (July);Decimal     ;CaptionML=[ENU=Taxable Incomes (July);
                                                              PTB=Rend. Tribut�veis (Julho)] }
    { 26  ;   ;Deductions (July)   ;Decimal       ;CaptionML=[ENU=Deductions (July);
                                                              PTB=Dedu�oes (Julho)] }
    { 27  ;   ;Restrained Tax (July);Decimal      ;CaptionML=[ENU=Restrained Tax (July);
                                                              PTB=Imposto Retido (Julho)] }
    { 28  ;   ;Taxable Incomes (August);Decimal   ;CaptionML=[ENU=Taxable Incomes (August);
                                                              PTB=Rend. Tribut�veis (Agosto)] }
    { 29  ;   ;Deductions (August) ;Decimal       ;CaptionML=[ENU=Deductions (August);
                                                              PTB=Dedu�oes (Agosto)] }
    { 30  ;   ;Restrained Tax (August);Decimal    ;CaptionML=[ENU=Restrained Tax (August);
                                                              PTB=Imposto Retido (Agosto)] }
    { 31  ;   ;Taxable Incomes (September);Decimal;CaptionML=[ENU=Taxable Incomes (September);
                                                              PTB=Rend. Tribut�veis (Setembro)] }
    { 32  ;   ;Deductions (September);Decimal     ;CaptionML=[ENU=Deductions (September);
                                                              PTB=Dedu�oes (Setembro)] }
    { 33  ;   ;Restrained Tax (September);Decimal ;CaptionML=[ENU=Restrained Tax (September);
                                                              PTB=Imposto Retido (Setembro)] }
    { 34  ;   ;Taxable Incomes (October);Decimal  ;CaptionML=[ENU=Taxable Incomes (October);
                                                              PTB=Rend. Tribut�veis (Outubro)] }
    { 35  ;   ;Deductions (October);Decimal       ;CaptionML=[ENU=Deductions (October);
                                                              PTB=Dedu�oes (Outubro)] }
    { 36  ;   ;Restrained Tax (October);Decimal   ;CaptionML=[ENU=Restrained Tax (October);
                                                              PTB=Imposto Retido (Outubro)] }
    { 37  ;   ;Taxable Incomes (November);Decimal ;CaptionML=[ENU=Taxable Incomes (November);
                                                              PTB=Rend. Tribut�veis (Novembro)] }
    { 38  ;   ;Deductions (November);Decimal      ;CaptionML=[ENU=Deductions (November);
                                                              PTB=Dedu�oes (Novembro)] }
    { 39  ;   ;Restrained Tax (November);Decimal  ;CaptionML=[ENU=Restrained Tax (November);
                                                              PTB=Imposto Retido (Novembro)] }
    { 40  ;   ;Taxable Incomes (December);Decimal ;CaptionML=[ENU=Taxable Incomes (December);
                                                              PTB=Rend. Tribut�veis (Dezembro)] }
    { 41  ;   ;Deductions (December);Decimal      ;CaptionML=[ENU=Deductions (December);
                                                              PTB=Dedu�oes (Dezembro)] }
    { 42  ;   ;Restrained Tax (December);Decimal  ;CaptionML=[ENU=Restrained Tax (December);
                                                              PTB=Imposto Retido (Dezembro)] }
    { 43  ;   ;Taxable Incomes (13rd Wage);Decimal;CaptionML=[ENU=Taxable Incomes (13rd Wage);
                                                              PTB=Rend. Tribut�veis (13 sal�rio)] }
    { 44  ;   ;Deductions (13rd Wage);Decimal     ;CaptionML=[ENU=Deductions (13rd Wage);
                                                              PTB=Dedu�oes (13 sal�rio)] }
    { 45  ;   ;Restrained Tax (13rd Wage);Decimal ;CaptionML=[ENU=Restrained Tax (13rd Wage);
                                                              PTB=Imposto Retido (13 sal�rio)] }
    { 46  ;   ;Type of Income/Tax  ;Integer       ;InitValue=0;
                                                   CaptionML=[ENU=Type of Income/Tax;
                                                              PTB=Tipo de Rendimento/Imposto] }
    { 47  ;   ;For Declarant Use   ;Text33        ;CaptionML=[ENU=For Declarant Use;
                                                              PTB=Para Uso do Declarante] }
    { 48  ;   ;Observation         ;Text250       ;CaptionML=[ENU=Observation;
                                                              PTB=Observa��es] }
    { 49  ;   ;Non-Taxable Incomes (January);Decimal;
                                                   CaptionML=PTB=Rend. N�o Tribut�veis (Janeiro) }
    { 50  ;   ;Non-Taxable Incomes (February);Decimal;
                                                   CaptionML=PTB=Rend. N�o Tribut�veis (Fevereiro) }
    { 51  ;   ;Non-Taxable Incomes (March);Decimal;CaptionML=PTB=Rend. N�o Tribut�veis (Mar�o) }
    { 52  ;   ;Non-Taxable Incomes (April);Decimal;CaptionML=PTB=Rend. N�o Tribut�veis (Abril) }
    { 53  ;   ;Non-Taxable Incomes (May);Decimal  ;CaptionML=PTB=Rend. N�o Tribut�veis (Maio) }
    { 54  ;   ;Non-Taxable Incomes (June);Decimal ;CaptionML=PTB=Rend. N�o Tribut�veis (Junho) }
    { 55  ;   ;Non-Taxable Incomes (July);Decimal ;CaptionML=PTB=Rend. N�o Tribut�veis (Julho) }
    { 56  ;   ;Non-Taxable Incomes (August);Decimal;
                                                   CaptionML=PTB=Rend. N�o Tribut�veis (Agosto) }
    { 57  ;   ;Non-Taxable Incomes(September);Decimal;
                                                   CaptionML=PTB=Rend. N�o Tribut�veis (Setembro) }
    { 58  ;   ;Non-Taxable Incomes (October);Decimal;
                                                   CaptionML=PTB=Rend. N�o Tribut�veis (Outubro) }
    { 59  ;   ;Non-Taxable Incomes (November);Decimal;
                                                   CaptionML=PTB=Rend. N�o Tribut�veis (Novembro) }
    { 60  ;   ;Non-Taxable Incomes (December);Decimal;
                                                   CaptionML=PTB=Rend. N�o Tribut�veis (Dezembro) }
    { 61  ;   ;Date of Payment     ;Date          ;CaptionML=PTB=Data do Pagamento }
    { 62  ;   ;Previdency (January);Decimal       ;CaptionML=PTB=Previdencia (Janeiro) }
    { 63  ;   ;Previdency (February);Decimal      ;CaptionML=PTB=Previdencia (Fevereiro) }
    { 64  ;   ;Previdency (March)  ;Decimal       ;CaptionML=PTB=Previdencia (Mar�o) }
    { 65  ;   ;Previdency (April)  ;Decimal       ;CaptionML=PTB=Previdencia (Abril) }
    { 66  ;   ;Previdency (May)    ;Decimal       ;CaptionML=PTB=Previdencia (Maio) }
    { 67  ;   ;Previdency (June)   ;Decimal       ;CaptionML=PTB=Previdencia (Junho) }
    { 68  ;   ;Previdency (July)   ;Decimal       ;CaptionML=PTB=Previdencia (Julho) }
    { 69  ;   ;Previdency (August) ;Decimal       ;CaptionML=PTB=Previdencia (Agosto) }
    { 70  ;   ;Previdency (September);Decimal     ;CaptionML=PTB=Previdencia (Setembro) }
    { 71  ;   ;Previdency (October);Decimal       ;CaptionML=PTB=Previdencia (Outubro) }
    { 72  ;   ;Previdency (November);Decimal      ;CaptionML=PTB=Previdencia (Novembro) }
    { 73  ;   ;Previdency (December);Decimal      ;CaptionML=PTB=Previdencia (Dezembro) }
    { 74  ;   ;Previdency (13rd)   ;Decimal       ;CaptionML=PTB=Previdencia (13 sal�rio) }
  }
  KEYS
  {
    {    ;DIRF Header Code,Beneficiary No.,Retention Code;
                                                   Clustered=Yes }
    {    ;Retention Code,Beneficiary Category,Beneficiary CPF/CNPJ;
                                                   SumIndexFields=Taxable Incomes (January),Taxable Incomes (February),Taxable Incomes (March),Taxable Incomes (April),Taxable Incomes (May),Taxable Incomes (June),Taxable Incomes (July),Taxable Incomes (August),Taxable Incomes (September),Taxable Incomes (October),Taxable Incomes (November),Taxable Incomes (December),Taxable Incomes (13rd Wage) }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      Vendor@1102300000 : Record 23;

    PROCEDURE FillWith@1102300001(Value@1000000000 : Text[250];Length@1000000001 : Integer;Caracteres@1102300000 : Boolean;Rigth@1102300001 : Boolean;Zeros@1102300002 : Boolean) : Text[250];
    VAR
      Aux@1000000002 : Text[1];
      I@1000000003 : Integer;
      Text@1000000004 : Text[250];
      Teste@1000000005 : Integer;
      ExtitValue_Aux@1102300003 : Text[1];
    BEGIN
      IF Value='' THEN
        Value:=' ';

      IF Zeros THEN
        ExtitValue_Aux := '0'
      ELSE
        ExtitValue_Aux := ' ';


      Value := COPYSTR(Value,1,STRLEN(Value));
      I := 0;

      REPEAT
        I := I + 1;
        Aux := COPYSTR(Value,I,1);

        CASE Caracteres OF
          TRUE:
            BEGIN
              Text := Text + Aux;
            END;
          FALSE:
            BEGIN
              IF NOT (Aux IN ['.',',','-','(',')','/','/','_','[',']','{','}','"','?','!','@','#','$','%','&','*',' ']) THEN
                Text := Text + Aux;
            END;
        END;
      UNTIL I=STRLEN(Value);

      Text := COPYSTR(Text,1,Length);

      IF Text = ' ' THEN
        Text := '';


      IF Rigth THEN
        Value := Text + PADSTR('',Length-STRLEN(Text),ExtitValue_Aux)
      ELSE
        Value := PADSTR('',Length-STRLEN(Text),ExtitValue_Aux) + Text;

      EXIT(Value);
    END;

    BEGIN
    END.
  }
}

