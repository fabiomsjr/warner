OBJECT Page 52112855 ECD Settlement
{
  OBJECT-PROPERTIES
  {
    Date=17/05/16;
    Time=09:37:15;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=ECD Settlement;
               PTB=Apura��o SPED Cont�bil];
    SourceTable=Table52112843;
    PageType=Card;
    ActionList=ACTIONS
    {
      { 1000000003;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 1000000000;1 ;Action    ;
                      CaptionML=[ENU=&Process;
                                 PTB=&Processar];
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=ExecuteBatch;
                      PromotedCategory=Process;
                      OnAction=VAR
                                 movSPEDContHeader@52006500 : Record 52112843;
                                 report@52006501 : Report 52112833;
                               BEGIN
                                 movSPEDContHeader.SETRANGE("No.","No.");
                                 report.SETTABLEVIEW(movSPEDContHeader);
                                 report.USEREQUESTPAGE := FALSE;
                                 report.RUNMODAL;
                               END;
                                }
      { 1000000001;1 ;Action    ;
                      Name=Export;
                      CaptionML=[ENU=&Export;
                                 PTB=&Exportar];
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=ExportFile;
                      PromotedCategory=Process;
                      OnAction=VAR
                                 fiscalFileExport@52006500 : Codeunit 52112524;
                               BEGIN
                                 fiscalFileExport.SetECDFileType("No.");
                                 fiscalFileExport.RUN;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1102300000;1;Group  ;
                CaptionML=[ENU=General;
                           PTB=Geral] }

    { 1102300001;2;Field  ;
                SourceExpr="No.";
                OnAssistEdit=BEGIN
                               IF AssistEdit(xRec) THEN
                                 CurrPage.UPDATE;
                             END;
                              }

    { 1102300008;2;Field  ;
                SourceExpr="Ident. Type Bookkeeping" }

    { 1102300013;2;Field  ;
                SourceExpr="Nature Book - Purpose" }

    { 1102300012;2;Field  ;
                SourceExpr="Instrument Order No" }

    { 1102300003;2;Field  ;
                SourceExpr="Begin Date" }

    { 1102300005;2;Field  ;
                SourceExpr="End Date" }

    { 35000000;2;Field    ;
                SourceExpr="Include End Releases" }

    { 52006501;2;Field    ;
                SourceExpr="ECD Closing By" }

    { 1000000002;2;Field  ;
                SourceExpr="Group Documents By" }

    { 52006500;2;Field    ;
                SourceExpr="Special Status Indicator" }

    { 52006502;2;Group    ;
                CaptionML=[ENU=v2.0;
                           PTB=v2.0];
                GroupType=Group }

    { 52006503;3;Field    ;
                SourceExpr=IND_SIT_INI_PER }

    { 52006504;3;Field    ;
                SourceExpr=IND_FIN_ESC }

    { 52006505;3;Field    ;
                SourceExpr=COD_HASH_SUB }

    { 52006506;3;Field    ;
                SourceExpr=NIRE_SUBST }

    { 52006507;3;Field    ;
                SourceExpr=IND_EMP_GRD_PRT }

    { 52006508;3;Field    ;
                SourceExpr=DT_EX_SOCIAL }

    { 52006509;3;Field    ;
                SourceExpr=NOME_AUDITOR }

    { 52006510;3;Field    ;
                SourceExpr=COD_CVM_AUDITOR }

    { 52006511;2;Group    ;
                CaptionML=[ENU=Functional Currency;
                           PTB=Moeda Funcional];
                GroupType=Group }

    { 52006512;3;Field    ;
                SourceExpr=IDENT_MF }

  }
  CODE
  {

    BEGIN
    END.
  }
}

