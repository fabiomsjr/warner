OBJECT Table 52113314 Code Amount Buffer
{
  OBJECT-PROPERTIES
  {
    Date=28/05/15;
    Time=09:38:03;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
  }
  FIELDS
  {
    { 11  ;   ;Code 1              ;Text30         }
    { 12  ;   ;Code 2              ;Text30         }
    { 13  ;   ;Code 3              ;Text30         }
    { 14  ;   ;Code 4              ;Text30         }
    { 21  ;   ;Amount 1            ;Decimal        }
    { 22  ;   ;Amount 2            ;Decimal        }
    { 23  ;   ;Amount 3            ;Decimal        }
    { 24  ;   ;Amount 4            ;Decimal        }
    { 31  ;   ;Boolean 1           ;Boolean        }
    { 32  ;   ;Boolean 2           ;Boolean        }
    { 33  ;   ;Boolean 3           ;Boolean        }
    { 34  ;   ;Boolean 4           ;Boolean        }
    { 41  ;   ;Text 1              ;Text250        }
    { 42  ;   ;Text 2              ;Text250        }
    { 43  ;   ;Text 3              ;Text250        }
    { 44  ;   ;Text 4              ;Text250        }
  }
  KEYS
  {
    {    ;Code 1,Code 2,Code 3,Code 4             ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

