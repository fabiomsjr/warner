OBJECT Codeunit 52113131 Purchase Line Hook
{
  OBJECT-PROPERTIES
  {
    Date=15/12/15;
    Time=15:59:56;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    OnRun=BEGIN
          END;

  }
  CODE
  {
    VAR
      PurchaseLineCHook@52006500 : Codeunit 52113132;
      ApprovalDimChangeErr@52006501 : TextConst 'ENU=Dimensions involved in the approval process can not be modified.;PTB=Dimens�es envolvidas no processo de aprova��o n�o podem ser modificadas.';

    PROCEDURE OnInsert@52006501(VAR purchLine@52006500 : Record 39);
    BEGIN
      PurchaseLineCHook.OnInsert(purchLine);
    END;

    PROCEDURE OnModify@52006503(VAR purchLine@52006500 : Record 39;VAR xPurchLine@52006501 : Record 39);
    BEGIN
      PurchaseLineCHook.OnModify(purchLine, xPurchLine);
    END;

    PROCEDURE OnDelete@52006502(VAR purchLine@52006500 : Record 39);
    VAR
      purchMgt@52006501 : Codeunit 52112431;
    BEGIN
      purchMgt.CheckApprovedAmtChangeOnDelete(purchLine);
      DeleteRelatedRecords(purchLine);

      PurchaseLineCHook.OnDelete(purchLine);
    END;

    PROCEDURE OnValidateType@52006500(purchHeader@52006502 : Record 38;VAR purchLine@52006500 : Record 39;VAR xPurchLine@52006501 : Record 39);
    BEGIN
      WITH purchLine DO BEGIN
        IF (Type = Type::Item) OR (Type = Type::"Fixed Asset") THEN
          "Allow Item Charge Assignment" := TRUE
        ELSE
          "Allow Item Charge Assignment" := FALSE;
      END;

      PurchaseLineCHook.OnValidateType(purchHeader, purchLine, xPurchLine);
    END;

    PROCEDURE OnValidateNo@52006504(purchHeader@52006502 : Record 38;VAR purchLine@52006500 : Record 39;VAR xPurchLine@52006501 : Record 39);
    VAR
      item@52006505 : Record 27;
      taxesMatrix@52006503 : Record 52112481;
      operationType@52006504 : Record 52112435;
      fiscalCodesMgt@52006506 : Codeunit 52112483;
      purchOrderLineToInv@52006507 : Record 52112466;
    BEGIN
      WITH purchLine DO BEGIN
        CASE Type OF
          Type::Item:
            BEGIN
              item.GET("No.");
              "NCM Code" := item."NCM Code";
              "Origin Code" := item."Origin Code";
              "NCM Exception Code" := item."NCM Exception Code";
              "Tax Exception Code" := item."Tax Exception Code";
            END;
          Type::"Fixed Asset": BEGIN
            "Allow Item Charge Assignment" := TRUE
          END;
        END;

        "Operation Type" := purchHeader."Operation Type";
        "End User" := purchHeader."End User";
        "CFOP Code" := purchHeader."CFOP Code";
        "Vendor Situation" := purchHeader."Vendor Situation";
        "Branch Code" := purchHeader."Branch Code";
        "DI Posting Date" := purchHeader."DI Posting Date";
        "DI No." := purchHeader."DI No.";

        IF operationType.GET("Operation Type") THEN BEGIN
          IF operationType."Gen. Prod. Posting Group" <> '' THEN
            "Gen. Prod. Posting Group" := operationType."Gen. Prod. Posting Group";
        END;

        "VAT Calculation Type" := "VAT Calculation Type"::"Sales Tax";
        "Prepmt. VAT Calc. Type" := "Prepmt. VAT Calc. Type"::"Sales Tax";

        fiscalCodesMgt.ApplyPurchLineTaxesMatrix(purchHeader, purchLine);

        IF "Document Type" = "Document Type"::Invoice THEN BEGIN
          purchOrderLineToInv.FilterByPurchLine(purchLine);
          purchOrderLineToInv.DELETEALL(TRUE);
        END;
      END;

      PurchaseLineCHook.OnValidateNo(purchHeader, purchLine, xPurchLine);
    END;

    PROCEDURE OnValidateOutstandingAmount@52006508(VAR PurchLine@52006500 : Record 39;VAR xPurchLine@52006501 : Record 39);
    VAR
      OperationType@52006505 : Record 52112435;
    BEGIN
      OperationType.SetColumnValuesFor(PurchLine."Operation Type");
      IF OperationType."Customer/Vendor Entry" <> OperationType."Customer/Vendor Entry"::"Amount Including Taxes" THEN
        PurchLine."Outstanding Amount" := 0;
    END;

    PROCEDURE OnUpdateAmounts@52006505(purchHeader@52006502 : Record 38;VAR purchLine@52006501 : Record 39;VAR xPurchLine@52006500 : Record 39);
    VAR
      purchMgt@52006503 : Codeunit 52112431;
      fiscalCodesMgt@52006504 : Codeunit 52112483;
    BEGIN
      WITH purchLine DO BEGIN
        purchMgt.CheckApprovedAmtChange(purchLine, xPurchLine);
        purchMgt.CheckForPostedPrepaymentInvoice(purchHeader);
        fiscalCodesMgt.UpdatePurchLineCST(purchHeader, purchLine);
      END;

      PurchaseLineCHook.OnUpdateAmounts(purchHeader, purchLine, xPurchLine);
    END;

    PROCEDURE OnDimensionSetUpdate@52006507(PurchLine@52006500 : Record 39;xPurchLine@52006501 : Record 39);
    VAR
      PurchHeader@52006505 : Record 38;
      DimMgt@52006502 : Codeunit 408;
      ApprovMgt@52006504 : Codeunit 439;
      ApprovFlowMgt@52006503 : Codeunit 52113073;
      DocApprovDimMgt@52006506 : Codeunit 52113080;
    BEGIN
      WITH PurchLine DO BEGIN
        PurchHeader.GET("Document Type", "Document No.");
        DimMgt.CheckDocLineBranchCode(PurchHeader."Dimension Set ID", "Dimension Set ID");
        IF Status <> Status::Open THEN BEGIN
          IF NOT ApprovMgt.CheckDocDimChange(DATABASE::"Purchase Line", "Document Type", "Dimension Set ID", xPurchLine."Dimension Set ID") THEN
            ERROR(ApprovalDimChangeErr);
          IF NOT DocApprovDimMgt.CheckPurchDimUpdate("Document Type", "Dimension Set ID", xPurchLine."Dimension Set ID") THEN
            ERROR(ApprovalDimChangeErr);
        END;
      END;

      PurchaseLineCHook.OnDimensionSetUpdate(PurchLine, xPurchLine);
    END;

    LOCAL PROCEDURE _@52006506();
    BEGIN
    END;

    PROCEDURE DeleteRelatedRecords@35000050(VAR purchLine@52006504 : Record 39);
    VAR
      modTaxAmtLine@35000000 : Record 52112450;
      criticalAnalysisMgt@52006500 : Codeunit 52112773;
      purchOrderLineToInv@52006501 : Record 52112466;
    BEGIN
      WITH modTaxAmtLine DO BEGIN
        SETRANGE("Table ID", DATABASE::"Purchase Line");
        SETRANGE("Document Type", purchLine."Document Type");
        SETRANGE("Document No.", purchLine."Document No.");
        SETRANGE("Document Line No.", purchLine."Line No.");
        DELETEALL;
      END;
      IF purchLine."Document Type" = purchLine."Document Type"::Invoice THEN BEGIN
        purchOrderLineToInv.FilterByPurchLine(purchLine);
        purchOrderLineToInv.DELETEALL(TRUE);
      END;

      criticalAnalysisMgt.OnDeleteItemCriticalAnalysis(purchLine);
    END;

    BEGIN
    END.
  }
}

