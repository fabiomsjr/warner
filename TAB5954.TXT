OBJECT Table 5954 Work-Hour Template
{
  OBJECT-PROPERTIES
  {
    Date=07/09/12;
    Time=12:00:00;
    Version List=NAVW17.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Work-Hour Template;
               PTB=Modelo Hor�rio Trabalho];
    LookupPageID=Page6017;
  }
  FIELDS
  {
    { 1   ;   ;Code                ;Code10        ;CaptionML=[ENU=Code;
                                                              PTB=C�digo];
                                                   NotBlank=Yes }
    { 2   ;   ;Description         ;Text50        ;CaptionML=[ENU=Description;
                                                              PTB=Descri��o] }
    { 3   ;   ;Monday              ;Decimal       ;OnValidate=BEGIN
                                                                CalculateWeekTotal;
                                                              END;

                                                   CaptionML=[ENU=Monday;
                                                              PTB=Segunda];
                                                   DecimalPlaces=0:5;
                                                   MinValue=0;
                                                   MaxValue=24 }
    { 4   ;   ;Tuesday             ;Decimal       ;OnValidate=BEGIN
                                                                CalculateWeekTotal;
                                                              END;

                                                   CaptionML=[ENU=Tuesday;
                                                              PTB=Ter�a-feira];
                                                   DecimalPlaces=0:5;
                                                   MinValue=0;
                                                   MaxValue=24 }
    { 5   ;   ;Wednesday           ;Decimal       ;OnValidate=BEGIN
                                                                CalculateWeekTotal;
                                                              END;

                                                   CaptionML=[ENU=Wednesday;
                                                              PTB=Quarta-feira];
                                                   DecimalPlaces=0:5;
                                                   MinValue=0;
                                                   MaxValue=24 }
    { 6   ;   ;Thursday            ;Decimal       ;OnValidate=BEGIN
                                                                CalculateWeekTotal;
                                                              END;

                                                   CaptionML=[ENU=Thursday;
                                                              PTB=Quinta-feira];
                                                   DecimalPlaces=0:5;
                                                   MinValue=0;
                                                   MaxValue=24 }
    { 7   ;   ;Friday              ;Decimal       ;OnValidate=BEGIN
                                                                CalculateWeekTotal;
                                                              END;

                                                   CaptionML=[ENU=Friday;
                                                              PTB=Sexta-feira];
                                                   DecimalPlaces=0:5;
                                                   MinValue=0;
                                                   MaxValue=24 }
    { 8   ;   ;Saturday            ;Decimal       ;OnValidate=BEGIN
                                                                CalculateWeekTotal;
                                                              END;

                                                   CaptionML=[ENU=Saturday;
                                                              PTB=S�bado];
                                                   DecimalPlaces=0:5;
                                                   MinValue=0;
                                                   MaxValue=24 }
    { 9   ;   ;Sunday              ;Decimal       ;OnValidate=BEGIN
                                                                CalculateWeekTotal;
                                                              END;

                                                   CaptionML=[ENU=Sunday;
                                                              PTB=Domingo];
                                                   DecimalPlaces=0:5;
                                                   MinValue=0;
                                                   MaxValue=24 }
    { 10  ;   ;Total per Week      ;Decimal       ;CaptionML=[ENU=Total per Week;
                                                              PTB=Total por Semana];
                                                   DecimalPlaces=0:5;
                                                   Editable=No }
  }
  KEYS
  {
    {    ;Code                                    ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    PROCEDURE CalculateWeekTotal@1();
    BEGIN
      "Total per Week" := Monday + Tuesday + Wednesday + Thursday + Friday + Saturday + Sunday;
    END;

    BEGIN
    END.
  }
}

