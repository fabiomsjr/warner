OBJECT Codeunit 52112626 NFe Events
{
  OBJECT-PROPERTIES
  {
    Date=04/01/16;
    Time=13:25:36;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    OnRun=BEGIN
          END;

  }
  CODE
  {

    PROCEDURE AfterBuild@52006500(NFe@52006501 : Record 52112624;NFeHeader@52006500 : DotNet "'NavNFe, Version=3.10.0.0, Culture=neutral, PublicKeyToken=1b70b0bb42ec0741'.NavNFe.NFe");
    VAR
      NFeLine@52006502 : DotNet "'NavNFe, Version=3.10.0.0, Culture=neutral, PublicKeyToken=1b70b0bb42ec0741'.NavNFe.NFeLinha";
      I@52006503 : Integer;
    BEGIN
      IF ISNULL(NFeHeader) THEN
        EXIT;

      FOR I := 0 TO NFeHeader.Linhas.Count - 1 DO BEGIN
        NFeLine := NFeHeader.Linhas.Item(I);
        IF NOT ISNULL(NFeLine) THEN
          AddSpecificItemDetails(NFe, NFeHeader, NFeLine);
      END;
    END;

    PROCEDURE BeforeSendHeaderData@52006574(VAR nfe@52006500 : Record 52112624;VAR NavNFeDadosHeader@52006501 : DotNet "'NavNFe, Version=3.10.0.0, Culture=neutral, PublicKeyToken=1b70b0bb42ec0741'.NavNFe.NavNFeDados");
    BEGIN
    END;

    PROCEDURE BeforeSendLineData@52006575(VAR nfe@52006502 : Record 52112624;VAR nfeDadosLine@52006500 : DotNet "'NavNFe, Version=3.10.0.0, Culture=neutral, PublicKeyToken=1b70b0bb42ec0741'.NavNFe.NavNFeDados";lineNo@52006501 : Integer);
    BEGIN
    END;

    PROCEDURE BeforeSendXMLData@52006501(VAR nfe@52006501 : Record 52112624;VAR xmlstring@52006500 : Text);
    BEGIN
    END;

    LOCAL PROCEDURE _@52006510();
    BEGIN
    END;

    LOCAL PROCEDURE AddSpecificItemDetails@52006511(NFe@52006501 : Record 52112624;NFeHeader@52006500 : DotNet "'NavNFe, Version=3.10.0.0, Culture=neutral, PublicKeyToken=1b70b0bb42ec0741'.NavNFe.NFe";NFeLine@52006503 : DotNet "'NavNFe, Version=3.10.0.0, Culture=neutral, PublicKeyToken=1b70b0bb42ec0741'.NavNFe.NFeLinha");
    VAR
      Item@52006502 : Record 27;
      ItemCategory@52006504 : Record 5722;
      x@52006505 : Record 17;
    BEGIN
      IF NOT Item.GET(NFeLine.Codigo) THEN
        EXIT;
      IF Item."Item Category Code" = '' THEN
        EXIT;

      ItemCategory.GET(Item."Item Category Code");
      CASE ItemCategory."NF-e Specific Type" OF
        ItemCategory."NF-e Specific Type"::Medication: AddMedicationItemDetails(NFe, NFeHeader, NFeLine);
      END;
    END;

    LOCAL PROCEDURE AddMedicationItemDetails@52006526(NFe@52006501 : Record 52112624;NFeHeader@52006500 : DotNet "'NavNFe, Version=3.10.0.0, Culture=neutral, PublicKeyToken=1b70b0bb42ec0741'.NavNFe.NFe";NFeLine@52006503 : DotNet "'NavNFe, Version=3.10.0.0, Culture=neutral, PublicKeyToken=1b70b0bb42ec0741'.NavNFe.NFeLinha");
    VAR
      PostedInvItemTracking@52006504 : Record 52112467;
      PostInvItemTrackingLot@52006505 : Query 52112623;
      Medicamento@52006502 : DotNet "'NavNFe, Version=3.10.0.0, Culture=neutral, PublicKeyToken=1b70b0bb42ec0741'.NavNFe.Medicamento";
      ItemLedgEntry@52006506 : Record 32;
    BEGIN
      PostedInvItemTracking.FilterByNFe(NFe);

      PostInvItemTrackingLot.SETFILTER(Document_Type, PostedInvItemTracking.GETFILTER("Document Type"));
      PostInvItemTrackingLot.SETRANGE(Document_No, NFe."Document No.");
      PostInvItemTrackingLot.SETRANGE(Document_Line_No, NFeLine.NumeroNAV);
      PostInvItemTrackingLot.SETFILTER(Lot_No, '<>%1', '');
      PostInvItemTrackingLot.OPEN;
      WHILE PostInvItemTrackingLot.READ DO BEGIN
        Medicamento := Medicamento.Medicamento;
        NFeLine.Medicamentos.Add(Medicamento);

        Medicamento.NumLote := PostInvItemTrackingLot.Lot_No;
        Medicamento.QtdLote := ABS(PostInvItemTrackingLot.Sum_Quantity);
        Medicamento.ValorPrecoMax := 0;

        ItemLedgEntry.RESET;
        ItemLedgEntry.SETCURRENTKEY("Item No.",Open,"Variant Code",Positive,"Location Code","Posting Date","Expiration Date","Lot No.");
        ItemLedgEntry.SETRANGE("Item No.", PostInvItemTrackingLot.Item_No);
        ItemLedgEntry.SETRANGE("Posting Date", 0D, DT2DATE(NFeHeader.DataHoraEmissao));
        ItemLedgEntry.SETRANGE("Lot No.", PostInvItemTrackingLot.Lot_No);
        ItemLedgEntry.SETRANGE(Positive, TRUE);
        IF ItemLedgEntry.FINDLAST THEN BEGIN
          Medicamento.DataFab := CREATEDATETIME(ItemLedgEntry."Posting Date", 0T);
          Medicamento.DataVal := CREATEDATETIME(ItemLedgEntry."Expiration Date", 0T);
        END;
      END;
      PostInvItemTrackingLot.CLOSE;
    END;

    BEGIN
    END.
  }
}

