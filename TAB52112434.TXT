OBJECT Table 52112434 Partial Payments
{
  OBJECT-PROPERTIES
  {
    Date=26/08/15;
    Time=09:04:13;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Partial Payments;
               PTB=Prazos Pagamentos/Cobran�as];
    LookupPageID=Page52112434;
    DrillDownPageID=Page52112434;
  }
  FIELDS
  {
    { 1   ;   ;Payment Terms Code  ;Code20        ;TableRelation="Payment Terms";
                                                   CaptionML=[ENU=Payment Terms Code;
                                                              PTB=Cod. Termos Pagto.] }
    { 2   ;   ;Line No.            ;Integer       ;CaptionML=[ENU=Line No.;
                                                              PTB=N� Linha] }
    { 3   ;   ;% of the Total      ;Decimal       ;OnValidate=BEGIN
                                                                IF ("Total Amount" <> 0) AND ("% of the Total" <> 0) THEN
                                                                  Amount := "Total Amount" * ("% of the Total" / 100)
                                                                ELSE
                                                                   Amount := 0;
                                                              END;

                                                   CaptionML=[ENU=% of the Total;
                                                              PTB=% do Total];
                                                   DecimalPlaces=2:10;
                                                   BlankZero=Yes }
    { 4   ;   ;Due Period Calc.    ;Code20        ;OnValidate=BEGIN
                                                                IF ("Due Date" <> 0D) AND ("Due Period Calc." <> '') THEN
                                                                  BEGIN
                                                                    TESTFIELD("Reference Date");
                                                                    "Due Date" := CALCDATE("Due Period Calc.","Reference Date");
                                                                  END;
                                                              END;

                                                   CaptionML=[ENU=Due Period Calc.;
                                                              PTB=Dist�ncia Entre Prazos];
                                                   DateFormula=Yes }
    { 5   ;   ;Reference Date      ;Date          ;OnValidate=BEGIN
                                                                CLEAR("Due Period Calc.");
                                                              END;
                                                               }
    { 6   ;   ;Due Date            ;Date          ;OnValidate=BEGIN
                                                                CLEAR("Due Period Calc.");
                                                                IF "Due Date" <> 0D THEN
                                                                  BEGIN
                                                                    TESTFIELD("Reference Date");
                                                                    IF "Due Date" <= "Reference Date" THEN
                                                                      ERROR ('Data Vencimento deve ser maior que Data Refer�ncia!');
                                                                    Dias := "Due Date" - "Reference Date";
                                                                    "Due Period Calc." := FORMAT(Dias) + 'D';
                                                                  END
                                                                ELSE
                                                                  CLEAR("Reference Date");
                                                              END;

                                                   CaptionML=[ENU=Due Date;
                                                              PTB=Data Vencimento] }
    { 7   ;   ;Total Amount        ;Decimal       ;CaptionML=[ENU=Total Amount;
                                                              PTB=Valor Total];
                                                   DecimalPlaces=5:5;
                                                   BlankZero=Yes }
    { 8   ;   ;Amount              ;Decimal       ;OnValidate=BEGIN
                                                                TESTFIELD("Total Amount");

                                                                IF Amount <> 0 THEN
                                                                  "% of the Total" := (Amount / "Total Amount") * 100
                                                                ELSE
                                                                   "% of the Total" := 0;
                                                              END;

                                                   CaptionML=[ENU=Amount;
                                                              PTB=Valor Parcela];
                                                   DecimalPlaces=5:5;
                                                   BlankZero=Yes }
  }
  KEYS
  {
    {    ;Payment Terms Code,Line No.             ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      "--- NAVBR5.01.0109"@1102300001 : Integer;
      Dias@1102300000 : Integer;

    BEGIN
    {
      NAVBR5.01.0109

      No.   dd.mm.yy   Developer   Company   DocNo.   Description
      -----------------------------------------------------------------------------------------------------------------
      00    01.12.06   MK          MICROSOFT LOC001   The reference Documentation file LOCALIZATIONBR.DOC
    }
    END.
  }
}

