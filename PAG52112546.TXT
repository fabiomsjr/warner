OBJECT Page 52112546 SPED PC Consolidation Subform
{
  OBJECT-PROPERTIES
  {
    Date=30/04/13;
    Time=15:58:18;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Companies;
               PTB=Empresas];
    SourceTable=Table52112558;
    DataCaptionExpr='';
    SourceTableView=SORTING(Settlement No.,Line No);
    PageType=CardPart;
    AutoSplitKey=Yes;
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1102300000;1;Group  ;
                GroupType=Repeater }

    { 1102300003;2;Field  ;
                SourceExpr=Active }

    { 1102300001;2;Field  ;
                SourceExpr="Company Name" }

    { 1102300005;2;Field  ;
                SourceExpr="Company Settlement No." }

  }
  CODE
  {

    BEGIN
    END.
  }
}

