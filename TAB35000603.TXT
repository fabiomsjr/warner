OBJECT Table 35000603 Accounting data 20092
{
  OBJECT-PROPERTIES
  {
    Date=29/02/12;
    Time=14:54:21;
    Version List=FNX023.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Accounting data;
               PTB=Dados Contabilista];
  }
  FIELDS
  {
    { 1   ;   ;CPF Accounting      ;Code11        ;CaptionML=PTB=CPF Contabilista }
    { 2   ;   ;Name                ;Text50        ;CaptionML=PTB=Nome }
    { 3   ;   ;CRC Accounting      ;Code11        ;CaptionML=PTB=CRC Contabilista }
    { 4   ;   ;CNPJ                ;Code14         }
    { 5   ;   ;CEP                 ;Code10        ;TableRelation="Post Code" }
    { 6   ;   ;Address             ;Text100       ;CaptionML=PTB=Endere�o }
    { 7   ;   ;Number              ;Code10        ;CaptionML=PTB=N�mero }
    { 8   ;   ;Complement Address  ;Text100       ;CaptionML=PTB=Complemento Endere�o }
    { 9   ;   ;District            ;Text50        ;CaptionML=PTB=Bairro }
    { 10  ;   ;Phone               ;Code10        ;CaptionML=PTB=Telefone }
    { 11  ;   ;Fax                 ;Code10        ;CaptionML=PTB=Fax }
    { 12  ;   ;e-Mail              ;Text100       ;CaptionML=PTB=e-Mail }
    { 14  ;   ;Start Date Service  ;Date          ;CaptionML=[ENU=Start Date Service;
                                                              PTB=Data In�cio Servi�o];
                                                   Description=NAVBR5.01.0810 }
    { 15  ;   ;End Date Service    ;Date          ;CaptionML=[ENU=End Date Service;
                                                              PTB=Data Final de Servi�o];
                                                   Description=NAVBR5.01.0810 }
    { 16  ;   ;PO Box              ;Text10        ;CaptionML=[ENU=PO Box;
                                                              PTB=Caixa Postal];
                                                   Description=NAVBR5.01.0810 }
    { 17  ;   ;PO Box CEP          ;Code10        ;TableRelation="Post Code";
                                                   CaptionML=[ENU=PO Box CEP;
                                                              PTB=CEP Caixa Postal];
                                                   Description=NAVBR5.01.0810 }
    { 18  ;   ;SPED Pis/Cofins     ;Boolean       ;Description=EFDPC 1.00 }
    { 19  ;   ;SPED Fiscal         ;Boolean       ;Description=EFDPC 1.00 }
    { 20  ;   ;SPED Cont�bil       ;Boolean       ;Description=EFDPC 1.00 }
  }
  KEYS
  {
    {    ;CPF Accounting                          ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

