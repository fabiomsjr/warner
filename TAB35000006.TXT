OBJECT Table 35000006 Operations Setup2
{
  OBJECT-PROPERTIES
  {
    Date=19/11/12;
    Time=17:07:07;
    Version List=FNX070.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Operations Setup;
               PTB=Conf. Tipos Opera��o];
    LookupPageID=Page35000055;
    DrillDownPageID=Page35000055;
  }
  FIELDS
  {
    { 1   ;   ;Code                ;Code10        ;CaptionML=[ENU=Code;
                                                              PTB=C�digo] }
    { 2   ;   ;Description         ;Text30        ;CaptionML=[ENU=Description;
                                                              PTB=Descri��o] }
    { 3   ;   ;Tax Groups          ;Code10        ;TableRelation="Tax Group";
                                                   CaptionML=[ENU=Tax Groups;
                                                              PTB=Grupo Impostos] }
    { 4   ;   ;Debit Compensation Account;Code20  ;TableRelation="G/L Account".No.;
                                                   CaptionML=[ENU=Debit Compensation Account;
                                                              PTB=Conta Compensa��o D�bito] }
    { 5   ;   ;Credit Compensation Account;Code20 ;TableRelation="G/L Account".No.;
                                                   CaptionML=[ENU=Credit Compensation Account;
                                                              PTB=Conta Compensa��o Cr�dito] }
    { 6   ;   ;Item Entry          ;Boolean       ;OnValidate=BEGIN
                                                                IF "Item Entry" THEN BEGIN
                                                                  "G/L Entry" := TRUE;
                                                                  "Customer/Vendor Entry" := "Customer/Vendor Entry"::" ";
                                                                END;
                                                              END;

                                                   CaptionML=[ENU=Item Entry;
                                                              PTB=Mov. Produto] }
    { 7   ;   ;G/L Entry           ;Boolean       ;OnValidate=BEGIN
                                                                IF "Item Entry" AND NOT "G/L Entry" THEN
                                                                  ERROR(STRSUBSTNO(Text35000000,FIELDCAPTION("G/L Entry"),FIELDCAPTION(Code),Code));
                                                              END;

                                                   CaptionML=[ENU=G/L Entry;
                                                              PTB=Mov. Cont�bil] }
    { 8   ;   ;Customer/Vendor Entry;Option       ;OnValidate=BEGIN
                                                                IF "Customer/Vendor Entry" <> "Customer/Vendor Entry"::" " THEN BEGIN
                                                                  "Item Entry" := FALSE;
                                                                  "Whs. Control" := FALSE;
                                                                  CLEAR("In-Transit Whs. Code");
                                                                END;
                                                              END;

                                                   CaptionML=[ENU=Customer/Vendor Entry;
                                                              PTB=Mov. Cliente/Fornecedor];
                                                   OptionCaptionML=[ENU=" ,IPI Amount,Amount Including Taxes";
                                                                    PTB=" ,Valor IPI,Valor Bruto"];
                                                   OptionString=[ ,IPI Amount,Amount Including Taxes] }
    { 9   ;   ;Whs. Control        ;Boolean       ;OnValidate=BEGIN
                                                                IF NOT "Item Entry" THEN
                                                                  ERROR(STRSUBSTNO(Text35000000,FIELDCAPTION("Item Entry"),FIELDCAPTION(Code),Code));
                                                              END;

                                                   CaptionML=[ENU=Whs. Control;
                                                              PTB=Controla Dep�sito] }
    { 10  ;   ;In-Transit Whs. Code;Code10        ;TableRelation=Location WHERE (Use As In-Transit=CONST(Yes));
                                                   OnValidate=BEGIN
                                                                IF NOT "Whs. Control" THEN
                                                                  ERROR(STRSUBSTNO(Text35000000,FIELDCAPTION("Whs. Control"),FIELDCAPTION(Code),Code));
                                                              END;

                                                   CaptionML=[ENU=In-Transit Whs. Code;
                                                              PTB=Cod. Dep�sito em Tr�nsito] }
    { 11  ;   ;Shipment            ;Boolean       ;CaptionML=[ENU=Shipment;
                                                              PTB=Remessa] }
    { 12  ;   ;Tax Area Code       ;Code20        ;TableRelation="Tax Area";
                                                   CaptionML=[ENU=Tax Area Code;
                                                              PTB=C�d. �rea Imposto] }
    { 13  ;   ;Gen. Prod. Posting Group;Code10    ;TableRelation="Gen. Product Posting Group";
                                                   CaptionML=[ENU=Gen. Prod. Posting Group;
                                                              PTB=Gr. Cont�bil Produto;
                                                              ESM=Grupo contable producto;
                                                              FRC=Param�tre report produit;
                                                              ENC=Gen. Prod. Posting Group] }
    { 20  ;   ;Charge as Expense   ;Boolean       ;CaptionML=[ENU=Charge as Expense;
                                                              PTB=Encargo como Despesa] }
  }
  KEYS
  {
    {    ;Code                                    ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      Text35000000@1102300000 : TextConst 'ENU=%1 must be Yes in %2 %3;PTB=%1 deve ser Sim em %2 %3';

    BEGIN
    END.
  }
}

