OBJECT Page 52112663 FCI Worksheet
{
  OBJECT-PROPERTIES
  {
    Date=20/07/15;
    Time=13:06:34;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=FCI Worksheet;
               PTB=Planilha FCI];
    SourceTable=Table52112663;
    PageType=List;
    OnOpenPage=BEGIN
                 SETCURRENTKEY("Calculation Date","Branch Code","Calculated Item No.","Item Level","Item No.");
                 SETFILTER("VI Amount", '>0');
               END;

    OnAfterGetRecord=BEGIN
                       ChildRecord := "Item Level" > 0;
                     END;

    OnAfterGetCurrRecord=BEGIN
                           RecalculateEnabled := "Calculated Item No." <> '';
                         END;

    ActionList=ACTIONS
    {
      { 52006512;    ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 52006514;1   ;Action    ;
                      Name=CalculateFCI;
                      CaptionML=[ENU=Calculate FCI;
                                 PTB=Calcular FCI];
                      Promoted=Yes;
                      Enabled=RecalculateEnabled;
                      PromotedIsBig=Yes;
                      Image=Calculate;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 REPORT.RUN(REPORT::"Calculate FCI");
                               END;
                                }
      { 52006513;1   ;Action    ;
                      Name=CalculateItem;
                      CaptionML=[ENU=Recalculate Item;
                                 PTB=Recalcular Produto];
                      Promoted=Yes;
                      Enabled=RecalculateEnabled;
                      PromotedIsBig=Yes;
                      Image=Recalculate;
                      PromotedCategory=Process;
                      OnAction=VAR
                                 FCIMgt@52006500 : Codeunit 52112663;
                               BEGIN
                                 FCIMgt.Calculate("Calculated Item No.", "Branch Code", DATE2DMY("Calculation Date", 2), DATE2DMY("Calculation Date", 3), TRUE);
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 52006500;0;Container;
                ContainerType=ContentArea }

    { 52006501;1;Group    ;
                Name=Group;
                IndentationColumnName="Item Level";
                ShowAsTree=Yes;
                GroupType=Repeater }

    { 52006502;2;Field    ;
                SourceExpr="Item No." }

    { 52006515;2;Field    ;
                SourceExpr="Item Description" }

    { 52006503;2;Field    ;
                SourceExpr="Branch Code";
                HideValue=ChildRecord }

    { 52006504;2;Field    ;
                SourceExpr="Calculation Date";
                HideValue=ChildRecord }

    { 52006505;2;Field    ;
                SourceExpr="VI Amount" }

    { 52006506;2;Field    ;
                SourceExpr="VO Amount" }

    { 52006507;2;Field    ;
                SourceExpr="CI Percentage" }

    { 52006508;2;Field    ;
                SourceExpr="CI Range" }

    { 52006509;2;Field    ;
                SourceExpr="New FCI Pending" }

    { 52006510;2;Field    ;
                SourceExpr="FCI File No." }

    { 52006511;2;Field    ;
                SourceExpr="FCI No." }

  }
  CODE
  {
    VAR
      RecalculateEnabled@52006500 : Boolean;
      ChildRecord@52006501 : Boolean;

    BEGIN
    END.
  }
}

