OBJECT Table 304 Issued Fin. Charge Memo Header
{
  OBJECT-PROPERTIES
  {
    Date=09/09/14;
    Time=12:00:00;
    Version List=NAVW18.00;
  }
  PROPERTIES
  {
    DataCaptionFields=No.,Name;
    OnDelete=BEGIN
               TESTFIELD("No. Printed");
               LOCKTABLE;
               FinChrgMemoIssue.DeleteIssuedFinChrgLines(Rec);

               FinChrgCommentLine.SETRANGE(Type,FinChrgCommentLine.Type::"Issued Finance Charge Memo");
               FinChrgCommentLine.SETRANGE("No.","No.");
               FinChrgCommentLine.DELETEALL;
             END;

    CaptionML=[ENU=Issued Fin. Charge Memo Header;
               PTB=Cab. Nota de Juros Emitida];
    LookupPageID=Page452;
    DrillDownPageID=Page452;
  }
  FIELDS
  {
    { 1   ;   ;No.                 ;Code20        ;CaptionML=[ENU=No.;
                                                              PTB=N�];
                                                   NotBlank=Yes }
    { 2   ;   ;Customer No.        ;Code20        ;TableRelation=Customer;
                                                   CaptionML=[ENU=Customer No.;
                                                              PTB=N� Cliente] }
    { 3   ;   ;Name                ;Text50        ;CaptionML=[ENU=Name;
                                                              PTB=Nome] }
    { 4   ;   ;Name 2              ;Text50        ;CaptionML=[ENU=Name 2;
                                                              PTB=Nome Complementar] }
    { 5   ;   ;Address             ;Text50        ;CaptionML=[ENU=Address;
                                                              PTB=Endere�o] }
    { 6   ;   ;Address 2           ;Text50        ;CaptionML=[ENU=Address 2;
                                                              PTB=Endere�o Complementar] }
    { 7   ;   ;Post Code           ;Code20        ;TableRelation="Post Code";
                                                   ValidateTableRelation=No;
                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=Post Code;
                                                              PTB=CEP] }
    { 8   ;   ;City                ;Text30        ;TableRelation="Post Code".City;
                                                   ValidateTableRelation=No;
                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=City;
                                                              PTB=Cidade] }
    { 9   ;   ;County              ;Text30        ;CaptionML=[ENU=County;
                                                              PTB=Regi�o] }
    { 10  ;   ;Country/Region Code ;Code10        ;TableRelation=Country/Region;
                                                   CaptionML=[ENU=Country/Region Code;
                                                              PTB=C�d. Pa�s/Regi�o] }
    { 11  ;   ;Language Code       ;Code10        ;TableRelation=Language;
                                                   CaptionML=[ENU=Language Code;
                                                              PTB=Cod. Idioma] }
    { 12  ;   ;Currency Code       ;Code10        ;TableRelation=Currency;
                                                   CaptionML=[ENU=Currency Code;
                                                              PTB=Cod. Moeda] }
    { 13  ;   ;Contact             ;Text50        ;CaptionML=[ENU=Contact;
                                                              PTB=Contato] }
    { 14  ;   ;Your Reference      ;Text35        ;CaptionML=[ENU=Your Reference;
                                                              PTB=Sua Refer�ncia] }
    { 15  ;   ;Shortcut Dimension 1 Code;Code20   ;TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(1));
                                                   CaptionML=[ENU=Shortcut Dimension 1 Code;
                                                              PTB=Cod. Atalho Dimens�o 1];
                                                   CaptionClass='1,2,1' }
    { 16  ;   ;Shortcut Dimension 2 Code;Code20   ;TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(2));
                                                   CaptionML=[ENU=Shortcut Dimension 2 Code;
                                                              PTB=Cod. Atalho Dimens�o 2];
                                                   CaptionClass='1,2,2' }
    { 17  ;   ;Customer Posting Group;Code10      ;TableRelation="Customer Posting Group";
                                                   CaptionML=[ENU=Customer Posting Group;
                                                              PTB=Gr. Cont�bil Cliente] }
    { 18  ;   ;Gen. Bus. Posting Group;Code10     ;TableRelation="Gen. Business Posting Group";
                                                   CaptionML=[ENU=Gen. Bus. Posting Group;
                                                              PTB=Gr. Cont�bil Neg�cio] }
    { 19  ;   ;VAT Registration No.;Text20        ;CaptionML=[ENU=VAT Registration No.;
                                                              PTB=N� Contribuinte] }
    { 20  ;   ;Reason Code         ;Code10        ;TableRelation="Reason Code";
                                                   CaptionML=[ENU=Reason Code;
                                                              PTB=Cod. Raz�o] }
    { 21  ;   ;Posting Date        ;Date          ;CaptionML=[ENU=Posting Date;
                                                              PTB=Data Registo] }
    { 22  ;   ;Document Date       ;Date          ;CaptionML=[ENU=Document Date;
                                                              PTB=Data Documento] }
    { 23  ;   ;Due Date            ;Date          ;CaptionML=[ENU=Due Date;
                                                              PTB=Data Vencimento] }
    { 25  ;   ;Fin. Charge Terms Code;Code10      ;TableRelation="Finance Charge Terms";
                                                   CaptionML=[ENU=Fin. Charge Terms Code;
                                                              PTB=Cod. Juros] }
    { 26  ;   ;Interest Posted     ;Boolean       ;CaptionML=[ENU=Interest Posted;
                                                              PTB=Juros Registrados] }
    { 27  ;   ;Additional Fee Posted;Boolean      ;CaptionML=[ENU=Additional Fee Posted;
                                                              PTB=Taxa Adicional Registrada] }
    { 29  ;   ;Posting Description ;Text50        ;CaptionML=[ENU=Posting Description;
                                                              PTB=Texto Registro] }
    { 30  ;   ;Comment             ;Boolean       ;FieldClass=FlowField;
                                                   CalcFormula=Exist("Fin. Charge Comment Line" WHERE (Type=CONST(Issued Finance Charge Memo),
                                                                                                       No.=FIELD(No.)));
                                                   CaptionML=[ENU=Comment;
                                                              PTB=Coment�rio];
                                                   Editable=No }
    { 31  ;   ;Remaining Amount    ;Decimal       ;FieldClass=FlowField;
                                                   CalcFormula=Sum("Issued Fin. Charge Memo Line"."Remaining Amount" WHERE (Finance Charge Memo No.=FIELD(No.)));
                                                   CaptionML=[ENU=Remaining Amount;
                                                              PTB=Valor Pendente];
                                                   Editable=No;
                                                   AutoFormatType=1;
                                                   AutoFormatExpr="Currency Code" }
    { 32  ;   ;Interest Amount     ;Decimal       ;FieldClass=FlowField;
                                                   CalcFormula=Sum("Issued Fin. Charge Memo Line".Amount WHERE (Finance Charge Memo No.=FIELD(No.),
                                                                                                                Type=CONST(Customer Ledger Entry)));
                                                   CaptionML=[ENU=Interest Amount;
                                                              PTB=Valor Juros];
                                                   Editable=No;
                                                   AutoFormatType=1;
                                                   AutoFormatExpr="Currency Code" }
    { 33  ;   ;Additional Fee      ;Decimal       ;FieldClass=FlowField;
                                                   CalcFormula=Sum("Issued Fin. Charge Memo Line".Amount WHERE (Finance Charge Memo No.=FIELD(No.),
                                                                                                                Type=CONST(G/L Account)));
                                                   CaptionML=[ENU=Additional Fee;
                                                              PTB=Taxa Adicional];
                                                   Editable=No;
                                                   AutoFormatType=1;
                                                   AutoFormatExpr="Currency Code" }
    { 34  ;   ;VAT Amount          ;Decimal       ;FieldClass=FlowField;
                                                   CalcFormula=Sum("Issued Fin. Charge Memo Line"."VAT Amount" WHERE (Finance Charge Memo No.=FIELD(No.)));
                                                   CaptionML=[ENU=VAT Amount;
                                                              PTB=Valor IPI];
                                                   Editable=No;
                                                   AutoFormatType=1;
                                                   AutoFormatExpr="Currency Code" }
    { 35  ;   ;No. Printed         ;Integer       ;CaptionML=[ENU=No. Printed;
                                                              PTB=N� Impress�es] }
    { 36  ;   ;User ID             ;Code50        ;TableRelation=User."User Name";
                                                   OnLookup=VAR
                                                              UserMgt@1000 : Codeunit 418;
                                                            BEGIN
                                                              UserMgt.LookupUserID("User ID");
                                                            END;

                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=User ID;
                                                              PTB=ID Usu�rio] }
    { 37  ;   ;No. Series          ;Code10        ;TableRelation="No. Series";
                                                   CaptionML=[ENU=No. Series;
                                                              PTB=N� S�ries];
                                                   Editable=No }
    { 38  ;   ;Pre-Assigned No. Series;Code10     ;TableRelation="No. Series";
                                                   CaptionML=[ENU=Pre-Assigned No. Series;
                                                              PTB=N� S�rie Pr�-Atribu�da] }
    { 39  ;   ;Pre-Assigned No.    ;Code20        ;CaptionML=[ENU=Pre-Assigned No.;
                                                              PTB=N� Pre-Atribu�do] }
    { 40  ;   ;Source Code         ;Code10        ;TableRelation="Source Code";
                                                   CaptionML=[ENU=Source Code;
                                                              PTB=Cod. Origem] }
    { 41  ;   ;Tax Area Code       ;Code20        ;TableRelation="Tax Area";
                                                   CaptionML=[ENU=Tax Area Code;
                                                              PTB=C�d. �rea Imposto] }
    { 42  ;   ;Tax Liable          ;Boolean       ;CaptionML=[ENU=Tax Liable;
                                                              PTB=Sujeito a Imposto] }
    { 43  ;   ;VAT Bus. Posting Group;Code10      ;TableRelation="VAT Business Posting Group";
                                                   CaptionML=[ENU=VAT Bus. Posting Group;
                                                              PTB=Gr. Registro Imp. Neg�cio] }
    { 480 ;   ;Dimension Set ID    ;Integer       ;TableRelation="Dimension Set Entry";
                                                   OnLookup=BEGIN
                                                              ShowDimensions;
                                                            END;

                                                   CaptionML=ENU=Dimension Set ID;
                                                   Editable=No }
  }
  KEYS
  {
    {    ;No.                                     ;Clustered=Yes }
    {    ;Customer No.,Posting Date                }
  }
  FIELDGROUPS
  {
    { 1   ;DropDown            ;No.,Customer No.,Name,Posting Date       }
  }
  CODE
  {
    VAR
      IssuedFinChrgMemoHeader@1000 : Record 304;
      FinChrgCommentLine@1001 : Record 306;
      FinChrgMemoIssue@1002 : Codeunit 395;
      DimMgt@1003 : Codeunit 408;

    PROCEDURE PrintRecords@1(ShowRequestForm@1000 : Boolean;SendAsEmail@1002 : Boolean);
    VAR
      ReportSelection@1001 : Record 77;
    BEGIN
      WITH IssuedFinChrgMemoHeader DO BEGIN
        COPY(Rec);
        ReportSelection.SETRANGE(Usage,ReportSelection.Usage::"Fin.Charge");
        ReportSelection.SETFILTER("Report ID",'<>0');
        ReportSelection.FIND('-');
        REPEAT
          IF NOT SendAsEmail THEN
            REPORT.RUNMODAL(ReportSelection."Report ID",ShowRequestForm,FALSE,IssuedFinChrgMemoHeader)
          ELSE
            SendReport(ReportSelection."Report ID");
        UNTIL ReportSelection.NEXT = 0;
      END;
    END;

    PROCEDURE Navigate@2();
    VAR
      NavigateForm@1000 : Page 344;
    BEGIN
      NavigateForm.SetDoc("Posting Date","No.");
      NavigateForm.RUN;
    END;

    PROCEDURE FormatAddr@30(VAR AddrLines@1000 : ARRAY [8] OF Text[50]);
    VAR
      Cust@1001 : Record 18;
      FormatAddrCodeunit@1002 : Codeunit 365;
    BEGIN
      Cust.Contact := Contact;
      Cust.Name := Name;
      Cust."Name 2" := "Name 2";
      Cust.Address := Address;
      Cust."Address 2" := "Address 2";
      Cust."Post Code" := "Post Code";
      Cust.City := City;
      Cust.County := County;
      Cust."Country/Region Code" := "Country/Region Code";
      FormatAddrCodeunit.Customer(AddrLines,Cust);
    END;

    PROCEDURE IncrNoPrinted@3();
    BEGIN
      FinChrgMemoIssue.IncrNoPrinted(Rec);
    END;

    PROCEDURE ShowDimensions@4();
    BEGIN
      DimMgt.ShowDimensionSet("Dimension Set ID",STRSUBSTNO('%1 %2',TABLECAPTION,"No."));
    END;

    LOCAL PROCEDURE SendReport@8(ReportId@1002 : Integer);
    VAR
      DocumentMailing@1003 : Codeunit 260;
      FileManagement@1001 : Codeunit 419;
      ServerAttachmentFilePath@1000 : Text[250];
    BEGIN
      ServerAttachmentFilePath := COPYSTR(FileManagement.ServerTempFileName('pdf'),1,250);
      REPORT.SAVEASPDF(ReportId,ServerAttachmentFilePath,IssuedFinChrgMemoHeader);
      COMMIT;
      DocumentMailing.EmailFileFromIssuedFinChrgMemo(IssuedFinChrgMemoHeader,ServerAttachmentFilePath);
    END;

    BEGIN
    END.
  }
}

