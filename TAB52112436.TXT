OBJECT Table 52112436 Additional Invoice Text Line
{
  OBJECT-PROPERTIES
  {
    Date=28/08/14;
    Time=08:55:24;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Additional Invoice Text Line;
               PTB=Linha Texto Adicional NF];
  }
  FIELDS
  {
    { 1   ;   ;Type                ;Option        ;CaptionML=[ENU=Type;
                                                              PTB=Tipo];
                                                   OptionCaptionML=[ENU=Purchase,Sale,Service;
                                                                    PTB=Compra,Venda,Servi�o];
                                                   OptionString=Purchase,Sale,Service }
    { 2   ;   ;Document Type       ;Option        ;CaptionML=[ENU=Document Type;
                                                              PTB=Tipo Documento];
                                                   OptionCaptionML=[ENU=Order,Invoice,Credit Memo,Post. Shipment,Post. Invoice,Post. Credit Memo,Return,Post. Return,Transfer Order,Post. Transfer,Serv. Invoice,Post Serv. Invoice,Cr. Memo Serv.;
                                                                    PTB=Encomenda,N.Fiscal,N.Cr�dito,Hist. Remessa,Hist. N.Fiscal,Hist. N.Cr�dito,N.Devolu��o,Hist. N.Devolu��o,Ordem Transf.,Ordem Transf. Reg.,Serv. Invoice,Post Serv. Invoice,Cr. Memo Serv.];
                                                   OptionString=Order,Invoice,Credit Memo,Post. Shipment,Post. Invoice,Post. Credit Memo,Return,Post. Return,Transfer Order,Post. Transfer,Serv. Invoice,Post Serv. Invoice,Cr. Memo Serv. }
    { 3   ;   ;Document No.        ;Code20        ;TableRelation=IF (Type=CONST(Sale),
                                                                     Document Type=CONST(Order)) "Sales Header".No. WHERE (Document Type=CONST(Order))
                                                                     ELSE IF (Type=CONST(Sale),
                                                                              Document Type=CONST(Invoice)) "Sales Header".No. WHERE (Document Type=CONST(Invoice))
                                                                              ELSE IF (Type=CONST(Sale),
                                                                                       Document Type=CONST(Credit Memo)) "Sales Header".No. WHERE (Document Type=CONST(Credit Memo))
                                                                                       ELSE IF (Type=CONST(Sale),
                                                                                                Document Type=CONST(Return)) "Sales Header".No. WHERE (Document Type=CONST(Return Order))
                                                                                                ELSE IF (Type=CONST(Sale),
                                                                                                         Document Type=CONST(Post. Invoice)) "Sales Invoice Header".No.
                                                                                                         ELSE IF (Type=CONST(Sale),
                                                                                                                  Document Type=CONST(Post. Credit Memo)) "Sales Cr.Memo Header".No.
                                                                                                                  ELSE IF (Type=CONST(Purchase),
                                                                                                                           Document Type=CONST(Order)) "Purchase Header".No. WHERE (Document Type=CONST(Order))
                                                                                                                           ELSE IF (Type=CONST(Purchase),
                                                                                                                                    Document Type=CONST(Invoice)) "Purchase Header".No. WHERE (Document Type=CONST(Invoice))
                                                                                                                                    ELSE IF (Type=CONST(Purchase),
                                                                                                                                             Document Type=CONST(Credit Memo)) "Purchase Header".No. WHERE (Document Type=CONST(Credit Memo))
                                                                                                                                             ELSE IF (Type=CONST(Purchase),
                                                                                                                                                      Document Type=CONST(Return)) "Purchase Header".No. WHERE (Document Type=CONST(Return Order))
                                                                                                                                                      ELSE IF (Type=CONST(Purchase),
                                                                                                                                                               Document Type=CONST(Post. Invoice)) "Purch. Inv. Header".No.
                                                                                                                                                               ELSE IF (Type=CONST(Purchase),
                                                                                                                                                                        Document Type=CONST(Post. Credit Memo)) "Purch. Cr. Memo Hdr.".No.;
                                                   CaptionML=[ENU=Document No.;
                                                              PTB=N� Documento] }
    { 4   ;   ;Order No.           ;Integer       ;CaptionML=[ENU=Order No.;
                                                              PTB=N� Ordem] }
    { 5   ;   ;Text                ;Text100       ;CaptionML=[ENU=Text;
                                                              PTB=Texto] }
    { 6   ;   ;Fisco               ;Boolean       ;CaptionML=[ENU=Fisco;
                                                              PTB=Fisco] }
    { 100 ;   ;Referenced Document Type;Option    ;OnValidate=BEGIN
                                                                "Referenced Document No." := '';
                                                                "Referenced NF-e Key" := '';
                                                              END;

                                                   CaptionML=[ENU=Referenced Doc. Type;
                                                              PTB=Tipo Doc. Referenciado];
                                                   OptionCaptionML=[ENU=Sales Invoice,Purch. Invoice,Sales Cr. Memo,Purch. Cr. Memo,,Service Invoice,Service Credit Memo;
                                                                    PTB=Venda,Compra,Cr�dito Venda,D�bito Compra,,Nota Servi�o,Nota Cr�dito Servi�o];
                                                   OptionString=Sales Invoice,Purch. Invoice,Sales Cr. Memo,Purch. Cr. Memo,,Service Invoice,Service Cr. Memo }
    { 110 ;   ;Referenced Document No.;Code20     ;TableRelation=IF (Referenced Document Type=CONST(Sales Invoice)) "Sales Invoice Header"
                                                                 ELSE IF (Referenced Document Type=CONST(Purch. Invoice)) "Purch. Inv. Header"
                                                                 ELSE IF (Referenced Document Type=CONST(Sales Cr. Memo)) "Sales Cr.Memo Header"
                                                                 ELSE IF (Referenced Document Type=CONST(Purch. Cr. Memo)) "Purch. Cr. Memo Hdr."
                                                                 ELSE IF (Referenced Document Type=CONST(Service Invoice)) "Service Invoice Header"
                                                                 ELSE IF (Referenced Document Type=CONST(Service Cr. Memo)) "Service Cr.Memo Header";
                                                   OnValidate=VAR
                                                                salesInvHeader@52006500 : Record 112;
                                                                purchInvHeader@52006501 : Record 122;
                                                                salesCrMemoHeader@52006502 : Record 114;
                                                                purchCrMemoHeader@52006503 : Record 124;
                                                                servInvHeader@52006504 : Record 5992;
                                                                servCrMemoHeader@52006505 : Record 5994;
                                                              BEGIN
                                                                IF "Referenced Document No." <> '' THEN BEGIN
                                                                  CASE "Referenced Document Type" OF
                                                                    "Referenced Document Type"::"Sales Invoice": BEGIN
                                                                      salesInvHeader.GET("Referenced Document No.");
                                                                      salesInvHeader.CALCFIELDS("NFe Chave Acesso");
                                                                      "Referenced NF-e Key" := salesInvHeader."NFe Chave Acesso";
                                                                    END;
                                                                    "Referenced Document Type"::"Purch. Invoice": BEGIN
                                                                      purchInvHeader.GET("Referenced Document No.");
                                                                      purchInvHeader.CALCFIELDS("NFe Chave Acesso");
                                                                      "Referenced NF-e Key" := purchInvHeader."NFe Chave Acesso";
                                                                    END;
                                                                    "Referenced Document Type"::"Sales Cr. Memo": BEGIN
                                                                      salesCrMemoHeader.GET("Referenced Document No.");
                                                                      salesCrMemoHeader.CALCFIELDS("NFe Chave Acesso");
                                                                      "Referenced NF-e Key" := salesCrMemoHeader."NFe Chave Acesso";
                                                                    END;
                                                                    "Referenced Document Type"::"Purch. Cr. Memo": BEGIN
                                                                      purchCrMemoHeader.GET("Referenced Document No.");
                                                                      purchCrMemoHeader.CALCFIELDS("NFe Chave Acesso");
                                                                      "Referenced NF-e Key" := purchCrMemoHeader."NFe Chave Acesso";
                                                                    END;
                                                                    "Referenced Document Type"::"Service Invoice": BEGIN
                                                                      servInvHeader.GET("Referenced Document No.");
                                                                      servInvHeader.CALCFIELDS("NFe Chave Acesso");
                                                                      "Referenced NF-e Key" := servInvHeader."NFe Chave Acesso";
                                                                    END;
                                                                    "Referenced Document Type"::"Service Cr. Memo": BEGIN
                                                                      servCrMemoHeader.GET("Referenced Document No.");
                                                                      servCrMemoHeader.CALCFIELDS("NFe Chave Acesso");
                                                                      "Referenced NF-e Key" := servCrMemoHeader."NFe Chave Acesso";
                                                                    END;
                                                                  END;
                                                                END;
                                                              END;

                                                   CaptionML=[ENU=Referenced Document No.;
                                                              PTB=N� Documento Referenciado] }
    { 120 ;   ;Referenced NF-e Key ;Code44        ;CaptionML=[ENU=Referenced NF-e Key;
                                                              PTB=Chave NF-e Ref.] }
  }
  KEYS
  {
    {    ;Type,Document Type,Document No.,Order No.;
                                                   Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      Text001@52006500 : TextConst 'ENU=Record %1 is not related to %2.;PTB=Registro %1 est� relacionado a %2.';

    PROCEDURE FilterBySalesHeader@52006503(salesHeader@52006500 : Record 36);
    BEGIN
      RESET;
      CASE salesHeader."Document Type" OF
        salesHeader."Document Type"::Order: SETRANGE("Document Type", "Document Type"::Order);
        salesHeader."Document Type"::Invoice: SETRANGE("Document Type", "Document Type"::Invoice);
        salesHeader."Document Type"::"Credit Memo": SETRANGE("Document Type", "Document Type"::"Credit Memo");
        salesHeader."Document Type"::"Return Order": SETRANGE("Document Type", "Document Type"::Return);
      ELSE
        ERROR(Text001, salesHeader, TABLECAPTION);
      END;
      SETRANGE(Type, Type::Sale);
      SETRANGE("Document No.", salesHeader."No.");
    END;

    PROCEDURE FilterByPurchHeader@52006502(purchHeader@52006500 : Record 38);
    BEGIN
      RESET;
      CASE purchHeader."Document Type" OF
        purchHeader."Document Type"::Order: SETRANGE("Document Type", "Document Type"::Order);
        purchHeader."Document Type"::Invoice: SETRANGE("Document Type", "Document Type"::Invoice);
        purchHeader."Document Type"::"Credit Memo": SETRANGE("Document Type", "Document Type"::"Credit Memo");
        purchHeader."Document Type"::"Return Order": SETRANGE("Document Type", "Document Type"::Return);
      ELSE
        ERROR(Text001, purchHeader, TABLECAPTION);
      END;
      SETRANGE(Type, Type::Purchase);
      SETRANGE("Document No.", purchHeader."No.");
    END;

    PROCEDURE FilterByServiceHeader@52006500(servHeader@52006500 : Record 5900);
    BEGIN
      RESET;
      CASE servHeader."Document Type" OF
        servHeader."Document Type"::Order: SETRANGE("Document Type", "Document Type"::Order);
        servHeader."Document Type"::Invoice: SETRANGE("Document Type", "Document Type"::Invoice);
        servHeader."Document Type"::"Credit Memo": SETRANGE("Document Type", "Document Type"::"Credit Memo");
      ELSE
        ERROR(Text001, servHeader, TABLECAPTION);
      END;
      SETRANGE(Type, Type::Service);
      SETRANGE("Document No.", servHeader."No.");
    END;

    BEGIN
    END.
  }
}

