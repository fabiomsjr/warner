OBJECT Page 52112441 Tax Progression Table
{
  OBJECT-PROPERTIES
  {
    Date=29/05/14;
    Time=09:52:16;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Tax Progression Table;
               PTB=Tabela Progressiva Imposto];
    SourceTable=Table52112441;
    DelayedInsert=Yes;
    DataCaptionFields=Tax Jurisdiction Code;
    PageType=List;
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1102300000;1;Group  ;
                GroupType=Repeater }

    { 1102300003;2;Field  ;
                SourceExpr="Min Tax Base" }

    { 1102300005;2;Field  ;
                SourceExpr="Max Tax Base" }

    { 1102300001;2;Field  ;
                SourceExpr="Tax %" }

    { 1102300010;2;Field  ;
                SourceExpr="Tax Deduction Amount" }

    { 1102300012;2;Field  ;
                SourceExpr="Dependent Base Deduc. Amount" }

    { 52006500;2;Field    ;
                SourceExpr="Elderlies Base Deduc. Amount" }

  }
  CODE
  {

    BEGIN
    END.
  }
}

