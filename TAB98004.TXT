OBJECT Table 98004 Target Fields
{
  OBJECT-PROPERTIES
  {
    Date=13/05/16;
    Time=14:24:32;
    Modified=Yes;
    Version List=;
  }
  PROPERTIES
  {
  }
  FIELDS
  {
    { 1   ;   ;Entry No.           ;Integer       ;AutoIncrement=Yes }
    { 2   ;   ;Table No.           ;Integer        }
    { 3   ;   ;Table Name          ;Text250        }
    { 4   ;   ;Field No.           ;Integer        }
    { 5   ;   ;Field Name          ;Text250        }
    { 6   ;   ;Field Type          ;Code30         }
    { 21  ;   ;Version             ;Option        ;OptionCaptionML=[ENU=Old,New;
                                                                    ENG=Old,New];
                                                   OptionString=Old,New }
  }
  KEYS
  {
    {    ;Entry No.                               ;Clustered=Yes }
    {    ;Table No.                                }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

