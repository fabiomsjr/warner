OBJECT Table 7380 Phys. Invt. Item Selection
{
  OBJECT-PROPERTIES
  {
    Date=09/09/14;
    Time=12:00:00;
    Version List=NAVW18.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Phys. Invt. Item Selection;
               PTB=Sele��o Prod. Inv. F�sico];
  }
  FIELDS
  {
    { 1   ;   ;Item No.            ;Code20        ;TableRelation=Item;
                                                   CaptionML=[ENU=Item No.;
                                                              PTB=No. Produto];
                                                   NotBlank=Yes;
                                                   Editable=No }
    { 2   ;   ;Variant Code        ;Code10        ;TableRelation="Item Variant".Code WHERE (Item No.=FIELD(Item No.));
                                                   CaptionML=[ENU=Variant Code;
                                                              PTB=C�digo Variante];
                                                   Editable=No }
    { 3   ;   ;Location Code       ;Code10        ;TableRelation=Location WHERE (Use As In-Transit=CONST(No));
                                                   CaptionML=[ENU=Location Code;
                                                              PTB=Cod. Dep�sito];
                                                   Editable=No }
    { 4   ;   ;Description         ;Text50        ;FieldClass=FlowField;
                                                   CalcFormula=Lookup(Item.Description WHERE (No.=FIELD(Item No.)));
                                                   CaptionML=[ENU=Description;
                                                              PTB=Descri��o];
                                                   Editable=No }
    { 5   ;   ;Shelf No.           ;Code10        ;CaptionML=[ENU=Shelf No.;
                                                              PTB=Prateleira];
                                                   Editable=No }
    { 6   ;   ;Phys Invt Counting Period Code;Code10;
                                                   TableRelation="Phys. Invt. Counting Period";
                                                   CaptionML=[ENU=Phys Invt Counting Period Code;
                                                              PTB=Cod. Per. Contagem Inv. F�sico];
                                                   Editable=No }
    { 7   ;   ;Last Counting Date  ;Date          ;CaptionML=[ENU=Last Counting Date;
                                                              PTB=�ltima Data Contagem];
                                                   Editable=No }
    { 8   ;   ;Next Counting Period;Text250       ;CaptionML=[ENU=Next Counting Period;
                                                              PTB=Pr�ximo Per�odo Contagem];
                                                   Editable=No }
    { 9   ;   ;Count Frequency per Year;Integer   ;CaptionML=[ENU=Count Frequency per Year;
                                                              PTB=Frequ�ncia de Contagem por Ano];
                                                   MinValue=0;
                                                   BlankZero=Yes;
                                                   Editable=No }
    { 10  ;   ;Selected            ;Boolean       ;CaptionML=[ENU=Selected;
                                                              PTB=Selecionado] }
    { 11  ;   ;Phys Invt Counting Period Type;Option;
                                                   CaptionML=[ENU=Phys Invt Counting Period Type;
                                                              PTB=Tipo Per. Contagem Inv. F�sico];
                                                   OptionCaptionML=[ENU=" ,Item,SKU";
                                                                    PTB=" ,Item,Unidade de Armazenamento"];
                                                   OptionString=[ ,Item,SKU] }
    { 12  ;   ;Next Counting Start Date;Date      ;CaptionML=ENU=Next Counting Start Date;
                                                   Editable=No }
    { 13  ;   ;Next Counting End Date;Date        ;CaptionML=ENU=Next Counting End Date;
                                                   Editable=No }
  }
  KEYS
  {
    {    ;Item No.,Variant Code,Location Code,Phys Invt Counting Period Code;
                                                   Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

