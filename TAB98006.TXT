OBJECT Table 98006 Mapped Tables
{
  OBJECT-PROPERTIES
  {
    Date=30/05/16;
    Time=16:01:46;
    Modified=Yes;
    Version List=;
  }
  PROPERTIES
  {
    DataPerCompany=No;
  }
  FIELDS
  {
    { 1   ;   ;From Table ID       ;Integer        }
    { 2   ;   ;To Table ID         ;Integer        }
    { 3   ;   ;Type                ;Option        ;OptionCaptionML=[ENU=Step1,Step 2;
                                                                    ENG=Step 1,Step2];
                                                   OptionString=Step 1,Step 2 }
    { 21  ;   ;Skip                ;Boolean        }
    { 31  ;   ;Data Per Company    ;Boolean        }
    { 41  ;   ;Data Per Company Migrated;Boolean   }
    { 51  ;   ;Migrated            ;Boolean        }
  }
  KEYS
  {
    {    ;From Table ID,To Table ID,Type          ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

