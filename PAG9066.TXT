OBJECT Page 9066 Serv Outbound Technician Act.
{
  OBJECT-PROPERTIES
  {
    Date=23/09/13;
    Time=12:00:00;
    Version List=NAVW17.10;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Activities;
               PTB=Atividades];
    SourceTable=Table9052;
    PageType=CardPart;
    OnOpenPage=BEGIN
                 RESET;
                 IF NOT GET THEN BEGIN
                   INIT;
                   INSERT;
                 END;

                 SetRespCenterFilter;
                 SETRANGE("Date Filter",WORKDATE,WORKDATE);
               END;

  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 9   ;1   ;Group     ;
                CaptionML=[ENU=Outbound Service Orders;
                           PTB=Sa�da Pedido Servi�os];
                GroupType=CueGroup;
                ActionList=ACTIONS
                {
                  { 15      ;0   ;Action    ;
                                  CaptionML=[ENU=New Service Order;
                                             PTB=Novo Pedido de Servi�o];
                                  RunObject=Page 5900;
                                  Image=Document;
                                  RunPageMode=Create }
                  { 16      ;0   ;Action    ;
                                  CaptionML=[ENU=Service Item Worksheet;
                                             PTB=Planilha Produto Servi�o];
                                  RunObject=Report 5936;
                                  Image=ServiceItemWorksheet }
                }
                 }

    { 11  ;2   ;Field     ;
                SourceExpr="Service Orders - Today";
                DrillDownPageID=Service Orders }

    { 12  ;2   ;Field     ;
                SourceExpr="Service Orders - to Follow-up";
                DrillDownPageID=Service Orders }

  }
  CODE
  {

    BEGIN
    END.
  }
}

