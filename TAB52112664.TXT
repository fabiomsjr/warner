OBJECT Table 52112664 CI Range
{
  OBJECT-PROPERTIES
  {
    Date=07/07/15;
    Time=13:04:33;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Importation Content Range;
               PTB=Faixa de Conte�do de Importa��o];
    LookupPageID=Page52112664;
  }
  FIELDS
  {
    { 20  ;   ;Range               ;Decimal       ;CaptionML=[ENU=Range;
                                                              PTB=Faixa];
                                                   DecimalPlaces=0:2;
                                                   MinValue=0,01;
                                                   MaxValue=100 }
    { 30  ;   ;Description         ;Text50        ;CaptionML=[ENU=Description;
                                                              PTB=Descri��o] }
    { 60  ;   ;VI Percentage       ;Decimal       ;CaptionML=[ENU=VI (%);
                                                              PTB=VI (%)];
                                                   DecimalPlaces=0:2;
                                                   MinValue=0;
                                                   MaxValue=100 }
  }
  KEYS
  {
    {    ;Range                                   ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

