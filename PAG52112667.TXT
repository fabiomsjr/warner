OBJECT Page 52112667 FCI File Subform
{
  OBJECT-PROPERTIES
  {
    Date=09/07/15;
    Time=09:42:36;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Items;
               PTB=Produtos];
    InsertAllowed=No;
    ModifyAllowed=No;
    SourceTable=Table52112663;
    PageType=ListPart;
    OnDeleteRecord=VAR
                     FCIWorksheet@52006500 : Record 52112663;
                   BEGIN
                     FCIWorksheet.GET("Calculation Date","Branch Code","Calculated Item No.","Item Level","Item No.");
                     FCIWorksheet."FCI File No." := '';
                     FCIWorksheet.MODIFY;
                     CurrPage.UPDATE;
                     EXIT(FALSE);
                   END;

  }
  CONTROLS
  {
    { 52006500;0;Container;
                ContainerType=ContentArea }

    { 52006501;1;Group    ;
                Name=Group;
                GroupType=Repeater }

    { 52006502;2;Field    ;
                SourceExpr="Item No." }

    { 52006503;2;Field    ;
                SourceExpr="Calculation Date" }

    { 52006504;2;Field    ;
                SourceExpr="VI Amount" }

    { 52006505;2;Field    ;
                SourceExpr="VO Amount" }

    { 52006506;2;Field    ;
                SourceExpr="CI Percentage" }

    { 52006507;2;Field    ;
                SourceExpr="FCI No." }

  }
  CODE
  {

    BEGIN
    END.
  }
}

