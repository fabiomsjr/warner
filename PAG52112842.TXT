OBJECT Page 52112842 Group Accounts/Native Accounts
{
  OBJECT-PROPERTIES
  {
    Date=15/04/14;
    Time=14:20:04;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Group Accounts/Native Accounts;
               PTB=Natureza Conta/Grupo Contas];
    SourceTable=Table52112834;
    PageType=List;
    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 1102300008;1 ;ActionGroup;
                      CaptionML=[ENU=Accounts Group;
                                 PTB=Grupo Contas] }
      { 1102300012;2 ;Separator  }
      { 1102300013;2 ;Action    ;
                      CaptionML=[ENU=Adjust Chart of Accounts;
                                 PTB=Ajustar Plano de Contas];
                      RunObject=Report 52112826 }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1102300000;1;Group  ;
                GroupType=Repeater }

    { 1102300001;2;Field  ;
                SourceExpr=Code }

    { 1102300003;2;Field  ;
                SourceExpr=Description }

    { 1102300010;2;Field  ;
                SourceExpr=Totaling }

  }
  CODE
  {

    BEGIN
    END.
  }
}

