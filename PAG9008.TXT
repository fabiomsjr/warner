OBJECT Page 9008 Whse. Basic Role Center
{
  OBJECT-PROPERTIES
  {
    Date=09/09/14;
    Time=12:00:00;
    Version List=NAVW18.00,NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Role Center;
               PTB=Centro de Fun��o];
    PageType=RoleCenter;
    ActionList=ACTIONS
    {
      { 1900000006;0 ;ActionContainer;
                      ActionContainerType=Reports }
      { 14      ;1   ;Action    ;
                      CaptionML=[ENU=Warehouse &Bin List;
                                 PTB=Localiza��es Dep�sito];
                      RunObject=Report 7319;
                      Image=Report }
      { 51      ;1   ;Separator  }
      { 15      ;1   ;Action    ;
                      CaptionML=[ENU=Physical &Inventory List;
                                 PTB=Invent�rio F�sico];
                      RunObject=Report 722;
                      Image=Report }
      { 54      ;1   ;Separator  }
      { 50      ;1   ;Action    ;
                      CaptionML=[ENU=Customer &Labels;
                                 PTB=Cliente - Etiquetas];
                      RunObject=Report 110;
                      Image=Report }
      { 1900000011;0 ;ActionContainer;
                      ActionContainerType=HomeItems }
      { 47      ;1   ;Action    ;
                      CaptionML=[ENU=Sales Orders;
                                 PTB=Pedidos Venda];
                      RunObject=Page 9305;
                      Image=Order }
      { 79      ;1   ;Action    ;
                      CaptionML=[ENU=Released;
                                 PTB=Liberada];
                      RunObject=Page 9305;
                      RunPageView=WHERE(Status=FILTER(Released)) }
      { 81      ;1   ;Action    ;
                      CaptionML=[ENU=Partially Shipped;
                                 PTB=Enviados Parcialmente];
                      RunObject=Page 9305;
                      RunPageView=WHERE(Status=FILTER(Released),
                                        Completely Shipped=FILTER(No)) }
      { 82      ;1   ;Action    ;
                      CaptionML=[ENU=Purchase Return Orders;
                                 PTB=Devolu��es Compras];
                      RunObject=Page 9311;
                      RunPageView=WHERE(Document Type=FILTER(Return Order)) }
      { 83      ;1   ;Action    ;
                      CaptionML=[ENU=Transfer Orders;
                                 PTB=Ordem Transfer�ncias];
                      RunObject=Page 5742;
                      Image=Document }
      { 31      ;1   ;Action    ;
                      CaptionML=[ENU=Released Production Orders;
                                 PTB=Pedidos Produ��o Liberada];
                      RunObject=Page 9326 }
      { 55      ;1   ;Action    ;
                      CaptionML=[ENU=Purchase Orders;
                                 PTB=Compra Pedidos];
                      RunObject=Page 9307 }
      { 33      ;1   ;Action    ;
                      CaptionML=[ENU=Released;
                                 PTB=Liberada];
                      RunObject=Page 9307;
                      RunPageView=WHERE(Status=FILTER(Released)) }
      { 34      ;1   ;Action    ;
                      CaptionML=[ENU=Partially Received;
                                 PTB=Parcialmente Recebidas];
                      RunObject=Page 9307;
                      RunPageView=WHERE(Status=FILTER(Released),
                                        Completely Received=FILTER(No)) }
      { 16      ;1   ;Action    ;
                      CaptionML=[ENU=Assembly Orders;
                                 PTB=Pedidos Montagem];
                      RunObject=Page 902 }
      { 35      ;1   ;Action    ;
                      CaptionML=[ENU=Sales Return Orders;
                                 PTB=Devolu��es Vendas];
                      RunObject=Page 9304;
                      Image=ReturnOrder }
      { 85      ;1   ;Action    ;
                      CaptionML=[ENU=Inventory Picks;
                                 PTB=Invent�rio Picks];
                      RunObject=Page 9316 }
      { 88      ;1   ;Action    ;
                      CaptionML=[ENU=Inventory Put-aways;
                                 PTB=Invent�rio Put-aways];
                      RunObject=Page 9315 }
      { 1       ;1   ;Action    ;
                      CaptionML=[ENU=Inventory Movements;
                                 PTB=Movs. Invent�rio];
                      RunObject=Page 9330 }
      { 5       ;1   ;Action    ;
                      CaptionML=[ENU=Internal Movements;
                                 PTB=Movimentos Internos];
                      RunObject=Page 7400 }
      { 94      ;1   ;Action    ;
                      CaptionML=[ENU=Bin Contents;
                                 PTB=Conte�do Localiza��o];
                      RunObject=Page 7305;
                      Image=BinContent }
      { 22      ;1   ;Action    ;
                      CaptionML=[ENU=Items;
                                 PTB=Produtos];
                      RunObject=Page 31;
                      Image=Item }
      { 23      ;1   ;Action    ;
                      CaptionML=[ENU=Customers;
                                 PTB=Clientes];
                      RunObject=Page 22;
                      Image=Customer }
      { 24      ;1   ;Action    ;
                      CaptionML=[ENU=Vendors;
                                 PTB=Fornecedores];
                      RunObject=Page 27;
                      Image=Vendor }
      { 25      ;1   ;Action    ;
                      CaptionML=[ENU=Shipping Agents;
                                 PTB=Transportadores];
                      RunObject=Page 428 }
      { 27      ;1   ;Action    ;
                      CaptionML=[ENU=Item Reclassification Journals;
                                 PTB=Se��es Di�rio Produtos];
                      RunObject=Page 262;
                      RunPageView=WHERE(Template Type=CONST(Transfer),
                                        Recurring=CONST(No)) }
      { 28      ;1   ;Action    ;
                      CaptionML=[ENU=Phys. Inventory Journals;
                                 PTB=Di�rios Invent�rio F�sico];
                      RunObject=Page 262;
                      RunPageView=WHERE(Template Type=CONST(Phys. Inventory),
                                        Recurring=CONST(No)) }
      { 1900000012;0 ;ActionContainer;
                      ActionContainerType=ActivityButtons }
      { 39      ;1   ;ActionGroup;
                      CaptionML=[ENU=Posted Documents;
                                 PTB=Posted Documentos];
                      Image=FiledPosted }
      { 29      ;2   ;Action    ;
                      CaptionML=[ENU=Posted Invt. Picks;
                                 PTB=Coletas Invt. Registrado];
                      RunObject=Page 7395 }
      { 136     ;2   ;Action    ;
                      CaptionML=[ENU=Posted Sales Shipment;
                                 PTB=Hist�rico Envio Vendas];
                      RunObject=Page 142 }
      { 137     ;2   ;Action    ;
                      CaptionML=[ENU=Posted Transfer Shipments;
                                 PTB=Hist. Envios Transfer�ncias];
                      RunObject=Page 5752 }
      { 138     ;2   ;Action    ;
                      CaptionML=[ENU=Posted Return Shipments;
                                 PTB=Hist�rico Devolu��es];
                      RunObject=Page 6652 }
      { 10      ;2   ;Action    ;
                      CaptionML=[ENU=Posted Invt. Put-aways;
                                 PTB=Estocagem Invt. Registrada];
                      RunObject=Page 7394 }
      { 6       ;2   ;Action    ;
                      CaptionML=[ENU=Registered Invt. Movements;
                                 PTB=Movimentos Invent�rio Registrado];
                      RunObject=Page 7386 }
      { 3       ;2   ;Action    ;
                      CaptionML=[ENU=Posted Transfer Receipts;
                                 PTB=Hist. Recep��es Transf.];
                      RunObject=Page 5753 }
      { 139     ;2   ;Action    ;
                      CaptionML=[ENU=Posted Purchase Receipts;
                                 PTB=Hist�rico Recebimento Compras];
                      RunObject=Page 145 }
      { 141     ;2   ;Action    ;
                      CaptionML=[ENU=Posted Return Receipts;
                                 PTB=Hist�rico Rec. Devolu��es];
                      RunObject=Page 6662;
                      Image=PostedReturnReceipt }
      { 17      ;2   ;Action    ;
                      CaptionML=[ENU=Posted Assembly Orders;
                                 PTB=Pedidos Montagem Registrado];
                      RunObject=Page 922 }
      { 7       ;0   ;ActionContainer;
                      ActionContainerType=NewDocumentItems }
      { 13      ;1   ;Action    ;
                      CaptionML=[ENU=T&ransfer Order;
                                 PTB=Ordem Transfer�ncia];
                      RunObject=Page 5740;
                      Promoted=No;
                      Image=Document;
                      PromotedCategory=Process;
                      RunPageMode=Create }
      { 11      ;1   ;Action    ;
                      CaptionML=[ENU=&Purchase Order;
                                 PTB=Pedido Compra];
                      RunObject=Page 50;
                      Promoted=No;
                      Image=Document;
                      PromotedCategory=Process;
                      RunPageMode=Create }
      { 9       ;1   ;Separator  }
      { 8       ;1   ;Action    ;
                      CaptionML=[ENU=Inventory Pi&ck;
                                 PTB=Inv. Picking];
                      RunObject=Page 7377;
                      Promoted=No;
                      Image=CreateInventoryPickup;
                      PromotedCategory=Process;
                      RunPageMode=Create }
      { 2       ;1   ;Action    ;
                      CaptionML=[ENU=Inventory P&ut-away;
                                 PTB=Inv. Localiza��o];
                      RunObject=Page 7375;
                      Promoted=No;
                      Image=CreatePutAway;
                      PromotedCategory=Process;
                      RunPageMode=Create }
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 52      ;1   ;Separator ;
                      CaptionML=[ENU=Tasks;
                                 PTB=Tarefas];
                      IsHeader=Yes }
      { 12      ;1   ;Action    ;
                      CaptionML=[ENU=Edit Item Reclassification &Journal;
                                 PTB=Di�rio Transf. Produto];
                      RunObject=Page 393;
                      Image=OpenWorksheet }
      { 53      ;1   ;Separator ;
                      CaptionML=[ENU=History;
                                 PTB=Hist�rico];
                      IsHeader=Yes }
      { 84      ;1   ;Action    ;
                      CaptionML=[ENU=Item &Tracing;
                                 PTB=Rastreabilidade de Produto];
                      RunObject=Page 6520;
                      Image=ItemTracing }
    }
  }
  CONTROLS
  {
    { 1900000008;0;Container;
                ContainerType=RoleCenterArea }

    { 1900724808;1;Group   }

    { 1906245608;2;Part   ;
                PagePartID=Page9050;
                PartType=Page }

    { 1907692008;2;Part   ;
                PagePartID=Page9150;
                PartType=Page }

    { 1900724708;1;Group   }

    { 4   ;2   ;Part      ;
                PagePartID=Page760;
                Visible=FALSE;
                PartType=Page }

    { 52006501;2;Part     ;
                Name=ApproveEntriesBR;
                PagePartID=Page52113087;
                PartType=Page }

    { 18  ;2   ;Part      ;
                PagePartID=Page675;
                Visible=false;
                PartType=Page }

    { 19  ;2   ;Part      ;
                PagePartID=Page681;
                PartType=Page }

    { 1903012608;2;Part   ;
                PagePartID=Page9175;
                Visible=FALSE;
                PartType=Page }

    { 1901377608;2;Part   ;
                PartType=System;
                SystemPartID=MyNotes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

