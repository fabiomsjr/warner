OBJECT Page 52112561 Tax Requirement Codes
{
  OBJECT-PROPERTIES
  {
    Date=02/07/13;
    Time=10:33:57;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Tax Requirement Codes;
               PTB=C�digos Obriga��o Imposto];
    SourceTable=Table52112561;
    PageType=List;
  }
  CONTROLS
  {
    { 52006500;0;Container;
                ContainerType=ContentArea }

    { 52006501;1;Group    ;
                Name=Group;
                GroupType=Repeater }

    { 52006504;2;Field    ;
                SourceExpr="Tax Identification" }

    { 52006502;2;Field    ;
                SourceExpr=Code }

    { 52006503;2;Field    ;
                SourceExpr=Description }

  }
  CODE
  {

    BEGIN
    END.
  }
}

