OBJECT Table 5208 Human Resource Comment Line
{
  OBJECT-PROPERTIES
  {
    Date=07/09/12;
    Time=12:00:00;
    Version List=NAVW17.00;
  }
  PROPERTIES
  {
    DataCaptionFields=No.;
    CaptionML=[ENU=Human Resource Comment Line;
               PTB=Linha Coment. Recurso Humano];
    LookupPageID=Page5223;
    DrillDownPageID=Page5223;
  }
  FIELDS
  {
    { 1   ;   ;Table Name          ;Option        ;CaptionML=[ENU=Table Name;
                                                              PTB=Nome Tabela];
                                                   OptionCaptionML=[ENU=Employee,Alternative Address,Employee Qualification,Employee Relative,Employee Absence,Misc. Article Information,Confidential Information;
                                                                    PTB=Empregado,Endere�o alternativo,Qualifica��o empregado,Familiar empregado,Aus�ncia empregado,Informa��o recurso diverso,Informa��o confidencial];
                                                   OptionString=Employee,Alternative Address,Employee Qualification,Employee Relative,Employee Absence,Misc. Article Information,Confidential Information }
    { 2   ;   ;No.                 ;Code20        ;TableRelation=IF (Table Name=CONST(Employee)) Employee.No.
                                                                 ELSE IF (Table Name=CONST(Alternative Address)) "Alternative Address"."Employee No."
                                                                 ELSE IF (Table Name=CONST(Employee Qualification)) "Employee Qualification"."Employee No."
                                                                 ELSE IF (Table Name=CONST(Misc. Article Information)) "Misc. Article Information"."Employee No."
                                                                 ELSE IF (Table Name=CONST(Confidential Information)) "Confidential Information"."Employee No."
                                                                 ELSE IF (Table Name=CONST(Employee Absence)) "Employee Absence"."Employee No."
                                                                 ELSE IF (Table Name=CONST(Employee Relative)) "Employee Relative"."Employee No.";
                                                   CaptionML=[ENU=No.;
                                                              PTB=N�] }
    { 3   ;   ;Table Line No.      ;Integer       ;CaptionML=[ENU=Table Line No.;
                                                              PTB=N� Lin. Tabela] }
    { 4   ;   ;Alternative Address Code;Code10    ;TableRelation=IF (Table Name=CONST(Alternative Address)) "Alternative Address".Code WHERE (Employee No.=FIELD(No.));
                                                   CaptionML=[ENU=Alternative Address Code;
                                                              PTB=Cod. Endere�o Alternativo] }
    { 6   ;   ;Line No.            ;Integer       ;CaptionML=[ENU=Line No.;
                                                              PTB=N� Linha] }
    { 7   ;   ;Date                ;Date          ;CaptionML=[ENU=Date;
                                                              PTB=Data] }
    { 8   ;   ;Code                ;Code10        ;CaptionML=[ENU=Code;
                                                              PTB=C�digo] }
    { 9   ;   ;Comment             ;Text80        ;CaptionML=[ENU=Comment;
                                                              PTB=Coment�rio] }
  }
  KEYS
  {
    {    ;Table Name,No.,Table Line No.,Alternative Address Code,Line No.;
                                                   Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    PROCEDURE SetUpNewLine@1();
    VAR
      HumanResCommentLine@1000 : Record 5208;
    BEGIN
      HumanResCommentLine := Rec;
      HumanResCommentLine.SETRECFILTER;
      HumanResCommentLine.SETRANGE("Line No.");
      HumanResCommentLine.SETRANGE(Date,WORKDATE);
      IF NOT HumanResCommentLine.FINDFIRST THEN
        Date := WORKDATE;
    END;

    BEGIN
    END.
  }
}

