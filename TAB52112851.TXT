OBJECT Table 52112851 G/L Entries Relation.
{
  OBJECT-PROPERTIES
  {
    Date=30/04/13;
    Time=18:14:18;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=G/L Entries Relation;
               PTB=Mov.Cont�bil Relacionado Raz�o];
    LookupPageID=Page52112833;
    DrillDownPageID=Page52112833;
  }
  FIELDS
  {
    { 1   ;   ;Entry No.           ;Integer       ;CaptionML=[ENU=Entry No.;
                                                              PTB=N� Mov.] }
    { 2   ;   ;G/L Account No.     ;Code20        ;TableRelation="ECD Chart of Accounts";
                                                   CaptionML=[ENU=G/L Account No.;
                                                              PTB=N� Conta] }
    { 3   ;   ;Posting Date        ;Date          ;CaptionML=[ENU=Posting Date;
                                                              PTB=Data Registo];
                                                   ClosingDates=Yes }
    { 4   ;   ;Document No.        ;Code20        ;CaptionML=[ENU=Document No.;
                                                              PTB=No. Documento] }
    { 5   ;   ;Description         ;Text50        ;CaptionML=[ENU=Description;
                                                              PTB=Descri��o] }
    { 6   ;   ;Bal. Account No.    ;Code20        ;CaptionML=[ENU=Bal. Account No.;
                                                              PTB=N� Conta Contrap.] }
    { 7   ;   ;Amount              ;Decimal       ;CaptionML=[ENU=Amount;
                                                              PTB=Valor];
                                                   AutoFormatType=1 }
    { 8   ;   ;Global Dimension 1 Code;Code20     ;TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(1));
                                                   CaptionML=[ENU=Global Dimension 1 Code;
                                                              PTB=Cod. Dimens�o 1 Global];
                                                   CaptionClass='1,1,1' }
    { 9   ;   ;Debit Amount        ;Decimal       ;CaptionML=[ENU=Debit Amount;
                                                              PTB=Valor D�bito];
                                                   BlankZero=Yes;
                                                   AutoFormatType=1 }
    { 10  ;   ;Credit Amount       ;Decimal       ;CaptionML=[ENU=Credit Amount;
                                                              PTB=Valor Cr�dito];
                                                   BlankZero=Yes;
                                                   AutoFormatType=1 }
    { 11  ;   ;G/L Account No. Rel.;Code20        ;CaptionML=[ENU=G/L Account No. Rel.;
                                                              PTB=N� Conta Relacionada] }
    { 12  ;   ;Release Type        ;Option        ;CaptionML=[ENU=Release Type;
                                                              PTB=Tipo Lan�amento];
                                                   OptionCaptionML=[ENU=Normal,Closure;
                                                                    PTB=Normal,Encerramento];
                                                   OptionString=Normal,Encerramento }
    { 13  ;   ;Transaction No.     ;Integer       ;CaptionML=[ENU=Transaction No.;
                                                              PTB=N� Lan�amento] }
  }
  KEYS
  {
    {    ;Entry No.                               ;Clustered=Yes }
    {    ;Posting Date,Transaction No.,Debit Amount,Credit Amount }
    {    ;G/L Account No.                         ;SumIndexFields=Amount,Debit Amount,Credit Amount }
    {    ;G/L Account No.,Posting Date            ;SumIndexFields=Amount,Debit Amount,Credit Amount }
    {    ;Transaction No.                          }
    {    ;Document No.,Posting Date                }
    {    ;Posting Date                             }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

