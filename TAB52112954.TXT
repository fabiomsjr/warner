OBJECT Table 52112954 Std. Inv. Legal Text Header
{
  OBJECT-PROPERTIES
  {
    Date=24/09/13;
    Time=11:42:17;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Std. Inv. Legal Text Header;
               PTB=Linha Textos Legais Padr�o NF];
    LookupPageID=Page52112933;
    DrillDownPageID=Page52112933;
  }
  FIELDS
  {
    { 1   ;   ;Code                ;Code10        ;CaptionML=[ENU=Code;
                                                              PTB=C�digo];
                                                   NotBlank=Yes }
    { 2   ;   ;Description         ;Text50        ;CaptionML=[ENU=Description;
                                                              PTB=Descri��o] }
    { 3   ;   ;Fisco Information   ;Boolean       ;CaptionML=[ENU=Fisco Information;
                                                              PTB=Interesse do Fisco] }
  }
  KEYS
  {
    {    ;Code                                    ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    {
      NAVBR5.01.0109

      No.   dd.mm.yy   Developer   Company   DocNo.   Description
      -----------------------------------------------------------------------------------------------------------------
      00    01.12.06   MK          MICROSOFT LOC001   The reference Documentation file LOCALIZATIONBR.DOC
    }
    END.
  }
}

