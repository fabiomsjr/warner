OBJECT Table 388 Dimension Translation
{
  OBJECT-PROPERTIES
  {
    Date=07/09/12;
    Time=12:00:00;
    Version List=NAVW17.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Dimension Translation;
               PTB=Tradu��o Dimens�o];
  }
  FIELDS
  {
    { 1   ;   ;Code                ;Code20        ;TableRelation=Dimension;
                                                   CaptionML=[ENU=Code;
                                                              PTB=C�digo];
                                                   NotBlank=Yes }
    { 2   ;   ;Language ID         ;Integer       ;TableRelation="Windows Language";
                                                   OnValidate=BEGIN
                                                                CALCFIELDS("Language Name");
                                                              END;

                                                   CaptionML=[ENU=Language ID;
                                                              PTB=ID Idioma];
                                                   NotBlank=Yes }
    { 3   ;   ;Name                ;Text30        ;OnValidate=BEGIN
                                                                IF "Code Caption" = '' THEN
                                                                  "Code Caption" := COPYSTR(STRSUBSTNO(Text001,Name),1,MAXSTRLEN("Code Caption"));
                                                                IF "Filter Caption" = '' THEN
                                                                  "Filter Caption" := COPYSTR(STRSUBSTNO(Text002,Name),1,MAXSTRLEN("Filter Caption"));
                                                              END;

                                                   CaptionML=[ENU=Name;
                                                              PTB=Nome] }
    { 4   ;   ;Code Caption        ;Text50        ;CaptionML=[ENU=Code Caption;
                                                              PTB=Cod. Titulo] }
    { 5   ;   ;Filter Caption      ;Text30        ;CaptionML=[ENU=Filter Caption;
                                                              PTB=Filtro Titulo] }
    { 6   ;   ;Language Name       ;Text80        ;FieldClass=FlowField;
                                                   CalcFormula=Lookup("Windows Language".Name WHERE (Language ID=FIELD(Language ID)));
                                                   CaptionML=[ENU=Language Name;
                                                              PTB=Nome Idioma];
                                                   Editable=No }
  }
  KEYS
  {
    {    ;Code,Language ID                        ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      Text001@1000 : TextConst 'ENU=%1 Code;PTB=Cod. %1';
      Text002@1001 : TextConst 'ENU=%1 Filter;PTB=Filtro %1';

    BEGIN
    END.
  }
}

