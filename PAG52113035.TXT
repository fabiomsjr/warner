OBJECT Page 52113035 Electronic Cash Receipts
{
  OBJECT-PROPERTIES
  {
    Date=04/08/15;
    Time=13:51:37;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    Permissions=TableData 21=m;
    Editable=Yes;
    CaptionML=[ENU=Electronic Cash Receipts;
               PTB=Cobran�a Eletr�nica];
    InsertAllowed=No;
    DeleteAllowed=No;
    ModifyAllowed=Yes;
    SourceTable=Table21;
    SourceTableView=SORTING(Open,Due Date)
                    WHERE(Open=CONST(Yes),
                          Positive=CONST(Yes));
    DataCaptionFields=Customer No.;
    PageType=List;
    PromotedActionCategoriesML=[ENU=New,Process,Report,Mark,Receivables;
                                PTB=Novo,Processo,Relat�rio,Marcar,Cobran�as];
    OnOpenPage=BEGIN
                 FilterCustLedgEntries;
               END;

    OnModifyRecord=VAR
                     eCashRecMgt@52006500 : Codeunit 52113025;
                   BEGIN
                     IF "CNAB - Sent" AND (NOT xRec."CNAB - Sent") THEN
                       ERROR(Text001)
                     ELSE
                       IF (NOT "CNAB - Sent") AND xRec."CNAB - Sent" THEN
                         eCashRecMgt.CancelEntry(Rec, TRUE)
                       ELSE
                         IF ((NOT "CNAB - Printed") AND xRec."CNAB - Printed") OR ("CNAB - Printed" AND (NOT xRec."CNAB - Printed")) THEN
                           eCashRecMgt.ChangePrintedFlag(Rec, "CNAB - Printed")
                         ELSE
                           eCashRecMgt.UpdateCustLedgEntry(Rec, "CNAB - Marked");

                     CurrPage.UPDATE(FALSE);
                     EXIT(FALSE);
                   END;

    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 24      ;1   ;ActionGroup;
                      CaptionML=[ENU=Ent&ry;
                                 PTB=M&ovimento] }
      { 35000000;2   ;Action    ;
                      ShortCutKey=Shift+Ctrl+D;
                      CaptionML=[ENU=Dimensions;
                                 PTB=Dimens�es];
                      Image=Dimensions;
                      OnAction=BEGIN
                                 ShowDimensions;
                               END;
                                }
      { 52      ;2   ;Action    ;
                      ShortCutKey=Ctrl+F7;
                      CaptionML=[ENU=Detailed &Ledger Entries;
                                 PTB=Movimentos Deta&lhados];
                      RunObject=Page 573;
                      RunPageView=SORTING(Cust. Ledger Entry No.,Posting Date);
                      RunPageLink=Cust. Ledger Entry No.=FIELD(Entry No.),
                                  Customer No.=FIELD(Customer No.);
                      Image=View }
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 52006505;1   ;ActionGroup;
                      CaptionML=[ENU=Receivables;
                                 PTB=Cobran�as] }
      { 52006504;2   ;Action    ;
                      Name=EditFiltered;
                      CaptionML=[ENU=Edit Filtered;
                                 PTB=Editar Filtradas];
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=EditReminder;
                      PromotedCategory=Category5;
                      OnAction=BEGIN
                                 ActionEditFiltered;
                               END;
                                }
      { 52006500;1   ;ActionGroup;
                      CaptionML=[ENU=Mark;
                                 PTB=Marcar] }
      { 52006501;2   ;Action    ;
                      CaptionML=[ENU=Mark Filtered;
                                 PTB=Marcar Filtrados];
                      Promoted=Yes;
                      Image=Default;
                      PromotedCategory=Category4;
                      OnAction=BEGIN
                                 ActionMarkFiltered(TRUE);
                               END;
                                }
      { 52006502;2   ;Action    ;
                      CaptionML=[ENU=Unmark Filtered;
                                 PTB=Desmarcar Filtrados];
                      Promoted=Yes;
                      Image=Default;
                      PromotedCategory=Category4;
                      OnAction=BEGIN
                                 ActionMarkFiltered(FALSE);
                               END;
                                }
      { 1102300000;1 ;ActionGroup;
                      CaptionML=[ENU=&Functions;
                                 PTB=&Fun��es] }
      { 1102300028;2 ;Action    ;
                      CaptionML=[ENU=Calculate Retained Taxes;
                                 PTB=Calcular Impostos Retidos];
                      Promoted=Yes;
                      Image=CalculateSalesTax;
                      PromotedCategory=Process;
                      OnAction=VAR
                                 electronicCashRecMgt@1102300000 : Codeunit 52113025;
                               BEGIN
                                 electronicCashRecMgt.CalcNetAmount(Rec);
                               END;
                                }
      { 1102300029;2 ;Separator  }
      { 1102300001;2 ;Action    ;
                      CaptionML=[ENU=&Export File;
                                 PTB=&Exportar Arquivo];
                      Promoted=Yes;
                      Image=Export;
                      PromotedCategory=Process;
                      OnAction=VAR
                                 electronicCashRecMgt@1102300001 : Codeunit 52113025;
                               BEGIN
                                 electronicCashRecMgt.ExportFile(Rec);
                               END;
                                }
      { 1102300002;2 ;Action    ;
                      CaptionML=[ENU=&Print Boleto;
                                 PTB=Im&primir Boleto];
                      Promoted=Yes;
                      Image=DepositSlip;
                      PromotedCategory=Report;
                      OnAction=VAR
                                 electronicCashRecMgt@1102300001 : Codeunit 52113025;
                               BEGIN
                                 electronicCashRecMgt.PrintBoletos(Rec);
                               END;
                                }
      { 1102300003;2 ;Action    ;
                      CaptionML=[ENU=&Import File;
                                 PTB=&Importar Arquivo];
                      Promoted=Yes;
                      Image=Import;
                      PromotedCategory=Process;
                      OnAction=VAR
                                 electronicCashRecMgt@1102300001 : Codeunit 52113025;
                               BEGIN
                                 electronicCashRecMgt.ImportFile;
                               END;
                                }
      { 37      ;1   ;Action    ;
                      CaptionML=[ENU=&Navigate;
                                 PTB=&Navegar];
                      Image=Navigate;
                      OnAction=VAR
                                 navigate@35000000 : Page 344;
                               BEGIN
                                 navigate.SetDoc("Posting Date","Document No.");
                                 navigate.RUN;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 1102300010;2;Field  ;
                SourceExpr="CNAB - Marked" }

    { 1102300020;2;Field  ;
                SourceExpr="Cnab - Text";
                Visible=FALSE }

    { 1102300018;2;Field  ;
                SourceExpr=Protesta;
                Visible=FALSE }

    { 1102300016;2;Field  ;
                Lookup=Yes;
                SourceExpr="Collection Instruction 1";
                Visible=FALSE;
                LookupPageID=CNAB Charge Instructions }

    { 1102300022;2;Field  ;
                SourceExpr="Collection Instruction 2";
                Visible=FALSE;
                LookupPageID=CNAB Charge Instructions }

    { 1102300006;2;Field  ;
                SourceExpr="CNAB - Sent" }

    { 1102300008;2;Field  ;
                SourceExpr="CNAB - Printed" }

    { 2   ;2   ;Field     ;
                SourceExpr="Posting Date";
                Editable=FALSE }

    { 4   ;2   ;Field     ;
                SourceExpr="Document Type";
                Editable=FALSE }

    { 6   ;2   ;Field     ;
                SourceExpr="Document No.";
                Editable=FALSE }

    { 1102300024;2;Field  ;
                SourceExpr="External Document No.";
                Editable=FALSE }

    { 8   ;2   ;Field     ;
                SourceExpr="Customer No.";
                Editable=FALSE }

    { 10  ;2   ;Field     ;
                SourceExpr=Description;
                Editable=FALSE }

    { 39  ;2   ;Field     ;
                SourceExpr="Global Dimension 1 Code";
                Visible=FALSE;
                Editable=FALSE }

    { 41  ;2   ;Field     ;
                SourceExpr="Global Dimension 2 Code";
                Visible=FALSE;
                Editable=FALSE }

    { 45  ;2   ;Field     ;
                SourceExpr="Currency Code";
                Visible=FALSE;
                Editable=FALSE }

    { 55  ;2   ;Field     ;
                SourceExpr="Original Amount";
                Visible=FALSE;
                Editable=FALSE }

    { 53  ;2   ;Field     ;
                SourceExpr="Original Amt. (LCY)";
                Visible=FALSE;
                Editable=FALSE }

    { 12  ;2   ;Field     ;
                SourceExpr=Amount;
                Visible=FALSE;
                Editable=FALSE }

    { 47  ;2   ;Field     ;
                SourceExpr="Amount (LCY)";
                Visible=FALSE;
                Editable=FALSE }

    { 14  ;2   ;Field     ;
                SourceExpr="Remaining Amount";
                Visible=FALSE;
                Editable=FALSE }

    { 49  ;2   ;Field     ;
                SourceExpr="Remaining Amt. (LCY)";
                Visible=TRUE;
                Editable=FALSE }

    { 35000001;2;Field    ;
                SourceExpr="Taxes Amount" }

    { 1102300026;2;Field  ;
                CaptionML=[ENU=Net Amount;
                           PTB=Valor L�quido];
                SourceExpr=GetNetAmount;
                Editable=FALSE }

    { 16  ;2   ;Field     ;
                SourceExpr="Due Date";
                Editable=FALSE }

    { 1102300004;2;Field  ;
                SourceExpr="CNAB - Our Number";
                Editable=FALSE }

    { 1102300012;2;Field  ;
                SourceExpr="CNAB - Filename";
                Visible=FALSE;
                Editable=FALSE }

    { 1102300014;2;Field  ;
                SourceExpr="CNAB - Datetime";
                Visible=FALSE;
                Editable=FALSE }

    { 18  ;2   ;Field     ;
                SourceExpr="Pmt. Discount Date";
                Visible=FALSE;
                Editable=FALSE }

    { 59  ;2   ;Field     ;
                SourceExpr="Pmt. Disc. Tolerance Date";
                Visible=FALSE;
                Editable=FALSE }

    { 20  ;2   ;Field     ;
                SourceExpr="Original Pmt. Disc. Possible";
                Visible=FALSE;
                Editable=FALSE }

    { 57  ;2   ;Field     ;
                SourceExpr="Remaining Pmt. Disc. Possible";
                Visible=FALSE;
                Editable=FALSE }

    { 61  ;2   ;Field     ;
                SourceExpr="Max. Payment Tolerance";
                Visible=FALSE;
                Editable=FALSE }

    { 22  ;2   ;Field     ;
                SourceExpr="On Hold";
                Visible=FALSE;
                Editable=FALSE }

    { 83  ;2   ;Field     ;
                SourceExpr="User ID";
                Visible=FALSE;
                Editable=FALSE }

    { 85  ;2   ;Field     ;
                SourceExpr="Source Code";
                Visible=FALSE;
                Editable=FALSE }

    { 87  ;2   ;Field     ;
                SourceExpr="Reason Code";
                Visible=FALSE;
                Editable=FALSE }

    { 30  ;2   ;Field     ;
                SourceExpr="Entry No.";
                Visible=FALSE;
                Editable=FALSE }

  }
  CODE
  {
    VAR
      Text001@1102300000 : TextConst 'ENU=This field can''t be marked manually;PTB=Esse campo n�o pode ser marcado manualmente';

    PROCEDURE ActionMarkFiltered@52006505(doMark@52006500 : Boolean);
    VAR
      custLedgEntry@52006501 : Record 21;
    BEGIN
      MODIFYALL("CNAB - Marked", doMark);
    END;

    PROCEDURE ActionEditFiltered@52006507();
    VAR
      editFiltered@52006500 : Page 52113056;
      tmpCustLedgEntry@52006501 : TEMPORARY Record 21;
    BEGIN
      editFiltered.DefaultValues := Rec;
      IF editFiltered.RUNMODAL = ACTION::OK THEN BEGIN
        editFiltered.GETRECORD(tmpCustLedgEntry);
        MODIFYALL(Protesta, tmpCustLedgEntry.Protesta);
        MODIFYALL("Cnab - Text", tmpCustLedgEntry."Cnab - Text");
        MODIFYALL("Collection Instruction 1", tmpCustLedgEntry."Collection Instruction 1");
        MODIFYALL("Collection Instruction 2", tmpCustLedgEntry."Collection Instruction 2");
      END;
    END;

    LOCAL PROCEDURE FilterCustLedgEntries@52006500();
    VAR
      CustLedgEntry@52006502 : Record 21;
      Customer@52006501 : Record 18;
      PaymMethod@52006500 : Record 289;
      Field@52006505 : Record 2000000041;
      RecRef@52006503 : RecordRef;
      FieldRef@52006504 : FieldRef;
      RecRefFilterSet@52006507 : Boolean;
      PaymMethodFilter@52006506 : Text;
    BEGIN
      PaymMethod.SETRANGE("CNAB Type", PaymMethod."CNAB Type"::Receipt);
      IF PaymMethod.FINDSET THEN
        REPEAT
          IF PaymMethodFilter <> '' THEN
            PaymMethodFilter += '|';
          PaymMethodFilter += PaymMethod.Code;
        UNTIL PaymMethod.NEXT = 0
      ELSE
        EXIT;

      Field.SETRANGE(TableNo, DATABASE::"Cust. Ledger Entry");
      Field.SETRANGE(FieldName, 'Payment Method Code');
      IF Field.FINDFIRST THEN BEGIN
        RecRef.OPEN(DATABASE::"Cust. Ledger Entry");
        FieldRef := RecRef.FIELD(Field."No.");
        FieldRef.SETFILTER(PaymMethodFilter);
        IF CustLedgEntry.ISEMPTY THEN
          FieldRef.SETRANGE
        ELSE BEGIN
          RecRefFilterSet := TRUE;
          RecRef.SETTABLE(CustLedgEntry);
        END;
      END;

      CustLedgEntry.SETCURRENTKEY(Open, "Due Date");
      CustLedgEntry.SETRANGE(Open, TRUE);
      CustLedgEntry.SETRANGE(Positive, TRUE);
      CustLedgEntry.SETRANGE("On Hold", '');
      IF CustLedgEntry.FINDFIRST THEN
        REPEAT
          IF NOT RecRefFilterSet THEN BEGIN
            Customer.GET(CustLedgEntry."Customer No.");
            PaymMethod.SETRANGE(Code, Customer."Payment Method Code");
          END;

          IF NOT PaymMethod.ISEMPTY THEN BEGIN
            GET(CustLedgEntry."Entry No.");
            MARK(TRUE);
          END;
        UNTIL CustLedgEntry.NEXT = 0;

      MARKEDONLY(TRUE);
    END;

    BEGIN
    END.
  }
}

