OBJECT Table 35000075 Posted Item Charge2
{
  OBJECT-PROPERTIES
  {
    Date=13/07/12;
    Time=10:43:42;
    Version List=FNX056.00;
  }
  PROPERTIES
  {
  }
  FIELDS
  {
    { 10  ;   ;Table Type          ;Option        ;CaptionML=[ENU=Table Type;
                                                              PTB=Tipo Tabela];
                                                   OptionCaptionML=[ENU=Sale,Purchase;
                                                                    PTB=Venda,Compra];
                                                   OptionString=Sale,Purchase }
    { 20  ;   ;Document Type       ;Option        ;CaptionML=[ENU=Document Type;
                                                              PTB=Tipo Documento];
                                                   OptionCaptionML=[ENU=Invoice,Credit Memo;
                                                                    PTB=Nota Fiscal,Nota Cr�dito];
                                                   OptionString=Invoice,Credit Memo }
    { 30  ;   ;Document No.        ;Code20        ;CaptionML=[ENU=Document No.;
                                                              PTB=N. Documento] }
    { 40  ;   ;Document Line No.   ;Integer       ;CaptionML=[ENU=Document Line No.;
                                                              PTB=N. Linha Documento] }
    { 50  ;   ;Line No.            ;Integer       ;CaptionML=[ENU=Line No.;
                                                              PTB=N. Linha] }
    { 70  ;   ;Item Charge No.     ;Code20        ;TableRelation="Item Charge";
                                                   CaptionML=[ENU=Item Charge No.;
                                                              PTB=N� Encargo] }
    { 1000;   ;Unit Cost           ;Decimal       ;DecimalPlaces=0:5 }
    { 1010;   ;ICMS Base Amount    ;Decimal        }
    { 1012;   ;ICMS Amount         ;Decimal       ;DecimalPlaces=0:5 }
    { 1013;   ;ICMS Exempt Amount  ;Decimal        }
    { 1014;   ;ICMS Others Amount  ;Decimal        }
    { 1020;   ;IPI Base Amount     ;Decimal        }
    { 1022;   ;IPI Amount          ;Decimal       ;AutoFormatType=1 }
    { 1023;   ;IPI Exempt Amount   ;Decimal        }
    { 1024;   ;IPI Others Amount   ;Decimal        }
    { 1030;   ;PIS Base Amount     ;Decimal        }
    { 1032;   ;PIS Amount          ;Decimal        }
    { 1040;   ;COFINS Base Amount  ;Decimal        }
    { 1042;   ;COFINS Amount       ;Decimal        }
    { 1050;   ;II Base Amount      ;Decimal        }
    { 1052;   ;II Amount           ;Decimal        }
    { 2010;   ;Insurance Amount    ;Decimal        }
    { 2020;   ;Freight Amount      ;Decimal        }
    { 3030;   ;Other Expenses Amount;Decimal       }
  }
  KEYS
  {
    {    ;Table Type,Document Type,Document No.,Document Line No.,Line No.;
                                                   Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

