OBJECT Table 5611 Depreciation Book
{
  OBJECT-PROPERTIES
  {
    Date=06/07/15;
    Time=15:41:15;
    Version List=NAVW18.00;
  }
  PROPERTIES
  {
    Permissions=TableData 5604=rimd,
                TableData 5612=rm;
    DataCaptionFields=Code,Description;
    OnInsert=BEGIN
               WITH FAPostingTypeSetup DO BEGIN
                 "Depreciation Book Code" := Code;
                 "FA Posting Type" := "FA Posting Type"::Appreciation;
                 "Part of Book Value" := TRUE;
                 "Part of Depreciable Basis" := TRUE;
                 "Include in Depr. Calculation" := TRUE;
                 "Include in Gain/Loss Calc." := FALSE;
                 "Depreciation Type" := FALSE;
                 "Acquisition Type" := TRUE;
                 Sign := Sign::Debit;
                 INSERT;
                 "FA Posting Type" := "FA Posting Type"::"Write-Down";
                 "Part of Depreciable Basis" := FALSE;
                 "Include in Gain/Loss Calc." := TRUE;
                 "Depreciation Type" := TRUE;
                 "Acquisition Type" := FALSE;
                 Sign := Sign::Credit;
                 INSERT;
                 "FA Posting Type" := "FA Posting Type"::"Custom 1";
                 INSERT;
                 "FA Posting Type" := "FA Posting Type"::"Custom 2";
                 INSERT;
               END;
             END;

    OnModify=BEGIN
               "Last Date Modified" := TODAY;
             END;

    OnDelete=BEGIN
               FASetup.GET;
               FADeprBook.SETCURRENTKEY("Depreciation Book Code");
               FADeprBook.SETRANGE("Depreciation Book Code",Code);
               IF FADeprBook.FIND('-') THEN
                 ERROR(Text000);

               IF InsCoverageLedgEntry.FINDFIRST AND (FASetup."Insurance Depr. Book" = Code) THEN
                 ERROR(
                   Text001,
                   FASetup.TABLECAPTION,FASetup.FIELDCAPTION("Insurance Depr. Book"),Code);

               FAPostingTypeSetup.SETRANGE("Depreciation Book Code",Code);
               FAPostingTypeSetup.DELETEALL;

               FAJnlSetup.SETRANGE("Depreciation Book Code",Code);
               FAJnlSetup.DELETEALL;
             END;

    OnRename=BEGIN
               "Last Date Modified" := TODAY;
             END;

    CaptionML=[ENU=Depreciation Book;
               PTB=Livro Deprecia��o];
    LookupPageID=Page5611;
    DrillDownPageID=Page5611;
  }
  FIELDS
  {
    { 1   ;   ;Code                ;Code10        ;CaptionML=[ENU=Code;
                                                              PTB=C�digo];
                                                   NotBlank=Yes }
    { 2   ;   ;Description         ;Text50        ;CaptionML=[ENU=Description;
                                                              PTB=Descri��o] }
    { 3   ;   ;G/L Integration - Acq. Cost;Boolean;CaptionML=[ENU=G/L Integration - Acq. Cost;
                                                              PTB=Integr. C/G - Custo] }
    { 4   ;   ;G/L Integration - Depreciation;Boolean;
                                                   CaptionML=[ENU=G/L Integration - Depreciation;
                                                              PTB=Integr. C/G - Deprecia��o] }
    { 5   ;   ;G/L Integration - Write-Down;Boolean;
                                                   CaptionML=[ENU=G/L Integration - Write-Down;
                                                              PTB=Integr. C/G - Baixa] }
    { 6   ;   ;G/L Integration - Appreciation;Boolean;
                                                   CaptionML=[ENU=G/L Integration - Appreciation;
                                                              PTB=Integr. C/G - Reavalia��o] }
    { 7   ;   ;G/L Integration - Custom 1;Boolean ;CaptionML=[ENU=G/L Integration - Custom 1;
                                                              PTB=Integr. C/G - Especial] }
    { 8   ;   ;G/L Integration - Custom 2;Boolean ;CaptionML=[ENU=G/L Integration - Custom 2;
                                                              PTB=Integr. C/G - Provis�o] }
    { 9   ;   ;G/L Integration - Disposal;Boolean ;CaptionML=[ENU=G/L Integration - Disposal;
                                                              PTB=Integr. C/G - Venda/Baixa] }
    { 10  ;   ;G/L Integration - Maintenance;Boolean;
                                                   CaptionML=[ENU=G/L Integration - Maintenance;
                                                              PTB=Integr. C/G - Manuten��o] }
    { 11  ;   ;Disposal Calculation Method;Option ;CaptionML=[ENU=Disposal Calculation Method;
                                                              PTB=M�todo C�lculo Venda/Baixa];
                                                   OptionCaptionML=[ENU=Net,Gross;
                                                                    PTB=L�quido,Bruto];
                                                   OptionString=Net,Gross }
    { 12  ;   ;Use Custom 1 Depreciation;Boolean  ;OnValidate=BEGIN
                                                                IF "Use Custom 1 Depreciation" THEN
                                                                  TESTFIELD("Fiscal Year 365 Days",FALSE);
                                                              END;

                                                   CaptionML=[ENU=Use Custom 1 Depreciation;
                                                              PTB=Utiliza Deprec. Especial] }
    { 13  ;   ;Allow Depr. below Zero;Boolean     ;CaptionML=[ENU=Allow Depr. below Zero;
                                                              PTB=Permite Deprec. Menor a Zero] }
    { 14  ;   ;Use FA Exch. Rate in Duplic.;Boolean;
                                                   OnValidate=BEGIN
                                                                IF NOT "Use FA Exch. Rate in Duplic." THEN
                                                                  "Default Exchange Rate" := 0;
                                                              END;

                                                   CaptionML=[ENU=Use FA Exch. Rate in Duplic.;
                                                              PTB=AF Utiliza. T.C. em Duplic.] }
    { 15  ;   ;Part of Duplication List;Boolean   ;CaptionML=[ENU=Part of Duplication List;
                                                              PTB=Parte da Lista Duplicados] }
    { 17  ;   ;Last Date Modified  ;Date          ;CaptionML=[ENU=Last Date Modified;
                                                              PTB=�ltima Data Modifica��o];
                                                   Editable=No }
    { 18  ;   ;Allow Indexation    ;Boolean       ;CaptionML=[ENU=Allow Indexation;
                                                              PTB=Permite Aj. valores] }
    { 19  ;   ;Use Same FA+G/L Posting Dates;Boolean;
                                                   InitValue=Yes;
                                                   CaptionML=[ENU=Use Same FA+G/L Posting Dates;
                                                              PTB=Usa Data Reg. Cont�bil+AF Comuns] }
    { 20  ;   ;Default Exchange Rate;Decimal      ;OnValidate=BEGIN
                                                                IF "Default Exchange Rate" > 0 THEN
                                                                  TESTFIELD("Use FA Exch. Rate in Duplic.",TRUE);
                                                              END;

                                                   CaptionML=[ENU=Default Exchange Rate;
                                                              PTB=Taxa c�mbio gen�rico];
                                                   DecimalPlaces=4:4;
                                                   MinValue=0 }
    { 23  ;   ;Use FA Ledger Check ;Boolean       ;InitValue=Yes;
                                                   CaptionML=[ENU=Use FA Ledger Check;
                                                              PTB=Usar Verif. Mov. AF] }
    { 24  ;   ;Use Rounding in Periodic Depr.;Boolean;
                                                   CaptionML=[ENU=Use Rounding in Periodic Depr.;
                                                              PTB=Arredondar em Deprecia��o Peri�dica] }
    { 25  ;   ;New Fiscal Year Starting Date;Date ;CaptionML=[ENU=New Fiscal Year Starting Date;
                                                              PTB=Data inicial novo exerc�cio] }
    { 26  ;   ;No. of Days in Fiscal Year;Integer ;CaptionML=[ENU=No. of Days in Fiscal Year;
                                                              PTB=N� Dias no Exerc�cio];
                                                   MinValue=10;
                                                   MaxValue=1080 }
    { 27  ;   ;Allow Changes in Depr. Fields;Boolean;
                                                   CaptionML=[ENU=Allow Changes in Depr. Fields;
                                                              PTB=Permite Modif. Campos Deprec.] }
    { 28  ;   ;Default Final Rounding Amount;Decimal;
                                                   CaptionML=[ENU=Default Final Rounding Amount;
                                                              PTB=Valor arredond. final predefinido];
                                                   MinValue=0;
                                                   AutoFormatType=1 }
    { 29  ;   ;Default Ending Book Value;Decimal  ;CaptionML=[ENU=Default Ending Book Value;
                                                              PTB=Valor L�quido Final Gen�rico];
                                                   MinValue=0;
                                                   AutoFormatType=1 }
    { 32  ;   ;Periodic Depr. Date Calc.;Option   ;OnValidate=BEGIN
                                                                IF "Periodic Depr. Date Calc." <> "Periodic Depr. Date Calc."::"Last Entry" THEN
                                                                  TESTFIELD("Fiscal Year 365 Days",FALSE);
                                                              END;

                                                   CaptionML=[ENU=Periodic Depr. Date Calc.;
                                                              PTB=C�lc. Data Deprec. Peri�dica];
                                                   OptionCaptionML=[ENU=Last Entry,Last Depr. Entry;
                                                                    PTB=�ltimo Mov.,�ltimo Mov. Deprec.];
                                                   OptionString=Last Entry,Last Depr. Entry }
    { 33  ;   ;Mark Errors as Corrections;Boolean ;CaptionML=[ENU=Mark Errors as Corrections;
                                                              PTB=Marcar anulado como corre��o] }
    { 34  ;   ;Add-Curr Exch Rate - Acq. Cost;Boolean;
                                                   AccessByPermission=TableData 4=R;
                                                   CaptionML=[ENU=Add-Curr Exch Rate - Acq. Cost;
                                                              PTB=Custo Taxa c�mbio Moed-adic.] }
    { 35  ;   ;Add.-Curr. Exch. Rate - Depr.;Boolean;
                                                   AccessByPermission=TableData 4=R;
                                                   CaptionML=[ENU=Add.-Curr. Exch. Rate - Depr.;
                                                              PTB=Depr.-Taxa c�mbio Moed-adic.] }
    { 36  ;   ;Add-Curr Exch Rate -Write-Down;Boolean;
                                                   AccessByPermission=TableData 4=R;
                                                   CaptionML=[ENU=Add-Curr Exch Rate -Write-Down;
                                                              PTB=Depre. Taxa C�mbio Moed-Adic.] }
    { 37  ;   ;Add-Curr. Exch. Rate - Apprec.;Boolean;
                                                   AccessByPermission=TableData 4=R;
                                                   CaptionML=[ENU=Add-Curr. Exch. Rate - Apprec.;
                                                              PTB=Reavalia��o T.C.Moeda-Adic.] }
    { 38  ;   ;Add-Curr. Exch Rate - Custom 1;Boolean;
                                                   AccessByPermission=TableData 4=R;
                                                   CaptionML=[ENU=Add-Curr. Exch Rate - Custom 1;
                                                              PTB=Especific�o t.c. Moed-adic.] }
    { 39  ;   ;Add-Curr. Exch Rate - Custom 2;Boolean;
                                                   AccessByPermission=TableData 4=R;
                                                   CaptionML=[ENU=Add-Curr. Exch Rate - Custom 2;
                                                              PTB=Provis�o t.c. Moed-adic.] }
    { 40  ;   ;Add.-Curr. Exch. Rate - Disp.;Boolean;
                                                   AccessByPermission=TableData 4=R;
                                                   CaptionML=[ENU=Add.-Curr. Exch. Rate - Disp.;
                                                              PTB=Venda/Baixa-t.c. Moed-adic.] }
    { 41  ;   ;Add.-Curr. Exch. Rate - Maint.;Boolean;
                                                   AccessByPermission=TableData 4=R;
                                                   CaptionML=[ENU=Add.-Curr. Exch. Rate - Maint.;
                                                              PTB=Manuten��o t.c. Moed-adic.] }
    { 42  ;   ;Use Default Dimension;Boolean      ;CaptionML=[ENU=Use Default Dimension;
                                                              PTB=Usar Dimens�o Padr�o] }
    { 43  ;   ;Subtract Disc. in Purch. Inv.;Boolean;
                                                   CaptionML=[ENU=Subtract Disc. in Purch. Inv.;
                                                              PTB=Subtrair Desc. na N.F. Compra] }
    { 44  ;   ;Allow Correction of Disposal;Boolean;
                                                   CaptionML=[ENU=Allow Correction of Disposal;
                                                              PTB=Permitir Corre��o de Venda/Baixa] }
    { 45  ;   ;Allow more than 360/365 Days;Boolean;
                                                   CaptionML=[ENU=Allow more than 360/365 Days;
                                                              PTB=Permitir mais de 360/365 Dias] }
    { 46  ;   ;VAT on Net Disposal Entries;Boolean;CaptionML=[ENU=VAT on Net Disposal Entries;
                                                              PTB=IVA no Movimento de Baixa L�quido] }
    { 47  ;   ;Allow Acq. Cost below Zero;Boolean ;CaptionML=[ENU=Allow Acq. Cost below Zero;
                                                              PTB=Permitir Acq. Custo abaixo Zero] }
    { 48  ;   ;Allow Identical Document No.;Boolean;
                                                   CaptionML=[ENU=Allow Identical Document No.;
                                                              PTB=Permitir N� Documento Id�ntico] }
    { 49  ;   ;Fiscal Year 365 Days;Boolean       ;OnValidate=BEGIN
                                                                IF "Fiscal Year 365 Days" THEN BEGIN
                                                                  TESTFIELD("Use Custom 1 Depreciation",FALSE);
                                                                  TESTFIELD("Periodic Depr. Date Calc.","Periodic Depr. Date Calc."::"Last Entry");
                                                                END;
                                                                FADeprBook.LOCKTABLE;
                                                                MODIFY;
                                                                FADeprBook.SETCURRENTKEY("Depreciation Book Code","FA No.");
                                                                FADeprBook.SETRANGE("Depreciation Book Code",Code);
                                                                IF FADeprBook.FINDSET(TRUE) THEN
                                                                  REPEAT
                                                                    FADeprBook.CalcDeprPeriod;
                                                                    FADeprBook.MODIFY;
                                                                  UNTIL FADeprBook.NEXT = 0;
                                                              END;

                                                   CaptionML=[ENU=Fiscal Year 365 Days;
                                                              PTB=Ano Fiscal 365 Dias] }
  }
  KEYS
  {
    {    ;Code                                    ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      Text000@1000 : TextConst 'ENU=The book cannot be deleted because it is in use.;PTB=N�o pode eliminar este livro j� que est� sendo usado.';
      Text001@1001 : TextConst 'ENU="The book cannot be deleted because %1 %2 = %3.";PTB="N�o pode eliminar o livro j� que %1 %2 = %3."';
      FASetup@1002 : Record 5603;
      FADeprBook@1003 : Record 5612;
      FAPostingTypeSetup@1004 : Record 5604;
      FAJnlSetup@1005 : Record 5605;
      InsCoverageLedgEntry@1006 : Record 5629;

    PROCEDURE IndexGLIntegration@1(VAR GLIntegration@1000 : ARRAY [9] OF Boolean) : Option;
    BEGIN
      GLIntegration[1] := "G/L Integration - Acq. Cost";
      GLIntegration[2] := "G/L Integration - Depreciation";
      GLIntegration[3] := "G/L Integration - Write-Down";
      GLIntegration[4] := "G/L Integration - Appreciation";
      GLIntegration[5] := "G/L Integration - Custom 1";
      GLIntegration[6] := "G/L Integration - Custom 2";
      GLIntegration[7] := "G/L Integration - Disposal";
      GLIntegration[8] := "G/L Integration - Maintenance";
      GLIntegration[9] := FALSE; // Salvage Value
    END;

    BEGIN
    END.
  }
}

