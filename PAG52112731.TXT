OBJECT Page 52112731 Purchase Tracking
{
  OBJECT-PROPERTIES
  {
    Date=14/09/15;
    Time=14:20:19;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Purchase Tracking;
               PTB=Rastreabilidade Compras];
    LinksAllowed=No;
    SourceTable=Table52112733;
    PageType=List;
    SourceTableTemporary=Yes;
    PromotedActionCategoriesML=[ENU=Documents;
                                PTB=Documentos];
    ActionList=ACTIONS
    {
      { 52006509;    ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 52006510;1   ;Action    ;
                      CaptionML=[ENU=Request;
                                 PTB=Requisi��o];
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=PlanningWorksheet;
                      OnAction=BEGIN
                                 OpenRequest;
                               END;
                                }
      { 52006511;1   ;Action    ;
                      CaptionML=[ENU=Quote;
                                 PTB=Cota��o];
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=Quote;
                      OnAction=BEGIN
                                 OpenQuote;
                               END;
                                }
      { 52006512;1   ;Action    ;
                      CaptionML=[ENU=Order;
                                 PTB=Pedido];
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=Order;
                      OnAction=BEGIN
                                 OpenOrder;
                               END;
                                }
      { 52006520;1   ;Action    ;
                      CaptionML=[ENU=Receipt;
                                 PTB=Recebimento];
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=Receipt;
                      OnAction=BEGIN
                                 OpenReceipt;
                               END;
                                }
      { 52006516;1   ;Action    ;
                      CaptionML=[ENU=Invoice;
                                 PTB=Nota Fiscal];
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=Invoice;
                      OnAction=BEGIN
                                 OpenInvoice;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 52006500;0;Container;
                ContainerType=ContentArea }

    { 52006501;1;Group    ;
                Name=Group;
                GroupType=Repeater }

    { 52006502;2;Field    ;
                SourceExpr="Item Type" }

    { 52006503;2;Field    ;
                SourceExpr="Item No." }

    { 52006504;2;Field    ;
                SourceExpr="Request No.";
                Style=Strong;
                StyleExpr=RequestBold }

    { 52006505;2;Field    ;
                SourceExpr="Request Line No.";
                Style=Strong;
                StyleExpr=RequestBold }

    { 52006506;2;Field    ;
                SourceExpr="Purch Req. Quote No.";
                Style=Strong;
                StyleExpr=QuoteBold }

    { 52006517;2;Field    ;
                SourceExpr="Purch Req. Quote Line No.";
                Style=Strong;
                StyleExpr=QuoteBold }

    { 52006507;2;Field    ;
                SourceExpr="Purch. Order No.";
                Style=Strong;
                StyleExpr=OrderBold }

    { 52006508;2;Field    ;
                SourceExpr="Purch. Order Line No.";
                Style=Strong;
                StyleExpr=OrderBold }

    { 52006518;2;Field    ;
                SourceExpr="Purch. Receipt No.";
                Style=Strong;
                StyleExpr=ReceiptBold }

    { 52006519;2;Field    ;
                SourceExpr="Purch. Receipt Line No.";
                Style=Strong;
                StyleExpr=ReceiptBold }

    { 52006513;2;Field    ;
                SourceExpr="Purch. Invoice Doc. No.";
                Style=Strong;
                StyleExpr=InvoiceBold }

    { 52006514;2;Field    ;
                SourceExpr="Purch. Invoice Line No.";
                Style=Strong;
                StyleExpr=InvoiceBold }

    { 52006515;2;Field    ;
                SourceExpr="Purch. Invoice No.";
                Style=Strong;
                StyleExpr=InvoiceBold }

  }
  CODE
  {
    VAR
      RequestBold@52006500 : Boolean;
      QuoteBold@52006501 : Boolean;
      OrderBold@52006502 : Boolean;
      ReceiptBold@52006504 : Boolean;
      InvoiceBold@52006503 : Boolean;

    PROCEDURE SetRecords@52006500(VAR purchTracking@52006500 : TEMPORARY Record 52112733);
    BEGIN
      RESET;
      DELETEALL;
      IF purchTracking.FINDSET THEN
        REPEAT
          INIT;
          TRANSFERFIELDS(purchTracking);
          INSERT;
        UNTIL purchTracking.NEXT = 0;
    END;

    PROCEDURE SetRequestBold@52006501();
    BEGIN
      RequestBold := TRUE;
    END;

    PROCEDURE SetQuoteBold@52006502();
    BEGIN
      QuoteBold := TRUE;
    END;

    PROCEDURE SetOrderBold@52006503();
    BEGIN
      OrderBold := TRUE;
    END;

    PROCEDURE SetReceiptBold@52006507();
    BEGIN
      ReceiptBold := TRUE;
    END;

    PROCEDURE SetInvoiceBold@52006504();
    BEGIN
      InvoiceBold := TRUE;
    END;

    BEGIN
    END.
  }
}

