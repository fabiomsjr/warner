OBJECT Codeunit 52112463 Cash Flow Adjustments
{
  OBJECT-PROPERTIES
  {
    Date=02/06/15;
    Time=09:39:54;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    OnRun=BEGIN
          END;

  }
  CODE
  {
    VAR
      CashFlowForecastNo@52006501 : Code[20];
      Summarized@52006500 : Boolean;

    PROCEDURE SetParameters@52006500(_CashFlowForecastNo@52006501 : Code[20];_Summarized@52006500 : Boolean);
    BEGIN
      CashFlowForecastNo := _CashFlowForecastNo;
      Summarized := _Summarized;
    END;

    PROCEDURE InsertTempCFWorksheetLineForPurchLine@52006509(VAR cashFlowForecastLineNo@52006501 : Integer;purchLine@52006504 : Record 39;VAR tempCFWorksheetLine@52006500 : TEMPORARY Record 846;VAR cfWorksheetLine@52006502 : Record 846);
    BEGIN
      IF NOT IsFinancialOperation(purchLine."Operation Type") THEN
        EXIT;

      IF (NOT Summarized) AND (purchLine."Expected Receipt Date" <> 0D) THEN BEGIN
        cfWorksheetLine."Document Date" := purchLine."Expected Receipt Date";
        cfWorksheetLine."Cash Flow Date" := cfWorksheetLine."Document Date";
      END;

      InsertLineWithInstallments(cashFlowForecastLineNo, tempCFWorksheetLine, cfWorksheetLine);
    END;

    PROCEDURE InsertTempCFWorksheetLineForSalesLine@52006514(VAR cashFlowForecastLineNo@52006503 : Integer;salesLine@52006504 : Record 37;VAR tempCFWorksheetLine@52006500 : TEMPORARY Record 846;VAR cfWorksheetLine@52006502 : Record 846);
    BEGIN
      IF NOT IsFinancialOperation(salesLine."Operation Type") THEN
        EXIT;

      IF (NOT Summarized) AND (salesLine."Planned Shipment Date" <> 0D) THEN BEGIN
        cfWorksheetLine."Document Date" := salesLine."Planned Shipment Date";
        cfWorksheetLine."Cash Flow Date" := cfWorksheetLine."Document Date";
      END;

      InsertLineWithInstallments(cashFlowForecastLineNo, tempCFWorksheetLine, cfWorksheetLine);
    END;

    PROCEDURE InsertTempCFWorksheetLineForServiceLine@52006506(VAR cashFlowForecastLineNo@52006501 : Integer;serviceLine@52006504 : Record 5902;VAR tempCFWorksheetLine@52006500 : TEMPORARY Record 846;VAR cfWorksheetLine@52006502 : Record 846);
    BEGIN
      IF NOT IsFinancialOperation(serviceLine."Operation Type") THEN
        EXIT;

      IF (NOT Summarized) AND (serviceLine."Planned Delivery Date" <> 0D) THEN BEGIN
        cfWorksheetLine."Document Date" := serviceLine."Planned Delivery Date";
        cfWorksheetLine."Cash Flow Date" := cfWorksheetLine."Document Date";
      END;

      InsertLineWithInstallments(cashFlowForecastLineNo, tempCFWorksheetLine, cfWorksheetLine);
    END;

    LOCAL PROCEDURE InsertLineWithInstallments@52006520(VAR cashFlowForecastLineNo@52006509 : Integer;VAR tempCFWorksheetLine@52006508 : TEMPORARY Record 846;VAR cfWorksheetLine@52006506 : Record 846);
    VAR
      paymTerms@52006500 : Record 3;
      paymentDetail@52006501 : Record 52112434;
      c@52006502 : Integer;
      factor@52006505 : Decimal;
      auxCFWorksheetLine@52006503 : TEMPORARY Record 846;
      diffCFWorksheetLine@52006504 : TEMPORARY Record 846;
    BEGIN
      WITH cfWorksheetLine DO BEGIN
        IF "Payment Terms Code" <> '' THEN BEGIN
          paymTerms.GET("Payment Terms Code");
          paymTerms.CALCFIELDS("Number of Due Dates");
        END;
        IF paymTerms."Number of Due Dates" = 0 THEN BEGIN
          InsertTempCFWorksheetLine(cashFlowForecastLineNo, tempCFWorksheetLine, cfWorksheetLine, 0);
          EXIT;
        END;

        auxCFWorksheetLine.TRANSFERFIELDS(cfWorksheetLine);
        diffCFWorksheetLine.TRANSFERFIELDS(cfWorksheetLine);
        paymentDetail.SETRANGE("Payment Terms Code", paymTerms.Code);
        IF paymentDetail.FINDFIRST THEN
          REPEAT
            paymentDetail.TESTFIELD("Due Period Calc.");
            c += 1;
            factor := paymentDetail."% of the Total" / 100;

            //�ltima parcela sempre utiliza o que sobrou
            IF c = paymentDetail.COUNT THEN BEGIN
              TRANSFERFIELDS(diffCFWorksheetLine);
              factor := 1;
            END;
            "Amount (LCY)" := ROUND("Amount (LCY)" * factor, 0.01);
            diffCFWorksheetLine."Amount (LCY)" -= "Amount (LCY)";

            "Cash Flow Date" := CALCDATE(paymentDetail."Due Period Calc.", auxCFWorksheetLine."Document Date");
            Description := COPYSTR(STRSUBSTNO('Vto:%1-', c) + Description, 1, 50);
            "Payment Terms Code" := '';
            InsertTempCFWorksheetLine(cashFlowForecastLineNo, tempCFWorksheetLine, cfWorksheetLine, 0);
            TRANSFERFIELDS(auxCFWorksheetLine);
          UNTIL paymentDetail.NEXT = 0;
      END;
    END;

    LOCAL PROCEDURE InsertTempCFWorksheetLine@16(VAR cashFlowForecastLineNo@52006501 : Integer;VAR tempCFWorksheetLine@52006500 : TEMPORARY Record 846;VAR cfWorksheetLine@52006502 : Record 846;maxPmtTolerance@1001 : Decimal);
    BEGIN
      WITH tempCFWorksheetLine DO BEGIN
        cashFlowForecastLineNo += 100;

        TRANSFERFIELDS(cfWorksheetLine);
        "Cash Flow Forecast No." := CashFlowForecastNo;
        "Line No." := cashFlowForecastLineNo;

        CalculateCFAmountAndCFDate;

        IF ABS("Amount (LCY)") < ABS(maxPmtTolerance) THEN
          "Amount (LCY)" := 0
        ELSE
          "Amount (LCY)" := "Amount (LCY)" - maxPmtTolerance;

        IF "Amount (LCY)" <> 0 THEN
          INSERT;
      END;
    END;

    LOCAL PROCEDURE IsFinancialOperation@52006501(OperationTypeCode@52006500 : Code[20]) : Boolean;
    VAR
      OperationType@52006501 : Record 52112435;
    BEGIN
      IF OperationTypeCode = '' THEN
        EXIT(TRUE);

      OperationType.GET(OperationTypeCode);
      EXIT(OperationType."Customer/Vendor Entry" = OperationType."Customer/Vendor Entry"::"Amount Including Taxes");
    END;

    BEGIN
    END.
  }
}

