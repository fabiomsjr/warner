OBJECT Page 52112880 ECF Posting Codes
{
  OBJECT-PROPERTIES
  {
    Date=25/05/15;
    Time=15:49:32;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=ECF Posting Codes;
               PTB=C�digos Lan�amento ECF];
    SourceTable=Table52112868;
    DelayedInsert=Yes;
    PageType=List;
    OnAfterGetRecord=BEGIN
                       UpdateStyle;
                     END;

  }
  CONTROLS
  {
    { 52006500;0;Container;
                ContainerType=ContentArea }

    { 52006501;1;Group    ;
                Name=Group;
                GroupType=Repeater }

    { 52006502;2;Field    ;
                SourceExpr=Code;
                StyleExpr=Style }

    { 52006503;2;Field    ;
                SourceExpr=Description;
                StyleExpr=Style }

    { 52006504;2;Field    ;
                SourceExpr=Type;
                StyleExpr=Style;
                OnValidate=BEGIN
                             UpdateStyle;
                           END;
                            }

  }
  CODE
  {
    VAR
      Style@52006500 : Text;

    PROCEDURE UpdateStyle@52006505();
    BEGIN
      IF Type = Type::" " THEN
        Style := 'Strong'
      ELSE
        Style := '';
    END;

    BEGIN
    END.
  }
}

