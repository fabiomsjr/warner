OBJECT Table 329 Currency for Reminder Level
{
  OBJECT-PROPERTIES
  {
    Date=27/11/14;
    Time=12:00:00;
    Version List=NAVW18.00.00.38798;
  }
  PROPERTIES
  {
    DataCaptionFields=Reminder Terms Code,No.;
    CaptionML=[ENU=Currency for Reminder Level;
               PTB=Moeda para n�vel carta aviso];
    LookupPageID=Page478;
    DrillDownPageID=Page478;
  }
  FIELDS
  {
    { 1   ;   ;Reminder Terms Code ;Code10        ;TableRelation="Reminder Terms";
                                                   CaptionML=[ENU=Reminder Terms Code;
                                                              PTB=Cod. Carta Aviso];
                                                   NotBlank=Yes;
                                                   Editable=No }
    { 2   ;   ;No.                 ;Integer       ;CaptionML=[ENU=No.;
                                                              PTB=N�];
                                                   Editable=No }
    { 3   ;   ;Currency Code       ;Code10        ;TableRelation=Currency;
                                                   CaptionML=[ENU=Currency Code;
                                                              PTB=Cod. Moeda];
                                                   NotBlank=Yes }
    { 4   ;   ;Additional Fee      ;Decimal       ;CaptionML=[ENU=Additional Fee;
                                                              PTB=Taxa Adicional];
                                                   MinValue=0;
                                                   AutoFormatType=1;
                                                   AutoFormatExpr="Currency Code" }
    { 5   ;   ;Add. Fee per Line   ;Decimal       ;CaptionML=ENU=Add. Fee per Line;
                                                   MinValue=0 }
  }
  KEYS
  {
    {    ;Reminder Terms Code,No.,Currency Code   ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

