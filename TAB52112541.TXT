OBJECT Table 52112541 EFD Unit of Measure
{
  OBJECT-PROPERTIES
  {
    Date=30/04/13;
    Time=15:27:38;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    Permissions=TableData 52112541=rimd;
    OnDelete=VAR
               USText001@1240470002 : TextConst 'ENU=The Unit of Measure - %1  is attached to a Payroll Rate record and therefore cannot be deleted.;ESM=La unidad de medida - %1  est� asociada a un registro de retribuci�n y no puede eliminarse.;FRC=L''unit� de mesure - %1 est associ�e � un enregistrement de type Taux salarial et ne peut pas �tre supprim�e.;ENC=The Unit of Measure - %1  is attached to a Payroll Rate record and therefore cannot be deleted.';
             BEGIN
             END;

    CaptionML=PTB=Unidade de Medida EFD;
  }
  FIELDS
  {
    { 1   ;   ;Tax Settlement No.  ;Code20         }
    { 2   ;   ;Code                ;Code10        ;CaptionML=[ENU=Code;
                                                              PTB=C�digo;
                                                              ESM=C�digo;
                                                              FRC="Code  ";
                                                              ENC=Code];
                                                   NotBlank=Yes }
    { 3   ;   ;Description         ;Text100       ;CaptionML=[ENU=Description;
                                                              PTB=Descri��o;
                                                              ESM=Descripci�n;
                                                              FRC=Description;
                                                              ENC=Description] }
    { 1000;   ;Company ID          ;Text30         }
  }
  KEYS
  {
    {    ;Tax Settlement No.,Company ID,Code      ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    PROCEDURE Add@1102300001(taxSettlementNo@1102300000 : Code[20];companyID@1102300003 : Text[30];unitCode@1102300001 : Code[10]);
    VAR
      unitOfMeasure@1102300002 : Record 204;
    BEGIN
      IF GET(taxSettlementNo, companyID, unitCode) THEN
        EXIT;

      unitOfMeasure.GET(unitCode);

      INIT;
      "Tax Settlement No." := taxSettlementNo;
      "Company ID" := companyID;
      Code := unitCode;
      Description := unitOfMeasure.Description;

      IF Code = Description THEN //Regra do EFD, n�o podem ser iguais
        Description += '.';

      INSERT;
    END;

    BEGIN
    END.
  }
}

