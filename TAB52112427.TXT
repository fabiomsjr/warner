OBJECT Table 52112427 NCM Code
{
  OBJECT-PROPERTIES
  {
    Date=07/07/14;
    Time=11:34:34;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    DataPerCompany=No;
    OnDelete=VAR
               ncmCodeTaxPerc@52006500 : Record 52112428;
               ncmException@52006501 : Record 52112461;
             BEGIN
               ncmCodeTaxPerc.SETRANGE("NCM Code", Code);
               ncmCodeTaxPerc.DELETEALL;
               ncmException.SETRANGE("NCM Code", Code);
               ncmException.DELETEALL;
             END;

    CaptionML=[ENU=NCM Code;
               PTB=C�digo NCM];
    LookupPageID=Page52112427;
    DrillDownPageID=Page52112427;
  }
  FIELDS
  {
    { 1   ;   ;Code                ;Code20        ;CaptionML=[ENU=Code;
                                                              PTB=C�digo];
                                                   NotBlank=Yes }
    { 2   ;   ;Description         ;Text250       ;CaptionML=[ENU=Description;
                                                              PTB=Descri��o] }
    { 35000700;;Gender             ;Code2         ;CaptionML=[ENU=Gender;
                                                              PTB=G�nero] }
  }
  KEYS
  {
    {    ;Code                                    ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

