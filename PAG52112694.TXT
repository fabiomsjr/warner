OBJECT Page 52112694 Siscoserv RP Card
{
  OBJECT-PROPERTIES
  {
    Date=21/07/15;
    Time=16:29:07;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=RP Card;
               PTB=Cart�o RP];
    SourceTable=Table52112689;
    PageType=Card;
    PromotedActionCategoriesML=[ENU=New,XML;
                                PTB=Novo,XML];
    ActionList=ACTIONS
    {
      { 52006509;    ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 52006510;1   ;Action    ;
                      CaptionML=[ENU=Inclusion;
                                 PTB=Inclus�o];
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=XMLFile;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 ExportIncluirRP;
                               END;
                                }
      { 52006511;1   ;Action    ;
                      CaptionML=[ENU=Correction;
                                 PTB=Cancelamento];
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=XMLFile;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 ExportCancelarRP;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 52006500;0;Container;
                ContainerType=ContentArea }

    { 52006501;1;Group    ;
                Name=Geral;
                CaptionML=[ENU=General;
                           PTB=Geral];
                GroupType=Group }

    { 52006502;2;Field    ;
                SourceExpr="No.";
                OnAssistEdit=BEGIN
                               IF AssistEdit(xRec) THEN
                                 CurrPage.UPDATE;
                             END;
                              }

    { 52006513;2;Field    ;
                SourceExpr=Description }

    { 52006512;2;Field    ;
                SourceExpr="RAS No.";
                OnValidate=BEGIN
                             CALCFIELDS("Vendor Name");
                           END;
                            }

    { 52006507;2;Field    ;
                SourceExpr="Vendor No.";
                Editable=FALSE }

    { 52006506;2;Field    ;
                SourceExpr="Vendor Name" }

    { 52006505;2;Field    ;
                SourceExpr="Document No." }

    { 52006504;2;Field    ;
                SourceExpr="Invoice No." }

    { 52006503;2;Field    ;
                SourceExpr="Posting Date" }

    { 52006514;1;Part     ;
                SubPageLink=RP No.=FIELD(No.);
                PagePartID=Page52112695;
                PartType=Page }

    { 52006508;1;Part     ;
                SubPageLink=RP No.=FIELD(No.);
                PagePartID=Page52112696;
                PartType=Page }

  }
  CODE
  {

    BEGIN
    END.
  }
}

