OBJECT Codeunit 1022 Job Jnl.-Post+Print
{
  OBJECT-PROPERTIES
  {
    Date=07/09/12;
    Time=12:00:00;
    Version List=NAVW17.00;
  }
  PROPERTIES
  {
    TableNo=210;
    OnRun=BEGIN
            JobJnlLine.COPY(Rec);
            Code;
            COPY(JobJnlLine);
          END;

  }
  CODE
  {
    VAR
      Text000@1000 : TextConst 'ENU=cannot be filtered when posting recurring journals.;PTB=N�o pode ser filtrado enquanto registra di�rio peri�dico.';
      Text001@1001 : TextConst 'ENU=Do you want to post the journal lines and print the posting report?;PTB=Confirma que deseja registrar os di�rios e imprimir o relat�rio de verifica��o?';
      Text002@1002 : TextConst 'ENU=There is nothing to post.;PTB=N�o h� nada para registrar.';
      Text003@1003 : TextConst 'ENU=The journal lines were successfully posted.;PTB=O registro das linhas do di�rio foi correto.';
      Text004@1004 : TextConst 'ENU="The journal lines were successfully posted. ";PTB=As linhas de di�rio foram registradas com sucesso.';
      Text005@1005 : TextConst 'ENU=You are now in the %1 journal.;PTB=Encontra-se no di�rio %1.';
      JobJnlTemplate@1006 : Record 209;
      JobJnlLine@1007 : Record 210;
      JobReg@1008 : Record 241;
      JobJnlPostbatch@1009 : Codeunit 1013;
      TempJnlBatchName@1010 : Code[10];

    LOCAL PROCEDURE Code@1();
    BEGIN
      WITH JobJnlLine DO BEGIN
        JobJnlTemplate.GET("Journal Template Name");
        JobJnlTemplate.TESTFIELD("Posting Report ID");
        IF JobJnlTemplate.Recurring AND (GETFILTER("Posting Date") <> '') THEN
          FIELDERROR("Posting Date",Text000);

        IF NOT CONFIRM(Text001) THEN
          EXIT;

        TempJnlBatchName := "Journal Batch Name";

        JobJnlPostbatch.RUN(JobJnlLine);

        IF JobReg.GET("Line No.") THEN BEGIN
          JobReg.SETRECFILTER;
          REPORT.RUN(JobJnlTemplate."Posting Report ID",FALSE,FALSE,JobReg);
        END;

        IF "Line No." = 0 THEN
          MESSAGE(Text002)
        ELSE
          IF TempJnlBatchName = "Journal Batch Name" THEN
            MESSAGE(Text003)
          ELSE
            MESSAGE(
              Text004 +
              Text005,
              "Journal Batch Name");

        IF NOT FIND('=><') OR (TempJnlBatchName <> "Journal Batch Name") THEN BEGIN
          RESET;
          FILTERGROUP(2);
          SETRANGE("Journal Template Name","Journal Template Name");
          SETRANGE("Journal Batch Name","Journal Batch Name");
          FILTERGROUP(0);
          "Line No." := 1;
        END;
      END;
    END;

    BEGIN
    END.
  }
}

