OBJECT Table 35000678 EFD PC F600/F7002
{
  OBJECT-PROPERTIES
  {
    Date=19/06/12;
    Time=14:41:58;
    Version List=FNX055.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Contributions Deductions Div.;
               PTB=Contribui��o Dedu��o Diversas];
  }
  FIELDS
  {
    { 1   ;   ;Tax Settlement No.  ;Code20        ;CaptionML=PTB=Id. }
    { 2   ;   ;User ID             ;Code20        ;CaptionML=PTB=ID Usuario }
    { 3   ;   ;Branch Code         ;Code20        ;CaptionML=PTB=Codigo Filial }
    { 4   ;   ;Indicator Nature Retention;Option  ;OnValidate=BEGIN
                                                                IF "Indicator Nature Retention" <> "Indicator Nature Retention"::" " THEN
                                                                "Indicator Origin Deduction":="Indicator Origin Deduction"::" ";
                                                              END;

                                                   CaptionML=PTB=Natureza Reten��o;
                                                   OptionCaptionML=[ENU=" ,01 - Retencao por Orgaos; Autarquias e Fundacoes Federais,02 - Retencao por outras Entidades da Administracao Publica Federal,03 - Retencao por Pessoas Juridicas de Direito Privado,04 - Recolhimento por Sociedade Cooperativa,05 - Retencao por Fabricante de Maquinas e Veiculos,99 - Outras Retencoes";
                                                                    PTB=" ,01 - Reten��o por Org�os; Autarquias e Funda��es Federais,02 - Reten��o por outras Entidades da Administra��o P�blica Federal,03 - Reten��o por Pessoas Jur�dicas de Direito Privado,04 - Recolhimento por Sociedade Cooperativa,05 - Reten��o por Fabricante de M�quinas e Ve�culos,99 - Outras Reten��es"];
                                                   OptionString=[ ,01,02,03,04,05,99] }
    { 5   ;   ;Retention Date      ;Date          ;CaptionML=PTB=Data }
    { 6   ;   ;Basis Amount        ;Decimal       ;CaptionML=PTB=Valor Base }
    { 7   ;   ;Retention Amount    ;Decimal       ;CaptionML=PTB=Valor Reten��o }
    { 8   ;   ;Retention Code      ;Code10        ;CaptionML=PTB=C�d. Receita }
    { 9   ;   ;Income Nature       ;Option        ;CaptionML=PTB=Natureza Receita;
                                                   OptionCaptionML=[ENU=0 - Receita de Natureza/Deducao Nao Cumulativa,1 - Receita de Natureza/Deducao Cumulativa;
                                                                    PTB=0 - Receita de Natureza/Dedu��o N�o Cumulativa,1 - Receita de Natureza/Dedu��o Cumulativa];
                                                   OptionString=0,1 }
    { 10  ;   ;CNPJ                ;Code20        ;CaptionML=PTB=CNPJ }
    { 11  ;   ;PIS Amount          ;Decimal       ;CaptionML=PTB=Valor PIS }
    { 12  ;   ;COFINS Amount       ;Decimal       ;CaptionML=PTB=Valor COFINS }
    { 13  ;   ;Condition Indicator ;Option        ;CaptionML=PTB=Condi��o Declarante;
                                                   OptionCaptionML=[ENU=0 - Beneficiaria da Retencao / Recolhimento,1 - Responsavel pela Retencao / Recolhimento;
                                                                    PTB=0 - Benefici�ria da Reten��o / Recolhimento,1 - Responsavel pela Reten��o / Recolhimento];
                                                   OptionString=0,1 }
    { 14  ;   ;Indicator Origin Deduction;Option  ;OnValidate=BEGIN
                                                                IF "Indicator Origin Deduction"<>"Indicator Origin Deduction"::" " THEN
                                                                "Indicator Nature Retention" := "Indicator Nature Retention"::" ";
                                                              END;

                                                   CaptionML=PTB=Origem Dedu��o;
                                                   OptionCaptionML=[ENU=" ,01 - Credito Presumido - Medicamentos,02 - Creditos Admitidos no Regime Cumulativo - Bebidas Frias,03 - Contribuicao Paga pelo Substituido Tributario - ZFM,04 - Substituicao Tribut�ria - Nao Ocorrencia do Fato Gerador Presumido,99 - Outras Deducoes";
                                                                    PTB=" ,01 - Cr�dito Presumido - Medicamentos,02 - Cr�ditos Admitidos no Regime Cumulativo - Bebidas Frias,03 - Contribui��o Paga pelo Substituido Tribut�rio - ZFM,04 - Substitui��o Tribut�ria - N�o Ocorr�ncia do Fato Gerador Presumido,99 - Outras Dedu��es"];
                                                   OptionString=[ ,01,02,03,04,99] }
    { 19  ;   ;Additional Information;Text90      ;CaptionML=PTB=Info. Complementares }
    { 1000;   ;Type                ;Option        ;OptionString=F600,F700 }
  }
  KEYS
  {
    {    ;Tax Settlement No.,Type,Indicator Nature Retention,Indicator Origin Deduction,CNPJ,Income Nature,Retention Date;
                                                   Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

