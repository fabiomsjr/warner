OBJECT Table 52112493 Third-Party Stock Entry
{
  OBJECT-PROPERTIES
  {
    Date=23/07/15;
    Time=16:48:48;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Third-Party Stock Entry;
               PTB=Mov. Produto Estoque Terceiros];
    LookupPageID=Page52112494;
    DrillDownPageID=Page52112494;
  }
  FIELDS
  {
    { 10  ;   ;Entry No.           ;Integer       ;CaptionML=[ENU=Entry No.;
                                                              PTB=N� Mov.] }
    { 20  ;   ;Item No.            ;Code20        ;TableRelation=Item;
                                                   CaptionML=[ENU=Item No.;
                                                              PTB=N� Produto] }
    { 30  ;   ;Inbound Item Ledger Entry No.;Integer;
                                                   TableRelation="Item Ledger Entry";
                                                   CaptionML=[ENU=Inbound Item Ledger Entry No.;
                                                              PTB=N� Mov. Produto Entrada] }
    { 32  ;   ;Outbound Item Ledger Entry No.;Integer;
                                                   TableRelation="Item Ledger Entry";
                                                   CaptionML=[ENU=Outbound Item Ledger Entry No.;
                                                              PTB=N� Mov. Produto Sa�da] }
    { 34  ;   ;Source Item Ledger Entry No.;Integer;
                                                   TableRelation="Item Ledger Entry";
                                                   CaptionML=[ENU=Source Item Ledger Entry No.;
                                                              PTB=N� Mov. Produto Origem] }
    { 40  ;   ;Posting Date        ;Date          ;CaptionML=[ENU=Posting Date;
                                                              PTB=Data Registro] }
    { 50  ;   ;Document No.        ;Code20        ;CaptionML=[ENU=Document No.;
                                                              PTB=N� Documento] }
    { 58  ;   ;Gen. Prod. Posting Group;Code10    ;TableRelation="Gen. Product Posting Group";
                                                   CaptionML=[ENU=Gen. Prod. Posting Group;
                                                              PTB=Gr. Cont�bil Produto] }
    { 60  ;   ;Description         ;Text50        ;CaptionML=[ENU=Description;
                                                              PTB=Descri��o] }
    { 61  ;   ;External Document No.;Code35       ;CaptionML=[ENU=External Document No.;
                                                              PTB=N� Documento Externo] }
    { 64  ;   ;No. Series          ;Code10        ;TableRelation="No. Series";
                                                   CaptionML=[ENU=No. Series;
                                                              PTB=N� S�ries] }
    { 70  ;   ;Quantity            ;Decimal       ;CaptionML=[ENU=Quantity;
                                                              PTB=Quantidade];
                                                   DecimalPlaces=0:5 }
    { 72  ;   ;Remaining Quantity  ;Decimal       ;CaptionML=[ENU=Remaining Quantity;
                                                              PTB=Quantidade Pendente];
                                                   DecimalPlaces=0:5 }
    { 80  ;   ;Location Code       ;Code10        ;TableRelation=Location;
                                                   CaptionML=[ENU=Location Code;
                                                              PTB=C�d. Dep�sito] }
    { 100 ;   ;Document Date       ;Date          ;CaptionML=[ENU=Document Date;
                                                              PTB=Data Documento] }
    { 200 ;   ;Cost to Post        ;Decimal       ;CaptionML=[ENU=Cost to Post;
                                                              PTB=Custo a Registrar] }
    { 210 ;   ;Posted              ;Boolean       ;CaptionML=[ENU=Posted;
                                                              PTB=Registrado] }
    { 6500;   ;Serial No.          ;Code20        ;CaptionML=[ENU=Serial No.;
                                                              PTB=N� S�rie] }
    { 6501;   ;Lot No.             ;Code20        ;CaptionML=[ENU=Lot No.;
                                                              PTB=N� Lote] }
  }
  KEYS
  {
    {    ;Entry No.                               ;Clustered=Yes }
    {    ;Item No.                                 }
    {    ;Item No.,Posting Date                    }
    {    ;Lot No.                                  }
    {    ;Serial No.                               }
    {    ;Source Item Ledger Entry No.             }
    {    ;Posted                                   }
    {    ;Location Code,Source Item Ledger Entry No.,Outbound Item Ledger Entry No. }
    {    ;Item No.,Serial No.,Lot No.,Location Code,Remaining Quantity }
  }
  FIELDGROUPS
  {
    { 1   ;DropDown            ;Field1,Field7,Field2,Field3,Field4,Field6 }
  }
  CODE
  {

    BEGIN
    END.
  }
}

