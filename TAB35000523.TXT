OBJECT Table 35000523 SPED Accounting Mov. Header2
{
  OBJECT-PROPERTIES
  {
    Date=29/04/15;
    Time=12:44:10;
    Modified=Yes;
    Version List=FNX056.00,WAR004.00;
  }
  PROPERTIES
  {
    OnInsert=BEGIN
               IF "No." = '' THEN BEGIN
                 ConfigSPED.GET;
                 ConfigSPED.TESTFIELD("Sped Mov Nos.");
                 NoSeriesMgt.InitSeries(ConfigSPED."Sped Mov Nos.",xRec."Series No.",0D,"No.","Series No.");
               END;
             END;

    CaptionML=[ENU=SPED Accounting Mov. Header;
               PTB=Mov. SPED Cont�bil Header];
    LookupPageID=Page35000552;
    DrillDownPageID=Page35000552;
  }
  FIELDS
  {
    { 1   ;   ;No.                 ;Code20        ;CaptionML=[ENU=No.;
                                                              PTB=N�] }
    { 2   ;   ;Begin Date          ;Date          ;CaptionML=[ENU=Begin Date;
                                                              PTB=Data Inicio] }
    { 3   ;   ;End Date            ;Date          ;CaptionML=[ENU=End Date;
                                                              PTB=Data Fim] }
    { 6   ;   ;Ident. Type Bookkeeping;Option     ;OnValidate=BEGIN
                                                                IdEscritCont�bil.RESET;
                                                                IdEscritCont�bil.SETRANGE("Bookkeeping Ident. Type","Ident. Type Bookkeeping");
                                                                IdEscritCont�bil.FIND('-');
                                                                IdEscritCont�bil.TESTFIELD("Book Nature");
                                                                "Nature Book - Purpose":=IdEscritCont�bil."Book Nature";

                                                                MovSPEDCtbHeader.RESET;
                                                                MovSPEDCtbHeader.SETCURRENTKEY("Ident. Type Bookkeeping","Instrument Order No");
                                                                MovSPEDCtbHeader.SETRANGE("Ident. Type Bookkeeping","Ident. Type Bookkeeping");
                                                                MovSPEDCtbHeader.SETFILTER("No.",'<>%1',"No.");
                                                                IF NOT MovSPEDCtbHeader.FIND('+') THEN
                                                                  MovSPEDCtbHeader."Instrument Order No":=0;
                                                                "Instrument Order No":=MovSPEDCtbHeader."Instrument Order No"+1;
                                                              END;

                                                   CaptionML=[ENU=Ident. Type Bookkeeping;
                                                              PTB=Tipo Ident. Escritura��o];
                                                   OptionCaptionML=[ENU=" ,G - Diary Book,R - Diary Bookkeeping Brief,A - Aux. Diary Book,B - Balance Sheet Paper,Z - Subsidiary Ledger";
                                                                    PTB=" ,G - Livro Di�rio,R - Livro Di�rio Escrit. Resumida,A - Livro Di�rio Aux.,B - Livro Balancete,Z - Raz�o Auxiliar"];
                                                   OptionString=[ ,G - Livro Di�rio,R - Livro Di�rio Escrit. Resumida,A - Livro Di�rio Aux.,B - Livro Balancete,Z - Raz�o Auxiliar] }
    { 7   ;   ;Series No.          ;Code10        ;TableRelation="No. Series";
                                                   CaptionML=[ENU=Series No.;
                                                              PTB=N� S�ries];
                                                   Editable=No }
    { 8   ;   ;Instr. Bookkeeping Order No.;Integer;
                                                   CaptionML=[ENU=Instrument Bookkeeping Order No.;
                                                              PTB=N� Ordem Instrum. Escritura��o] }
    { 9   ;   ;Nature Book - Purpose;Text80       ;CaptionML=[ENU=Nature Book - Purpose;
                                                              PTB=Natureza Livro - Finalidade] }
    { 10  ;   ;Instrument Order No ;Integer       ;CaptionML=[ENU=Instrument Order No;
                                                              PTB=N� Ordem do Instrumento] }
    { 12  ;   ;Receipt No. Offices. Previous;Text41;
                                                   CaptionML=PTB=No. Recibo Escrit. Anterior }
    { 13  ;   ;Time Indication     ;Option        ;CaptionML=PTB=Indica��o Per�odo;
                                                   OptionString=[ ,Anual,Primeiro Trimestre,Segundo Trimestre,Terceiro Trimestre,Quarto Trimestre] }
    { 17  ;   ;Include End Releases;Boolean       ;CaptionML=[ENU=Include End Releases;
                                                              PTB=Incluir Lan�amentos Fechamento] }
    { 18  ;   ;FCont Process       ;Boolean        }
    { 19  ;   ;Type Bookkeeping    ;Option        ;CaptionML=PTB=Tipo Escritura��o;
                                                   OptionCaptionML=PTB=0-Original,1-Retificadora;
                                                   OptionString=0-Original,1-Retificadora }
    { 30  ;   ;Period Start Indicator;Option      ;CaptionML=PTB=Indicador In�cio Per�odo;
                                                   OptionCaptionML=PTB=Primeiro dia do ano,Abertura,Resultante de cis�o/fus�o,In�cio da obrigatoriedade;
                                                   OptionString=PrimeiroDiaAno,Abertura,ResultanteCisaoFusao,InicioObrigatoriedade }
    { 40  ;   ;Tributation Method  ;Option        ;CaptionML=PTB=Forma Tributa��o;
                                                   OptionCaptionML=PTB=" ,Real,Real Arbitrado,Real Presumido (Trimestral),Real Presumido Arbitrado (Trimestral)";
                                                   OptionString=[ ,1,2,3,4] }
    { 50  ;   ;Special Status Indicator;Code10    ;TableRelation="Special Indicator Situation.";
                                                   CaptionML=[ENU=Special Status Indicator;
                                                              PTB=Indicador Situa��o Especial] }
    { 60  ;   ;Group Documents By  ;Option        ;CaptionML=[ENU=Group Documents By;
                                                              PTB=Agrupar Documentos Por];
                                                   OptionCaptionML=[ENU=Document,Day;
                                                                    PTB=Documento,Dia];
                                                   OptionString=Document,Day }
    { 70  ;   ;ECD Closing By      ;Option        ;CaptionML=[ENU=Closing By;
                                                              PTB=Fechamento Por];
                                                   OptionCaptionML=[ENU=Year,Quarter;
                                                                    PTB=Ano,Trimestre];
                                                   OptionString=Year,Quarter }
    { 1012;   ;IND_SIT_INI_PER     ;Option        ;CaptionML=[ENU=Situation at Period Start;
                                                              PTB=Situa��o no In�cio do Per�odo];
                                                   OptionCaptionML=[ENU=Normal,Opening,Result of Fusion,Start of obligation during fiscal period;
                                                                    PTB=Normal,Abertura,Resultante de cis�o/fus�o ou remanescente de cis�o ou realizou incorpora��o,In�cio de obrigatoriedade no curso do ano calend�rio];
                                                   OptionString=Regular,Opening,FusionIncorp,BeginDuringPeriod }
    { 1014;   ;IND_FIN_ESC         ;Option        ;CaptionML=[ENU=Bookkeeping Purpose;
                                                              PTB=Finalidade Escritura��o];
                                                   OptionCaptionML=[ENU=Original,Replacement with NIRE,Replacement without NIRE,Replacement with NIRE Change;
                                                                    PTB=Original,Substituta com NIRE,Substituta sem NIRE,Substituta com troca de NIRE];
                                                   OptionString=Original,SubstWithNIRE,SubstWithoutNIRE,SubstWithNIREUpdate }
    { 1015;   ;COD_HASH_SUB        ;Text40        ;CaptionML=[ENU=Replace Bookkeeping Hash;
                                                              PTB=Hash Escritura��o Substitu�da] }
    { 1016;   ;NIRE_SUBST          ;Text11        ;CaptionML=[ENU=Replaced Bookkeeping NIRE;
                                                              PTB=NIRE da Escritura��o Subst.] }
    { 1017;   ;IND_EMP_GRD_PRT     ;Boolean       ;CaptionML=[ENU=Large Company;
                                                              PTB=Empresa Grande Porte] }
    { 2012;   ;DT_EX_SOCIAL        ;Date          ;CaptionML=[ENU=Fiscal Year Closing Date (opcional);
                                                              PTB=Data de Encerramento do Exerc�cio Social] }
    { 2013;   ;NOME_AUDITOR        ;Text50        ;CaptionML=[ENU=Independent Auditor Name;
                                                              PTB=Nome Auditor Independente] }
    { 2014;   ;COD_CVM_AUDITOR     ;Text30        ;CaptionML=[ENU=Independent Auditor CVM No.;
                                                              PTB="Registro Auditor Independente na CVM "] }
  }
  KEYS
  {
    {    ;No.                                     ;Clustered=Yes }
    {    ;Ident. Type Bookkeeping,Instrument Order No }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      NoSeriesMgt@1102300000 : Codeunit 396;
      ConfigSPED@1102300001 : Record 35000500;
      IdEscritCont�bil@1102300002 : Record 35000502;
      MovSPEDCtbHeader@1102300003 : Record 35000523;

    PROCEDURE AssistEdit@2(OldMovSped@1000 : Record 35000523) : Boolean;
    VAR
      MovSped@1001 : Record 35000523;
    BEGIN
      WITH MovSped DO BEGIN
        MovSped := Rec;
        ConfigSPED.GET;
        ConfigSPED.TESTFIELD("Sped Mov Nos.");
        IF NoSeriesMgt.SelectSeries(ConfigSPED."Sped Mov Nos.",OldMovSped."Series No.","Series No.") THEN BEGIN
          NoSeriesMgt.SetSeries("No.");
          Rec := MovSped;
          EXIT(TRUE);
        END;
      END;
    END;

    BEGIN
    END.
  }
}

