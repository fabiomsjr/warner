OBJECT Page 52113080 Approval Flow Rules
{
  OBJECT-PROPERTIES
  {
    Date=04/03/15;
    Time=08:49:38;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Approval Flow Rules;
               PTB=Regras Fluxo Aprova��o];
    SourceTable=Table52113077;
    PageType=List;
    CardPageID=Approval Flow Rule Card;
  }
  CONTROLS
  {
    { 52006500;0;Container;
                ContainerType=ContentArea }

    { 52006501;1;Group    ;
                Name=Group;
                GroupType=Repeater }

    { 52006502;2;Field    ;
                SourceExpr=Code }

    { 52006503;2;Field    ;
                SourceExpr=Description }

    { 52006504;2;Field    ;
                SourceExpr="Minimum No. of Approvals" }

  }
  CODE
  {

    BEGIN
    END.
  }
}

