OBJECT Page 96 Sales Cr. Memo Subform
{
  OBJECT-PROPERTIES
  {
    Date=30/03/16;
    Time=09:57:17;
    Version List=NAVW18.00.00.40262,NAVBR8;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Lines;
               PTB=Linhas];
    MultipleNewLines=Yes;
    LinksAllowed=No;
    SourceTable=Table37;
    DelayedInsert=Yes;
    SourceTableView=WHERE(Document Type=FILTER(Credit Memo));
    PageType=ListPart;
    AutoSplitKey=Yes;
    OnAfterGetRecord=BEGIN
                       ShowShortcutDimCode(ShortcutDimCode);
                     END;

    OnNewRecord=BEGIN
                  InitType;
                  CLEAR(ShortcutDimCode);
                END;

    OnDeleteRecord=VAR
                     ReserveSalesLine@1000 : Codeunit 99000832;
                   BEGIN
                     IF (Quantity <> 0) AND ItemExists("No.") THEN BEGIN
                       COMMIT;
                       IF NOT ReserveSalesLine.DeleteLineConfirm(Rec) THEN
                         EXIT(FALSE);
                       ReserveSalesLine.DeleteLine(Rec);
                     END;
                   END;

    OnAfterGetCurrRecord=BEGIN
                           IF SalesHeader.GET("Document Type","Document No.") THEN;

                           DocumentTotals.SalesUpdateTotalsControls(Rec,TotalSalesHeader,TotalSalesLine,RefreshMessageEnabled,
                             TotalAmountStyle,RefreshMessageText,InvDiscAmountEditable,VATAmount);

                           TypeChosen := HasTypeToFillMandatotyFields;
                         END;

    ActionList=ACTIONS
    {
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 1906587504;1 ;ActionGroup;
                      CaptionML=[ENU=F&unctions;
                                 PTB=F&un��es];
                      Image=Action }
      { 1904522204;2 ;Action    ;
                      AccessByPermission=TableData 90=R;
                      CaptionML=[ENU=E&xplode BOM;
                                 PTB=E&xplodir L.M.];
                      Image=ExplodeBOM;
                      OnAction=BEGIN
                                 ExplodeBOM;
                               END;
                                }
      { 1902056104;2 ;Action    ;
                      AccessByPermission=TableData 279=R;
                      CaptionML=[ENU=Insert &Ext. Texts;
                                 PTB=Inserir Textos &Extendidos];
                      Image=Text;
                      OnAction=BEGIN
                                 InsertExtendedText(TRUE);
                               END;
                                }
      { 1901991804;2 ;Action    ;
                      AccessByPermission=TableData 6660=R;
                      Ellipsis=Yes;
                      CaptionML=[ENU=Get Return &Receipt Lines;
                                 PTB=Buscar Linhas Receb. Devolu��o];
                      Image=ReturnReceipt;
                      OnAction=BEGIN
                                 GetReturnReceipt;
                               END;
                                }
      { 52006510;2   ;Action    ;
                      Name=ActionAssignDocCharges;
                      Ellipsis=Yes;
                      CaptionML=[ENU=Assign Document Charges;
                                 PTB=Atribuir Encargos Documento];
                      Image=ItemCosts;
                      OnAction=BEGIN
                                 ActionAssignDocCharges;
                               END;
                                }
      { 1907935204;1 ;ActionGroup;
                      CaptionML=[ENU=&Line;
                                 PTB=&Linha];
                      Image=Line }
      { 1907981204;2 ;ActionGroup;
                      CaptionML=[ENU=Item Availability by;
                                 PTB=Disponibilidade de Produto Por];
                      Image=ItemAvailability }
      { 5       ;3   ;Action    ;
                      CaptionML=[ENU=Event;
                                 PTB=Evento];
                      Image=Event;
                      OnAction=BEGIN
                                 ItemAvailFormsMgt.ShowItemAvailFromSalesLine(Rec,ItemAvailFormsMgt.ByEvent)
                               END;
                                }
      { 1903587104;3 ;Action    ;
                      CaptionML=[ENU=Period;
                                 PTB=Per�odo];
                      Image=Period;
                      OnAction=BEGIN
                                 ItemAvailFormsMgt.ShowItemAvailFromSalesLine(Rec,ItemAvailFormsMgt.ByPeriod)
                               END;
                                }
      { 1903193004;3 ;Action    ;
                      CaptionML=[ENU=Variant;
                                 PTB=Variante];
                      Image=ItemVariant;
                      OnAction=BEGIN
                                 ItemAvailFormsMgt.ShowItemAvailFromSalesLine(Rec,ItemAvailFormsMgt.ByVariant)
                               END;
                                }
      { 1901743104;3 ;Action    ;
                      AccessByPermission=TableData 14=R;
                      CaptionML=[ENU=Location;
                                 PTB=Dep�sito];
                      Image=Warehouse;
                      OnAction=BEGIN
                                 ItemAvailFormsMgt.ShowItemAvailFromSalesLine(Rec,ItemAvailFormsMgt.ByLocation)
                               END;
                                }
      { 7       ;3   ;Action    ;
                      CaptionML=[ENU=BOM Level;
                                 PTB=N�vel L.M.];
                      Image=BOMLevel;
                      OnAction=BEGIN
                                 ItemAvailFormsMgt.ShowItemAvailFromSalesLine(Rec,ItemAvailFormsMgt.ByBOM)
                               END;
                                }
      { 1902740304;2 ;Action    ;
                      AccessByPermission=TableData 348=R;
                      ShortCutKey=Shift+Ctrl+D;
                      CaptionML=[ENU=Dimensions;
                                 PTB=Dimens�es];
                      Image=Dimensions;
                      OnAction=BEGIN
                                 ShowDimensions;
                               END;
                                }
      { 52006506;2   ;Action    ;
                      Name=DimAlloc;
                      ShortCutKey=Shift+Ctrl+R;
                      CaptionML=[ENU=Dimensions Allocation;
                                 PTB=Rateio Dimens�es];
                      Image=Allocate;
                      OnAction=VAR
                                 salesMgt@52006501 : Codeunit 52112432;
                               BEGIN
                                 salesMgt.EditDimensionsAllocation(Rec);
                               END;
                                }
      { 1907838004;2 ;Action    ;
                      CaptionML=[ENU=Co&mments;
                                 PTB=Co&ment�rios];
                      Image=ViewComments;
                      OnAction=BEGIN
                                 ShowLineComments;
                               END;
                                }
      { 1907184504;2 ;Action    ;
                      AccessByPermission=TableData 5800=R;
                      CaptionML=[ENU=Item Charge &Assignment;
                                 PTB=&Atribui��o Encargo];
                      OnAction=BEGIN
                                 ItemChargeAssgnt;
                               END;
                                }
      { 1905987604;2 ;Action    ;
                      Name=ItemTrackingLines;
                      ShortCutKey=Shift+Ctrl+I;
                      CaptionML=[ENU=Item &Tracking Lines;
                                 PTB=Linhas Ras&treabilidade Produto];
                      Image=ItemTrackingLines;
                      OnAction=BEGIN
                                 OpenItemTrackingLines;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                SourceExpr=Type;
                OnValidate=BEGIN
                             NoOnAfterValidate;
                             TypeChosen := HasTypeToFillMandatotyFields;

                             IF xRec."No." <> '' THEN
                               RedistributeTotalsOnAfterValidate;
                           END;
                            }

    { 4   ;2   ;Field     ;
                SourceExpr="No.";
                OnValidate=BEGIN
                             ShowShortcutDimCode(ShortcutDimCode);
                             NoOnAfterValidate;

                             IF xRec."No." <> '' THEN
                               RedistributeTotalsOnAfterValidate;
                           END;

                ShowMandatory=TypeChosen }

    { 34  ;2   ;Field     ;
                SourceExpr="Cross-Reference No.";
                Visible=FALSE;
                OnValidate=BEGIN
                             CrossReferenceNoOnAfterValidat;
                             NoOnAfterValidate;
                           END;

                OnLookup=BEGIN
                           CrossReferenceNoLookUp;
                           InsertExtendedText(FALSE);
                           NoOnAfterValidate;
                         END;
                          }

    { 46  ;2   ;Field     ;
                SourceExpr="IC Partner Code";
                Visible=FALSE }

    { 40  ;2   ;Field     ;
                SourceExpr="IC Partner Ref. Type";
                Visible=FALSE }

    { 42  ;2   ;Field     ;
                SourceExpr="IC Partner Reference";
                Visible=FALSE }

    { 18  ;2   ;Field     ;
                SourceExpr="Variant Code";
                Visible=FALSE }

    { 38  ;2   ;Field     ;
                SourceExpr=Nonstock;
                Visible=FALSE }

    { 58  ;2   ;Field     ;
                SourceExpr="VAT Prod. Posting Group";
                Visible=FALSE;
                OnValidate=BEGIN
                             RedistributeTotalsOnAfterValidate;
                           END;
                            }

    { 6   ;2   ;Field     ;
                SourceExpr=Description }

    { 35000001;2;Field    ;
                SourceExpr="Description 2";
                Visible=FALSE }

    { 24  ;2   ;Field     ;
                SourceExpr="Return Reason Code";
                Visible=FALSE }

    { 72  ;2   ;Field     ;
                SourceExpr="Location Code";
                Visible=TRUE }

    { 50  ;2   ;Field     ;
                SourceExpr="Bin Code";
                Visible=FALSE }

    { 28  ;2   ;Field     ;
                SourceExpr=Reserve;
                Visible=FALSE;
                OnValidate=BEGIN
                             ReserveOnAfterValidate;
                           END;
                            }

    { 8   ;2   ;Field     ;
                BlankZero=Yes;
                SourceExpr=Quantity;
                OnValidate=BEGIN
                             QuantityOnAfterValidate;
                             RedistributeTotalsOnAfterValidate;
                           END;

                ShowMandatory=TypeChosen }

    { 26  ;2   ;Field     ;
                BlankZero=Yes;
                SourceExpr="Reserved Quantity";
                Visible=FALSE;
                OnDrillDown=BEGIN
                              CurrPage.SAVERECORD;
                              COMMIT;
                              ShowReservationEntries(TRUE);
                              UpdateForm(TRUE);
                            END;
                             }

    { 22  ;2   ;Field     ;
                SourceExpr="Unit of Measure Code";
                OnValidate=BEGIN
                             UnitofMeasureCodeOnAfterValida;
                             RedistributeTotalsOnAfterValidate;
                           END;
                            }

    { 10  ;2   ;Field     ;
                SourceExpr="Unit of Measure";
                Visible=FALSE }

    { 70  ;2   ;Field     ;
                SourceExpr="Unit Cost (LCY)";
                Visible=FALSE }

    { 12  ;2   ;Field     ;
                BlankZero=Yes;
                SourceExpr="Unit Price";
                OnValidate=BEGIN
                             RedistributeTotalsOnAfterValidate;
                           END;

                ShowMandatory=TypeChosen }

    { 92  ;2   ;Field     ;
                SourceExpr="Line Amount";
                OnValidate=BEGIN
                             RedistributeTotalsOnAfterValidate;
                           END;
                            }

    { 52006508;2;Field    ;
                SourceExpr="Operation Type" }

    { 1102300007;2;Field  ;
                SourceExpr="Tax Area Code" }

    { 1102300005;2;Field  ;
                SourceExpr="CFOP Code" }

    { 1102300004;2;Field  ;
                SourceExpr="NCM Code" }

    { 52006500;2;Field    ;
                SourceExpr="NCM Exception Code";
                Visible=FALSE }

    { 52006504;2;Field    ;
                SourceExpr="Tax Exception Code" }

    { 52006507;2;Field    ;
                SourceExpr="End User";
                Visible=FALSE }

    { 52006501;2;Field    ;
                SourceExpr="Origin Code" }

    { 1102300003;2;Field  ;
                SourceExpr="ICMS CST Code" }

    { 1102300002;2;Field  ;
                SourceExpr="PIS CST Code" }

    { 1102300001;2;Field  ;
                SourceExpr="COFINS CST Code" }

    { 1102300000;2;Field  ;
                SourceExpr="IPI CST Code" }

    { 16  ;2   ;Field     ;
                BlankZero=Yes;
                SourceExpr="Line Discount %";
                OnValidate=BEGIN
                             RedistributeTotalsOnAfterValidate;
                           END;
                            }

    { 56  ;2   ;Field     ;
                SourceExpr="Line Discount Amount";
                Visible=FALSE;
                OnValidate=BEGIN
                             RedistributeTotalsOnAfterValidate;
                           END;
                            }

    { 54  ;2   ;Field     ;
                SourceExpr="Allow Invoice Disc.";
                Visible=FALSE }

    { 20  ;2   ;Field     ;
                SourceExpr="Inv. Discount Amount";
                Visible=FALSE }

    { 14  ;2   ;Field     ;
                SourceExpr="Allow Item Charge Assignment";
                Visible=FALSE }

    { 52  ;2   ;Field     ;
                BlankZero=Yes;
                SourceExpr="Qty. to Assign";
                OnDrillDown=BEGIN
                              CurrPage.SAVERECORD;
                              ShowItemChargeAssgnt;
                              UpdateForm(FALSE);
                            END;
                             }

    { 60  ;2   ;Field     ;
                BlankZero=Yes;
                SourceExpr="Qty. Assigned";
                OnDrillDown=BEGIN
                              CurrPage.SAVERECORD;
                              ShowItemChargeAssgnt;
                              UpdateForm(FALSE);
                            END;
                             }

    { 44  ;2   ;Field     ;
                SourceExpr="Job No.";
                Visible=FALSE;
                OnValidate=BEGIN
                             ShowShortcutDimCode(ShortcutDimCode);
                           END;
                            }

    { 3   ;2   ;Field     ;
                SourceExpr="Job Task No.";
                Visible=FALSE }

    { 62  ;2   ;Field     ;
                SourceExpr="Work Type Code";
                Visible=FALSE }

    { 30  ;2   ;Field     ;
                SourceExpr="Blanket Order No.";
                Visible=FALSE }

    { 32  ;2   ;Field     ;
                SourceExpr="Blanket Order Line No.";
                Visible=FALSE }

    { 5810;2   ;Field     ;
                SourceExpr="Appl.-from Item Entry";
                Visible=FALSE }

    { 48  ;2   ;Field     ;
                SourceExpr="Appl.-to Item Entry";
                Visible=FALSE }

    { 76  ;2   ;Field     ;
                SourceExpr="Shortcut Dimension 1 Code";
                Visible=FALSE }

    { 74  ;2   ;Field     ;
                SourceExpr="Shortcut Dimension 2 Code";
                Visible=FALSE }

    { 300 ;2   ;Field     ;
                SourceExpr=ShortcutDimCode[3];
                CaptionClass='1,2,3';
                Visible=FALSE;
                OnValidate=BEGIN
                             ValidateShortcutDimCode(3,ShortcutDimCode[3]);
                           END;

                OnLookup=BEGIN
                           LookupShortcutDimCode(3,ShortcutDimCode[3]);
                         END;
                          }

    { 302 ;2   ;Field     ;
                SourceExpr=ShortcutDimCode[4];
                CaptionClass='1,2,4';
                Visible=FALSE;
                OnValidate=BEGIN
                             ValidateShortcutDimCode(4,ShortcutDimCode[4]);
                           END;

                OnLookup=BEGIN
                           LookupShortcutDimCode(4,ShortcutDimCode[4]);
                         END;
                          }

    { 304 ;2   ;Field     ;
                SourceExpr=ShortcutDimCode[5];
                CaptionClass='1,2,5';
                Visible=FALSE;
                OnValidate=BEGIN
                             ValidateShortcutDimCode(5,ShortcutDimCode[5]);
                           END;

                OnLookup=BEGIN
                           LookupShortcutDimCode(5,ShortcutDimCode[5]);
                         END;
                          }

    { 306 ;2   ;Field     ;
                SourceExpr=ShortcutDimCode[6];
                CaptionClass='1,2,6';
                Visible=FALSE;
                OnValidate=BEGIN
                             ValidateShortcutDimCode(6,ShortcutDimCode[6]);
                           END;

                OnLookup=BEGIN
                           LookupShortcutDimCode(6,ShortcutDimCode[6]);
                         END;
                          }

    { 308 ;2   ;Field     ;
                SourceExpr=ShortcutDimCode[7];
                CaptionClass='1,2,7';
                Visible=FALSE;
                OnValidate=BEGIN
                             ValidateShortcutDimCode(7,ShortcutDimCode[7]);
                           END;

                OnLookup=BEGIN
                           LookupShortcutDimCode(7,ShortcutDimCode[7]);
                         END;
                          }

    { 310 ;2   ;Field     ;
                SourceExpr=ShortcutDimCode[8];
                CaptionClass='1,2,8';
                Visible=FALSE;
                OnValidate=BEGIN
                             ValidateShortcutDimCode(8,ShortcutDimCode[8]);
                           END;

                OnLookup=BEGIN
                           LookupShortcutDimCode(8,ShortcutDimCode[8]);
                         END;
                          }

    { 1000000000;2;Field  ;
                SourceExpr="Gross Weight";
                Visible=false }

    { 1000000001;2;Field  ;
                SourceExpr="Net Weight";
                Visible=false }

    { 52006503;2;Field    ;
                SourceExpr="Customer Order No.";
                Visible=FALSE }

    { 52006502;2;Field    ;
                SourceExpr="Customer Order Line No.";
                Visible=FALSE }

    { 52006505;2;Field    ;
                SourceExpr="Line No.";
                Visible=FALSE;
                Editable=FALSE }

    { 39  ;1   ;Group     ;
                GroupType=Group }

    { 35  ;2   ;Group     ;
                Visible=FALSE;
                GroupType=Group }

    { 33  ;3   ;Field     ;
                Name=Invoice Discount Amount;
                CaptionML=ENU=Invoice Discount Amount;
                SourceExpr=TotalSalesLine."Inv. Discount Amount";
                AutoFormatType=1;
                Editable=InvDiscAmountEditable;
                Style=Subordinate;
                StyleExpr=RefreshMessageEnabled;
                OnValidate=BEGIN
                             SalesHeader.GET("Document Type","Document No.");
                             SalesCalcDiscByType.ApplyInvDiscBasedOnAmt(TotalSalesLine."Inv. Discount Amount",SalesHeader);
                             CurrPage.UPDATE(FALSE);
                           END;
                            }

    { 31  ;3   ;Field     ;
                Name=Invoice Disc. Pct.;
                CaptionML=ENU=Invoice Discount %;
                DecimalPlaces=0:2;
                SourceExpr=SalesCalcDiscByType.GetCustInvoiceDiscountPct(Rec);
                Visible=TRUE;
                Editable=FALSE;
                Style=Subordinate;
                StyleExpr=RefreshMessageEnabled }

    { 17  ;2   ;Group     ;
                GroupType=Group }

    { 15  ;3   ;Field     ;
                Name=Total Amount Excl. VAT;
                DrillDown=No;
                CaptionML=ENU=Total Amount Excl. VAT;
                SourceExpr=TotalSalesLine.Amount;
                AutoFormatType=1;
                CaptionClass=DocumentTotals.GetTotalExclVATCaption(SalesHeader."Currency Code");
                Visible=FALSE;
                Editable=FALSE;
                Style=Subordinate;
                StyleExpr=RefreshMessageEnabled }

    { 13  ;3   ;Field     ;
                Name=Total VAT Amount;
                CaptionML=ENU=Total VAT;
                SourceExpr=VATAmount;
                AutoFormatType=1;
                CaptionClass=DocumentTotals.GetTotalVATCaption(SalesHeader."Currency Code");
                Visible=FALSE;
                Editable=FALSE;
                Style=Subordinate;
                StyleExpr=RefreshMessageEnabled }

    { 11  ;3   ;Field     ;
                Name=Total Amount Incl. VAT;
                CaptionML=ENU=Total Amount Incl. VAT;
                SourceExpr=TotalSalesLine."Amount Including VAT";
                AutoFormatType=1;
                CaptionClass=DocumentTotals.GetTotalInclVATCaption(SalesHeader."Currency Code");
                Editable=FALSE;
                StyleExpr=TotalAmountStyle }

    { 9   ;3   ;Field     ;
                Name=RefreshTotals;
                DrillDown=Yes;
                SourceExpr=RefreshMessageText;
                Enabled=RefreshMessageEnabled;
                Editable=FALSE;
                OnDrillDown=BEGIN
                              DocumentTotals.SalesRedistributeInvoiceDiscountAmounts(Rec,VATAmount,TotalSalesLine);
                              CurrPage.UPDATE(FALSE);
                            END;

                ShowCaption=No }

  }
  CODE
  {
    VAR
      SalesHeader@1011 : Record 36;
      TotalSalesHeader@1021 : Record 36;
      TotalSalesLine@1020 : Record 37;
      TransferExtendedText@1000 : Codeunit 378;
      ItemAvailFormsMgt@1004 : Codeunit 353;
      SalesCalcDiscByType@1015 : Codeunit 56;
      DocumentTotals@1014 : Codeunit 57;
      VATAmount@1013 : Decimal;
      UpdateAllowedVar@1002 : Boolean;
      ShortcutDimCode@1001 : ARRAY [8] OF Code[20];
      Text000@1003 : TextConst 'ENU=Unable to run this function while in View mode.;PTB=N�o � poss�vel executar essa fun��o em modo de Visualiza��o.';
      InvDiscAmountEditable@1010 : Boolean;
      TotalAmountStyle@1009 : Text;
      RefreshMessageEnabled@1008 : Boolean;
      RefreshMessageText@1007 : Text;
      TypeChosen@1022 : Boolean;

    PROCEDURE ApproveCalcInvDisc@1();
    BEGIN
      CODEUNIT.RUN(CODEUNIT::"Sales-Disc. (Yes/No)",Rec);
    END;

    PROCEDURE CalcInvDisc@6();
    BEGIN
      CODEUNIT.RUN(CODEUNIT::"Sales-Calc. Discount",Rec);
    END;

    PROCEDURE ExplodeBOM@3();
    BEGIN
      CODEUNIT.RUN(CODEUNIT::"Sales-Explode BOM",Rec);
    END;

    PROCEDURE GetReturnReceipt@2();
    BEGIN
      CODEUNIT.RUN(CODEUNIT::"Sales-Get Return Receipts",Rec);
    END;

    PROCEDURE InsertExtendedText@5(Unconditionally@1000 : Boolean);
    BEGIN
      IF TransferExtendedText.SalesCheckIfAnyExtText(Rec,Unconditionally) THEN BEGIN
        CurrPage.SAVERECORD;
        COMMIT;
        TransferExtendedText.InsertSalesExtText(Rec);
      END;
      IF TransferExtendedText.MakeUpdate THEN
        UpdateForm(TRUE);
    END;

    PROCEDURE OpenItemTrackingLines@6500();
    BEGIN
      OpenItemTrackingLines;
    END;

    PROCEDURE ItemChargeAssgnt@5800();
    BEGIN
      ShowItemChargeAssgnt;
    END;

    PROCEDURE UpdateForm@12(SetSaveRecord@1000 : Boolean);
    BEGIN
      CurrPage.UPDATE(SetSaveRecord);
    END;

    PROCEDURE SetUpdateAllowed@4(UpdateAllowed@1000 : Boolean);
    BEGIN
      UpdateAllowedVar := UpdateAllowed;
    END;

    PROCEDURE UpdateAllowed@9() : Boolean;
    BEGIN
      IF UpdateAllowedVar = FALSE THEN BEGIN
        MESSAGE(Text000);
        EXIT(FALSE);
      END;
      EXIT(TRUE);
    END;

    PROCEDURE ShowLineComments@10();
    BEGIN
      ShowLineComments;
    END;

    LOCAL PROCEDURE NoOnAfterValidate@19066594();
    BEGIN
      InsertExtendedText(FALSE);
      IF (Type = Type::"Charge (Item)") AND ("No." <> xRec."No.") AND
         (xRec."No." <> '')
      THEN
        CurrPage.SAVERECORD;
    END;

    LOCAL PROCEDURE CrossReferenceNoOnAfterValidat@19048248();
    BEGIN
      InsertExtendedText(FALSE);
    END;

    LOCAL PROCEDURE ReserveOnAfterValidate@19004502();
    BEGIN
      IF (Reserve = Reserve::Always) AND ("Outstanding Qty. (Base)" <> 0) THEN BEGIN
        CurrPage.SAVERECORD;
        AutoReserve;
      END;
    END;

    LOCAL PROCEDURE QuantityOnAfterValidate@19032465();
    BEGIN
      IF Reserve = Reserve::Always THEN BEGIN
        CurrPage.SAVERECORD;
        AutoReserve;
      END;
    END;

    LOCAL PROCEDURE UnitofMeasureCodeOnAfterValida@19057939();
    BEGIN
      IF Reserve = Reserve::Always THEN BEGIN
        CurrPage.SAVERECORD;
        AutoReserve;
      END;
    END;

    LOCAL PROCEDURE RedistributeTotalsOnAfterValidate@11();
    BEGIN
      CurrPage.SAVERECORD;

      SalesHeader.GET("Document Type","Document No.");
      IF DocumentTotals.SalesCheckNumberOfLinesLimit(SalesHeader) THEN
        DocumentTotals.SalesRedistributeInvoiceDiscountAmounts(Rec,VATAmount,TotalSalesLine);
      CurrPage.UPDATE;
    END;

    LOCAL PROCEDURE ">NAVBR FUNCTIONS"@52006500();
    BEGIN
    END;

    LOCAL PROCEDURE ActionAssignDocCharges@52006504();
    VAR
      AssignDocChargesSales@52006500 : Codeunit 52112445;
      SelectedSalesLine@52006501 : Record 37;
    BEGIN
      CurrPage.SETSELECTIONFILTER(SelectedSalesLine);
      AssignDocChargesSales.AssignDocumentChargesFor(SelectedSalesLine);
    END;

    BEGIN
    END.
  }
}

