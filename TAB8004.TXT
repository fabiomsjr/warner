OBJECT Table 8004 Notification Line
{
  OBJECT-PROPERTIES
  {
    Date=29/02/12;
    Time=18:14:17;
    Version List=NAVW16.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Notification Line;
               PTB=Linha de Notifica��o];
    DrillDownPageID=Page8005;
  }
  FIELDS
  {
    { 1   ;   ;Notification Batch Name;Code10     ;TableRelation="Notification Worksheet Batch";
                                                   CaptionML=[ENU=Notification Batch Name;
                                                              PTB=Notifica��o Nome de Lote] }
    { 2   ;   ;Notification Code   ;Code10        ;TableRelation=Notification;
                                                   CaptionML=[ENU=Notification Code;
                                                              PTB=Cod. Notifica��o] }
    { 3   ;   ;Line No.            ;Integer       ;CaptionML=[ENU=Line No.;
                                                              PTB=N� Linha] }
    { 4   ;   ;Record ID           ;RecordID      ;CaptionML=[ENU=Record ID;
                                                              PTB=Record ID] }
    { 5   ;   ;Search Record ID    ;Code250       ;CaptionML=[ENU=Search Record ID;
                                                              PTB=Procurar Registro ID] }
    { 6   ;   ;Description         ;Text250       ;CaptionML=[ENU=Description;
                                                              PTB=Descri��o];
                                                   Editable=No }
    { 7   ;   ;Sent                ;Boolean       ;FieldClass=FlowField;
                                                   CalcFormula=Exist("Notification Log Entry" WHERE (Notification Code=FIELD(Notification Code),
                                                                                                     Search Record ID=FIELD(Search Record ID),
                                                                                                     Date and Time Sent=FIELD(FILTER(Date and Time Sent Filter))));
                                                   CaptionML=[ENU=Sent;
                                                              PTB=Enviado];
                                                   Editable=No }
    { 8   ;   ;Last Date and Time Sent;DateTime   ;FieldClass=FlowField;
                                                   CalcFormula=Max("Notification Log Entry"."Date and Time Sent" WHERE (Notification Code=FIELD(Notification Code),
                                                                                                                        Search Record ID=FIELD(Search Record ID)));
                                                   CaptionML=[ENU=Last Date and Time Sent;
                                                              PTB=�ltima Data e Hora enviada];
                                                   Editable=No }
    { 9   ;   ;Date and Time Sent Filter;DateTime ;FieldClass=FlowFilter;
                                                   CaptionML=[ENU=Date and Time Sent Filter;
                                                              PTB=Filtro Data e Hora de Envio] }
  }
  KEYS
  {
    {    ;Notification Batch Name,Notification Code,Line No.;
                                                   Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

