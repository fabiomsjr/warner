OBJECT Table 5822 Invt. Post to G/L Test Buffer
{
  OBJECT-PROPERTIES
  {
    Date=07/09/12;
    Time=12:00:00;
    Version List=NAVW17.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Invt. Post to G/L Test Buffer;
               PTB=Teste Buffer Reg. Inv. na Contabilidade];
  }
  FIELDS
  {
    { 1   ;   ;Line No.            ;Integer       ;CaptionML=[ENU=Line No.;
                                                              PTB=Nｧ Linha] }
    { 2   ;   ;Account No.         ;Code20        ;TableRelation="G/L Account";
                                                   CaptionML=[ENU=Account No.;
                                                              PTB=Conta No. 2] }
    { 3   ;   ;Posting Date        ;Date          ;CaptionML=[ENU=Posting Date;
                                                              PTB=Data Registo];
                                                   ClosingDates=Yes }
    { 4   ;   ;Document No.        ;Code20        ;CaptionML=[ENU=Document No.;
                                                              PTB=No. Documento] }
    { 5   ;   ;Description         ;Text50        ;CaptionML=[ENU=Description;
                                                              PTB=Descri�ﾆo] }
    { 6   ;   ;Amount              ;Decimal       ;CaptionML=[ENU=Amount;
                                                              PTB=Valor];
                                                   AutoFormatType=1 }
    { 8   ;   ;Source Code         ;Code10        ;TableRelation="Source Code";
                                                   CaptionML=[ENU=Source Code;
                                                              PTB=Cod. Origem];
                                                   Editable=No }
    { 9   ;   ;System-Created Entry;Boolean       ;CaptionML=[ENU=System-Created Entry;
                                                              PTB=Lan�amento Autom�tico];
                                                   Editable=No }
    { 10  ;   ;Gen. Bus. Posting Group;Code10     ;TableRelation="Gen. Business Posting Group";
                                                   CaptionML=[ENU=Gen. Bus. Posting Group;
                                                              PTB=Gr. Cont�bil Neg｢cio] }
    { 11  ;   ;Gen. Prod. Posting Group;Code10    ;TableRelation="Gen. Product Posting Group";
                                                   CaptionML=[ENU=Gen. Prod. Posting Group;
                                                              PTB=Gr. Cont�bil Produto] }
    { 13  ;   ;Additional-Currency Posting;Option ;CaptionML=[ENU=Additional-Currency Posting;
                                                              PTB=Registro Moeda-Adicional];
                                                   OptionCaptionML=[ENU=None,Amount Only,Additional-Currency Amount Only;
                                                                    PTB=Nenhum,Somente Valor,Somente Valor Moeda Adicional];
                                                   OptionString=None,Amount Only,Additional-Currency Amount Only;
                                                   Editable=No }
    { 14  ;   ;Source Currency Code;Code10        ;TableRelation=Currency;
                                                   CaptionML=[ENU=Source Currency Code;
                                                              PTB=Cod. Moeda Origem];
                                                   Editable=No }
    { 15  ;   ;Source Currency Amount;Decimal     ;CaptionML=[ENU=Source Currency Amount;
                                                              PTB=Valor Moeda Origem];
                                                   Editable=No;
                                                   AutoFormatType=1 }
    { 16  ;   ;Value Entry No.     ;Integer       ;TableRelation="Value Entry";
                                                   CaptionML=[ENU=Value Entry No.;
                                                              PTB=Nｧ Mov. Valor];
                                                   Editable=No }
    { 17  ;   ;Location Code       ;Code10        ;TableRelation=Location;
                                                   CaptionML=[ENU=Location Code;
                                                              PTB=Cod. Dep｢sito] }
    { 18  ;   ;Invt. Posting Group Code;Code10    ;TableRelation="Inventory Posting Group";
                                                   CaptionML=[ENU=Invt. Posting Group Code;
                                                              PTB=Cod. Gr. Cont�bil Invent�rio] }
    { 19  ;   ;Inventory Account Type;Option      ;CaptionML=[ENU=Inventory Account Type;
                                                              PTB=Tipo Conta Invent�rio];
                                                   OptionCaptionML=[ENU=Inventory (Interim),Invt. Accrual (Interim),Inventory,WIP Inventory,Inventory Adjmt.,Direct Cost Applied,Overhead Applied,Purchase Variance,COGS,COGS (Interim),Material Variance,Capacity Variance,Subcontracted Variance,Cap. Overhead Variance,Mfg. Overhead Variance;
                                                                    PTB=Estoque,Estoque (Interim),Estoque WIP,Ajuste Estoque,Acruam. Estoque (Interim),Custo Direto Aplicado,Overhead Aplicado,Variハcia de Compra,CMV,CMV (Interim),Variハcia de Material,Variハcia de Capacidade,Variハcia Subcontratada,Variハcia de Cap. Overhead,Variハcia de Overhead Manuf.];
                                                   OptionString=Inventory (Interim),Invt. Accrual (Interim),Inventory,WIP Inventory,Inventory Adjmt.,Direct Cost Applied,Overhead Applied,Purchase Variance,COGS,COGS (Interim),Material Variance,Capacity Variance,Subcontracted Variance,Cap. Overhead Variance,Mfg. Overhead Variance }
    { 480 ;   ;Dimension Set ID    ;Integer       ;TableRelation="Dimension Set Entry";
                                                   CaptionML=ENU=Dimension Set ID;
                                                   Editable=No }
  }
  KEYS
  {
    {    ;Line No.                                ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

