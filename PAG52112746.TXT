OBJECT Page 52112746 Closed Purch. Req. Quote
{
  OBJECT-PROPERTIES
  {
    Date=07/10/13;
    Time=15:36:53;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Request Quote;
               PTB=Cota��o Requisi��o];
    SourceTable=Table52112730;
    PageType=Card;
    OnOpenPage=VAR
                 purchSetup@52006500 : Record 312;
                 i@52006501 : Integer;
                 showVendor@52006502 : ARRAY [10] OF Boolean;
               BEGIN
                 WITH purchSetup DO BEGIN
                   GET;
                   TESTFIELD("Request Quote Vendor Limit");
                 END;
               END;

    ActionList=ACTIONS
    {
      { 52006500;    ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 52006501;1   ;Action    ;
                      Name=SelItems;
                      Ellipsis=Yes;
                      CaptionML=[ENU=Select &Requests;
                                 PTB=Visualizar &Requisi��es];
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=Item;
                      PromotedCategory=Process;
                      RunPageMode=Edit;
                      OnAction=BEGIN
                                 _ViewItems;
                               END;
                                }
      { 52006502;1   ;Action    ;
                      Name=SelVendors;
                      Ellipsis=Yes;
                      CaptionML=[ENU=Select &Vendors;
                                 PTB=Visualizar &Fornecedores];
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=Vendor;
                      PromotedCategory=Process;
                      RunPageMode=Edit;
                      OnAction=BEGIN
                                 _ViewVendors;
                               END;
                                }
      { 52006507;    ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 52006508;1   ;Action    ;
                      Name=OpenFiles;
                      CaptionML=[ENU=Files;
                                 PTB=Arquivos];
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=ExternalDocument;
                      PromotedCategory=Process;
                      OnAction=VAR
                                 docFileMgt@52006500 : Codeunit 52112428;
                               BEGIN
                                 docFileMgt.OpenFiles(Rec);
                               END;
                                }
      { 52006509;1   ;Action    ;
                      Name=PurchTracking;
                      CaptionML=[ENU=Purchase Tracking;
                                 PTB=Rastreabilidade Compras];
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=Track;
                      PromotedCategory=Process;
                      OnAction=VAR
                                 purchTracking@52006500 : Record 52112733;
                               BEGIN
                                 purchTracking.TrackByPurchReqQuote(Rec);
                               END;
                                }
      { 52006510;1   ;Action    ;
                      Name=Print;
                      CaptionML=[ENU=&Print;
                                 PTB=Im&primir];
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=Quote;
                      PromotedCategory=Report;
                      OnAction=BEGIN
                                 Print;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 35000000;0;Container;
                ContainerType=ContentArea }

    { 35000001;1;Group    ;
                Name=Geral;
                CaptionML=[ENU=General;
                           PTB=Geral];
                GroupType=Group }

    { 35000002;2;Field    ;
                AssistEdit=Yes;
                SourceExpr="No.";
                OnAssistEdit=BEGIN
                               IF AssistEdit(xRec) THEN
                                 CurrPage.UPDATE;
                             END;
                              }

    { 35000003;2;Field    ;
                SourceExpr=Description }

    { 35000006;2;Field    ;
                SourceExpr=Date;
                Editable=FALSE }

    { 35000007;2;Field    ;
                SourceExpr="User ID" }

    { 35000008;2;Field    ;
                SourceExpr="User Name" }

    { 35000009;2;Field    ;
                SourceExpr="Purchaser Code" }

    { 52006511;2;Field    ;
                SourceExpr=Status }

    { 52006506;1;Part     ;
                Name=ItemsPage;
                SubPageLink=Purch Req. Quote No.=FIELD(No.);
                PagePartID=Page52112743;
                PartType=Page }

    { 52006505;0;Container;
                ContainerType=FactBoxArea }

    { 52006504;1;Part     ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 52006503;1;Part     ;
                Visible=TRUE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      Text001@52006500 : TextConst 'ENU=Are you sure you want to create purchase orders?;PTB=Tem certeza que deseja criar os pedidos de compra?';
      Text002@52006501 : TextConst 'ENU=Suggest winners for all items?;PTB=Sugerir vencedores para todos os produtos?';

    PROCEDURE _ViewVendors@52006506();
    VAR
      selectVendors@52006500 : Page 52112742;
    BEGIN
      selectVendors.EDITABLE(FALSE);
      selectVendors.SetReqQuoteNo("No.");
      selectVendors.RUNMODAL;
    END;

    PROCEDURE _ViewItems@52006503();
    BEGIN
      Rec.ViewItems;
    END;

    BEGIN
    END.
  }
}

