OBJECT Table 52112526 EFD Setup
{
  OBJECT-PROPERTIES
  {
    Date=02/07/13;
    Time=15:50:47;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=EFD Setup;
               PTB=Config. SPED Fiscal];
  }
  FIELDS
  {
    { 1   ;   ;Primary Key         ;Integer       ;CaptionML=[ENU=Primary Key;
                                                              PTB=Primary Key] }
    { 2   ;   ;Layout Version      ;Text3         ;CaptionML=[ENU=Layout Version;
                                                              PTB=Vers�o Layout] }
    { 3   ;   ;Sped Mov Nos.       ;Code10        ;TableRelation="No. Series";
                                                   CaptionML=[ENU=Sped Mov Nos.;
                                                              PTB=N� S�rie Mov. SPED] }
    { 4   ;   ;Default Profile     ;Option        ;CaptionML=[ENU=Default Profile;
                                                              PTB=Perfil Padr�o];
                                                   OptionCaptionML=[ENU=A Profile,B Profile,C Profile;
                                                                    PTB=Perfil A,Perfil B,Perfil C];
                                                   OptionString=Perfil A,Perfil B,Perfil C }
    { 5   ;   ;Activity Type       ;Option        ;CaptionML=[ENU=Activity Type;
                                                              PTB=Tipo Atividade];
                                                   OptionCaptionML=[ENU=0-Industry or industry-like,1-Others;
                                                                    PTB=0-Industrial ou equiparado a industrial,1-Outros];
                                                   OptionString=0-Industrial ou equiparado a industrial,1-Outros }
    { 13  ;   ;File Separator Character;Text1     ;CaptionML=[ENU=File Separator Character;
                                                              PTB=Caracter Separador Arquivo] }
    { 14  ;   ;ICMS Period Results ;Option        ;CaptionML=[ENU=ICMS Period Results;
                                                              PTB=Per�odo Apura��o ICMS];
                                                   OptionCaptionML=[ENU=" ,0-Monthly,1-Tenth";
                                                                    PTB=" ,0-Mensal,1-Decendial"];
                                                   OptionString=[ ,0-Mensal,1-Decendial] }
    { 15  ;   ;IPI Period Results  ;Option        ;CaptionML=[ENU=IPI Period Results;
                                                              PTB=Per�odo Apura��o IPI];
                                                   OptionCaptionML=[ENU=" ,0-Monthly,1-Tenth";
                                                                    PTB=" ,0-Mensal,1-Decendial"];
                                                   OptionString=[ ,0-Mensal,1-Decendial] }
    { 16  ;   ;Inv. Synthetic Account No.;Code20  ;TableRelation="G/L Account";
                                                   CaptionML=[ENU=Inv. Synthetic Account No.;
                                                              PTB=N� Conta Sint�tica Invent�rio] }
  }
  KEYS
  {
    {    ;Primary Key                             ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

