OBJECT Report 52112828 Convert History Data
{
  OBJECT-PROPERTIES
  {
    Date=30/04/13;
    Time=11:25:26;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Convert History Data;
               PTB=Converter Dados Hist�rico];
    ProcessingOnly=Yes;
    OnPreReport=BEGIN
                  IF NOT CONFIRM ('Confirma que deseja converter os dados para o Hist�rico?',FALSE) THEN
                    ERROR ('Processo cancelado.');
                END;

    OnPostReport=BEGIN
                   MESSAGE ('Processo conclu�do.');
                 END;

  }
  DATASET
  {
    { 9066;    ;DataItem;                    ;
               DataItemTable=Table52112829;
               DataItemTableView=SORTING(Entry No.);
               OnPreDataItem=BEGIN
                               Window.OPEN(
                                 Text001 +
                                 '@1@@@@@@@@@@@@@@@@@@@@@@@@@\');
                               Window.UPDATE(1,0);
                               TotalRecNo := COUNT;
                               RecNo := 0;

                               PostedAccountEntry.RESET;
                               IF PostedAccountEntry.FIND('+') THEN
                                 NextEntryNo := PostedAccountEntry."Entry No." + 1
                               ELSE
                                 NextEntryNo := 1;
                             END;

               OnAfterGetRecord=BEGIN
                                  RecNo := RecNo + 1;
                                  Window.UPDATE(1,ROUND(RecNo / TotalRecNo * 10000,1));

                                  PostedAccountEntry.INIT;
                                  PostedAccountEntry.TRANSFERFIELDS("Related Accounting Mov.");
                                  PostedAccountEntry."Entry No." := NextEntryNo;
                                  PostedAccountEntry.INSERT;

                                  NextEntryNo := NextEntryNo + 1;

                                  DELETE;
                                END;

               OnPostDataItem=BEGIN
                                Window.CLOSE;
                              END;

               ReqFilterFields=Posting Date }

    { 9302;    ;DataItem;                    ;
               DataItemTable=Table52112839;
               DataItemTableView=SORTING(Sped No.,Line No.);
               OnPreDataItem=BEGIN
                               Window.OPEN(
                                 Text002 +
                                 '@1@@@@@@@@@@@@@@@@@@@@@@@@@\');
                               Window.UPDATE(1,0);
                               TotalRecNo := COUNT;
                               RecNo := 0;
                             END;

               OnAfterGetRecord=BEGIN
                                  RecNo := RecNo + 1;
                                  Window.UPDATE(1,ROUND(RecNo / TotalRecNo * 10000,1));

                                  PostedSPEDEntry.INIT;
                                  PostedSPEDEntry.TRANSFERFIELDS("SPED Accounting Mov. Lines.");
                                  PostedSPEDEntry.INSERT;

                                  DELETE;
                                END;

               ReqFilterFields=Sped No. }

  }
  REQUESTPAGE
  {
    PROPERTIES
    {
    }
    CONTROLS
    {
    }
  }
  LABELS
  {
  }
  CODE
  {
    VAR
      Window@1102300000 : Dialog;
      PostedAccountEntry@1102300001 : Record 52112840;
      PostedSPEDEntry@1102300002 : Record 52112841;
      Text001@1102300003 : TextConst 'ENU=Analyzing Data...\\;PTB=Convertendo Movs. Cont�beis...\\';
      Text002@1102300004 : TextConst 'ENU=Converting SPED Movs. ...\\;PTB=Convertendo Movs. SPED...\\';
      TotalRecNo@1102300005 : Integer;
      RecNo@1102300006 : Integer;
      NextEntryNo@1102300007 : Integer;

    BEGIN
    END.
  }
  RDLDATA
  {
  }
}

