OBJECT Table 424 IC Comment Line
{
  OBJECT-PROPERTIES
  {
    Date=07/09/12;
    Time=12:00:00;
    Version List=NAVW17.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=IC Comment Line;
               PTB=IC Linha de Comet�rio];
  }
  FIELDS
  {
    { 1   ;   ;Table Name          ;Option        ;CaptionML=[ENU=Table Name;
                                                              PTB=Nome Tabela];
                                                   OptionCaptionML=[ENU=IC Inbox Transaction,IC Outbox Transaction,Handled IC Inbox Transaction,Handled IC Outbox Transaction;
                                                                    PTB=IC Caixa Entrada Transa��o,IC Caixa Sa�da Transa��o,Caixa Entrada Transa��o IC Utilizada,Caixa Sa�da Transa��o IC Utilizada];
                                                   OptionString=IC Inbox Transaction,IC Outbox Transaction,Handled IC Inbox Transaction,Handled IC Outbox Transaction }
    { 2   ;   ;Transaction No.     ;Integer       ;CaptionML=[ENU=Transaction No.;
                                                              PTB=N� Lan�amento] }
    { 3   ;   ;IC Partner Code     ;Code20        ;TableRelation="IC Partner";
                                                   CaptionML=[ENU=IC Partner Code;
                                                              PTB=IC C�digo Parceiro] }
    { 4   ;   ;Line No.            ;Integer       ;CaptionML=[ENU=Line No.;
                                                              PTB=N� Linha] }
    { 5   ;   ;Date                ;Date          ;CaptionML=[ENU=Date;
                                                              PTB=Data] }
    { 6   ;   ;Comment             ;Text50        ;CaptionML=[ENU=Comment;
                                                              PTB=Coment�rio] }
    { 7   ;   ;Transaction Source  ;Option        ;CaptionML=[ENU=Transaction Source;
                                                              PTB=Fonte Transa��o];
                                                   OptionCaptionML=[ENU=Rejected,Created;
                                                                    PTB=Rejeitado,Criado];
                                                   OptionString=Rejected,Created }
  }
  KEYS
  {
    {    ;Table Name,Transaction No.,IC Partner Code,Transaction Source,Line No.;
                                                   Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    PROCEDURE SetUpNewLine@1();
    VAR
      ICCommentLine@1000 : Record 424;
    BEGIN
      ICCommentLine.SETRANGE("Table Name","Table Name");
      ICCommentLine.SETRANGE("Transaction No.","Transaction No.");
      ICCommentLine.SETRANGE("IC Partner Code","IC Partner Code");
      ICCommentLine.SETRANGE("Transaction Source","Transaction Source");
      ICCommentLine.SETRANGE(Date,WORKDATE);
      IF NOT ICCommentLine.FINDFIRST THEN
        Date := WORKDATE;
    END;

    BEGIN
    END.
  }
}

