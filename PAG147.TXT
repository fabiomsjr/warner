OBJECT Page 147 Posted Purchase Credit Memos
{
  OBJECT-PROPERTIES
  {
    Date=13/10/14;
    Time=16:26:12;
    Modified=Yes;
    Version List=NAVW18.00,NAVBR7,FNX057.00,WAR003.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Posted Purchase Credit Memos;
               PTB=Hist�rico Notas Cr�ditos Compras];
    SourceTable=Table124;
    PageType=List;
    CardPageID=Posted Purchase Credit Memo;
    OnOpenPage=BEGIN
                 SetSecurityFilterOnRespCenter;
               END;

    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 23      ;1   ;ActionGroup;
                      CaptionML=[ENU=&Cr. Memo;
                                 PTB=&Recebimentos];
                      Image=CreditMemo }
      { 29      ;2   ;Action    ;
                      ShortCutKey=F7;
                      CaptionML=[ENU=Statistics;
                                 PTB=Estatisticas];
                      RunObject=Page 401;
                      RunPageLink=No.=FIELD(No.);
                      Promoted=Yes;
                      Image=Statistics;
                      PromotedCategory=Process }
      { 30      ;2   ;Action    ;
                      CaptionML=[ENU=Co&mments;
                                 PTB=Co&ment�rios];
                      RunObject=Page 66;
                      RunPageLink=Document Type=CONST(Posted Credit Memo),
                                  No.=FIELD(No.);
                      Image=ViewComments }
      { 1102601000;2 ;Action    ;
                      AccessByPermission=TableData 348=R;
                      ShortCutKey=Shift+Ctrl+D;
                      CaptionML=[ENU=Dimensions;
                                 PTB=Dimens�es];
                      Image=Dimensions;
                      OnAction=BEGIN
                                 ShowDimensions;
                               END;
                                }
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 21      ;1   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[ENU=&Print;
                                 PTB=&Imprimir];
                      Promoted=Yes;
                      Image=Print;
                      PromotedCategory=Process;
                      OnAction=VAR
                                 PurchCrMemoHdr@1102 : Record 124;
                               BEGIN
                                 CurrPage.SETSELECTIONFILTER(PurchCrMemoHdr);
                                 PurchCrMemoHdr.PrintRecords(TRUE);
                               END;
                                }
      { 22      ;1   ;Action    ;
                      CaptionML=[ENU=&Navigate;
                                 PTB=&Navegar];
                      Promoted=Yes;
                      Image=Navigate;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 Navigate;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                SourceExpr="No." }

    { 4   ;2   ;Field     ;
                SourceExpr="Buy-from Vendor No." }

    { 52006501;2;Field    ;
                SourceExpr="Branch Code" }

    { 35000000;2;Field    ;
                SourceExpr="Vendor Cr. Memo No." }

    { 15  ;2   ;Field     ;
                SourceExpr="Order Address Code";
                Visible=FALSE }

    { 6   ;2   ;Field     ;
                SourceExpr="Buy-from Vendor Name" }

    { 35100000;2;Field    ;
                SourceExpr="Posting Text" }

    { 35  ;2   ;Field     ;
                SourceExpr="Currency Code" }

    { 13  ;2   ;Field     ;
                SourceExpr=Amount;
                OnDrillDown=BEGIN
                              SETRANGE("No.");
                              PAGE.RUNMODAL(PAGE::"Posted Purchase Credit Memo",Rec)
                            END;
                             }

    { 17  ;2   ;Field     ;
                SourceExpr="Amount Including VAT";
                OnDrillDown=BEGIN
                              SETRANGE("No.");
                              PAGE.RUNMODAL(PAGE::"Posted Purchase Credit Memo",Rec)
                            END;
                             }

    { 31  ;2   ;Field     ;
                SourceExpr="Buy-from Post Code";
                Visible=FALSE }

    { 27  ;2   ;Field     ;
                SourceExpr="Buy-from Country/Region Code";
                Visible=FALSE }

    { 39  ;2   ;Field     ;
                SourceExpr="Buy-from Contact";
                Visible=FALSE }

    { 127 ;2   ;Field     ;
                SourceExpr="Pay-to Vendor No.";
                Visible=FALSE }

    { 125 ;2   ;Field     ;
                SourceExpr="Pay-to Name";
                Visible=FALSE }

    { 37  ;2   ;Field     ;
                SourceExpr="Pay-to Post Code";
                Visible=FALSE }

    { 33  ;2   ;Field     ;
                SourceExpr="Pay-to Country/Region Code";
                Visible=FALSE }

    { 115 ;2   ;Field     ;
                SourceExpr="Pay-to Contact";
                Visible=FALSE }

    { 111 ;2   ;Field     ;
                SourceExpr="Ship-to Code";
                Visible=FALSE }

    { 109 ;2   ;Field     ;
                SourceExpr="Ship-to Name";
                Visible=FALSE }

    { 25  ;2   ;Field     ;
                SourceExpr="Ship-to Post Code";
                Visible=FALSE }

    { 19  ;2   ;Field     ;
                SourceExpr="Ship-to Country/Region Code";
                Visible=FALSE }

    { 99  ;2   ;Field     ;
                SourceExpr="Ship-to Contact";
                Visible=FALSE }

    { 97  ;2   ;Field     ;
                SourceExpr="Posting Date";
                Visible=FALSE }

    { 71  ;2   ;Field     ;
                SourceExpr="Purchaser Code";
                Visible=FALSE }

    { 87  ;2   ;Field     ;
                SourceExpr="Shortcut Dimension 1 Code";
                Visible=FALSE }

    { 85  ;2   ;Field     ;
                SourceExpr="Shortcut Dimension 2 Code";
                Visible=FALSE }

    { 89  ;2   ;Field     ;
                SourceExpr="Location Code";
                Visible=TRUE }

    { 8   ;2   ;Field     ;
                SourceExpr="No. Printed" }

    { 1102601001;2;Field  ;
                SourceExpr="Document Date";
                Visible=FALSE }

    { 1102601003;2;Field  ;
                SourceExpr="Applies-to Doc. Type";
                Visible=FALSE }

    { 52006500;2;Field    ;
                SourceExpr="Issue Date/Time" }

    { 35000003;2;Field    ;
                SourceExpr="NFe cStat" }

    { 35000002;2;Field    ;
                SourceExpr="NFe xMotivo" }

    { 35000001;2;Field    ;
                SourceExpr="NFe Chave Acesso" }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=TRUE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

