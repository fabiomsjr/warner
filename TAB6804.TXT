OBJECT Table 6804 EP Group
{
  OBJECT-PROPERTIES
  {
    Date=29/02/12;
    Time=18:14:16;
    Version List=NAVW16.00;
  }
  PROPERTIES
  {
    OnDelete=VAR
               EPGrpReqType@1000000000 : Record 6805;
               EPGrpUser@1000000001 : Record 6840;
             BEGIN
               EPGrpReqType.SETRANGE("Group Code",Code);
               IF EPGrpReqType.FIND('-') THEN
                 REPEAT
                   EPGrpReqType.DELETE(TRUE);
                 UNTIL EPGrpReqType.NEXT = 0;

               EPGrpUser.SETRANGE("Group Code",Code);
               IF EPGrpUser.FIND('-') THEN
                 REPEAT
                   EPGrpUser.DELETE(TRUE);
                 UNTIL EPGrpUser.NEXT = 0;
             END;

    CaptionML=[ENU=EP Group;
               PTB=EP Grupo];
    LookupPageID=Page6804;
  }
  FIELDS
  {
    { 1   ;   ;Code                ;Code20        ;CaptionML=[ENU=Code;
                                                              PTB=C�digo];
                                                   NotBlank=Yes }
    { 2   ;   ;Description         ;Text30        ;CaptionML=[ENU=Description;
                                                              PTB=Descri��o] }
    { 3   ;   ;No. of WP Requests  ;Integer       ;FieldClass=FlowField;
                                                   CalcFormula=Count(Table6805 WHERE (Field1=FIELD(Code)));
                                                   CaptionML=[ENU=No. of WP Requests;
                                                              PTB=N. de Requisi��o WP];
                                                   Editable=No }
    { 4   ;   ;Language ID         ;Integer       ;TableRelation="Windows Language" WHERE (STX File Exist=CONST(Yes),
                                                                                           ETX File Exist=CONST(Yes));
                                                   OnValidate=VAR
                                                                WinLanguage@1000 : Record 2000000045;
                                                              BEGIN
                                                                IF "Language ID" <> 0 THEN BEGIN
                                                                  WinLanguage.GET("Language ID");
                                                                  "Language Name" := WinLanguage.Name;
                                                                END ELSE
                                                                  "Language Name" := '';
                                                              END;

                                                   CaptionML=[ENU=Language ID;
                                                              PTB=ID Idioma] }
    { 5   ;   ;Caption ID          ;Integer       ;InitValue=0;
                                                   CaptionML=[ENU=Caption ID;
                                                              PTB=ID Captions];
                                                   Editable=No }
    { 6   ;   ;Language Name       ;Text80        ;CaptionML=[ENU=Language Name;
                                                              PTB=Nome Idioma];
                                                   Editable=No }
    { 7   ;   ;Caption             ;Text80        ;CaptionML=[ENU=Caption;
                                                              PTB=Caption];
                                                   Editable=No }
  }
  KEYS
  {
    {    ;Code                                    ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

