OBJECT Table 99008521 BizTalk Message Check
{
  OBJECT-PROPERTIES
  {
    Date=29/02/12;
    Time=18:14:20;
    Version List=NAVW16.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=BizTalk Message Check;
               PTB=Verif. Mensagem BizTalk];
  }
  FIELDS
  {
    { 1   ;   ;Message Check ID    ;Integer       ;CaptionML=[ENU=Message Check ID;
                                                              PTB=ID Verf. Mensagem] }
    { 2   ;   ;Company             ;Text30        ;CaptionML=[ENU=Company;
                                                              PTB=Empresa] }
    { 3   ;   ;Check Type          ;Text30        ;CaptionML=[ENU=Check Type;
                                                              PTB=Tipo Cheque] }
    { 4   ;   ;Message             ;Text250       ;CaptionML=[ENU=Message;
                                                              PTB=Mensagem] }
  }
  KEYS
  {
    {    ;Message Check ID                        ;Clustered=Yes }
    {    ;Company,Check Type                       }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

