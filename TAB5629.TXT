OBJECT Table 5629 Ins. Coverage Ledger Entry
{
  OBJECT-PROPERTIES
  {
    Date=07/09/12;
    Time=12:00:00;
    Version List=NAVW17.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Ins. Coverage Ledger Entry;
               PTB=Mov. seguro];
    LookupPageID=Page5647;
    DrillDownPageID=Page5647;
  }
  FIELDS
  {
    { 1   ;   ;Entry No.           ;Integer       ;CaptionML=[ENU=Entry No.;
                                                              PTB=N� Mov.] }
    { 2   ;   ;Insurance No.       ;Code20        ;TableRelation=Insurance;
                                                   CaptionML=[ENU=Insurance No.;
                                                              PTB=N� Seguro] }
    { 3   ;   ;Disposed FA         ;Boolean       ;CaptionML=[ENU=Disposed FA;
                                                              PTB=AF Vendido/Baixado] }
    { 4   ;   ;FA No.              ;Code20        ;TableRelation="Fixed Asset";
                                                   CaptionML=[ENU=FA No.;
                                                              PTB=N� AF] }
    { 5   ;   ;FA Description      ;Text50        ;CaptionML=[ENU=FA Description;
                                                              PTB=Descri��o AF] }
    { 6   ;   ;Posting Date        ;Date          ;CaptionML=[ENU=Posting Date;
                                                              PTB=Data Registo] }
    { 7   ;   ;Document Type       ;Option        ;CaptionML=[ENU=Document Type;
                                                              PTB=Tipo Documento];
                                                   OptionCaptionML=[ENU=" ,Payment,Invoice,Credit Memo,Finance Charge Memo,Reminder,Refund";
                                                                    PTB=" ,Pagamento,Nota Fiscal,Nota de Cr�dito,Nota de Encargo Financeiro,Lembrete,Reembolso"];
                                                   OptionString=[ ,Payment,Invoice,Credit Memo,Finance Charge Memo,Reminder,Refund] }
    { 8   ;   ;Document Date       ;Date          ;CaptionML=[ENU=Document Date;
                                                              PTB=Data Documento] }
    { 9   ;   ;Document No.        ;Code20        ;CaptionML=[ENU=Document No.;
                                                              PTB=No. Documento] }
    { 10  ;   ;External Document No.;Code35       ;CaptionML=[ENU=External Document No.;
                                                              PTB=N� Documento Externo] }
    { 14  ;   ;Amount              ;Decimal       ;CaptionML=[ENU=Amount;
                                                              PTB=Valor];
                                                   AutoFormatType=1 }
    { 16  ;   ;Description         ;Text50        ;CaptionML=[ENU=Description;
                                                              PTB=Descri��o] }
    { 17  ;   ;FA Class Code       ;Code10        ;TableRelation="FA Class";
                                                   CaptionML=[ENU=FA Class Code;
                                                              PTB=Cod. Classe AF] }
    { 18  ;   ;FA Subclass Code    ;Code10        ;TableRelation="FA Subclass";
                                                   CaptionML=[ENU=FA Subclass Code;
                                                              PTB=Cod. Subclasse AF] }
    { 19  ;   ;FA Location Code    ;Code10        ;TableRelation="FA Location";
                                                   CaptionML=[ENU=FA Location Code;
                                                              PTB=Cod. Dep�sito AF] }
    { 20  ;   ;Global Dimension 1 Code;Code20     ;TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(1));
                                                   CaptionML=[ENU=Global Dimension 1 Code;
                                                              PTB=Cod. Dimens�o 1 Global];
                                                   CaptionClass='1,1,1' }
    { 21  ;   ;Global Dimension 2 Code;Code20     ;TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(2));
                                                   CaptionML=[ENU=Global Dimension 2 Code;
                                                              PTB=Cod. Dimens�o 2 Global];
                                                   CaptionClass='1,1,2' }
    { 22  ;   ;Location Code       ;Code10        ;TableRelation=Location WHERE (Use As In-Transit=CONST(No));
                                                   CaptionML=[ENU=Location Code;
                                                              PTB=Cod. Dep�sito] }
    { 23  ;   ;User ID             ;Code50        ;TableRelation=User."User Name";
                                                   OnLookup=VAR
                                                              UserMgt@1000 : Codeunit 418;
                                                            BEGIN
                                                              UserMgt.LookupUserID("User ID");
                                                            END;

                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=User ID;
                                                              PTB=ID Usu�rio] }
    { 24  ;   ;Source Code         ;Code10        ;TableRelation="Source Code";
                                                   CaptionML=[ENU=Source Code;
                                                              PTB=Cod. Origem] }
    { 25  ;   ;Journal Batch Name  ;Code10        ;CaptionML=[ENU=Journal Batch Name;
                                                              PTB=Nome Se��o Di�rio] }
    { 26  ;   ;Reason Code         ;Code10        ;TableRelation="Reason Code";
                                                   CaptionML=[ENU=Reason Code;
                                                              PTB=Cod. Raz�o] }
    { 27  ;   ;Index Entry         ;Boolean       ;CaptionML=[ENU=Index Entry;
                                                              PTB=Mov. Ajuste] }
    { 28  ;   ;No. Series          ;Code10        ;TableRelation="No. Series";
                                                   CaptionML=[ENU=No. Series;
                                                              PTB=N� S�ries] }
    { 480 ;   ;Dimension Set ID    ;Integer       ;TableRelation="Dimension Set Entry";
                                                   OnLookup=BEGIN
                                                              ShowDimensions;
                                                            END;

                                                   CaptionML=ENU=Dimension Set ID;
                                                   Editable=No }
  }
  KEYS
  {
    {    ;Entry No.                               ;Clustered=Yes }
    {    ;Insurance No.,Posting Date              ;SumIndexFields=Amount }
    {    ;Insurance No.,Disposed FA,Posting Date  ;SumIndexFields=Amount }
    {    ;FA No.,Insurance No.,Disposed FA,Posting Date;
                                                   SumIndexFields=Amount }
    {    ;FA No.,Disposed FA,Posting Date          }
    {    ;Document No.,Posting Date                }
  }
  FIELDGROUPS
  {
    { 1   ;DropDown            ;Entry No.,Insurance No.,FA No.,FA Description,Posting Date }
  }
  CODE
  {
    VAR
      DimMgt@1000 : Codeunit 408;

    PROCEDURE ShowDimensions@1();
    BEGIN
      DimMgt.ShowDimensionSet("Dimension Set ID",STRSUBSTNO('%1 %2',TABLECAPTION,"Entry No."));
    END;

    BEGIN
    END.
  }
}

