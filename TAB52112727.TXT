OBJECT Table 52112727 Approval User
{
  OBJECT-PROPERTIES
  {
    Date=30/04/13;
    Time=13:07:06;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Approval User;
               PTB=Usu�rio Aprova��o];
    LookupPageID=Page119;
    DrillDownPageID=Page119;
  }
  FIELDS
  {
    { 1   ;   ;User ID             ;Code50        ;TableRelation=User;
                                                   OnValidate=VAR
                                                                LoginMgt@1000 : Codeunit 418;
                                                              BEGIN
                                                                LoginMgt.ValidateUserID("User ID");
                                                              END;

                                                   OnLookup=VAR
                                                              LoginMgt@1000 : Codeunit 418;
                                                            BEGIN
                                                              LoginMgt.LookupUserID("User ID");
                                                            END;

                                                   ValidateTableRelation=No;
                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=User ID;
                                                              PTB=ID Usu�rio];
                                                   NotBlank=Yes }
    { 2   ;   ;Approval Code       ;Code20        ;TableRelation="Approval Code".Code;
                                                   CaptionML=[ENU=Approval Code;
                                                              PTB=C�digo de Aprova��o] }
    { 3   ;   ;Approval Type       ;Option        ;CaptionML=[ENU=Approval Type;
                                                              PTB=Aprova��o Tipo];
                                                   OptionCaptionML=[ENU=" ,Sales Pers./Purchaser,Approver";
                                                                    PTB=" ,Vendedor/Comprador,Aprovador"];
                                                   OptionString=[ ,Sales Pers./Purchaser,Approver] }
    { 4   ;   ;Document Type       ;Option        ;CaptionML=[ENU=Document Type;
                                                              PTB=Tipo Documento];
                                                   OptionCaptionML=[ENU=Quote,Order,Invoice,Credit Memo,Blanket Order,Return Order,None,,,,Request;
                                                                    PTB=Cota��o,Pedido,Nota Fiscal,Nota Cr�dito,Ordem Cobertura,Ordem Retorno,Nenhum,,,,Requisi��o];
                                                   OptionString=Quote,Order,Invoice,Credit Memo,Blanket Order,Return Order,None,,,,Request }
    { 5   ;   ;Limit Type          ;Option        ;CaptionML=[ENU=Limit Type;
                                                              PTB=Tipo Limite];
                                                   OptionCaptionML=[ENU=Approval Limits,Credit Limits,Request Limits,No Limits;
                                                                    PTB=Limite Aprova��o,Limite Cr�dito,Limite Solicita��o,Sem Limite];
                                                   OptionString=Approval Limits,Credit Limits,Request Limits,No Limits }
    { 6   ;   ;Dimension 1 Code    ;Code20        ;FieldClass=FlowField;
                                                   CalcFormula=Lookup("Approval Templates"."Dimension 1 Code" WHERE (Approval Code=FIELD(Approval Code),
                                                                                                                     Approval Type=FIELD(Approval Type),
                                                                                                                     Document Type=FIELD(Document Type),
                                                                                                                     Limit Type=FIELD(Limit Type)));
                                                   CaptionML=[ENU=Dimension 1 Code;
                                                              PTB=C�d. Dimens�o 1];
                                                   Editable=No }
    { 7   ;   ;Dimension 1 Value Code;Code20      ;TableRelation=IF (Dimension 1 Code=FILTER(<>'')) "Dimension Value".Code WHERE (Dimension Code=FIELD(Dimension 1 Code));
                                                   OnValidate=BEGIN
                                                                UpdateDimSetID;
                                                              END;

                                                   CaptionML=[ENU=Dimension 1 Value Code;
                                                              PTB=C�d. Valor Dimens�o 1] }
    { 8   ;   ;Dimension 2 Code    ;Code20        ;FieldClass=FlowField;
                                                   CalcFormula=Lookup("Approval Templates"."Dimension 2 Code" WHERE (Approval Code=FIELD(Approval Code),
                                                                                                                     Approval Type=FIELD(Approval Type),
                                                                                                                     Document Type=FIELD(Document Type),
                                                                                                                     Limit Type=FIELD(Limit Type)));
                                                   CaptionML=[ENU=Dimension 2 Code;
                                                              PTB=C�d. Dimens�o 2];
                                                   Editable=No }
    { 9   ;   ;Dimension 2 Value Code;Code20      ;TableRelation=IF (Dimension 2 Code=FILTER(<>'')) "Dimension Value".Code WHERE (Dimension Code=FIELD(Dimension 2 Code));
                                                   OnValidate=BEGIN
                                                                UpdateDimSetID;
                                                              END;

                                                   CaptionML=[ENU=Dimension 2 Value Code;
                                                              PTB=C�d. Valor Dimens�o 2] }
    { 11  ;   ;Approver ID         ;Code50        ;TableRelation="User Setup"."User ID";
                                                   ValidateTableRelation=No;
                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=Approver ID;
                                                              PTB=ID Aprovador] }
    { 12  ;   ;Amount Approval Limit;Integer      ;OnValidate=BEGIN
                                                                TESTFIELD("Unlimited Approval", FALSE);
                                                              END;

                                                   CaptionML=[ENU=Amount Limit;
                                                              PTB=Valor Limite];
                                                   BlankZero=Yes }
    { 14  ;   ;Unlimited Approval  ;Boolean       ;OnValidate=BEGIN
                                                                IF "Unlimited Approval" THEN
                                                                  "Amount Approval Limit" := 0;
                                                              END;

                                                   CaptionML=[ENU=Unlimited Approval;
                                                              PTB=Aprova��o Ilimitada] }
    { 480 ;   ;Dimension Set ID    ;Integer       ;TableRelation="Dimension Set Entry" }
  }
  KEYS
  {
    {    ;Approval Code,Approval Type,Document Type,Limit Type,Dimension 1 Value Code,Dimension 2 Value Code,User ID;
                                                   Clustered=Yes }
    {    ;Dimension 1 Value Code,Dimension 2 Value Code,User ID,Unlimited Approval,Amount Approval Limit }
    {    ;Dimension 1 Value Code,Dimension 2 Value Code,Unlimited Approval,Amount Approval Limit,User ID }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      Text001@1000 : TextConst 'ENU=The %1 Salesperson/Purchaser code is already assigned to another User ID %2.;PTB=O C�d. Vendedor/Comprador %1 j� esta determinado a outro ID usu�rio %2.';
      Text003@1002 : TextConst 'ENU="You cannot have both a %1 and %2. ";PTB=Voc� n�o pode ter ambos %1 e %2.';
      Text005@1004 : TextConst 'ENU=You cannot have approval limits less than zero.;PTB=Voc� n�o pode ter limite de aprova��o menor que zero.';

    PROCEDURE UpdateDimSetID@35000001();
    VAR
      tmpDimSetEntry@35000000 : TEMPORARY Record 480;
      dimMgt@35000001 : Codeunit 408;
    BEGIN
      CALCFIELDS("Dimension 1 Code", "Dimension 2 Code");
      IF ("Dimension 1 Code" <> '') AND ("Dimension 1 Value Code" <> '') THEN BEGIN
        tmpDimSetEntry."Dimension Code" := "Dimension 1 Code";
        tmpDimSetEntry.VALIDATE("Dimension Value Code", "Dimension 1 Value Code");
        tmpDimSetEntry.INSERT;
      END;
      IF ("Dimension 2 Code" <> '') AND ("Dimension 2 Value Code" <> '') THEN BEGIN
        tmpDimSetEntry."Dimension Code" := "Dimension 2 Code";
        tmpDimSetEntry.VALIDATE("Dimension Value Code", "Dimension 2 Value Code");
        tmpDimSetEntry.INSERT;
      END;
      IF tmpDimSetEntry.ISEMPTY THEN
        "Dimension Set ID" := 0
      ELSE
        "Dimension Set ID" := dimMgt.GetDimensionSetID(tmpDimSetEntry);
    END;

    BEGIN
    END.
  }
}

