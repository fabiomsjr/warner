OBJECT Codeunit 52112603 DIRF Mgt
{
  OBJECT-PROPERTIES
  {
    Date=30/09/14;
    Time=14:28:01;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    OnRun=BEGIN
          END;

  }
  CODE
  {

    PROCEDURE SuggestLines@52006500(dirf@52006500 : Record 52112603);
    VAR
      createRetTaxData@52006501 : Codeunit 52112525;
      retTaxData@52006503 : TEMPORARY Record 52112586;
      retTaxDataINSS@52006506 : TEMPORARY Record 52112586;
      savedRetTaxData@52006509 : TEMPORARY Record 52112586;
      savedRetTaxEntryNo@52006510 : Integer;
      startDate@52006502 : Date;
      endDate@52006504 : Date;
      dirfLine@52006505 : Record 52112604;
      vendor@52006507 : Record 23;
      vendorRetTaxesBenef@52006508 : Record 52112605;
      textFunctions@1000000000 : Codeunit 52112453;
    BEGIN
      dirf.TESTFIELD("Fiscal Year");

      dirfLine.SETRANGE("DIRF No.", dirf."No.");
      dirfLine.DELETEALL;

      startDate := DMY2DATE(1, 1, dirf."Fiscal Year");
      endDate   := DMY2DATE(31, 12, dirf."Fiscal Year");
      createRetTaxData.GetRetTaxesData(startDate, endDate, '', 1, TRUE, FALSE, retTaxData);

      WITH retTaxData DO BEGIN
        RESET;
        SETFILTER("Tax Identification", '%1|%2', "Tax Identification"::INSS, "Tax Identification"::"INSS Ret.");
        IF FINDSET THEN
          REPEAT
            retTaxDataINSS.INIT;
            retTaxDataINSS.TRANSFERFIELDS(retTaxData);
            retTaxDataINSS.INSERT;
            DELETE;
          UNTIL NEXT = 0;

        RESET;
        SETFILTER("Tax Revenue Code", '<>%1', '');
        IF FINDSET THEN
          REPEAT
            vendor.GET("Customer/Vendor No.");

            dirfLine.INIT;
            dirfLine."DIRF No." := dirf."No.";
            dirfLine."Vendor No." := "Customer/Vendor No.";
            dirfLine."Beneficiary CPFCNPJ" := "Beneficiary CPFCNPJ";
            IF dirfLine."Beneficiary CPFCNPJ" = '' THEN BEGIN
              vendor.GET("Customer/Vendor No.");
              vendor.TESTFIELD(Category);
              vendor.TESTFIELD("C.N.P.J./C.P.F.");
              vendor.TESTFIELD(Name);
              dirfLine."Beneficiary CPFCNPJ" := vendor."C.N.P.J./C.P.F.";
              dirfLine."Beneficiary Name" := vendor.Name;
            END ELSE BEGIN
              vendorRetTaxesBenef.GET(dirfLine."Vendor No.", dirfLine."Beneficiary CPFCNPJ");
              vendorRetTaxesBenef.TESTFIELD(Name);
              dirfLine."Beneficiary Name" := vendorRetTaxesBenef.Name;
            END;

            dirfLine."Tax Revenue Code" := "Tax Revenue Code";
            dirfLine.Month := DATE2DMY("Retention Date", 2);
            IF NOT dirfLine.FIND THEN
              dirfLine.INSERT;

            //Controle para n�o multiplicar bases de C�digos de Receita utilizados em mais de um imposto (ex: PCC);
            savedRetTaxData.RESET;
            savedRetTaxData.SETRANGE("Customer/Vendor No.", "Customer/Vendor No.");
            savedRetTaxData.SETRANGE("Posting Date", "Posting Date");
            savedRetTaxData.SETRANGE("Tax Revenue Code", "Tax Revenue Code");
            savedRetTaxData.SETRANGE("Document No.", "Document No.");
            IF NOT savedRetTaxData.FINDFIRST THEN BEGIN
              dirfLine."Taxed Income" += "Base Amount";

              savedRetTaxEntryNo += 1;
              savedRetTaxData.INIT;
              savedRetTaxData."Entry No." := savedRetTaxEntryNo;
              savedRetTaxData."Customer/Vendor No." := "Customer/Vendor No.";
              savedRetTaxData."Posting Date" := "Posting Date";
              savedRetTaxData."Tax Revenue Code" := "Tax Revenue Code";
              savedRetTaxData."Document No." := "Document No.";
              savedRetTaxData.INSERT;
            END;

            IF vendor.Category = vendor.Category::"1.- Person" THEN BEGIN
              retTaxDataINSS.RESET;
              retTaxDataINSS.SETCURRENTKEY(Type, "Document No.", "Posting Date", "Tax Identification");
              retTaxDataINSS.SETRANGE(Type, Type);
              retTaxDataINSS.SETRANGE("Customer/Vendor No.", "Customer/Vendor No.");
              retTaxDataINSS.SETRANGE("Posting Date", "Posting Date");
              retTaxDataINSS.SETRANGE("Document No.", "Document No.");
              IF retTaxDataINSS.FINDSET THEN
                REPEAT
                  dirfLine."Previdence Deductions" += retTaxDataINSS."Tax Amount";
                UNTIL retTaxDataINSS.NEXT = 0;
            END;

            dirfLine."Retained Taxes" += "Tax Amount";
            dirfLine.UpdateOrder;
            dirfLine.MODIFY;
          UNTIL NEXT = 0;
      END;
    END;

    PROCEDURE ExportFile@52006501(dirf@52006500 : Record 52112603);
    VAR
      _dirf@52006501 : Record 52112603;
      dirfReport@52006502 : Report 52112538;
      fiscalFileExport@52006503 : Codeunit 52112524;
    BEGIN
      _dirf.SETRANGE("No.", dirf."No.");
      dirfReport.SETTABLEVIEW(_dirf);
      dirfReport.USEREQUESTPAGE(FALSE);
      dirfReport.RUN;
      fiscalFileExport.SetDIRFFileType(dirf."No.");
      fiscalFileExport.RUN;
    END;

    BEGIN
    END.
  }
}

