OBJECT Table 269 G/L Account Net Change
{
  OBJECT-PROPERTIES
  {
    Date=07/09/12;
    Time=12:00:00;
    Version List=NAVW17.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=G/L Account Net Change;
               PTB=Saldo Per.];
  }
  FIELDS
  {
    { 1   ;   ;No.                 ;Code20        ;CaptionML=[ENU=No.;
                                                              PTB=N�] }
    { 2   ;   ;Name                ;Text50        ;CaptionML=[ENU=Name;
                                                              PTB=Nome] }
    { 3   ;   ;Net Change in Jnl.  ;Decimal       ;CaptionML=[ENU=Net Change in Jnl.;
                                                              PTB=Saldo Mov. no Di�rio];
                                                   AutoFormatType=1 }
    { 4   ;   ;Balance after Posting;Decimal      ;CaptionML=[ENU=Balance after Posting;
                                                              PTB=Saldo ap�s Registro];
                                                   AutoFormatType=1 }
  }
  KEYS
  {
    {    ;No.                                     ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

