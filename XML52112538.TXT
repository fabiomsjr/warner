OBJECT XMLport 52112538 Siscoserv CancelarPagamento
{
  OBJECT-PROPERTIES
  {
    Date=21/07/15;
    Time=16:38:04;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    Direction=Export;
    Encoding=UTF-8;
    Format/Evaluate=XML Format/Evaluate;
  }
  ELEMENTS
  {
    { [{4789AA0B-8276-4CF3-B458-1A5FE61D42B9}];  ;CancelarPagamento   ;Element ;Table   ;
                                                  ReqFilterFields=Field10;
                                                  VariableName=RP;
                                                  SourceTable=Table52112689;
                                                  SourceTableView=SORTING(Field10);
                                                  Export::OnAfterGetRecord=BEGIN
                                                                             RP.GetXMLData;
                                                                           END;
                                                                            }

    { [{ADE58FCB-52CC-41C2-AAFA-02ED0DFA9001}];1 ;IdPagamentoEmpresa  ;Element ;Field   ;
                                                  DataType=Code;
                                                  SourceField=RP::Document No. }

  }
  EVENTS
  {
  }
  REQUESTPAGE
  {
    PROPERTIES
    {
    }
    CONTROLS
    {
    }
  }
  CODE
  {
    VAR
      MoreThan8NotAllowedErr@52006500 : TextConst 'ENU=It is not allowed to have more than eight %1 in an RF.;PTB=N�o s�o permitidos mais que oito %1 em uma RF.';

    BEGIN
    END.
  }
}

