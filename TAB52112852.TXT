OBJECT Table 52112852 Reference Chart of Accounts
{
  OBJECT-PROPERTIES
  {
    Date=08/05/15;
    Time=08:25:53;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    OnDelete=VAR
               relation@52006500 : Record 52112853;
             BEGIN
               relation.SETRANGE("Account Plan Reference", No);
               relation.DELETEALL;
             END;

    CaptionML=[ENU=Reference Chart of Accounts;
               PTB=Plano de Contas Referencial];
    LookupPageID=Page52112868;
    DrillDownPageID=Page52112868;
  }
  FIELDS
  {
    { 1   ;   ;No                  ;Text20        ;CaptionML=[ENU=No;
                                                              PTB=N�] }
    { 2   ;   ;Name                ;Text80        ;CaptionML=[ENU=Name;
                                                              PTB=Nome] }
    { 3   ;   ;Account Type        ;Option        ;CaptionML=[ENU=Account Type;
                                                              PTB=Tipo Conta];
                                                   OptionCaptionML=[ENU=" ,Heading,Posting";
                                                                    PTB=" ,Sint�tica,Anal�tica"];
                                                   OptionString=[ ,Heading,Posting] }
  }
  KEYS
  {
    {    ;No                                      ;Clustered=Yes }
  }
  FIELDGROUPS
  {
    { 1   ;DropDown            ;No,Name,Account Type                     }
  }
  CODE
  {

    BEGIN
    END.
  }
}

