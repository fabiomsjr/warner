OBJECT Page 52112690 Siscoserv RAS Subform LF
{
  OBJECT-PROPERTIES
  {
    Date=21/07/15;
    Time=15:02:05;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Legal Frameworks;
               PTB=Enquadramentos];
    SourceTable=Table52112686;
    DelayedInsert=Yes;
    PageType=List;
  }
  CONTROLS
  {
    { 52006500;0;Container;
                ContainerType=ContentArea }

    { 52006501;1;Group    ;
                Name=Group;
                GroupType=Repeater }

    { 52006502;2;Field    ;
                SourceExpr="Statute Code";
                OnValidate=BEGIN
                             CALCFIELDS(Description);
                           END;
                            }

    { 52006503;2;Field    ;
                SourceExpr=Description }

    { 52006504;2;Field    ;
                SourceExpr="Credit Reg. No." }

  }
  CODE
  {

    BEGIN
    END.
  }
}

