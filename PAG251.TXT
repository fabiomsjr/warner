OBJECT Page 251 General Journal Batches
{
  OBJECT-PROPERTIES
  {
    Date=21/03/12;
    Time=13:42:21;
    Modified=Yes;
    Version List=NAVW18.00,NAVBR7,WCMW1601_01_3;
  }
  PROPERTIES
  {
    CaptionML=[ENU=General Journal Batches;
               PTB=Se��es Di�rio Geral];
    SourceTable=Table232;
    DataCaptionExpr=DataCaption;
    PageType=List;
    OnOpenPage=BEGIN
                 GenJnlManagement.OpenJnlBatch(Rec);

                 // > NAVBR
                 SetupPage;
                 // < NAVBR
               END;

    OnNewRecord=BEGIN
                  SetupNewBatch;
                END;

    ActionList=ACTIONS
    {
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 29      ;1   ;Action    ;
                      Name=EditJournal;
                      ShortCutKey=Return;
                      CaptionML=[ENU=Edit Journal;
                                 PTB=Editar Di�rio];
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=OpenJournal;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 GenJnlManagement.TemplateSelectionFromBatch(Rec);
                               END;
                                }
      { 15      ;1   ;ActionGroup;
                      CaptionML=[ENU=P&osting;
                                 PTB=Re&gistro];
                      Image=Post }
      { 23      ;2   ;Action    ;
                      ShortCutKey=Ctrl+F11;
                      CaptionML=[ENU=Reconcile;
                                 PTB=Reconciliar];
                      Image=Reconcile;
                      OnAction=BEGIN
                                 GenJnlLine.SETRANGE("Journal Template Name","Journal Template Name");
                                 GenJnlLine.SETRANGE("Journal Batch Name",Name);
                                 GLReconcile.SetGenJnlLine(GenJnlLine);
                                 GLReconcile.RUN;
                               END;
                                }
      { 16      ;2   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[ENU=Test Report;
                                 PTB=Relat�rio Verifica��o];
                      Image=TestReport;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 ReportPrint.PrintGenJnlBatch(Rec);
                               END;
                                }
      { 17      ;2   ;Action    ;
                      ShortCutKey=F9;
                      CaptionML=[ENU=P&ost;
                                 PTB=Re&gistrar];
                      RunObject=Codeunit 233;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=PostOrder;
                      PromotedCategory=Process }
      { 18      ;2   ;Action    ;
                      ShortCutKey=Shift+F9;
                      CaptionML=[ENU=Post and &Print;
                                 PTB=Registrar e Im&primir];
                      RunObject=Codeunit 234;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=PostPrint;
                      PromotedCategory=Process }
      { 8       ;1   ;ActionGroup;
                      CaptionML=ENU=Periodic Activities }
      { 7       ;2   ;Action    ;
                      CaptionML=ENU=Recurring General Journal;
                      RunObject=Page 283;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=Journal;
                      PromotedCategory=Process }
      { 24      ;2   ;Action    ;
                      CaptionML=ENU=Close Income Statement;
                      RunObject=Report 94;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=CloseYear;
                      PromotedCategory=Process }
      { 9       ;2   ;Action    ;
                      CaptionML=ENU=G/L Register;
                      RunObject=Page 116;
                      Promoted=Yes;
                      Image=GLRegisters;
                      PromotedCategory=Process }
      { 22      ;    ;ActionContainer;
                      ActionContainerType=Reports }
      { 20      ;1   ;Action    ;
                      CaptionML=ENU=Detail Trial Balance;
                      RunObject=Report 4;
                      Promoted=Yes;
                      Image=Report;
                      PromotedCategory=Report }
      { 14      ;1   ;Action    ;
                      CaptionML=ENU=Trial Balance;
                      RunObject=Report 6;
                      Promoted=Yes;
                      Image=Report;
                      PromotedCategory=Report }
      { 12      ;1   ;Action    ;
                      CaptionML=ENU=Trial Balance by Period;
                      RunObject=Report 38;
                      Promoted=No;
                      Image=Report;
                      PromotedCategory=Report }
      { 10      ;1   ;Action    ;
                      CaptionML=ENU=G/L Register;
                      RunObject=Report 3;
                      Promoted=Yes;
                      Image=GLRegisters;
                      PromotedCategory=Report }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                SourceExpr=Name }

    { 4   ;2   ;Field     ;
                SourceExpr=Description }

    { 11  ;2   ;Field     ;
                SourceExpr="Bal. Account Type" }

    { 13  ;2   ;Field     ;
                SourceExpr="Bal. Account No." }

    { 19  ;2   ;Field     ;
                SourceExpr="No. Series" }

    { 21  ;2   ;Field     ;
                SourceExpr="Posting No. Series" }

    { 6   ;2   ;Field     ;
                SourceExpr="Reason Code" }

    { 25  ;2   ;Field     ;
                SourceExpr="Copy VAT Setup to Jnl. Lines";
                Visible=FALSE }

    { 27  ;2   ;Field     ;
                SourceExpr="Allow VAT Difference";
                Visible=FALSE }

    { 52006500;2;Field    ;
                SourceExpr="Approval Batch";
                Visible=ApprovalEnabled;
                Editable=FALSE }

    { 52006501;2;Field    ;
                SourceExpr=Status;
                Visible=ApprovalEnabled;
                Editable=FALSE }

    { 52006502;2;Field    ;
                SourceExpr="Approval-From Batch Name";
                Visible=FALSE;
                Editable=FALSE }

    { 3   ;2   ;Field     ;
                SourceExpr="Allow Payment Export" }

    { 5   ;2   ;Field     ;
                SourceExpr="Bank Statement Import Format";
                Visible=FALSE }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      ReportPrint@1000 : Codeunit 228;
      GenJnlManagement@1003 : Codeunit 230;
      GenJnlLine@1001 : Record 81;
      GLReconcile@1002 : Page 345;
      ">NAVBR VARS"@52006500 : Integer;
      ApprovalEnabled@52006501 : Boolean;

    LOCAL PROCEDURE DataCaption@1() : Text[250];
    VAR
      GenJnlTemplate@1000 : Record 80;
    BEGIN
      IF NOT CurrPage.LOOKUPMODE THEN
        IF GETFILTER("Journal Template Name") <> '' THEN
          IF GETRANGEMIN("Journal Template Name") = GETRANGEMAX("Journal Template Name") THEN
            IF GenJnlTemplate.GET(GETRANGEMIN("Journal Template Name")) THEN
              EXIT(GenJnlTemplate.Name + ' ' + GenJnlTemplate.Description);
    END;

    LOCAL PROCEDURE ">NAVBR FUNC"@52006501();
    BEGIN
    END;

    LOCAL PROCEDURE SetupPage@52006502();
    VAR
      GenJnlApprovalMgt@52006500 : Codeunit 52113074;
    BEGIN
      ApprovalEnabled := GenJnlApprovalMgt.IsApprovalEnabled;
      IF ApprovalEnabled THEN
        SETCURRENTKEY("Approval Batch");
    END;

    BEGIN
    {
      001  13/10/09  JJB *  Promote Terst report
    }
    END.
  }
}

