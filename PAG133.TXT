OBJECT Page 133 Posted Sales Invoice Subform
{
  OBJECT-PROPERTIES
  {
    Date=07/07/15;
    Time=08:53:23;
    Version List=NAVW18.00,NAVBR7;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Lines;
               PTB=Linhas];
    LinksAllowed=No;
    SourceTable=Table113;
    PageType=ListPart;
    AutoSplitKey=Yes;
    OnAfterGetCurrRecord=BEGIN
                           DocumentTotals.CalculatePostedSalesInvoiceTotals(TotalSalesInvoiceHeader,VATAmount,Rec);
                         END;

    ActionList=ACTIONS
    {
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 1907935204;1 ;ActionGroup;
                      CaptionML=[ENU=&Line;
                                 PTB=&Linha];
                      Image=Line }
      { 1901314304;2 ;Action    ;
                      AccessByPermission=TableData 348=R;
                      ShortCutKey=Shift+Ctrl+D;
                      CaptionML=[ENU=Dimensions;
                                 PTB=Dimens�es];
                      Image=Dimensions;
                      OnAction=BEGIN
                                 ShowDimensions;
                               END;
                                }
      { 52006502;2   ;Action    ;
                      Name=DimAlloc;
                      ShortCutKey=Shift+Ctrl+R;
                      CaptionML=[ENU=Dimensions Allocation;
                                 PTB=Rateio Dimens�es];
                      Image=Allocate;
                      OnAction=VAR
                                 docLineDimAlloc@52006500 : Page 52112485;
                               BEGIN
                                 docLineDimAlloc.ShowAllocation("Dimension Allocation ID", "Line Amount");
                               END;
                                }
      { 1900639404;2 ;Action    ;
                      CaptionML=[ENU=Co&mments;
                                 PTB=Co&ment�rios];
                      Image=ViewComments;
                      OnAction=BEGIN
                                 ShowLineComments;
                               END;
                                }
      { 1905987604;2 ;Action    ;
                      Name=ItemTrackingEntries;
                      CaptionML=[ENU=Item &Tracking Entries;
                                 PTB=Linhas Ras&treabilidade Produto];
                      Image=ItemTrackingLedger;
                      OnAction=BEGIN
                                 ShowItemTrackingLines;
                               END;
                                }
      { 1905427604;2 ;Action    ;
                      CaptionML=[ENU=Item Shipment &Lines;
                                 PTB=Linhas Remessa];
                      Image=ShipmentLines;
                      OnAction=BEGIN
                                 IF NOT (Type IN [Type::Item,Type::"Charge (Item)"]) THEN
                                   TESTFIELD(Type);
                                 ShowItemShipmentLines;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                SourceExpr=Type }

    { 4   ;2   ;Field     ;
                SourceExpr="No." }

    { 26  ;2   ;Field     ;
                SourceExpr="Cross-Reference No.";
                Visible=FALSE }

    { 20  ;2   ;Field     ;
                SourceExpr="IC Partner Code";
                Visible=FALSE }

    { 18  ;2   ;Field     ;
                SourceExpr="Variant Code";
                Visible=FALSE }

    { 6   ;2   ;Field     ;
                SourceExpr=Description }

    { 35000001;2;Field    ;
                SourceExpr="Description 2";
                Visible=FALSE }

    { 14  ;2   ;Field     ;
                SourceExpr="Return Reason Code";
                Visible=FALSE }

    { 8   ;2   ;Field     ;
                BlankZero=Yes;
                SourceExpr=Quantity }

    { 24  ;2   ;Field     ;
                SourceExpr="Unit of Measure Code" }

    { 10  ;2   ;Field     ;
                SourceExpr="Unit of Measure";
                Visible=FALSE }

    { 66  ;2   ;Field     ;
                SourceExpr="Unit Cost (LCY)";
                Visible=FALSE }

    { 12  ;2   ;Field     ;
                BlankZero=Yes;
                SourceExpr="Unit Price" }

    { 30  ;2   ;Field     ;
                BlankZero=Yes;
                SourceExpr="Line Amount" }

    { 52006508;2;Field    ;
                SourceExpr="Operation Type" }

    { 1102300009;2;Field  ;
                SourceExpr="Tax Area Code" }

    { 1102300007;2;Field  ;
                SourceExpr="CFOP Code" }

    { 1102300006;2;Field  ;
                SourceExpr="NCM Code" }

    { 52006500;2;Field    ;
                SourceExpr="NCM Exception Code";
                Visible=FALSE }

    { 52006503;2;Field    ;
                SourceExpr="Tax Exception Code" }

    { 52006504;2;Field    ;
                SourceExpr="End User";
                Visible=FALSE }

    { 52006501;2;Field    ;
                SourceExpr="Origin Code" }

    { 1102300005;2;Field  ;
                SourceExpr="ICMS CST Code" }

    { 1102300004;2;Field  ;
                SourceExpr="PIS CST Code" }

    { 1102300003;2;Field  ;
                SourceExpr="COFINS CST Code" }

    { 1102300002;2;Field  ;
                SourceExpr="IPI CST Code" }

    { 1102300001;2;Field  ;
                SourceExpr="PIS CST Income Nature";
                Visible=false }

    { 1102300000;2;Field  ;
                SourceExpr="COFINS CST Income Nature";
                Visible=false }

    { 52006506;2;Field    ;
                SourceExpr="Estimated Tax % (IBPT)" }

    { 52006507;2;Field    ;
                BlankZero=Yes;
                SourceExpr="Estimated Tax Amount (IBPT)" }

    { 16  ;2   ;Field     ;
                BlankZero=Yes;
                SourceExpr="Line Discount %" }

    { 52  ;2   ;Field     ;
                SourceExpr="Line Discount Amount";
                Visible=FALSE }

    { 50  ;2   ;Field     ;
                SourceExpr="Allow Invoice Disc.";
                Visible=FALSE }

    { 40  ;2   ;Field     ;
                SourceExpr="Job No.";
                Visible=FALSE }

    { 44  ;2   ;Field     ;
                SourceExpr="Appl.-to Item Entry";
                Visible=FALSE }

    { 72  ;2   ;Field     ;
                SourceExpr="Shortcut Dimension 1 Code";
                Visible=FALSE }

    { 70  ;2   ;Field     ;
                SourceExpr="Shortcut Dimension 2 Code";
                Visible=FALSE }

    { 52006505;2;Field    ;
                SourceExpr="Line No.";
                Visible=FALSE;
                Editable=FALSE }

    { 52006509;2;Field    ;
                SourceExpr="FCI No.";
                Visible=FALSE }

    { 28  ;1   ;Group     ;
                GroupType=Group }

    { 23  ;2   ;Group     ;
                GroupType=Group }

    { 22  ;3   ;Field     ;
                Name=Invoice Discount Amount;
                CaptionML=ENU=Invoice Discount Amount;
                SourceExpr=TotalSalesInvoiceHeader."Invoice Discount Amount";
                AutoFormatType=1;
                AutoFormatExpr=TotalSalesInvoiceHeader."Currency Code";
                CaptionClass=DocumentTotals.GetInvoiceDiscAmountWithVATCaption(TotalSalesInvoiceHeader."Prices Including VAT");
                Editable=FALSE }

    { 9   ;2   ;Group     ;
                GroupType=Group }

    { 7   ;3   ;Field     ;
                Name=Total Amount Excl. VAT;
                DrillDown=No;
                CaptionML=ENU=Total Amount Excl. VAT;
                SourceExpr=TotalSalesInvoiceHeader.Amount;
                AutoFormatType=1;
                AutoFormatExpr=TotalSalesInvoiceHeader."Currency Code";
                CaptionClass=DocumentTotals.GetTotalExclVATCaption(TotalSalesInvoiceHeader."Currency Code");
                Editable=FALSE }

    { 5   ;3   ;Field     ;
                Name=Total VAT Amount;
                CaptionML=ENU=Total VAT;
                SourceExpr=VATAmount;
                AutoFormatType=1;
                AutoFormatExpr=TotalSalesInvoiceHeader."Currency Code";
                CaptionClass=DocumentTotals.GetTotalVATCaption(TotalSalesInvoiceHeader."Currency Code");
                Editable=FALSE }

    { 3   ;3   ;Field     ;
                Name=Total Amount Incl. VAT;
                CaptionML=ENU=Total Amount Incl. VAT;
                SourceExpr=TotalSalesInvoiceHeader."Amount Including VAT";
                AutoFormatType=1;
                AutoFormatExpr=TotalSalesInvoiceHeader."Currency Code";
                CaptionClass=DocumentTotals.GetTotalInclVATCaption(TotalSalesInvoiceHeader."Currency Code");
                Editable=FALSE;
                Style=Strong;
                StyleExpr=TRUE }

  }
  CODE
  {
    VAR
      TotalSalesInvoiceHeader@1002 : Record 112;
      DocumentTotals@1001 : Codeunit 57;
      VATAmount@1000 : Decimal;

    BEGIN
    END.
  }
}

