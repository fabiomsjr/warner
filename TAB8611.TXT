OBJECT Table 8611 Config. Question Area
{
  OBJECT-PROPERTIES
  {
    Date=07/09/12;
    Time=12:00:00;
    Version List=NAVW17.00;
  }
  PROPERTIES
  {
    OnDelete=VAR
               ConfigQuestion@1000 : Record 8612;
             BEGIN
               ConfigQuestion.RESET;
               ConfigQuestion.SETRANGE("Questionnaire Code","Questionnaire Code");
               ConfigQuestion.SETRANGE("Question Area Code",Code);
               ConfigQuestion.DELETEALL;
             END;

    OnRename=BEGIN
               ERROR(Text003);
             END;

    CaptionML=[ENU=Config. Question Area;
               PTB=�rea de Perguntas];
    LookupPageID=Page8613;
  }
  FIELDS
  {
    { 1   ;   ;Questionnaire Code  ;Code10        ;TableRelation="Config. Questionnaire";
                                                   CaptionML=[ENU=Questionnaire Code;
                                                              PTB=C�d. Question�rio];
                                                   NotBlank=Yes;
                                                   Editable=No }
    { 2   ;   ;Code                ;Code10        ;OnValidate=VAR
                                                                TestValue@1000 : Integer;
                                                              BEGIN
                                                                IF EVALUATE(TestValue,COPYSTR(Code,1,1)) THEN
                                                                  ERROR(Text002);
                                                              END;

                                                   CaptionML=[ENU=Code;
                                                              PTB=C�digo];
                                                   NotBlank=Yes }
    { 3   ;   ;Description         ;Text50        ;CaptionML=[ENU=Description;
                                                              PTB=Descri��o] }
    { 4   ;   ;Table ID            ;Integer       ;TableRelation=Object.ID WHERE (Type=FILTER(Table));
                                                   OnValidate=VAR
                                                                ConfigQuestion@1000 : Record 8612;
                                                                ConfigQuestionArea@1001 : Record 8611;
                                                              BEGIN
                                                                IF (xRec."Table ID" <> "Table ID") AND (xRec."Table ID" > 0) THEN BEGIN
                                                                  ConfigQuestion.SETRANGE("Questionnaire Code","Questionnaire Code");
                                                                  ConfigQuestion.SETRANGE("Question Area Code",Code);
                                                                  IF NOT ConfigQuestion.ISEMPTY THEN
                                                                    ERROR(STRSUBSTNO(Text000,Code));
                                                                  ConfigQuestionArea.SETRANGE("Questionnaire Code","Questionnaire Code");
                                                                  ConfigQuestionArea.SETRANGE("Table ID","Table ID");
                                                                  IF NOT ConfigQuestionArea.ISEMPTY THEN
                                                                    ERROR(STRSUBSTNO(Text001,"Table ID"));
                                                                END;
                                                                CALCFIELDS("Table Name","Table Caption");
                                                              END;

                                                   OnLookup=VAR
                                                              ConfigValidateMgt@1000 : Codeunit 8617;
                                                            BEGIN
                                                              ConfigValidateMgt.LookupTable("Table ID");
                                                              VALIDATE("Table ID");
                                                            END;

                                                   CaptionML=[ENU=Table ID;
                                                              PTB=TableID] }
    { 5   ;   ;Table Name          ;Text250       ;FieldClass=FlowField;
                                                   CalcFormula=Lookup(AllObjWithCaption."Object Name" WHERE (Object Type=CONST(Table),
                                                                                                             Object ID=FIELD(Table ID)));
                                                   CaptionML=[ENU=Table Name;
                                                              PTB=Nome Tabela];
                                                   Editable=No }
    { 6   ;   ;Table Caption       ;Text250       ;FieldClass=FlowField;
                                                   CalcFormula=Lookup(AllObjWithCaption."Object Caption" WHERE (Object Type=CONST(Table),
                                                                                                                Object ID=FIELD(Table ID)));
                                                   CaptionML=[ENU=Table Caption;
                                                              PTB=Caption Tabela] }
    { 7   ;   ;No. of Questions    ;Integer       ;FieldClass=FlowField;
                                                   CalcFormula=Count("Config. Question" WHERE (Questionnaire Code=FIELD(Questionnaire Code),
                                                                                               Question Area Code=FIELD(Code)));
                                                   CaptionML=ENU=No. of Questions;
                                                   Editable=No }
  }
  KEYS
  {
    {    ;Questionnaire Code,Code                 ;Clustered=Yes }
    {    ;Table ID                                 }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      Text000@1003 : TextConst 'ENU=Delete questions for question area %1 to change the table relationship.';
      Text001@1000 : TextConst 'ENU=A question area already exists for table %1.;PTB=Perguntas para tabela %1 j� existem.';
      Text002@1001 : TextConst 'ENU=The first character cannot be a numeric value.;PTB=O primeiro caracter n�o pode ser um valor num�rico.';
      Text003@1002 : TextConst 'ENU=You cannot rename a question area.;PTB=Voc� n�o pode renomear uma �rea Quest�es.';

    BEGIN
    END.
  }
}

