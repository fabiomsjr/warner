OBJECT Table 35000666 Credits Average 20092
{
  OBJECT-PROPERTIES
  {
    Date=29/02/12;
    Time=14:16:00;
    Version List=FNX023.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Credits Average;
               PTB=Rateio Cr�ditos];
    LookupPageID=Page35000666;
    DrillDownPageID=Page35000666;
  }
  FIELDS
  {
    { 1   ;   ;Tax Settlement Code ;Code20        ;CaptionML=PTB=N� }
    { 4   ;   ;Non Cumulative Rec. ;Decimal       ;CaptionML=PTB=Rec. N�o Cumulativa Trib. }
    { 5   ;   ;Non Cumulative Rec. Non Taxed;Decimal;
                                                   CaptionML=PTB=Rec. N�o Cumulativa N�o Trib. }
    { 6   ;   ;Non Cumulative Rec. Export.;Decimal;CaptionML=PTB=Rec. N�o Cumulativa Exporta��o }
    { 7   ;   ;Cumulative Receipt  ;Decimal       ;CaptionML=PTB=Rec. Cumulativa }
    { 8   ;   ;Total Receipt       ;Decimal       ;CaptionML=PTB=Rec. Total }
  }
  KEYS
  {
    {    ;Tax Settlement Code                     ;SumIndexFields=Non Cumulative Rec.,Non Cumulative Rec. Non Taxed,Non Cumulative Rec. Export.,Cumulative Receipt,Total Receipt;
                                                   Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

