OBJECT Page 52113028 CNAB Record Subform
{
  OBJECT-PROPERTIES
  {
    Date=02/08/13;
    Time=15:16:00;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Fields;
               PTB=Campos];
    SourceTable=Table52113026;
    PageType=ListPart;
    OnNewRecord=BEGIN
                  "Order No." := xRec."Order No." + 1;
                END;

  }
  CONTROLS
  {
    { 35000000;0;Container;
                ContainerType=ContentArea }

    { 35000001;1;Group    ;
                Name=Group;
                GroupType=Repeater }

    { 35000002;2;Field    ;
                SourceExpr="Order No." }

    { 35000007;2;Field    ;
                CaptionML=[ENU=Position;
                           PTB=Posi��o];
                SourceExpr=GetPosition;
                Style=StandardAccent;
                StyleExpr=TRUE }

    { 35000005;2;Field    ;
                SourceExpr=Description }

    { 35000004;2;Field    ;
                SourceExpr=Length }

    { 35000003;2;Field    ;
                SourceExpr=Type }

    { 35000006;2;Field    ;
                SourceExpr=Mandatory }

    { 52006502;2;Field    ;
                SourceExpr="Return Code" }

    { 35000009;2;Field    ;
                SourceExpr="Fixed Content" }

    { 52006501;2;Field    ;
                SourceExpr="Decimals No." }

    { 35000010;2;Field    ;
                SourceExpr="Field Code" }

    { 35000011;2;Field    ;
                SourceExpr="Sub Record Code" }

    { 52006500;2;Field    ;
                SourceExpr="Custom Field Code" }

  }
  CODE
  {

    BEGIN
    END.
  }
}

