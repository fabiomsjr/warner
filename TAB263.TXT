OBJECT Table 263 Intrastat Jnl. Line
{
  OBJECT-PROPERTIES
  {
    Date=09/09/14;
    Time=12:00:00;
    Version List=NAVW18.00;
  }
  PROPERTIES
  {
    OnInsert=BEGIN
               IntraJnlTemplate.GET("Journal Template Name");
               IntrastatJnlBatch.GET("Journal Template Name","Journal Batch Name");
             END;

    OnModify=BEGIN
               IntrastatJnlBatch.GET("Journal Template Name","Journal Batch Name");
               IntrastatJnlBatch.TESTFIELD(Reported,FALSE);
             END;

    OnRename=BEGIN
               IntrastatJnlBatch.GET(xRec."Journal Template Name",xRec."Journal Batch Name");
               IntrastatJnlBatch.TESTFIELD(Reported,FALSE);
             END;

    CaptionML=[ENU=Intrastat Jnl. Line;
               PTB=Linha Di�rio Intrastat];
  }
  FIELDS
  {
    { 1   ;   ;Journal Template Name;Code10       ;TableRelation="Intrastat Jnl. Template";
                                                   CaptionML=[ENU=Journal Template Name;
                                                              PTB=Nome Livro Di�rio] }
    { 2   ;   ;Journal Batch Name  ;Code10        ;TableRelation="Intrastat Jnl. Batch".Name WHERE (Journal Template Name=FIELD(Journal Template Name));
                                                   CaptionML=[ENU=Journal Batch Name;
                                                              PTB=Nome Se��o Di�rio] }
    { 3   ;   ;Line No.            ;Integer       ;CaptionML=[ENU=Line No.;
                                                              PTB=N� Linha] }
    { 4   ;   ;Type                ;Option        ;CaptionML=[ENU=Type;
                                                              PTB=Tipo];
                                                   OptionCaptionML=[ENU=Receipt,Shipment;
                                                                    PTB=Introdu��o,Expedi��o];
                                                   OptionString=Receipt,Shipment }
    { 5   ;   ;Date                ;Date          ;CaptionML=[ENU=Date;
                                                              PTB=Data] }
    { 6   ;   ;Tariff No.          ;Code20        ;TableRelation="Tariff Number";
                                                   OnValidate=BEGIN
                                                                TESTFIELD("Item No.",'');
                                                                GetItemDescription;
                                                              END;

                                                   CaptionML=[ENU=Tariff No.;
                                                              PTB=Cod. Aduaneiro];
                                                   NotBlank=Yes }
    { 7   ;   ;Item Description    ;Text50        ;CaptionML=[ENU=Item Description;
                                                              PTB=Descri��o Produto] }
    { 8   ;   ;Country/Region Code ;Code10        ;TableRelation=Country/Region;
                                                   CaptionML=[ENU=Country/Region Code;
                                                              PTB=C�d. Pa�s/Regi�o] }
    { 9   ;   ;Transaction Type    ;Code10        ;TableRelation="Transaction Type";
                                                   CaptionML=[ENU=Transaction Type;
                                                              PTB=Natureza Transa��o] }
    { 10  ;   ;Transport Method    ;Code10        ;TableRelation="Transport Method";
                                                   CaptionML=[ENU=Transport Method;
                                                              PTB=Modo Transporte] }
    { 11  ;   ;Source Type         ;Option        ;CaptionML=[ENU=Source Type;
                                                              PTB=Tipo Origem];
                                                   OptionCaptionML=[ENU=,Item Entry,Job Entry;
                                                                    PTB=,Mov. produto,Mov. Projeto];
                                                   OptionString=,Item Entry,Job Entry;
                                                   BlankZero=Yes }
    { 12  ;   ;Source Entry No.    ;Integer       ;TableRelation=IF (Source Type=CONST(Item Entry)) "Item Ledger Entry"
                                                                 ELSE IF (Source Type=CONST(Job Entry)) "Job Ledger Entry";
                                                   CaptionML=[ENU=Source Entry No.;
                                                              PTB=N� Ordem Mov. Proced�ncia];
                                                   Editable=No }
    { 13  ;   ;Net Weight          ;Decimal       ;OnValidate=BEGIN
                                                                IF Quantity <> 0 THEN
                                                                  "Total Weight" := ROUND("Net Weight" * Quantity,0.00001)
                                                                ELSE
                                                                  "Total Weight" := 0;
                                                              END;

                                                   CaptionML=[ENU=Net Weight;
                                                              PTB=Peso L�quido];
                                                   DecimalPlaces=2:5 }
    { 14  ;   ;Amount              ;Decimal       ;OnValidate=BEGIN
                                                                IF "Cost Regulation %" <> 0 THEN
                                                                  VALIDATE("Cost Regulation %")
                                                                ELSE
                                                                  "Statistical Value" := Amount + "Indirect Cost";
                                                              END;

                                                   CaptionML=[ENU=Amount;
                                                              PTB=Valor];
                                                   DecimalPlaces=0:0 }
    { 15  ;   ;Quantity            ;Decimal       ;OnValidate=BEGIN
                                                                IF (Quantity <> 0) AND Item.GET("Item No.") THEN
                                                                  VALIDATE("Net Weight",Item."Net Weight")
                                                                ELSE
                                                                  VALIDATE("Net Weight",0);
                                                              END;

                                                   CaptionML=[ENU=Quantity;
                                                              PTB=Quantidade];
                                                   DecimalPlaces=0:0 }
    { 16  ;   ;Cost Regulation %   ;Decimal       ;OnValidate=BEGIN
                                                                "Indirect Cost" := ROUND(Amount * "Cost Regulation %" / 100,1);
                                                                "Statistical Value" := ROUND(Amount + "Indirect Cost",1);
                                                              END;

                                                   CaptionML=[ENU=Cost Regulation %;
                                                              PTB=% Custo Regulamento];
                                                   DecimalPlaces=2:2;
                                                   MinValue=-100;
                                                   MaxValue=100 }
    { 17  ;   ;Indirect Cost       ;Decimal       ;OnValidate=BEGIN
                                                                "Cost Regulation %" := 0;
                                                                "Statistical Value" := Amount + "Indirect Cost";
                                                              END;

                                                   CaptionML=[ENU=Indirect Cost;
                                                              PTB=Custo Indireto];
                                                   DecimalPlaces=0:0 }
    { 18  ;   ;Statistical Value   ;Decimal       ;CaptionML=[ENU=Statistical Value;
                                                              PTB=Valor Estat�stico];
                                                   DecimalPlaces=0:0;
                                                   Editable=No }
    { 19  ;   ;Document No.        ;Code20        ;CaptionML=[ENU=Document No.;
                                                              PTB=No. Documento] }
    { 20  ;   ;Item No.            ;Code20        ;TableRelation=Item;
                                                   OnValidate=BEGIN
                                                                TESTFIELD("Source Type",0);

                                                                IF "Item No." = '' THEN
                                                                  CLEAR(Item)
                                                                ELSE BEGIN
                                                                  Item.GET("Item No.");
                                                                  Item.TESTFIELD("Tariff No.");
                                                                END;

                                                                Name := Item.Description;
                                                                "Tariff No." := Item."Tariff No.";
                                                                "Country/Region of Origin Code" := Item."Country/Region of Origin Code";
                                                                GetItemDescription;
                                                              END;

                                                   CaptionML=[ENU=Item No.;
                                                              PTB=No. Produto] }
    { 21  ;   ;Name                ;Text50        ;CaptionML=[ENU=Name;
                                                              PTB=Nome] }
    { 22  ;   ;Total Weight        ;Decimal       ;CaptionML=[ENU=Total Weight;
                                                              PTB=Peso Total];
                                                   DecimalPlaces=0:0;
                                                   Editable=No }
    { 23  ;   ;Supplementary Units ;Boolean       ;CaptionML=[ENU=Supplementary Units;
                                                              PTB=Unidades Suplementares];
                                                   Editable=No }
    { 24  ;   ;Internal Ref. No.   ;Text10        ;CaptionML=[ENU=Internal Ref. No.;
                                                              PTB=N� Ref. Interno];
                                                   Editable=No }
    { 25  ;   ;Country/Region of Origin Code;Code10;
                                                   TableRelation=Country/Region;
                                                   CaptionML=[ENU=Country/Region of Origin Code;
                                                              PTB=C�d. Pa�s/Regi�o de Origem] }
    { 26  ;   ;Entry/Exit Point    ;Code10        ;TableRelation="Entry/Exit Point";
                                                   CaptionML=[ENU=Entry/Exit Point;
                                                              PTB=Porto/Aeroporto] }
    { 27  ;   ;Area                ;Code10        ;TableRelation=Area;
                                                   CaptionML=[ENU=Area;
                                                              PTB=�rea] }
    { 28  ;   ;Transaction Specification;Code10   ;TableRelation="Transaction Specification";
                                                   CaptionML=[ENU=Transaction Specification;
                                                              PTB=Especifica�ao Transa��o] }
  }
  KEYS
  {
    {    ;Journal Template Name,Journal Batch Name,Line No.;
                                                   SumIndexFields=Statistical Value;
                                                   MaintainSIFTIndex=No;
                                                   Clustered=Yes }
    {    ;Source Type,Source Entry No.             }
    {    ;Type,Country/Region Code,Tariff No.,Transaction Type,Transport Method }
    {    ;Internal Ref. No.                        }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      IntraJnlTemplate@1000 : Record 261;
      IntrastatJnlBatch@1001 : Record 262;
      Item@1002 : Record 27;
      TariffNumber@1003 : Record 260;

    LOCAL PROCEDURE GetItemDescription@1();
    BEGIN
      IF "Tariff No." <> '' THEN BEGIN
        TariffNumber.GET("Tariff No.");
        "Item Description" := TariffNumber.Description;
        "Supplementary Units" := TariffNumber."Supplementary Units";
      END ELSE
        "Item Description" := '';
    END;

    BEGIN
    END.
  }
}

