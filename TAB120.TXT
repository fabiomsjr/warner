OBJECT Table 120 Purch. Rcpt. Header
{
  OBJECT-PROPERTIES
  {
    Date=13/10/14;
    Time=18:29:08;
    Modified=Yes;
    Version List=NAVW18.00,NAVBR7,WAR003.00;
  }
  PROPERTIES
  {
    DataCaptionFields=No.,Buy-from Vendor Name;
    OnDelete=VAR
               "> NAVBR"@52006501 : Integer;
               docFileMgt@52006500 : Codeunit 52112428;
             BEGIN
               LOCKTABLE;
               PostPurchLinesDelete.DeletePurchRcptLines(Rec);

               PurchCommentLine.SETRANGE("Document Type",PurchCommentLine."Document Type"::Receipt);
               PurchCommentLine.SETRANGE("No.","No.");
               PurchCommentLine.DELETEALL;
               ApprovalsMgt.DeletePostedApprovalEntry(DATABASE::"Purch. Rcpt. Header","No.");

               // > NAVBR
               docFileMgt.DeleteFiles(Rec);
               // < NAVBR
             END;

    OnRename=VAR
               "> NAVBR"@52006501 : Integer;
               docFileMgt@52006500 : Codeunit 52112428;
             BEGIN
               // > NAVBR
               docFileMgt.RenameFiles(Rec, xRec);
               // < NAVBR
             END;

    CaptionML=[ENU=Purch. Rcpt. Header;
               PTB=Hist. Cab. Remessa compra];
    LookupPageID=Page145;
    DrillDownPageID=Page145;
  }
  FIELDS
  {
    { 2   ;   ;Buy-from Vendor No. ;Code20        ;TableRelation=Vendor;
                                                   CaptionML=[ENU=Buy-from Vendor No.;
                                                              PTB=Compra a-N� Fornecedor];
                                                   NotBlank=Yes }
    { 3   ;   ;No.                 ;Code20        ;CaptionML=[ENU=No.;
                                                              PTB=N�] }
    { 4   ;   ;Pay-to Vendor No.   ;Code20        ;TableRelation=Vendor;
                                                   CaptionML=[ENU=Pay-to Vendor No.;
                                                              PTB=Pagto.-a N� Forn.];
                                                   NotBlank=Yes }
    { 5   ;   ;Pay-to Name         ;Text50        ;CaptionML=[ENU=Pay-to Name;
                                                              PTB=Pagto.-a Nome] }
    { 6   ;   ;Pay-to Name 2       ;Text50        ;CaptionML=[ENU=Pay-to Name 2;
                                                              PTB=Pagto.-a Nome 2] }
    { 7   ;   ;Pay-to Address      ;Text50        ;CaptionML=[ENU=Pay-to Address;
                                                              PTB=Pagto-a Endere�o] }
    { 8   ;   ;Pay-to Address 2    ;Text50        ;CaptionML=[ENU=Pay-to Address 2;
                                                              PTB=Pagto.-a Endere�o Complementar] }
    { 9   ;   ;Pay-to City         ;Text30        ;TableRelation="Post Code".City;
                                                   ValidateTableRelation=No;
                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=Pay-to City;
                                                              PTB=Pagto.-a Cidade] }
    { 10  ;   ;Pay-to Contact      ;Text50        ;CaptionML=[ENU=Pay-to Contact;
                                                              PTB=Pagto.-a Contato] }
    { 11  ;   ;Your Reference      ;Text35        ;CaptionML=[ENU=Your Reference;
                                                              PTB=Sua Refer�ncia] }
    { 12  ;   ;Ship-to Code        ;Code10        ;TableRelation="Ship-to Address".Code WHERE (Customer No.=FIELD(Sell-to Customer No.));
                                                   CaptionML=[ENU=Ship-to Code;
                                                              PTB=Cod. Endere�o Envio] }
    { 13  ;   ;Ship-to Name        ;Text50        ;CaptionML=[ENU=Ship-to Name;
                                                              PTB=Envio-a Nome] }
    { 14  ;   ;Ship-to Name 2      ;Text50        ;CaptionML=[ENU=Ship-to Name 2;
                                                              PTB=Envio-a Nome 2] }
    { 15  ;   ;Ship-to Address     ;Text50        ;CaptionML=[ENU=Ship-to Address;
                                                              PTB=Envio-a Endere�o] }
    { 16  ;   ;Ship-to Address 2   ;Text50        ;CaptionML=[ENU=Ship-to Address 2;
                                                              PTB=Envio-a Endere�o Complemantar] }
    { 17  ;   ;Ship-to City        ;Text30        ;TableRelation="Post Code".City;
                                                   ValidateTableRelation=No;
                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=Ship-to City;
                                                              PTB=Envio-a Cidade] }
    { 18  ;   ;Ship-to Contact     ;Text50        ;CaptionML=[ENU=Ship-to Contact;
                                                              PTB=Envio-a Contato] }
    { 19  ;   ;Order Date          ;Date          ;CaptionML=[ENU=Order Date;
                                                              PTB=Data Pedido] }
    { 20  ;   ;Posting Date        ;Date          ;CaptionML=[ENU=Posting Date;
                                                              PTB=Data Registo] }
    { 21  ;   ;Expected Receipt Date;Date         ;CaptionML=[ENU=Expected Receipt Date;
                                                              PTB=Data Recep��o Esperada] }
    { 22  ;   ;Posting Description ;Text50        ;CaptionML=[ENU=Posting Description;
                                                              PTB=Texto Registro] }
    { 23  ;   ;Payment Terms Code  ;Code10        ;TableRelation="Payment Terms";
                                                   CaptionML=[ENU=Payment Terms Code;
                                                              PTB=Cod. Termos Pagto.] }
    { 24  ;   ;Due Date            ;Date          ;CaptionML=[ENU=Due Date;
                                                              PTB=Data Vencimento] }
    { 25  ;   ;Payment Discount %  ;Decimal       ;CaptionML=[ENU=Payment Discount %;
                                                              PTB=% Desconto P.P.];
                                                   DecimalPlaces=0:5;
                                                   MinValue=0;
                                                   MaxValue=100 }
    { 26  ;   ;Pmt. Discount Date  ;Date          ;CaptionML=[ENU=Pmt. Discount Date;
                                                              PTB=Data Desconto Pagamento] }
    { 27  ;   ;Shipment Method Code;Code10        ;TableRelation="Shipment Method";
                                                   CaptionML=[ENU=Shipment Method Code;
                                                              PTB=Cod. Condi��es Envio] }
    { 28  ;   ;Location Code       ;Code10        ;TableRelation=Location WHERE (Use As In-Transit=CONST(No));
                                                   CaptionML=[ENU=Location Code;
                                                              PTB=Cod. Dep�sito] }
    { 29  ;   ;Shortcut Dimension 1 Code;Code20   ;TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(1));
                                                   CaptionML=[ENU=Shortcut Dimension 1 Code;
                                                              PTB=Cod. Atalho Dimens�o 1];
                                                   CaptionClass='1,2,1' }
    { 30  ;   ;Shortcut Dimension 2 Code;Code20   ;TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(2));
                                                   CaptionML=[ENU=Shortcut Dimension 2 Code;
                                                              PTB=Cod. Atalho Dimens�o 2];
                                                   CaptionClass='1,2,2' }
    { 31  ;   ;Vendor Posting Group;Code10        ;TableRelation="Vendor Posting Group";
                                                   CaptionML=[ENU=Vendor Posting Group;
                                                              PTB=Gr. Cont�bil Fornecedor];
                                                   Editable=No }
    { 32  ;   ;Currency Code       ;Code10        ;TableRelation=Currency;
                                                   CaptionML=[ENU=Currency Code;
                                                              PTB=Cod. Moeda];
                                                   Editable=No }
    { 33  ;   ;Currency Factor     ;Decimal       ;CaptionML=[ENU=Currency Factor;
                                                              PTB=Fator Moeda];
                                                   DecimalPlaces=0:15;
                                                   MinValue=0 }
    { 37  ;   ;Invoice Disc. Code  ;Code20        ;CaptionML=[ENU=Invoice Disc. Code;
                                                              PTB=Cod. Desconto N.Fiscal] }
    { 41  ;   ;Language Code       ;Code10        ;TableRelation=Language;
                                                   CaptionML=[ENU=Language Code;
                                                              PTB=Cod. Idioma] }
    { 43  ;   ;Purchaser Code      ;Code10        ;TableRelation=Salesperson/Purchaser;
                                                   CaptionML=[ENU=Purchaser Code;
                                                              PTB=Cod. Comprador] }
    { 44  ;   ;Order No.           ;Code20        ;AccessByPermission=TableData 120=R;
                                                   CaptionML=[ENU=Order No.;
                                                              PTB=N� Pedido] }
    { 46  ;   ;Comment             ;Boolean       ;FieldClass=FlowField;
                                                   CalcFormula=Exist("Purch. Comment Line" WHERE (Document Type=CONST(Receipt),
                                                                                                  No.=FIELD(No.),
                                                                                                  Document Line No.=CONST(0)));
                                                   CaptionML=[ENU=Comment;
                                                              PTB=Coment�rio];
                                                   Editable=No }
    { 47  ;   ;No. Printed         ;Integer       ;CaptionML=[ENU=No. Printed;
                                                              PTB=N� Impress�es];
                                                   Editable=No }
    { 51  ;   ;On Hold             ;Code3         ;CaptionML=[ENU=On Hold;
                                                              PTB=Retido] }
    { 52  ;   ;Applies-to Doc. Type;Option        ;CaptionML=[ENU=Applies-to Doc. Type;
                                                              PTB=Aplicado por Tipo Doc.];
                                                   OptionCaptionML=[ENU=" ,Payment,Invoice,Credit Memo,Finance Charge Memo,Reminder,Refund";
                                                                    PTB=" ,Pagamento,Nota Fiscal,Nota de Cr�dito,Nota de Encargo Financeiro,Lembrete,Reembolso"];
                                                   OptionString=[ ,Payment,Invoice,Credit Memo,Finance Charge Memo,Reminder,Refund] }
    { 53  ;   ;Applies-to Doc. No. ;Code20        ;OnLookup=BEGIN
                                                              VendLedgEntry.SETCURRENTKEY("Document No.");
                                                              VendLedgEntry.SETRANGE("Document Type","Applies-to Doc. Type");
                                                              VendLedgEntry.SETRANGE("Document No.","Applies-to Doc. No.");
                                                              PAGE.RUN(0,VendLedgEntry);
                                                            END;

                                                   CaptionML=[ENU=Applies-to Doc. No.;
                                                              PTB=Aplicado por N� Doc.] }
    { 55  ;   ;Bal. Account No.    ;Code20        ;TableRelation=IF (Bal. Account Type=CONST(G/L Account)) "G/L Account"
                                                                 ELSE IF (Bal. Account Type=CONST(Bank Account)) "Bank Account";
                                                   CaptionML=[ENU=Bal. Account No.;
                                                              PTB=N� Conta Contrap.] }
    { 66  ;   ;Vendor Order No.    ;Code35        ;CaptionML=[ENU=Vendor Order No.;
                                                              PTB=N� Ped. Fornecedor] }
    { 67  ;   ;Vendor Shipment No. ;Code35        ;CaptionML=[ENU=Vendor Shipment No.;
                                                              PTB=N� Remessa Forn.] }
    { 70  ;   ;VAT Registration No.;Text20        ;CaptionML=[ENU=VAT Registration No.;
                                                              PTB=N� Contribuinte] }
    { 72  ;   ;Sell-to Customer No.;Code20        ;TableRelation=Customer;
                                                   CaptionML=[ENU=Sell-to Customer No.;
                                                              PTB=Venda-a N� Cliente] }
    { 73  ;   ;Reason Code         ;Code10        ;TableRelation="Reason Code";
                                                   CaptionML=[ENU=Reason Code;
                                                              PTB=Cod. Raz�o] }
    { 74  ;   ;Gen. Bus. Posting Group;Code10     ;TableRelation="Gen. Business Posting Group";
                                                   CaptionML=[ENU=Gen. Bus. Posting Group;
                                                              PTB=Gr. Cont�bil Neg�cio] }
    { 76  ;   ;Transaction Type    ;Code10        ;TableRelation="Transaction Type";
                                                   CaptionML=[ENU=Transaction Type;
                                                              PTB=Natureza Transa��o] }
    { 77  ;   ;Transport Method    ;Code10        ;TableRelation="Transport Method";
                                                   CaptionML=[ENU=Transport Method;
                                                              PTB=Modo Transporte] }
    { 78  ;   ;VAT Country/Region Code;Code10     ;TableRelation=Country/Region;
                                                   CaptionML=[ENU=VAT Country/Region Code;
                                                              PTB=C�digo Imposto Pa�s/Regi�o] }
    { 79  ;   ;Buy-from Vendor Name;Text50        ;CaptionML=[ENU=Buy-from Vendor Name;
                                                              PTB=Compra-a Nome Fornecedor] }
    { 80  ;   ;Buy-from Vendor Name 2;Text50      ;CaptionML=[ENU=Buy-from Vendor Name 2;
                                                              PTB=Compra-a Nome Fornecedor 2] }
    { 81  ;   ;Buy-from Address    ;Text50        ;CaptionML=[ENU=Buy-from Address;
                                                              PTB=Compra-a Endere�o] }
    { 82  ;   ;Buy-from Address 2  ;Text50        ;CaptionML=[ENU=Buy-from Address 2;
                                                              PTB=Compra a-Endere�o Complementar] }
    { 83  ;   ;Buy-from City       ;Text30        ;TableRelation="Post Code".City;
                                                   ValidateTableRelation=No;
                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=Buy-from City;
                                                              PTB=Compra-a Cidade] }
    { 84  ;   ;Buy-from Contact    ;Text50        ;CaptionML=[ENU=Buy-from Contact;
                                                              PTB=Compra-a Contato] }
    { 85  ;   ;Pay-to Post Code    ;Code20        ;TableRelation="Post Code";
                                                   ValidateTableRelation=No;
                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=Pay-to Post Code;
                                                              PTB=Pagamento-a CEP] }
    { 86  ;   ;Pay-to County       ;Text30        ;CaptionML=[ENU=Pay-to County;
                                                              PTB=Pagto. a-Distrito] }
    { 87  ;   ;Pay-to Country/Region Code;Code10  ;TableRelation=Country/Region;
                                                   CaptionML=[ENU=Pay-to Country/Region Code;
                                                              PTB=Pagar a C�d. Pa�s/Regi�o] }
    { 88  ;   ;Buy-from Post Code  ;Code20        ;TableRelation="Post Code";
                                                   ValidateTableRelation=No;
                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=Buy-from Post Code;
                                                              PTB=Compra-a CEP] }
    { 89  ;   ;Buy-from County     ;Text30        ;CaptionML=[ENU=Buy-from County;
                                                              PTB=Compra-a Distrito] }
    { 90  ;   ;Buy-from Country/Region Code;Code10;TableRelation=Country/Region;
                                                   CaptionML=[ENU=Buy-from Country/Region Code;
                                                              PTB=Comprar de C�d. Pa�s/Regi�o] }
    { 91  ;   ;Ship-to Post Code   ;Code20        ;TableRelation="Post Code";
                                                   ValidateTableRelation=No;
                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=Ship-to Post Code;
                                                              PTB=Envio-a CEP] }
    { 92  ;   ;Ship-to County      ;Text30        ;CaptionML=[ENU=Ship-to County;
                                                              PTB=Envio-a Distrito] }
    { 93  ;   ;Ship-to Country/Region Code;Code10 ;TableRelation=Country/Region;
                                                   CaptionML=[ENU=Ship-to Country/Region Code;
                                                              PTB=C�d. Enviar - para Pa�s/Regi�o] }
    { 94  ;   ;Bal. Account Type   ;Option        ;CaptionML=[ENU=Bal. Account Type;
                                                              PTB=Tipo Contrapartida];
                                                   OptionCaptionML=[ENU=G/L Account,Bank Account;
                                                                    PTB=Conta,Banco];
                                                   OptionString=G/L Account,Bank Account }
    { 95  ;   ;Order Address Code  ;Code10        ;TableRelation="Order Address".Code WHERE (Vendor No.=FIELD(Buy-from Vendor No.));
                                                   CaptionML=[ENU=Order Address Code;
                                                              PTB=Cod. Endere�o Ped. Forn.] }
    { 97  ;   ;Entry Point         ;Code10        ;TableRelation="Entry/Exit Point";
                                                   CaptionML=[ENU=Entry Point;
                                                              PTB=Porto/Aerop. Entrada] }
    { 98  ;   ;Correction          ;Boolean       ;CaptionML=[ENU=Correction;
                                                              PTB=Corre��o] }
    { 99  ;   ;Document Date       ;Date          ;CaptionML=[ENU=Document Date;
                                                              PTB=Data Documento] }
    { 101 ;   ;Area                ;Code10        ;TableRelation=Area;
                                                   CaptionML=[ENU=Area;
                                                              PTB=�rea] }
    { 102 ;   ;Transaction Specification;Code10   ;TableRelation="Transaction Specification";
                                                   CaptionML=[ENU=Transaction Specification;
                                                              PTB=Especifica�ao Transa��o] }
    { 104 ;   ;Payment Method Code ;Code10        ;TableRelation="Payment Method";
                                                   CaptionML=[ENU=Payment Method Code;
                                                              PTB=Cod. Forma Pagamento] }
    { 109 ;   ;No. Series          ;Code10        ;TableRelation="No. Series";
                                                   CaptionML=[ENU=No. Series;
                                                              PTB=N� S�ries];
                                                   Editable=No }
    { 110 ;   ;Order No. Series    ;Code10        ;TableRelation="No. Series";
                                                   CaptionML=[ENU=Order No. Series;
                                                              PTB=N� S�rie Pedido] }
    { 112 ;   ;User ID             ;Code50        ;TableRelation=User."User Name";
                                                   OnLookup=VAR
                                                              UserMgt@1000 : Codeunit 418;
                                                            BEGIN
                                                              UserMgt.LookupUserID("User ID");
                                                            END;

                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=User ID;
                                                              PTB=ID Usu�rio] }
    { 113 ;   ;Source Code         ;Code10        ;TableRelation="Source Code";
                                                   CaptionML=[ENU=Source Code;
                                                              PTB=Cod. Origem] }
    { 114 ;   ;Tax Area Code       ;Code20        ;TableRelation="Tax Area";
                                                   CaptionML=[ENU=Tax Area Code;
                                                              PTB=C�d. �rea Imposto] }
    { 115 ;   ;Tax Liable          ;Boolean       ;CaptionML=[ENU=Tax Liable;
                                                              PTB=Sujeito a Imposto] }
    { 116 ;   ;VAT Bus. Posting Group;Code10      ;TableRelation="VAT Business Posting Group";
                                                   CaptionML=[ENU=VAT Bus. Posting Group;
                                                              PTB=Gr. Registro Imp. Neg�cio] }
    { 119 ;   ;VAT Base Discount % ;Decimal       ;CaptionML=[ENU=VAT Base Discount %;
                                                              PTB=% Desc. Base IPI];
                                                   DecimalPlaces=0:5;
                                                   MinValue=0;
                                                   MaxValue=100 }
    { 151 ;   ;Quote No.           ;Code20        ;CaptionML=ENU=Quote No.;
                                                   Editable=No }
    { 480 ;   ;Dimension Set ID    ;Integer       ;TableRelation="Dimension Set Entry";
                                                   OnLookup=BEGIN
                                                              ShowDimensions;
                                                            END;

                                                   CaptionML=ENU=Dimension Set ID;
                                                   Editable=No }
    { 5050;   ;Campaign No.        ;Code20        ;TableRelation=Campaign;
                                                   CaptionML=ENU=Campaign No. }
    { 5052;   ;Buy-from Contact No.;Code20        ;TableRelation=Contact;
                                                   CaptionML=ENU=Buy-from Contact No. }
    { 5053;   ;Pay-to Contact no.  ;Code20        ;TableRelation=Contact;
                                                   CaptionML=ENU=Pay-to Contact no. }
    { 5700;   ;Responsibility Center;Code10       ;TableRelation="Responsibility Center";
                                                   CaptionML=[ENU=Responsibility Center;
                                                              PTB=Centro Responsabilidade] }
    { 5790;   ;Requested Receipt Date;Date        ;AccessByPermission=TableData 99000880=R;
                                                   CaptionML=[ENU=Requested Receipt Date;
                                                              PTB=Data Recep��o Requerida];
                                                   Editable=No }
    { 5791;   ;Promised Receipt Date;Date         ;CaptionML=[ENU=Promised Receipt Date;
                                                              PTB=Data Recep��o Prometida];
                                                   Editable=No }
    { 5792;   ;Lead Time Calculation;DateFormula  ;AccessByPermission=TableData 120=R;
                                                   CaptionML=[ENU=Lead Time Calculation;
                                                              PTB=Prazo Entrega (Dias)];
                                                   Editable=No }
    { 5793;   ;Inbound Whse. Handling Time;DateFormula;
                                                   AccessByPermission=TableData 14=R;
                                                   CaptionML=[ENU=Inbound Whse. Handling Time;
                                                              PTB=Tempo Proc. Entrada Dep�sito];
                                                   Editable=No }
    { 50000;  ;Posting Text        ;Text250       ;CaptionML=[ENU=Posting Text;
                                                              PTB=Texto Registro];
                                                   Description=EMIBR CORE 50 }
  }
  KEYS
  {
    {    ;No.                                     ;Clustered=Yes }
    {    ;Order No.                                }
    {    ;Pay-to Vendor No.                        }
    {    ;Buy-from Vendor No.                      }
  }
  FIELDGROUPS
  {
    { 1   ;DropDown            ;No.,Buy-from Vendor No.,Pay-to Vendor No.,Posting Date,Posting Description }
  }
  CODE
  {
    VAR
      PurchRcptHeader@1000 : Record 120;
      PurchCommentLine@1001 : Record 43;
      VendLedgEntry@1002 : Record 25;
      PostPurchLinesDelete@1003 : Codeunit 364;
      DimMgt@1004 : Codeunit 408;
      ApprovalsMgt@1008 : Codeunit 439;
      UserSetupMgt@1005 : Codeunit 5700;

    PROCEDURE PrintRecords@1(ShowRequestForm@1000 : Boolean);
    VAR
      ReportSelection@1001 : Record 77;
    BEGIN
      WITH PurchRcptHeader DO BEGIN
        COPY(Rec);
        ReportSelection.SETRANGE(Usage,ReportSelection.Usage::"P.Receipt");
        ReportSelection.SETFILTER("Report ID",'<>0');
        ReportSelection.FIND('-');
        REPEAT
          REPORT.RUNMODAL(ReportSelection."Report ID",ShowRequestForm,FALSE,PurchRcptHeader);
        UNTIL ReportSelection.NEXT = 0;
      END;
    END;

    PROCEDURE Navigate@2();
    VAR
      NavigateForm@1000 : Page 344;
    BEGIN
      NavigateForm.SetDoc("Posting Date","No.");
      NavigateForm.RUN;
    END;

    PROCEDURE ShowDimensions@3();
    BEGIN
      DimMgt.ShowDimensionSet("Dimension Set ID",STRSUBSTNO('%1 %2',TABLECAPTION,"No."));
    END;

    PROCEDURE SetSecurityFilterOnRespCenter@4();
    BEGIN
      IF UserSetupMgt.GetPurchasesFilter <> '' THEN BEGIN
        FILTERGROUP(2);
        SETRANGE("Responsibility Center",UserSetupMgt.GetPurchasesFilter);
        FILTERGROUP(0);
      END;
    END;

    BEGIN
    END.
  }
}

