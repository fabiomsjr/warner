OBJECT Table 80037 Fixed Asset_UPG
{
  OBJECT-PROPERTIES
  {
    Date=13/05/16;
    Time=16:18:59;
    Modified=Yes;
    Version List=UPG;
  }
  PROPERTIES
  {
    Permissions=TableData 5629=r;
    DataCaptionFields=No.,Description;
    OnInsert=BEGIN
               IF "No." = '' THEN BEGIN
                 FASetup.GET;
                 FASetup.TESTFIELD("Fixed Asset Nos.");
                 NoSeriesMgt.InitSeries(FASetup."Fixed Asset Nos.",xRec."No. Series",0D,"No.","No. Series");
               END;

               "Main Asset/Component" := "Main Asset/Component":: " ";
               "Component of Main Asset" := '';

               DimMgt.UpdateDefaultDim(
                 DATABASE::"Fixed Asset","No.",
                 "Global Dimension 1 Code","Global Dimension 2 Code");
             END;

    OnModify=BEGIN
               "Last Date Modified" := TODAY;
             END;

    OnDelete=BEGIN
               LOCKTABLE;
               FADeprBook.LOCKTABLE;
               MainAssetComp.LOCKTABLE;
               InsCoverageLedgEntry.LOCKTABLE;
               IF "Main Asset/Component" = "Main Asset/Component"::"Main Asset" THEN
                 ERROR(Text000);
               FAMoveEntries.MoveFAInsuranceEntries("No.");
               FADeprBook.SETCURRENTKEY("FA No.");
               FADeprBook.SETRANGE("FA No.","No.");
               FADeprBook.DELETEALL(TRUE);
               IF FADeprBook.FIND('-') THEN
                 ERROR(Text001,TABLECAPTION,"No.");

               MainAssetComp.SETCURRENTKEY("FA No.");
               MainAssetComp.SETRANGE("FA No.","No.");
               MainAssetComp.DELETEALL;
               IF "Main Asset/Component" = "Main Asset/Component"::Component THEN BEGIN
                 MainAssetComp.RESET;
                 MainAssetComp.SETRANGE("Main Asset No.","Component of Main Asset");
                 MainAssetComp.SETRANGE("FA No.",'');
                 MainAssetComp.DELETEALL;
                 MainAssetComp.SETRANGE("FA No.");
                 IF NOT MainAssetComp.FIND('-') THEN BEGIN
                   FA.GET("Component of Main Asset");
                   FA."Main Asset/Component" := FA."Main Asset/Component"::" ";
                   FA."Component of Main Asset" := '';
                   FA.MODIFY;
                 END;
               END;

               MaintenanceRegistration.SETRANGE("FA No.","No.");
               MaintenanceRegistration.DELETEALL;

               CommentLine.SETRANGE("Table Name",CommentLine."Table Name"::"Fixed Asset");
               CommentLine.SETRANGE("No.","No.");
               CommentLine.DELETEALL;

               DimMgt.DeleteDefaultDim(DATABASE::"Fixed Asset","No.");
             END;

    OnRename=BEGIN
               "Last Date Modified" := TODAY;
             END;

    CaptionML=[ENU=Fixed Asset;
               PTB=Ativo Fixo];
    LookupPageID=Page5601;
    DrillDownPageID=Page5601;
  }
  FIELDS
  {
    { 1   ;   ;No.                 ;Code20        ;AltSearchField=Search Description;
                                                   OnValidate=BEGIN
                                                                IF "No." <> xRec."No." THEN BEGIN
                                                                  FASetup.GET;
                                                                  NoSeriesMgt.TestManual(FASetup."Fixed Asset Nos.");
                                                                  "No. Series" := '';
                                                                END;
                                                              END;

                                                   CaptionML=[ENU=No.;
                                                              PTB=N�] }
    { 2   ;   ;Description         ;Text30        ;OnValidate=BEGIN
                                                                IF ("Search Description" = UPPERCASE(xRec.Description)) OR ("Search Description" = '') THEN
                                                                  "Search Description" := Description;
                                                                IF Description <> xRec.Description THEN BEGIN
                                                                  FADeprBook.SETCURRENTKEY("FA No.");
                                                                  FADeprBook.SETRANGE("FA No.","No.");
                                                                  FADeprBook.MODIFYALL(Description,Description);
                                                                END;
                                                                MODIFY(TRUE);
                                                              END;

                                                   CaptionML=[ENU=Description;
                                                              PTB=Descri��o] }
    { 3   ;   ;Search Description  ;Code30        ;CaptionML=[ENU=Search Description;
                                                              PTB=Nome Fantasia] }
    { 4   ;   ;Description 2       ;Text30        ;CaptionML=[ENU=Description 2;
                                                              PTB=Descricao 2] }
    { 5   ;   ;FA Class Code       ;Code10        ;TableRelation="FA Class";
                                                   CaptionML=[ENU=FA Class Code;
                                                              PTB=Cod. Classe AF] }
    { 6   ;   ;FA Subclass Code    ;Code10        ;TableRelation="FA Subclass";
                                                   CaptionML=[ENU=FA Subclass Code;
                                                              PTB=Cod. Subclasse AF] }
    { 7   ;   ;Global Dimension 1 Code;Code20     ;TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(1));
                                                   OnValidate=BEGIN
                                                                ValidateShortcutDimCode(1,"Global Dimension 1 Code");
                                                              END;

                                                   CaptionML=[ENU=Global Dimension 1 Code;
                                                              PTB=Cod. Dimens�o 1 Global];
                                                   CaptionClass='1,1,1' }
    { 8   ;   ;Global Dimension 2 Code;Code20     ;TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(2));
                                                   OnValidate=BEGIN
                                                                ValidateShortcutDimCode(2,"Global Dimension 2 Code");
                                                              END;

                                                   CaptionML=[ENU=Global Dimension 2 Code;
                                                              PTB=Cod. Dimens�o 2 Global];
                                                   CaptionClass='1,1,2' }
    { 9   ;   ;Location Code       ;Code10        ;TableRelation=Location WHERE (Use As In-Transit=CONST(No));
                                                   CaptionML=[ENU=Location Code;
                                                              PTB=Cod. Dep�sito] }
    { 10  ;   ;FA Location Code    ;Code10        ;TableRelation="FA Location";
                                                   CaptionML=[ENU=FA Location Code;
                                                              PTB=Cod. Dep�sito AF] }
    { 11  ;   ;Vendor No.          ;Code20        ;TableRelation=Vendor;
                                                   CaptionML=[ENU=Vendor No.;
                                                              PTB=N� Fornecedor] }
    { 12  ;   ;Main Asset/Component;Option        ;CaptionML=[ENU=Main Asset/Component;
                                                              PTB=Ativo Principal, Componente];
                                                   OptionCaptionML=[ENU=" ,Main Asset,Component";
                                                                    PTB=" ,Ativo Principal,Componente"];
                                                   OptionString=[ ,Main Asset,Component];
                                                   Editable=No }
    { 13  ;   ;Component of Main Asset;Code20     ;TableRelation="Fixed Asset";
                                                   CaptionML=[ENU=Component of Main Asset;
                                                              PTB=Componente Ativo Fixo Principal];
                                                   Editable=No }
    { 14  ;   ;Budgeted Asset      ;Boolean       ;CaptionML=[ENU=Budgeted Asset;
                                                              PTB=Ativo Fixo Or�ado] }
    { 15  ;   ;Warranty Date       ;Date          ;CaptionML=[ENU=Warranty Date;
                                                              PTB=Data Garantia] }
    { 16  ;   ;Responsible Employee;Code20        ;TableRelation=Employee;
                                                   CaptionML=[ENU=Responsible Employee;
                                                              PTB=Empregado respons�vel] }
    { 17  ;   ;Serial No.          ;Text30        ;CaptionML=[ENU=Serial No.;
                                                              PTB=No. S�rie] }
    { 18  ;   ;Last Date Modified  ;Date          ;CaptionML=[ENU=Last Date Modified;
                                                              PTB=�ltima Data Modifica��o];
                                                   Editable=No }
    { 19  ;   ;Insured             ;Boolean       ;FieldClass=FlowField;
                                                   CalcFormula=Exist("Ins. Coverage Ledger Entry" WHERE (FA No.=FIELD(No.),
                                                                                                         Disposed FA=CONST(No)));
                                                   CaptionML=[ENU=Insured;
                                                              PTB=Segurado];
                                                   Editable=No }
    { 20  ;   ;Comment             ;Boolean       ;FieldClass=FlowField;
                                                   CalcFormula=Exist("Comment Line" WHERE (Table Name=CONST(Fixed Asset),
                                                                                           No.=FIELD(No.)));
                                                   CaptionML=[ENU=Comment;
                                                              PTB=Coment�rio];
                                                   Editable=No }
    { 21  ;   ;Blocked             ;Boolean       ;CaptionML=[ENU=Blocked;
                                                              PTB=Bloqueado] }
    { 22  ;   ;Picture             ;BLOB          ;CaptionML=[ENU=Picture;
                                                              PTB=Imagem];
                                                   SubType=Bitmap }
    { 23  ;   ;Maintenance Vendor No.;Code20      ;TableRelation=Vendor;
                                                   CaptionML=[ENU=Maintenance Vendor No.;
                                                              PTB=N� Fornecedor Manuten��o] }
    { 24  ;   ;Under Maintenance   ;Boolean       ;CaptionML=[ENU=Under Maintenance;
                                                              PTB=Sob Manuten��o] }
    { 25  ;   ;Next Service Date   ;Date          ;CaptionML=[ENU=Next Service Date;
                                                              PTB=Pr�xima Data Servi�o] }
    { 26  ;   ;Inactive            ;Boolean       ;CaptionML=[ENU=Inactive;
                                                              PTB=Inativo] }
    { 27  ;   ;FA Posting Date Filter;Date        ;FieldClass=FlowFilter;
                                                   CaptionML=[ENU=FA Posting Date Filter;
                                                              PTB=Filtro Data Registro AF] }
    { 28  ;   ;No. Series          ;Code10        ;TableRelation="No. Series";
                                                   CaptionML=[ENU=No. Series;
                                                              PTB=N� S�ries];
                                                   Editable=No }
    { 29  ;   ;FA Posting Group    ;Code10        ;TableRelation="FA Posting Group";
                                                   CaptionML=[ENU=FA Posting Group;
                                                              PTB=Grupo Cont. AF] }
    { 35000000;;Branch Code        ;Code20        ;TableRelation="Branch Information2";
                                                   CaptionML=[ENU=Branch Code;
                                                              PTB=Cod. Filial];
                                                   Description=NAVBR5.01.0109 }
    { 35000001;;Credit Apropriation;Boolean       ;CaptionML=PTB=Apropria��o Cr�dito Ciap;
                                                   Description=NAVBR5.01.0109 }
    { 35000002;;Retention Group Code;Code20       ;TableRelation="Retention Groups.2";
                                                   CaptionML=[ENU=Retention Group Code;
                                                              PTB=C�d. Grupo Reten��o];
                                                   Description=NAVBR5.01.0109 }
    { 35000600;;Qty. ICMS Plots Apropriation;Integer;
                                                   CaptionML=PTB=Qtd. Parcelas ICMS � Apropriar;
                                                   Description=EFD2.0.8 }
    { 35000601;;Area Property Used ;Option        ;CaptionML=PTB=Area Utiliza��o Bem;
                                                   OptionString=[ ,1-Operacional (Com�rcio ou Servi�o),2-Administrativa (Com�rcio ou Servi�o),3-Produtiva (Industrial),4-Apoio Produ��o (Industrial),5-Administrativa (Industrial)];
                                                   Description=EFD2.0.8 }
    { 35000602;;Function Description;Text60       ;CaptionML=PTB=Descri��o Suscinta Fun��o;
                                                   Description=EFD2.0.8 }
    { 35000603;;Quantity months validity;Integer  ;CaptionML=PTB=Validade em Meses;
                                                   Description=EFD2.0.8 }
    { 35000604;;Base Amount ICMS   ;Decimal       ;CaptionML=PTB=Valor Base ICMS;
                                                   Description=EFD2.0.8 }
    { 35000605;;Amount ICMS        ;Decimal       ;CaptionML=PTB=Valor do ICMS;
                                                   Description=EFD2.0.8 }
    { 35000606;;ICMS %             ;Decimal       ;CaptionML=PTB=% ICMS;
                                                   Description=EFD2.0.8 }
    { 35000607;;Acquisition Date   ;Date          ;CaptionML=PTB=Data Aquisi��o;
                                                   Description=EFD2.0.8 }
    { 35000608;;PIS Amount         ;Decimal       ;CaptionML=PTB=Valor PIS;
                                                   Description=EFD2.0.8 }
    { 35000609;;COFINS Amount      ;Decimal       ;CaptionML=PTB=Valor Cofins;
                                                   Description=EFD2.0.8 }
    { 35000610;;Approp.Qty.ICMS Plots Already;Integer;
                                                   CaptionML=PTB=Qtd. Parcelas Apropriadas;
                                                   Description=EFD2.0.8 }
    { 35000611;;Entirely Appropriate Date;Date    ;CaptionML=PTB=Data Final Apropria��o;
                                                   Description=EFD2.0.8 }
    { 35000612;;NCM Code           ;Code20        ;TableRelation="NCM Codes2";
                                                   CaptionML=[ENU=NCM Code;
                                                              PTB=C�digo NCM];
                                                   Description=EFD2.0.8 }
  }
  KEYS
  {
    {    ;No.                                     ;Clustered=Yes }
    {    ;Search Description                       }
    {    ;FA Class Code                            }
    {    ;FA Subclass Code                         }
    {    ;Component of Main Asset,Main Asset/Component }
    {    ;FA Location Code                         }
    {    ;Global Dimension 1 Code                  }
    {    ;Global Dimension 2 Code                  }
    {    ;FA Posting Group                         }
    {    ;Description                             ;KeyGroups=SearchCol }
  }
  FIELDGROUPS
  {
    { 1   ;DropDown            ;No.,Description,FA Class Code            }
  }
  CODE
  {
    VAR
      Text000@1000 : TextConst 'ENU=A main asset cannot be deleted.;PTB=N�o pode eliminar um ativo fixo principal.';
      Text001@1001 : TextConst 'ENU=You cannot delete %1 %2 because it has associated depreciation books.;PTB=N�o pode eliminar %1 %2 j� que t�m associados Livros de Deprecia��o.';
      CommentLine@1002 : Record 97;
      FA@1003 : Record 5600;
      FASetup@1004 : Record 5603;
      MaintenanceRegistration@1005 : Record 5616;
      FADeprBook@1006 : Record 5612;
      MainAssetComp@1007 : Record 5640;
      InsCoverageLedgEntry@1008 : Record 5629;
      FAMoveEntries@1009 : Codeunit 5623;
      NoSeriesMgt@1010 : Codeunit 396;
      DimMgt@1011 : Codeunit 408;

    PROCEDURE AssistEdit@2(OldFA@1000 : Record 5600) : Boolean;
    BEGIN
    END;

    PROCEDURE ValidateShortcutDimCode@29(FieldNumber@1000 : Integer;VAR ShortcutDimCode@1001 : Code[20]);
    BEGIN
      DimMgt.ValidateDimValueCode(FieldNumber,ShortcutDimCode);
      DimMgt.SaveDefaultDim(DATABASE::"Fixed Asset","No.",FieldNumber,ShortcutDimCode);
      MODIFY(TRUE);
    END;

    BEGIN
    END.
  }
}

