OBJECT Table 32 Item Ledger Entry
{
  OBJECT-PROPERTIES
  {
    Date=30/07/15;
    Time=12:00:00;
    Version List=NAVW18.00.00.41779,NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Item Ledger Entry;
               PTB=Mov. Produto];
    LookupPageID=Page38;
    DrillDownPageID=Page38;
  }
  FIELDS
  {
    { 1   ;   ;Entry No.           ;Integer       ;CaptionML=[ENU=Entry No.;
                                                              PTB=N� Mov.] }
    { 2   ;   ;Item No.            ;Code20        ;TableRelation=Item;
                                                   CaptionML=[ENU=Item No.;
                                                              PTB=N� Produto] }
    { 3   ;   ;Posting Date        ;Date          ;CaptionML=[ENU=Posting Date;
                                                              PTB=Data Registro] }
    { 4   ;   ;Entry Type          ;Option        ;CaptionML=[ENU=Entry Type;
                                                              PTB=Tipo Mov.];
                                                   OptionCaptionML=[ENU=Purchase,Sale,Positive Adjmt.,Negative Adjmt.,Transfer,Consumption,Output, ,Assembly Consumption,Assembly Output;
                                                                    PTB=Compra,Venda,Ajuste Positivo,Ajuste Negativo,Transf.,Consumo,Sa�da, ,Consumo Montagem,Sa�da Montagem];
                                                   OptionString=Purchase,Sale,Positive Adjmt.,Negative Adjmt.,Transfer,Consumption,Output, ,Assembly Consumption,Assembly Output }
    { 5   ;   ;Source No.          ;Code20        ;TableRelation=IF (Source Type=CONST(Customer)) Customer
                                                                 ELSE IF (Source Type=CONST(Vendor)) Vendor
                                                                 ELSE IF (Source Type=CONST(Item)) Item;
                                                   CaptionML=[ENU=Source No.;
                                                              PTB=N� Origem] }
    { 6   ;   ;Document No.        ;Code20        ;CaptionML=[ENU=Document No.;
                                                              PTB=N� Documento] }
    { 7   ;   ;Description         ;Text50        ;CaptionML=[ENU=Description;
                                                              PTB=Descri��o] }
    { 8   ;   ;Location Code       ;Code10        ;TableRelation=Location;
                                                   CaptionML=[ENU=Location Code;
                                                              PTB=Cod. Dep�sito] }
    { 12  ;   ;Quantity            ;Decimal       ;CaptionML=[ENU=Quantity;
                                                              PTB=Quantidade];
                                                   DecimalPlaces=0:5 }
    { 13  ;   ;Remaining Quantity  ;Decimal       ;CaptionML=[ENU=Remaining Quantity;
                                                              PTB=Qtd. Pendente];
                                                   DecimalPlaces=0:5 }
    { 14  ;   ;Invoiced Quantity   ;Decimal       ;CaptionML=[ENU=Invoiced Quantity;
                                                              PTB=Qtd. Faturada];
                                                   DecimalPlaces=0:5 }
    { 28  ;   ;Applies-to Entry    ;Integer       ;CaptionML=[ENU=Applies-to Entry;
                                                              PTB=Aplicado por N� Mov.] }
    { 29  ;   ;Open                ;Boolean       ;CaptionML=[ENU=Open;
                                                              PTB=Pendente] }
    { 33  ;   ;Global Dimension 1 Code;Code20     ;TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(1));
                                                   CaptionML=[ENU=Global Dimension 1 Code;
                                                              PTB=Cod. Dimens�o 1 Global];
                                                   CaptionClass='1,1,1' }
    { 34  ;   ;Global Dimension 2 Code;Code20     ;TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(2));
                                                   CaptionML=[ENU=Global Dimension 2 Code;
                                                              PTB=Cod. Dimens�o 2 Global];
                                                   CaptionClass='1,1,2' }
    { 36  ;   ;Positive            ;Boolean       ;CaptionML=[ENU=Positive;
                                                              PTB=Positivo] }
    { 41  ;   ;Source Type         ;Option        ;CaptionML=[ENU=Source Type;
                                                              PTB=Tipo Origem];
                                                   OptionCaptionML=[ENU=" ,Customer,Vendor,Item";
                                                                    PTB=" ,Cliente,Fornecedor,Item"];
                                                   OptionString=[ ,Customer,Vendor,Item] }
    { 47  ;   ;Drop Shipment       ;Boolean       ;AccessByPermission=TableData 223=R;
                                                   CaptionML=[ENU=Drop Shipment;
                                                              PTB=Envio Direto] }
    { 50  ;   ;Transaction Type    ;Code10        ;TableRelation="Transaction Type";
                                                   CaptionML=[ENU=Transaction Type;
                                                              PTB=Natureza Transa��o] }
    { 51  ;   ;Transport Method    ;Code10        ;TableRelation="Transport Method";
                                                   CaptionML=[ENU=Transport Method;
                                                              PTB=Modo Transporte] }
    { 52  ;   ;Country/Region Code ;Code10        ;TableRelation=Country/Region;
                                                   CaptionML=[ENU=Country/Region Code;
                                                              PTB=C�d. Pa�s/Regi�o] }
    { 59  ;   ;Entry/Exit Point    ;Code10        ;TableRelation="Entry/Exit Point";
                                                   CaptionML=[ENU=Entry/Exit Point;
                                                              PTB=Porto/Aeroporto] }
    { 60  ;   ;Document Date       ;Date          ;CaptionML=[ENU=Document Date;
                                                              PTB=Data Documento] }
    { 61  ;   ;External Document No.;Code35       ;CaptionML=[ENU=External Document No.;
                                                              PTB=N� Documento Externo] }
    { 62  ;   ;Area                ;Code10        ;TableRelation=Area;
                                                   CaptionML=[ENU=Area;
                                                              PTB=�rea] }
    { 63  ;   ;Transaction Specification;Code10   ;TableRelation="Transaction Specification";
                                                   CaptionML=[ENU=Transaction Specification;
                                                              PTB=Especifica�ao Transa��o] }
    { 64  ;   ;No. Series          ;Code10        ;TableRelation="No. Series";
                                                   CaptionML=[ENU=No. Series;
                                                              PTB=N� S�ries] }
    { 70  ;   ;Reserved Quantity   ;Decimal       ;FieldClass=FlowField;
                                                   CalcFormula=Sum("Reservation Entry"."Quantity (Base)" WHERE (Source ID=CONST(),
                                                                                                                Source Ref. No.=FIELD(Entry No.),
                                                                                                                Source Type=CONST(32),
                                                                                                                Source Subtype=CONST(0),
                                                                                                                Source Batch Name=CONST(),
                                                                                                                Source Prod. Order Line=CONST(0),
                                                                                                                Reservation Status=CONST(Reservation)));
                                                   AccessByPermission=TableData 120=R;
                                                   CaptionML=[ENU=Reserved Quantity;
                                                              PTB=Qtd. Reservada];
                                                   DecimalPlaces=0:5;
                                                   Editable=No }
    { 79  ;   ;Document Type       ;Option        ;CaptionML=[ENU=Document Type;
                                                              PTB=Tipo Documento];
                                                   OptionCaptionML=[ENU=" ,Sales Shipment,Sales Invoice,Sales Return Receipt,Sales Credit Memo,Purchase Receipt,Purchase Invoice,Purchase Return Shipment,Purchase Credit Memo,Transfer Shipment,Transfer Receipt,Service Shipment,Service Invoice,Service Credit Memo,Posted Assembly";
                                                                    PTB=" ,Envio Vendas,Nota Fiscal Vendas,Recebimento Devolu��o,Nota Cr�dito Venda,Recebimento Compra,Nota Fiscal Compra,Envio Retorno Compra,Nota Cr�dito Compra,Envio Transf.,Recebimento Transfer.,Envio Servi�o,Nota Fiscal de Servi�o,Nota Cr�dito Servi�o,Montagem Reg."];
                                                   OptionString=[ ,Sales Shipment,Sales Invoice,Sales Return Receipt,Sales Credit Memo,Purchase Receipt,Purchase Invoice,Purchase Return Shipment,Purchase Credit Memo,Transfer Shipment,Transfer Receipt,Service Shipment,Service Invoice,Service Credit Memo,Posted Assembly] }
    { 80  ;   ;Document Line No.   ;Integer       ;CaptionML=[ENU=Document Line No.;
                                                              PTB=N� Linha Documento] }
    { 90  ;   ;Order Type          ;Option        ;CaptionML=[ENU=Order Type;
                                                              PTB=Tipo Ordem];
                                                   OptionCaptionML=[ENU=" ,Production,Transfer,Service,Assembly";
                                                                    PTB=" ,Produ��o,Transfer�ncia,Servi�o,Montagem"];
                                                   OptionString=[ ,Production,Transfer,Service,Assembly];
                                                   Editable=No }
    { 91  ;   ;Order No.           ;Code20        ;CaptionML=[ENU=Order No.;
                                                              PTB=N� Ordem];
                                                   Editable=No }
    { 92  ;   ;Order Line No.      ;Integer       ;CaptionML=[ENU=Order Line No.;
                                                              PTB=N� Linha Ordem];
                                                   Editable=No }
    { 480 ;   ;Dimension Set ID    ;Integer       ;TableRelation="Dimension Set Entry";
                                                   OnLookup=BEGIN
                                                              ShowDimensions;
                                                            END;

                                                   CaptionML=ENU=Dimension Set ID;
                                                   Editable=No }
    { 904 ;   ;Assemble to Order   ;Boolean       ;AccessByPermission=TableData 90=R;
                                                   CaptionML=ENU=Assemble to Order }
    { 1000;   ;Job No.             ;Code20        ;TableRelation=Job.No.;
                                                   CaptionML=[ENU=Job No.;
                                                              PTB=N� Projeto] }
    { 1001;   ;Job Task No.        ;Code20        ;TableRelation="Job Task"."Job Task No." WHERE (Job No.=FIELD(Job No.));
                                                   CaptionML=[ENU=Job Task No.;
                                                              PTB=No. Processamento Tarefa] }
    { 1002;   ;Job Purchase        ;Boolean       ;CaptionML=[ENU=Job Purchase;
                                                              PTB=Tarefa Compra] }
    { 5402;   ;Variant Code        ;Code10        ;TableRelation="Item Variant".Code WHERE (Item No.=FIELD(Item No.));
                                                   CaptionML=[ENU=Variant Code;
                                                              PTB=C�digo Variante] }
    { 5404;   ;Qty. per Unit of Measure;Decimal   ;CaptionML=[ENU=Qty. per Unit of Measure;
                                                              PTB=Qtd. por Unidade Medida];
                                                   DecimalPlaces=0:5 }
    { 5407;   ;Unit of Measure Code;Code10        ;TableRelation="Item Unit of Measure".Code WHERE (Item No.=FIELD(Item No.));
                                                   CaptionML=[ENU=Unit of Measure Code;
                                                              PTB=Cod. Unidade Medida] }
    { 5408;   ;Derived from Blanket Order;Boolean ;CaptionML=[ENU=Derived from Blanket Order;
                                                              PTB=Derivado de Ped. Aberto] }
    { 5700;   ;Cross-Reference No. ;Code20        ;CaptionML=[ENU=Cross-Reference No.;
                                                              PTB=N� Ref. Cruzada] }
    { 5701;   ;Originally Ordered No.;Code20      ;TableRelation=Item;
                                                   AccessByPermission=TableData 5715=R;
                                                   CaptionML=[ENU=Originally Ordered No.;
                                                              PTB=N� Originalmente Ped.] }
    { 5702;   ;Originally Ordered Var. Code;Code10;TableRelation="Item Variant".Code WHERE (Item No.=FIELD(Originally Ordered No.));
                                                   AccessByPermission=TableData 5715=R;
                                                   CaptionML=[ENU=Originally Ordered Var. Code;
                                                              PTB=Cod. Variante Originalmente Ped.] }
    { 5703;   ;Out-of-Stock Substitution;Boolean  ;CaptionML=[ENU=Out-of-Stock Substitution;
                                                              PTB=Substitui��o por Rotura] }
    { 5704;   ;Item Category Code  ;Code10        ;TableRelation="Item Category";
                                                   CaptionML=[ENU=Item Category Code;
                                                              PTB=Cod. Categoria Prod.] }
    { 5705;   ;Nonstock            ;Boolean       ;CaptionML=[ENU=Nonstock;
                                                              PTB=N�o Estoc�vel] }
    { 5706;   ;Purchasing Code     ;Code10        ;TableRelation=Purchasing;
                                                   CaptionML=[ENU=Purchasing Code;
                                                              PTB=Cod. Compra] }
    { 5707;   ;Product Group Code  ;Code10        ;TableRelation="Product Group".Code WHERE (Item Category Code=FIELD(Item Category Code));
                                                   CaptionML=[ENU=Product Group Code;
                                                              PTB=Cod. Gr. Produto] }
    { 5800;   ;Completely Invoiced ;Boolean       ;CaptionML=[ENU=Completely Invoiced;
                                                              PTB=Completamente Faturado] }
    { 5801;   ;Last Invoice Date   ;Date          ;CaptionML=[ENU=Last Invoice Date;
                                                              PTB=�ltima Data N.Fiscal] }
    { 5802;   ;Applied Entry to Adjust;Boolean    ;CaptionML=[ENU=Applied Entry to Adjust;
                                                              PTB=Mov. Aplic. para Ajustar] }
    { 5803;   ;Cost Amount (Expected);Decimal     ;FieldClass=FlowField;
                                                   CalcFormula=Sum("Value Entry"."Cost Amount (Expected)" WHERE (Item Ledger Entry No.=FIELD(Entry No.)));
                                                   CaptionML=[ENU=Cost Amount (Expected);
                                                              PTB=Valor Custo (Esperado)];
                                                   Editable=No;
                                                   AutoFormatType=1 }
    { 5804;   ;Cost Amount (Actual);Decimal       ;FieldClass=FlowField;
                                                   CalcFormula=Sum("Value Entry"."Cost Amount (Actual)" WHERE (Item Ledger Entry No.=FIELD(Entry No.)));
                                                   CaptionML=[ENU=Cost Amount (Actual);
                                                              PTB=Valor Custo (Real)];
                                                   Editable=No;
                                                   AutoFormatType=1 }
    { 5805;   ;Cost Amount (Non-Invtbl.);Decimal  ;FieldClass=FlowField;
                                                   CalcFormula=Sum("Value Entry"."Cost Amount (Non-Invtbl.)" WHERE (Item Ledger Entry No.=FIELD(Entry No.)));
                                                   CaptionML=[ENU=Cost Amount (Non-Invtbl.);
                                                              PTB=Valor Custo (N�o-Inventari�vel.)];
                                                   Editable=No;
                                                   AutoFormatType=1 }
    { 5806;   ;Cost Amount (Expected) (ACY);Decimal;
                                                   FieldClass=FlowField;
                                                   CalcFormula=Sum("Value Entry"."Cost Amount (Expected) (ACY)" WHERE (Item Ledger Entry No.=FIELD(Entry No.)));
                                                   CaptionML=[ENU=Cost Amount (Expected) (ACY);
                                                              PTB=Valor Custo (Esperado) (MA)];
                                                   Editable=No;
                                                   AutoFormatType=1;
                                                   AutoFormatExpr=GetCurrencyCode }
    { 5807;   ;Cost Amount (Actual) (ACY);Decimal ;FieldClass=FlowField;
                                                   CalcFormula=Sum("Value Entry"."Cost Amount (Actual) (ACY)" WHERE (Item Ledger Entry No.=FIELD(Entry No.)));
                                                   CaptionML=[ENU=Cost Amount (Actual) (ACY);
                                                              PTB=Valor Custo (Real) (MA)];
                                                   Editable=No;
                                                   AutoFormatType=1;
                                                   AutoFormatExpr=GetCurrencyCode }
    { 5808;   ;Cost Amount (Non-Invtbl.)(ACY);Decimal;
                                                   FieldClass=FlowField;
                                                   CalcFormula=Sum("Value Entry"."Cost Amount (Non-Invtbl.)(ACY)" WHERE (Item Ledger Entry No.=FIELD(Entry No.)));
                                                   CaptionML=[ENU=Cost Amount (Non-Invtbl.)(ACY);
                                                              PTB=Valor Custo (N�o-Inventari�vel.) (MA)];
                                                   Editable=No;
                                                   AutoFormatType=1;
                                                   AutoFormatExpr=GetCurrencyCode }
    { 5813;   ;Purchase Amount (Expected);Decimal ;FieldClass=FlowField;
                                                   CalcFormula=Sum("Value Entry"."Purchase Amount (Expected)" WHERE (Item Ledger Entry No.=FIELD(Entry No.)));
                                                   CaptionML=[ENU=Purchase Amount (Expected);
                                                              PTB=Valor de Compra (Esperado)];
                                                   Editable=No;
                                                   AutoFormatType=1 }
    { 5814;   ;Purchase Amount (Actual);Decimal   ;FieldClass=FlowField;
                                                   CalcFormula=Sum("Value Entry"."Purchase Amount (Actual)" WHERE (Item Ledger Entry No.=FIELD(Entry No.)));
                                                   CaptionML=[ENU=Purchase Amount (Actual);
                                                              PTB=Valor de compra (Real)];
                                                   Editable=No;
                                                   AutoFormatType=1 }
    { 5815;   ;Sales Amount (Expected);Decimal    ;FieldClass=FlowField;
                                                   CalcFormula=Sum("Value Entry"."Sales Amount (Expected)" WHERE (Item Ledger Entry No.=FIELD(Entry No.)));
                                                   CaptionML=[ENU=Sales Amount (Expected);
                                                              PTB=Valor Venda (Esperado)];
                                                   Editable=No;
                                                   AutoFormatType=1 }
    { 5816;   ;Sales Amount (Actual);Decimal      ;FieldClass=FlowField;
                                                   CalcFormula=Sum("Value Entry"."Sales Amount (Actual)" WHERE (Item Ledger Entry No.=FIELD(Entry No.)));
                                                   CaptionML=[ENU=Sales Amount (Actual);
                                                              PTB=Valor Venda (Atual)];
                                                   Editable=No;
                                                   AutoFormatType=1 }
    { 5817;   ;Correction          ;Boolean       ;CaptionML=[ENU=Correction;
                                                              PTB=Corre��o] }
    { 5818;   ;Shipped Qty. Not Returned;Decimal  ;AccessByPermission=TableData 36=R;
                                                   CaptionML=[ENU=Shipped Qty. Not Returned;
                                                              PTB=Qtd. Enviada N�o Retornada];
                                                   DecimalPlaces=0:5 }
    { 5833;   ;Prod. Order Comp. Line No.;Integer ;AccessByPermission=TableData 5405=R;
                                                   CaptionML=[ENU=Prod. Order Comp. Line No.;
                                                              PTB=N� Linha Componente Ordem Prod] }
    { 6500;   ;Serial No.          ;Code20        ;OnLookup=BEGIN
                                                              ItemTrackingMgt.LookupLotSerialNoInfo("Item No.","Variant Code",0,"Serial No.");
                                                            END;

                                                   CaptionML=[ENU=Serial No.;
                                                              PTB=N� S�rie] }
    { 6501;   ;Lot No.             ;Code20        ;OnLookup=BEGIN
                                                              ItemTrackingMgt.LookupLotSerialNoInfo("Item No.","Variant Code",1,"Lot No.");
                                                            END;

                                                   CaptionML=[ENU=Lot No.;
                                                              PTB=N� Lote] }
    { 6502;   ;Warranty Date       ;Date          ;CaptionML=[ENU=Warranty Date;
                                                              PTB=Data Garantia] }
    { 6503;   ;Expiration Date     ;Date          ;CaptionML=[ENU=Expiration Date;
                                                              PTB=Data Validade] }
    { 6510;   ;Item Tracking       ;Option        ;CaptionML=[ENU=Item Tracking;
                                                              PTB=Acompanhamento Produto];
                                                   OptionCaptionML=[ENU=None,Lot No.,Lot and Serial No.,Serial No.;
                                                                    PTB=Nenhum,No. Lote,No. Lote e S�rie,No. S�rie];
                                                   OptionString=None,Lot No.,Lot and Serial No.,Serial No.;
                                                   Editable=No }
    { 6602;   ;Return Reason Code  ;Code10        ;TableRelation="Return Reason";
                                                   CaptionML=[ENU=Return Reason Code;
                                                              PTB=Cod. Raz�o Devolu��o] }
    { 52112430;;Invoice No.        ;Code20        ;CaptionML=[ENU=Invoice Doc. No.;
                                                              PTB=N� Doc. Nota Fiscal] }
    { 52112440;;Branch Code (Old)  ;Code20        ;TableRelation="Branch Information" }
    { 52112450;;Branch Code        ;Code20        ;TableRelation="Branch Information";
                                                   CaptionML=[ENU=Branch Code;
                                                              PTB=C�d. Filial] }
    { 52112460;;Third-Party Stock Entry;Boolean   ;CaptionML=[ENU=Third-Party Stock Entry;
                                                              PTB=Mov. Estoque Terceiros] }
  }
  KEYS
  {
    {    ;Entry No.                               ;Clustered=Yes }
    {    ;Item No.                                ;SumIndexFields=Invoiced Quantity }
    {    ;Item No.,Posting Date                    }
    {    ;Item No.,Entry Type,Variant Code,Drop Shipment,Location Code,Posting Date;
                                                   SumIndexFields=Quantity,Invoiced Quantity }
    {    ;Source Type,Source No.,Item No.,Variant Code,Posting Date;
                                                   SumIndexFields=Quantity }
    {    ;Item No.,Open,Variant Code,Positive,Location Code,Posting Date;
                                                   SumIndexFields=Quantity,Remaining Quantity }
    {    ;Item No.,Open,Variant Code,Positive,Location Code,Posting Date,Expiration Date,Lot No.,Serial No.;
                                                   SumIndexFields=Quantity,Remaining Quantity }
    {    ;Country/Region Code,Entry Type,Posting Date }
    {    ;Document No.,Document Type,Document Line No. }
    { No ;Item No.,Entry Type,Variant Code,Drop Shipment,Global Dimension 1 Code,Global Dimension 2 Code,Location Code,Posting Date;
                                                   SumIndexFields=Quantity,Invoiced Quantity }
    { No ;Source Type,Source No.,Global Dimension 1 Code,Global Dimension 2 Code,Item No.,Variant Code,Posting Date;
                                                   SumIndexFields=Quantity }
    {    ;Order Type,Order No.,Order Line No.,Entry Type,Prod. Order Comp. Line No.;
                                                   SumIndexFields=Quantity;
                                                   MaintainSIFTIndex=No }
    {    ;Item No.,Applied Entry to Adjust         }
    {    ;Item No.,Positive,Location Code,Variant Code }
    { No ;Entry Type,Nonstock,Item No.,Posting Date }
    { No ;Item No.,Location Code,Open,Variant Code,Unit of Measure Code,Lot No.,Serial No.;
                                                   SumIndexFields=Remaining Quantity }
    {    ;Item No.,Open,Variant Code,Positive,Lot No.,Serial No.;
                                                   MaintainSQLIndex=No;
                                                   MaintainSIFTIndex=No }
    {    ;Item No.,Open,Variant Code,Location Code,Item Tracking,Lot No.,Serial No.;
                                                   SumIndexFields=Remaining Quantity;
                                                   MaintainSQLIndex=No;
                                                   MaintainSIFTIndex=No }
    { No ;Lot No.                                  }
    { No ;Serial No.                               }
  }
  FIELDGROUPS
  {
    { 1   ;DropDown            ;Entry No.,Description,Item No.,Posting Date,Entry Type,Document No. }
  }
  CODE
  {
    VAR
      GLSetup@1000 : Record 98;
      ReservEntry@1001 : Record 337;
      ReservEngineMgt@1002 : Codeunit 99000831;
      ReserveItemLedgEntry@1003 : Codeunit 99000841;
      ItemTrackingMgt@1006 : Codeunit 6500;
      GLSetupRead@1005 : Boolean;
      IsNotOnInventoryErr@1004 : TextConst 'ENU=You have insufficient quantity of Item %1 on inventory.';

    PROCEDURE GetCurrencyCode@1() : Code[10];
    BEGIN
      IF NOT GLSetupRead THEN BEGIN
        GLSetup.GET;
        GLSetupRead := TRUE;
      END;
      EXIT(GLSetup."Additional Reporting Currency");
    END;

    PROCEDURE ShowReservationEntries@2(Modal@1000 : Boolean);
    BEGIN
      ReservEngineMgt.InitFilterAndSortingLookupFor(ReservEntry,TRUE);
      ReserveItemLedgEntry.FilterReservFor(ReservEntry,Rec);
      IF Modal THEN
        PAGE.RUNMODAL(PAGE::"Reservation Entries",ReservEntry)
      ELSE
        PAGE.RUN(PAGE::"Reservation Entries",ReservEntry);
    END;

    PROCEDURE SetAppliedEntryToAdjust@66(AppliedEntryToAdjust@1001 : Boolean);
    BEGIN
      IF "Applied Entry to Adjust" <> AppliedEntryToAdjust THEN BEGIN
        "Applied Entry to Adjust" := AppliedEntryToAdjust;
        MODIFY;
      END;
    END;

    PROCEDURE SetAvgTransCompletelyInvoiced@80() : Boolean;
    VAR
      ItemApplnEntry@1005 : Record 339;
      InbndItemLedgEntry@1004 : Record 32;
      CompletelyInvoiced@1002 : Boolean;
    BEGIN
      IF "Entry Type" <> "Entry Type"::Transfer THEN
        EXIT(FALSE);

      ItemApplnEntry.SETCURRENTKEY("Item Ledger Entry No.");
      ItemApplnEntry.SETRANGE("Item Ledger Entry No.","Entry No.");
      ItemApplnEntry.FIND('-');
      IF NOT "Completely Invoiced" THEN BEGIN
        CompletelyInvoiced := TRUE;
        REPEAT
          InbndItemLedgEntry.GET(ItemApplnEntry."Inbound Item Entry No.");
          IF NOT InbndItemLedgEntry."Completely Invoiced" THEN
            CompletelyInvoiced := FALSE;
        UNTIL ItemApplnEntry.NEXT = 0;

        IF CompletelyInvoiced THEN BEGIN
          SetCompletelyInvoiced;
          EXIT(TRUE);
        END;
      END;
      EXIT(FALSE);
    END;

    PROCEDURE SetCompletelyInvoiced@3();
    BEGIN
      IF NOT "Completely Invoiced" THEN BEGIN
        "Completely Invoiced" := TRUE;
        MODIFY;
      END;
    END;

    PROCEDURE AppliedEntryToAdjustExists@40(ItemNo@1001 : Code[20]) : Boolean;
    BEGIN
      RESET;
      SETCURRENTKEY("Item No.","Applied Entry to Adjust");
      SETRANGE("Item No.",ItemNo);
      SETRANGE("Applied Entry to Adjust",TRUE);
      EXIT(FIND('-'));
    END;

    PROCEDURE IsOutbndConsump@57() : Boolean;
    BEGIN
      EXIT(("Entry Type" = "Entry Type"::Consumption) AND NOT Positive);
    END;

    PROCEDURE IsExactCostReversingPurchase@59() : Boolean;
    BEGIN
      EXIT(
        ("Applies-to Entry" <> 0) AND
        ("Entry Type" = "Entry Type"::Purchase) AND
        ("Invoiced Quantity" < 0));
    END;

    PROCEDURE IsExactCostReversingOutput@8() : Boolean;
    BEGIN
      EXIT(
        ("Applies-to Entry" <> 0) AND
        ("Entry Type" IN ["Entry Type"::Output,"Entry Type"::"Assembly Output"]) AND
        ("Invoiced Quantity" < 0));
    END;

    PROCEDURE UpdateItemTracking@5();
    VAR
      ItemTrackingMgt@1000 : Codeunit 6500;
    BEGIN
      "Item Tracking" := ItemTrackingMgt.ItemTrackingOption("Lot No.","Serial No.");
    END;

    PROCEDURE GetUnitCostLCY@4() : Decimal;
    BEGIN
      IF Quantity = 0 THEN
        EXIT("Cost Amount (Actual)");

      EXIT(ROUND("Cost Amount (Actual)" / Quantity,0.00001));
    END;

    PROCEDURE FilterLinesWithItemToPlan@69(VAR Item@1000 : Record 27;NetChange@1001 : Boolean);
    BEGIN
      RESET;
      SETCURRENTKEY("Item No.",Open,"Variant Code",Positive,"Location Code","Posting Date");
      SETRANGE("Item No.",Item."No.");
      SETRANGE(Open,TRUE);
      SETFILTER("Variant Code",Item.GETFILTER("Variant Filter"));
      SETFILTER("Location Code",Item.GETFILTER("Location Filter"));
      SETFILTER("Global Dimension 1 Code",Item.GETFILTER("Global Dimension 1 Filter"));
      SETFILTER("Global Dimension 2 Code",Item.GETFILTER("Global Dimension 2 Filter"));
      IF NetChange THEN
        SETFILTER("Posting Date",Item.GETFILTER("Date Filter"));
    END;

    PROCEDURE FindLinesWithItemToPlan@7(VAR Item@1000 : Record 27;NetChange@1001 : Boolean) : Boolean;
    BEGIN
      FilterLinesWithItemToPlan(Item,NetChange);
      EXIT(FIND('-'));
    END;

    PROCEDURE LinesWithItemToPlanExist@67(VAR Item@1000 : Record 27;NetChange@1001 : Boolean) : Boolean;
    BEGIN
      FilterLinesWithItemToPlan(Item,NetChange);
      EXIT(NOT ISEMPTY);
    END;

    PROCEDURE IsOutbndSale@5888() : Boolean;
    BEGIN
      EXIT(("Entry Type" = "Entry Type"::Sale) AND NOT Positive);
    END;

    PROCEDURE ShowDimensions@6();
    VAR
      DimMgt@1000 : Codeunit 408;
    BEGIN
      DimMgt.ShowDimensionSet("Dimension Set ID",STRSUBSTNO('%1 %2',TABLECAPTION,"Entry No."));
    END;

    PROCEDURE CalculateRemQuantity@10(ItemLedgEntryNo@1000 : Integer;PostingDate@1001 : Date) : Decimal;
    VAR
      ItemApplnEntry@1002 : Record 339;
      RemQty@1003 : Decimal;
    BEGIN
      ItemApplnEntry.SETCURRENTKEY("Inbound Item Entry No.");
      ItemApplnEntry.SETRANGE("Inbound Item Entry No.",ItemLedgEntryNo);
      RemQty := 0;
      IF ItemApplnEntry.FINDSET THEN
        REPEAT
          IF ItemApplnEntry."Posting Date" <= PostingDate THEN
            RemQty += ItemApplnEntry.Quantity;
        UNTIL ItemApplnEntry.NEXT = 0;
      EXIT(RemQty);
    END;

    PROCEDURE VerifyOnInventory@9();
    VAR
      Item@1000 : Record 27;
    BEGIN
      IF NOT Open THEN
        EXIT;
      IF Quantity >= 0 THEN
        EXIT;
      CASE "Entry Type" OF
        "Entry Type"::Consumption,"Entry Type"::"Assembly Consumption","Entry Type"::Transfer:
          ERROR(IsNotOnInventoryErr,"Item No.");
        ELSE BEGIN
          Item.GET("Item No.");
          IF Item.PreventNegativeInventory THEN
            ERROR(IsNotOnInventoryErr,"Item No.");
        END;
      END;
    END;

    PROCEDURE CalculateRemInventoryValue@12(ItemLedgEntryNo@1000 : Integer;ItemLedgEntryQty@1004 : Decimal;RemQty@1005 : Decimal;IncludeExpectedCost@1001 : Boolean;PostingDate@1006 : Date) : Decimal;
    VAR
      ValueEntry@1002 : Record 5802;
      AdjustedCost@1003 : Decimal;
      TotalQty@1007 : Decimal;
    BEGIN
      ValueEntry.SETCURRENTKEY("Item Ledger Entry No.");
      ValueEntry.SETRANGE("Item Ledger Entry No.",ItemLedgEntryNo);
      ValueEntry.SETFILTER("Posting Date",'<=%1',PostingDate);
      IF NOT IncludeExpectedCost THEN
        ValueEntry.SETRANGE("Expected Cost",FALSE);
      IF ValueEntry.FINDSET THEN
        REPEAT
          IF ValueEntry."Entry Type" = ValueEntry."Entry Type"::Revaluation THEN
            TotalQty := ValueEntry."Valued Quantity"
          ELSE
            TotalQty := ItemLedgEntryQty;
          IF ValueEntry."Entry Type" <> ValueEntry."Entry Type"::Rounding THEN
            IF IncludeExpectedCost THEN
              AdjustedCost += RemQty / TotalQty * (ValueEntry."Cost Amount (Actual)" + ValueEntry."Cost Amount (Expected)")
            ELSE
              AdjustedCost += RemQty / TotalQty * ValueEntry."Cost Amount (Actual)";
        UNTIL ValueEntry.NEXT = 0;
      EXIT(AdjustedCost);
    END;

    BEGIN
    END.
  }
}

