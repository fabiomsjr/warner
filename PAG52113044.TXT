OBJECT Page 52113044 CNAB Payment
{
  OBJECT-PROPERTIES
  {
    Date=19/01/16;
    Time=16:36:05;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Electronic Payment;
               PTB=Pagamento Eletr�nico];
    SourceTable=Table52113043;
    PageType=Card;
    RefreshOnActivate=Yes;
    PromotedActionCategoriesML=[ENU=New,Process,Report,Payment,Approval,Posting,Dummy;
                                PTB=Novo,Processo,Relat�rio,Pagamento,Aprova��o,Registro,Dummy];
    OnOpenPage=VAR
                 glSetup@52006500 : Record 98;
                 ApprovFlowSetup@52006501 : Record 52113076;
               BEGIN
                 glSetup.GET;
                 BranchVisible := glSetup."Branch Dimension Code" <> '';
                 ApprovalVisible := ApprovFlowSetup.GET AND ApprovFlowSetup."CNAB Payment Approval";
                 Editable := TRUE;
               END;

    OnAfterGetCurrRecord=BEGIN
                           UpdateApprovalControls;
                         END;

    ActionList=ACTIONS
    {
      { 52006516;    ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 52006522;1   ;ActionGroup;
                      CaptionML=[ENU=Functions;
                                 PTB=Fun��es] }
      { 52006520;2   ;Action    ;
                      Name=SuggestVendorPayments;
                      Ellipsis=Yes;
                      CaptionML=[ENU=Suggest Payments;
                                 PTB=Sugerir Pagamentos];
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=SuggestVendorPayments;
                      PromotedCategory=Category4;
                      OnAction=VAR
                                 suggestVendorPayments@1001 : Report 393;
                               BEGIN
                                 CurrPage.UPDATE(TRUE);
                                 suggestVendorPayments.SetCNABPayment(Rec);
                                 suggestVendorPayments.RUNMODAL;
                               END;
                                }
      { 52006521;2   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[ENU=Export Remittance File;
                                 PTB=Exportar Arquivo Remessa];
                      RunObject=Codeunit 52113023;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=ExportToBank;
                      PromotedCategory=Category4;
                      OnAction=VAR
                                 cnab@52006500 : Codeunit 52113023;
                               BEGIN
                               END;
                                }
      { 52006530;1   ;ActionGroup;
                      CaptionML=[ENU=Posting;
                                 PTB=Registro] }
      { 52006531;2   ;Action    ;
                      Name=PostPreview;
                      CaptionML=[ENU=Posting Preview;
                                 PTB=Verificar Contabiliza��o];
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=TestReport;
                      PromotedCategory=Category6;
                      OnAction=VAR
                                 postingPreview@52006500 : Page 52112451;
                               BEGIN
                                 postingPreview.SetCNABPayment(Rec);
                                 postingPreview.RUNMODAL;
                               END;
                                }
      { 52006529;2   ;Action    ;
                      Name=Post;
                      ShortCutKey=F9;
                      Ellipsis=Yes;
                      CaptionML=[ENU=Post;
                                 PTB=Registrar];
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=PostBatch;
                      PromotedCategory=Category6;
                      OnAction=VAR
                                 cnabPost@52006500 : Codeunit 52113024;
                               BEGIN
                                 IF CONFIRM(Text001) THEN
                                   cnabPost.RUN(Rec);
                               END;
                                }
      { 52006528;1   ;ActionGroup;
                      CaptionML=[ENU=Approval;
                                 PTB=Aprova��o];
                      Visible=ApprovalEnabled }
      { 52006515;2   ;Action    ;
                      CaptionML=[ENU=Send Approval Request;
                                 PTB=Enviar Requisi��o Aprova��o];
                      Promoted=Yes;
                      Visible=ApprovalVisible;
                      Enabled=ApprovalEnabled;
                      PromotedIsBig=Yes;
                      Image=SendApprovalRequest;
                      PromotedCategory=Category5;
                      OnAction=BEGIN
                                 SendApprovalRequest;
                                 CurrPage.UPDATE;
                               END;
                                }
      { 52006514;2   ;Action    ;
                      CaptionML=[ENU=Cancel Approval Request;
                                 PTB=Cancelar Requisi��o Aprova��o];
                      Promoted=Yes;
                      Visible=ApprovalVisible;
                      Enabled=ApprovalEnabled;
                      PromotedIsBig=Yes;
                      Image=Cancel;
                      PromotedCategory=Category5;
                      OnAction=BEGIN
                                 CancelApprovalRequest;
                                 CurrPage.UPDATE;
                               END;
                                }
      { 52006505;2   ;Action    ;
                      CaptionML=[ENU=Re&open;
                                 PTB=Re&abrir];
                      Promoted=Yes;
                      Visible=ApprovalVisible;
                      Enabled=ApprovalEnabled;
                      Image=ReOpen;
                      PromotedCategory=Category5;
                      OnAction=VAR
                                 ReleasePurchDoc@1001 : Codeunit 415;
                               BEGIN
                                 Reopen;
                                 CurrPage.UPDATE;
                               END;
                                }
      { 52006523;    ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 52006537;1   ;Action    ;
                      CaptionML=[ENU=Aprovals;
                                 PTB=Aprova��es];
                      Promoted=Yes;
                      Visible=ApprovalVisible;
                      Enabled=ApprovalEnabled;
                      PromotedIsBig=Yes;
                      Image=Approvals;
                      PromotedCategory=Category5;
                      OnAction=BEGIN
                                 OpenApprovalFlowCard;
                               END;
                                }
      { 52006518;1   ;Action    ;
                      Name=OpenFiles;
                      CaptionML=[ENU=Files;
                                 PTB=Arquivos];
                      Promoted=Yes;
                      Image=ExternalDocument;
                      PromotedCategory=Category4;
                      OnAction=VAR
                                 docFileMgt@52006500 : Codeunit 52112428;
                               BEGIN
                                 docFileMgt.OpenFiles(Rec);
                               END;
                                }
      { 52006533;1   ;Action    ;
                      CaptionML=[ENU=&Navigate;
                                 PTB=&Navegar];
                      Promoted=Yes;
                      Image=Navigate;
                      PromotedCategory=Category4;
                      OnAction=BEGIN
                                 Navigate;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 52006500;0;Container;
                ContainerType=ContentArea }

    { 52006501;1;Group    ;
                Name=General;
                CaptionML=[ENU=General;
                           PTB=Geral];
                Editable=Editable;
                GroupType=Group }

    { 52006502;2;Field    ;
                SourceExpr="No.";
                Importance=Promoted;
                OnAssistEdit=BEGIN
                               IF AssistEdit(xRec) THEN
                                 CurrPage.UPDATE;
                             END;
                              }

    { 52006503;2;Field    ;
                SourceExpr=Description }

    { 52006512;2;Field    ;
                SourceExpr="Bank Account No.";
                Importance=Promoted }

    { 52006532;2;Field    ;
                SourceExpr="Branch Code";
                Visible=BranchVisible;
                Editable=FALSE }

    { 52006513;2;Field    ;
                SourceExpr="Layout Code";
                Importance=Promoted;
                OnValidate=BEGIN
                             UpdateApprovalControls;
                           END;
                            }

    { 52006517;2;Field    ;
                SourceExpr="Approval Status";
                Importance=Promoted;
                Visible=ApprovalVisible;
                Editable=FALSE }

    { 52006506;2;Field    ;
                SourceExpr="User ID";
                Importance=Promoted;
                Editable=FALSE }

    { 52006507;2;Field    ;
                SourceExpr="User Name";
                Visible=FALSE }

    { 52006504;2;Field    ;
                SourceExpr="Create Date";
                Editable=FALSE }

    { 52006511;1;Part     ;
                Name=Subpage;
                SubPageLink=CNAB Payment No.=FIELD(No.);
                PagePartID=Page52113045;
                Editable=Editable;
                PartType=Page }

    { 52006508;0;Container;
                ContainerType=FactBoxArea }

    { 52006524;1;Part     ;
                SubPageLink=No.=FIELD(Bank Account No.);
                PagePartID=Page52113051;
                PartType=Page }

    { 52006525;1;Part     ;
                SubPageView=SORTING(No.);
                SubPageLink=No.=FIELD(Vendor No.);
                PagePartID=Page9093;
                ProviderID=52006511;
                PartType=Page }

    { 52006526;1;Part     ;
                SubPageView=SORTING(No.);
                SubPageLink=No.=FIELD(Vendor No.);
                PagePartID=Page9094;
                ProviderID=52006511;
                PartType=Page }

    { 52006519;1;Part     ;
                SubPageView=SORTING(CNAB Payment No.,CNAB Payment Line No.,Custom Field Code);
                SubPageLink=CNAB Payment No.=FIELD(CNAB Payment No.),
                            CNAB Payment Line No.=FIELD(Line No.);
                PagePartID=Page52113050;
                ProviderID=52006511;
                PartType=Page;
                ShowFilter=No }

    { 52006527;1;Part     ;
                SubPageView=SORTING(CNAB Payment No.,CNAB Payment Line No.,Line No.);
                SubPageLink=CNAB Payment No.=FIELD(CNAB Payment No.),
                            CNAB Payment Line No.=FIELD(Line No.);
                PagePartID=Page52113052;
                ProviderID=52006511;
                PartType=Page }

    { 52006509;1;Part     ;
                PartType=System;
                SystemPartID=RecordLinks }

    { 52006510;1;Part     ;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      Text001@52006500 : TextConst 'ENU=Are you sure you wish to post the open entries?;PTB=Confirma que deseja registrar os movimentos pendentes?';
      BranchVisible@52006501 : Boolean;
      ApprovalEnabled@52006502 : Boolean;
      ApprovalVisible@52006503 : Boolean;
      Editable@52006504 : Boolean;

    LOCAL PROCEDURE UpdateApprovalControls@52006500();
    VAR
      PaymentApprovMgt@52006500 : Codeunit 52113083;
    BEGIN
      ApprovalEnabled := PaymentApprovMgt.IsApprovalEnabled(Rec);
      IF NOT ApprovalEnabled THEN
        Editable := TRUE
      ELSE
        Editable := ("Approval Status" = "Approval Status"::Open);
    END;

    BEGIN
    END.
  }
}

