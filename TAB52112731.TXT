OBJECT Table 52112731 Purchase Request Quote Line
{
  OBJECT-PROPERTIES
  {
    Date=16/12/15;
    Time=11:44:51;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    OnInsert=VAR
               PurchTracking@52006500 : Record 52112736;
             BEGIN
               IF "Tracking ID" = 0 THEN BEGIN
                 PurchTracking.Create;
                 "Tracking ID" := PurchTracking.ID;
               END;
             END;

    OnDelete=VAR
               purchReqLine@35000000 : Record 52112724;
             BEGIN
               IF "Item No." = '' THEN
                 EXIT;

               purchReqLine.SETRANGE("Item Type", purchReqLine."Item Type"::Item);
               purchReqLine.SETRANGE("Item No.", "Item No.");
               purchReqLine.SETRANGE("Quote No.", "Purch Req. Quote No.");
               WHILE purchReqLine.FINDFIRST DO BEGIN
                 purchReqLine."Quote No." := '';
                 purchReqLine.MODIFY;
               END;

               purchReqLine.RESET;
               purchReqLine.SETCURRENTKEY("Tracking ID");
               purchReqLine.SETRANGE("Tracking ID", "Tracking ID");
               purchReqLine.MODIFYALL("Tracking ID", 0);
             END;

    CaptionML=[ENU=Quote Request Line;
               PTB=Linha Cota��o Requisi��o];
  }
  FIELDS
  {
    { 1   ;   ;Purch Req. Quote No.;Code20        ;TableRelation="Purchase Request Quote";
                                                   CaptionML=[ENU=No.;
                                                              PTB=N�] }
    { 4   ;   ;Item Type           ;Option        ;OnValidate=BEGIN
                                                                VALIDATE("Item No.", '');
                                                              END;

                                                   CaptionML=[ENU=Item Type;
                                                              PTB=Tipo Item];
                                                   OptionCaptionML=[ENU=" ,Item,Fixed Asset";
                                                                    PTB=" ,Produto,Ativo Fixo"];
                                                   OptionString=[ ,Item,Fixed Asset] }
    { 5   ;   ;Item No.            ;Code20        ;TableRelation=IF (Item Type=CONST(Item)) Item
                                                                 ELSE IF (Item Type=CONST(Fixed Asset)) "Fixed Asset";
                                                   OnValidate=VAR
                                                                item@52006500 : Record 27;
                                                                fa@52006501 : Record 5600;
                                                              BEGIN
                                                                ValidateItemQtyUpdate;

                                                                CLEAR(Description);
                                                                IF "Item No." = '' THEN
                                                                  EXIT;

                                                                CASE "Item Type" OF
                                                                  "Item Type"::Item: BEGIN
                                                                    item.GET("Item No.");
                                                                    item.TESTFIELD("Base Unit of Measure");
                                                                    VALIDATE("Unit of Measure", item."Base Unit of Measure");
                                                                    Description := item.Description;
                                                                  END;
                                                                  "Item Type"::"Fixed Asset": BEGIN
                                                                    fa.GET("Item No.");
                                                                    Description := fa.Description;
                                                                  END;
                                                                END;
                                                              END;

                                                   CaptionML=[ENU=Item No.;
                                                              PTB=N� Produto] }
    { 7   ;   ;Line No.            ;Integer       ;CaptionML=[ENU=Line No.;
                                                              PTB=N� Linha] }
    { 10  ;   ;Description         ;Text250       ;CaptionML=[ENU=Description;
                                                              PTB=Descri��o];
                                                   Editable=No }
    { 20  ;   ;Quantity            ;Decimal       ;OnValidate=BEGIN
                                                                ValidateItemQtyUpdate;
                                                              END;

                                                   CaptionML=[ENU=Quantity;
                                                              PTB=Quantidade];
                                                   MinValue=0;
                                                   BlankNumbers=BlankZero }
    { 25  ;   ;Unit of Measure     ;Code20        ;TableRelation="Unit of Measure";
                                                   CaptionML=[ENU=Unit of Measure;
                                                              PTB=Unidade de Medida] }
    { 30  ;   ;From Request        ;Boolean       ;CaptionML=[ENU=From Request;
                                                              PTB=De Requisi��o] }
    { 40  ;   ;Winning Vendor No.  ;Code20        ;OnValidate=BEGIN
                                                                CALCFIELDS("Winning Vendor Name");
                                                              END;

                                                   CaptionML=[ENU=Winning Vendor No.;
                                                              PTB=N� Fornecedor Vencedor] }
    { 41  ;   ;Winning Vendor Name ;Text50        ;FieldClass=FlowField;
                                                   CalcFormula=Lookup(Vendor.Name WHERE (No.=FIELD(Winning Vendor No.)));
                                                   CaptionML=[ENU=Winning Vendor Name;
                                                              PTB=Nome Fornecedor Vencedor];
                                                   Editable=No }
    { 100 ;   ;Purch. Order No.    ;Code20        ;TableRelation="Purchase Header".No. WHERE (Document Type=CONST(Order));
                                                   CaptionML=[ENU=Purch. Order No.;
                                                              PTB=N� Pedido Compra] }
    { 110 ;   ;Purch. Order Line No.;Integer      ;TableRelation="Purchase Line"."Line No." WHERE (Document Type=CONST(Order),
                                                                                                   Document No.=FIELD(Purch. Order No.));
                                                   CaptionML=[ENU=Purch. Order Line No.;
                                                              PTB=N� Linha Pedido Compra] }
    { 200 ;   ;Tracking ID         ;Integer       ;TableRelation="Purchase Tracking";
                                                   CaptionML=[ENU=Tracking ID;
                                                              PTB=ID Rastreabilidade] }
    { 480 ;   ;Dimension Set ID    ;Integer       ;TableRelation="Dimension Set Entry";
                                                   CaptionML=[ENU=Dimension Set ID;
                                                              PTB=ID Dimens�es] }
    { 1001;   ;Vendor 1 Price      ;Decimal       ;OnValidate=BEGIN
                                                                VALIDATE("Winning Vendor No.", '');
                                                                "Vendor 1 Total" := ROUND(Quantity * "Vendor 1 Price");
                                                              END;

                                                   CaptionML=[ENU=Vendor 1 Price;
                                                              PTB=Pre�o Fornecedor 1];
                                                   DecimalPlaces=2:10;
                                                   MinValue=0;
                                                   BlankNumbers=BlankZero;
                                                   CaptionClass=GetCaption(1) }
    { 1002;   ;Vendor 2 Price      ;Decimal       ;OnValidate=BEGIN
                                                                VALIDATE("Winning Vendor No.", '');
                                                                "Vendor 2 Total" := ROUND(Quantity * "Vendor 2 Price");
                                                              END;

                                                   CaptionML=[ENU=Vendor 2 Price;
                                                              PTB=Pre�o Fornecedor 2];
                                                   DecimalPlaces=2:10;
                                                   MinValue=0;
                                                   BlankNumbers=BlankZero;
                                                   CaptionClass=GetCaption(2) }
    { 1003;   ;Vendor 3 Price      ;Decimal       ;OnValidate=BEGIN
                                                                VALIDATE("Winning Vendor No.", '');
                                                                "Vendor 3 Total" := ROUND(Quantity * "Vendor 3 Price");
                                                              END;

                                                   CaptionML=[ENU=Vendor 3 Price;
                                                              PTB=Pre�o Fornecedor 3];
                                                   DecimalPlaces=2:10;
                                                   MinValue=0;
                                                   BlankNumbers=BlankZero;
                                                   CaptionClass=GetCaption(3) }
    { 1004;   ;Vendor 4 Price      ;Decimal       ;OnValidate=BEGIN
                                                                VALIDATE("Winning Vendor No.", '');
                                                                "Vendor 4 Total" := ROUND(Quantity * "Vendor 4 Price");
                                                              END;

                                                   CaptionML=[ENU=Vendor 4 Price;
                                                              PTB=Pre�o Fornecedor 4];
                                                   DecimalPlaces=2:10;
                                                   MinValue=0;
                                                   BlankNumbers=BlankZero;
                                                   CaptionClass=GetCaption(4) }
    { 1005;   ;Vendor 5 Price      ;Decimal       ;OnValidate=BEGIN
                                                                VALIDATE("Winning Vendor No.", '');
                                                                "Vendor 5 Total" := ROUND(Quantity * "Vendor 5 Price");
                                                              END;

                                                   CaptionML=[ENU=Vendor 5 Price;
                                                              PTB=Pre�o Fornecedor 5];
                                                   DecimalPlaces=2:10;
                                                   MinValue=0;
                                                   BlankNumbers=BlankZero;
                                                   CaptionClass=GetCaption(5) }
    { 1006;   ;Vendor 6 Price      ;Decimal       ;OnValidate=BEGIN
                                                                VALIDATE("Winning Vendor No.", '');
                                                                "Vendor 6 Total" := ROUND(Quantity * "Vendor 6 Price");
                                                              END;

                                                   CaptionML=[ENU=Vendor 6 Price;
                                                              PTB=Pre�o Fornecedor 6];
                                                   DecimalPlaces=2:10;
                                                   MinValue=0;
                                                   BlankNumbers=BlankZero;
                                                   CaptionClass=GetCaption(6) }
    { 1007;   ;Vendor 7 Price      ;Decimal       ;OnValidate=BEGIN
                                                                VALIDATE("Winning Vendor No.", '');
                                                                "Vendor 7 Total" := ROUND(Quantity * "Vendor 7 Price");
                                                              END;

                                                   CaptionML=[ENU=Vendor 7 Price;
                                                              PTB=Pre�o Fornecedor 7];
                                                   DecimalPlaces=2:10;
                                                   MinValue=0;
                                                   BlankNumbers=BlankZero;
                                                   CaptionClass=GetCaption(7) }
    { 1008;   ;Vendor 8 Price      ;Decimal       ;OnValidate=BEGIN
                                                                VALIDATE("Winning Vendor No.", '');
                                                                "Vendor 8 Total" := ROUND(Quantity * "Vendor 8 Price");
                                                              END;

                                                   CaptionML=[ENU=Vendor 8 Price;
                                                              PTB=Pre�o Fornecedor 8];
                                                   DecimalPlaces=2:10;
                                                   MinValue=0;
                                                   BlankNumbers=BlankZero;
                                                   CaptionClass=GetCaption(8) }
    { 1009;   ;Vendor 9 Price      ;Decimal       ;OnValidate=BEGIN
                                                                VALIDATE("Winning Vendor No.", '');
                                                                "Vendor 9 Total" := ROUND(Quantity * "Vendor 9 Price");
                                                              END;

                                                   CaptionML=[ENU=Vendor 9 Price;
                                                              PTB=Pre�o Fornecedor 9];
                                                   DecimalPlaces=2:10;
                                                   MinValue=0;
                                                   BlankNumbers=BlankZero;
                                                   CaptionClass=GetCaption(9) }
    { 1010;   ;Vendor 10 Price     ;Decimal       ;OnValidate=BEGIN
                                                                VALIDATE("Winning Vendor No.", '');
                                                                "Vendor 10 Total" := ROUND(Quantity * "Vendor 10 Price");
                                                              END;

                                                   CaptionML=[ENU=Vendor 10 Price;
                                                              PTB=Pre�o Fornecedor 10];
                                                   DecimalPlaces=2:10;
                                                   MaxValue=0;
                                                   BlankNumbers=BlankZero;
                                                   CaptionClass=GetCaption(10) }
    { 1101;   ;Vendor 1 Total      ;Decimal       ;DecimalPlaces=2:10;
                                                   BlankNumbers=BlankZero }
    { 1102;   ;Vendor 2 Total      ;Decimal       ;DecimalPlaces=2:10;
                                                   BlankNumbers=BlankZero }
    { 1103;   ;Vendor 3 Total      ;Decimal       ;DecimalPlaces=2:10;
                                                   BlankNumbers=BlankZero }
    { 1104;   ;Vendor 4 Total      ;Decimal       ;DecimalPlaces=2:10;
                                                   BlankNumbers=BlankZero }
    { 1105;   ;Vendor 5 Total      ;Decimal       ;DecimalPlaces=2:10;
                                                   BlankNumbers=BlankZero }
    { 1106;   ;Vendor 6 Total      ;Decimal       ;DecimalPlaces=2:10;
                                                   BlankNumbers=BlankZero }
    { 1107;   ;Vendor 7 Total      ;Decimal       ;DecimalPlaces=2:10;
                                                   BlankNumbers=BlankZero }
    { 1108;   ;Vendor 8 Total      ;Decimal       ;DecimalPlaces=2:10;
                                                   BlankNumbers=BlankZero }
    { 1109;   ;Vendor 9 Total      ;Decimal       ;DecimalPlaces=2:10;
                                                   BlankNumbers=BlankZero }
    { 1110;   ;Vendor 10 Total     ;Decimal       ;DecimalPlaces=2:10;
                                                   BlankNumbers=BlankZero }
  }
  KEYS
  {
    {    ;Purch Req. Quote No.,Item Type,Item No.,Line No.;
                                                   Clustered=Yes }
    {    ;Purch. Order No.,Purch. Order Line No.   }
    {    ;Purch Req. Quote No.,Line No.            }
    {    ;Tracking ID                              }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      NoneLbl@52006500 : TextConst 'ENU=None;PTB=Nenhum';
      ItemQtyUpdateErr@52006506 : TextConst 'ENU=Updating a request item is not allowed.;PTB=N�o � permitido alterar um item de requisi��o.';

    PROCEDURE GetCaption@52006504(index@52006500 : Integer) : Text;
    VAR
      glSetup@52006502 : Record 98;
      purchReqQuote@52006501 : Record 52112730;
      capt@52006503 : Text;
    BEGIN
      IF "Purch Req. Quote No." = '' THEN
        EXIT;

      glSetup.GET;
      purchReqQuote.GET("Purch Req. Quote No.");
      purchReqQuote.CALCFIELDS("Vendor Name 1", "Vendor Name 2", "Vendor Name 3", "Vendor Name 4", "Vendor Name 5",
                               "Vendor Name 6", "Vendor Name 7", "Vendor Name 8", "Vendor Name 9", "Vendor Name 10");

      CASE index OF
        1:  capt := purchReqQuote."Vendor Name 1";
        2:  capt := purchReqQuote."Vendor Name 2";
        3:  capt := purchReqQuote."Vendor Name 3";
        4:  capt := purchReqQuote."Vendor Name 4";
        5:  capt := purchReqQuote."Vendor Name 5";
        6:  capt := purchReqQuote."Vendor Name 6";
        7:  capt := purchReqQuote."Vendor Name 7";
        8:  capt := purchReqQuote."Vendor Name 8";
        9:  capt := purchReqQuote."Vendor Name 9";
        10: capt := purchReqQuote."Vendor Name 10";
      END;
      EXIT(capt);
    END;

    PROCEDURE SelectWinner@52006500();
    VAR
      vendors@52006501 : ARRAY [10] OF Code[20];
      listedVendors@52006508 : ARRAY [11] OF Code[20];
      prices@52006500 : ARRAY [10] OF Decimal;
      i@52006502 : Integer;
      y@52006509 : Integer;
      dlgText@52006503 : Text;
      vendor@52006504 : Record 23;
      bestPrice@52006506 : Decimal;
      bestPriceI@52006507 : Integer;
    BEGIN
      GetPricesVector(vendors, prices);
      bestPrice :=  2147483647;
      dlgText += NoneLbl + ',';
      y += 1;
      FOR i := 1 TO 10 DO BEGIN
        IF (vendors[i] <> '') AND (prices[i] <> 0) THEN BEGIN
          y += 1;
          listedVendors[y] := vendors[i];
          vendor.GET(vendors[i]);
          dlgText += STRSUBSTNO('%1,', vendor.Name);

          IF "Winning Vendor No." <> '' THEN BEGIN
            IF "Winning Vendor No." = vendors[i] THEN
              bestPriceI := y;
          END ELSE IF (prices[i] < bestPrice) THEN BEGIN
            bestPrice := prices[i];
            bestPriceI := y;
          END;
        END;
      END;
      IF y > 0 THEN BEGIN
        dlgText := DELCHR(dlgText, '>', ',');
        i := STRMENU(dlgText, bestPriceI);
        IF i = 0 THEN
          EXIT;
        VALIDATE("Winning Vendor No.", listedVendors[i]);
      END;
    END;

    PROCEDURE GetPricesVector@52006506(VAR vendors@52006501 : ARRAY [10] OF Code[20];VAR prices@52006500 : ARRAY [10] OF Decimal);
    VAR
      quote@52006503 : Record 52112730;
      vendor@52006502 : Record 23;
    BEGIN
      quote.GET("Purch Req. Quote No.");
      quote.GetVendorsVector(vendors);
      prices[1] := "Vendor 1 Price";
      prices[2] := "Vendor 2 Price";
      prices[3] := "Vendor 3 Price";
      prices[4] := "Vendor 4 Price";
      prices[5] := "Vendor 5 Price";
      prices[6] := "Vendor 6 Price";
      prices[7] := "Vendor 7 Price";
      prices[8] := "Vendor 8 Price";
      prices[9] := "Vendor 9 Price";
      prices[10] := "Vendor 10 Price";
    END;

    PROCEDURE GetWinningPrice@52006501() : Decimal;
    VAR
      vendors@52006501 : ARRAY [10] OF Code[20];
      prices@52006500 : ARRAY [10] OF Decimal;
      i@52006502 : Integer;
    BEGIN
      IF "Winning Vendor No." = '' THEN
        EXIT;

      GetPricesVector(vendors, prices);
      FOR i := 1 TO 10 DO
        IF vendors[i] = "Winning Vendor No." THEN
          EXIT(prices[i]);
    END;

    LOCAL PROCEDURE ValidateItemQtyUpdate@52006510();
    BEGIN
      IF "From Request" OR ("Tracking ID" <> 0) THEN
        ERROR(ItemQtyUpdateErr);
    END;

    BEGIN
    END.
  }
}

