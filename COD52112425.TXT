OBJECT Codeunit 52112425 BR Check Post
{
  OBJECT-PROPERTIES
  {
    Date=09/06/15;
    Time=14:45:08;
    Version List=NAVBR8;
  }
  PROPERTIES
  {
    OnRun=BEGIN
          END;

  }
  CODE
  {
    VAR
      Text001@1102300006 : TextConst 'ENU=Quantity can''t be greater than quantity on-hand. Item %1.;PTB=Quantidade n�o pode ser maior que a quantidade em invent�rio. Produto %1.';
      Text002@1102300007 : TextConst 'ENU=The item %1 is not available on-hand.;PTB=O produto %1 n�o possui quantidade em invent�rio.';

    PROCEDURE CheckPostPurch@52006500(purchHeader@1102300000 : Record 38);
    VAR
      PurchPostValidation@52006500 : Codeunit 52112475;
    BEGIN
      PurchPostValidation.ThrowOnFirstError := TRUE;
      PurchPostValidation.Validate(purchHeader)
    END;

    PROCEDURE CheckPostSales@52006501(salesHeader@1102300000 : Record 36);
    VAR
      SalesPostValidation@52006500 : Codeunit 52112474;
    BEGIN
      SalesPostValidation.ThrowOnFirstError := TRUE;
      SalesPostValidation.Validate(salesHeader);
    END;

    PROCEDURE CheckPostService@52006502(VAR servHeader@1102300000 : TEMPORARY Record 5900;VAR servLine@52006512 : TEMPORARY Record 5902);
    VAR
      ServPostValidation@52006514 : Codeunit 52112476;
    BEGIN
      ServPostValidation.ThrowOnFirstError := TRUE;
      ServPostValidation.Validate(servHeader, servLine);
    END;

    PROCEDURE CheckGenJnlLine@52006503(genJnlLine@1102300000 : Record 81);
    BEGIN
    END;

    PROCEDURE CheckItemJournalLine@52006504(ItemJournalLine@1102300000 : Record 83);
    VAR
      Item@52006501 : Record 27;
    BEGIN
      WITH ItemJournalLine DO BEGIN
        IF Adjustment OR ("Item Charge No." <> '') THEN
          EXIT;

        IF (NOT Correction) AND (Quantity <> 0) THEN
          TESTFIELD("Unit of Measure Code");

        IF "Item Charge No." = '' THEN BEGIN
          Item.GET("Item No.");
          Item.TESTFIELD("Non-Stock Item", FALSE);
        END;
      END;
    END;

    PROCEDURE CheckFAJnlLine@52006506(faJnlLine@1102300000 : Record 5621);
    BEGIN
    END;

    PROCEDURE CheckJobJnlLine@52006507(jobJnlLine@1102300000 : Record 210);
    BEGIN
    END;

    PROCEDURE CheckResJnlLine@52006508(resJnlLine@1102300000 : Record 207);
    BEGIN
    END;

    BEGIN
    END.
  }
}

