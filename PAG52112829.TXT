OBJECT Page 52112829 Participants
{
  OBJECT-PROPERTIES
  {
    Date=30/04/13;
    Time=13:30:45;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Participants;
               PTB=Participantes];
    SourceTable=Table52112827;
    PageType=Worksheet;
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1102300000;1;Group  ;
                GroupType=Repeater }

    { 1102300001;2;Field  ;
                SourceExpr="Participant Code" }

    { 1102300003;2;Field  ;
                SourceExpr="Participant Name" }

    { 1102300005;2;Field  ;
                SourceExpr="County Code" }

    { 1102300007;2;Field  ;
                SourceExpr="C.N.P.J." }

    { 1102300009;2;Field  ;
                SourceExpr="C.P.F." }

    { 1102300011;2;Field  ;
                SourceExpr="No. Ident. Worker" }

    { 1102300013;2;Field  ;
                SourceExpr="Federation Unit Participant" }

    { 1102300015;2;Field  ;
                SourceExpr="Participant State Registration" }

    { 1102300017;2;Field  ;
                SourceExpr="State Reg. Fed. Unit Part." }

    { 1102300019;2;Field  ;
                SourceExpr="Municipality Code" }

    { 1102300021;2;Field  ;
                SourceExpr="Municipal Registration" }

    { 1102300023;2;Field  ;
                SourceExpr="No. Suframa Registration" }

    { 1102300028;2;Field  ;
                SourceExpr="Relationship Code" }

    { 1102300030;2;Field  ;
                SourceExpr="Relationship Initial Date" }

    { 1102300032;2;Field  ;
                SourceExpr="Relationship End Date" }

  }
  CODE
  {

    BEGIN
    END.
  }
}

