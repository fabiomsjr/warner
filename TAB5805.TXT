OBJECT Table 5805 Item Charge Assignment (Purch)
{
  OBJECT-PROPERTIES
  {
    Date=07/09/12;
    Time=12:00:00;
    Version List=NAVW17.00;
  }
  PROPERTIES
  {
    OnDelete=BEGIN
               TESTFIELD("Qty. Assigned",0);
             END;

    CaptionML=[ENU=Item Charge Assignment (Purch);
               PTB=Atribui��o Encargo Prod. (Compra)];
    PasteIsValid=No;
  }
  FIELDS
  {
    { 1   ;   ;Document Type       ;Option        ;CaptionML=[ENU=Document Type;
                                                              PTB=Tipo Documento];
                                                   OptionCaptionML=[ENU=Quote,Order,Invoice,Credit Memo,Blanket Order,Return Order;
                                                                    PTB=Cota��o,Pedido,Nota Fiscal,Nota Cr�dito,Ordem Cobertura,Ordem de Retorno];
                                                   OptionString=Quote,Order,Invoice,Credit Memo,Blanket Order,Return Order }
    { 2   ;   ;Document No.        ;Code20        ;TableRelation="Purchase Header".No. WHERE (Document Type=FIELD(Document Type));
                                                   CaptionML=[ENU=Document No.;
                                                              PTB=No. Documento] }
    { 3   ;   ;Document Line No.   ;Integer       ;TableRelation="Purchase Line"."Line No." WHERE (Document Type=FIELD(Document Type),
                                                                                                   Document No.=FIELD(Document No.));
                                                   CaptionML=[ENU=Document Line No.;
                                                              PTB=N� Linha Documento] }
    { 4   ;   ;Line No.            ;Integer       ;CaptionML=[ENU=Line No.;
                                                              PTB=N� Linha] }
    { 5   ;   ;Item Charge No.     ;Code20        ;TableRelation="Item Charge";
                                                   CaptionML=[ENU=Item Charge No.;
                                                              PTB=N� Encargo Prod.];
                                                   NotBlank=Yes }
    { 6   ;   ;Item No.            ;Code20        ;TableRelation=Item;
                                                   CaptionML=[ENU=Item No.;
                                                              PTB=No. Produto] }
    { 7   ;   ;Description         ;Text50        ;CaptionML=[ENU=Description;
                                                              PTB=Descri��o] }
    { 8   ;   ;Qty. to Assign      ;Decimal       ;OnValidate=BEGIN
                                                                TESTFIELD("Applies-to Doc. Line No.");
                                                                IF ("Qty. to Assign" <> 0) AND ("Applies-to Doc. Type" = "Document Type") THEN
                                                                  IF PurchLineInvoiced THEN
                                                                    ERROR(Text000,PurchLine.TABLECAPTION);
                                                                VALIDATE("Amount to Assign");
                                                              END;

                                                   CaptionML=[ENU=Qty. to Assign;
                                                              PTB=Qtd. a Atribuir];
                                                   DecimalPlaces=0:5;
                                                   BlankZero=Yes }
    { 9   ;   ;Qty. Assigned       ;Decimal       ;CaptionML=[ENU=Qty. Assigned;
                                                              PTB=Qtd. Atribu�da];
                                                   DecimalPlaces=0:5;
                                                   BlankZero=Yes;
                                                   Editable=No }
    { 10  ;   ;Unit Cost           ;Decimal       ;OnValidate=BEGIN
                                                                VALIDATE("Amount to Assign");
                                                              END;

                                                   CaptionML=[ENU=Unit Cost;
                                                              PTB=Custo Unit�rio];
                                                   AutoFormatType=2 }
    { 11  ;   ;Amount to Assign    ;Decimal       ;OnValidate=BEGIN
                                                                PurchLine.GET("Document Type","Document No.","Document Line No.");
                                                                IF NOT Currency.GET(PurchLine."Currency Code") THEN
                                                                  Currency.InitRoundingPrecision;
                                                                "Amount to Assign" := ROUND("Qty. to Assign" * "Unit Cost",Currency."Amount Rounding Precision");
                                                              END;

                                                   CaptionML=[ENU=Amount to Assign;
                                                              PTB=Valor a Atribuir];
                                                   AutoFormatType=1 }
    { 12  ;   ;Applies-to Doc. Type;Option        ;CaptionML=[ENU=Applies-to Doc. Type;
                                                              PTB=Aplicado por Tipo Doc.];
                                                   OptionCaptionML=[ENU=Quote,Order,Invoice,Credit Memo,Blanket Order,Return Order,Receipt,Transfer Receipt,Return Shipment,Sales Shipment,Return Receipt;
                                                                    PTB=Cota��o,Pedido,Nota Fiscal,Nota Cr�dito,Ped. Aberto,Devolu��o,Recep��o,Rec. Transf.,Env. Devolu��o,Remessa Venda,Rec. Devolu��o];
                                                   OptionString=Quote,Order,Invoice,Credit Memo,Blanket Order,Return Order,Receipt,Transfer Receipt,Return Shipment,Sales Shipment,Return Receipt }
    { 13  ;   ;Applies-to Doc. No. ;Code20        ;TableRelation=IF (Applies-to Doc. Type=CONST(Order)) "Purchase Header".No. WHERE (Document Type=CONST(Order))
                                                                 ELSE IF (Applies-to Doc. Type=CONST(Invoice)) "Purchase Header".No. WHERE (Document Type=CONST(Invoice))
                                                                 ELSE IF (Applies-to Doc. Type=CONST(Return Order)) "Purchase Header".No. WHERE (Document Type=CONST(Return Order))
                                                                 ELSE IF (Applies-to Doc. Type=CONST(Credit Memo)) "Purchase Header".No. WHERE (Document Type=CONST(Credit Memo))
                                                                 ELSE IF (Applies-to Doc. Type=CONST(Receipt)) "Purch. Rcpt. Header".No.
                                                                 ELSE IF (Applies-to Doc. Type=CONST(Return Shipment)) "Return Shipment Header".No.;
                                                   CaptionML=[ENU=Applies-to Doc. No.;
                                                              PTB=Aplicado por N� Doc.] }
    { 14  ;   ;Applies-to Doc. Line No.;Integer   ;TableRelation=IF (Applies-to Doc. Type=CONST(Order)) "Purchase Line"."Line No." WHERE (Document Type=CONST(Order),
                                                                                                                                          Document No.=FIELD(Applies-to Doc. No.))
                                                                                                                                          ELSE IF (Applies-to Doc. Type=CONST(Invoice)) "Purchase Line"."Line No." WHERE (Document Type=CONST(Invoice),
                                                                                                                                                                                                                          Document No.=FIELD(Applies-to Doc. No.))
                                                                                                                                                                                                                          ELSE IF (Applies-to Doc. Type=CONST(Return Order)) "Purchase Line"."Line No." WHERE (Document Type=CONST(Return Order),
                                                                                                                                                                                                                                                                                                               Document No.=FIELD(Applies-to Doc. No.))
                                                                                                                                                                                                                                                                                                               ELSE IF (Applies-to Doc. Type=CONST(Credit Memo)) "Purchase Line"."Line No." WHERE (Document Type=CONST(Credit Memo),
                                                                                                                                                                                                                                                                                                                                                                                                   Document No.=FIELD(Applies-to Doc. No.))
                                                                                                                                                                                                                                                                                                                                                                                                   ELSE IF (Applies-to Doc. Type=CONST(Receipt)) "Purch. Rcpt. Line"."Line No." WHERE (Document No.=FIELD(Applies-to Doc. No.))
                                                                                                                                                                                                                                                                                                                                                                                                   ELSE IF (Applies-to Doc. Type=CONST(Return Shipment)) "Return Shipment Line"."Line No." WHERE (Document No.=FIELD(Applies-to Doc. No.));
                                                   CaptionML=[ENU=Applies-to Doc. Line No.;
                                                              PTB=N� Linha Doc. Liq.-Por] }
    { 15  ;   ;Applies-to Doc. Line Amount;Decimal;CaptionML=[ENU=Applies-to Doc. Line Amount;
                                                              PTB=Valor Linha Doc. Liq.-Por];
                                                   AutoFormatType=1 }
  }
  KEYS
  {
    {    ;Document Type,Document No.,Document Line No.,Line No.;
                                                   SumIndexFields=Qty. to Assign,Qty. Assigned,Amount to Assign;
                                                   MaintainSIFTIndex=No;
                                                   Clustered=Yes }
    {    ;Applies-to Doc. Type,Applies-to Doc. No.,Applies-to Doc. Line No. }
    {    ;Applies-to Doc. Type                     }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      Text000@1000 : TextConst 'ENU=You cannot assign item charges to the %1 because it has been invoiced. Instead you can get the posted document line and then assign the item charge to that line.;PTB=Voc� n�o pode atribuir encargos produto ao %1 porque foi faturado. Em vez disso pode obter a linha do documento registrado e atribuir o encargo produto a essa linha.';
      PurchLine@1001 : Record 39;
      Currency@1002 : Record 4;

    PROCEDURE PurchLineInvoiced@1() : Boolean;
    BEGIN
      IF "Applies-to Doc. Type" <> "Document Type" THEN
        EXIT(FALSE);
      PurchLine.GET("Applies-to Doc. Type","Applies-to Doc. No.","Applies-to Doc. Line No.");
      EXIT(PurchLine.Quantity = PurchLine."Quantity Invoiced");
    END;

    BEGIN
    END.
  }
}

