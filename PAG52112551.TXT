OBJECT Page 52112551 Territory Tax Payable Binding
{
  OBJECT-PROPERTIES
  {
    Date=10/04/15;
    Time=11:45:29;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Payables;
               PTB=Guias Pagamento];
    SourceTable=Table52112531;
    DelayedInsert=Yes;
    PageType=ListPart;
    AutoSplitKey=Yes;
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1102300000;1;Group  ;
                GroupType=Repeater }

    { 52006507;2;Field    ;
                SourceExpr="Territory Code" }

    { 52006501;2;Field    ;
                SourceExpr="Requirement Code" }

    { 52006502;2;Field    ;
                SourceExpr="Due Date" }

    { 52006503;2;Field    ;
                SourceExpr=Amount }

    { 52006504;2;Field    ;
                SourceExpr="Revenue Code" }

    { 52006500;2;Field    ;
                SourceExpr="Reference Date" }

    { 52006505;2;Field    ;
                SourceExpr="Process Number" }

    { 52006506;2;Field    ;
                SourceExpr="Source Process Type" }

  }
  CODE
  {

    BEGIN
    END.
  }
}

