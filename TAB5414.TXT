OBJECT Table 5414 Prod. Order Comment Line
{
  OBJECT-PROPERTIES
  {
    Date=07/09/12;
    Time=12:00:00;
    Version List=NAVW17.00;
  }
  PROPERTIES
  {
    OnInsert=BEGIN
               IF Status = Status::Finished THEN
                 ERROR(Text000,Status,TABLECAPTION);
             END;

    OnModify=BEGIN
               IF Status = Status::Finished THEN
                 ERROR(Text000,Status,TABLECAPTION);
             END;

    OnDelete=BEGIN
               IF Status = Status::Finished THEN
                 ERROR(Text000,Status,TABLECAPTION);
             END;

    CaptionML=[ENU=Prod. Order Comment Line;
               PTB=Linha Coment�rio Ordem Prod.];
    LookupPageID=Page99000839;
    DrillDownPageID=Page99000839;
  }
  FIELDS
  {
    { 1   ;   ;Status              ;Option        ;CaptionML=[ENU=Status;
                                                              PTB=Status];
                                                   OptionCaptionML=[ENU=Simulated,Planned,Firm Planned,Released,Finished;
                                                                    PTB=Simulada,Planejada,Planejada Firme,Lan�ada,Terminada];
                                                   OptionString=Simulated,Planned,Firm Planned,Released,Finished }
    { 2   ;   ;Prod. Order No.     ;Code20        ;TableRelation="Production Order".No. WHERE (Status=FIELD(Status));
                                                   CaptionML=[ENU=Prod. Order No.;
                                                              PTB=N� Ordem Produ��o];
                                                   NotBlank=Yes }
    { 3   ;   ;Line No.            ;Integer       ;CaptionML=[ENU=Line No.;
                                                              PTB=N� Linha] }
    { 4   ;   ;Date                ;Date          ;CaptionML=[ENU=Date;
                                                              PTB=Data] }
    { 5   ;   ;Code                ;Code10        ;CaptionML=[ENU=Code;
                                                              PTB=C�digo] }
    { 6   ;   ;Comment             ;Text80        ;CaptionML=[ENU=Comment;
                                                              PTB=Coment�rio] }
  }
  KEYS
  {
    {    ;Status,Prod. Order No.,Line No.         ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      Text000@1000 : TextConst 'ENU=A %1 %2 cannot be inserted, modified, or deleted.;PTB=Um %1 %2 n�o pode ser inserido, modificado ou eliminado.';

    PROCEDURE SetupNewLine@1();
    VAR
      ProdOrderCommentLine@1000 : Record 5414;
    BEGIN
      ProdOrderCommentLine.SETRANGE(Status,Status);
      ProdOrderCommentLine.SETRANGE("Prod. Order No.","Prod. Order No.");
      ProdOrderCommentLine.SETRANGE(Date,WORKDATE);
      IF NOT ProdOrderCommentLine.FINDFIRST THEN
        Date := WORKDATE;
    END;

    BEGIN
    END.
  }
}

