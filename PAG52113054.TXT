OBJECT Page 52113054 CNAB Layout Select
{
  OBJECT-PROPERTIES
  {
    Date=07/08/13;
    Time=14:07:10;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Select the layout...;
               PTB=Selecione o Layout...];
    InsertAllowed=No;
    DeleteAllowed=No;
    ModifyAllowed=No;
    SourceTable=Table52113023;
    PageType=List;
    OnOpenPage=BEGIN
                 TypeVisible := NOT CurrPage.LOOKUPMODE;
               END;

  }
  CONTROLS
  {
    { 35000000;0;Container;
                ContainerType=ContentArea }

    { 35000001;1;Group    ;
                Name=Group;
                GroupType=Repeater }

    { 35000002;2;Field    ;
                SourceExpr=Code }

    { 35000003;2;Field    ;
                SourceExpr=Description }

  }
  CODE
  {
    VAR
      TypeVisible@52006500 : Boolean;

    BEGIN
    END.
  }
}

