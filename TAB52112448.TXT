OBJECT Table 52112448 Ledger Entry Tax
{
  OBJECT-PROPERTIES
  {
    Date=22/12/14;
    Time=16:59:03;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Ledger Entry Tax;
               PTB=Imposto Mov.];
    LookupPageID=Page52112448;
    DrillDownPageID=Page52112448;
  }
  FIELDS
  {
    { 10  ;   ;Entry No.           ;Integer       ;CaptionML=[ENU=Entry No.;
                                                              PTB=N� Mov.] }
    { 15  ;   ;Customer/Vendor     ;Option        ;CaptionML=[ENU=Customer/Vendor;
                                                              PTB=Cliente/Fornecedor];
                                                   OptionCaptionML=[ENU=Customer,Vendor;
                                                                    PTB=Cliente,Fornecedor];
                                                   OptionString=Cust,Vend }
    { 20  ;   ;Ledger Entry No.    ;Integer       ;TableRelation=IF (Customer/Vendor=CONST(Cust)) "Cust. Ledger Entry"
                                                                 ELSE IF (Customer/Vendor=CONST(Vend)) "Vendor Ledger Entry" }
    { 30  ;   ;Tax Identification  ;Option        ;CaptionML=[ENU=Tax Identification;
                                                              PTB=Classifica��o Imposto];
                                                   OptionCaptionML=[ENU=" ,IPI,ICMS,PIS,COFINS,II,IRRF,INSS,ISS,CSL Ret.,PIS Ret.,COFINS Ret.,ISS Ret.,INSS Ret.,Dif. Aliq. ICMS,ST,PCC,CIDE,Funrural,Simples";
                                                                    PTB=" ,IPI,ICMS,PIS,COFINS,II,IRRF,INSS,ISS,CSL Ret.,PIS Ret.,COFINS Ret.,ISS Ret.,INSS Ret.,Dif. Aliq. ICMS,ST,PCC,CIDE,Funrural,Simples"];
                                                   OptionString=[ ,IPI,ICMS,PIS,COFINS,II,IRRF,INSS,ISS,CSL Ret.,PIS Ret.,COFINS Ret.,ISS Ret.,INSS Ret.,Dif. Aliq. ICMS,ST,PCC,CIDE,Funrural,Simples] }
    { 35  ;   ;Tax Amount          ;Decimal       ;CaptionML=[ENU=Amount;
                                                              PTB=Valor] }
    { 40  ;   ;Customer/Vendor No. ;Code20        ;TableRelation=IF (Customer/Vendor=CONST(Cust)) Customer
                                                                 ELSE IF (Customer/Vendor=CONST(Vend)) Vendor;
                                                   CaptionML=[ENU=Customer/Vendor No.;
                                                              PTB=N� Cliente/Fornecedor] }
    { 60  ;   ;VAT Entry No.       ;Integer       ;TableRelation="VAT Entry";
                                                   CaptionML=[ENU=VAT Entry No.;
                                                              PTB=N� Mov. Imposto] }
    { 65  ;   ;Date                ;Date          ;CaptionML=[ENU=Date;
                                                              PTB=Data] }
    { 70  ;   ;Tax Base Amount     ;Decimal       ;CaptionML=[ENU=Payment Base Amount;
                                                              PTB=Base Pagamento];
                                                   BlankNumbers=BlankZero }
    { 80  ;   ;Tax %               ;Decimal       ;CaptionML=[ENU=Tax %;
                                                              PTB=% Imposto];
                                                   BlankNumbers=BlankZero }
    { 90  ;   ;Month Base Amount   ;Decimal       ;CaptionML=[ENU=Month Previous Base Amount;
                                                              PTB=Base Pr�via do M�s];
                                                   BlankNumbers=BlankZero }
    { 100 ;   ;Month Tax Amount    ;Decimal       ;CaptionML=[ENU=Month Previous Tax Amount;
                                                              PTB=Valor Pr�vio do M�s];
                                                   BlankNumbers=BlankZero }
    { 1010;   ;Payable Account Type;Option        ;OnValidate=BEGIN
                                                                "Payable Account No." := '';
                                                              END;

                                                   CaptionML=[ENU=Payable Account Type;
                                                              PTB=Tipo Conta a Pagar];
                                                   OptionCaptionML=[ENU=" ,G/L Account,Customer,Vendor,Bank Account,Fixed Asset,IC Partner";
                                                                    PTB=" ,Conta Cont�bil,Cliente,Fornecedor,Conta Banc�ria,Ativo Fixo,Parceiro IC"];
                                                   OptionString=[ ,G/L Account,Customer,Vendor,Bank Account,Fixed Asset,IC Partner] }
    { 1012;   ;Payable Account No. ;Code20        ;TableRelation=IF (Payable Account Type=CONST(G/L Account)) "G/L Account"
                                                                 ELSE IF (Payable Account Type=CONST(Customer)) Customer
                                                                 ELSE IF (Payable Account Type=CONST(Vendor)) Vendor
                                                                 ELSE IF (Payable Account Type=CONST(Bank Account)) "Bank Account"
                                                                 ELSE IF (Payable Account Type=CONST(Fixed Asset)) "Fixed Asset"
                                                                 ELSE IF (Payable Account Type=CONST(IC Partner)) "IC Partner";
                                                   CaptionML=[ENU=Payable Account No.;
                                                              PTB=N� Conta a Pagar] }
    { 1020;   ;Expense Account Type;Option        ;OnValidate=BEGIN
                                                                "Expense Account No." := '';
                                                              END;

                                                   CaptionML=[ENU=Expense Account Type;
                                                              PTB=Tipo Conta Despesa];
                                                   OptionCaptionML=[ENU=" ,G/L Account,Customer,Vendor,Bank Account,Fixed Asset,IC Partner";
                                                                    PTB=" ,Conta Cont�bil,Cliente,Fornecedor,Conta Banc�ria,Ativo Fixo,Parceiro IC"];
                                                   OptionString=[ ,G/L Account,Customer,Vendor,Bank Account,Fixed Asset,IC Partner] }
    { 1022;   ;Expense Account No. ;Code20        ;TableRelation=IF (Expense Account Type=CONST(G/L Account)) "G/L Account"
                                                                 ELSE IF (Expense Account Type=CONST(Customer)) Customer
                                                                 ELSE IF (Expense Account Type=CONST(Vendor)) Vendor
                                                                 ELSE IF (Expense Account Type=CONST(Bank Account)) "Bank Account"
                                                                 ELSE IF (Expense Account Type=CONST(Fixed Asset)) "Fixed Asset"
                                                                 ELSE IF (Expense Account Type=CONST(IC Partner)) "IC Partner";
                                                   CaptionML=[ENU=Expense Account No.;
                                                              PTB=N� Conta Despesa] }
    { 1030;   ;Tax Credit Account Type;Option     ;OnValidate=BEGIN
                                                                "Tax Credit Account No." := '';
                                                              END;

                                                   CaptionML=[ENU=Tax Credit Account Type;
                                                              PTB=Tipo Conta a Recuperar];
                                                   OptionCaptionML=[ENU=" ,G/L Account,Customer,Vendor,Bank Account,Fixed Asset,IC Partner";
                                                                    PTB=" ,Conta Cont�bil,Cliente,Fornecedor,Conta Banc�ria,Ativo Fixo,Parceiro IC"];
                                                   OptionString=[ ,G/L Account,Customer,Vendor,Bank Account,Fixed Asset,IC Partner] }
    { 1032;   ;Tax Credit Account No.;Code20      ;TableRelation=IF (Tax Credit Account Type=CONST(G/L Account)) "G/L Account"
                                                                 ELSE IF (Tax Credit Account Type=CONST(Customer)) Customer
                                                                 ELSE IF (Tax Credit Account Type=CONST(Vendor)) Vendor
                                                                 ELSE IF (Tax Credit Account Type=CONST(Bank Account)) "Bank Account"
                                                                 ELSE IF (Tax Credit Account Type=CONST(Fixed Asset)) "Fixed Asset"
                                                                 ELSE IF (Tax Credit Account Type=CONST(IC Partner)) "IC Partner";
                                                   CaptionML=[ENU=Tax Credit Account No.;
                                                              PTB=N� Conta a Recuperar] }
    { 35000340;;Payment - Financial;Option        ;FieldClass=FlowField;
                                                   CalcFormula=Lookup("VAT Entry"."Payment - Financial" WHERE (Entry No.=FIELD(VAT Entry No.)));
                                                   CaptionML=[ENU=Financial;
                                                              PTB=Financeiro];
                                                   OptionCaptionML=[ENU=" ,Add to Total,Subtract from Total";
                                                                    PTB=" ,Somar ao Total,Subtrair do Total"];
                                                   OptionString=[ ,Add,Subtract];
                                                   Editable=No }
    { 35000350;;Payment - Payable Tax;Boolean     ;FieldClass=FlowField;
                                                   CalcFormula=Lookup("VAT Entry"."Payment - Payable Tax" WHERE (Entry No.=FIELD(VAT Entry No.)));
                                                   CaptionML=[ENU=Payable Tax;
                                                              PTB=Imposto a Pagar];
                                                   Editable=No }
    { 35000360;;Payment - Expense  ;Boolean       ;FieldClass=FlowField;
                                                   CalcFormula=Lookup("VAT Entry"."Payment - Expense" WHERE (Entry No.=FIELD(VAT Entry No.)));
                                                   CaptionML=[ENU=Expense;
                                                              PTB=Despesa];
                                                   Editable=No }
    { 35000370;;Payment - Tax Credit;Boolean      ;FieldClass=FlowField;
                                                   CalcFormula=Lookup("VAT Entry"."Payment - Tax Credit" WHERE (Entry No.=FIELD(VAT Entry No.)));
                                                   CaptionML=[ENU=Tax Credit;
                                                              PTB=Imposto a Recuperar];
                                                   Editable=No }
  }
  KEYS
  {
    {    ;Entry No.                               ;Clustered=Yes }
    {    ;Customer/Vendor,Ledger Entry No.         }
    {    ;Date,Customer/Vendor,Customer/Vendor No. }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

