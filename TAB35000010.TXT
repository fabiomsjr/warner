OBJECT Table 35000010 NCM Codes2
{
  OBJECT-PROPERTIES
  {
    Date=16/04/12;
    Time=16:48:47;
    Version List=FNX048.00;
  }
  PROPERTIES
  {
    DataPerCompany=No;
    CaptionML=PTB=C�digos NCM;
    LookupPageID=Page35000056;
    DrillDownPageID=Page35000056;
  }
  FIELDS
  {
    { 1   ;   ;Code                ;Code20        ;CaptionML=[ENU=Code;
                                                              PTB=C�digo] }
    { 2   ;   ;Description         ;Text100       ;CaptionML=[ENU=Description;
                                                              PTB=Descri��o] }
    { 35000600;;Exception NCM Code ;Code10        ;TableRelation="Exception Code NCM.2";
                                                   CaptionML=PTB=C�d. Excess�o NCM;
                                                   Description=EFD5.0209 }
    { 35000700;;Gender             ;Code2         ;CaptionML=PTB=G�nero;
                                                   Description=NFe1.00 }
  }
  KEYS
  {
    {    ;Code                                    ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    {
      NAVBR5.01.0109

      No.   dd.mm.yy   Developer   Company   DocNo.   Description
      -----------------------------------------------------------------------------------------------------------------
      00    01.12.06   MK          MICROSOFT LOC001   The reference Documentation file LOCALIZATIONBR.DOC
    }
    END.
  }
}

