OBJECT Page 52113076 Approval Flow List
{
  OBJECT-PROPERTIES
  {
    Date=04/03/15;
    Time=08:45:02;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Approval Flow List;
               PTB=Fluxos Aprova��o];
    SourceTable=Table52113073;
    PageType=List;
    CardPageID=Approval Flow Card;
    OnOpenPage=BEGIN
                 FILTERGROUP(2);
                 SETRANGE("Sender ID", USERID);
                 FILTERGROUP(0);
               END;

    ActionList=ACTIONS
    {
      { 52006513;    ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 52006511;1   ;Action    ;
                      CaptionML=[ENU=Open Document;
                                 PTB=Abrir Documento];
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=Document;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 ActionOpenDocument;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 52006500;0;Container;
                ContainerType=ContentArea }

    { 52006501;1;Group    ;
                Name=Group;
                GroupType=Repeater }

    { 52006502;2;Field    ;
                SourceExpr="No." }

    { 52006503;2;Field    ;
                SourceExpr=Description }

    { 52006504;2;Field    ;
                SourceExpr=Status }

    { 52006505;2;Field    ;
                SourceExpr="Sender ID" }

    { 52006506;2;Field    ;
                SourceExpr="Date-Time Sent for Approval" }

    { 52006507;2;Field    ;
                SourceExpr=Amount }

    { 52006509;2;Field    ;
                SourceExpr="Minimum No. of Approvals" }

    { 52006510;2;Field    ;
                SourceExpr=Comments }

  }
  CODE
  {

    LOCAL PROCEDURE ActionOpenDocument@52006502();
    BEGIN
      OpenDocument;
    END;

    BEGIN
    END.
  }
}

