OBJECT Table 52112628 NFe Table Filter
{
  OBJECT-PROPERTIES
  {
    Date=29/07/13;
    Time=09:54:21;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    DataPerCompany=No;
    CaptionML=[ENU=NF-e Table Filter;
               PTB=NF-e - Filtro Tabela];
    LookupPageID=Page52112630;
    DrillDownPageID=Page52112630;
  }
  FIELDS
  {
    { 10  ;   ;NavNFe Code         ;Code20        ;TableRelation="NFe Layout";
                                                   CaptionML=PTB=C�digo NavNFe }
    { 20  ;   ;Table Code          ;Code20        ;TableRelation="NFe Table".Code WHERE (NavNFe Code=FIELD(NavNFe Code));
                                                   CaptionML=PTB=C�d. Tabela }
    { 25  ;   ;Table No.           ;Integer       ;FieldClass=FlowField;
                                                   CalcFormula=Lookup("NFe Table"."Table No." WHERE (NavNFe Code=FIELD(NavNFe Code),
                                                                                                     Code=FIELD(Table Code)));
                                                   CaptionML=PTB=N� Tabela;
                                                   Editable=No }
    { 30  ;   ;Line No.            ;Integer       ;CaptionML=PTB=N� Linha }
    { 40  ;   ;Field No.           ;Integer       ;TableRelation=Field.No. WHERE (TableNo=FIELD(Table No.));
                                                   CaptionML=PTB=N� Campo }
    { 45  ;   ;Field Name          ;Text250       ;FieldClass=FlowField;
                                                   CalcFormula=Lookup(Field."Field Caption" WHERE (TableNo=FIELD(Table No.),
                                                                                                   No.=FIELD(Field No.)));
                                                   CaptionML=PTB=Nome Campo;
                                                   Editable=No }
    { 50  ;   ;Filter Type         ;Option        ;OnValidate=BEGIN
                                                                "Filter Value" := '';
                                                              END;

                                                   CaptionML=PTB=Tipo Filtro;
                                                   OptionString=[ ,N� Documento,Filtro,Campo Tab. Pai] }
    { 60  ;   ;Filter Value        ;Text250       ;OnValidate=VAR
                                                                fieldNo@35000001 : Integer;
                                                                field@35000000 : Record 2000000041;
                                                                nfeTable@35000002 : Record 52112627;
                                                              BEGIN
                                                                CASE "Filter Type" OF
                                                                  "Filter Type"::"N� Documento": ERROR(Text001, FIELDCAPTION("Filter Type"), "Filter Type");
                                                                  "Filter Type"::"Campo Tab. Pai": BEGIN
                                                                    EVALUATE(fieldNo, "Filter Value");

                                                                    TESTFIELD("Table Code");
                                                                    nfeTable.GET("NavNFe Code", "Table Code");
                                                                    nfeTable.TESTFIELD("Parent Table Code");
                                                                    nfeTable.GET("NavNFe Code", nfeTable."Parent Table Code");
                                                                    field.GET(nfeTable."Table No.", fieldNo);
                                                                  END;
                                                                END;
                                                              END;

                                                   OnLookup=VAR
                                                              table@35000005 : Record 52112627;
                                                              field@35000004 : Record 2000000041;
                                                              fieldForm@35000003 : Page 52112638;
                                                              customLookupForm@35000002 : Page 52112634;
                                                              recRef@35000001 : RecordRef;
                                                              fieldRef@35000000 : FieldRef;
                                                            BEGIN
                                                              CASE "Filter Type" OF
                                                                "Filter Type"::"Campo Tab. Pai":
                                                                  BEGIN
                                                                    table.GET("NavNFe Code", "Table Code");
                                                                    table.TESTFIELD("Parent Table Code");
                                                                    table.GET("NavNFe Code", table."Parent Table Code");
                                                                    table.TESTFIELD("Table No.");
                                                                    field.SETRANGE(TableNo, table."Table No.");
                                                                    fieldForm.SETTABLEVIEW(field);
                                                                    fieldForm.LOOKUPMODE := TRUE;
                                                                    IF fieldForm.RUNMODAL = ACTION::LookupOK THEN
                                                                      BEGIN
                                                                        fieldForm.GETRECORD(field);
                                                                        "Filter Value" := FORMAT(field."No.");
                                                                        "Parent Table Field Name" := field."Field Caption";
                                                                      END;
                                                                  END;
                                                                "Filter Type"::Filtro:
                                                                  BEGIN
                                                                    table.GET("NavNFe Code", "Table Code");
                                                                    table.TESTFIELD("Table No.");
                                                                    TESTFIELD("Field No.");
                                                                    recRef.OPEN(table."Table No.");
                                                                    fieldRef := recRef.FIELD("Field No.");
                                                                    customLookupForm.SetValues(fieldRef.OPTIONCAPTION);
                                                                    IF customLookupForm.RUNMODAL = ACTION::LookupOK THEN
                                                                      BEGIN
                                                                        "Filter Value" += customLookupForm.GetValue;
                                                                      END;
                                                                  END;
                                                              END;
                                                            END;

                                                   CaptionML=PTB=Valor Filtro }
    { 70  ;   ;Parent Table Field Name;Text50     ;CaptionML=PTB=Nome Campo Tab. Pai }
  }
  KEYS
  {
    {    ;NavNFe Code,Table Code,Line No.         ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      Text001@35000000 : TextConst 'PTB=Valor deve ser branco para %1 %2.';

    BEGIN
    {
      --- THBR177 ---
      rafaelr,050811,NF-E
    }
    END.
  }
}

