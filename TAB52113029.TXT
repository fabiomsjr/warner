OBJECT Table 52113029 CNAB Field Total Buffer
{
  OBJECT-PROPERTIES
  {
    Date=20/08/13;
    Time=13:57:42;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
  }
  FIELDS
  {
    { 10  ;   ;Field Code          ;Code30        ;CaptionML=[ENU=Field Code;
                                                              PTB=C�d. Campo] }
    { 20  ;   ;Total               ;Decimal       ;CaptionML=[ENU=Total;
                                                              PTB=Total] }
  }
  KEYS
  {
    {    ;Field Code                              ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

