OBJECT Table 52112431 CST Impostos
{
  OBJECT-PROPERTIES
  {
    Date=03/03/16;
    Time=08:24:14;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    DataPerCompany=No;
    CaptionML=[ENU=Taxes CST;
               PTB=CST Impostos];
    LookupPageID=Page52112431;
    DrillDownPageID=Page52112431;
  }
  FIELDS
  {
    { 1   ;   ;Tax Type            ;Option        ;CaptionML=[ENU=Tipo;
                                                              PTB=Tipo Imposto];
                                                   OptionCaptionML=[ENU=,IPI,PIS,COFINS;
                                                                    PTB=,IPI,PIS,COFINS];
                                                   OptionString=,IPI,PIS,COFINS }
    { 2   ;   ;Code                ;Code10        ;CaptionML=[ENU=Code;
                                                              PTB=C�digo];
                                                   NotBlank=Yes }
    { 3   ;   ;Description         ;Text250       ;CaptionML=[ENU=Description;
                                                              PTB=Descri��o] }
    { 4   ;   ;Type                ;Option        ;CaptionML=PTB=Tipo;
                                                   OptionCaptionML=[ENU=Inbound/Outbound,Inbound,Outbound;
                                                                    PTB=Entrada/Sa�da,Entrada,Sa�da];
                                                   OptionString=Inbound/Outbound,Inbound,Outbound }
    { 100 ;   ;Tax Credit          ;Boolean       ;OnValidate=BEGIN
                                                                IF NOT "Tax Credit" THEN BEGIN
                                                                  "Credit Over Taxed Income" := FALSE;
                                                                  "Credit Over Non-Taxed Income" := FALSE;
                                                                  "Credit Over Exportation Income" := FALSE;
                                                                END;
                                                              END;

                                                   CaptionML=[ENU=Tax Credit;
                                                              PTB=Cr�dito Imposto] }
    { 101 ;   ;Credit Over Taxed Income;Boolean   ;OnValidate=BEGIN
                                                                UpdateTaxCredit;
                                                              END;

                                                   CaptionML=[ENU=Credit Over Taxed Income;
                                                              PTB=Cr�dito Vinculado a Receita Tributada no Mercado Interno] }
    { 102 ;   ;Credit Over Non-Taxed Income;Boolean;
                                                   OnValidate=BEGIN
                                                                UpdateTaxCredit;
                                                              END;

                                                   CaptionML=[ENU=Credit Over Non-Taxed Income;
                                                              PTB=Cr�dito Vinculado a Receita N�o Tributada no Mercado Interno] }
    { 103 ;   ;Credit Over Exportation Income;Boolean;
                                                   OnValidate=BEGIN
                                                                UpdateTaxCredit;
                                                              END;

                                                   CaptionML=[ENU=Credit Over Exportation Income;
                                                              PTB=Cr�dito Vinculado a Receita Exporta��o] }
    { 110 ;   ;Income Nature Mandatory;Boolean    ;CaptionML=[ENU=Income Nature Mandatory;
                                                              PTB=Natureza Receita Obrigat�ria] }
  }
  KEYS
  {
    {    ;Tax Type,Code                           ;Clustered=Yes }
  }
  FIELDGROUPS
  {
    { 1   ;DropDown            ;Code,Description                         }
  }
  CODE
  {

    PROCEDURE UpdateTaxCredit@52006501();
    BEGIN
      IF "Credit Over Taxed Income" OR "Credit Over Non-Taxed Income" OR "Credit Over Exportation Income" THEN
        "Tax Credit" := TRUE;
    END;

    BEGIN
    END.
  }
}

