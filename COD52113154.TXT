OBJECT Codeunit 52113154 Serv-Documents Mgt. C-Hook
{
  OBJECT-PROPERTIES
  {
    Date=28/10/14;
    Time=09:39:43;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    OnRun=BEGIN
          END;

  }
  CODE
  {
    VAR
      ServPostingJnlsMgt@52006501 : Codeunit 5987;
      ServAmountsMgt@52006500 : Codeunit 5986;

    PROCEDURE PrepareDocument@8(VAR servHeader@52006501 : TEMPORARY Record 5900;VAR servLine@52006500 : TEMPORARY Record 5902;VAR taxAmtLine2@52006503 : TEMPORARY Record 52112449;VAR servPostingJnlsMgt2@52006505 : Codeunit 5987;VAR servAmountsMgt2@52006504 : Codeunit 5986);
    BEGIN
      ServPostingJnlsMgt := servPostingJnlsMgt2;
      ServAmountsMgt := servAmountsMgt2;
    END;

    PROCEDURE SetPostingOptions@47(passedShip@1000 : Boolean;passedConsume@1001 : Boolean;passedInvoice@1002 : Boolean);
    BEGIN
    END;

    PROCEDURE SetGenJnlLineDocNos@15(VAR servHeader@52006500 : TEMPORARY Record 5900;VAR docType@1000 : Integer;VAR docNo@1001 : Code[20];VAR extDocNo@1002 : Code[20]);
    VAR
      fiscalDocType@52006501 : Record 52112425;
      noSeriesMgt@52006502 : Codeunit 396;
    BEGIN
    END;

    PROCEDURE ShouldCheckAdjustedLines@18(servLine@52006500 : Record 5902) : Boolean;
    VAR
      postingPreviewProcess@52006501 : Codeunit 52112437;
    BEGIN
      EXIT(TRUE);
    END;

    PROCEDURE BeforeInsertInvoiceHeader@52006504(servHeader@52006501 : Record 5900;VAR servInvHeader@52006500 : Record 5992);
    BEGIN
    END;

    PROCEDURE BeforeInsertCrMemoHeader@52006503(servHeader@52006501 : Record 5900;VAR servCrMemoHeader@52006500 : Record 5994);
    BEGIN
    END;

    PROCEDURE AfterPrepareShipmentHeader@11(servHeader@52006500 : Record 5900;VAR servShipHeader@52006501 : Record 5990);
    BEGIN
    END;

    PROCEDURE AfterPrepareInvoiceHeader@12(servHeader@52006501 : Record 5900;VAR servInvHeader@52006500 : Record 5992);
    BEGIN
    END;

    PROCEDURE AfterPrepareCrMemoHeader@14(servHeader@52006501 : Record 5900;VAR servCrMemoHeader@52006500 : Record 5994);
    BEGIN
    END;

    PROCEDURE BeforeInsertServInvLine@52006500(servHeader@52006502 : Record 5900;servLine@52006503 : Record 5902;VAR servInvHeader@52006500 : Record 5992;VAR servInvLine@52006501 : Record 5993);
    BEGIN
    END;

    PROCEDURE BeforeInsertServCrMemoLine@52006508(servHeader@52006503 : Record 5900;servLine@52006502 : Record 5902;VAR salesCrMemoHeader@52006500 : Record 5994;VAR salesCrMemoLine@52006501 : Record 5995);
    BEGIN
    END;

    PROCEDURE BeforeInsertServShptLine@52006512(servHeader@52006503 : Record 5900;servLine@52006502 : Record 5902;servShptHeader@52006501 : TEMPORARY Record 5990;VAR servShptLine@52006500 : TEMPORARY Record 5991);
    BEGIN
    END;

    PROCEDURE AfterFinalize@9(VAR servHeader@1000 : Record 5900;servInvHeader@52006501 : Record 5992;servCrMemoHeader@52006500 : Record 5994);
    BEGIN
    END;

    PROCEDURE AfterFinalizeHeader@52006502(VAR servHeader@1000 : Record 5900);
    BEGIN
    END;

    BEGIN
    END.
  }
}

