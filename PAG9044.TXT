OBJECT Page 9044 Shop Super. basic Activities
{
  OBJECT-PROPERTIES
  {
    Date=07/09/12;
    Time=12:00:00;
    Version List=NAVW17.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Activities;
               PTB=Atividades];
    SourceTable=Table9056;
    PageType=CardPart;
    OnOpenPage=BEGIN
                 RESET;
                 IF NOT GET THEN BEGIN
                   INIT;
                   INSERT;
                 END;
               END;

  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 11  ;1   ;Group     ;
                CaptionML=[ENU=Production Orders;
                           PTB=Pedidos Produ��o];
                GroupType=CueGroup;
                ActionList=ACTIONS
                {
                  { 9       ;0   ;Action    ;
                                  CaptionML=[ENU=New Production Order;
                                             PTB=Novo Pedido Produ��o];
                                  RunObject=Page 99000813;
                                  RunPageMode=Create }
                  { 8       ;0   ;Action    ;
                                  CaptionML=[ENU=View Production Order - Shortage List;
                                             PTB=Ver Pedido Produ��o - Lista Curta];
                                  RunObject=Report 99000788 }
                  { 10      ;0   ;Action    ;
                                  CaptionML=[ENU=Change Production Order Status;
                                             PTB=Alterar Status Pedido Produ��o];
                                  RunObject=Page 99000914;
                                  Image=ChangeStatus }
                }
                 }

    { 1   ;2   ;Field     ;
                SourceExpr="Planned Prod. Orders - All";
                DrillDownPageID=Planned Production Orders }

    { 4   ;2   ;Field     ;
                SourceExpr="Firm Plan. Prod. Orders - All";
                DrillDownPageID=Firm Planned Prod. Orders }

    { 6   ;2   ;Field     ;
                SourceExpr="Released Prod. Orders - All";
                DrillDownPageID=Released Production Orders }

    { 12  ;1   ;Group     ;
                CaptionML=[ENU=Operations;
                           PTB=Opera��es];
                GroupType=CueGroup;
                ActionList=ACTIONS
                {
                  { 18      ;0   ;Action    ;
                                  CaptionML=[ENU=Edit Order Planning;
                                             PTB=Editar Pedido Planejamento];
                                  RunObject=Page 5522 }
                  { 19      ;0   ;Action    ;
                                  CaptionML=[ENU=Edit Consumption Journal;
                                             PTB=Editar Di�rio Consumo];
                                  RunObject=Page 99000846 }
                  { 20      ;0   ;Action    ;
                                  CaptionML=[ENU=Edit Output Journal;
                                             PTB=Editar Di�rio Sa�da];
                                  RunObject=Page 99000823 }
                }
                 }

    { 14  ;2   ;Field     ;
                SourceExpr="Prod. Orders Routings-in Queue";
                DrillDownPageID=Prod. Order Routing }

    { 16  ;2   ;Field     ;
                SourceExpr="Prod. Orders Routings-in Prog.";
                DrillDownPageID=Prod. Order Routing }

    { 21  ;1   ;Group     ;
                CaptionML=[ENU=Warehouse Documents;
                           PTB=Documentos Armaz�m];
                GroupType=CueGroup }

    { 24  ;2   ;Field     ;
                SourceExpr="Invt. Picks to Production";
                DrillDownPageID=Inventory Picks }

    { 22  ;2   ;Field     ;
                SourceExpr="Invt. Put-aways from Prod.";
                DrillDownPageID=Inventory Put-aways }

  }
  CODE
  {

    BEGIN
    END.
  }
}

