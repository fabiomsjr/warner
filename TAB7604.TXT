OBJECT Table 7604 Where Used Base Calendar
{
  OBJECT-PROPERTIES
  {
    Date=05/11/08;
    Time=12:00:00;
    Version List=NAVW16.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Where Used Base Calendar;
               PTB=Onde � Usado Calend�rio Base];
  }
  FIELDS
  {
    { 1   ;   ;Source Type         ;Option        ;CaptionML=[ENU=Source Type;
                                                              PTB=Tipo Origem];
                                                   OptionCaptionML=[ENU=Company,Customer,Vendor,Location,Shipping Agent,Service;
                                                                    PTB=Empresa,Cliente,Fornecedor,Localiza��o,Transportador,Servi�o];
                                                   OptionString=Company,Customer,Vendor,Location,Shipping Agent,Service;
                                                   Editable=No }
    { 2   ;   ;Source Code         ;Code20        ;CaptionML=[ENU=Source Code;
                                                              PTB=Cod. Origem];
                                                   Editable=No }
    { 3   ;   ;Additional Source Code;Code20      ;CaptionML=[ENU=Additional Source Code;
                                                              PTB=Cod. Origem Adicional];
                                                   Editable=No }
    { 4   ;   ;Base Calendar Code  ;Code10        ;TableRelation="Base Calendar";
                                                   CaptionML=[ENU=Base Calendar Code;
                                                              PTB=Cod. Calend�rio Base] }
    { 5   ;   ;Source Name         ;Text50        ;CaptionML=[ENU=Source Name;
                                                              PTB=Nome Origem];
                                                   Editable=No }
    { 6   ;   ;Customized Changes Exist;Boolean   ;CaptionML=[ENU=Customized Changes Exist;
                                                              PTB=Existem Altera��es Person.];
                                                   Editable=No }
  }
  KEYS
  {
    {    ;Base Calendar Code,Source Type,Source Code,Source Name;
                                                   Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

