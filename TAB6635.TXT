OBJECT Table 6635 Return Reason
{
  OBJECT-PROPERTIES
  {
    Date=07/09/12;
    Time=12:00:00;
    Version List=NAVW18.00.00.41779;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Return Reason;
               PTB=Raz�o Devolu��o];
    LookupPageID=Page6635;
    DrillDownPageID=Page6635;
  }
  FIELDS
  {
    { 1   ;   ;Code                ;Code10        ;CaptionML=[ENU=Code;
                                                              PTB=C�digo];
                                                   NotBlank=Yes }
    { 2   ;   ;Description         ;Text50        ;CaptionML=[ENU=Description;
                                                              PTB=Descri��o] }
    { 3   ;   ;Default Location Code;Code10       ;TableRelation=Location WHERE (Use As In-Transit=CONST(No));
                                                   CaptionML=[ENU=Default Location Code;
                                                              PTB=Cod. Localiza��o Padr�o] }
    { 4   ;   ;Inventory Value Zero;Boolean       ;CaptionML=[ENU=Inventory Value Zero;
                                                              PTB=Invent�rio Valor Zero] }
  }
  KEYS
  {
    {    ;Code                                    ;Clustered=Yes }
  }
  FIELDGROUPS
  {
    { 1   ;DropDown            ;Code,Description,Default Location Code,Inventory Value Zero }
  }
  CODE
  {

    BEGIN
    END.
  }
}

