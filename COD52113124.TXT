OBJECT Codeunit 52113124 ApplicationManagement C-Hook
{
  OBJECT-PROPERTIES
  {
    Date=19/03/14;
    Time=10:14:27;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    OnRun=BEGIN
          END;

  }
  CODE
  {

    PROCEDURE CompanyOpen@30();
    BEGIN
    END;

    PROCEDURE GetSystemIndicator@51(VAR Text@1000 : Text[250];VAR Style@1001 : 'Standard,Accent1,Accent2,Accent3,Accent4,Accent5,Accent6,Accent7,Accent8,Accent9');
    BEGIN
    END;

    PROCEDURE CompanyClose@31();
    BEGIN
    END;

    PROCEDURE GetDatabaseTableTriggerSetup@25(TableId@1000 : Integer;VAR OnDatabaseInsert@1001 : Boolean;VAR OnDatabaseModify@1002 : Boolean;VAR OnDatabaseDelete@1003 : Boolean;VAR OnDatabaseRename@1004 : Boolean);
    VAR
      IntegrationManagement@1005 : Codeunit 5150;
      ChangeLogMgt@1006 : Codeunit 423;
    BEGIN
    END;

    PROCEDURE OnDatabaseInsert@26(RecRef@1000 : RecordRef);
    BEGIN
    END;

    PROCEDURE OnDatabaseModify@27(RecRef@1000 : RecordRef);
    BEGIN
    END;

    PROCEDURE OnDatabaseDelete@28(RecRef@1000 : RecordRef);
    BEGIN
    END;

    PROCEDURE OnDatabaseRename@29(RecRef@1000 : RecordRef;xRecRef@1001 : RecordRef);
    BEGIN
    END;

    BEGIN
    END.
  }
}

