OBJECT Table 99008534 Outbound Product Catalog Hdr.
{
  OBJECT-PROPERTIES
  {
    Date=29/02/12;
    Time=18:14:20;
    Version List=NAVW16.00;
  }
  PROPERTIES
  {
    OnDelete=VAR
               OutProductCatalogLine@1000 : Record 99008510;
             BEGIN
               OutProductCatalogLine.LOCKTABLE;
               OutProductCatalogLine.SETRANGE("Outbound Document No.",Rec."Outbound Document No.");
               OutProductCatalogLine.DELETEALL;
             END;

    CaptionML=[ENU=Outbound Product Catalog Hdr.;
               PTB=Cab. Cat�logo Produto Sa�da];
    LookupPageID=Page99008537;
  }
  FIELDS
  {
    { 1   ;   ;Customer No.        ;Code20        ;AltSearchField=Search Name;
                                                   OnValidate=VAR
                                                                Customer@1000 : Record 18;
                                                              BEGIN
                                                              END;

                                                   CaptionML=[ENU=Customer No.;
                                                              PTB=N� Cliente] }
    { 2   ;   ;Customer Name       ;Text50        ;CaptionML=[ENU=Customer Name;
                                                              PTB=Nome Cliente] }
    { 3   ;   ;Search Name         ;Code50        ;CaptionML=[ENU=Search Name;
                                                              PTB=Nome Fantasia] }
    { 4   ;   ;Name 2              ;Text50        ;CaptionML=[ENU=Name 2;
                                                              PTB=Nome Complementar] }
    { 5   ;   ;Customer Address    ;Text50        ;CaptionML=[ENU=Customer Address;
                                                              PTB=Endere�o Cliente] }
    { 6   ;   ;Customer Address 2  ;Text50        ;CaptionML=[ENU=Customer Address 2;
                                                              PTB=Endere�o Cliente 2] }
    { 7   ;   ;Customer City       ;Text30        ;CaptionML=[ENU=Customer City;
                                                              PTB=Cidade Cliente] }
    { 8   ;   ;Customer Contact    ;Text50        ;CaptionML=[ENU=Customer Contact;
                                                              PTB=Contato Cliente] }
    { 9   ;   ;Customer Phone No.  ;Text30        ;ExtendedDatatype=Phone No.;
                                                   CaptionML=[ENU=Customer Phone No.;
                                                              PTB=N� Telefone Cliente] }
    { 10  ;   ;Telex No.           ;Text20        ;CaptionML=[ENU=Telex No.;
                                                              PTB=N� Telex] }
    { 14  ;   ;Our Account No.     ;Text20        ;CaptionML=[ENU=Our Account No.;
                                                              PTB=Nosso N� Conta] }
    { 15  ;   ;Territory Code      ;Code10        ;CaptionML=[ENU=Territory Code;
                                                              PTB=Cod. Unidade Federal] }
    { 21  ;   ;Customer Posting Group;Code10      ;CaptionML=[ENU=Customer Posting Group;
                                                              PTB=Gr. Cont�bil Cliente] }
    { 22  ;   ;Currency Code       ;Code10        ;CaptionML=[ENU=Currency Code;
                                                              PTB=Cod. Moeda] }
    { 24  ;   ;Language Code       ;Code10        ;CaptionML=[ENU=Language Code;
                                                              PTB=Cod. Idioma] }
    { 29  ;   ;Salesperson Code    ;Code10        ;CaptionML=[ENU=Salesperson Code;
                                                              PTB=Cod. Vendedor] }
    { 30  ;   ;Shipment Method Code;Code10        ;CaptionML=[ENU=Shipment Method Code;
                                                              PTB=Cod. Condi��es Envio] }
    { 31  ;   ;Shipping Agent Code ;Code10        ;CaptionML=[ENU=Shipping Agent Code;
                                                              PTB=Cod. Transportador] }
    { 35  ;   ;Customer Country/Region Code;Code10;CaptionML=[ENU=Customer Country/Region Code;
                                                              PTB=C�d. Pa�s/Regi�o Cliente] }
    { 91  ;   ;Customer Post Code  ;Code20        ;ValidateTableRelation=No;
                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=Customer Post Code;
                                                              PTB=Cliente CEP] }
    { 92  ;   ;Customer County     ;Text30        ;CaptionML=[ENU=Customer County;
                                                              PTB=Distrito Cliente] }
    { 102 ;   ;Customer E-Mail     ;Text80        ;ExtendedDatatype=E-Mail;
                                                   CaptionML=[ENU=Customer E-Mail;
                                                              PTB=E-Mail Cliente] }
    { 99008500;;Outbound Document No.;Integer     ;CaptionML=[ENU=Outbound Document No.;
                                                              PTB=N� Documento Sa�da] }
    { 99008501;;Date Sent          ;Date          ;CaptionML=[ENU=Date Sent;
                                                              PTB=Data Enviada] }
    { 99008502;;Time Sent          ;Time          ;CaptionML=[ENU=Time Sent;
                                                              PTB=Hora Enviada] }
    { 99008503;;No. Resent         ;Integer       ;CaptionML=[ENU=No. Resent;
                                                              PTB=N� Reenvios] }
    { 99008505;;Status             ;Option        ;CaptionML=[ENU=Status;
                                                              PTB=Status];
                                                   OptionCaptionML=[ENU=Unsent,Sent,Processing;
                                                                    PTB=Por Enviar,Enviado,Precessando];
                                                   OptionString=Unsent,Sent,Processing }
    { 99008506;;Tracking ID        ;Text50        ;CaptionML=[ENU=Tracking ID;
                                                              PTB=ID Rastreabilidade] }
    { 99008507;;Document Selected  ;Boolean       ;CaptionML=[ENU=Document Selected;
                                                              PTB=Documento Selecionado] }
  }
  KEYS
  {
    {    ;Outbound Document No.,Customer No.      ;Clustered=Yes }
    {    ;Status                                   }
    {    ;Tracking ID                              }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

