OBJECT Table 5771 Warehouse Source Filter
{
  OBJECT-PROPERTIES
  {
    Date=09/09/14;
    Time=12:00:00;
    Version List=NAVW18.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Warehouse Source Filter;
               PTB=Filtro Origem Dep�sito];
  }
  FIELDS
  {
    { 1   ;   ;Code                ;Code10        ;CaptionML=[ENU=Code;
                                                              PTB=C�digo];
                                                   NotBlank=Yes }
    { 2   ;   ;Description         ;Text50        ;CaptionML=[ENU=Description;
                                                              PTB=Descri��o] }
    { 3   ;   ;Item No. Filter     ;Code100       ;TableRelation=Item;
                                                   ValidateTableRelation=No;
                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=Item No. Filter;
                                                              PTB=Filtro N� Produto] }
    { 4   ;   ;Variant Code Filter ;Code100       ;TableRelation="Item Variant".Code;
                                                   ValidateTableRelation=No;
                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=Variant Code Filter;
                                                              PTB=Filtro Cod. Variante] }
    { 5   ;   ;Unit of Measure Filter;Code100     ;TableRelation="Unit of Measure";
                                                   ValidateTableRelation=No;
                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=Unit of Measure Filter;
                                                              PTB=Filtro Unid. Medida] }
    { 6   ;   ;Sell-to Customer No. Filter;Code100;TableRelation=Customer;
                                                   ValidateTableRelation=No;
                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=Sell-to Customer No. Filter;
                                                              PTB=Filtro Venda-a N� Cliente] }
    { 7   ;   ;Buy-from Vendor No. Filter;Code100 ;TableRelation=Vendor;
                                                   ValidateTableRelation=No;
                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=Buy-from Vendor No. Filter;
                                                              PTB=Filtro Compra-a N� Fornecedor] }
    { 8   ;   ;Customer No. Filter ;Code100       ;TableRelation=Customer;
                                                   ValidateTableRelation=No;
                                                   TestTableRelation=No;
                                                   CaptionML=ENU=Customer No. Filter }
    { 10  ;   ;Planned Delivery Date Filter;Date  ;FieldClass=FlowFilter;
                                                   CaptionML=[ENU=Planned Delivery Date Filter;
                                                              PTB=Filtro Data Entrega Planejada] }
    { 11  ;   ;Shipment Method Code Filter;Code100;TableRelation="Shipment Method";
                                                   ValidateTableRelation=No;
                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=Shipment Method Code Filter;
                                                              PTB=Filtro Cod. M�todo Envio] }
    { 12  ;   ;Shipping Agent Code Filter;Code100 ;TableRelation="Shipping Agent";
                                                   ValidateTableRelation=No;
                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=Shipping Agent Code Filter;
                                                              PTB=Filtro Cod. Transportador] }
    { 13  ;   ;Shipping Advice Filter;Code100     ;CaptionML=[ENU=Shipping Advice Filter;
                                                              PTB=Filtro Envio Parcial/Total] }
    { 15  ;   ;Do Not Fill Qty. to Handle;Boolean ;CaptionML=[ENU=Do Not Fill Qty. to Handle;
                                                              PTB=N�o Preench. Qtd. a Processar] }
    { 16  ;   ;Show Filter Request ;Boolean       ;CaptionML=[ENU=Show Filter Request;
                                                              PTB=Mostrar Pedido Filtro] }
    { 17  ;   ;Shipping Agent Service Filter;Code100;
                                                   TableRelation="Shipping Agent Services".Code;
                                                   ValidateTableRelation=No;
                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=Shipping Agent Service Filter;
                                                              PTB=Filtro Servi�o Transportador] }
    { 18  ;   ;In-Transit Code Filter;Code100     ;TableRelation=Location WHERE (Use As In-Transit=CONST(Yes));
                                                   ValidateTableRelation=No;
                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=In-Transit Code Filter;
                                                              PTB=Filtro Cod. Em-Tr�nsito] }
    { 19  ;   ;Transfer-from Code Filter;Code100  ;TableRelation=Location WHERE (Use As In-Transit=CONST(No));
                                                   ValidateTableRelation=No;
                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=Transfer-from Code Filter;
                                                              PTB=Filtro Transf.-de Cod.] }
    { 20  ;   ;Transfer-to Code Filter;Code100    ;TableRelation=Location WHERE (Use As In-Transit=CONST(No));
                                                   ValidateTableRelation=No;
                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=Transfer-to Code Filter;
                                                              PTB=Filtro Transf.-para Cod.] }
    { 21  ;   ;Planned Shipment Date Filter;Date  ;FieldClass=FlowFilter;
                                                   CaptionML=[ENU=Planned Shipment Date Filter;
                                                              PTB=Filtro Data Planejada Envio] }
    { 22  ;   ;Planned Receipt Date Filter;Date   ;FieldClass=FlowFilter;
                                                   CaptionML=[ENU=Planned Receipt Date Filter;
                                                              PTB=Filtro Data Recep��o Planejada] }
    { 23  ;   ;Expected Receipt Date Filter;Date  ;FieldClass=FlowFilter;
                                                   CaptionML=[ENU=Expected Receipt Date Filter;
                                                              PTB=Filtro Data Recep��o Esperada] }
    { 24  ;   ;Shipment Date Filter;Date          ;FieldClass=FlowFilter;
                                                   CaptionML=[ENU=Shipment Date Filter;
                                                              PTB=Filtro Data Envio] }
    { 25  ;   ;Receipt Date Filter ;Date          ;FieldClass=FlowFilter;
                                                   CaptionML=[ENU=Receipt Date Filter;
                                                              PTB=Filtro Data Recep��o] }
    { 28  ;   ;Sales Shipment Date Filter;Date    ;FieldClass=FlowFilter;
                                                   CaptionML=[ENU=Sales Shipment Date Filter;
                                                              PTB=Filtro Data Envio Venda] }
    { 98  ;   ;Source No. Filter   ;Code100       ;CaptionML=[ENU=Source No. Filter;
                                                              PTB=Filtro N� Origem] }
    { 99  ;   ;Source Document     ;Code250       ;CaptionML=[ENU=Source Document;
                                                              PTB=Documento Origem] }
    { 100 ;   ;Type                ;Option        ;OnValidate=BEGIN
                                                                IF Type = Type::Inbound THEN BEGIN
                                                                  "Sales Orders" := FALSE;
                                                                  "Purchase Return Orders" := FALSE;
                                                                  "Outbound Transfers" := FALSE;
                                                                  "Service Orders" := FALSE;
                                                                END ELSE BEGIN
                                                                  "Purchase Orders" := FALSE;
                                                                  "Sales Return Orders" := FALSE;
                                                                  "Inbound Transfers" := FALSE;
                                                                END;
                                                              END;

                                                   CaptionML=[ENU=Type;
                                                              PTB=Tipo];
                                                   OptionCaptionML=[ENU=Inbound,Outbound;
                                                                    PTB=Sa�da,Entrada];
                                                   OptionString=Inbound,Outbound }
    { 101 ;   ;Sales Orders        ;Boolean       ;InitValue=Yes;
                                                   OnValidate=BEGIN
                                                                IF Type = Type::Outbound THEN
                                                                  CheckOutboundSourceDocumentChosen;
                                                              END;

                                                   AccessByPermission=TableData 110=R;
                                                   CaptionML=[ENU=Sales Orders;
                                                              PTB=Pedidos Venda] }
    { 102 ;   ;Sales Return Orders ;Boolean       ;InitValue=Yes;
                                                   OnValidate=BEGIN
                                                                IF Type = Type::Inbound THEN
                                                                  CheckInboundSourceDocumentChosen;
                                                              END;

                                                   AccessByPermission=TableData 6660=R;
                                                   CaptionML=[ENU=Sales Return Orders;
                                                              PTB=Devolu��es Venda] }
    { 103 ;   ;Purchase Orders     ;Boolean       ;InitValue=Yes;
                                                   OnValidate=BEGIN
                                                                IF Type = Type::Inbound THEN
                                                                  CheckInboundSourceDocumentChosen;
                                                              END;

                                                   AccessByPermission=TableData 120=R;
                                                   CaptionML=[ENU=Purchase Orders;
                                                              PTB=Pedidos Compra] }
    { 104 ;   ;Purchase Return Orders;Boolean     ;InitValue=Yes;
                                                   OnValidate=BEGIN
                                                                IF Type = Type::Outbound THEN
                                                                  CheckOutboundSourceDocumentChosen;
                                                              END;

                                                   AccessByPermission=TableData 6650=R;
                                                   CaptionML=[ENU=Purchase Return Orders;
                                                              PTB=Devolu��es Compra] }
    { 105 ;   ;Inbound Transfers   ;Boolean       ;InitValue=Yes;
                                                   OnValidate=BEGIN
                                                                IF Type = Type::Inbound THEN
                                                                  CheckInboundSourceDocumentChosen;
                                                              END;

                                                   AccessByPermission=TableData 5740=R;
                                                   CaptionML=[ENU=Inbound Transfers;
                                                              PTB=Transf. Entrada] }
    { 106 ;   ;Outbound Transfers  ;Boolean       ;InitValue=Yes;
                                                   OnValidate=BEGIN
                                                                IF Type = Type::Outbound THEN
                                                                  CheckOutboundSourceDocumentChosen;
                                                              END;

                                                   AccessByPermission=TableData 5740=R;
                                                   CaptionML=[ENU=Outbound Transfers;
                                                              PTB=Transf. Sa�da] }
    { 108 ;   ;Partial             ;Boolean       ;InitValue=Yes;
                                                   OnValidate=BEGIN
                                                                IF NOT Partial AND NOT Complete THEN
                                                                  ERROR(Text000,FIELDCAPTION("Shipping Advice Filter"));
                                                              END;

                                                   CaptionML=[ENU=Partial;
                                                              PTB=Parcial] }
    { 109 ;   ;Complete            ;Boolean       ;InitValue=Yes;
                                                   OnValidate=BEGIN
                                                                IF NOT Partial AND NOT Complete THEN
                                                                  ERROR(Text000,FIELDCAPTION("Shipping Advice Filter"));
                                                              END;

                                                   CaptionML=[ENU=Complete;
                                                              PTB=Completo] }
    { 110 ;   ;Service Orders      ;Boolean       ;InitValue=Yes;
                                                   OnValidate=BEGIN
                                                                IF Type = Type::Outbound THEN
                                                                  CheckOutboundSourceDocumentChosen;
                                                              END;

                                                   CaptionML=ENU=Service Orders }
    { 7300;   ;Planned Delivery Date;Text250      ;CaptionML=[ENU=Planned Delivery Date;
                                                              PTB=Data de Entrega Planejada] }
    { 7301;   ;Planned Shipment Date;Text250      ;CaptionML=[ENU=Planned Shipment Date;
                                                              PTB=Data Planejada Envio] }
    { 7302;   ;Planned Receipt Date;Text250       ;CaptionML=[ENU=Planned Receipt Date;
                                                              PTB=Data Planejada Recep��o] }
    { 7303;   ;Expected Receipt Date;Text250      ;CaptionML=[ENU=Expected Receipt Date;
                                                              PTB=Data Recep��o Esperada] }
    { 7304;   ;Shipment Date       ;Text250       ;CaptionML=[ENU=Shipment Date;
                                                              PTB=Data Envio] }
    { 7305;   ;Receipt Date        ;Text250       ;CaptionML=[ENU=Receipt Date;
                                                              PTB=Data Remessa Compra] }
    { 7306;   ;Sales Shipment Date ;Text250       ;CaptionML=[ENU=Sales Shipment Date;
                                                              PTB=Data Remessa Venda] }
  }
  KEYS
  {
    {    ;Type,Code                               ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      Text000@1000 : TextConst 'ENU=%1 must be chosen.;PTB=%1 deve ser escolhido';

    PROCEDURE SetFilters@1(VAR GetSourceBatch@1000 : Report 5753;LocationCode@1001 : Code[10]);
    VAR
      WhseRequest@1002 : Record 5765;
      SalesLine@1004 : Record 37;
      PurchLine@1006 : Record 39;
      TransLine@1008 : Record 5741;
      SalesHeader@1003 : Record 36;
      ServiceHeader@1005 : Record 5900;
      ServiceLine@1007 : Record 5902;
    BEGIN
      "Source Document" := '';

      IF "Sales Orders" THEN BEGIN
        WhseRequest."Source Document" := WhseRequest."Source Document"::"Sales Order";
        AddFilter("Source Document",FORMAT(WhseRequest."Source Document"));
      END;

      IF "Service Orders" THEN BEGIN
        WhseRequest."Source Document" := WhseRequest."Source Document"::"Service Order";
        AddFilter("Source Document",FORMAT(WhseRequest."Source Document"));
      END;

      IF "Sales Return Orders" THEN BEGIN
        WhseRequest."Source Document" := WhseRequest."Source Document"::"Sales Return Order";
        AddFilter("Source Document",FORMAT(WhseRequest."Source Document"));
      END;

      IF "Outbound Transfers" THEN BEGIN
        WhseRequest."Source Document" := WhseRequest."Source Document"::"Outbound Transfer";
        AddFilter("Source Document",FORMAT(WhseRequest."Source Document"));
      END;

      IF "Purchase Orders" THEN BEGIN
        WhseRequest."Source Document" := WhseRequest."Source Document"::"Purchase Order";
        AddFilter("Source Document",FORMAT(WhseRequest."Source Document"));
      END;

      IF "Purchase Return Orders" THEN BEGIN
        WhseRequest."Source Document" := WhseRequest."Source Document"::"Purchase Return Order";
        AddFilter("Source Document",FORMAT(WhseRequest."Source Document"));
      END;

      IF "Inbound Transfers" THEN BEGIN
        WhseRequest."Source Document" := WhseRequest."Source Document"::"Inbound Transfer";
        AddFilter("Source Document",FORMAT(WhseRequest."Source Document"));
      END;

      IF "Source Document" = '' THEN
        ERROR(Text000,FIELDCAPTION("Source Document"));

      WhseRequest.SETFILTER("Source Document","Source Document");

      WhseRequest.SETFILTER("Source No.","Source No. Filter");
      WhseRequest.SETFILTER("Shipment Method Code","Shipment Method Code Filter");

      "Shipping Advice Filter" := '';

      IF Partial THEN BEGIN
        WhseRequest."Shipping Advice" := WhseRequest."Shipping Advice"::Partial;
        AddFilter("Shipping Advice Filter",FORMAT(WhseRequest."Shipping Advice"));
      END;

      IF Complete THEN BEGIN
        WhseRequest."Shipping Advice" := WhseRequest."Shipping Advice"::Complete;
        AddFilter("Shipping Advice Filter",FORMAT(WhseRequest."Shipping Advice"));
      END;

      WhseRequest.SETFILTER("Shipping Advice","Shipping Advice Filter");
      WhseRequest.SETRANGE("Location Code",LocationCode);

      SalesLine.SETFILTER("No.","Item No. Filter");
      SalesLine.SETFILTER("Variant Code","Variant Code Filter");
      SalesLine.SETFILTER("Unit of Measure Code","Unit of Measure Filter");

      ServiceLine.SETRANGE(Type,ServiceLine.Type::Item);
      ServiceLine.SETFILTER("No.","Item No. Filter");
      ServiceLine.SETFILTER("Variant Code","Variant Code Filter");
      ServiceLine.SETFILTER("Unit of Measure Code","Unit of Measure Filter");

      PurchLine.SETFILTER("No.","Item No. Filter");
      PurchLine.SETFILTER("Variant Code","Variant Code Filter");
      PurchLine.SETFILTER("Unit of Measure Code","Unit of Measure Filter");

      TransLine.SETFILTER("Item No.","Item No. Filter");
      TransLine.SETFILTER("Variant Code","Variant Code Filter");
      TransLine.SETFILTER("Unit of Measure Code","Unit of Measure Filter");

      SalesHeader.SETFILTER("Sell-to Customer No.","Sell-to Customer No. Filter");
      SalesLine.SETFILTER("Planned Delivery Date","Planned Delivery Date");
      SalesLine.SETFILTER("Planned Shipment Date","Planned Shipment Date");
      SalesLine.SETFILTER("Shipment Date","Sales Shipment Date");

      ServiceHeader.SETFILTER("Customer No.","Customer No. Filter");

      ServiceLine.SETFILTER("Planned Delivery Date","Planned Delivery Date");

      PurchLine.SETFILTER("Buy-from Vendor No.","Buy-from Vendor No. Filter");
      PurchLine.SETFILTER("Expected Receipt Date","Expected Receipt Date");
      PurchLine.SETFILTER("Planned Receipt Date","Planned Receipt Date");

      TransLine.SETFILTER("In-Transit Code","In-Transit Code Filter");
      TransLine.SETFILTER("Transfer-from Code","Transfer-from Code Filter");
      TransLine.SETFILTER("Transfer-to Code","Transfer-to Code Filter");
      TransLine.SETFILTER("Shipment Date","Shipment Date");
      TransLine.SETFILTER("Receipt Date","Receipt Date");

      SalesLine.SETFILTER("Shipping Agent Code","Shipping Agent Code Filter");
      SalesLine.SETFILTER("Shipping Agent Service Code","Shipping Agent Service Filter");

      ServiceLine.SETFILTER("Shipping Agent Code","Shipping Agent Code Filter");
      ServiceLine.SETFILTER("Shipping Agent Service Code","Shipping Agent Service Filter");

      TransLine.SETFILTER("Shipping Agent Code","Shipping Agent Code Filter");
      TransLine.SETFILTER("Shipping Agent Service Code","Shipping Agent Service Filter");

      GetSourceBatch.SETTABLEVIEW(WhseRequest);
      GetSourceBatch.SETTABLEVIEW(SalesHeader);
      GetSourceBatch.SETTABLEVIEW(SalesLine);
      GetSourceBatch.SETTABLEVIEW(PurchLine);
      GetSourceBatch.SETTABLEVIEW(TransLine);
      GetSourceBatch.SETTABLEVIEW(ServiceHeader);
      GetSourceBatch.SETTABLEVIEW(ServiceLine);
      GetSourceBatch.SetDoNotFillQtytoHandle("Do Not Fill Qty. to Handle");
    END;

    LOCAL PROCEDURE AddFilter@2(VAR CodeField@1000 : Code[250];NewFilter@1001 : Text[100]);
    BEGIN
      IF CodeField = '' THEN
        CodeField := NewFilter
      ELSE
        CodeField := CodeField + '|' + NewFilter;
    END;

    LOCAL PROCEDURE CheckInboundSourceDocumentChosen@3();
    BEGIN
      IF NOT ("Sales Return Orders" OR "Purchase Orders" OR "Inbound Transfers") THEN
        ERROR(Text000,FIELDCAPTION("Source Document"));
    END;

    LOCAL PROCEDURE CheckOutboundSourceDocumentChosen@5();
    BEGIN
      IF NOT ("Sales Orders" OR "Purchase Return Orders" OR "Outbound Transfers" OR "Service Orders") THEN
        ERROR(Text000,FIELDCAPTION("Source Document"));
    END;

    BEGIN
    END.
  }
}

