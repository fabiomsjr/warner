OBJECT Codeunit 52112725 Purchase Order E-Mail Job
{
  OBJECT-PROPERTIES
  {
    Date=09/09/13;
    Time=14:19:58;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    TableNo=472;
    Permissions=TableData 454=rimd;
    OnRun=VAR
            thisCodeunit@52006500 : Codeunit 52112725;
            dummyRec@52006501 : Record 472;
          BEGIN
            dummyRec."Parameter String" := "Parameter String";

            IF "Object ID to Run" = 0 THEN
              Send
            ELSE BEGIN
              IF NOT thisCodeunit.RUN(dummyRec) THEN
                SetError(GETLASTERRORTEXT);
            END;
          END;

  }
  CODE
  {
    VAR
      NavMail@35000001 : DotNet "'NavMail, Version=1.0.0.0, Culture=neutral, PublicKeyToken=1b70b0bb42ec0741'.NavMail.NavMail";
      InitOK@35000000 : Boolean;
      Text001@52006501 : TextConst 'ENU=Dear Supplier,<br /><br />Please find attached a Purchase %1 from %2.<br /><br />Just let me know if you need further information.<br /><br />Purchasing Department - %2;PTB=Prezado Fornecedor,<br /><br />Segue anexo %1 de Compras da %2.<br />Por gentileza notar as Condi��es Gerais do %1 de Compras.<br /><br />Ficamos � disposi��o para eventuais esclarecimentos.<br /><br />Compras - %2';
      Text002@35000027 : TextConst 'ENU=Purchase %1 %2 - %3;PTB=%1 de Compra %2 - %3';
      PurchSetup@52006500 : Record 312;
      Text003@52006502 : TextConst 'ENU=Dear Supplier,<br /><br />Due to internal operation issues, the Purchase %1 %2 attached is being cancelled.<br /><br />Please get in contact with us to clarify the reason if it is necessary.<br /><br />Regards,<br /><br />Purchases - %3;PTB=Prezado Fornecedor,<br /><br />Por gentileza desconsiderar o %1 de Compras %2 em anexo, cancelado por quest�es operacionais.<br /><br />Ficamos � disposi��o para eventuais esclarecimentos.<br /><br />Desde j� agradecemos a compreens�o!<br /><br />Compras - %3';
      Text004@52006503 : TextConst 'ENU=Cancelled;PTB=Cancelado';

    PROCEDURE Send@35000012();
    VAR
      reportSelections@52006501 : Record 77;
      purchHeader@52006500 : Record 38;
      purchHeaderToEmail@35000004 : Record 38;
      purchHeaderModify@52006502 : Record 38;
      purchHeaderArc@52006505 : Record 5109;
      purchHeaderArcToEmail@52006504 : Record 5109;
      purchHeaderArcModify@52006503 : Record 5109;
    BEGIN
      reportSelections.SETRANGE(Usage, reportSelections.Usage::"P.Order");
      reportSelections.SETFILTER("Report ID", '>0');
      reportSelections.FINDFIRST;

      WITH purchHeader DO BEGIN
        SETCURRENTKEY("Document Type",Status,"E-Mail Sent to Vendor");
        SETRANGE("Document Type", "Document Type"::Order);
        SETRANGE(Status, Status::Released);
        SETRANGE("E-Mail Sent to Vendor", FALSE);
        IF FINDSET THEN
          REPEAT
            purchHeaderToEmail.GET("Document Type", "No.");
            purchHeaderToEmail.MARK(TRUE);
          UNTIL NEXT = 0;
      END;
      WITH purchHeaderToEmail DO BEGIN
        MARKEDONLY(TRUE);
        IF FINDFIRST THEN
          REPEAT
            AddPurchOrder(purchHeaderToEmail, reportSelections."Report ID");
            purchHeaderModify.GET("Document Type", "No.");
            purchHeaderModify."E-Mail Sent to Vendor" := TRUE;
            purchHeaderModify.MODIFY;
          UNTIL NEXT = 0;
      END;

      //Cancellations
      reportSelections.SETRANGE(Usage, 48);
      reportSelections.SETFILTER("Report ID", '>0');
      reportSelections.FINDFIRST;

      WITH purchHeaderArc DO BEGIN
        SETCURRENTKEY("Document Type","E-Mail Sent to Vendor","Send Cancellation E-Mail");
        SETRANGE("Document Type", "Document Type"::Order);
        SETRANGE("Send Cancellation E-Mail", TRUE);
        SETRANGE("E-Mail Sent to Vendor", FALSE);
        IF FINDSET THEN
          REPEAT
            purchHeaderArcToEmail.GET("Document Type", "No.", "Doc. No. Occurrence", "Version No.");
            purchHeaderArcToEmail.MARK(TRUE);
          UNTIL NEXT = 0;
      END;
      WITH purchHeaderArcToEmail DO BEGIN
        MARKEDONLY(TRUE);
        IF FINDFIRST THEN
          REPEAT
            AddPurchOrderCancel(purchHeaderArcToEmail, reportSelections."Report ID");
            purchHeaderArcModify.GET("Document Type", "No.", "Doc. No. Occurrence", "Version No.");
            purchHeaderArcModify."E-Mail Sent to Vendor" := TRUE;
            purchHeaderArcModify.MODIFY;
          UNTIL NEXT = 0;
      END;

      SendMails;
    END;

    PROCEDURE SendMails@35000002();
    BEGIN
      IF InitOK THEN
        NavMail.SendMails();
    END;

    PROCEDURE AddPurchOrder@35000005(purchHeader@35000001 : Record 38;reportID@52006503 : Integer);
    VAR
      vendor@52006500 : Record 23;
      compInfo@52006502 : Record 79;
      approvEntry@52006501 : Record 454;
      userSetup@35000004 : Record 91;
      subject@35000002 : Text[1024];
      tmpFilename@52006504 : Text[512];
      purchHeaderFiltered@52006505 : Record 38;
    BEGIN
      InitDLL;
      purchHeader.TESTFIELD("Buy-from Vendor No.");
      vendor.GET(purchHeader."Buy-from Vendor No.");
      IF vendor."E-Mail" = '' THEN
        EXIT;
      IF vendor.Category = vendor.Category::"3.- Foreign" THEN
        GLOBALLANGUAGE(1033)  //EN-US
      ELSE
        GLOBALLANGUAGE(1046); //PT-BR
      compInfo.GetCompanyBranchInfo(purchHeader."Branch Code");

      approvEntry.SETFILTER("Table ID", '%1|%2', DATABASE::"Purchase Header", DATABASE::"Purchase Line");
      approvEntry.SETRANGE("Document Type", purchHeader."Document Type");
      approvEntry.SETRANGE("Document No.", purchHeader."No.");
      approvEntry.SETRANGE(Status, approvEntry.Status::Approved);
      IF NOT approvEntry.FINDLAST THEN
        EXIT;
      userSetup.GET(approvEntry."Sender ID");

      tmpFilename := TEMPORARYPATH + purchHeader."No." + '.pdf';
      purchHeaderFiltered.GET(purchHeader."Document Type", purchHeader."No.");
      purchHeaderFiltered.SETRECFILTER;
      REPORT.SAVEASPDF(reportID, tmpFilename, purchHeaderFiltered);
      NavMail.AddAttachment(tmpFilename);

      NavMail.AppendBody(STRSUBSTNO(Text001, purchHeader."Document Type", compInfo.Name));
      subject := STRSUBSTNO(Text002, purchHeader."Document Type", purchHeader."No.", compInfo.Name);
      NavMail.AddMailMessage(vendor."E-Mail", subject, userSetup."E-Mail", TRUE, '', '');
    END;

    PROCEDURE AddPurchOrderCancel@52006503(purchHeaderArc@35000001 : Record 5109;reportID@52006503 : Integer);
    VAR
      vendor@52006500 : Record 23;
      compInfo@52006502 : Record 79;
      approvEntry@52006501 : Record 454;
      userSetup@35000004 : Record 91;
      subject@35000002 : Text[1024];
      tmpFilename@52006504 : Text[512];
      purchHeaderArcFiltered@52006505 : Record 5109;
    BEGIN
      InitDLL;
      IF purchHeaderArc."Assigned User ID" = '' THEN
        EXIT;
      IF purchHeaderArc."Buy-from Vendor No." = '' THEN
        EXIT;
      vendor.GET(purchHeaderArc."Buy-from Vendor No.");
      IF vendor."E-Mail" = '' THEN
        EXIT;
      IF vendor.Category = vendor.Category::"3.- Foreign" THEN
        GLOBALLANGUAGE(1033)  //EN-US
      ELSE
        GLOBALLANGUAGE(1046); //PT-BR
      compInfo.GetCompanyBranchInfo(purchHeaderArc."Branch Code");
      userSetup.GET(purchHeaderArc."Assigned User ID");

      tmpFilename := TEMPORARYPATH + purchHeaderArc."No." + '.pdf';
      purchHeaderArcFiltered.GET(purchHeaderArc."Document Type", purchHeaderArc."No.",
                                 purchHeaderArc."Doc. No. Occurrence", purchHeaderArc."Version No.");
      purchHeaderArcFiltered.SETRECFILTER;
      REPORT.SAVEASPDF(reportID, tmpFilename, purchHeaderArcFiltered);
      NavMail.AddAttachment(tmpFilename);

      NavMail.AppendBody(STRSUBSTNO(Text003, purchHeaderArc."Document Type", purchHeaderArc."No.", compInfo.Name));
      subject := STRSUBSTNO(Text002, purchHeaderArc."Document Type", purchHeaderArc."No.", compInfo.Name) + ' - ' + Text004;
      NavMail.AddMailMessage(vendor."E-Mail", subject, userSetup."E-Mail", TRUE, '', '');
    END;

    PROCEDURE InitDLL@35000004();
    BEGIN
      IF InitOK THEN
        EXIT;

      PurchSetup.GET;
      PurchSetup.TESTFIELD("PE SMTP Server");
      PurchSetup.TESTFIELD("PE SMTP Port");
      PurchSetup.TESTFIELD("PE SMTP Sender Mail");

      NavMail := NavMail.NavMail;
      NavMail.SetupSMTP(PurchSetup."PE SMTP Server", PurchSetup."PE SMTP Port", PurchSetup."PE SMTP SSL",
                        PurchSetup."PE SMTP Username", PurchSetup."PE SMTP Password",
                        PurchSetup."PE SMTP Sender Name", PurchSetup."PE SMTP Sender Mail");
      InitOK := TRUE;
    END;

    BEGIN
    END.
  }
}

