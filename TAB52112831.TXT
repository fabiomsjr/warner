OBJECT Table 52112831 Special Indicator Situation.
{
  OBJECT-PROPERTIES
  {
    Date=30/04/13;
    Time=18:14:18;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Special Indicator Situation;
               PTB=Indicador de Situa��o Especial];
    LookupPageID=Page52112837;
    DrillDownPageID=Page52112836;
  }
  FIELDS
  {
    { 1   ;   ;Code                ;Code20        ;CaptionML=[ENU=Code;
                                                              PTB=C�digo] }
    { 2   ;   ;Description         ;Text30        ;CaptionML=[ENU=Description;
                                                              PTB=Descri��o] }
  }
  KEYS
  {
    {    ;Code                                    ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

