OBJECT Table 52112559 Tax Consolidation Amount.
{
  OBJECT-PROPERTIES
  {
    Date=30/04/13;
    Time=18:14:18;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Tax Consolidation Amount;
               PTB=Consolida��o Valores IPI];
  }
  FIELDS
  {
    { 1   ;   ;CFOP Code           ;Code10        ;CaptionML=[ENU=CFOP Code;
                                                              PTB=C�d. CFOP] }
    { 2   ;   ;ICMS %              ;Decimal       ;CaptionML=[ENU=ICMS %;
                                                              PTB=% ICMS] }
    { 3   ;   ;G/L Amount          ;Decimal       ;CaptionML=[ENU=G/L Amount;
                                                              PTB=Valor Cont�bil] }
    { 4   ;   ;ICMS Basis Amount   ;Decimal       ;CaptionML=[ENU=ICMS Basis Amount;
                                                              PTB=Valor Base ICMS] }
    { 5   ;   ;ICMS Exempt Amount  ;Decimal       ;CaptionML=[ENU=ICMS Exempt Amount;
                                                              PTB=Valor ICMS Isento] }
    { 6   ;   ;ICMS Others Amount  ;Decimal       ;CaptionML=[ENU=ICMS Others Amount;
                                                              PTB=Valor ICMS Outros] }
    { 7   ;   ;IPI Basis Amount    ;Decimal       ;CaptionML=[ENU=IPI Basis Amount;
                                                              PTB=Valor Base IPI] }
    { 8   ;   ;IPI Exempt Amount   ;Decimal       ;CaptionML=[ENU=IPI Exempt Amount;
                                                              PTB=Valor IPI Isento] }
    { 9   ;   ;IPI Others Amount   ;Decimal       ;CaptionML=[ENU=IPI Others Amount;
                                                              PTB=Valor IPI Outros] }
    { 10  ;   ;ICMS Amount         ;Decimal       ;CaptionML=[ENU=ICMS Amount;
                                                              PTB=Valor ICMS] }
    { 11  ;   ;IPI Amount          ;Decimal       ;CaptionML=[ENU=IPI Amount;
                                                              PTB=Valor IPI] }
    { 12  ;   ;CFOP Description    ;Text50        ;CaptionML=[ENU=CFOP Description;
                                                              PTB=Descr. CFOP] }
    { 13  ;   ;G/L Amount 1        ;Decimal       ;CaptionML=[ENU=G/L Amount 1;
                                                              PTB=Valor Cont�bil 1] }
    { 14  ;   ;G/L Amount2         ;Decimal       ;CaptionML=[ENU=G/L Amount2;
                                                              PTB=Valor Cont�bil 2] }
    { 15  ;   ;G/L Amount 3        ;Decimal       ;CaptionML=[ENU=G/L Amount 3;
                                                              PTB=Valor Cont�bil 3] }
    { 16  ;   ;Fiscal Book         ;Boolean       ;CaptionML=[ENU=Fiscal Book;
                                                              PTB=Livro Fiscal] }
    { 17  ;   ;Posting Date        ;Date          ;CaptionML=[ENU=Posting Date;
                                                              PTB=Data de Registro] }
    { 18  ;   ;Fiscal Document Type;Code10        ;CaptionML=[ENU=Fiscal Document Type;
                                                              PTB=Tipo do Documento Fiscal] }
    { 19  ;   ;Series/SubSeries    ;Code10        ;CaptionML=[ENU=Series/SubSeries;
                                                              PTB=S�ries/Sub S�ries] }
    { 20  ;   ;Document Date       ;Date          ;CaptionML=[ENU=Document Date;
                                                              PTB=Data do Documento] }
    { 21  ;   ;Bill-to/Pay-to No.  ;Code20        ;CaptionML=[ENU=Bill-to/Pay-to No.;
                                                              PTB=Conta-a/Pagar-para No.] }
    { 22  ;   ;Territory Code      ;Code10        ;CaptionML=[ENU=Territory Code;
                                                              PTB=C�d. Territory] }
    { 23  ;   ;Bill-to/Pay-to Name ;Text100       ;CaptionML=[ENU=Bill-to/Pay-to Name;
                                                              PTB=Conta-a/Pagar-para Nome] }
    { 24  ;   ;C.N.P.J.            ;Code20        ;CaptionML=[ENU=C.N.P.J.;
                                                              PTB=C.N.P.J.] }
    { 25  ;   ;I.E.                ;Code20        ;CaptionML=[ENU=I.E.;
                                                              PTB=I.E.] }
    { 26  ;   ;Observations        ;Text250       ;CaptionML=[ENU=Observations;
                                                              PTB=Observ.] }
    { 27  ;   ;Document No.        ;Code20        ;CaptionML=[ENU=Document No.;
                                                              PTB=No. Documento] }
    { 28  ;   ;Document Type       ;Option        ;CaptionML=[ENU=Document Type;
                                                              PTB=Tipo do Documento];
                                                   OptionCaptionML=[ENU=" ,Payment,Invoice,Credit Memo,Finance Charge Memo,Reminder,Refund";
                                                                    PTB=" ,Pagamento,N.Fiscal,Nota Cr�dito,Nota Juros,Carta Aviso,Reembolso"];
                                                   OptionString=[ ,Pagamento,N.Fiscal,Nota Cr�dito,Nota Juros,Carta Aviso,Reembolso] }
    { 29  ;   ;Applied-to Doc. Type;Option        ;CaptionML=[ENU=Applied-to Doc. Type;
                                                              PTB=Liq. por Tipo Doc.];
                                                   OptionCaptionML=[ENU=" ,Payment,Invoice,Credit Memo,Finance Charge Memo,Reminder,Refund,,Deliery Note,,,Shipment";
                                                                    PTB=" ,Pagamento,Nota Fiscal,Nota Cr�dito,Nota Cobran�a Financeira,Carta Aviso,Reembolso,,Nota Entrega,,,Remessa"];
                                                   OptionString=[ ,Payment,Invoice,Credit Memo,Finance Charge Memo,Reminder,Refund,,Deliery Note,,,Shipment] }
    { 30  ;   ;Applied-to Doc. No. ;Code20        ;CaptionML=[ENU=Applied-to Doc. No.;
                                                              PTB=Liq. por N� Doc.] }
    { 31  ;   ;Fiscal Type         ;Option        ;CaptionML=[ENU=Fiscal Type;
                                                              PTB=Tipo Fiscal];
                                                   OptionCaptionML=[ENU=" ,Input,Output";
                                                                    PTB=" ,Entrada,Sa�da"];
                                                   OptionString=[ ,Input,Output] }
    { 32  ;   ;Cancel Inv          ;Boolean       ;CaptionML=[ENU=Cancel Inv;
                                                              PTB=Cancelar Inv] }
    { 33  ;   ;First               ;Boolean       ;CaptionML=[ENU=First;
                                                              PTB=Primeiro] }
    { 34  ;   ;ICMS Pos Lines 1    ;Option        ;CaptionML=[ENU=ICMS Pos Lines 1;
                                                              PTB=Pos ICMS Linha 1];
                                                   OptionCaptionML=[ENU=" ,Basis Amount,Exempt Amount,Others Amount";
                                                                    PTB=" ,Valor Base,Valor Isentas,Valor Outras"];
                                                   OptionString=[ ,Basis Amount,Exempt Amount,Others Amount] }
    { 35  ;   ;ICMS Pos Lines 2    ;Option        ;CaptionML=[ENU=ICMS Pos Lines 2;
                                                              PTB=Pos ICMS Linha 2];
                                                   OptionCaptionML=[ENU=" ,Basis Amount,Exempt Amount,Others Amount";
                                                                    PTB=" ,Valor Base,Valor Isentas,Valor Outras"];
                                                   OptionString=[ ,Basis Amount,Exempt Amount,Others Amount] }
    { 36  ;   ;ICMS Pos Lines 3    ;Option        ;CaptionML=[ENU=ICMS Pos Lines 3;
                                                              PTB=Pos ICMS Linha 3];
                                                   OptionCaptionML=[ENU=" ,Basis Amount,Exempt Amount,Others Amount";
                                                                    PTB=" ,Valor Base,Valor Isentas,Valor Outras"];
                                                   OptionString=[ ,Basis Amount,Exempt Amount,Others Amount] }
    { 37  ;   ;IPI Pos Lines 1     ;Option        ;CaptionML=[ENU=IPI Pos Lines 1;
                                                              PTB=Pos IPI Linha 1];
                                                   OptionCaptionML=[ENU=" ,Basis Amount,Exempt Amount,Others Amount";
                                                                    PTB=" ,Valor Base,Valor Isentas,Valor Outras"];
                                                   OptionString=[ ,Basis Amount,Exempt Amount,Others Amount] }
    { 38  ;   ;IPI Pos Lines 2     ;Option        ;CaptionML=[ENU=IPI Pos Lines 2;
                                                              PTB=Pos IPI Linha 2];
                                                   OptionCaptionML=[ENU=" ,Basis Amount,Exempt Amount,Others Amount";
                                                                    PTB=" ,Valor Base,Valor Isentas,Valor Outras"];
                                                   OptionString=[ ,Basis Amount,Exempt Amount,Others Amount] }
    { 39  ;   ;IPI Pos Lines 3     ;Option        ;CaptionML=[ENU=IPI Pos Lines 3;
                                                              PTB=Pos IPI Linha 3];
                                                   OptionCaptionML=[ENU=" ,Basis Amount,Exempt Amount,Others Amount";
                                                                    PTB=" ,Valor Base,Valor Isentas,Valor Outras"];
                                                   OptionString=[ ,Basis Amount,Exempt Amount,Others Amount] }
    { 40  ;   ;Vendor Detail       ;Boolean       ;CaptionML=[ENU=Vendor Detail;
                                                              PTB=Detalhe Fornecedor] }
    { 41  ;   ;User ID             ;Code20        ;CaptionML=[ENU=User ID;
                                                              PTB=ID Usu�rio] }
    { 42  ;   ;Suframa             ;Boolean       ;CaptionML=[ENU=Suframa;
                                                              PTB=Suframa] }
    { 43  ;   ;Delivery City       ;Code10        ;CaptionML=[ENU=Delivery City;
                                                              PTB=Cidade de Entrega] }
    { 44  ;   ;Tax Settlement Code ;Code20        ;CaptionML=[ENU=Tax Settlement Code;
                                                              PTB=C�d. Apura��o Imp.] }
    { 45  ;   ;Service             ;Boolean       ;CaptionML=[ENU=Service;
                                                              PTB=Servi�o] }
    { 46  ;   ;Sintegra            ;Boolean       ;CaptionML=[ENU=SIntegra;
                                                              PTB=Sintegra] }
    { 47  ;   ;Original CFOP Code  ;Code20        ;CaptionML=PTB=C�d. CFOP Original }
    { 35000019;;Fiscal Model Code  ;Code10        ;CaptionML=PTB=C�d. Modelo Fiscal;
                                                   Description=EFD1.00 }
    { 35000020;;Tribut. Situation  ;Code10        ;CaptionML=PTB=Situa��o Tribut�ria;
                                                   Description=EFD1.00 }
    { 35000021;;Payment Type       ;Code10        ;CaptionML=PTB=Tipo Pagamento;
                                                   Description=EFD1.00 }
    { 35000022;;Freight By         ;Code10        ;CaptionML=PTB=Frete por Conta;
                                                   Description=EFD1.00 }
    { 35000023;;DI No.             ;Code10        ;CaptionML=PTB=N� DI;
                                                   Description=EFD1.00 }
    { 35000024;;Credit Title Type  ;Code10        ;CaptionML=PTB=Tipo T�tulo Cr�dito;
                                                   Description=EFD1.00 }
    { 35000025;;Complementary Cred. Title Type;Text50;
                                                   CaptionML=PTB=Descr. Complem. Tipo T�t. Cr�dito;
                                                   Description=EFD1.00 }
    { 35000026;;Number of Plots    ;Integer       ;CaptionML=PTB=N� Parcelas;
                                                   Description=EFD1.00 }
    { 35000027;;Item Mov.          ;Boolean       ;CaptionML=PTB=Mov. Item;
                                                   Description=EFD1.00 }
    { 35000028;;CST Code           ;Code10        ;CaptionML=PTB=C�d. CST;
                                                   Description=EFD1.00 }
  }
  KEYS
  {
    {    ;Tax Settlement Code,CST Code,CFOP Code,Fiscal Type,User ID;
                                                   Clustered=Yes }
    {    ;Document No.                             }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    {
      MBS/BR

      No.   dd.mm.yy   Developer   Company   DocNo.   Description
      -----------------------------------------------------------------------------------------------------------------
      00    01.12.06   MK          MICROSOFT LOC001   The reference Documentation file LOCALIZATIONBR.DOC
    }
    END.
  }
}

