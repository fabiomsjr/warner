OBJECT Table 35000079 NFe Table2
{
  OBJECT-PROPERTIES
  {
    Date=29/02/12;
    Time=19:56:58;
    Version List=;
  }
  PROPERTIES
  {
    DataPerCompany=No;
    LookupPageID=Page35000095;
    DrillDownPageID=Page35000095;
  }
  FIELDS
  {
    { 10  ;   ;NavNFe Code         ;Code20        ;TableRelation="NFe Layout2";
                                                   CaptionML=PTB=C�digo }
    { 15  ;   ;Parent Table Code   ;Code20        ;TableRelation="NFe Table2".Code WHERE (NavNFe Code=FIELD(NavNFe Code));
                                                   CaptionML=PTB=C�d. Tabela Pai }
    { 20  ;   ;Code                ;Code20        ;OnValidate=BEGIN
                                                                IF STRPOS(Code, '|') > 0 THEN
                                                                  ERROR(Text002);
                                                              END;

                                                   CaptionML=PTB=C�digo }
    { 25  ;   ;Type                ;Option        ;CaptionML=PTB=Tipo;
                                                   OptionString=Cabe�alho,Linha,Imposto,Cobran�a,Volume,DI,Encargos }
    { 30  ;   ;Description         ;Text50        ;CaptionML=PTB=Descri��o }
    { 40  ;   ;Table No.           ;Integer       ;TableRelation=Object.ID WHERE (Type=CONST(Table));
                                                   CaptionML=PTB=N� Tabela }
    { 50  ;   ;Table Name          ;Text250       ;FieldClass=FlowField;
                                                   CalcFormula=Lookup(Object.Name WHERE (Type=CONST(TableData),
                                                                                         ID=FIELD(Table No.)));
                                                   CaptionML=PTB=Nome Tabela;
                                                   Editable=No }
    { 70  ;   ;Filters             ;Integer       ;FieldClass=FlowField;
                                                   CalcFormula=Count("NFe Table Filter2" WHERE (NavNFe Code=FIELD(NavNFe Code),
                                                                                                Table Code=FIELD(Code)));
                                                   CaptionML=PTB=Filtros;
                                                   Editable=No }
    { 80  ;   ;Fields              ;Integer       ;FieldClass=FlowField;
                                                   CalcFormula=Count("NFe Field2" WHERE (NavNFe Code=FIELD(NavNFe Code),
                                                                                         Table Code=FIELD(Code)));
                                                   CaptionML=PTB=Campos;
                                                   Editable=No }
    { 90  ;   ;Child Tables        ;Integer       ;FieldClass=FlowField;
                                                   CalcFormula=Count("NFe Table2" WHERE (NavNFe Code=FIELD(NavNFe Code),
                                                                                         Parent Table Code=FIELD(Code)));
                                                   CaptionML=PTB=Tabelas Filhas;
                                                   Editable=No }
  }
  KEYS
  {
    {    ;NavNFe Code,Code                        ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      Text001@1102300000 : TextConst 'PTB=J� existe uma tabela de cabe�alho configurada.';
      Text002@1102300001 : TextConst 'PTB=O c�digo da tabela n�o pode conter o caracter |';

    BEGIN
    {
      --- THBR177 ---
      rafaelr,050811,NF-E

      --- THBR191 ---
      rafaelr,311011,(00) Encargos
    }
    END.
  }
}

