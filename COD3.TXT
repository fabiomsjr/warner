OBJECT Codeunit 3 G/L Account-Indent
{
  OBJECT-PROPERTIES
  {
    Date=15/09/04;
    Time=12:00:00;
    Version List=NAVW14.00,NAVBR7;
  }
  PROPERTIES
  {
    OnRun=BEGIN
            IF NOT
               CONFIRM(
                 Text000 +
                 Text001 +
                 Text002 +
                 Text003,TRUE)
            THEN
              EXIT;

            // > NAVBR
            IF BrazilianFunctionality THEN
              IdentBR
            ELSE
            // < NAVBR

            Indent;
          END;

  }
  CODE
  {
    VAR
      Text000@1000 : TextConst 'ENU="This function updates the indentation of all the G/L accounts in the chart of accounts. ";PTB=Essa fun��o atualiza a indenta��o de todas as Contas Cont�beis no plano de contas.';
      Text001@1001 : TextConst 'ENU="All accounts between a Begin-Total and the matching End-Total are indented one level. ";PTB=Todas as contas entre em Total-Inicial e compat�vel Total - Final s�o memorizados em um n�vel.';
      Text002@1002 : TextConst 'ENU=The Totaling for each End-total is also updated.;PTB=A Toaliza��o para cada Final Total tamb�m esta atualizada.';
      Text003@1003 : TextConst 'ENU=\\Do you want to indent the chart of accounts?;PTB=\\Voc� deseja indentar o plano de contas?';
      Text004@1004 : TextConst 'ENU=Indenting the Chart of Accounts #1##########;PTB=Indentando o Plano de Contas    #1##########';
      Text005@1005 : TextConst 'ENU=End-Total %1 is missing a matching Begin-Total.;PTB=Final-Total %1 n�o encontrou o correspondente Come�o-Total.';
      GLAcc@1006 : Record 15;
      Window@1007 : Dialog;
      AccNo@1008 : ARRAY [10] OF Code[20];
      i@1009 : Integer;

    PROCEDURE Indent@1();
    BEGIN
      Window.OPEN(Text004);

      WITH GLAcc DO
        IF FIND('-') THEN
          REPEAT
            Window.UPDATE(1,"No.");

            IF "Account Type" = "Account Type"::"End-Total" THEN BEGIN
              IF i < 1 THEN
                ERROR(
                  Text005,
                  "No.");
              Totaling := AccNo[i] + '..' + "No.";
              i := i - 1;
            END;

            Indentation := i;
            MODIFY;

            IF "Account Type" = "Account Type"::"Begin-Total" THEN BEGIN
              i := i + 1;
              AccNo[i] := "No.";
            END;
          UNTIL NEXT = 0;

      Window.CLOSE;
    END;

    PROCEDURE RunICAccountIndent@4();
    BEGIN
      IF NOT
         CONFIRM(
           Text000 +
           Text001 +
           Text003,TRUE)
      THEN
        EXIT;

      IndentICAccount;
    END;

    PROCEDURE IndentICAccount@2();
    VAR
      ICGLAcc@1000 : Record 410;
    BEGIN
      Window.OPEN(Text004);
      WITH ICGLAcc DO
        IF FIND('-') THEN
          REPEAT
            Window.UPDATE(1,"No.");

            IF "Account Type" = "Account Type"::"End-Total" THEN BEGIN
              IF i < 1 THEN
                ERROR(
                  Text005,
                  "No.");
              i := i - 1;
            END;

            Indentation := i;
            MODIFY;

            IF "Account Type" = "Account Type"::"Begin-Total" THEN BEGIN
              i := i + 1;
              AccNo[i] := "No.";
            END;
          UNTIL NEXT = 0;
      Window.CLOSE;
    END;

    PROCEDURE "> NAVBR"@1102300001();
    BEGIN
    END;

    PROCEDURE IdentBR@1102300000();
    VAR
      X@35000001 : Integer;
      AuxSTR@35000000 : Text[30];
    BEGIN
      Window.OPEN(Text004);

      WITH GLAcc DO
        IF FIND('-') THEN
          REPEAT
            Window.UPDATE(1,"No.");

            IF "Account Type" = "Account Type"::"End-Total" THEN BEGIN
              IF i < 1 THEN
                ERROR(Text005,"No.");
              Totaling := AccNo[i] + '..' + "No.";
              i := i - 1;
            END;

            Indentation := i;
            MODIFY;

            IF "Account Type" = "Account Type"::"Begin-Total" THEN BEGIN
              i := i + 1;
              AccNo[i] := "No.";
            END;

            IF "Account Type" = "Account Type"::Total THEN BEGIN
              Totaling := "No." + '..' + PADSTR("No.",20,'9');

              Indentation := STRLEN("No.") - 1;


              MODIFY;
              i := STRLEN("No.") + 1;
            END;

            IF "Account Type" = "Account Type"::Posting THEN BEGIN
              Indentation := i;

              MODIFY;
            END;
          UNTIL NEXT = 0;

      Window.CLOSE;
    END;

    PROCEDURE BrazilianFunctionality@1102300002() : Boolean;
    VAR
      glAcct@1102300001 : Record 15;
    BEGIN
      glAcct.SETFILTER("Account Type", '%1|%2|%3', glAcct."Account Type"::Heading,
                                                   glAcct."Account Type"::"Begin-Total",
                                                   glAcct."Account Type"::"End-Total");
      IF glAcct.COUNT > 0 THEN
        EXIT(FALSE)
      ELSE
        EXIT(TRUE);
    END;

    BEGIN
    END.
  }
}

