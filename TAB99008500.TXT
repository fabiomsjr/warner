OBJECT Table 99008500 Inbound Sales Document Header
{
  OBJECT-PROPERTIES
  {
    Date=29/02/12;
    Time=18:14:20;
    Version List=NAVW16.00;
  }
  PROPERTIES
  {
    DataCaptionFields=BizTalk Document Type;
    OnInsert=BEGIN
               InboundSalesDocumentHeader.RESET;
               IF InboundSalesDocumentHeader.FIND('+') THEN
                 "Inbound Document No." := InboundSalesDocumentHeader."Inbound Document No." + 1
               ELSE
                 "Inbound Document No." := 1;
             END;

    OnDelete=BEGIN
               InboundSalesDocumentLine.LOCKTABLE;
               InboundSalesDocumentLine.SETRANGE("Inbound Document No.","Inbound Document No.");
               InboundSalesDocumentLine.DELETEALL(TRUE);
             END;

    CaptionML=[ENU=Inbound Sales Document Header;
               PTB=Cab. Documento Venda Entrada];
    LookupPageID=Page99008508;
  }
  FIELDS
  {
    { 1   ;   ;Document Type       ;Option        ;CaptionML=[ENU=Document Type;
                                                              PTB=Tipo Documento];
                                                   OptionCaptionML=[ENU=Quote,Order,Invoice,Credit Memo,Blanket Order,Return Order;
                                                                    PTB=Cota��o,Pedido,Nota Fiscal,Nota Cr�dito,Ordem Cobertura,Ordem de Retorno];
                                                   OptionString=Quote,Order,Invoice,Credit Memo,Blanket Order,Return Order }
    { 2   ;   ;Sell-to Customer No.;Code20        ;CaptionML=[ENU=Sell-to Customer No.;
                                                              PTB=Venda-a N� Cliente] }
    { 3   ;   ;No.                 ;Code20        ;CaptionML=[ENU=No.;
                                                              PTB=N�] }
    { 4   ;   ;Bill-to Customer No.;Code20        ;CaptionML=[ENU=Bill-to Customer No.;
                                                              PTB=N.Fiscal-a N� Cliente];
                                                   NotBlank=Yes }
    { 5   ;   ;Bill-to Name        ;Text50        ;CaptionML=[ENU=Bill-to Name;
                                                              PTB=N.Fiscal-a Nome] }
    { 6   ;   ;Bill-to Name 2      ;Text50        ;CaptionML=[ENU=Bill-to Name 2;
                                                              PTB=N.Fiscal-a Nome 2] }
    { 7   ;   ;Bill-to Address     ;Text50        ;CaptionML=[ENU=Bill-to Address;
                                                              PTB=N.Fiscal-a Endere�o] }
    { 8   ;   ;Bill-to Address 2   ;Text50        ;CaptionML=[ENU=Bill-to Address 2;
                                                              PTB=N.Fiscal-a Endere�o Complementar] }
    { 9   ;   ;Bill-to City        ;Text30        ;CaptionML=[ENU=Bill-to City;
                                                              PTB=N.Fiscal-a Cidade] }
    { 10  ;   ;Bill-to Contact     ;Text50        ;CaptionML=[ENU=Bill-to Contact;
                                                              PTB=N.Fiscal-a Contato] }
    { 11  ;   ;Your Reference      ;Text30        ;CaptionML=[ENU=Your Reference;
                                                              PTB=Sua Refer�ncia] }
    { 12  ;   ;Ship-to Code        ;Code10        ;CaptionML=[ENU=Ship-to Code;
                                                              PTB=Cod. Endere�o Envio] }
    { 13  ;   ;Ship-to Name        ;Text50        ;CaptionML=[ENU=Ship-to Name;
                                                              PTB=Envio-a Nome] }
    { 14  ;   ;Ship-to Name 2      ;Text50        ;CaptionML=[ENU=Ship-to Name 2;
                                                              PTB=Envio-a Nome 2] }
    { 15  ;   ;Ship-to Address     ;Text50        ;CaptionML=[ENU=Ship-to Address;
                                                              PTB=Envio-a Endere�o] }
    { 16  ;   ;Ship-to Address 2   ;Text50        ;CaptionML=[ENU=Ship-to Address 2;
                                                              PTB=Envio-a Endere�o Complemantar] }
    { 17  ;   ;Ship-to City        ;Text30        ;CaptionML=[ENU=Ship-to City;
                                                              PTB=Envio-a Cidade] }
    { 18  ;   ;Ship-to Contact     ;Text50        ;CaptionML=[ENU=Ship-to Contact;
                                                              PTB=Envio-a Contato] }
    { 19  ;   ;Order Date          ;Date          ;CaptionML=[ENU=Order Date;
                                                              PTB=Data Pedido] }
    { 20  ;   ;Posting Date        ;Date          ;CaptionML=[ENU=Posting Date;
                                                              PTB=Data Registo] }
    { 21  ;   ;Shipment Date       ;Date          ;CaptionML=[ENU=Shipment Date;
                                                              PTB=Data Envio] }
    { 22  ;   ;Posting Description ;Text50        ;CaptionML=[ENU=Posting Description;
                                                              PTB=Texto Registro] }
    { 23  ;   ;Payment Terms Code  ;Code10        ;CaptionML=[ENU=Payment Terms Code;
                                                              PTB=Cod. Termos Pagto.] }
    { 24  ;   ;Due Date            ;Date          ;CaptionML=[ENU=Due Date;
                                                              PTB=Data Vencimento] }
    { 25  ;   ;Payment Discount %  ;Decimal       ;CaptionML=[ENU=Payment Discount %;
                                                              PTB=% Desconto P.P.];
                                                   DecimalPlaces=0:5 }
    { 26  ;   ;Pmt. Discount Date  ;Date          ;CaptionML=[ENU=Pmt. Discount Date;
                                                              PTB=Data Desconto Pagamento] }
    { 27  ;   ;Shipment Method Code;Code10        ;CaptionML=[ENU=Shipment Method Code;
                                                              PTB=Cod. Condi��es Envio] }
    { 28  ;   ;Location Code       ;Code10        ;CaptionML=[ENU=Location Code;
                                                              PTB=Cod. Dep�sito] }
    { 29  ;   ;Shortcut Dimension 1 Code;Code20   ;TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(1));
                                                   CaptionML=[ENU=Shortcut Dimension 1 Code;
                                                              PTB=Cod. Atalho Dimens�o 1];
                                                   CaptionClass='1,2,1' }
    { 30  ;   ;Shortcut Dimension 2 Code;Code20   ;TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(2));
                                                   CaptionML=[ENU=Shortcut Dimension 2 Code;
                                                              PTB=Cod. Atalho Dimens�o 2];
                                                   CaptionClass='1,2,2' }
    { 31  ;   ;Customer Posting Group;Code10      ;CaptionML=[ENU=Customer Posting Group;
                                                              PTB=Gr. Cont�bil Cliente];
                                                   Editable=Yes }
    { 32  ;   ;Currency Code       ;Code10        ;CaptionML=[ENU=Currency Code;
                                                              PTB=Cod. Moeda] }
    { 33  ;   ;Currency Factor     ;Decimal       ;CaptionML=[ENU=Currency Factor;
                                                              PTB=Fator Moeda];
                                                   DecimalPlaces=0:15;
                                                   MinValue=0;
                                                   Editable=Yes }
    { 34  ;   ;Price Group Code    ;Code10        ;CaptionML=[ENU=Price Group Code;
                                                              PTB=C�digo Pre�os] }
    { 35  ;   ;Prices Including VAT;Boolean       ;CaptionML=[ENU=Prices Including VAT;
                                                              PTB=Pre�os Brutos] }
    { 36  ;   ;Allow Quantity Disc.;Boolean       ;CaptionML=[ENU=Allow Quantity Disc.;
                                                              PTB=Permite Desc. Qtd.] }
    { 37  ;   ;Invoice Disc. Code  ;Code20        ;CaptionML=[ENU=Invoice Disc. Code;
                                                              PTB=Cod. Desconto N.Fiscal] }
    { 40  ;   ;Cust./Item Disc. Gr.;Code10        ;CaptionML=[ENU=Cust./Item Disc. Gr.;
                                                              PTB=Cod. Desc. Cliente/Prod.] }
    { 41  ;   ;Language Code       ;Code10        ;CaptionML=[ENU=Language Code;
                                                              PTB=Cod. Idioma] }
    { 43  ;   ;Salesperson Code    ;Code10        ;CaptionML=[ENU=Salesperson Code;
                                                              PTB=Cod. Vendedor] }
    { 45  ;   ;Order Class         ;Code10        ;CaptionML=[ENU=Order Class;
                                                              PTB=Classe Pedido] }
    { 46  ;   ;Comment             ;Boolean       ;FieldClass=Normal;
                                                   CaptionML=[ENU=Comment;
                                                              PTB=Coment�rio];
                                                   Editable=Yes }
    { 47  ;   ;No. Printed         ;Integer       ;CaptionML=[ENU=No. Printed;
                                                              PTB=N� Impress�es];
                                                   Editable=Yes }
    { 51  ;   ;On Hold             ;Code3         ;CaptionML=[ENU=On Hold;
                                                              PTB=Retido] }
    { 52  ;   ;Applies-to Doc. Type;Option        ;CaptionML=[ENU=Applies-to Doc. Type;
                                                              PTB=Aplicado por Tipo Doc.];
                                                   OptionCaptionML=[ENU=" ,Payment,Invoice,Credit Memo,Finance Charge Memo,Reminder,Refund";
                                                                    PTB=" ,Pagamento,Nota Fiscal,Nota de Cr�dito,Nota de Encargo Financeiro,Lembrete,Reembolso"];
                                                   OptionString=[ ,Payment,Invoice,Credit Memo,Finance Charge Memo,Reminder,Refund] }
    { 53  ;   ;Applies-to Doc. No. ;Code20        ;CaptionML=[ENU=Applies-to Doc. No.;
                                                              PTB=Aplicado por N� Doc.] }
    { 55  ;   ;Bal. Account No.    ;Code20        ;CaptionML=[ENU=Bal. Account No.;
                                                              PTB=N� Conta Contrap.] }
    { 57  ;   ;Ship                ;Boolean       ;CaptionML=[ENU=Ship;
                                                              PTB=Enviar] }
    { 58  ;   ;Invoice             ;Boolean       ;CaptionML=[ENU=Invoice;
                                                              PTB=Proforma Invoice] }
    { 60  ;   ;Amount              ;Decimal       ;CaptionML=[ENU=Amount;
                                                              PTB=Valor];
                                                   Editable=Yes;
                                                   AutoFormatType=1 }
    { 61  ;   ;Amount Including VAT;Decimal       ;CaptionML=[ENU=Amount Including VAT;
                                                              PTB=Valor Bruto];
                                                   Editable=Yes;
                                                   AutoFormatType=1 }
    { 62  ;   ;Shipping No.        ;Code20        ;CaptionML=[ENU=Shipping No.;
                                                              PTB=N� Remessa Venda] }
    { 63  ;   ;Posting No.         ;Code20        ;CaptionML=[ENU=Posting No.;
                                                              PTB=N� Registro] }
    { 64  ;   ;Last Shipping No.   ;Code20        ;CaptionML=[ENU=Last Shipping No.;
                                                              PTB=�ltimo N� Remessa Venda];
                                                   Editable=Yes }
    { 65  ;   ;Last Posting No.    ;Code20        ;CaptionML=[ENU=Last Posting No.;
                                                              PTB=�ltimo N� N.Fiscal];
                                                   Editable=Yes }
    { 70  ;   ;VAT Registration No.;Text20        ;CaptionML=[ENU=VAT Registration No.;
                                                              PTB=N� Contribuinte] }
    { 71  ;   ;Combine Shipments   ;Boolean       ;CaptionML=[ENU=Combine Shipments;
                                                              PTB=Envios Combinados] }
    { 73  ;   ;Reason Code         ;Code10        ;CaptionML=[ENU=Reason Code;
                                                              PTB=Cod. Raz�o] }
    { 74  ;   ;Gen. Bus. Posting Group;Code10     ;CaptionML=[ENU=Gen. Bus. Posting Group;
                                                              PTB=Gr. Cont�bil Neg�cio] }
    { 75  ;   ;EU 3-Party Trade    ;Boolean       ;CaptionML=[ENU=EU 3-Party Trade;
                                                              PTB=Opera��o Triangular] }
    { 76  ;   ;Transaction Type    ;Code10        ;CaptionML=[ENU=Transaction Type;
                                                              PTB=Natureza Transa��o] }
    { 77  ;   ;Transport Method    ;Code10        ;CaptionML=[ENU=Transport Method;
                                                              PTB=Modo Transporte] }
    { 78  ;   ;VAT Country/Region Code;Code10     ;CaptionML=[ENU=VAT Country/Region Code;
                                                              PTB=C�digo Imposto Pa�s/Regi�o] }
    { 79  ;   ;Sell-to Customer Name;Text50       ;CaptionML=[ENU=Sell-to Customer Name;
                                                              PTB=Venda-a Nome Cliente] }
    { 80  ;   ;Sell-to Customer Name 2;Text50     ;CaptionML=[ENU=Sell-to Customer Name 2;
                                                              PTB=Venda-a Nome Cliente 2] }
    { 81  ;   ;Sell-to Address     ;Text50        ;CaptionML=[ENU=Sell-to Address;
                                                              PTB=Venda-a Endere�o] }
    { 82  ;   ;Sell-to Address 2   ;Text50        ;CaptionML=[ENU=Sell-to Address 2;
                                                              PTB=Venda-a Endere�o Complementar] }
    { 83  ;   ;Sell-to City        ;Text30        ;CaptionML=[ENU=Sell-to City;
                                                              PTB=Venda-a Cidade] }
    { 84  ;   ;Sell-to Contact     ;Text50        ;CaptionML=[ENU=Sell-to Contact;
                                                              PTB=Venda-a Contato] }
    { 85  ;   ;Bill-to Post Code   ;Code20        ;ValidateTableRelation=No;
                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=Bill-to Post Code;
                                                              PTB=N.Fiscal-a CEP] }
    { 86  ;   ;Bill-to County      ;Text30        ;CaptionML=[ENU=Bill-to County;
                                                              PTB=N.Fiscal-a Distrito] }
    { 87  ;   ;Bill-to Country/Region Code;Code10 ;CaptionML=[ENU=Bill-to Country/Region Code;
                                                              PTB=Faturar a C�d. Pa�s/Regi�o] }
    { 88  ;   ;Sell-to Post Code   ;Code20        ;ValidateTableRelation=No;
                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=Sell-to Post Code;
                                                              PTB=Venda-a -CEP] }
    { 89  ;   ;Sell-to County      ;Text30        ;CaptionML=[ENU=Sell-to County;
                                                              PTB=Venda a-Pa�s] }
    { 90  ;   ;Sell-to Country/Region Code;Code10 ;CaptionML=[ENU=Sell-to Country/Region Code;
                                                              PTB=C�d. Enviar - para Pa�s/Regi�o] }
    { 91  ;   ;Ship-to Post Code   ;Code20        ;ValidateTableRelation=No;
                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=Ship-to Post Code;
                                                              PTB=Envio-a CEP] }
    { 92  ;   ;Ship-to County      ;Text30        ;CaptionML=[ENU=Ship-to County;
                                                              PTB=Envio-a Distrito] }
    { 93  ;   ;Ship-to Country/Region Code;Code10 ;CaptionML=[ENU=Ship-to Country/Region Code;
                                                              PTB=C�d. Enviar - para Pa�s/Regi�o] }
    { 94  ;   ;Bal. Account Type   ;Option        ;CaptionML=[ENU=Bal. Account Type;
                                                              PTB=Tipo Contrapartida];
                                                   OptionCaptionML=[ENU=G/L Account,Bank Account;
                                                                    PTB=Conta,Banco];
                                                   OptionString=G/L Account,Bank Account }
    { 97  ;   ;Exit Point          ;Code10        ;CaptionML=[ENU=Exit Point;
                                                              PTB=Ponto Sa�da] }
    { 98  ;   ;Correction          ;Boolean       ;CaptionML=[ENU=Correction;
                                                              PTB=Corre��o] }
    { 99  ;   ;Document Date       ;Date          ;CaptionML=[ENU=Document Date;
                                                              PTB=Data Documento] }
    { 100 ;   ;External Document No.;Code20       ;CaptionML=[ENU=External Document No.;
                                                              PTB=N� Documento Externo] }
    { 101 ;   ;Area                ;Code10        ;CaptionML=[ENU=Area;
                                                              PTB=�rea] }
    { 102 ;   ;Transaction Specification;Code10   ;CaptionML=[ENU=Transaction Specification;
                                                              PTB=Especifica�ao Transa��o] }
    { 104 ;   ;Payment Method Code ;Code10        ;CaptionML=[ENU=Payment Method Code;
                                                              PTB=Cod. Forma Pagamento] }
    { 105 ;   ;Shipping Agent Code ;Code10        ;CaptionML=[ENU=Shipping Agent Code;
                                                              PTB=Cod. Transportador] }
    { 106 ;   ;Package Tracking No.;Text30        ;CaptionML=[ENU=Package Tracking No.;
                                                              PTB=N� Rastreabilidade de Entrega] }
    { 107 ;   ;No. Series          ;Code10        ;CaptionML=[ENU=No. Series;
                                                              PTB=N� S�ries];
                                                   Editable=Yes }
    { 108 ;   ;Posting No. Series  ;Code10        ;CaptionML=[ENU=Posting No. Series;
                                                              PTB=N� S�ries Registro] }
    { 109 ;   ;Shipping No. Series ;Code10        ;CaptionML=[ENU=Shipping No. Series;
                                                              PTB=N� S�rie Remesa Venda] }
    { 114 ;   ;Tax Area Code       ;Code20        ;CaptionML=[ENU=Tax Area Code;
                                                              PTB=C�d. �rea Imposto] }
    { 115 ;   ;Tax Liable          ;Boolean       ;CaptionML=[ENU=Tax Liable;
                                                              PTB=Sujeito a Imposto] }
    { 116 ;   ;VAT Bus. Posting Group;Code10      ;CaptionML=[ENU=VAT Bus. Posting Group;
                                                              PTB=Gr. Registro Imp. Neg�cio] }
    { 117 ;   ;Reserve             ;Option        ;CaptionML=[ENU=Reserve;
                                                              PTB=Reserva];
                                                   OptionCaptionML=[ENU=Never,Optional,Always;
                                                                    PTB=Nunca,Opcional,Sempre];
                                                   OptionString=Never,Optional,Always }
    { 118 ;   ;Applies-to ID       ;Code20        ;CaptionML=[ENU=Applies-to ID;
                                                              PTB=Aplicado por ID.] }
    { 119 ;   ;VAT Base Discount % ;Decimal       ;CaptionML=[ENU=VAT Base Discount %;
                                                              PTB=% Desc. Base IPI];
                                                   DecimalPlaces=0:5;
                                                   MinValue=0;
                                                   MaxValue=100 }
    { 120 ;   ;Order No.           ;Code20        ;CaptionML=[ENU=Order No.;
                                                              PTB=N� Pedido] }
    { 121 ;   ;Order No. Series    ;Code10        ;CaptionML=[ENU=Order No. Series;
                                                              PTB=N� S�rie Pedido] }
    { 122 ;   ;User ID             ;Code20        ;CaptionML=[ENU=User ID;
                                                              PTB=ID Usu�rio] }
    { 123 ;   ;Source Code         ;Code10        ;CaptionML=[ENU=Source Code;
                                                              PTB=Cod. Origem] }
    { 124 ;   ;Pre-Assigned No. Series;Code10     ;CaptionML=[ENU=Pre-Assigned No. Series;
                                                              PTB=N� S�rie Pr�-Atribu�da] }
    { 125 ;   ;Pre-Assigned No.    ;Code20        ;CaptionML=[ENU=Pre-Assigned No.;
                                                              PTB=N� Pre-Atribu�do] }
    { 99008500;;Inbound Document No.;Integer      ;CaptionML=[ENU=Inbound Document No.;
                                                              PTB=N� Documento Entrada];
                                                   NotBlank=No }
    { 99008501;;Date Received      ;Date          ;CaptionML=[ENU=Date Received;
                                                              PTB=Data recep��o] }
    { 99008502;;Time Received      ;Time          ;CaptionML=[ENU=Time Received;
                                                              PTB=Hora Recep��o] }
    { 99008503;;Date Processed     ;Date          ;CaptionML=[ENU=Date Processed;
                                                              PTB=Data Processamento] }
    { 99008504;;Time Processed     ;Time          ;CaptionML=[ENU=Time Processed;
                                                              PTB=Hora Processamento] }
    { 99008505;;Status             ;Option        ;CaptionML=[ENU=Status;
                                                              PTB=Status];
                                                   OptionCaptionML=[ENU=Pending,Accepted,Rejected,Processing;
                                                                    PTB=Pendente,Aceite,Rejeitado,Processamento];
                                                   OptionString=Pending,Accepted,Rejected,Processing }
    { 99008506;;Tracking ID        ;Text50        ;CaptionML=[ENU=Tracking ID;
                                                              PTB=ID Rastreabilidade] }
    { 99008507;;Auto. Accept Failed;Boolean       ;CaptionML=[ENU=Auto. Accept Failed;
                                                              PTB=Aceita��o Auto. Falhada] }
    { 99008508;;BizTalk Document Type;Option      ;CaptionML=[ENU=BizTalk Document Type;
                                                              PTB=Tipo Documento BizTalk];
                                                   OptionCaptionML=[ENU=Request for Quote,Order;
                                                                    PTB=Solicitado para Cota��o,Pedido];
                                                   OptionString=Request for Quote,Order }
    { 99008509;;Customer Quote No. ;Code20        ;CaptionML=[ENU=Customer Quote No.;
                                                              PTB=N� Cota��o Cliente] }
    { 99008510;;Customer Order No. ;Code20        ;CaptionML=[ENU=Customer Order No.;
                                                              PTB=N� Pedido Cliente] }
    { 99008511;;Reference Quote No.;Code20        ;CaptionML=[ENU=Reference Quote No.;
                                                              PTB=No Cota�. Refer�ncia] }
    { 99008512;;Corresp. Accepted Document No.;Code20;
                                                   CaptionML=[ENU=Corresp. Accepted Document No.;
                                                              PTB=N� Documento Aceite Corresp.] }
    { 99008513;;Doc. Transformation Succeeded;Boolean;
                                                   CaptionML=[ENU=Doc. Transformation Succeeded;
                                                              PTB=Transforma��o Doc. c/ Sucesso] }
    { 99008514;;Document Selected  ;Boolean       ;CaptionML=[ENU=Document Selected;
                                                              PTB=Documento Selecionado] }
  }
  KEYS
  {
    {    ;Inbound Document No.                    ;Clustered=Yes }
    {    ;Status                                   }
    {    ;Tracking ID                              }
  }
  FIELDGROUPS
  {
    { 1   ;DropDown            ;Inbound Document No.,Document Type,Sell-to Customer No.,Status }
  }
  CODE
  {
    VAR
      Text000@1001 : TextConst 'ENU="The information in the document has been rejected and may no longer be valid. ";PTB=O informa��o no documento foi rejeiitada e n�o � mais v�lida.';
      Text001@1002 : TextConst 'ENU=Do you want to continue?;PTB=Deseja continuar?';
      Text002@1003 : TextConst 'ENU=The BizTalk sales document number %1 has been rejected.;PTB=O documento venda BizTalk n�mero %1 foi rejeitado.';
      InboundSalesDocumentHeader@1004 : Record 99008500;
      InboundSalesDocumentLine@1000 : Record 99008501;

    PROCEDURE AcceptSalesDocument@2();
    BEGIN
      IF Status = Status::Rejected THEN
        IF NOT CONFIRM(Text000 +
                       Text001,TRUE) THEN
          EXIT;
      CASE "BizTalk Document Type" OF
        "BizTalk Document Type"::"Request for Quote":
          CODEUNIT.RUN(99008500,Rec);
        "BizTalk Document Type"::Order:
          CODEUNIT.RUN(99008502,Rec);
      END;
    END;

    PROCEDURE RejectSalesDocument@1();
    BEGIN
      Status := Status::Rejected;
      IF MODIFY THEN
        MESSAGE(Text002,"Inbound Document No.");
    END;

    BEGIN
    END.
  }
}

