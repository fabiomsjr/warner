OBJECT Table 353 Dimension ID Buffer
{
  OBJECT-PROPERTIES
  {
    Date=05/11/08;
    Time=12:00:00;
    Version List=NAVW16.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Dimension ID Buffer;
               PTB=ID Dimens�o Buffer];
  }
  FIELDS
  {
    { 1   ;   ;Parent ID           ;Integer       ;CaptionML=[ENU=Parent ID;
                                                              PTB=ID Pai] }
    { 2   ;   ;Dimension Code      ;Code20        ;CaptionML=[ENU=Dimension Code;
                                                              PTB=C�digo Dimens�o] }
    { 3   ;   ;Dimension Value     ;Code20        ;CaptionML=[ENU=Dimension Value;
                                                              PTB=Valor Dimens�o] }
    { 4   ;   ;ID                  ;Integer       ;CaptionML=[ENU=ID;
                                                              PTB=Id.] }
  }
  KEYS
  {
    {    ;Parent ID,Dimension Code,Dimension Value;Clustered=Yes }
    {    ;ID                                       }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

