OBJECT Table 5721 Purchasing
{
  OBJECT-PROPERTIES
  {
    Date=09/09/14;
    Time=12:00:00;
    Version List=NAVW18.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Purchasing;
               PTB=Compra];
    LookupPageID=Page5729;
    DrillDownPageID=Page5729;
  }
  FIELDS
  {
    { 1   ;   ;Code                ;Code10        ;CaptionML=[ENU=Code;
                                                              PTB=C�digo];
                                                   NotBlank=Yes }
    { 2   ;   ;Description         ;Text50        ;CaptionML=[ENU=Description;
                                                              PTB=Descri��o] }
    { 3   ;   ;Drop Shipment       ;Boolean       ;OnValidate=BEGIN
                                                                IF "Special Order" AND "Drop Shipment" THEN
                                                                  ERROR(Text000);
                                                              END;

                                                   AccessByPermission=TableData 223=R;
                                                   CaptionML=[ENU=Drop Shipment;
                                                              PTB=Envio Direto] }
    { 4   ;   ;Special Order       ;Boolean       ;OnValidate=BEGIN
                                                                IF "Drop Shipment" AND "Special Order" THEN
                                                                  ERROR(Text000);
                                                              END;

                                                   CaptionML=[ENU=Special Order;
                                                              PTB=Ped. Especial] }
  }
  KEYS
  {
    {    ;Code                                    ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      Text000@1000 : TextConst 'ENU=This purchasing code may be either a Drop Ship, or a Special Order.;PTB=O c�digo compra pode ser Entrega Direta ou Ped. Especial.';

    BEGIN
    END.
  }
}

