OBJECT Table 8617 Config. Package Error
{
  OBJECT-PROPERTIES
  {
    Date=07/09/12;
    Time=12:00:00;
    Version List=NAVW17.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Config. Package Error;
               PTB=Erro Data Migra��o];
    LookupPageID=Page8616;
    DrillDownPageID=Page8616;
  }
  FIELDS
  {
    { 1   ;   ;Package Code        ;Code20        ;TableRelation="Config. Package";
                                                   CaptionML=[ENU=Package Code;
                                                              PTB=C�d. Pacote];
                                                   NotBlank=Yes }
    { 2   ;   ;Table ID            ;Integer       ;TableRelation=Object.ID WHERE (Type=CONST(Table));
                                                   CaptionML=[ENU=Table ID;
                                                              PTB=ID Tabela];
                                                   NotBlank=Yes;
                                                   Editable=No }
    { 3   ;   ;Record No.          ;Integer       ;TableRelation="Config. Package Record".No. WHERE (Table ID=FIELD(Table ID));
                                                   CaptionML=[ENU=Record No.;
                                                              PTB=N� Registro];
                                                   NotBlank=Yes;
                                                   Editable=No }
    { 4   ;   ;Field ID            ;Integer       ;CaptionML=[ENU=Field ID;
                                                              PTB=ID Campo];
                                                   NotBlank=Yes;
                                                   Editable=No }
    { 5   ;   ;Field Name          ;Text30        ;FieldClass=FlowField;
                                                   CalcFormula=Lookup(Field.FieldName WHERE (TableNo=FIELD(Table ID),
                                                                                             No.=FIELD(Field ID)));
                                                   CaptionML=[ENU=Field Name;
                                                              PTB=Nome Campo];
                                                   Editable=No }
    { 6   ;   ;Error Text          ;Text250       ;CaptionML=[ENU=Error Text;
                                                              PTB=Mensagem Erro];
                                                   Editable=No }
    { 7   ;   ;Field Caption       ;Text250       ;FieldClass=FlowField;
                                                   CalcFormula=Lookup(Field."Field Caption" WHERE (TableNo=FIELD(Table ID),
                                                                                                   No.=FIELD(Field ID)));
                                                   CaptionML=[ENU=Field Caption;
                                                              PTB=Caption Campo];
                                                   Editable=No }
    { 8   ;   ;Error Type          ;Option        ;CaptionML=[ENU=Error Type;
                                                              PTB=Tipo Erro];
                                                   OptionCaptionML=ENU=,TableRelation;
                                                   OptionString=,TableRelation }
  }
  KEYS
  {
    {    ;Package Code,Table ID,Record No.,Field ID;
                                                   Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

