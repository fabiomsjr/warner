OBJECT Page 9011 Shop Supervisor Mfg Foundation
{
  OBJECT-PROPERTIES
  {
    Date=09/09/14;
    Time=12:00:00;
    Version List=NAVW18.00,NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Role Center;
               PTB=Centro de Fun��o];
    PageType=RoleCenter;
    ActionList=ACTIONS
    {
      { 1900000006;0 ;ActionContainer;
                      ActionContainerType=Reports }
      { 43      ;1   ;Action    ;
                      CaptionML=[ENU=Production Order - &Shortage List;
                                 PTB=Ord. Prod. - Lista Car�ncias];
                      RunObject=Report 99000788;
                      Image=Report }
      { 45      ;1   ;Action    ;
                      CaptionML=[ENU=Subcontractor - Dis&patch List;
                                 PTB=Subcontratante - Prioridades];
                      RunObject=Report 99000789;
                      Image=Report }
      { 42      ;1   ;Separator  }
      { 47      ;1   ;Action    ;
                      CaptionML=[ENU=Production &Order Calculation;
                                 PTB=C�lculo - Ordem Prod.];
                      RunObject=Report 99000767;
                      Image=Report }
      { 48      ;1   ;Action    ;
                      CaptionML=[ENU=S&tatus;
                                 PTB=S&tatus];
                      RunObject=Report 706;
                      Image=Report }
      { 49      ;1   ;Action    ;
                      CaptionML=[ENU=&Item Registers - Quantity;
                                 PTB=Registros Produto - Quantidade];
                      RunObject=Report 703;
                      Image=Report }
      { 50      ;1   ;Action    ;
                      CaptionML=[ENU=Inventory Valuation &WIP;
                                 PTB=Valoriza��o Inv. - Trab.Curso];
                      RunObject=Report 5802;
                      Image=Report }
      { 1900000011;0 ;ActionContainer;
                      ActionContainerType=HomeItems }
      { 31      ;1   ;Action    ;
                      CaptionML=[ENU=Simulated Production Orders;
                                 PTB=Pedidos Produ��o Simulada];
                      RunObject=Page 9323 }
      { 32      ;1   ;Action    ;
                      CaptionML=[ENU=Planned Production Orders;
                                 PTB=Pedidos Produ��o Planejada];
                      RunObject=Page 9324 }
      { 33      ;1   ;Action    ;
                      CaptionML=[ENU=Firm Planned Production Orders;
                                 PTB=Pedidos Prod. Emp. Planejada];
                      RunObject=Page 9325 }
      { 34      ;1   ;Action    ;
                      CaptionML=[ENU=Released Production Orders;
                                 PTB=Pedidos Produ��o Liberada];
                      RunObject=Page 9326 }
      { 35      ;1   ;Action    ;
                      CaptionML=[ENU=Finished Production Orders;
                                 PTB=Pedidos Produ��o Acabada];
                      RunObject=Page 9327 }
      { 2       ;1   ;Action    ;
                      CaptionML=[ENU=Items;
                                 PTB=Produtos];
                      RunObject=Page 31;
                      Image=Item }
      { 37      ;1   ;Action    ;
                      CaptionML=[ENU=Produced;
                                 PTB=Produzido];
                      RunObject=Page 31;
                      RunPageView=WHERE(Replenishment System=CONST(Prod. Order)) }
      { 94      ;1   ;Action    ;
                      CaptionML=[ENU=Raw Materials;
                                 PTB=Mat�rias Primas];
                      RunObject=Page 31;
                      RunPageView=WHERE(Low-Level Code=FILTER(>0),
                                        Replenishment System=CONST(Purchase),
                                        Production BOM No.=FILTER(='')) }
      { 95      ;1   ;Action    ;
                      CaptionML=[ENU=Stockkeeping Units;
                                 PTB=Unidades Armazenadas];
                      RunObject=Page 5701;
                      Image=SKU }
      { 3       ;1   ;Action    ;
                      CaptionML=[ENU=Production BOM;
                                 PTB=L.M. Produ��o];
                      RunObject=Page 99000787;
                      Image=BOM }
      { 15      ;1   ;Action    ;
                      CaptionML=[ENU=Under Development;
                                 PTB=Em Desenvolvimento];
                      RunObject=Page 99000787;
                      RunPageView=WHERE(Status=CONST(Under Development)) }
      { 63      ;1   ;Action    ;
                      CaptionML=[ENU=Certified;
                                 PTB=Autorizado];
                      RunObject=Page 99000787;
                      RunPageView=WHERE(Status=CONST(Certified)) }
      { 97      ;1   ;Action    ;
                      CaptionML=[ENU=Work Centers;
                                 PTB=Centro Trabalhos];
                      RunObject=Page 99000755 }
      { 83      ;1   ;Action    ;
                      CaptionML=[ENU=Routings;
                                 PTB=Roteiros];
                      RunObject=Page 99000764 }
      { 14      ;1   ;Action    ;
                      CaptionML=[ENU=Sales Orders;
                                 PTB=Pedidos Vendas];
                      RunObject=Page 9305;
                      Image=Order }
      { 6       ;1   ;Action    ;
                      CaptionML=[ENU=Purchase Orders;
                                 PTB=Compra Pedidos];
                      RunObject=Page 9307 }
      { 106     ;1   ;Action    ;
                      CaptionML=[ENU=Transfer Orders;
                                 PTB=Ordem Transfer�ncias];
                      RunObject=Page 5742;
                      Image=Document }
      { 16      ;1   ;Action    ;
                      CaptionML=[ENU=Inventory Put-aways;
                                 PTB=Invent�rio Put-aways];
                      RunObject=Page 9315 }
      { 18      ;1   ;Action    ;
                      CaptionML=[ENU=Inventory Picks;
                                 PTB=Invent�rio Picks];
                      RunObject=Page 9316 }
      { 29      ;1   ;Action    ;
                      CaptionML=[ENU=Standard Cost Worksheets;
                                 PTB=Planilhas Custo Padr�o];
                      RunObject=Page 5840 }
      { 22      ;1   ;Action    ;
                      CaptionML=[ENU=Subcontracting Worksheets;
                                 PTB=Folhas Necessidades];
                      RunObject=Page 295;
                      RunPageView=WHERE(Template Type=CONST(For. Labor),
                                        Recurring=CONST(No)) }
      { 20      ;1   ;Action    ;
                      CaptionML=[ENU=Requisition Worksheets;
                                 PTB=Planilha Requisi��o];
                      RunObject=Page 295;
                      RunPageView=WHERE(Template Type=CONST(Req.),
                                        Recurring=CONST(No)) }
      { 1900000012;0 ;ActionContainer;
                      ActionContainerType=ActivityButtons }
      { 25      ;1   ;ActionGroup;
                      CaptionML=[ENU=Journals;
                                 PTB=Journals];
                      Image=Journals }
      { 38      ;2   ;Action    ;
                      CaptionML=[ENU=Revaluation Journals;
                                 PTB=Di�rios Reavalia��o];
                      RunObject=Page 262;
                      RunPageView=WHERE(Template Type=CONST(Revaluation),
                                        Recurring=CONST(No)) }
      { 46      ;2   ;Action    ;
                      CaptionML=[ENU=Consumption Journals;
                                 PTB=Di�rios Consumo];
                      RunObject=Page 262;
                      RunPageView=WHERE(Template Type=CONST(Consumption),
                                        Recurring=CONST(No)) }
      { 51      ;2   ;Action    ;
                      CaptionML=[ENU=Output Journals;
                                 PTB=Di�rios Sa�da];
                      RunObject=Page 262;
                      RunPageView=WHERE(Template Type=CONST(Output),
                                        Recurring=CONST(No)) }
      { 52      ;2   ;Action    ;
                      CaptionML=[ENU=Recurring Consumption Journals;
                                 PTB=Di�rios Consumo Recorrente];
                      RunObject=Page 262;
                      RunPageView=WHERE(Template Type=CONST(Consumption),
                                        Recurring=CONST(Yes)) }
      { 53      ;2   ;Action    ;
                      CaptionML=[ENU=Recurring Output Journals;
                                 PTB=Di�rios Sa�da Recorrente];
                      RunObject=Page 262;
                      RunPageView=WHERE(Template Type=CONST(Output),
                                        Recurring=CONST(Yes)) }
      { 5       ;1   ;ActionGroup;
                      CaptionML=[ENU=Administration;
                                 PTB=Administra��o];
                      Image=Administration }
      { 23      ;2   ;Action    ;
                      CaptionML=[ENU=Work Shifts;
                                 PTB=Turnos Trabalho];
                      RunObject=Page 99000750 }
      { 24      ;2   ;Action    ;
                      CaptionML=[ENU=Shop Calendars;
                                 PTB=Calend�rios Loja];
                      RunObject=Page 99000751 }
      { 11      ;2   ;Action    ;
                      CaptionML=[ENU=Work Center Groups;
                                 PTB=Grupos Centro Trabalho];
                      RunObject=Page 99000758 }
      { 12      ;2   ;Action    ;
                      CaptionML=[ENU=Stop Codes;
                                 PTB=C�digos Parada];
                      RunObject=Page 99000779 }
      { 13      ;2   ;Action    ;
                      CaptionML=[ENU=Scrap Codes;
                                 PTB=C�digos Desperd�cio];
                      RunObject=Page 99000780 }
      { 17      ;2   ;Action    ;
                      CaptionML=[ENU=Standard Tasks;
                                 PTB=Tarefas Padr�o];
                      RunObject=Page 99000799 }
      { 1       ;0   ;ActionContainer;
                      ActionContainerType=NewDocumentItems }
      { 19      ;1   ;Action    ;
                      CaptionML=[ENU=Production &Order;
                                 PTB=Pedido Produ��o Planejada];
                      RunObject=Page 99000813;
                      Promoted=No;
                      Image=Order;
                      PromotedCategory=Process;
                      RunPageMode=Create }
      { 4       ;1   ;Action    ;
                      CaptionML=[ENU=P&urchase Order;
                                 PTB=Pedido Compra];
                      RunObject=Page 50;
                      Promoted=No;
                      Image=Document;
                      PromotedCategory=Process;
                      RunPageMode=Create }
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 67      ;1   ;Separator ;
                      CaptionML=[ENU=Tasks;
                                 PTB=Tarefas];
                      IsHeader=Yes }
      { 7       ;1   ;Action    ;
                      CaptionML=[ENU=Co&nsumption Journal;
                                 PTB=Di�rio Consumo];
                      RunObject=Page 99000846;
                      Image=ConsumptionJournal }
      { 8       ;1   ;Action    ;
                      CaptionML=[ENU=Output &Journal;
                                 PTB=Di�rio Sa�da];
                      RunObject=Page 99000823;
                      Image=OutputJournal }
      { 9       ;1   ;Separator  }
      { 10      ;1   ;Action    ;
                      CaptionML=[ENU=Requisition &Worksheet;
                                 PTB=Folha Necessidades];
                      RunObject=Page 291;
                      Image=Worksheet }
      { 123     ;1   ;Action    ;
                      CaptionML=[ENU=Order &Planning;
                                 PTB=Planejar Pedido];
                      RunObject=Page 5522;
                      Image=Planning }
      { 28      ;1   ;Separator  }
      { 26      ;1   ;Action    ;
                      CaptionML=[ENU=&Change Production Order Status;
                                 PTB=Status Pedido Mudan�a Prod.];
                      RunObject=Page 99000914;
                      Image=ChangeStatus }
      { 110     ;1   ;Separator ;
                      CaptionML=[ENU=Administration;
                                 PTB=Administra��o];
                      IsHeader=Yes }
      { 111     ;1   ;Action    ;
                      CaptionML=[ENU=Manu&facturing Setup;
                                 PTB=Instala��o Industrial];
                      RunObject=Page 99000768;
                      Image=ProductionSetup }
      { 89      ;1   ;Separator ;
                      CaptionML=[ENU=History;
                                 PTB=Hist�rico];
                      IsHeader=Yes }
      { 126     ;1   ;Action    ;
                      CaptionML=[ENU=Item &Tracing;
                                 PTB=Rastreabilidade de Item];
                      RunObject=Page 6520;
                      Image=ItemTracing }
      { 90      ;1   ;Action    ;
                      CaptionML=[ENU=Navi&gate;
                                 PTB=Nave&gar];
                      RunObject=Page 344;
                      Image=Navigate }
    }
  }
  CONTROLS
  {
    { 1900000008;0;Container;
                ContainerType=RoleCenterArea }

    { 1900724808;1;Group   }

    { 1907234908;2;Part   ;
                PagePartID=Page9044;
                PartType=Page }

    { 1905989608;2;Part   ;
                PagePartID=Page9152;
                PartType=Page }

    { 52006501;2;Part     ;
                Name=ApproveEntriesBR;
                PagePartID=Page52113087;
                PartType=Page }

    { 1900724708;1;Group   }

    { 21  ;2   ;Part      ;
                PagePartID=Page675;
                Visible=false;
                PartType=Page }

    { 27  ;2   ;Part      ;
                PagePartID=Page681;
                Visible=FALSE;
                PartType=Page }

    { 1903012608;2;Part   ;
                PagePartID=Page9175;
                Visible=FALSE;
                PartType=Page }

    { 1901377608;2;Part   ;
                PartType=System;
                SystemPartID=MyNotes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

