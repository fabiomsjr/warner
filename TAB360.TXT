OBJECT Table 360 Dimension Buffer
{
  OBJECT-PROPERTIES
  {
    Date=05/11/08;
    Time=12:00:00;
    Version List=NAVW16.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Dimension Buffer;
               PTB=Mem. Int. Dimens�o];
  }
  FIELDS
  {
    { 1   ;   ;Table ID            ;Integer       ;TableRelation=AllObj."Object ID" WHERE (Object Type=CONST(Table));
                                                   CaptionML=[ENU=Table ID;
                                                              PTB=N� Tabela] }
    { 2   ;   ;Entry No.           ;Integer       ;CaptionML=[ENU=Entry No.;
                                                              PTB=N� Mov.] }
    { 3   ;   ;Dimension Code      ;Code20        ;TableRelation=Dimension;
                                                   OnValidate=BEGIN
                                                                IF NOT DimMgt.CheckDim("Dimension Code") THEN
                                                                  ERROR(DimMgt.GetDimErr);
                                                              END;

                                                   CaptionML=[ENU=Dimension Code;
                                                              PTB=C�digo Dimens�o];
                                                   NotBlank=Yes }
    { 4   ;   ;Dimension Value Code;Code20        ;TableRelation="Dimension Value".Code WHERE (Dimension Code=FIELD(Dimension Code));
                                                   OnValidate=BEGIN
                                                                IF NOT DimMgt.CheckDimValue("Dimension Code","Dimension Value Code") THEN
                                                                  ERROR(DimMgt.GetDimErr);
                                                              END;

                                                   CaptionML=[ENU=Dimension Value Code;
                                                              PTB=Cod. Valor Dimens�o];
                                                   NotBlank=Yes }
    { 5   ;   ;New Dimension Value Code;Code20    ;TableRelation="Dimension Value".Code WHERE (Dimension Code=FIELD(Dimension Code));
                                                   OnValidate=BEGIN
                                                                IF NOT DimMgt.CheckDimValue("Dimension Code","New Dimension Value Code") THEN
                                                                  ERROR(DimMgt.GetDimErr);
                                                              END;

                                                   CaptionML=[ENU=New Dimension Value Code;
                                                              PTB=Novo Cod. Valor Dimens�o] }
    { 6   ;   ;Line No.            ;Integer       ;CaptionML=[ENU=Line No.;
                                                              PTB=N� Linha] }
    { 7   ;   ;No. Of Dimensions   ;Integer       ;CaptionML=[ENU=No. Of Dimensions;
                                                              PTB=No. de Dimens�es] }
  }
  KEYS
  {
    {    ;Table ID,Entry No.,Dimension Code       ;Clustered=Yes }
    {    ;No. Of Dimensions                        }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      DimMgt@1000 : Codeunit 408;

    BEGIN
    END.
  }
}

