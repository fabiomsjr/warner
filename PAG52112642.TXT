OBJECT Page 52112642 NFS-e Batch Messages
{
  OBJECT-PROPERTIES
  {
    Date=05/07/13;
    Time=09:16:05;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=NFS-e - Batch Messages;
               PTB=NFS-e - Mensagens Lote];
    SourceTable=Table52112634;
    PageType=List;
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1102300000;1;Group  ;
                GroupType=Repeater }

    { 1102300001;2;Field  ;
                SourceExpr=Message }

  }
  CODE
  {

    BEGIN
    {
      --- THBR176 ---
      rafaelr,030811,NFS-E
    }
    END.
  }
}

