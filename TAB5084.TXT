OBJECT Table 5084 Team Salesperson
{
  OBJECT-PROPERTIES
  {
    Date=05/11/08;
    Time=12:00:00;
    Version List=NAVW16.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Team Salesperson;
               PTB=Vendedor da Equipe];
  }
  FIELDS
  {
    { 1   ;   ;Team Code           ;Code10        ;TableRelation=Team;
                                                   CaptionML=[ENU=Team Code;
                                                              PTB=Cod. Equipe];
                                                   NotBlank=Yes }
    { 2   ;   ;Salesperson Code    ;Code10        ;TableRelation=Salesperson/Purchaser;
                                                   CaptionML=[ENU=Salesperson Code;
                                                              PTB=Cod. Vendedor];
                                                   NotBlank=Yes }
    { 3   ;   ;Team Name           ;Text30        ;FieldClass=FlowField;
                                                   CalcFormula=Lookup(Team.Name WHERE (Code=FIELD(Team Code)));
                                                   CaptionML=[ENU=Team Name;
                                                              PTB=Nome Equipe];
                                                   Editable=No }
    { 4   ;   ;Salesperson Name    ;Text50        ;FieldClass=FlowField;
                                                   CalcFormula=Lookup(Salesperson/Purchaser.Name WHERE (Code=FIELD(Salesperson Code)));
                                                   CaptionML=[ENU=Salesperson Name;
                                                              PTB=Nome Vendedor];
                                                   Editable=No }
  }
  KEYS
  {
    {    ;Team Code,Salesperson Code              ;Clustered=Yes }
    {    ;Salesperson Code                         }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

