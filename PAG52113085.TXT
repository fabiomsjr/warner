OBJECT Page 52113085 Document Approval Setup
{
  OBJECT-PROPERTIES
  {
    Date=05/05/15;
    Time=13:21:31;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Documents Approval Setup;
               PTB=Conf. Aprova��o Documentos];
    SourceTable=Table52113082;
    DelayedInsert=Yes;
    PageType=List;
    ActionList=ACTIONS
    {
      { 52006512;0   ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 52006511;1   ;Action    ;
                      CaptionML=[ENU=&Rules;
                                 PTB=&Regras];
                      RunObject=Page 52113086;
                      RunPageLink=Type=FIELD(Type),
                                  Document Type=FIELD(Document Type);
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=CheckRulesSyntax;
                      PromotedCategory=Process }
    }
  }
  CONTROLS
  {
    { 52006500;0;Container;
                ContainerType=ContentArea }

    { 52006501;1;Group    ;
                Name=Group;
                GroupType=Repeater }

    { 52006514;2;Field    ;
                SourceExpr=Type }

    { 52006502;2;Field    ;
                SourceExpr="Document Type" }

    { 52006503;2;Field    ;
                SourceExpr=Enabled }

    { 52006504;2;Field    ;
                SourceExpr="Dimension 1 Code" }

    { 52006505;2;Field    ;
                SourceExpr="Dimension 2 Code" }

    { 52006513;2;Field    ;
                SourceExpr="Limit Currency Code" }

    { 52006515;2;Field    ;
                SourceExpr="Include Taxes" }

    { 52006517;2;Field    ;
                SourceExpr="Addit. Approvers List Code" }

    { 52006506;2;Field    ;
                SourceExpr="Allow Approved Amount Change" }

    { 52006507;2;Field    ;
                SourceExpr="Max. Positive Amount Change" }

    { 52006508;2;Field    ;
                SourceExpr="Max. Negative Amount Change" }

    { 52006516;2;Field    ;
                SourceExpr="Limit Update to Approvers" }

    { 52006509;2;Field    ;
                SourceExpr="E-Mail Remarks Source" }

    { 52006510;2;Field    ;
                SourceExpr="Attach Files to E-Mail" }

  }
  CODE
  {

    BEGIN
    END.
  }
}

