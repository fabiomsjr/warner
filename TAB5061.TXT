OBJECT Table 5061 Rlshp. Mgt. Comment Line
{
  OBJECT-PROPERTIES
  {
    Date=07/09/12;
    Time=12:00:00;
    Version List=NAVW17.00;
  }
  PROPERTIES
  {
    OnModify=BEGIN
               "Last Date Modified" := TODAY;
             END;

    CaptionML=[ENU=Rlshp. Mgt. Comment Line;
               PTB=Linha Coment�rio Gest. Rel.];
    LookupPageID=Page5118;
    DrillDownPageID=Page5118;
  }
  FIELDS
  {
    { 1   ;   ;Table Name          ;Option        ;CaptionML=[ENU=Table Name;
                                                              PTB=Nome Tabela];
                                                   OptionCaptionML=[ENU=Contact,Campaign,To-do,Web Source,Sales Cycle,Sales Cycle Stage,Opportunity;
                                                                    PTB=Contato,Campanha,A��o Efetuar,Fonte Web,Ciclo Vendas,Est�gio Ciclo Vendas,Oportunidade];
                                                   OptionString=Contact,Campaign,To-do,Web Source,Sales Cycle,Sales Cycle Stage,Opportunity }
    { 2   ;   ;No.                 ;Code20        ;TableRelation=IF (Table Name=CONST(Contact)) Contact
                                                                 ELSE IF (Table Name=CONST(Campaign)) Campaign
                                                                 ELSE IF (Table Name=CONST(To-do)) To-do
                                                                 ELSE IF (Table Name=CONST(Web Source)) "Web Source"
                                                                 ELSE IF (Table Name=CONST(Sales Cycle)) "Sales Cycle"
                                                                 ELSE IF (Table Name=CONST(Sales Cycle Stage)) "Sales Cycle Stage"
                                                                 ELSE IF (Table Name=CONST(Opportunity)) Opportunity;
                                                   CaptionML=[ENU=No.;
                                                              PTB=N�] }
    { 3   ;   ;Sub No.             ;Integer       ;TableRelation=IF (Table Name=CONST(Sales Cycle Stage)) "Sales Cycle Stage".Stage WHERE (Sales Cycle Code=FIELD(No.));
                                                   CaptionML=[ENU=Sub No.;
                                                              PTB=Sub N�] }
    { 4   ;   ;Line No.            ;Integer       ;CaptionML=[ENU=Line No.;
                                                              PTB=N� Linha] }
    { 5   ;   ;Date                ;Date          ;CaptionML=[ENU=Date;
                                                              PTB=Data] }
    { 6   ;   ;Code                ;Code10        ;CaptionML=[ENU=Code;
                                                              PTB=C�digo] }
    { 7   ;   ;Comment             ;Text80        ;CaptionML=[ENU=Comment;
                                                              PTB=Coment�rio] }
    { 8   ;   ;Last Date Modified  ;Date          ;CaptionML=[ENU=Last Date Modified;
                                                              PTB=�ltima Data Modifica��o];
                                                   Editable=No }
  }
  KEYS
  {
    {    ;Table Name,No.,Sub No.,Line No.         ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    PROCEDURE SetUpNewLine@1();
    VAR
      RMCommentLine@1000 : Record 5061;
    BEGIN
      RMCommentLine.SETRANGE("Table Name","Table Name");
      RMCommentLine.SETRANGE("No.","No.");
      RMCommentLine.SETRANGE("Sub No.","Sub No.");
      RMCommentLine.SETRANGE(Date,WORKDATE);
      IF NOT RMCommentLine.FINDFIRST THEN
        Date := WORKDATE;
    END;

    BEGIN
    END.
  }
}

