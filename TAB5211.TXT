OBJECT Table 5211 Employment Contract
{
  OBJECT-PROPERTIES
  {
    Date=07/09/12;
    Time=12:00:00;
    Version List=NAVW17.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Employment Contract;
               PTB=Contrato trabalho];
    LookupPageID=Page5217;
    DrillDownPageID=Page5217;
  }
  FIELDS
  {
    { 1   ;   ;Code                ;Code10        ;CaptionML=[ENU=Code;
                                                              PTB=C�digo];
                                                   NotBlank=Yes }
    { 2   ;   ;Description         ;Text50        ;CaptionML=[ENU=Description;
                                                              PTB=Descri��o] }
    { 3   ;   ;No. of Contracts    ;Integer       ;FieldClass=FlowField;
                                                   CalcFormula=Count(Employee WHERE (Status=CONST(Active),
                                                                                     Emplymt. Contract Code=FIELD(Code)));
                                                   CaptionML=[ENU=No. of Contracts;
                                                              PTB=N� contratos];
                                                   Editable=No }
  }
  KEYS
  {
    {    ;Code                                    ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

