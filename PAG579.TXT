OBJECT Page 579 Post Application
{
  OBJECT-PROPERTIES
  {
    Date=10/06/15;
    Time=13:01:18;
    Version List=NAVW17.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Post Application;
               PTB=Registrar Aplica��o];
    PageType=StandardDialog;
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 3   ;1   ;Group     ;
                CaptionML=[ENU=General;
                           PTB=Geral] }

    { 4   ;2   ;Field     ;
                CaptionML=[ENU=Document No.;
                           PTB=N� Documento];
                SourceExpr=DocNo }

    { 7   ;2   ;Field     ;
                CaptionML=[ENU=Posting Date;
                           PTB=Data Registro];
                SourceExpr=PostingDate }

  }
  CODE
  {
    VAR
      DocNo@1000 : Code[20];
      PostingDate@1001 : Date;

    PROCEDURE SetValues@1(NewDocNo@1000 : Code[20];NewPostingDate@1001 : Date);
    BEGIN
      DocNo := NewDocNo;
      PostingDate := NewPostingDate;
    END;

    PROCEDURE GetValues@2(VAR NewDocNo@1000 : Code[20];VAR NewPostingDate@1001 : Date);
    BEGIN
      NewDocNo := DocNo;
      NewPostingDate := PostingDate;
    END;

    BEGIN
    END.
  }
}

