OBJECT Codeunit 52113138 Gen. Jnl.-Post Line C-Hook
{
  OBJECT-PROPERTIES
  {
    Date=11/06/15;
    Time=16:14:37;
    Version List=NAVBR8;
  }
  PROPERTIES
  {
    OnRun=BEGIN
          END;

  }
  CODE
  {

    PROCEDURE BeforePostCust@52006503(genJnlLine@52006507 : Record 81;VAR custLedgEntry@52006506 : Record 21);
    BEGIN
    END;

    PROCEDURE BeforePostVend@52006502(genJnlLine@52006507 : Record 81;VAR vendLedgEntry@52006506 : Record 25);
    BEGIN
    END;

    PROCEDURE AfterPostVend@52006509(genJnlLine@52006500 : Record 81;vend@52006502 : Record 23;vendLedgEntry@52006501 : Record 25);
    BEGIN
    END;

    PROCEDURE BeforeInsertGLEntry@52006508(genJnlLine@52006500 : Record 81;VAR glEntry@52006501 : Record 17);
    BEGIN
    END;

    PROCEDURE BeforeInsertGLRegister@52006510(VAR GLRegister@52006500 : Record 45;GLEntry@52006501 : Record 17);
    BEGIN
    END;

    PROCEDURE BeforeInsertBankAccLedgEntry@52006500(genJnlLine@52006500 : Record 81;VAR bankAccLedgEntry@52006501 : Record 271);
    BEGIN
    END;

    PROCEDURE BeforeInsertReversedGLEntry@52006516(VAR glEntry@52006500 : Record 17;reversedGLEntry@52006501 : Record 17);
    BEGIN
    END;

    PROCEDURE AfterCalcApplication@52112424(VAR NewCVLedgEntryBuf@1000000006 : Record 382;VAR OldCVLedgEntryBuf@1000000005 : Record 382;VAR GenJnlLine@1000000003 : Record 81;ApplyingDate@1000000015 : Date;AppliedAmt@52112424 : Decimal;IsCustomer@52112423 : Boolean);
    BEGIN
    END;

    PROCEDURE AfterUnapplyCustLedgEntry@1000000017(VAR dtldCustLedgEntry@1000000002 : Record 379;VAR appliedDtldCustLedgEntry@1000000003 : Record 379;VAR genJnlLine@1000000001 : Record 81;applyingDate@1000000000 : Date);
    BEGIN
    END;

    PROCEDURE AfterUnapplyVendLedgEntry@1000000001(VAR dtldVendLedgEntry@1000000002 : Record 380;VAR appliedDtldVendLedgEntry@1000000003 : Record 380;VAR genJnlLine@1000000001 : Record 81;applyingDate@1000000000 : Date);
    BEGIN
    END;

    PROCEDURE AfterInitGLEntry@52006501(genJnlLine@52006500 : Record 81;VAR glEntry@52006501 : Record 17);
    BEGIN
    END;

    PROCEDURE TransferCustLedgEntry@52006505(VAR cvLedgEntryBuf@52006502 : Record 382;VAR custLedgEntry@52006501 : Record 21;custToCV@52006500 : Boolean);
    BEGIN
    END;

    PROCEDURE TransferVendLedgEntry@52006504(VAR cvLedgEntryBuf@52006502 : Record 382;VAR vendLedgEntry@52006501 : Record 25;vendToCV@52006500 : Boolean);
    BEGIN
    END;

    BEGIN
    END.
  }
}

