OBJECT Table 281 Phys. Inventory Ledger Entry
{
  OBJECT-PROPERTIES
  {
    Date=09/09/14;
    Time=12:00:00;
    Version List=NAVW18.00.00.41779;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Phys. Inventory Ledger Entry;
               PTB=Mov. Invent�rio F�sico];
    LookupPageID=Page390;
    DrillDownPageID=Page390;
  }
  FIELDS
  {
    { 1   ;   ;Entry No.           ;Integer       ;CaptionML=[ENU=Entry No.;
                                                              PTB=N� Mov.] }
    { 2   ;   ;Item No.            ;Code20        ;TableRelation=Item;
                                                   CaptionML=[ENU=Item No.;
                                                              PTB=No. Produto] }
    { 3   ;   ;Posting Date        ;Date          ;CaptionML=[ENU=Posting Date;
                                                              PTB=Data Registo] }
    { 4   ;   ;Entry Type          ;Option        ;CaptionML=[ENU=Entry Type;
                                                              PTB=Tipo Mov.];
                                                   OptionCaptionML=[ENU=Purchase,Sale,Positive Adjmt.,Negative Adjmt.,Transfer,Consumption,Output, ,Assembly Consumption,Assembly Output;
                                                                    PTB=Compra,Venda,Ajuste Positivo,Ajuste Negativo,Transf.,Consumo,Sa�da];
                                                   OptionString=Purchase,Sale,Positive Adjmt.,Negative Adjmt.,Transfer,Consumption,Output, ,Assembly Consumption,Assembly Output }
    { 6   ;   ;Document No.        ;Code20        ;CaptionML=[ENU=Document No.;
                                                              PTB=No. Documento] }
    { 7   ;   ;Description         ;Text50        ;CaptionML=[ENU=Description;
                                                              PTB=Descri��o] }
    { 8   ;   ;Location Code       ;Code10        ;TableRelation=Location;
                                                   CaptionML=[ENU=Location Code;
                                                              PTB=Cod. Dep�sito] }
    { 9   ;   ;Inventory Posting Group;Code10     ;TableRelation="Inventory Posting Group";
                                                   CaptionML=[ENU=Inventory Posting Group;
                                                              PTB=Gr. Cont�bil Invent�rio] }
    { 12  ;   ;Quantity            ;Decimal       ;CaptionML=[ENU=Quantity;
                                                              PTB=Quantidade];
                                                   DecimalPlaces=0:5 }
    { 15  ;   ;Unit Amount         ;Decimal       ;CaptionML=[ENU=Unit Amount;
                                                              PTB=Pre�o Unit�rio];
                                                   AutoFormatType=2 }
    { 16  ;   ;Unit Cost           ;Decimal       ;CaptionML=[ENU=Unit Cost;
                                                              PTB=Custo Unit�rio];
                                                   AutoFormatType=2 }
    { 17  ;   ;Amount              ;Decimal       ;CaptionML=[ENU=Amount;
                                                              PTB=Valor];
                                                   AutoFormatType=1 }
    { 22  ;   ;Salespers./Purch. Code;Code10      ;TableRelation=Salesperson/Purchaser;
                                                   CaptionML=[ENU=Salespers./Purch. Code;
                                                              PTB=Cod. Vendedor/Comprador] }
    { 24  ;   ;User ID             ;Code50        ;TableRelation=User."User Name";
                                                   OnLookup=VAR
                                                              UserMgt@1000 : Codeunit 418;
                                                            BEGIN
                                                              UserMgt.LookupUserID("User ID");
                                                            END;

                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=User ID;
                                                              PTB=ID Usu�rio] }
    { 25  ;   ;Source Code         ;Code10        ;TableRelation="Source Code";
                                                   CaptionML=[ENU=Source Code;
                                                              PTB=Cod. Origem] }
    { 33  ;   ;Global Dimension 1 Code;Code20     ;TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(1));
                                                   CaptionML=[ENU=Global Dimension 1 Code;
                                                              PTB=Cod. Dimens�o 1 Global];
                                                   CaptionClass='1,1,1' }
    { 34  ;   ;Global Dimension 2 Code;Code20     ;TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(2));
                                                   CaptionML=[ENU=Global Dimension 2 Code;
                                                              PTB=Cod. Dimens�o 2 Global];
                                                   CaptionClass='1,1,2' }
    { 45  ;   ;Journal Batch Name  ;Code10        ;CaptionML=[ENU=Journal Batch Name;
                                                              PTB=Nome Se��o Di�rio] }
    { 46  ;   ;Reason Code         ;Code10        ;TableRelation="Reason Code";
                                                   CaptionML=[ENU=Reason Code;
                                                              PTB=Cod. Raz�o] }
    { 54  ;   ;Qty. (Calculated)   ;Decimal       ;CaptionML=[ENU=Qty. (Calculated);
                                                              PTB=Qtd. (Calculada)];
                                                   DecimalPlaces=0:5 }
    { 55  ;   ;Qty. (Phys. Inventory);Decimal     ;CaptionML=[ENU=Qty. (Phys. Inventory);
                                                              PTB=Custo Total];
                                                   DecimalPlaces=0:5 }
    { 56  ;   ;Last Item Ledger Entry No.;Integer ;TableRelation="Item Ledger Entry";
                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=Last Item Ledger Entry No.;
                                                              PTB=N� Lin. �ltimo Di�rio Prod.] }
    { 60  ;   ;Document Date       ;Date          ;CaptionML=[ENU=Document Date;
                                                              PTB=Data Documento] }
    { 61  ;   ;External Document No.;Code35       ;CaptionML=[ENU=External Document No.;
                                                              PTB=N� Documento Externo] }
    { 64  ;   ;No. Series          ;Code10        ;TableRelation="No. Series";
                                                   CaptionML=[ENU=No. Series;
                                                              PTB=N� S�ries] }
    { 480 ;   ;Dimension Set ID    ;Integer       ;TableRelation="Dimension Set Entry";
                                                   OnLookup=BEGIN
                                                              ShowDimensions;
                                                            END;

                                                   CaptionML=ENU=Dimension Set ID;
                                                   Editable=No }
    { 5402;   ;Variant Code        ;Code10        ;TableRelation="Item Variant".Code WHERE (Item No.=FIELD(Item No.));
                                                   CaptionML=[ENU=Variant Code;
                                                              PTB=C�digo Variante] }
    { 5407;   ;Unit of Measure Code;Code10        ;TableRelation="Item Unit of Measure".Code WHERE (Item No.=FIELD(Item No.));
                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=Unit of Measure Code;
                                                              PTB=Cod. Unidade Medida] }
    { 7380;   ;Phys Invt Counting Period Code;Code10;
                                                   TableRelation="Phys. Invt. Counting Period";
                                                   CaptionML=[ENU=Phys Invt Counting Period Code;
                                                              PTB=Cod. Per. Contagem Inv. F�sico];
                                                   Editable=No }
    { 7381;   ;Phys Invt Counting Period Type;Option;
                                                   CaptionML=[ENU=Phys Invt Counting Period Type;
                                                              PTB=Tipo Per. Contagem Inv. F�sico];
                                                   OptionCaptionML=[ENU=" ,Item,SKU";
                                                                    PTB=" ,Item,Unidade de Armazenamento"];
                                                   OptionString=[ ,Item,SKU];
                                                   Editable=No }
  }
  KEYS
  {
    {    ;Entry No.                               ;Clustered=Yes }
    {    ;Item No.,Variant Code,Location Code,Posting Date;
                                                   SumIndexFields=Quantity }
    {    ;Item No.,Variant Code,Global Dimension 1 Code,Global Dimension 2 Code,Location Code,Posting Date;
                                                   SumIndexFields=Quantity }
    {    ;Document No.,Posting Date                }
    {    ;Item No.,Phys Invt Counting Period Type,Posting Date }
  }
  FIELDGROUPS
  {
    { 1   ;DropDown            ;Entry No.,Item No.,Posting Date,Entry Type,Document No. }
  }
  CODE
  {
    VAR
      DimMgt@1000 : Codeunit 408;

    PROCEDURE ShowDimensions@1();
    BEGIN
      DimMgt.ShowDimensionSet("Dimension Set ID",STRSUBSTNO('%1 %2',TABLECAPTION,"Entry No."));
    END;

    BEGIN
    END.
  }
}

