OBJECT Page 52112550 Territory Tax Settlements
{
  OBJECT-PROPERTIES
  {
    Date=13/04/15;
    Time=10:40:45;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Settlements;
               PTB=Apura��es];
    SourceTable=Table52112590;
    PageType=ListPart;
    ActionList=ACTIONS
    {
      { 52006511;    ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 52006512;1   ;Action    ;
                      Name=ActionCalculate;
                      CaptionML=[ENU=Calculate;
                                 PTB=Calcular];
                      Visible=FALSE;
                      Image=CalculateVAT;
                      OnAction=BEGIN
                                 ActionCalculate;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 52006500;0;Container;
                ContainerType=ContentArea }

    { 52006501;1;Group    ;
                Name=Group;
                GroupType=Repeater }

    { 52006502;2;Field    ;
                SourceExpr="Territory Code" }

    { 52006513;2;Field    ;
                SourceExpr="Debits Total";
                Editable=FALSE }

    { 52006514;2;Field    ;
                SourceExpr="Credits Total";
                Editable=FALSE }

    { 52006515;2;Field    ;
                SourceExpr="Returns Total";
                Visible=FALSE;
                Editable=FALSE }

    { 52006503;2;Field    ;
                SourceExpr="Others Debits" }

    { 52006504;2;Field    ;
                SourceExpr="Reversal of Credits" }

    { 52006505;2;Field    ;
                SourceExpr="Others Credits" }

    { 52006506;2;Field    ;
                SourceExpr="Reversal of Debits" }

    { 52006507;2;Field    ;
                SourceExpr=Deductions }

    { 52006508;2;Field    ;
                SourceExpr="Credit Bal. Prev. Period" }

    { 52006510;2;Field    ;
                SourceExpr="Tax Payable" }

    { 52006509;2;Field    ;
                SourceExpr="Special Debits" }

  }
  CODE
  {

    LOCAL PROCEDURE ActionCalculate@52006502();
    VAR
      TaxSettlement@52006500 : Record 52112523;
    BEGIN
      TaxSettlement.GET(GETRANGEMIN("Tax Settlement No."));
      TaxSettlement.CreateICMSSTStatements;
    END;

    BEGIN
    END.
  }
}

