OBJECT Page 181 License Agreement Accept
{
  OBJECT-PROPERTIES
  {
    Date=09/09/14;
    Time=12:00:00;
    Version List=NAVW18.00;
  }
  PROPERTIES
  {
    CaptionML=ENU=License Agreement Accept;
    InsertAllowed=No;
    DeleteAllowed=No;
    LinksAllowed=No;
    SourceTable=Table140;
    PageType=Card;
    OnOpenPage=BEGIN
                 GET;
                 OrgAccepted := Accepted;
               END;

    ActionList=ACTIONS
    {
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 11      ;1   ;Action    ;
                      CaptionML=ENU=&Accept;
                      Promoted=Yes;
                      Image=Approve;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 VALIDATE(Accepted,TRUE);
                                 CurrPage.UPDATE;
                                 // CurrForm.CLOSE;
                               END;
                                }
      { 12      ;1   ;Action    ;
                      CaptionML=ENU=Read the End User License Agreement;
                      Promoted=Yes;
                      Image=Agreement;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 ShowEULA;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                CaptionML=ENU=General }

    { 2   ;2   ;Field     ;
                CaptionClass=FORMAT("Message for Accepting User");
                Editable=FALSE;
                MultiLine=Yes }

    { 3   ;2   ;Field     ;
                CaptionClass=Text19050732 }

    { 6   ;2   ;Field     ;
                SourceExpr=Accepted;
                OnValidate=BEGIN
                             IF OrgAccepted AND NOT Accepted THEN
                               ERROR(CannotRevokeEULAErr);
                           END;
                            }

    { 8   ;2   ;Field     ;
                SourceExpr="Accepted By";
                Editable=FALSE }

    { 14  ;2   ;Field     ;
                SourceExpr="Accepted On";
                Editable=FALSE }

    { 4   ;2   ;Field     ;
                SourceExpr="Effective Date";
                Editable=FALSE }

  }
  CODE
  {
    VAR
      OrgAccepted@1000 : Boolean;
      CannotRevokeEULAErr@1001 : TextConst 'ENU=You cannot revoke the End User License Agreement accept. The accept can only be revoked by changing the Go Live Date field in the Go Live Information Setup Card.';
      Text19050732@19045109 : TextConst 'ENU=Do you accept the license terms?';

    BEGIN
    END.
  }
}

