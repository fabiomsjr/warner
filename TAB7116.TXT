OBJECT Table 7116 Analysis Column Template
{
  OBJECT-PROPERTIES
  {
    Date=07/09/12;
    Time=12:00:00;
    Version List=NAVW17.00;
  }
  PROPERTIES
  {
    DataCaptionFields=Name,Description;
    OnDelete=VAR
               ItemColumnLayout@1000 : Record 7118;
             BEGIN
               ItemColumnLayout.SETRANGE("Analysis Area","Analysis Area");
               ItemColumnLayout.SETRANGE("Analysis Column Template",Name);
               ItemColumnLayout.DELETEALL(TRUE);
             END;

    CaptionML=[ENU=Analysis Column Template;
               PTB=An�lise Modelo Coluna];
    LookupPageID=Page7113;
  }
  FIELDS
  {
    { 1   ;   ;Analysis Area       ;Option        ;CaptionML=[ENU=Analysis Area;
                                                              PTB=�rea de An�lise];
                                                   OptionCaptionML=[ENU=Sales,Purchase,Inventory;
                                                                    PTB=Vendas,Compras,Invent�rio];
                                                   OptionString=Sales,Purchase,Inventory }
    { 2   ;   ;Name                ;Code10        ;CaptionML=[ENU=Name;
                                                              PTB=Nome];
                                                   NotBlank=Yes }
    { 3   ;   ;Description         ;Text80        ;CaptionML=[ENU=Description;
                                                              PTB=Descri��o] }
  }
  KEYS
  {
    {    ;Analysis Area,Name                      ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

