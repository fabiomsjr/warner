OBJECT Table 294 Reminder Text
{
  OBJECT-PROPERTIES
  {
    Date=07/09/12;
    Time=12:00:00;
    Version List=NAVW17.00;
  }
  PROPERTIES
  {
    OnInsert=BEGIN
               ReminderLevel.GET("Reminder Terms Code","Reminder Level");
             END;

    CaptionML=[ENU=Reminder Text;
               PTB=Teste Lembrete];
    LookupPageID=Page433;
    DrillDownPageID=Page433;
  }
  FIELDS
  {
    { 1   ;   ;Reminder Terms Code ;Code10        ;TableRelation="Reminder Terms";
                                                   CaptionML=[ENU=Reminder Terms Code;
                                                              PTB=Cod. Carta Aviso];
                                                   NotBlank=Yes }
    { 2   ;   ;Reminder Level      ;Integer       ;TableRelation="Reminder Level".No. WHERE (Reminder Terms Code=FIELD(Reminder Terms Code));
                                                   CaptionML=[ENU=Reminder Level;
                                                              PTB=N�vel Carta Aviso];
                                                   MinValue=1;
                                                   NotBlank=Yes }
    { 3   ;   ;Position            ;Option        ;CaptionML=[ENU=Position;
                                                              PTB=Posi��o];
                                                   OptionCaptionML=[ENU=Beginning,Ending;
                                                                    PTB=Princ�pio,Final];
                                                   OptionString=Beginning,Ending }
    { 4   ;   ;Line No.            ;Integer       ;CaptionML=[ENU=Line No.;
                                                              PTB=N� Linha] }
    { 5   ;   ;Text                ;Text100       ;CaptionML=[ENU=Text;
                                                              PTB=Texto] }
  }
  KEYS
  {
    {    ;Reminder Terms Code,Reminder Level,Position,Line No.;
                                                   Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      ReminderLevel@1000 : Record 293;

    BEGIN
    END.
  }
}

