OBJECT Page 52112654 Transmit Serv. NFS-e Batch
{
  OBJECT-PROPERTIES
  {
    Date=18/12/14;
    Time=13:29:34;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Transmit NFS-e Batch (Service);
               PTB=NFS-e - Transmitir Lote (Servi�o)];
    SaveValues=Yes;
    InsertAllowed=No;
    DeleteAllowed=No;
    ModifyAllowed=No;
    SourceTable=Table5992;
    SourceTableView=SORTING(Service E-Invoice,Branch Code,Posting Date,No.)
                    ORDER(Descending)
                    WHERE(Service E-Invoice=CONST(Yes),
                          Credit Memos=CONST(No));
    PageType=Worksheet;
    PromotedActionCategoriesML=[ENU=New,Process,RPS,Invoice,Batch;
                                PTB=Novo,Processo,RPS,Nota Fiscal,Lote];
    ShowFilter=Yes;
    OnOpenPage=VAR
                 nfeSetup@1102300000 : Record 52112625;
               BEGIN
                 FilterRecords;
                 IF FINDFIRST THEN;
               END;

    OnAfterGetRecord=BEGIN
                       UpdateButtons;
                     END;

    OnAfterGetCurrRecord=BEGIN
                           UpdateButtons;
                         END;

    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 1102300018;1 ;ActionGroup;
                      CaptionML=[ENU=&Invoice;
                                 PTB=&Nota Fiscal] }
      { 1102300020;2 ;Action    ;
                      ShortCutKey=Shift+F7;
                      CaptionML=[ENU=Card;
                                 PTB=Cart�o];
                      RunObject=Page 5978;
                      RunPageLink=No.=FIELD(No.);
                      Promoted=Yes;
                      Image=EditLines;
                      PromotedCategory=Category4 }
      { 1102300021;2 ;Action    ;
                      ShortCutKey=F7;
                      CaptionML=[ENU=Statistics;
                                 PTB=Estat�sticas];
                      RunObject=Page 6033;
                      RunPageLink=No.=FIELD(No.);
                      Image=Statistics }
      { 52006509;2   ;Action    ;
                      CaptionML=[ENU=Co&mments;
                                 PTB=Co&ment�rios];
                      RunObject=Page 5911;
                      RunPageLink=Type=CONST(General),
                                  Table Name=CONST(Service Invoice Header),
                                  No.=FIELD(No.);
                      Image=ViewComments }
      { 52006502;2   ;Action    ;
                      CaptionML=[ENU=&Navigate;
                                 PTB=&Navegar];
                      Promoted=Yes;
                      Image=Navigate;
                      PromotedCategory=Category4;
                      OnAction=BEGIN
                                 Navigate;
                               END;
                                }
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 52006504;1   ;ActionGroup;
                      CaptionML=[ENU=Invoices;
                                 PTB=Notas Fiscais] }
      { 1102300017;2 ;Action    ;
                      CaptionML=[ENU=Transmit Pending Docs.;
                                 PTB=Transmitir Docs. Pendentes];
                      Promoted=Yes;
                      Image=TransmitElectronicDoc;
                      PromotedCategory=Process;
                      OnAction=VAR
                                 nfseWS@1102300000 : Codeunit 52112625;
                                 servInvHeader@52006500 : Record 5992;
                               BEGIN
                                 servInvHeader := Rec;
                                 servInvHeader.COPYFILTERS(Rec);
                                 servInvHeader.SETFILTER("E-Invoice Status", '<>%1', servInvHeader."E-Invoice Status"::Sucesso);
                                 servInvHeader.SETRANGE("E-Invoice Verification Code", '');

                                 IF servInvHeader.COUNT = 0 THEN
                                   ERROR(Text002);

                                 IF NOT CONFIRM(STRSUBSTNO(Text001, servInvHeader.COUNT)) THEN
                                   EXIT;

                                 nfseWS.EnviarLoteServ(servInvHeader);
                               END;
                                }
      { 1102300023;2 ;Action    ;
                      CaptionML=[ENU=Print;
                                 PTB=Imprimir];
                      Promoted=Yes;
                      Image=Invoice;
                      PromotedCategory=Report;
                      OnAction=VAR
                                 nfseWS@1102300000 : Codeunit 52112625;
                               BEGIN
                                 IF COUNT = 0 THEN
                                   ERROR(Text002);

                                 nfseWS.ImprimirRPSServ(Rec);
                               END;
                                }
      { 52006501;2   ;Action    ;
                      CaptionML=[ENU=Create New No.;
                                 PTB=Criar Novo N�];
                      Promoted=Yes;
                      Image=CreateSerialNo;
                      PromotedCategory=Report;
                      OnAction=VAR
                                 nfse@52006500 : Codeunit 52112625;
                               BEGIN
                                 IF CONFIRM(STRSUBSTNO(Text004, "No.")) THEN
                                   nfse.CriarNovoNumeroRPSServ(Rec);
                               END;
                                }
      { 52006508;1   ;ActionGroup;
                      CaptionML=[ENU=Batch;
                                 PTB=Lote];
                      ActionContainerType=NewDocumentItems }
      { 52006507;2   ;Action    ;
                      CaptionML=[ENU=Verify;
                                 PTB=Consultar];
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=Import;
                      PromotedCategory=Category5;
                      OnAction=BEGIN
                                 ActionQueryBatchStatus;
                               END;
                                }
      { 52006506;2   ;Action    ;
                      CaptionML=[ENU=Errors List;
                                 PTB=Lista Erros];
                      RunObject=Page 52112642;
                      RunPageView=WHERE(Type=CONST(Error));
                      RunPageLink=Batch No.=FIELD(E-Invoice Batch No.);
                      Promoted=Yes;
                      Enabled=ShowError;
                      Image=ErrorLog;
                      PromotedCategory=Category5 }
      { 52006505;2   ;Action    ;
                      CaptionML=[ENU=Warning List;
                                 PTB=Lista Avisos];
                      RunObject=Page 52112642;
                      RunPageView=WHERE(Type=CONST(Warning));
                      RunPageLink=Batch No.=FIELD(E-Invoice Batch No.);
                      Promoted=Yes;
                      Enabled=ShowWarn;
                      Image=ViewComments;
                      PromotedCategory=Category5 }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 52006500;1;Field    ;
                Name=BranchCodeField;
                CaptionML=[ENU=Branch Code;
                           PTB=C�d. Filial];
                SourceExpr=BranchCode;
                TableRelation="Branch Information";
                OnValidate=BEGIN
                             FilterRecords;
                           END;
                            }

    { 52006510;1;Field    ;
                Name=ShowOpenOnly;
                CaptionML=[ENU=Show Open Only;
                           PTB=Mostrar Somente Pendentes];
                SourceExpr=ShowOpenOnly;
                OnValidate=BEGIN
                             FilterOpen;
                             CurrPage.UPDATE;
                           END;
                            }

    { 1102300000;1;Group  ;
                Editable=FALSE;
                GroupType=Repeater }

    { 1102300009;2;Field  ;
                CaptionML=[ENU=RPS Sequence;
                           PTB=N� RPS];
                SourceExpr="E-Invoice RPS No." }

    { 1102300001;2;Field  ;
                SourceExpr="No." }

    { 1102300002;2;Field  ;
                SourceExpr="External Document No." }

    { 1102300003;2;Field  ;
                SourceExpr="Bill-to Customer No." }

    { 1102300005;2;Field  ;
                SourceExpr="Bill-to Name" }

    { 1102300011;2;Field  ;
                SourceExpr="Posting Date" }

    { 1102300013;2;Field  ;
                SourceExpr="Due Date" }

    { 1102300015;2;Field  ;
                SourceExpr=Amount }

    { 1102300007;2;Field  ;
                SourceExpr="E-Invoice Status" }

    { 52006503;2;Field    ;
                SourceExpr="E-Invoice Batch No." }

  }
  CODE
  {
    VAR
      Text001@1102300000 : TextConst 'PTB=Confirma que deseja enviar os %1 documentos?';
      Text002@1102300001 : TextConst 'ENU=There is nothing to be sent;PTB=N�o h� nenhuma nota fiscal pendente de envio.';
      BranchCode@1102300002 : Code[20];
      Text003@1102300003 : TextConst 'PTB=Confirma que deseja imprimir %1 RPS?';
      Text004@52006500 : TextConst 'ENU=Do you wish to create a new RPS no. for document %1?;PTB=Deseja criar um novo n� RPS para o documento %1?';
      ShowError@52006501 : Boolean;
      ShowWarn@52006502 : Boolean;
      ShowOpenOnly@52006503 : Boolean;

    PROCEDURE SetBranchCode@52006503(_branchCode@52006500 : Code[20]);
    BEGIN
      BranchCode := _branchCode;
    END;

    LOCAL PROCEDURE FilterRecords@52006504();
    VAR
      nfeSetup@52006500 : Record 52112625;
    BEGIN
      FILTERGROUP(2);
      IF nfeSetup.GET(BranchCode) THEN
        SETRANGE("Posting Date", nfeSetup."Start Date", 31129999D);
      SETRANGE("Branch Code", BranchCode);
      FILTERGROUP(0);
      FilterOpen;
      CurrPage.UPDATE;
    END;

    LOCAL PROCEDURE FilterOpen@52006510();
    BEGIN
      IF ShowOpenOnly THEN
        SETRANGE("E-Invoice Verification Code", '')
      ELSE
        SETRANGE("E-Invoice Verification Code");
    END;

    LOCAL PROCEDURE ActionQueryBatchStatus@52006500();
    VAR
      nfseBatch@52006501 : Record 52112633;
      nfseWS@52006500 : Codeunit 52112625;
      servInvHeader@52006502 : Record 5992;
    BEGIN
      CurrPage.SETSELECTIONFILTER(servInvHeader);
      servInvHeader.SETAUTOCALCFIELDS("E-Invoice Status");
      IF servInvHeader.FINDSET THEN
        REPEAT
          servInvHeader.TESTFIELD("E-Invoice Batch No.");
          IF servInvHeader."E-Invoice Status" <> servInvHeader."E-Invoice Status"::Sucesso THEN BEGIN
            nfseBatch.GET(servInvHeader."E-Invoice Batch No.", servInvHeader."Branch Code");
            nfseWS.ConsultarLote(nfseBatch);
          END;
        UNTIL servInvHeader.NEXT = 0;

      UpdateButtons;
    END;

    LOCAL PROCEDURE UpdateButtons@52006501();
    VAR
      nfseBatch@52006500 : Record 52112633;
    BEGIN
      IF "E-Invoice Batch No." <> '' THEN
        nfseBatch.GET("E-Invoice Batch No.", "Branch Code");

      nfseBatch.CALCFIELDS(Errors);
      ShowError := nfseBatch.Errors > 0;
      nfseBatch.CALCFIELDS(Warnings);
      ShowWarn := nfseBatch.Warnings > 0;
    END;

    BEGIN
    END.
  }
}

