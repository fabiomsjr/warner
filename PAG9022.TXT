OBJECT Page 9022 Small Business Role Center
{
  OBJECT-PROPERTIES
  {
    Date=09/09/14;
    Time=12:00:00;
    Version List=NAVW18.00;
  }
  PROPERTIES
  {
    CaptionML=ENU=Small Business Role Center;
    PageType=RoleCenter;
    ActionList=ACTIONS
    {
      { 1       ;    ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 4       ;1   ;ActionGroup;
                      CaptionML=ENU=New }
      { 52      ;2   ;Action    ;
                      CaptionML=ENU=Sales Quote;
                      ToolTipML=ENU=Create a new sales quote;
                      RunObject=Page 1324;
                      Image=Quote;
                      RunPageMode=Create }
      { 2       ;2   ;Action    ;
                      CaptionML=ENU=Sales Invoice;
                      ToolTipML=ENU=Create a new sales invoice.;
                      RunObject=Page 1304;
                      Image=NewInvoice;
                      RunPageMode=Create }
      { 32      ;2   ;Action    ;
                      CaptionML=ENU=Purchase Invoice;
                      ToolTipML=ENU=Create a new purchase invoice.;
                      RunObject=Page 1354;
                      Image=NewInvoice;
                      RunPageMode=Create }
      { 58      ;1   ;ActionGroup;
                      CaptionML=ENU=Payments }
      { 59      ;2   ;Action    ;
                      CaptionML=ENU=Payment Registration;
                      ToolTipML=ENU=Process your customer's payments by matching amounts received on your bank account with the related unpaid sales invoices, and then post and apply the payments to your books.;
                      RunObject=Page 981;
                      Image=Payment }
      { 21      ;1   ;ActionGroup;
                      CaptionML=ENU=Setup;
                      Image=Setup }
      { 27      ;2   ;ActionGroup;
                      CaptionML=ENU=Setup;
                      Image=Setup }
      { 49      ;3   ;Action    ;
                      CaptionML=ENU=Company Information;
                      ToolTipML=ENU=Enter the company name, address, and bank information that will be inserted on your business documents.;
                      RunObject=Page 1352;
                      Image=CompanyInformation }
      { 22      ;3   ;Action    ;
                      CaptionML=ENU=General Ledger Setup;
                      ToolTipML=ENU=Define your general accounting policies, such as the allowed posting period and how payments are processed. Set up your default dimensions for financial analysis.;
                      RunObject=Page 1348;
                      Image=JournalSetup }
      { 31      ;3   ;Action    ;
                      CaptionML=ENU=Sales & Receivables Setup;
                      ToolTipML=ENU=Define your general policies for sales invoicing and returns, such as when to show credit and stockout warnings and how to post sales discounts. Set up your number series for creating customers and different sales documents.;
                      RunObject=Page 1350;
                      Image=ReceivablesPayablesSetup }
      { 46      ;3   ;Action    ;
                      CaptionML=ENU=Purchases & Payables Setup;
                      ToolTipML=ENU=Define your general policies for purchase invoicing and returns, such as whether to require vendor invoice numbers and how to post purchase discounts. Set up your number series for creating vendors and different purchase documents.;
                      RunObject=Page 1349;
                      Image=Purchase }
      { 47      ;3   ;Action    ;
                      CaptionML=ENU=Inventory Setup;
                      ToolTipML=ENU=Define your general inventory policies, such as whether to allow negative inventory and how to post and adjust item costs. Set up your number series for creating new inventory items or services.;
                      RunObject=Page 1351;
                      Image=InventorySetup }
      { 60      ;3   ;Action    ;
                      CaptionML=ENU=Fixed Assets Setup;
                      ToolTipML=ENU=Define your accounting policies for fixed assets, such as the allowed posting period and whether to allow posting to main assets. Set up your number series for creating new fixed assets.;
                      RunObject=Page 1353;
                      Image=FixedAssets }
      { 61      ;3   ;Action    ;
                      CaptionML=ENU=Human Resources Setup;
                      ToolTipML=ENU=Set up number series for creating new employee cards and define if employment time is measured by days or hours.;
                      RunObject=Page 5233;
                      Image=HRSetup }
      { 93      ;3   ;Action    ;
                      CaptionML=ENU=Jobs Setup;
                      RunObject=Page 463;
                      Image=Job }
      { 65      ;1   ;ActionGroup;
                      Name=Getting Started;
                      CaptionML=ENU=Getting Started }
      { 54      ;2   ;Action    ;
                      CaptionML=ENU=Show/Hide Getting Started;
                      RunObject=Codeunit 1321;
                      Image=Help }
      { 8       ;0   ;ActionContainer;
                      ActionContainerType=HomeItems }
      { 14      ;1   ;Action    ;
                      CaptionML=ENU=Customers;
                      RunObject=Page 1301 }
      { 6       ;1   ;Action    ;
                      CaptionML=ENU=Vendors;
                      RunObject=Page 1331 }
      { 15      ;1   ;Action    ;
                      CaptionML=ENU=Items;
                      RunObject=Page 1303 }
      { 5       ;1   ;Action    ;
                      CaptionML=ENU=Posted Sales Invoices;
                      RunObject=Page 1309 }
      { 69      ;1   ;Action    ;
                      Name=PostedPurchaseInvoices;
                      CaptionML=ENU=Posted Purchase Invoices;
                      RunObject=Page 1359 }
      { 66      ;1   ;Action    ;
                      CaptionML=ENU=Ongoing Sales Quotes;
                      RunObject=Page 1326 }
      { 38      ;    ;ActionContainer;
                      ActionContainerType=ActivityButtons }
      { 39      ;1   ;ActionGroup;
                      CaptionML=ENU=Bookkeeping;
                      Image=Journals }
      { 40      ;2   ;Action    ;
                      CaptionML=ENU=General Journals;
                      RunObject=Page 251;
                      RunPageView=WHERE(Template Type=CONST(General),
                                        Recurring=CONST(No));
                      Image=Journal }
      { 37      ;2   ;Action    ;
                      CaptionML=ENU=Chart of Accounts;
                      RunObject=Page 16 }
      { 62      ;2   ;Action    ;
                      CaptionML=ENU=G/L Budgets;
                      RunObject=Page 121 }
      { 63      ;2   ;Action    ;
                      CaptionML=ENU=Fixed Assets;
                      RunObject=Page 5601 }
      { 64      ;2   ;Action    ;
                      CaptionML=ENU=Employees;
                      RunObject=Page 5201 }
      { 43      ;2   ;Action    ;
                      CaptionML=ENU=Cash Receipt Journals;
                      RunObject=Page 251;
                      RunPageView=WHERE(Template Type=CONST(Cash Receipts),
                                        Recurring=CONST(No));
                      Image=Journals }
      { 42      ;2   ;Action    ;
                      CaptionML=ENU=Payment Journals;
                      RunObject=Page 251;
                      RunPageView=WHERE(Template Type=CONST(Payments),
                                        Recurring=CONST(No));
                      Image=Journals }
      { 57      ;2   ;Action    ;
                      CaptionML=ENU=Sales Invoices;
                      RunObject=Page 1306 }
      { 56      ;2   ;Action    ;
                      CaptionML=ENU=Posted Sales Invoices;
                      RunObject=Page 1309 }
      { 29      ;2   ;Action    ;
                      CaptionML=ENU=Sales Credit Memos;
                      RunObject=Page 1317 }
      { 20      ;2   ;Action    ;
                      CaptionML=ENU=Posted Sales Credit Memos;
                      RunObject=Page 1321 }
      { 25      ;2   ;Action    ;
                      CaptionML=ENU=Reminders;
                      RunObject=Page 436;
                      Image=Reminder }
      { 30      ;2   ;Action    ;
                      CaptionML=ENU=Issued Reminders;
                      RunObject=Page 440;
                      Image=OrderReminder }
      { 24      ;2   ;Action    ;
                      CaptionML=ENU=Finance Charge Memos;
                      RunObject=Page 448;
                      Image=FinChargeMemo }
      { 26      ;2   ;Action    ;
                      CaptionML=ENU=Issued Finance Charge Memos;
                      RunObject=Page 452;
                      Image=PostedMemo }
      { 34      ;2   ;Action    ;
                      Name=<Page Mini Purchase Invoices>;
                      CaptionML=ENU=Purchase Invoices;
                      RunObject=Page 1356 }
      { 18      ;2   ;Action    ;
                      Name=<Page Mini Posted Purchase Invoices>;
                      CaptionML=ENU=Posted Purchase Invoices;
                      RunObject=Page 1359 }
      { 53      ;2   ;Action    ;
                      Name=<Page Mini Purchase Credit Memos>;
                      CaptionML=ENU=Purchase Credit Memos;
                      RunObject=Page 1367 }
      { 19      ;2   ;Action    ;
                      Name=<Page Mini Posted Purchase Credit Memos>;
                      CaptionML=ENU=Posted Purchase Credit Memos;
                      RunObject=Page 1371 }
      { 70      ;1   ;ActionGroup;
                      CaptionML=ENU=Analysis;
                      Image=AnalysisView }
      { 71      ;2   ;Action    ;
                      CaptionML=ENU=Account Schedules;
                      RunObject=Page 103 }
      { 72      ;2   ;Action    ;
                      CaptionML=ENU=Sales Analysis Reports;
                      RunObject=Page 9376 }
      { 73      ;2   ;Action    ;
                      CaptionML=ENU=Purchase Analysis Reports;
                      RunObject=Page 9375 }
      { 74      ;2   ;Action    ;
                      CaptionML=ENU=Inventory Analysis Reports;
                      RunObject=Page 9377 }
      { 79      ;2   ;Action    ;
                      CaptionML=ENU=VAT Reports;
                      RunObject=Page 744 }
      { 75      ;2   ;Action    ;
                      CaptionML=ENU=Cash Flow Forecasts;
                      RunObject=Page 849 }
      { 76      ;2   ;Action    ;
                      CaptionML=ENU=Chart of Cash Flow Accounts;
                      RunObject=Page 851 }
      { 77      ;2   ;Action    ;
                      CaptionML=ENU=Cash Flow Manual Revenues;
                      RunObject=Page 857 }
      { 78      ;2   ;Action    ;
                      CaptionML=ENU=Cash Flow Manual Expenses;
                      RunObject=Page 859 }
      { 23      ;1   ;ActionGroup;
                      CaptionML=ENU=Bank & Payments;
                      Image=Bank }
      { 17      ;2   ;Action    ;
                      CaptionML=ENU=Bank Accounts;
                      RunObject=Page 371;
                      Image=BankAccount }
      { 28      ;2   ;Action    ;
                      CaptionML=ENU=Bank Acc. Reconciliations;
                      RunObject=Page 388;
                      Image=BankAccountRec }
      { 33      ;2   ;Action    ;
                      CaptionML=ENU=Bank Acc. Statements;
                      RunObject=Page 389;
                      Image=BankAccountStatement }
      { 3       ;2   ;Action    ;
                      CaptionML=ENU=General Journals;
                      RunObject=Page 251;
                      RunPageView=WHERE(Template Type=CONST(General),
                                        Recurring=CONST(No));
                      Image=Journal }
      { 36      ;2   ;Action    ;
                      CaptionML=ENU=Payment Journals;
                      RunObject=Page 251;
                      RunPageView=WHERE(Template Type=CONST(Payments),
                                        Recurring=CONST(No));
                      Image=Journals }
      { 41      ;2   ;Action    ;
                      CaptionML=ENU=Cash Receipt Journals;
                      RunObject=Page 251;
                      RunPageView=WHERE(Template Type=CONST(Cash Receipts),
                                        Recurring=CONST(No));
                      Image=Journals }
      { 44      ;2   ;Action    ;
                      CaptionML=ENU=Currencies;
                      RunObject=Page 5;
                      Image=Currency }
      { 45      ;2   ;Action    ;
                      CaptionML=ENU=Direct Debit Collections;
                      RunObject=Page 1207 }
      { 67      ;1   ;ActionGroup;
                      CaptionML=ENU=Jobs;
                      Image=Job }
      { 84      ;2   ;Action    ;
                      CaptionML=ENU=Jobs;
                      RunObject=Page 89;
                      Image=Job }
      { 83      ;2   ;Action    ;
                      CaptionML=ENU=On Order;
                      RunObject=Page 89;
                      RunPageView=WHERE(Status=FILTER(Order)) }
      { 82      ;2   ;Action    ;
                      CaptionML=ENU=Planned and Quoted;
                      RunObject=Page 89;
                      RunPageView=WHERE(Status=FILTER(Quote|Planning)) }
      { 81      ;2   ;Action    ;
                      CaptionML=ENU=Completed;
                      RunObject=Page 89;
                      RunPageView=WHERE(Status=FILTER(Completed)) }
      { 68      ;2   ;Action    ;
                      CaptionML=ENU=Unassigned;
                      RunObject=Page 89;
                      RunPageView=WHERE(Person Responsible=FILTER('')) }
      { 85      ;2   ;Action    ;
                      CaptionML=ENU=Job Tasks;
                      RunObject=Page 1004 }
      { 86      ;2   ;Action    ;
                      AccessByPermission=TableData 167=R;
                      CaptionML=ENU=Sales Invoices;
                      RunObject=Page 1306 }
      { 87      ;2   ;Action    ;
                      AccessByPermission=TableData 167=R;
                      CaptionML=ENU=Sales Credit Memos;
                      RunObject=Page 1317 }
      { 95      ;2   ;Action    ;
                      CaptionML=ENU=Resources;
                      RunObject=Page 77 }
      { 94      ;2   ;Action    ;
                      CaptionML=ENU=Resource Groups;
                      RunObject=Page 72 }
      { 88      ;2   ;Action    ;
                      Name=MiniPurchasesInvoices;
                      AccessByPermission=TableData 167=R;
                      CaptionML=ENU=Purchase Invoices;
                      RunObject=Page 1356 }
      { 89      ;2   ;Action    ;
                      Name=MiniCreditMemos;
                      AccessByPermission=TableData 167=R;
                      CaptionML=ENU=Purchase Credit Memos;
                      RunObject=Page 1367 }
      { 90      ;2   ;Action    ;
                      AccessByPermission=TableData 167=R;
                      CaptionML=ENU=Items;
                      RunObject=Page 1303 }
      { 91      ;2   ;Action    ;
                      AccessByPermission=TableData 167=R;
                      CaptionML=ENU=Customers;
                      RunObject=Page 1301 }
      { 92      ;2   ;Action    ;
                      CaptionML=ENU=Time Sheets;
                      RunObject=Page 951 }
      { 35      ;1   ;ActionGroup;
                      CaptionML=ENU=VAT Reporting;
                      Image=Statistics }
      { 11      ;2   ;Action    ;
                      CaptionML=ENU=VAT Statements;
                      RunObject=Page 320 }
      { 80      ;2   ;Action    ;
                      CaptionML=ENU=Intrastat Journals;
                      RunObject=Page 327 }
      { 51      ;2   ;Action    ;
                      CaptionML=ENU=Posted Sales Invoices;
                      RunObject=Page 1309 }
      { 48      ;2   ;Action    ;
                      CaptionML=ENU=Posted Sales Credit Memos;
                      RunObject=Page 1321 }
      { 50      ;2   ;Action    ;
                      CaptionML=ENU=Posted Purchase Invoices;
                      RunObject=Page 146 }
    }
  }
  CONTROLS
  {
    { 13  ;0   ;Container ;
                ContainerType=RoleCenterArea }

    { 12  ;1   ;Group      }

    { 16  ;2   ;Part      ;
                AccessByPermission=TableData 17=R;
                PagePartID=Page1310;
                PartType=Page }

    { 7   ;2   ;Part      ;
                CaptionML=ENU=Favorite Customers;
                PagePartID=Page1374;
                PartType=Page }

    { 10  ;1   ;Group     ;
                GroupType=Group }

    { 55  ;2   ;Part      ;
                AccessByPermission=TableData 84=R;
                PagePartID=Page1390;
                PartType=Page }

    { 9   ;2   ;Part      ;
                AccessByPermission=TableData 17=R;
                PagePartID=Page1393;
                PartType=Page }

    { 96  ;2   ;Part      ;
                PagePartID=Page681;
                PartType=Page }

  }
  CODE
  {

    BEGIN
    END.
  }
}

