OBJECT Table 52112958 Detail VAT Entry
{
  OBJECT-PROPERTIES
  {
    Date=30/04/13;
    Time=16:16:03;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Detail VAT Entry;
               PTB=Movimento Detalhado de Imposto];
    LookupPageID=Page35000082;
    DrillDownPageID=Page35000082;
  }
  FIELDS
  {
    { 1   ;   ;Entry No.           ;Integer        }
    { 2   ;   ;VAT Entry No.       ;Integer        }
    { 3   ;   ;Type                ;Option        ;CaptionML=[ENU=Type;
                                                              PTB=Tipo;
                                                              ESM=Tipo;
                                                              FRC=Type;
                                                              ENC=Type];
                                                   OptionCaptionML=[ENU=" ,Purchase,Sale,Settlement";
                                                                    PTB=" ,Compra,Venda,Liquida��o";
                                                                    ESM=" ,Compra,Venta,Liquidaci�n";
                                                                    FRC=" ,Achat,Vente,R�glement";
                                                                    ENC=" ,Purchase,Sale,Settlement"];
                                                   OptionString=[ ,Purchase,Sale,Settlement];
                                                   Editable=No }
    { 4   ;   ;Document No.        ;Code20        ;CaptionML=[ENU=Document No.;
                                                              PTB=N� Documento;
                                                              ESM=N� documento;
                                                              FRC=N� de document;
                                                              ENC=Document No.];
                                                   Editable=No }
    { 5   ;   ;Document Type       ;Option        ;CaptionML=[ENU=Document Type;
                                                              PTB=Tipo Documento;
                                                              ESM=Tipo documento;
                                                              FRC=Type de document;
                                                              ENC=Document Type];
                                                   OptionCaptionML=[ENU=" ,Payment,Invoice,Credit Memo,Finance Charge Memo,Reminder,Refund";
                                                                    PTB=" ,Pagamento,N.Fiscal,Nota Cr�dito,Nota Juros,Carta Aviso,Reembolso";
                                                                    ESM=" ,Pago,Factura,Cr�dito,Docs. inter�s,Recordatorio,Reembolso";
                                                                    FRC=" ,Paiement,Facture,Note de cr�dit,Note de frais financiers,Rappel,Remboursement";
                                                                    ENC=" ,Payment,Invoice,Credit Memo,Finance Charge Memo,Reminder,Refund"];
                                                   OptionString=[ ,Payment,Invoice,Credit Memo,Finance Charge Memo,Reminder,Refund];
                                                   Editable=No }
    { 6   ;   ;Base                ;Decimal       ;CaptionML=[ENU=Base;
                                                              PTB=Base;
                                                              ESM=Base;
                                                              FRC=Base;
                                                              ENC=Base];
                                                   Editable=No;
                                                   AutoFormatType=1 }
    { 7   ;   ;Amount              ;Decimal       ;CaptionML=[ENU=Amount;
                                                              PTB=Valor;
                                                              ESM=Importe;
                                                              FRC=Montant;
                                                              ENC=Amount];
                                                   Editable=No;
                                                   AutoFormatType=1 }
    { 8   ;   ;Gen. Bus. Posting Group;Code10     ;TableRelation="Gen. Business Posting Group";
                                                   CaptionML=[ENU=Gen. Bus. Posting Group;
                                                              PTB=Gr. Cont�bil Neg�cio;
                                                              ESM=Grupo contable negocio;
                                                              FRC=Param�tre report march�;
                                                              ENC=Gen. Bus. Posting Group];
                                                   Editable=No }
    { 9   ;   ;Gen. Prod. Posting Group;Code10    ;TableRelation=IF (Invoice Line Type=FILTER(<>Fixed Asset)) "Gen. Product Posting Group"
                                                                 ELSE IF (Invoice Line Type=FILTER(Fixed Asset)) "FA Posting Group";
                                                   CaptionML=[ENU=Gen. Prod. Posting Group;
                                                              PTB=Gr. Cont�bil Produto;
                                                              ESM=Grupo contable producto;
                                                              FRC=Param�tre report produit;
                                                              ENC=Gen. Prod. Posting Group];
                                                   Editable=No }
    { 10  ;   ;Tax Jurisdiction Code;Code20       ;TableRelation="Tax Jurisdiction";
                                                   CaptionML=[ENU=Tax Jurisdiction Code;
                                                              PTB=Cod. Jurisdi��o Imposto;
                                                              ESM=C�d. jurisdicci�n impuesto;
                                                              FRC=Code de juridiction fiscale;
                                                              ENC=Tax Jurisdiction Code];
                                                   Editable=No }
    { 11  ;   ;Due Date            ;Date           }
    { 12  ;   ;Tax Identification  ;Option        ;CaptionML=[ENU=Tax Identification;
                                                              PTB=Identifica��o do imposto];
                                                   OptionCaptionML=[ENU=" ,IPI,ICMS,PIS,COFINS,II,IRRF,INSS,ISS,CSL Ret.,PIS Ret.,COFINS Ret.,ISS Ret.,INSS Ret.,Dif. Aliq. ICMS";
                                                                    PTB=" ,IPI,ICMS,PIS,COFINS,II,IRRF,INSS,ISS,CSL Ret.,PIS Ret.,COFINS Ret.,ISS Ret.,INSS Ret.,Dif. Aliq. ICMS"];
                                                   OptionString=[ ,IPI,ICMS,PIS,COFINS,II,IRRF,INSS,ISS,CSL Ret.,PIS Ret.,COFINS Ret.,ISS Ret.,INSS Ret.,Dif. Aliq. ICMS];
                                                   Editable=No }
    { 13  ;   ;Posting Date        ;Date          ;CaptionML=[ENU=Posting Date;
                                                              PTB=Data Registro;
                                                              ESM=Fecha registro;
                                                              FRC=Date de report;
                                                              ENC=Posting Date];
                                                   Editable=No }
    { 14  ;   ;Invoice Line No.    ;Integer       ;CaptionML=[ENU=Line No.;
                                                              PTB=N� Linha] }
    { 15  ;   ;Invoice Line Type   ;Option        ;CaptionML=[ENU=Type;
                                                              PTB=Tipo];
                                                   OptionCaptionML=[ENU=" ,G/L Account,Item,Resource,Fixed Asset,Charge (Item)";
                                                                    PTB=" ,Conta,Produto,Recurso,Ativo Fixo,Encargo (Prod.)";
                                                                    ESM=" ,Cuenta,Producto,Recurso,Activo fijo,Cargo (prod.)";
                                                                    FRC=" ,Compte (GL),Article,Ressource,Immobilisation,Frais annexes";
                                                                    ENC=" ,G/L Account,Item,Resource,Fixed Asset,Charge (Item)"];
                                                   OptionString=[ ,G/L Account,Item,Resource,Fixed Asset,Charge (Item)] }
    { 16  ;   ;Ledger Post         ;Boolean        }
    { 17  ;   ;Tax Invoice No.     ;Code20         }
    { 18  ;   ;Tax Invoice Type    ;Option        ;CaptionML=[ENU=Document Type;
                                                              PTB=Tipo Documento;
                                                              ESM=Tipo documento;
                                                              FRC=Type de document;
                                                              ENC=Document Type];
                                                   OptionCaptionML=[ENU=" ,Payment,Invoice,Credit Memo,Finance Charge Memo,Reminder,Refund";
                                                                    PTB=" ,Pagamento,N.Fiscal,Nota Cr�dito,Nota Juros,Carta Aviso,Reembolso";
                                                                    ESM=" ,Pago,Factura,Cr�dito,Docs. inter�s,Recordatorio,Reembolso";
                                                                    FRC=" ,Paiement,Facture,Note de cr�dit,Note de frais financiers,Rappel,Remboursement";
                                                                    ENC=" ,Payment,Invoice,Credit Memo,Finance Charge Memo,Reminder,Refund"];
                                                   OptionString=[ ,Payment,Invoice,Credit Memo,Finance Charge Memo,Reminder,Refund];
                                                   Editable=No }
    { 19  ;   ;Credit Apropriation ;Boolean        }
  }
  KEYS
  {
    {    ;Entry No.,Credit Apropriation           ;Clustered=Yes }
    {    ;Document No.,Posting Date                }
    {    ;Tax Identification,Gen. Prod. Posting Group }
    {    ;Tax Identification,Tax Jurisdiction Code }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

