OBJECT Table 5952 Resource Location
{
  OBJECT-PROPERTIES
  {
    Date=23/09/13;
    Time=12:00:00;
    Version List=NAVW17.10;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Resource Location;
               PTB=Localiza��o Recurso];
    LookupPageID=Page6015;
    DrillDownPageID=Page6015;
  }
  FIELDS
  {
    { 1   ;   ;Location Code       ;Code10        ;TableRelation=Location;
                                                   OnValidate=BEGIN
                                                                CALCFIELDS("Location Name");
                                                              END;

                                                   CaptionML=[ENU=Location Code;
                                                              PTB=Cod. Dep�sito] }
    { 2   ;   ;Location Name       ;Text50        ;FieldClass=FlowField;
                                                   CalcFormula=Lookup(Location.Name WHERE (Code=FIELD(Location Code)));
                                                   CaptionML=[ENU=Location Name;
                                                              PTB=Nome Dep�sito];
                                                   Editable=No }
    { 3   ;   ;Resource No.        ;Code20        ;TableRelation=Resource;
                                                   OnValidate=BEGIN
                                                                CALCFIELDS("Resource Name");
                                                              END;

                                                   CaptionML=[ENU=Resource No.;
                                                              PTB=N� Recurso] }
    { 4   ;   ;Resource Name       ;Text50        ;FieldClass=FlowField;
                                                   CalcFormula=Lookup(Resource.Name WHERE (No.=FIELD(Resource No.)));
                                                   CaptionML=[ENU=Resource Name;
                                                              PTB=Nome Recurso];
                                                   Editable=No }
    { 5   ;   ;Starting Date       ;Date          ;CaptionML=[ENU=Starting Date;
                                                              PTB=Data Inicial] }
  }
  KEYS
  {
    {    ;Location Code,Starting Date             ;Clustered=Yes }
    {    ;Resource No.,Starting Date               }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

