OBJECT Page 52112927 Est. Settlement Group Setup
{
  OBJECT-PROPERTIES
  {
    Date=30/04/13;
    Time=18:16:36;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Est. Settlement Group Setup;
               PTB=Config. C�d. Apura��o ICMS];
    SourceTable=Table52112961;
    PageType=Card;
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1102300000;1;Group  ;
                GroupType=Repeater }

    { 1102300001;2;Field  ;
                SourceExpr="Est. Settlement Group Code" }

    { 1102300003;2;Field  ;
                SourceExpr="Branch Code" }

    { 1102300005;2;Field  ;
                SourceExpr="Level %" }

    { 1102300007;2;Field  ;
                SourceExpr="FMPES %" }

    { 1102300009;2;Field  ;
                SourceExpr="UEA %" }

    { 1102300011;2;Field  ;
                SourceExpr="P&D %" }

  }
  CODE
  {

    BEGIN
    END.
  }
}

