OBJECT Table 8726 Mobile Customer Price
{
  OBJECT-PROPERTIES
  {
    Date=29/02/12;
    Time=18:14:17;
    Version List=NAVW16.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Mobile Customer Price;
               PTB=Pre�o Cliente Mobile];
  }
  FIELDS
  {
    { 1   ;   ;Item No.            ;Code20        ;TableRelation=Item;
                                                   CaptionML=[ENU=Item No.;
                                                              PTB=N� Item] }
    { 2   ;   ;Customer No.        ;Code20        ;TableRelation=Customer;
                                                   CaptionML=[ENU=Customer No.;
                                                              PTB=N� Cliente] }
    { 3   ;   ;Item Price          ;Decimal       ;CaptionML=[ENU=Item Price;
                                                              PTB=Pre�o Item] }
  }
  KEYS
  {
    {    ;Item No.,Customer No.                   ;Clustered=Yes }
    {    ;Customer No.                             }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

