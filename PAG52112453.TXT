OBJECT Page 52112453 Posting Preview VAT Entry
{
  OBJECT-PROPERTIES
  {
    Date=30/04/13;
    Time=10:17:41;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Tax Entries;
               PTB=Movimentos Impostos];
    InsertAllowed=No;
    DeleteAllowed=No;
    ModifyAllowed=No;
    SourceTable=Table254;
    SourceTableView=SORTING(Entry No.);
    PageType=ListPart;
    SourceTableTemporary=Yes;
  }
  CONTROLS
  {
    { 35000000;0;Container;
                ContainerType=ContentArea }

    { 35000001;1;Group    ;
                Name=Group;
                GroupType=Repeater }

    { 35000002;2;Field    ;
                SourceExpr="No." }

    { 35000003;2;Field    ;
                SourceExpr="Tax Identification" }

    { 35000004;2;Field    ;
                SourceExpr=Base }

    { 35000010;2;Field    ;
                SourceExpr="Tax %" }

    { 35000005;2;Field    ;
                SourceExpr=Amount }

    { 35000006;2;Field    ;
                SourceExpr="Exempt Basis Amount" }

    { 35000007;2;Field    ;
                SourceExpr="Others Basis Amount" }

    { 35000008;2;Field    ;
                SourceExpr="Payment/Receipt Base" }

  }
  CODE
  {

    PROCEDURE SetEntries@35000002(VAR entry@35000000 : Record 254);
    BEGIN
      RESET;
      DELETEALL;
      IF entry.FINDSET THEN
        REPEAT
          INIT;
          TRANSFERFIELDS(entry);
          INSERT;
        UNTIL entry.NEXT = 0;
      IF FINDFIRST THEN ;
      CurrPage.UPDATE(FALSE);
    END;

    BEGIN
    END.
  }
}

