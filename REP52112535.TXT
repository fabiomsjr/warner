OBJECT Report 52112535 IPI Statement SPED
{
  OBJECT-PROPERTIES
  {
    Date=30/04/13;
    Time=13:37:46;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=IPI Statement;
               PTB=Apura��o do IPI];
    ProcessingOnly=Yes;
    UseRequestPage=No;
  }
  DATASET
  {
    { 2814;    ;DataItem;TaxSettlement       ;
               DataItemTable=Table52112523;
               DataItemTableView=SORTING(No.);
               OnAfterGetRecord=BEGIN
                                  SeguirEntradas := TRUE;
                                  SeguirSalidas  := TRUE;

                                  IF TaxSettlement."Branch Code" = '' THEN BEGIN
                                    RegCompany.GET;
                                    NomeComp := RegCompany.Name + RegCompany."Name 2";
                                    CNPJComp := RegCompany."C.N.P.J.";
                                    IEComp   := RegCompany."I.E.";
                                  END ELSE BEGIN
                                    IF RegFilial.GET(TaxSettlement."Branch Code") THEN BEGIN
                                        NomeComp := RegFilial.Name + RegFilial."Name 2";
                                        CNPJComp := RegFilial."C.N.P.J.";
                                        IEComp   := RegFilial."I.E.";
                                    END;
                                  END;

                                  CodFilial := "Branch Code";

                                  DataInicial := "Start Date";
                                  DataFinal   := "End Date";

                                  Saldo       := "IPI Credit Bal. Prev. Period";

                                  Numero1 := "IPI Number 1";
                                  Numero2 := "IPI Number 2";
                                  Numero3 := "IPI Number 3";

                                  Data1  := "IPI Date 1";
                                  Data2  := "IPI Date 2";
                                  Data3  := "IPI Date 3";
                                  Data4  := "IPI Delivery Date";

                                  Orgao1 := "IPI Tax Colector 1";
                                  Orgao2 := "IPI Tax Colector 2";
                                  Orgao3 := "IPI Tax Colector 3";
                                  Orgao4 := "IPI Delivery Place";

                                  Valor1      := "IPI Amount 1";
                                  Valor2      := "IPI Amount 2";
                                  Valor3      := "IPI Amount 3";


                                  Observ01    := "IPI Observation 1";
                                  Observ02    := "IPI Observation 2";
                                  Observ03    := "IPI Observation 3";

                                  NoLivro     := "IPI Book No.";
                                  P�gina      := "IPI Page No.";

                                  IF (DataInicial = 0D) OR (DataFinal = 0D) THEN
                                    ERROR(Text35000000);
                                END;
                                 }

    { 4008;1   ;DataItem;InPut               ;
               DataItemTable=Table52112537;
               DataItemTableView=SORTING(CFOP 1st. Dig.)
                                 WHERE(CFOP 1st. Dig.=FILTER(1..3));
               OnAfterGetRecord=BEGIN
                                  TotalBase                                 += InPut."IPI Basis Amount";
                                  TotalImp1                                 += InPut."IPI Amount";
                                  TotalImp2                                 += InPut."IPI Exempt Amount";
                                  TotalImp3                                 += InPut."IPI Others Amount";
                                  TotalValor                                += InPut."G/L Amount";
                                  TotalVAlorCont[InPut."CFOP 1st. Dig."]    += InPut."G/L Amount";
                                  TotalValorBase[InPut."CFOP 1st. Dig."]    += InPut."IPI Basis Amount";
                                  TotalVAlorICMS[InPut."CFOP 1st. Dig."]    += InPut."IPI Amount";
                                  TotalValorIsentas[InPut."CFOP 1st. Dig."] += InPut."IPI Exempt Amount";
                                  TotalValorOutras[InPut."CFOP 1st. Dig."]  += InPut."IPI Others Amount";
                                END;

               OnPostDataItem=BEGIN
                                TotalEntradas := TotalImp1;
                              END;

               DataItemLink=Tax Settlement No.=FIELD(No.) }

    { 9907;1   ;DataItem;OutPut              ;
               DataItemTable=Table52112537;
               DataItemTableView=SORTING(CFOP 1st. Dig.)
                                 WHERE(CFOP 1st. Dig.=FILTER(>3));
               OnPreDataItem=BEGIN
                               TotalBase  := 0;
                               TotalImp1  := 0;
                               TotalImp2  := 0;
                               TotalImp3  := 0;
                               TotalValor := 0;
                             END;

               OnAfterGetRecord=BEGIN
                                  TotalBase   += OutPut."IPI Basis Amount";
                                  TotalImp1   += OutPut."IPI Amount";
                                  TotalImp2   += OutPut."IPI Exempt Amount";
                                  TotalImp3   += OutPut."IPI Others Amount";
                                  TotalValor  += OutPut."G/L Amount";

                                  TotalVAlorCont[OutPut."CFOP 1st. Dig."]    += OutPut."G/L Amount";
                                  TotalValorBase[OutPut."CFOP 1st. Dig."]    += OutPut."IPI Basis Amount";
                                  TotalVAlorICMS[OutPut."CFOP 1st. Dig."]    += OutPut."IPI Amount";
                                  TotalValorIsentas[OutPut."CFOP 1st. Dig."] += OutPut."IPI Exempt Amount";
                                  TotalValorOutras[OutPut."CFOP 1st. Dig."]  += OutPut."IPI Others Amount";
                                END;

               OnPostDataItem=BEGIN
                                TotalSalidas := TotalImp1;
                              END;

               DataItemLink=Tax Settlement No.=FIELD(No.) }

    { 4958;1   ;DataItem;Inteiro1            ;
               DataItemTable=Table2000000026;
               DataItemTableView=SORTING(Number)
                                 WHERE(Number=CONST(1));
               OnPreDataItem=BEGIN
                               CredPres := FALSE;
                             END;

               OnAfterGetRecord=BEGIN
                                  TaxSettlement.CALCFIELDS("IPI Others Debits","IPI Reversal of Credits","IPI Others Credits",
                                                                 "IPI Reversal of Debits","IPI Deductions");

                                  TotalDebito     := TotalSalidas + TaxSettlement."IPI Others Debits" + TaxSettlement."IPI Reversal of Credits";
                                  SubTotalCredito := TotalEntradas + TaxSettlement."IPI Others Credits" + TaxSettlement."IPI Reversal of Debits";
                                  TotalCredito    := SubTotalCredito + Saldo;

                                  IF TotalDebito - TotalCredito > 0 THEN
                                    SaldoDevedor := ABS(TotalDebito - TotalCredito)
                                  ELSE
                                    SaldoCredor := ABS(TotalDebito - TotalCredito);


                                  Imposto := SaldoDevedor - TaxSettlement."IPI Deductions";
                                  Valor1  := SaldoDevedor;
                                  P�gina  += 1;

                                  TaxSettlement."IPI Total Amount Debits":=TotalSalidas;
                                  TaxSettlement."IPI Total Amount Credits":=TotalEntradas;
                                  TaxSettlement."IPI Credit Bal. Prev Period":=Saldo;
                                  TaxSettlement."IPI Balance Cred Transport":=SaldoCredor;
                                  TaxSettlement."IPI Amount Collected":=Imposto;
                                  TaxSettlement.MODIFY;
                                END;
                                 }

  }
  REQUESTPAGE
  {
    PROPERTIES
    {
    }
    CONTROLS
    {
    }
  }
  LABELS
  {
  }
  CODE
  {
    VAR
      DataInicial@1102300000 : Date;
      DataFinal@1102300001 : Date;
      ImpFactura@1102300005 : Boolean;
      TotImpuesto@1102300013 : Decimal;
      TotBase@1102300014 : Decimal;
      SubTotBase@1102300019 : Decimal;
      SubTotImp1@1102300020 : Decimal;
      SubTotImp2@1102300021 : Decimal;
      SubTotImp3@1102300022 : Decimal;
      SubTotValor@1102300023 : Decimal;
      TotalBase@1102300024 : Decimal;
      TotalImp1@1102300025 : Decimal;
      TotalImp2@1102300026 : Decimal;
      TotalImp3@1102300027 : Decimal;
      TotalValor@1102300028 : Decimal;
      TotalEntradas@1102300029 : Decimal;
      TotalSalidas@1102300030 : Decimal;
      Saldo@1102300031 : Decimal;
      TotalDebito@1102300032 : Decimal;
      SubTotalCredito@1102300033 : Decimal;
      TotalCredito@1102300034 : Decimal;
      Imposto@1102300035 : Decimal;
      SaldoDevedor@1102300036 : Decimal;
      SaldoCredor@1102300037 : Decimal;
      SeguirEntradas@1102300039 : Boolean;
      SeguirSalidas@1102300040 : Boolean;
      Txt0021@1102300041 : Text[30];
      Txt0022@1102300042 : Text[30];
      Txt0031@1102300043 : Text[30];
      Txt0032@1102300044 : Text[30];
      Txt0061@1102300045 : Text[30];
      Txt0062@1102300046 : Text[30];
      Txt007@1102300047 : Text[30];
      Txt0121@1102300048 : Text[30];
      Txt0122@1102300049 : Text[30];
      Numero1@1102300059 : Integer;
      Numero2@1102300060 : Integer;
      Numero3@1102300061 : Integer;
      Data1@1102300062 : Date;
      Data2@1102300063 : Date;
      Data3@1102300064 : Date;
      Data4@1102300065 : Date;
      Valor1@1102300066 : Decimal;
      Valor2@1102300067 : Decimal;
      Valor3@1102300068 : Decimal;
      Orgao1@1102300069 : Text[10];
      Orgao2@1102300070 : Text[10];
      Orgao3@1102300071 : Text[10];
      Orgao4@1102300072 : Text[10];
      Observ01@1102300073 : Text[125];
      Observ02@1102300074 : Text[125];
      Observ03@1102300075 : Text[125];
      NoLivro@1102300076 : Code[10];
      P�gina@1102300077 : Integer;
      ExisteAbono@1102300081 : Boolean;
      RegCompany@1102300083 : Record 79;
      NomeComp@1102300084 : Text[100];
      CNPJComp@1102300085 : Text[20];
      IEComp@1102300086 : Text[20];
      GerarLinha@1102300088 : Boolean;
      RegFilial@1102300090 : Record 52112423;
      Text35000000@1102300092 : TextConst 'PTB=Por favor, especifique um per�odo.';
      Text35000001@1102300091 : TextConst 'PTB=Por favor, especifique N� do livro e N� da p�gina.';
      Text35000002@1102300087 : TextConst 'PTB=N�o h� registros para imprimir.';
      CodFilial@1102300096 : Code[20];
      BranchInformation@1102300097 : Record 52112423;
      ICMSDebitado@1102300099 : Decimal;
      ICMSCreditado@1102300100 : Decimal;
      VatEntry3@1102300105 : Record 254;
      CredPres@1102300106 : Boolean;
      TotalVAlorCont@1102300109 : ARRAY [10] OF Decimal;
      TotalValorBase@1102300110 : ARRAY [10] OF Decimal;
      TotalVAlorICMS@1102300111 : ARRAY [10] OF Decimal;
      TotalValorIsentas@1102300112 : ARRAY [10] OF Decimal;
      TotalValorOutras@1102300113 : ARRAY [10] OF Decimal;
      N�oGerarMovimentos@1102300002 : Boolean;

    BEGIN
    END.
  }
  RDLDATA
  {
  }
}

