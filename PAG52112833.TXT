OBJECT Page 52112833 Related Accounting Mov.
{
  OBJECT-PROPERTIES
  {
    Date=16/03/17;
    Time=16:22:16;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Related Accounting Mov.;
               PTB=Mov. Cont bil Relacionado];
    SourceTable=Table52112829;
    PageType=List;
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1102300000;1;Group  ;
                GroupType=Repeater }

    { 1102300001;2;Field  ;
                SourceExpr="Entry No." }

    { 1102300003;2;Field  ;
                SourceExpr="G/L Account No." }

    { 1102300005;2;Field  ;
                SourceExpr="Posting Date" }

    { 1102300007;2;Field  ;
                SourceExpr="Document No." }

    { 1102300009;2;Field  ;
                SourceExpr=Description }

    { 1102300013;2;Field  ;
                SourceExpr=Amount }

    { 1102300017;2;Field  ;
                SourceExpr="Debit Amount" }

    { 1102300019;2;Field  ;
                SourceExpr="Credit Amount" }

    { 52006500;2;Field    ;
                SourceExpr="Funct. Curr. Amount";
                Visible=FALSE }

    { 52006501;2;Field    ;
                SourceExpr="Funct. Curr. Debit Amount";
                Visible=FALSE }

    { 52006502;2;Field    ;
                SourceExpr="Funct. Curr. Credit Amount";
                Visible=FALSE }

    { 1102300026;2;Field  ;
                SourceExpr="Transaction Type";
                Visible=FALSE }

    { 1102300028;2;Field  ;
                SourceExpr="Transaction No.";
                Visible=FALSE }

    { 52006503;2;Field    ;
                SourceExpr="Cost Center Code";
                Visible=FALSE }

    { 52006504;2;Field    ;
                SourceExpr="ECF Dimension 1 Code";
                Visible=FALSE }

    { 52006505;2;Field    ;
                SourceExpr="ECF Dimension 2 Code";
                Visible=FALSE }

  }
  CODE
  {

    BEGIN
    END.
  }
}

