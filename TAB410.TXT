OBJECT Table 410 IC G/L Account
{
  OBJECT-PROPERTIES
  {
    Date=07/09/12;
    Time=12:00:00;
    Version List=NAVW17.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=IC G/L Account;
               PTB=IC Conta Cont�bil];
    LookupPageID=Page607;
  }
  FIELDS
  {
    { 1   ;   ;No.                 ;Code20        ;CaptionML=[ENU=No.;
                                                              PTB=N�];
                                                   NotBlank=Yes }
    { 2   ;   ;Name                ;Text50        ;CaptionML=[ENU=Name;
                                                              PTB=Nome] }
    { 3   ;   ;Account Type        ;Option        ;CaptionML=[ENU=Account Type;
                                                              PTB=Tipo Conta];
                                                   OptionCaptionML=[ENU=Posting,Heading,Total,Begin-Total,End-Total;
                                                                    PTB=Anal�tica,Cabe�alho,Sint�tica,Total-In�cio,Total-Final];
                                                   OptionString=Posting,Heading,Total,Begin-Total,End-Total }
    { 4   ;   ;Income/Balance      ;Option        ;CaptionML=[ENU=Income/Balance;
                                                              PTB=Resultado/Balan�o];
                                                   OptionCaptionML=[ENU=Income Statement,Balance Sheet;
                                                                    PTB=Resultado,Balan�o];
                                                   OptionString=Income Statement,Balance Sheet }
    { 5   ;   ;Blocked             ;Boolean       ;CaptionML=[ENU=Blocked;
                                                              PTB=Bloqueado] }
    { 6   ;   ;Map-to G/L Acc. No. ;Code20        ;TableRelation="G/L Account".No.;
                                                   CaptionML=[ENU=Map-to G/L Acc. No.;
                                                              PTB=Mapa para No. Conta Cont�bil] }
    { 7   ;   ;Indentation         ;Integer       ;CaptionML=[ENU=Indentation;
                                                              PTB=Indentar] }
  }
  KEYS
  {
    {    ;No.                                     ;Clustered=Yes }
  }
  FIELDGROUPS
  {
    { 1   ;DropDown            ;No.,Name,Income/Balance,Blocked,Map-to G/L Acc. No. }
  }
  CODE
  {

    BEGIN
    END.
  }
}

