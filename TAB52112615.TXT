OBJECT Table 52112615 CIAP Report History
{
  OBJECT-PROPERTIES
  {
    Date=29/04/15;
    Time=11:04:59;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=CIAP Report History;
               PTB=Hist�rico Relat�rio CIAP];
  }
  FIELDS
  {
    { 10  ;   ;FA No.              ;Code20        ;TableRelation="Fixed Asset".No.;
                                                   CaptionML=[ENU=AF No.;
                                                              PTB=N� AF] }
    { 20  ;   ;Description         ;Text250       ;CaptionML=[ENU=Description;
                                                              PTB=Descri��o] }
    { 30  ;   ;Final Date          ;Date          ;CaptionML=[ENU=Final Date;
                                                              PTB=Data Final] }
    { 40  ;   ;Installment         ;Text50        ;CaptionML=[ENU=Installment;
                                                              PTB=Parcela] }
    { 50  ;   ;Amount              ;Decimal       ;CaptionML=[ENU=Amount;
                                                              PTB=Valor] }
    { 60  ;   ;Short Term          ;Decimal       ;CaptionML=[ENU=Short Term;
                                                              PTB=Curto Prazo] }
    { 70  ;   ;Long Term           ;Decimal       ;CaptionML=[ENU=Long Term;
                                                              PTB=Longo Prazo] }
  }
  KEYS
  {
    {    ;FA No.                                  ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

