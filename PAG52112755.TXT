OBJECT Page 52112755 Purch. Prepayment Order Subfor
{
  OBJECT-PROPERTIES
  {
    Date=30/10/13;
    Time=09:31:56;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Lines;
               PTB=Linhas];
    MultipleNewLines=Yes;
    LinksAllowed=No;
    SourceTable=Table39;
    DelayedInsert=Yes;
    SourceTableView=WHERE(Document Type=FILTER(Order));
    PageType=ListPart;
    AutoSplitKey=Yes;
    OnNewRecord=BEGIN
                  InitType;
                END;

    OnDeleteRecord=VAR
                     ReservePurchLine@1000 : Codeunit 99000834;
                   BEGIN
                   END;

  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 52006500;2;Field    ;
                SourceExpr=Type }

    { 52006501;2;Field    ;
                SourceExpr="No." }

    { 6   ;2   ;Field     ;
                SourceExpr=Description }

    { 52006502;2;Field    ;
                SourceExpr=Quantity }

    { 12  ;2   ;Field     ;
                Name=Amt;
                CaptionML=[ENU=Amount;
                           PTB=Valor];
                BlankZero=Yes;
                SourceExpr="Direct Unit Cost";
                OnValidate=BEGIN
                             CurrPage.SAVERECORD;
                           END;
                            }

    { 52006503;2;Field    ;
                SourceExpr="Line Amount" }

    { 52006505;2;Field    ;
                SourceExpr="Line No.";
                Visible=FALSE;
                Editable=FALSE }

  }
  CODE
  {
    VAR
      AmountCaption@52006500 : TextConst 'ENU=Amount;PTB=Valor';

    BEGIN
    END.
  }
}

