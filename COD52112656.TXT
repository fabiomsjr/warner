OBJECT Codeunit 52112656 Send Email NFe
{
  OBJECT-PROPERTIES
  {
    Date=23/09/15;
    Time=14:19:57;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    OnRun=BEGIN
          END;

  }
  CODE
  {
    VAR
      MessageSentSuccessfully@52006500 : TextConst 'ENU=Mensagem enviada com sucesso.;PTB=Mensagem enviada com sucesso.';

    PROCEDURE SalesInvoiceSendEmail@1000000000(NFe@52006513 : Record 52112624);
    VAR
      SMTPMail@1000000001 : Codeunit 400;
      InvoiceNo_SalesInvoice@1000000002 : TextConst 'ENU=<p>Esta mensagem refere-se � <b>Nota Fiscal Eletr�nica de s�rie/n�mero (%1/%2)</b>, emitida em %3 para:</p>;PTB=<p>Esta mensagem refere-se � <b>Nota Fiscal Eletr�nica de s�rie/n�mero (%1/%2)</b>, emitida em %3 para:</p>';
      CustName_SalesInvoice@1000000009 : TextConst 'ENU=<p><b>Raz�o Social:</b> %1</p>;PTB=<p><b>Raz�o Social:</b> %1</p>';
      CPFCNPJ_SalesInvoice@1000000010 : TextConst 'ENU=<p><b>CPF/CNPJ:</b> %1</p><br>;PTB=<p><b>CPF/CNPJ:</b> %1</p><br>';
      SiteSefaz_SalesInvoice@1000000003 : TextConst 'ENU=<p>Para verificar a autoriza��o da SEFAZ referente � nota acima mencionada, acesse o s�tio http://www.nfe.fazenda.gov.br/portal</p>;PTB=<p>Para verificar a autoriza��o da SEFAZ referente � nota acima mencionada, acesse o s�tio http://www.nfe.fazenda.gov.br/portal</p>';
      AccessKey_SalesInvoice@1000000004 : TextConst 'ENU=<p><b>Chave de acesso:</b> %1</p>;PTB=<p><b>Chave de acesso:</b> %1</p>';
      Protocol_SalesInvoice@1000000005 : TextConst 'ENU=<p><b>Protocolo:</b> %1</p><br><br>;PTB=<p><b>Protocolo:</b> %1</p><br><br>';
      AutoMessage_SalesInvoice@1000000006 : TextConst 'ENU=<p><i>Esta mensagem foi enviada automaticamente pelo sistema</i> <b>Microsoft Dynamics NAV</b>.</p>;PTB=<p><i>Esta mensagem foi enviada automaticamente pelo sistema</i> <b>Microsoft Dynamics NAV</b>.</p>';
      MessageText@1000000007 : Text;
      SubjectText@52006502 : Text;
      Customer@52006500 : Record 18;
      SenderName@52006501 : TextConst 'ENU=Microsoft Dynamics NAV;PTB=Microsoft Dynamics NAV';
      Subject_SalesInvoice@52006503 : TextConst 'ENU=Emiss�o de Nota Fiscal Eletr�nica - NF.: %1 - %2;PTB=Emiss�o de Nota Fiscal Eletr�nica - NF.: %1 - %2';
      CompanyInfo@52006504 : Record 79;
      FileNameDanfe@52006506 : Text;
      DanfeNotFoud@52006507 : TextConst 'ENU=Arquivo DANFe n�o encontrado;PTB=Arquivo DANFe n�o encontrado';
      FileNameXML@52006508 : Text;
      XMLNotFound@52006509 : TextConst 'ENU=Arquivo XML n�o encontrado;PTB=Arquivo XML n�o encontrado';
      CancelledInvoice@52006510 : TextConst 'ENU=A Nota Fiscal %1 est� cancelada.;PTB=A Nota Fiscal %1 est� cancelada.';
      ServiceInvoice@52006511 : TextConst 'ENU=A Nota Fiscal %1 � um servi�o.;PTB=A Nota Fiscal %1 � um servi�o.';
      NotAuthorized@52006512 : TextConst 'ENU=A Nota Fiscal ainda n�o est� autorizada.;PTB=A Nota Fiscal ainda n�o est� autorizada.';
      SalesInvHeader@52006505 : Record 112;
      NFeSetup@52006514 : Record 52112625;
      TextFunctions@52006515 : Codeunit 52112453;
    BEGIN
      SalesInvHeader.GET(NFe."Document No.");

      IF Customer.GET(SalesInvHeader."Sell-to Customer No.") THEN
        Customer.TESTFIELD("E-Mail - NFe");

      MessageText := STRSUBSTNO(InvoiceNo_SalesInvoice, NFe.Serie, NFe."No.",
                                FORMAT(NFe."Sent On Date", 0, '<Day,2>/<Month,2>/<Year4>'));
      MessageText += STRSUBSTNO(CustName_SalesInvoice, Customer.Name);
      MessageText += STRSUBSTNO(CPFCNPJ_SalesInvoice, Customer."C.N.P.J./C.P.F.");
      MessageText += SiteSefaz_SalesInvoice;
      MessageText += STRSUBSTNO(AccessKey_SalesInvoice, NFe."Access Key");
      MessageText += STRSUBSTNO(Protocol_SalesInvoice, NFe.Protocolo);
      MessageText += AutoMessage_SalesInvoice;

      CompanyInfo.GET;
      SubjectText := STRSUBSTNO(Subject_SalesInvoice, NFe."No.", CompanyInfo.Name);

      SMTPMail.CreateMessage(SenderName, 'nav@nav.com.br', TextFunctions.Replace(Customer."E-Mail - NFe", ',', ';'),
                             SubjectText, MessageText, TRUE);

      IF NFeSetup.GET(SalesInvHeader."Branch Code") THEN
        IF NFeSetup."BCC E-Mail" <> '' THEN
          SMTPMail.AddBCC(NFeSetup."BCC E-Mail");

      NFe.TESTFIELD("Access Key");
      NFe.CALCFIELDS("Arquivo DANFe");
      IF NOT NFe."Arquivo DANFe".HASVALUE THEN
        ERROR(DanfeNotFoud);
      FileNameDanfe := TEMPORARYPATH + '\' + NFe."Access Key" + '-DANFe.pdf';
      SMTPMail.AddAttachment(NFe."Arquivo DANFe".EXPORT(FileNameDanfe), NFe."Access Key" + '-DANFe.pdf');

      NFe.CALCFIELDS("Arquivo XML");
      IF NOT NFe."Arquivo XML".HASVALUE THEN
        ERROR(XMLNotFound);
      FileNameXML := TEMPORARYPATH + '\' + NFe."Access Key" + '-procNFe.xml';
      SMTPMail.AddAttachment(NFe."Arquivo XML".EXPORT(FileNameXML), NFe."Access Key" + '-procNFe.xml');

      SMTPMail.Send;

      MESSAGE(MessageSentSuccessfully);
    END;

    PROCEDURE SalesCreditMemoSendEmail@1000000066(NFe@52006505 : Record 52112624);
    VAR
      SMTPMail@1000000001 : Codeunit 400;
      InvoiceNo_SalesCrMemo@1000000002 : TextConst 'ENU=<p>Esta mensagem refere-se ao cancelamento da Nota Fiscal Eletr�nica de s�rie/n�mero (%1/%2)</b>, emitida em %3 para:</p>;PTB=<p>Esta mensagem refere-se ao cancelamento da Nota Fiscal Eletr�nica de s�rie/n�mero (%1/%2)</b>, emitida em %3 para:</p>';
      CustName_SalesCrMemo@1000000009 : TextConst 'ENU=<p><b>Raz�o Social:</b> %1</p>;PTB=<p><b>Raz�o Social:</b> %1</p>';
      CPFCNPJ_SalesCrMemo@1000000010 : TextConst 'ENU=<p><b>CPF/CNPJ:</b> %1</p><br>;PTB=<p><b>CPF/CNPJ:</b> %1</p><br>';
      SiteSefaz_SalesCrMemo@1000000003 : TextConst 'ENU=<p>Para verificar a autoriza��o da SEFAZ referente ao cancelamento acima mencionado, acesse o s�tio http://www.nfe.fazenda.gov.br/portal</p>;PTB=<p>Para verificar a autoriza��o da SEFAZ referente ao cancelamento acima mencionado, acesse o s�tio http://www.nfe.fazenda.gov.br/portal</p>';
      AccessKey_SalesCrMemo@1000000004 : TextConst 'ENU=<p><b>Chave de acesso:</b> %1</p>;PTB=<p><b>Chave de acesso:</b> %1</p>';
      Protocol_SalesCrMemo@1000000005 : TextConst 'ENU=<p><b>Protocolo:</b> %1</p><br><br>;PTB=<p><b>Protocolo:</b> %1</p><br><br>';
      AutoMessage_SalesCrMemo@1000000006 : TextConst 'ENU=<p><i>Esta mensagem foi enviada automaticamente pelo sistema</i> <b>Microsoft Dynamics NAV</b>.</p>;PTB=<p><i>Esta mensagem foi enviada automaticamente pelo sistema</i> <b>Microsoft Dynamics NAV</b>.</p>';
      MessageText@1000000007 : Text;
      SubjectText@52006502 : Text;
      Customer@52006500 : Record 18;
      SenderName@52006501 : TextConst 'ENU=Microsoft Dynamics NAV;PTB=Microsoft Dynamics NAV';
      Subject_SalesCrMemo@52006503 : TextConst 'ENU=Emiss�o de Nota Fiscal Eletr�nica - NF.: %1 - %2;PTB=Cancelamento de Nota Fiscal Eletr�nica - NF.: %1 - %2';
      CompanyInfo@52006504 : Record 79;
      FileNameDanfe@52006506 : Text;
      DanfeNotFoud@52006507 : TextConst 'ENU=Arquivo DANFe n�o encontrado;PTB=Arquivo DANFe n�o encontrado';
      FileNameXML@52006508 : Text;
      XMLNotFoud@52006509 : TextConst 'ENU=Arquivo XML n�o encontrado;PTB=Arquivo XML n�o encontrado';
      CancelledInvoice@52006510 : TextConst 'ENU=A Nota Fiscal %1 est� cancelada.;PTB=A Nota Fiscal %1 est� cancelada.';
      ServiceInvoice@52006511 : TextConst 'ENU=A Nota Fiscal %1 � um servi�o.;PTB=A Nota Fiscal %1 � um servi�o.';
      NotAuthorized@52006512 : TextConst 'ENU=O cancelamento da Nota Fiscal ainda n�o est� autorizado.;PTB=O cancelamento da Nota Fiscal ainda n�o est� autorizado.';
      SalesCrMemoHeader@52006513 : Record 114;
      SalesInvHeader@52006514 : Record 112;
      NFeSetup@52006515 : Record 52112625;
      TextFunctions@52006516 : Codeunit 52112453;
    BEGIN
      IF NFe."Document Type" = NFe."Document Type"::"Sales Cr. Memo" THEN BEGIN
        SalesCrMemoHeader.GET(NFe."Document No.");
        SalesInvHeader.GET(SalesCrMemoHeader."Applies-to Doc. No.");
      END
      ELSE
        SalesInvHeader.GET(NFe."Document No.");

      SalesInvHeader.CALCFIELDS("NFe cStat");
      SalesInvHeader.CALCFIELDS("NFe Chave Acesso");
      SalesInvHeader.CALCFIELDS("NFe Data Envio");
      SalesInvHeader.CALCFIELDS("NFe Protocolo");

      IF Customer.GET(SalesInvHeader."Sell-to Customer No.") THEN
        Customer.TESTFIELD("E-Mail - NFe");

      MessageText := STRSUBSTNO(InvoiceNo_SalesCrMemo, SalesInvHeader."Print Serie", SalesInvHeader."External Document No.",
                                FORMAT(SalesInvHeader."NFe Data Envio", 0, '<Day,2>/<Month,2>/<Year4>'));
      MessageText += STRSUBSTNO(CustName_SalesCrMemo, Customer.Name);
      MessageText += STRSUBSTNO(CPFCNPJ_SalesCrMemo, Customer."C.N.P.J./C.P.F.");
      MessageText += SiteSefaz_SalesCrMemo;
      MessageText += STRSUBSTNO(AccessKey_SalesCrMemo, SalesInvHeader."NFe Chave Acesso");
      MessageText += STRSUBSTNO(Protocol_SalesCrMemo, NFe.Protocolo);
      MessageText += AutoMessage_SalesCrMemo;

      CompanyInfo.GET;
      SubjectText := STRSUBSTNO(Subject_SalesCrMemo, SalesInvHeader."External Document No.", CompanyInfo.Name);

      SMTPMail.CreateMessage(SenderName, 'nav@nav.com.br', TextFunctions.Replace(Customer."E-Mail - NFe", ',', ';'),
                             SubjectText, MessageText, TRUE);

      IF NFeSetup.GET(SalesInvHeader."Branch Code") THEN
        IF NFeSetup."BCC E-Mail" <> '' THEN
          SMTPMail.AddBCC(NFeSetup."BCC E-Mail");

      NFe.CALCFIELDS("Arquivo XML");
      IF NOT NFe."Arquivo XML".HASVALUE THEN
        ERROR(XMLNotFoud);
      FileNameXML := TEMPORARYPATH + '\' + NFe."Access Key" + '-procEventoNFe.xml';
      SMTPMail.AddAttachment(NFe."Arquivo XML".EXPORT(FileNameXML), NFe."Access Key" + '-procEventoNFe.xml');

      SMTPMail.Send;

      MESSAGE(MessageSentSuccessfully);
    END;

    PROCEDURE PurchInvoiceSendEmail@52006576(NFe@52006513 : Record 52112624);
    VAR
      SMTPMail@1000000001 : Codeunit 400;
      InvoiceNo_PurchInvoice@1000000002 : TextConst 'ENU=<p>Esta mensagem refere-se � <b>Nota Fiscal Eletr�nica de s�rie/n�mero (%1/%2)</b>, emitida em %3 para:</p>;PTB=<p>Esta mensagem refere-se � <b>Nota Fiscal Eletr�nica de s�rie/n�mero (%1/%2)</b>, emitida em %3 para:</p>';
      CustName_PurchInvoice@1000000009 : TextConst 'ENU=<p><b>Raz�o Social:</b> %1</p>;PTB=<p><b>Raz�o Social:</b> %1</p>';
      CPFCNPJ_PurchInvoice@1000000010 : TextConst 'ENU=<p><b>CPF/CNPJ:</b> %1</p><br>;PTB=<p><b>CPF/CNPJ:</b> %1</p><br>';
      SiteSefaz_PurchInvoice@1000000003 : TextConst 'ENU=<p>Para verificar a autoriza��o da SEFAZ referente � nota acima mencionada, acesse o s�tio http://www.nfe.fazenda.gov.br/portal</p>;PTB=<p>Para verificar a autoriza��o da SEFAZ referente � nota acima mencionada, acesse o s�tio http://www.nfe.fazenda.gov.br/portal</p>';
      AccessKey_PurchInvoice@1000000004 : TextConst 'ENU=<p><b>Chave de acesso:</b> %1</p>;PTB=<p><b>Chave de acesso:</b> %1</p>';
      Protocol_PurchInvoice@1000000005 : TextConst 'ENU=<p><b>Protocolo:</b> %1</p><br><br>;PTB=<p><b>Protocolo:</b> %1</p><br><br>';
      AutoMessage_PurchInvoice@1000000006 : TextConst 'ENU=<p><i>Esta mensagem foi enviada automaticamente pelo sistema</i> <b>Microsoft Dynamics NAV</b>.</p>;PTB=<p><i>Esta mensagem foi enviada automaticamente pelo sistema</i> <b>Microsoft Dynamics NAV</b>.</p>';
      MessageText@1000000007 : Text;
      SubjectText@52006502 : Text;
      Vendor@52006500 : Record 23;
      SenderName@52006501 : TextConst 'ENU=Microsoft Dynamics NAV;PTB=Microsoft Dynamics NAV';
      Subject_PurchInvoice@52006503 : TextConst 'ENU=Emiss�o de Nota Fiscal Eletr�nica - NF.: %1 - %2;PTB=Emiss�o de Nota Fiscal Eletr�nica - NF.: %1 - %2';
      CompanyInfo@52006504 : Record 79;
      FileNameDanfe@52006506 : Text;
      DanfeNotFoud@52006507 : TextConst 'ENU=Arquivo DANFe n�o encontrado;PTB=Arquivo DANFe n�o encontrado';
      FileNameXML@52006508 : Text;
      XMLNotFound@52006509 : TextConst 'ENU=Arquivo XML n�o encontrado;PTB=Arquivo XML n�o encontrado';
      NotAuthorized@52006512 : TextConst 'ENU=A Nota Fiscal ainda n�o est� autorizada.;PTB=A Nota Fiscal ainda n�o est� autorizada.';
      PurchInvHeader@52006505 : Record 122;
      NFeSetup@52006510 : Record 52112625;
      TextFunctions@52006511 : Codeunit 52112453;
    BEGIN
      PurchInvHeader.GET(NFe."Document No.");
      PurchInvHeader.CALCFIELDS("Credit Memos");

      IF Vendor.GET(PurchInvHeader."Buy-from Vendor No.") THEN
        Vendor.TESTFIELD("E-Mail - NFe");

      MessageText := STRSUBSTNO(InvoiceNo_PurchInvoice, NFe.Serie, NFe."No.",
                                FORMAT(NFe."Sent On Date", 0, '<Day,2>/<Month,2>/<Year4>'));
      MessageText += STRSUBSTNO(CustName_PurchInvoice, Vendor.Name);
      MessageText += STRSUBSTNO(CPFCNPJ_PurchInvoice, Vendor."C.N.P.J./C.P.F.");
      MessageText += SiteSefaz_PurchInvoice;
      MessageText += STRSUBSTNO(AccessKey_PurchInvoice, NFe."Access Key");
      MessageText += STRSUBSTNO(Protocol_PurchInvoice, NFe.Protocolo);
      MessageText += AutoMessage_PurchInvoice;

      CompanyInfo.GET;
      SubjectText := STRSUBSTNO(Subject_PurchInvoice, NFe."No.", CompanyInfo.Name);

      SMTPMail.CreateMessage(SenderName, 'nav@nav.com.br', TextFunctions.Replace(Vendor."E-Mail - NFe", ',', ';'),
                             SubjectText, MessageText, TRUE);

      IF NFeSetup.GET(PurchInvHeader."Branch Code") THEN
        IF NFeSetup."BCC E-Mail" <> '' THEN
          SMTPMail.AddBCC(NFeSetup."BCC E-Mail");

      NFe.TESTFIELD("Access Key");
      NFe.CALCFIELDS("Arquivo DANFe");
      IF NOT NFe."Arquivo DANFe".HASVALUE THEN
        ERROR(DanfeNotFoud);
      FileNameDanfe := TEMPORARYPATH + '\' + NFe."Access Key" + '-DANFe.pdf';
      SMTPMail.AddAttachment(NFe."Arquivo DANFe".EXPORT(FileNameDanfe), NFe."Access Key" + '-DANFe.pdf');

      NFe.CALCFIELDS("Arquivo XML");
      IF NOT NFe."Arquivo XML".HASVALUE THEN
        ERROR(XMLNotFound);
      FileNameXML := TEMPORARYPATH + '\' + NFe."Access Key" + '-procNFe.xml';
      SMTPMail.AddAttachment(NFe."Arquivo XML".EXPORT(FileNameXML), NFe."Access Key" + '-procNFe.xml');

      SMTPMail.Send;

      MESSAGE(MessageSentSuccessfully);
    END;

    PROCEDURE PurchCreditMemoSendEmail@52006575(NFe@52006505 : Record 52112624);
    VAR
      SMTPMail@1000000001 : Codeunit 400;
      InvoiceNo_SalesCrMemo@1000000002 : TextConst 'ENU=<p>Esta mensagem refere-se ao cancelamento da Nota Fiscal Eletr�nica de s�rie/n�mero (%1/%2)</b>, emitida em %3 para:</p>;PTB=<p>Esta mensagem refere-se ao cancelamento da Nota Fiscal Eletr�nica de s�rie/n�mero (%1/%2)</b>, emitida em %3 para:</p>';
      CustName_SalesCrMemo@1000000009 : TextConst 'ENU=<p><b>Raz�o Social:</b> %1</p>;PTB=<p><b>Raz�o Social:</b> %1</p>';
      CPFCNPJ_SalesCrMemo@1000000010 : TextConst 'ENU=<p><b>CPF/CNPJ:</b> %1</p><br>;PTB=<p><b>CPF/CNPJ:</b> %1</p><br>';
      SiteSefaz_SalesCrMemo@1000000003 : TextConst 'ENU=<p>Para verificar a autoriza��o da SEFAZ referente ao cancelamento acima mencionado, acesse o s�tio http://www.nfe.fazenda.gov.br/portal</p>;PTB=<p>Para verificar a autoriza��o da SEFAZ referente ao cancelamento acima mencionado, acesse o s�tio http://www.nfe.fazenda.gov.br/portal</p>';
      AccessKey_SalesCrMemo@1000000004 : TextConst 'ENU=<p><b>Chave de acesso:</b> %1</p>;PTB=<p><b>Chave de acesso:</b> %1</p>';
      Protocol_SalesCrMemo@1000000005 : TextConst 'ENU=<p><b>Protocolo:</b> %1</p><br><br>;PTB=<p><b>Protocolo:</b> %1</p><br><br>';
      AutoMessage_SalesCrMemo@1000000006 : TextConst 'ENU=<p><i>Esta mensagem foi enviada automaticamente pelo sistema</i> <b>Microsoft Dynamics NAV</b>.</p>;PTB=<p><i>Esta mensagem foi enviada automaticamente pelo sistema</i> <b>Microsoft Dynamics NAV</b>.</p>';
      MessageText@1000000007 : Text;
      SubjectText@52006502 : Text;
      Vendor@52006500 : Record 23;
      SenderName@52006501 : TextConst 'ENU=Microsoft Dynamics NAV;PTB=Microsoft Dynamics NAV';
      Subject_SalesCrMemo@52006503 : TextConst 'ENU=Emiss�o de Nota Fiscal Eletr�nica - NF.: %1 - %2;PTB=Cancelamento de Nota Fiscal Eletr�nica - NF.: %1 - %2';
      CompanyInfo@52006504 : Record 79;
      FileNameDanfe@52006506 : Text;
      DanfeNotFoud@52006507 : TextConst 'ENU=Arquivo DANFe n�o encontrado;PTB=Arquivo DANFe n�o encontrado';
      FileNameXML@52006508 : Text;
      XMLNotFoud@52006509 : TextConst 'ENU=Arquivo XML n�o encontrado;PTB=Arquivo XML n�o encontrado';
      CancelledInvoice@52006510 : TextConst 'ENU=A Nota Fiscal %1 est� cancelada.;PTB=A Nota Fiscal %1 est� cancelada.';
      ServiceInvoice@52006511 : TextConst 'ENU=A Nota Fiscal %1 � um servi�o.;PTB=A Nota Fiscal %1 � um servi�o.';
      NotAuthorized@52006512 : TextConst 'ENU=O cancelamento da Nota Fiscal ainda n�o est� autorizado.;PTB=O cancelamento da Nota Fiscal ainda n�o est� autorizado.';
      PurchCrMemoHeader@52006513 : Record 124;
      PurchInvHeader@52006514 : Record 122;
      NFeSetup@52006515 : Record 52112625;
      TextFunctions@52006516 : Codeunit 52112453;
    BEGIN
      IF NFe."Document Type" = NFe."Document Type"::"Purch. Cr. Memo" THEN BEGIN
        PurchCrMemoHeader.GET(NFe."Document No.");
        PurchInvHeader.GET(PurchCrMemoHeader."Applies-to Doc. No.");
      END
      ELSE
        PurchInvHeader.GET(NFe."Document No.");

      PurchInvHeader.CALCFIELDS("NFe cStat");
      PurchInvHeader.CALCFIELDS("NFe Chave Acesso");
      PurchInvHeader.CALCFIELDS("NFe Data Envio");
      PurchInvHeader.CALCFIELDS("NFe Protocolo");

      IF Vendor.GET(PurchInvHeader."Buy-from Vendor No.") THEN
        Vendor.TESTFIELD("E-Mail - NFe");

      MessageText := STRSUBSTNO(InvoiceNo_SalesCrMemo, PurchInvHeader."Print Serie", PurchInvHeader."Vendor Invoice No.",
                                FORMAT(PurchInvHeader."NFe Data Envio", 0, '<Day,2>/<Month,2>/<Year4>'));
      MessageText += STRSUBSTNO(CustName_SalesCrMemo, Vendor.Name);
      MessageText += STRSUBSTNO(CPFCNPJ_SalesCrMemo, Vendor."C.N.P.J./C.P.F.");
      MessageText += SiteSefaz_SalesCrMemo;
      MessageText += STRSUBSTNO(AccessKey_SalesCrMemo, PurchInvHeader."NFe Chave Acesso");
      MessageText += STRSUBSTNO(Protocol_SalesCrMemo, NFe.Protocolo);
      MessageText += AutoMessage_SalesCrMemo;

      CompanyInfo.GET;
      SubjectText := STRSUBSTNO(Subject_SalesCrMemo, PurchInvHeader."Vendor Invoice No.", CompanyInfo.Name);

      SMTPMail.CreateMessage(SenderName, 'nav@nav.com.br', TextFunctions.Replace(Vendor."E-Mail - NFe", ',', ';'),
                             SubjectText, MessageText, TRUE);

      IF NFeSetup.GET(PurchInvHeader."Branch Code") THEN
        IF NFeSetup."BCC E-Mail" <> '' THEN
          SMTPMail.AddBCC(NFeSetup."BCC E-Mail");

      NFe.CALCFIELDS("Arquivo XML");
      IF NOT NFe."Arquivo XML".HASVALUE THEN
        ERROR(XMLNotFoud);
      FileNameXML := TEMPORARYPATH + '\' + NFe."Access Key" + '-procEventoNFe.xml';
      SMTPMail.AddAttachment(NFe."Arquivo XML".EXPORT(FileNameXML), NFe."Access Key" + '-procEventoNFe.xml');

      SMTPMail.Send;

      MESSAGE(MessageSentSuccessfully);
    END;

    PROCEDURE SendEmail@52006500(NFe@52006500 : Record 52112624);
    VAR
      SalesInvHeader@52006501 : Record 112;
      SalesCrMemoHeader@52006502 : Record 114;
      PurchInvHeader@52006503 : Record 122;
      PurchCrMemoHeader@52006504 : Record 124;
    BEGIN
      CASE NFe."Document Type" OF
        NFe."Document Type"::"Sales Invoice": BEGIN
          CASE NFe.cStat OF
            100 : SalesInvoiceSendEmail(NFe);
            101 : SalesCreditMemoSendEmail(NFe);
          ELSE
            EXIT;
          END;
        END;
        NFe."Document Type"::"Sales Cr. Memo": BEGIN
          CASE NFe.cStat OF
            101 : SalesCreditMemoSendEmail(NFe);
            135 : SalesCreditMemoSendEmail(NFe);
          ELSE
            EXIT;
          END;
        END;
        NFe."Document Type"::"Purch. Invoice": BEGIN
          CASE NFe.cStat OF
            100 : PurchInvoiceSendEmail(NFe);
            101 : PurchCreditMemoSendEmail(NFe);
          ELSE
          END;
        END;
        NFe."Document Type"::"Purch. Cr. Memo": BEGIN
          CASE NFe.cStat OF
            101 : PurchCreditMemoSendEmail(NFe);
            135 : PurchCreditMemoSendEmail(NFe);
          ELSE
          END;
        END;
      ELSE
        EXIT;
      END;
    END;

    BEGIN
    END.
  }
}

