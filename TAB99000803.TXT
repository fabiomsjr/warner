OBJECT Table 99000803 Routing Personnel
{
  OBJECT-PROPERTIES
  {
    Date=05/11/08;
    Time=12:00:00;
    Version List=NAVW16.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Routing Personnel;
               PTB=Pessoal do Roteiro];
  }
  FIELDS
  {
    { 1   ;   ;Routing No.         ;Code20        ;TableRelation="Routing Header";
                                                   CaptionML=[ENU=Routing No.;
                                                              PTB=N� Roteiro];
                                                   NotBlank=Yes }
    { 2   ;   ;Line No.            ;Integer       ;CaptionML=[ENU=Line No.;
                                                              PTB=N� Linha] }
    { 4   ;   ;No.                 ;Code20        ;CaptionML=[ENU=No.;
                                                              PTB=N�] }
    { 5   ;   ;Description         ;Text50        ;CaptionML=[ENU=Description;
                                                              PTB=Descri��o] }
    { 20  ;   ;Version Code        ;Code10        ;TableRelation="Routing Version"."Version Code" WHERE (Routing No.=FIELD(Routing No.));
                                                   CaptionML=[ENU=Version Code;
                                                              PTB=C�digo de Vers�o] }
    { 21  ;   ;Operation No.       ;Code10        ;TableRelation="Routing Line"."Operation No." WHERE (Routing No.=FIELD(Routing No.),
                                                                                                       Version Code=FIELD(Version Code));
                                                   CaptionML=[ENU=Operation No.;
                                                              PTB=N� Opera��o];
                                                   NotBlank=Yes }
  }
  KEYS
  {
    {    ;Routing No.,Version Code,Operation No.,Line No.;
                                                   Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    PROCEDURE Caption@1() : Text[100];
    VAR
      RtngHeader@1000 : Record 99000763;
    BEGIN
      IF GETFILTERS = '' THEN
        EXIT('');

      IF "Routing No." = '' THEN
        EXIT('');

      RtngHeader.GET("Routing No.");

      EXIT(
        STRSUBSTNO('%1 %2 %3',
          "Routing No.",RtngHeader.Description,"Operation No."));
    END;

    BEGIN
    END.
  }
}

