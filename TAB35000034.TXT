OBJECT Table 35000034 Service Codes2
{
  OBJECT-PROPERTIES
  {
    Date=15/03/16;
    Time=13:48:04;
    Modified=Yes;
    Version List=FNX059.00,WAR005.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=C�digos Servi�o;
               PTB=C�digos Servi�o];
    LookupPageID=Page35000034;
    DrillDownPageID=Page35000034;
  }
  FIELDS
  {
    { 1   ;   ;Code                ;Code20        ;CaptionML=[ENU=C�digo;
                                                              PTB=C�digo] }
    { 2   ;   ;Description         ;Text30        ;CaptionML=[ENU=Description;
                                                              PTB=Descri��o] }
    { 3   ;   ;Account No.         ;Code20        ;TableRelation="G/L Account" WHERE (Account Type=FILTER(<>Total),
                                                                                      Show Account=CONST(Yes));
                                                   CaptionML=[ENU=N� Conta;
                                                              PTB=N� Conta] }
    { 4   ;   ;Tax Group Code      ;Code10        ;TableRelation="Tax Group";
                                                   CaptionML=[ENU=Tax Group Code;
                                                              PTB=Cod. Gr. Imposto;
                                                              ESM=C�d. grupo impuesto;
                                                              FRC=Code groupe fiscal;
                                                              ENC=Tax Group Code] }
    { 5   ;   ;Retention Group Code;Code20        ;TableRelation="Retention Groups.2";
                                                   CaptionML=[ENU=Retention Group Code;
                                                              PTB=C�d. Grupo Reten��o] }
    { 6   ;   ;Tributation Code    ;Code20        ;CaptionML=[ENU=Tributation Code;
                                                              PTB=C�d. Tributa��o (CNAE)] }
    { 7   ;   ;PIS Ret. Group Code ;Code20        ;TableRelation="Retention Groups.2";
                                                   CaptionML=[ENU=Retention Group Code;
                                                              PTB=PIS C�d. Grupo Reten��o] }
    { 8   ;   ;COFINS Ret. Group Code;Code20      ;TableRelation="Retention Groups.2";
                                                   CaptionML=[ENU=Retention Group Code;
                                                              PTB=COFINS C�d. Grupo Reten��o] }
    { 9   ;   ;CSL Ret. Group Code ;Code20        ;TableRelation="Retention Groups.2";
                                                   CaptionML=[ENU=Retention Group Code;
                                                              PTB=CSL C�d. Grupo Reten��o] }
    { 10  ;   ;Service Item Code   ;Code20        ;CaptionML=[ENU=Service Item Code;
                                                              PTB=Item Lista Servi�o] }
    { 50000;  ;Aliquota IBPT       ;Decimal       ;CaptionML=[ENU=IBPT %;
                                                              PTB=Al�quota IBPT];
                                                   Description=WAR005 }
    { 35000000;;Gen. Prod. Posting Group;Code10   ;TableRelation="Gen. Product Posting Group";
                                                   CaptionML=[ENU=Gen. Prod. Posting Group;
                                                              PTB=Gr. Cont�bil Produto];
                                                   Description=NAVBR5.01.1210 }
    { 35000600;;LST Code           ;Integer       ;CaptionML=PTB=C�d. LST;
                                                   Description=EFD1.00 }
  }
  KEYS
  {
    {    ;Code                                    ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

