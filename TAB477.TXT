OBJECT Table 477 Report Inbox
{
  OBJECT-PROPERTIES
  {
    Date=09/06/15;
    Time=14:02:47;
    Version List=NAVW18.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Report Inbox;
               PTB=Caixa de Entrada de Relat�rio];
  }
  FIELDS
  {
    { 1   ;   ;Entry No.           ;Integer       ;AutoIncrement=Yes;
                                                   CaptionML=[ENU=Entry No.;
                                                              PTB=N� Mov.] }
    { 2   ;   ;User ID             ;Text65        ;CaptionML=[ENU=User ID;
                                                              PTB=ID Usu�rio];
                                                   Editable=No }
    { 3   ;   ;Report Output       ;BLOB          ;CaptionML=[ENU=Report Output;
                                                              PTB=Sa�da Relat�rio] }
    { 4   ;   ;Created Date-Time   ;DateTime      ;CaptionML=[ENU=Created Date-Time;
                                                              PTB=Data-Hora Cria��o];
                                                   Editable=No }
    { 5   ;   ;Job Queue Log Entry ID;GUID        ;CaptionML=[ENU=Job Queue Log Entry ID;
                                                              PTB=ID Log Mov. Job Queue];
                                                   Editable=No }
    { 6   ;   ;Output Type         ;Option        ;CaptionML=[ENU=Output Type;
                                                              PTB=Tipo Sa�da];
                                                   OptionCaptionML=ENU=PDF,Word,Excel;
                                                   OptionString=PDF,Word,Excel;
                                                   Editable=No }
    { 7   ;   ;Description         ;Text250       ;CaptionML=[ENU=Description;
                                                              PTB=Descri��o];
                                                   Editable=No }
    { 8   ;   ;Report ID           ;Integer       ;CaptionML=[ENU=Report ID;
                                                              PTB=ID Relat�rio];
                                                   Editable=No }
    { 9   ;   ;Report Name         ;Text250       ;FieldClass=FlowField;
                                                   CalcFormula=Lookup(AllObjWithCaption."Object Caption" WHERE (Object Type=CONST(Report),
                                                                                                                Object ID=FIELD(Report ID)));
                                                   CaptionML=[ENU=Report Name;
                                                              PTB=Nome Relat�rio];
                                                   Editable=No }
    { 10  ;   ;Read                ;Boolean       ;CaptionML=[ENU=Read;
                                                              PTB=Lido] }
  }
  KEYS
  {
    {    ;Entry No.                               ;Clustered=Yes }
    {    ;User ID,Created Date-Time                }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      FileDownLoadTxt@1000 : TextConst 'ENU=Export';
      ReportIsEmptyMsg@1001 : TextConst 'ENU=The report is empty.';

    PROCEDURE ShowReport@1();
    VAR
      Instr@1003 : InStream;
      Downloaded@1002 : Boolean;
      FileName@1004 : Text;
    BEGIN
      CALCFIELDS("Report Output","Report Name");
      IF NOT "Report Output".HASVALUE THEN BEGIN
        Read := TRUE;
        MODIFY;
        COMMIT;
        MESSAGE(ReportIsEmptyMsg);
        EXIT;
      END;
      FileName := DELCHR("Report Name",'=','\/:*?"<>|') + Suffix;
      "Report Output".CREATEINSTREAM(Instr);
      Downloaded := DOWNLOADFROMSTREAM(Instr,FileDownLoadTxt,'','',FileName);

      IF NOT Read AND Downloaded THEN BEGIN
        Read := TRUE;
        MODIFY;
      END;
    END;

    PROCEDURE Suffix@5() : Text;
    BEGIN
      CASE "Output Type" OF
        "Output Type"::PDF:
          EXIT('.pdf');
        "Output Type"::Word:
          EXIT('.docx');
        "Output Type"::Excel:
          EXIT('.xlsx');
      END;
    END;

    BEGIN
    END.
  }
}

