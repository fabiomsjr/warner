OBJECT Page 52112469 NCM Exception Codes
{
  OBJECT-PROPERTIES
  {
    Date=31/05/13;
    Time=09:53:02;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=NCM Exception Codes;
               PTB=C�digos Exce��o NCM];
    SourceTable=Table52112461;
    PageType=List;
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1102300000;1;Group  ;
                GroupType=Repeater }

    { 1102300001;2;Field  ;
                SourceExpr=Code }

    { 1102300003;2;Field  ;
                SourceExpr=Description }

  }
  CODE
  {

    BEGIN
    END.
  }
}

