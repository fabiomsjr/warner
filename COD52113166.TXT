OBJECT Codeunit 52113166 General Journal Page Hook
{
  OBJECT-PROPERTIES
  {
    Date=07/01/15;
    Time=10:35:34;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    OnRun=BEGIN
          END;

  }
  CODE
  {
    VAR
      InsertAllowed@35100001 : Boolean;
      EditAllowed@35100000 : Boolean;
      InsertNotAllowed@35100003 : TextConst 'ENU=Inserting lines into an approval journal is not allowed.;PTB=N�o � permitido inserir linhas em um di�rio de aprova��o.';
      EditNotAllowed@35100002 : TextConst 'ENU=Updating a journal with Status "%1" is not allowed.;PTB=N�o � permitido atualizar um di�rio com Status "%1".';
      DeleteAllowed@35100004 : Boolean;
      DeleteNotAllowed@35100005 : TextConst 'ENU=Deleting lines into an approval journal is not allowed.;PTB=N�o � permitido excluir linhas de um di�rio de aprova��o.';

    PROCEDURE OnInsertRecord@35100002(VAR genJnlLine@35100001 : Record 81);
    BEGIN
      CheckInsertAllowed(genJnlLine);
    END;

    PROCEDURE OnModifyRecord@35100040(VAR genJnlLine@35100001 : Record 81);
    VAR
      EditNotAllowed@35100000 : TextConst 'ENU=Updating a journal with Status "%1" is not allowed.;PTB=N�o � permitido atualizar um di�rio com Status "%1".';
    BEGIN
      CheckModifyAllowed(genJnlLine);
    END;

    PROCEDURE OnDeleteRecord@35100000(VAR genJnlLine@35100001 : Record 81);
    BEGIN
      CheckDeleteAllowed(genJnlLine);
    END;

    PROCEDURE IsApprovalEnabled@52006501() : Boolean;
    VAR
      genJnlApprovMgt@35100000 : Codeunit 52113074;
    BEGIN
      EXIT(genJnlApprovMgt.IsApprovalEnabled);
    END;

    PROCEDURE GetBatchStatusText@52006500(VAR genJnlLine@35100001 : Record 81) : Text[20];
    VAR
      genJnlBatch@35100000 : Record 232;
      genJnlApprovMgt@35100002 : Codeunit 52113074;
    BEGIN
      genJnlLine.FILTERGROUP(2);
      genJnlBatch.GET(genJnlLine.GETFILTER("Journal Template Name"), genJnlLine.GETFILTER("Journal Batch Name"));
      genJnlLine.FILTERGROUP(0);
      EditAllowed := (genJnlBatch.Status = genJnlBatch.Status::Open);
      InsertAllowed := EditAllowed AND (NOT genJnlBatch."Approval Batch");
      DeleteAllowed := InsertAllowed;
      EXIT(FORMAT(genJnlBatch.Status));
    END;

    PROCEDURE ActionSendApprovalRequest@52006502(VAR GenJnlLine@52006500 : Record 81);
    VAR
      GenJnlApprovMgt@52006501 : Codeunit 52113074;
    BEGIN
      GenJnlApprovMgt.SendApprovalRequest(GenJnlLine);
    END;

    PROCEDURE ActionCancelApprovalRequest@52006503(VAR GenJnlLine@52006500 : Record 81);
    VAR
      GenJnlApprovMgt@52006501 : Codeunit 52113074;
    BEGIN
      GenJnlApprovMgt.CancelApprovalRequest(GenJnlLine);
    END;

    PROCEDURE ActionOpenApprovalFlow@52006504(VAR GenJnlLine@52006500 : Record 81);
    VAR
      GenJnlApprovMgt@52006501 : Codeunit 52113074;
    BEGIN
      GenJnlApprovMgt.OpenApprovalFlowFromGenJnlLine(GenJnlLine);
    END;

    PROCEDURE ActionReopenApproval@52006505(VAR GenJnlLine@52006500 : Record 81);
    VAR
      GenJnlApprovMgt@52006501 : Codeunit 52113074;
    BEGIN
      GenJnlApprovMgt.Reopen(GenJnlLine);
    END;

    LOCAL PROCEDURE _@35100003();
    BEGIN
    END;

    LOCAL PROCEDURE CheckInsertAllowed@35100007(VAR genJnlLine@35100001 : Record 81);
    BEGIN
      CheckModifyAllowed(genJnlLine);
      IF NOT InsertAllowed THEN
        ERROR(InsertNotAllowed);
    END;

    LOCAL PROCEDURE CheckModifyAllowed@35100006(VAR genJnlLine@35100001 : Record 81);
    VAR
      EditNotAllowed@35100000 : TextConst 'ENU=Updating a journal with Status "%1" is not allowed.;PTB=N�o � permitido atualizar um di�rio com Status "%1".';
      status@35100003 : Text[100];
    BEGIN
      status := GetBatchStatusText(genJnlLine);
      IF NOT EditAllowed THEN
        ERROR(EditNotAllowed, status);
    END;

    LOCAL PROCEDURE CheckDeleteAllowed@35100004(VAR genJnlLine@35100001 : Record 81);
    BEGIN
      CheckModifyAllowed(genJnlLine);
      IF NOT DeleteAllowed THEN
        ERROR(DeleteNotAllowed);
    END;

    BEGIN
    END.
  }
}

