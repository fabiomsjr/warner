OBJECT XMLport 52113023 CNAB Layout Export/Import
{
  OBJECT-PROPERTIES
  {
    Date=26/09/14;
    Time=10:55:09;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Export/Import CNAB Layout;
               PTB=Exportar/Importar Layout CNAB];
    Encoding=UTF-8;
    DefaultFieldsValidation=No;
    Format/Evaluate=XML Format/Evaluate;
    PreserveWhiteSpace=Yes;
    TextEncoding=UTF-8;
    OnPreXMLport=BEGIN
                   IF LayoutCode = '' THEN
                     ERROR(Text001);

                   currXMLport.IMPORTFILE := NOT IsExport;

                   currXMLport.FILENAME := LayoutCode + '.xml';
                   GLOBALLANGUAGE(1046);
                 END;

    Format=Xml;
    FieldDelimiter=";
    FieldSeparator=|;
    FileName=Layout.xml;
  }
  ELEMENTS
  {
    { [{29652AEA-5C7E-41BB-B929-2C0087DBB7DC}];  ;Root                ;Element ;Text    ;
                                                  Width=1024 }

    { [{B6638B4E-8250-4B4E-BBCD-EFF15E9DCD41}];1 ;CNABLayout          ;Element ;Table   ;
                                                  VariableName=CNABLayout;
                                                  SourceTable=Table52113023;
                                                  SourceTableView=SORTING(Field10);
                                                  Import::OnBeforeInsertRecord=BEGIN
                                                                                 CNABLayout.Code := LayoutCode;
                                                                               END;

                                                  Export::OnPreXMLItem=BEGIN
                                                                         CNABLayout.SETRANGE(Code, LayoutCode);
                                                                       END;
                                                                        }

    { [{D1DF6089-3927-48B4-9236-208E355064C9}];2 ;Code                ;Element ;Field   ;
                                                  DataType=Code;
                                                  SourceField=CNABLayout::Code }

    { [{67825FE3-C99C-495B-A43F-1DC7C2A00FB9}];2 ;Description         ;Element ;Field   ;
                                                  DataType=Text;
                                                  SourceField=CNABLayout::Description }

    { [{A74CDAF6-B3F7-4B70-9C89-DA651FEE2782}];2 ;Type                ;Element ;Field   ;
                                                  DataType=Option;
                                                  SourceField=CNABLayout::Type }

    { [{090E164D-B954-4D53-83E8-C12A8067A106}];2 ;DateFormat          ;Element ;Field   ;
                                                  DataType=Text;
                                                  SourceField=CNABLayout::Date Format }

    { [{037A4F96-6FDA-428D-AF63-E0FE5035B1C9}];2 ;FilenameFormat      ;Element ;Field   ;
                                                  DataType=Text;
                                                  SourceField=CNABLayout::Filename Format }

    { [{82DE5154-B230-44CF-97DC-6CB820027475}];1 ;CNABLayoutLine      ;Element ;Table   ;
                                                  VariableName=CNABLayoutLine;
                                                  SourceTable=Table52113024;
                                                  SourceTableView=SORTING(Field10,Field20,Field30);
                                                  Import::OnBeforeInsertRecord=BEGIN
                                                                                 CNABLayoutLine."Layout Code" := LayoutCode;
                                                                               END;

                                                  Export::OnPreXMLItem=BEGIN
                                                                         CNABLayoutLine.SETRANGE("Layout Code", LayoutCode);
                                                                       END;
                                                                        }

    { [{16224E6F-CF0A-4144-881C-A862D2BE4AAE}];2 ;LayoutCode          ;Element ;Field   ;
                                                  DataType=Code;
                                                  SourceField=CNABLayoutLine::Layout Code }

    { [{500CDCBD-717C-4153-95D0-C0B651DF3626}];2 ;BatchCode           ;Element ;Field   ;
                                                  DataType=Code;
                                                  SourceField=CNABLayoutLine::Batch Code }

    { [{CC101963-588A-40C9-BCB6-F5664FC17B8C}];2 ;PaymentMethodCode   ;Element ;Field   ;
                                                  DataType=Code;
                                                  SourceField=CNABLayoutLine::Payment Method Code;
                                                  MinOccurs=Zero }

    { [{11C59E7C-DAEF-4220-80F2-28D9C85BEB42}];2 ;SectionType         ;Element ;Field   ;
                                                  DataType=Option;
                                                  SourceField=CNABLayoutLine::Section Type }

    { [{FDEF188B-1D33-4C7B-89D8-8C134F6F50B3}];2 ;RecordCode          ;Element ;Field   ;
                                                  DataType=Code;
                                                  SourceField=CNABLayoutLine::Record Code }

    { [{3F91D37C-6D71-4944-83C8-6182781DD68C}];2 ;AdditRecordCode     ;Element ;Field   ;
                                                  DataType=Code;
                                                  SourceField=CNABLayoutLine::Additional Record Code }

    { [{1B344C3A-4B4D-478F-A699-6CCE1EE27427}];2 ;PaymentMethodNumber ;Element ;Field   ;
                                                  DataType=Text;
                                                  SourceField=CNABLayoutLine::Payment Method Number }

    { [{70C6CCC7-077A-4788-AD46-F8D5308F64AB}];2 ;Order               ;Element ;Field   ;
                                                  DataType=Integer;
                                                  SourceField=CNABLayoutLine::Order }

    { [{18E12EE2-9D5D-40DC-A891-24342ED88F3F}];1 ;CNABRecord          ;Element ;Table   ;
                                                  VariableName=CNABRecord;
                                                  SourceTable=Table52113025;
                                                  SourceTableView=SORTING(Field5,Field10);
                                                  Import::OnBeforeInsertRecord=BEGIN
                                                                                 CNABRecord."Layout Code" := LayoutCode;
                                                                               END;

                                                  Export::OnPreXMLItem=BEGIN
                                                                         CNABRecord.SETRANGE("Layout Code", LayoutCode);
                                                                       END;
                                                                        }

    { [{BBDC4FC1-B0DC-486C-98AA-F0773F923A20}];2 ;LayoutCode          ;Element ;Field   ;
                                                  DataType=Code;
                                                  SourceField=CNABRecord::Layout Code }

    { [{1FDC9BDD-BD7D-4B95-83A7-4B48B8DD9789}];2 ;Code                ;Element ;Field   ;
                                                  DataType=Code;
                                                  SourceField=CNABRecord::Code }

    { [{2760F592-04A4-4951-9CE8-758EA3F51190}];2 ;Description         ;Element ;Field   ;
                                                  DataType=Text;
                                                  SourceField=CNABRecord::Description }

    { [{89908633-5CB4-48C9-9966-8992F7EE23AC}];2 ;IDField             ;Element ;Field   ;
                                                  DataType=Integer;
                                                  SourceField=CNABRecord::ID Field }

    { [{1AAC70C0-E603-414D-93C6-37E0A89D3DE9}];1 ;CNABRecordField     ;Element ;Table   ;
                                                  VariableName=CNABRecordField;
                                                  SourceTable=Table52113026;
                                                  SourceTableView=SORTING(Field5,Field10,Field20);
                                                  Import::OnBeforeInsertRecord=BEGIN
                                                                                 CNABRecordField."Layout Code" := LayoutCode;
                                                                               END;

                                                  Export::OnPreXMLItem=BEGIN
                                                                         CNABRecordField.SETRANGE("Layout Code", LayoutCode);
                                                                       END;
                                                                        }

    { [{E5A9B5CD-9EBA-40DB-B271-FC11A2A82871}];2 ;LayoutCode          ;Element ;Field   ;
                                                  DataType=Code;
                                                  SourceField=CNABRecordField::Layout Code }

    { [{22F3EB3D-1BAE-405A-88EF-D984E2A71E04}];2 ;RecordCode          ;Element ;Field   ;
                                                  DataType=Code;
                                                  SourceField=CNABRecordField::Record Code }

    { [{01D4A2AD-B4F5-442A-9A35-80F046BA19F5}];2 ;OrderNo             ;Element ;Field   ;
                                                  DataType=Integer;
                                                  SourceField=CNABRecordField::Order No. }

    { [{FBE66277-6C31-448C-9259-946E09A599AC}];2 ;Description         ;Element ;Field   ;
                                                  DataType=Text;
                                                  SourceField=CNABRecordField::Description }

    { [{6AF8826C-4D58-4F30-8CA2-02DD004E25C8}];2 ;Type                ;Element ;Field   ;
                                                  DataType=Option;
                                                  SourceField=CNABRecordField::Type }

    { [{F38CCDE2-ECB0-43C7-9BC5-5F3B299B1F69}];2 ;Length              ;Element ;Field   ;
                                                  DataType=Integer;
                                                  SourceField=CNABRecordField::Length }

    { [{FA2B9506-F134-4AC1-8263-4CB0C8CA4EE0}];2 ;DecimalsNo          ;Element ;Field   ;
                                                  DataType=Integer;
                                                  SourceField=CNABRecordField::Decimals No. }

    { [{6D6D06BA-A9DD-49B3-8723-A8C7F230003C}];2 ;FixedContent        ;Element ;Field   ;
                                                  DataType=Text;
                                                  SourceField=CNABRecordField::Fixed Content }

    { [{AD060AC6-8F19-47E0-8DC1-EE7EE6198D3E}];2 ;FieldCode           ;Element ;Field   ;
                                                  DataType=Code;
                                                  SourceField=CNABRecordField::Field Code }

    { [{DC69AD43-22EE-4544-B7F2-5CD177D860BF}];2 ;SubRecordCode       ;Element ;Field   ;
                                                  DataType=Code;
                                                  SourceField=CNABRecordField::Sub Record Code }

    { [{2B68CC61-59DB-49AA-AFAC-EE636BCDE72B}];2 ;Mandatory           ;Element ;Field   ;
                                                  DataType=Boolean;
                                                  SourceField=CNABRecordField::Mandatory }

    { [{E4CAC621-58D2-4136-A7B7-12E51D24D88F}];2 ;CustomFieldCode     ;Element ;Field   ;
                                                  DataType=Code;
                                                  SourceField=CNABRecordField::Custom Field Code }

    { [{8C5D5486-CE9F-4CD5-B0B8-2EC683FEC6A4}];2 ;ReturnCode          ;Element ;Field   ;
                                                  DataType=Boolean;
                                                  SourceField=CNABRecordField::Return Code }

    { [{964F6AFB-9A75-4BC9-A269-82AD2BF44A16}];1 ;CNABReturnCode      ;Element ;Table   ;
                                                  VariableName=CNABReturnCode;
                                                  SourceTable=Table52113028;
                                                  SourceTableView=SORTING(Field5,Field10);
                                                  Import::OnBeforeInsertRecord=BEGIN
                                                                                 CNABReturnCode."Layout Code" := LayoutCode;
                                                                               END;

                                                  Export::OnPreXMLItem=BEGIN
                                                                         CNABReturnCode.SETRANGE("Layout Code", LayoutCode);
                                                                       END;
                                                                        }

    { [{E1C615D9-1B02-4EF8-A0AE-551483B94A37}];2 ;LayoutCode          ;Element ;Field   ;
                                                  DataType=Code;
                                                  SourceField=CNABReturnCode::Layout Code }

    { [{3D54D459-2F52-4394-8255-1962ECADE1CC}];2 ;Code                ;Element ;Field   ;
                                                  DataType=Code;
                                                  SourceField=CNABReturnCode::Code }

    { [{5DDC0E7E-0258-436F-B0B7-18F1910BC966}];2 ;Description         ;Element ;Field   ;
                                                  DataType=Text;
                                                  SourceField=CNABReturnCode::Description }

    { [{4BED809A-EF64-4995-89DF-437697815929}];2 ;Status              ;Element ;Field   ;
                                                  DataType=Option;
                                                  SourceField=CNABReturnCode::Status }

  }
  EVENTS
  {
  }
  REQUESTPAGE
  {
    PROPERTIES
    {
      OnInit=VAR
               direction@52006500 : Integer;
             BEGIN
               direction := STRMENU(Text002);
               IF direction = 0 THEN
                 ERROR('');

               IsExport := (direction = 1);
               currXMLport.IMPORTFILE(NOT IsExport);
             END;

      OnQueryClosePage=BEGIN
                         IF (CloseAction = ACTION::OK) AND (LayoutCode = '') THEN BEGIN
                           ERROR(Text001);
                         END;
                       END;

    }
    CONTROLS
    {
      { 52006500;;Container ;
                  ContainerType=ContentArea }

      { 52006501;1;Group    ;
                  CaptionML=[ENU=Options;
                             PTB=Op��es];
                  GroupType=Group }

      { 52006502;2;Field    ;
                  CaptionML=[ENU=Layout Code;
                             PTB=C�d. Layout];
                  SourceExpr=LayoutCode;
                  TableRelation="CNAB Layout";
                  Visible=IsExport }

      { 52006504;2;Field    ;
                  CaptionML=[ENU=New Layout Code;
                             PTB=C�d. Novo Layout];
                  SourceExpr=LayoutCode;
                  Visible=NOT IsExport;
                  OnValidate=VAR
                               layout@52006500 : Record 52113023;
                             BEGIN
                               IF layout.GET(LayoutCode) THEN
                                 ERROR(Text003, LayoutCode);
                             END;
                              }

    }
  }
  CODE
  {
    VAR
      Text001@52006501 : TextConst 'ENU=The layout must be selected.;PTB=O layout deve ser selecionado.';
      LayoutCode@52006500 : Code[30];
      IsExport@52006503 : Boolean INDATASET;
      Text002@52006504 : TextConst 'ENU=Export,Import;PTB=Exportar,Importar';
      Text003@52006502 : TextConst 'ENU=The %1 layout already exists.;PTB=O layout %1 j� existe.';

    BEGIN
    END.
  }
}

