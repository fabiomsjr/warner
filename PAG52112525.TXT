OBJECT Page 52112525 Tax Settlement Amounts
{
  OBJECT-PROPERTIES
  {
    Date=10/04/15;
    Time=08:28:20;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Tax Settlement Adjustment Amounts;
               PTB=Valores Ajuste Apura��o];
    SourceTable=Table52112525;
    PageType=Worksheet;
    AutoSplitKey=Yes;
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1102300000;1;Group  ;
                GroupType=Repeater }

    { 1102300001;2;Field  ;
                SourceExpr=Description }

    { 1102300012;2;Field  ;
                SourceExpr="Adjust Code" }

    { 1102300005;2;Field  ;
                SourceExpr="Base Amount" }

    { 52006501;2;Field    ;
                SourceExpr="Tax %" }

    { 1102300014;2;Field  ;
                SourceExpr=Amount }

    { 1010000000;2;Field  ;
                SourceExpr="Reference Date" }

    { 1102300003;2;Field  ;
                SourceExpr="Document No." }

    { 1102300007;2;Field  ;
                SourceExpr="Est. Settlement Group Code";
                Visible=FALSE }

    { 52006500;2;Field    ;
                SourceExpr="GIA Occurrence Code" }

  }
  CODE
  {

    BEGIN
    END.
  }
}

