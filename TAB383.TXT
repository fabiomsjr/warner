OBJECT Table 383 Detailed CV Ledg. Entry Buffer
{
  OBJECT-PROPERTIES
  {
    Date=09/09/14;
    Time=12:00:00;
    Version List=NAVW18.00,NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Detailed CV Ledg. Entry Buffer;
               PTB=Mem. Int. Mov. CV Detalhe];
    LookupPageID=Page573;
    DrillDownPageID=Page573;
  }
  FIELDS
  {
    { 1   ;   ;Entry No.           ;Integer       ;CaptionML=[ENU=Entry No.;
                                                              PTB=N� Mov.] }
    { 2   ;   ;CV Ledger Entry No. ;Integer       ;CaptionML=[ENU=CV Ledger Entry No.;
                                                              PTB=N� Mov. CF] }
    { 3   ;   ;Entry Type          ;Option        ;CaptionML=[ENU=Entry Type;
                                                              PTB=Tipo Mov.];
                                                   OptionCaptionML=[ENU=,Initial Entry,Application,Unrealized Loss,Unrealized Gain,Realized Loss,Realized Gain,Payment Discount,Payment Discount (VAT Excl.),Payment Discount (VAT Adjustment),Appln. Rounding,Correction of Remaining Amount,Payment Tolerance,Payment Discount Tolerance,Payment Tolerance (VAT Excl.),Payment Tolerance (VAT Adjustment),Payment Discount Tolerance (VAT Excl.),Payment Discount Tolerance (VAT Adjustment);
                                                                    PTB=,Mov. Inicial,Liquida��o,Perda n�o Real.,Ganho n�o Real.,Perda Real.,Ganho Real.,Desconto Pagamento,Desconto Pagamento (Excl. IPI),Desconto Pagamento (Ajuste IPI),Arredond. Liq.,Corre��o do Valor Pendente,Toler�ncia Pagamento,Toler�ncia Desconto Pagamento,Toler�ncia Pagamento (Excl. IPI),Toler�ncia Pagamento (Ajuste IPI),Toler�ncia Desconto Pagamento (Excl. IPI),Toler�ncia Desconto Pagamento (Ajuste IPI)];
                                                   OptionString=,Initial Entry,Application,Unrealized Loss,Unrealized Gain,Realized Loss,Realized Gain,Payment Discount,Payment Discount (VAT Excl.),Payment Discount (VAT Adjustment),Appln. Rounding,Correction of Remaining Amount,Payment Tolerance,Payment Discount Tolerance,Payment Tolerance (VAT Excl.),Payment Tolerance (VAT Adjustment),Payment Discount Tolerance (VAT Excl.),Payment Discount Tolerance (VAT Adjustment) }
    { 4   ;   ;Posting Date        ;Date          ;CaptionML=[ENU=Posting Date;
                                                              PTB=Data Registo] }
    { 5   ;   ;Document Type       ;Option        ;CaptionML=[ENU=Document Type;
                                                              PTB=Tipo Documento];
                                                   OptionCaptionML=[ENU=" ,Payment,Invoice,Credit Memo,Finance Charge Memo,Reminder,Refund";
                                                                    PTB=" ,Pagamento,Nota Fiscal,Nota de Cr�dito,Nota de Encargo Financeiro,Lembrete,Reembolso"];
                                                   OptionString=[ ,Payment,Invoice,Credit Memo,Finance Charge Memo,Reminder,Refund] }
    { 6   ;   ;Document No.        ;Code20        ;CaptionML=[ENU=Document No.;
                                                              PTB=No. Documento] }
    { 7   ;   ;Amount              ;Decimal       ;CaptionML=[ENU=Amount;
                                                              PTB=Valor];
                                                   AutoFormatType=1;
                                                   AutoFormatExpr="Currency Code" }
    { 8   ;   ;Amount (LCY)        ;Decimal       ;CaptionML=[ENU=Amount (LCY);
                                                              PTB=Valor (ML)];
                                                   AutoFormatType=1 }
    { 9   ;   ;CV No.              ;Code20        ;CaptionML=[ENU=CV No.;
                                                              PTB=N� CF] }
    { 10  ;   ;Currency Code       ;Code10        ;TableRelation=Currency;
                                                   CaptionML=[ENU=Currency Code;
                                                              PTB=Cod. Moeda] }
    { 11  ;   ;User ID             ;Code50        ;TableRelation=User."User Name";
                                                   OnLookup=VAR
                                                              UserMgt@1000 : Codeunit 418;
                                                            BEGIN
                                                              UserMgt.LookupUserID("User ID");
                                                            END;

                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=User ID;
                                                              PTB=ID Usu�rio] }
    { 12  ;   ;Source Code         ;Code10        ;TableRelation="Source Code";
                                                   CaptionML=[ENU=Source Code;
                                                              PTB=Cod. Origem] }
    { 13  ;   ;Transaction No.     ;Integer       ;CaptionML=[ENU=Transaction No.;
                                                              PTB=N� Lan�amento] }
    { 14  ;   ;Journal Batch Name  ;Code10        ;TestTableRelation=No;
                                                   CaptionML=[ENU=Journal Batch Name;
                                                              PTB=Nome Se��o Di�rio] }
    { 15  ;   ;Reason Code         ;Code10        ;TableRelation="Reason Code";
                                                   CaptionML=[ENU=Reason Code;
                                                              PTB=Cod. Raz�o] }
    { 16  ;   ;Debit Amount        ;Decimal       ;CaptionML=[ENU=Debit Amount;
                                                              PTB=Valor D�bito];
                                                   BlankZero=Yes;
                                                   AutoFormatType=1;
                                                   AutoFormatExpr="Currency Code" }
    { 17  ;   ;Credit Amount       ;Decimal       ;CaptionML=[ENU=Credit Amount;
                                                              PTB=Valor Cr�dito];
                                                   BlankZero=Yes;
                                                   AutoFormatType=1;
                                                   AutoFormatExpr="Currency Code" }
    { 18  ;   ;Debit Amount (LCY)  ;Decimal       ;CaptionML=[ENU=Debit Amount (LCY);
                                                              PTB=Valor D�bito (ML)];
                                                   BlankZero=Yes;
                                                   AutoFormatType=1 }
    { 19  ;   ;Credit Amount (LCY) ;Decimal       ;CaptionML=[ENU=Credit Amount (LCY);
                                                              PTB=Valor Cr�dito (ML)];
                                                   BlankZero=Yes;
                                                   AutoFormatType=1 }
    { 20  ;   ;Initial Entry Due Date;Date        ;CaptionML=[ENU=Initial Entry Due Date;
                                                              PTB=Data Vencimento] }
    { 21  ;   ;Initial Entry Global Dim. 1;Code20 ;CaptionML=[ENU=Initial Entry Global Dim. 1;
                                                              PTB=Mov. Inicial Dimens�o Global 1] }
    { 22  ;   ;Initial Entry Global Dim. 2;Code20 ;CaptionML=[ENU=Initial Entry Global Dim. 2;
                                                              PTB=Mov. Inicial Dimens�o Global 2] }
    { 23  ;   ;Gen. Posting Type   ;Option        ;CaptionML=[ENU=Gen. Posting Type;
                                                              PTB=Tipo Registro Geral];
                                                   OptionCaptionML=[ENU=" ,Purchase,Sale,Settlement";
                                                                    PTB=" ,Compra,Venda,Ajuste"];
                                                   OptionString=[ ,Purchase,Sale,Settlement] }
    { 24  ;   ;Gen. Bus. Posting Group;Code10     ;TableRelation="Gen. Business Posting Group";
                                                   CaptionML=[ENU=Gen. Bus. Posting Group;
                                                              PTB=Gr. Cont�bil Neg�cio] }
    { 25  ;   ;Gen. Prod. Posting Group;Code10    ;TableRelation="Gen. Product Posting Group";
                                                   CaptionML=[ENU=Gen. Prod. Posting Group;
                                                              PTB=Gr. Cont�bil Produto] }
    { 26  ;   ;Tax Area Code       ;Code20        ;TableRelation="Tax Area";
                                                   CaptionML=[ENU=Tax Area Code;
                                                              PTB=C�d. �rea Imposto] }
    { 27  ;   ;Tax Liable          ;Boolean       ;CaptionML=[ENU=Tax Liable;
                                                              PTB=Sujeito a Imposto] }
    { 28  ;   ;Tax Group Code      ;Code10        ;TableRelation="Tax Group";
                                                   CaptionML=[ENU=Tax Group Code;
                                                              PTB=Cod. Gr. Imposto] }
    { 29  ;   ;Use Tax             ;Boolean       ;CaptionML=[ENU=Use Tax;
                                                              PTB=Utiliza Imposto] }
    { 30  ;   ;VAT Bus. Posting Group;Code10      ;TableRelation="VAT Business Posting Group";
                                                   CaptionML=[ENU=VAT Bus. Posting Group;
                                                              PTB=Gr. Registro Imp. Neg�cio] }
    { 31  ;   ;VAT Prod. Posting Group;Code10     ;TableRelation="VAT Product Posting Group";
                                                   CaptionML=[ENU=VAT Prod. Posting Group;
                                                              PTB=Gr. Registo Imp. Produto] }
    { 32  ;   ;Additional-Currency Amount;Decimal ;CaptionML=[ENU=Additional-Currency Amount;
                                                              PTB=Valor Moeda-Adicional];
                                                   AutoFormatType=1 }
    { 33  ;   ;VAT Amount (LCY)    ;Decimal       ;CaptionML=[ENU=VAT Amount (LCY);
                                                              PTB=Valor IPI (ML)];
                                                   AutoFormatType=1 }
    { 34  ;   ;Use Additional-Currency Amount;Boolean;
                                                   CaptionML=[ENU=Use Additional-Currency Amount;
                                                              PTB=Usar Valor Moeda-Adicional] }
    { 35  ;   ;Initial Document Type;Option       ;CaptionML=[ENU=Initial Document Type;
                                                              PTB=Tipo Documento Inicial];
                                                   OptionCaptionML=[ENU=" ,Payment,Invoice,Credit Memo,Finance Charge Memo,Reminder,Refund";
                                                                    PTB=" ,Pagamento,Nota Fiscal,Nota de Cr�dito,Nota de Encargo Financeiro,Lembrete,Reembolso"];
                                                   OptionString=[ ,Payment,Invoice,Credit Memo,Finance Charge Memo,Reminder,Refund] }
    { 36  ;   ;Applied CV Ledger Entry No.;Integer;CaptionML=[ENU=Applied CV Ledger Entry No.;
                                                              PTB=CV Aplicaco No. Lan�amento] }
    { 39  ;   ;Remaining Pmt. Disc. Possible;Decimal;
                                                   CaptionML=[ENU=Remaining Pmt. Disc. Possible;
                                                              PTB=Desc. Pag. Poss�vel Pendente];
                                                   AutoFormatType=1;
                                                   AutoFormatExpr="Currency Code" }
    { 40  ;   ;Max. Payment Tolerance;Decimal     ;CaptionML=[ENU=Max. Payment Tolerance;
                                                              PTB=Max. Toler�ncia de Pagamento];
                                                   AutoFormatType=1;
                                                   AutoFormatExpr="Currency Code" }
    { 41  ;   ;Tax Jurisdiction Code;Code20       ;TableRelation="Tax Jurisdiction";
                                                   CaptionML=[ENU=Tax Jurisdiction Code;
                                                              PTB=Cod. Jurisdi��o Imposto];
                                                   Editable=No }
    { 52112430;;Branch Code        ;Code20        ;TableRelation="Branch Information";
                                                   CaptionML=[ENU=Branch Code;
                                                              PTB=Cod. Filial] }
    { 52112440;;BR Prepayment      ;Boolean       ;CaptionML=[ENU=Prepayment;
                                                              PTB=Adiantamento];
                                                   Editable=No }
  }
  KEYS
  {
    {    ;Entry No.                               ;Clustered=Yes }
    {    ;CV Ledger Entry No.,Entry Type          ;SumIndexFields=Amount,Amount (LCY),Debit Amount,Credit Amount,Debit Amount (LCY),Credit Amount (LCY) }
    {    ;CV No.,Initial Entry Due Date,Posting Date,Currency Code;
                                                   SumIndexFields=Amount,Amount (LCY),Debit Amount,Credit Amount,Debit Amount (LCY),Credit Amount (LCY) }
    {    ;CV No.,Posting Date,Entry Type,Currency Code;
                                                   SumIndexFields=Amount,Amount (LCY) }
    {    ;CV No.,Initial Document Type,Document Type;
                                                   SumIndexFields=Amount,Amount (LCY) }
    {    ;Document Type,Document No.,Posting Date  }
    {    ;Initial Document Type,CV No.,Posting Date,Currency Code,Entry Type;
                                                   SumIndexFields=Amount,Amount (LCY) }
    { No ;CV No.,Initial Entry Due Date,Posting Date,Initial Entry Global Dim. 1,Initial Entry Global Dim. 2,Currency Code;
                                                   SumIndexFields=Amount,Amount (LCY),Debit Amount,Credit Amount,Debit Amount (LCY),Credit Amount (LCY) }
    { No ;CV No.,Posting Date,Entry Type,Initial Entry Global Dim. 1,Initial Entry Global Dim. 2,Currency Code;
                                                   SumIndexFields=Amount,Amount (LCY) }
    { No ;CV No.,Initial Document Type,Document Type,Initial Entry Global Dim. 1,Initial Entry Global Dim. 2;
                                                   SumIndexFields=Amount,Amount (LCY) }
    { No ;Initial Document Type,CV No.,Posting Date,Currency Code,Entry Type,Initial Entry Global Dim. 1,Initial Entry Global Dim. 2;
                                                   SumIndexFields=Amount,Amount (LCY) }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    PROCEDURE InsertDtldCVLedgEntry@53(VAR DtldCVLedgEntryBuf@1000 : Record 383;VAR CVLedgEntryBuf@1001 : Record 382;InsertZeroAmout@1004 : Boolean);
    VAR
      NewDtldCVLedgEntryBuf@1002 : Record 383;
      NextDtldBufferEntryNo@1003 : Integer;
    BEGIN
      IF (DtldCVLedgEntryBuf.Amount = 0) AND
         (DtldCVLedgEntryBuf."Amount (LCY)" = 0) AND
         (DtldCVLedgEntryBuf."Additional-Currency Amount" = 0) AND
         (NOT InsertZeroAmout)
      THEN
        EXIT;

      DtldCVLedgEntryBuf.TESTFIELD("Entry Type" );

      NewDtldCVLedgEntryBuf.INIT;
      NewDtldCVLedgEntryBuf := DtldCVLedgEntryBuf;

      IF NextDtldBufferEntryNo = 0 THEN BEGIN
        DtldCVLedgEntryBuf.RESET;
        IF DtldCVLedgEntryBuf.FINDLAST THEN
          NextDtldBufferEntryNo := DtldCVLedgEntryBuf."Entry No." + 1
        ELSE
          NextDtldBufferEntryNo := 1;
      END;

      DtldCVLedgEntryBuf.RESET;
      DtldCVLedgEntryBuf.SETRANGE("CV Ledger Entry No.",CVLedgEntryBuf."Entry No.");
      DtldCVLedgEntryBuf.SETRANGE("Entry Type",NewDtldCVLedgEntryBuf."Entry Type");
      DtldCVLedgEntryBuf.SETRANGE("Posting Date",NewDtldCVLedgEntryBuf."Posting Date");
      DtldCVLedgEntryBuf.SETRANGE("Document Type",NewDtldCVLedgEntryBuf."Document Type");
      DtldCVLedgEntryBuf.SETRANGE("Document No.",NewDtldCVLedgEntryBuf."Document No.");
      DtldCVLedgEntryBuf.SETRANGE("CV No.",NewDtldCVLedgEntryBuf."CV No.");
      DtldCVLedgEntryBuf.SETRANGE("Gen. Posting Type",NewDtldCVLedgEntryBuf."Gen. Posting Type");
      DtldCVLedgEntryBuf.SETRANGE(
        "Gen. Bus. Posting Group",NewDtldCVLedgEntryBuf."Gen. Bus. Posting Group");
      DtldCVLedgEntryBuf.SETRANGE(
        "Gen. Prod. Posting Group",NewDtldCVLedgEntryBuf."Gen. Prod. Posting Group");
      DtldCVLedgEntryBuf.SETRANGE(
        "VAT Bus. Posting Group",NewDtldCVLedgEntryBuf."VAT Bus. Posting Group");
      DtldCVLedgEntryBuf.SETRANGE(
        "VAT Prod. Posting Group",NewDtldCVLedgEntryBuf."VAT Prod. Posting Group");
      DtldCVLedgEntryBuf.SETRANGE("Tax Area Code",NewDtldCVLedgEntryBuf."Tax Area Code");
      DtldCVLedgEntryBuf.SETRANGE("Tax Liable",NewDtldCVLedgEntryBuf."Tax Liable");
      DtldCVLedgEntryBuf.SETRANGE("Tax Group Code",NewDtldCVLedgEntryBuf."Tax Group Code");
      DtldCVLedgEntryBuf.SETRANGE("Use Tax",NewDtldCVLedgEntryBuf."Use Tax");
      DtldCVLedgEntryBuf.SETRANGE(
        "Tax Jurisdiction Code",NewDtldCVLedgEntryBuf."Tax Jurisdiction Code");

      IF DtldCVLedgEntryBuf.FINDFIRST THEN BEGIN
        DtldCVLedgEntryBuf.Amount := DtldCVLedgEntryBuf.Amount + NewDtldCVLedgEntryBuf.Amount;
        DtldCVLedgEntryBuf."Amount (LCY)" :=
          DtldCVLedgEntryBuf."Amount (LCY)" + NewDtldCVLedgEntryBuf."Amount (LCY)";
        DtldCVLedgEntryBuf."VAT Amount (LCY)" :=
          DtldCVLedgEntryBuf."VAT Amount (LCY)" + NewDtldCVLedgEntryBuf."VAT Amount (LCY)";
        DtldCVLedgEntryBuf."Additional-Currency Amount" :=
          DtldCVLedgEntryBuf."Additional-Currency Amount" +
          NewDtldCVLedgEntryBuf."Additional-Currency Amount";
        DtldCVLedgEntryBuf.MODIFY;
      END ELSE BEGIN
        NewDtldCVLedgEntryBuf."Entry No." := NextDtldBufferEntryNo;
        NextDtldBufferEntryNo := NextDtldBufferEntryNo + 1;
        DtldCVLedgEntryBuf := NewDtldCVLedgEntryBuf;
        DtldCVLedgEntryBuf.INSERT;
      END;

      CVLedgEntryBuf."Amount to Apply" := NewDtldCVLedgEntryBuf.Amount + CVLedgEntryBuf."Amount to Apply";
      CVLedgEntryBuf."Remaining Amount" := NewDtldCVLedgEntryBuf.Amount + CVLedgEntryBuf."Remaining Amount";
      CVLedgEntryBuf."Remaining Amt. (LCY)" :=
        NewDtldCVLedgEntryBuf."Amount (LCY)" + CVLedgEntryBuf."Remaining Amt. (LCY)";

      IF DtldCVLedgEntryBuf."Entry Type" = DtldCVLedgEntryBuf."Entry Type"::"Initial Entry" THEN BEGIN
        CVLedgEntryBuf."Original Amount" := NewDtldCVLedgEntryBuf.Amount;
        CVLedgEntryBuf."Original Amt. (LCY)" := NewDtldCVLedgEntryBuf."Amount (LCY)";
      END;
      DtldCVLedgEntryBuf.RESET;
    END;

    PROCEDURE CopyPostingGroupsFromVATEntry@1(VATEntry@1000 : Record 254);
    BEGIN
      "Gen. Posting Type" := VATEntry.Type;
      "Gen. Bus. Posting Group" := VATEntry."Gen. Bus. Posting Group";
      "Gen. Prod. Posting Group" := VATEntry."Gen. Prod. Posting Group";
      "VAT Bus. Posting Group" := VATEntry."VAT Bus. Posting Group";
      "VAT Prod. Posting Group" := VATEntry."VAT Prod. Posting Group";
      "Tax Area Code" := VATEntry."Tax Area Code";
      "Tax Liable" := VATEntry."Tax Liable";
      "Tax Group Code" := VATEntry."Tax Group Code";
      "Use Tax" := VATEntry."Use Tax";
    END;

    PROCEDURE CopyFromGenJnlLine@2(GenJnlLine@1000 : Record 81);
    BEGIN
      "Entry Type" := "Entry Type"::"Initial Entry";
      "Posting Date" := GenJnlLine."Posting Date";
      "Document Type" := GenJnlLine."Document Type";
      "Document No." := GenJnlLine."Document No.";
      Amount := GenJnlLine.Amount;
      "Amount (LCY)" := GenJnlLine."Amount (LCY)";
      "Additional-Currency Amount" := GenJnlLine.Amount;
      "CV No." := GenJnlLine."Account No.";
      "Currency Code" := GenJnlLine."Currency Code";
      "User ID" := USERID;
      "Initial Entry Due Date" := GenJnlLine."Due Date";
      "Initial Entry Global Dim. 1" := GenJnlLine."Shortcut Dimension 1 Code";
      "Initial Entry Global Dim. 2" := GenJnlLine."Shortcut Dimension 2 Code";
      "Initial Document Type" := GenJnlLine."Document Type";
    END;

    PROCEDURE InitFromGenJnlLine@7(GenJnlLine@1001 : Record 81);
    BEGIN
      INIT;
      "Posting Date" := GenJnlLine."Posting Date";
      "Document Type" := GenJnlLine."Document Type";
      "Document No." := GenJnlLine."Document No.";
      "User ID" := USERID;
    END;

    PROCEDURE CopyFromCVLedgEntryBuf@20(CVLedgEntryBuf@1001 : Record 382);
    BEGIN
      "CV Ledger Entry No." := CVLedgEntryBuf."Entry No.";
      "CV No." := CVLedgEntryBuf."CV No.";
      "Currency Code" := CVLedgEntryBuf."Currency Code";
      "Initial Entry Due Date" := CVLedgEntryBuf."Due Date";
      "Initial Entry Global Dim. 1" := CVLedgEntryBuf."Global Dimension 1 Code";
      "Initial Entry Global Dim. 2" := CVLedgEntryBuf."Global Dimension 2 Code";
      "Initial Document Type" := CVLedgEntryBuf."Document Type";
    END;

    PROCEDURE InitDtldCVLedgEntryBuf@26(GenJnlLine@1000 : Record 81;VAR CVLedgEntryBuf@1001 : Record 382;VAR DtldCVLedgEntryBuf@1002 : Record 383;EntryType@1009 : Option;AmountFCY@1003 : Decimal;AmountLCY@1004 : Decimal;AmountAddCurr@1005 : Decimal;AppliedEntryNo@1006 : Integer;RemainingPmtDiscPossible@1007 : Decimal;MaxPaymentTolerance@1008 : Decimal);
    BEGIN
      WITH DtldCVLedgEntryBuf DO BEGIN
        InitFromGenJnlLine(GenJnlLine);
        CopyFromCVLedgEntryBuf(CVLedgEntryBuf);
        "Entry Type" := EntryType;
        Amount := AmountFCY;
        "Amount (LCY)" := AmountLCY;
        "Additional-Currency Amount" := AmountAddCurr;
        "Applied CV Ledger Entry No." := AppliedEntryNo;
        "Remaining Pmt. Disc. Possible" := RemainingPmtDiscPossible;
        "Max. Payment Tolerance" := MaxPaymentTolerance;
        InsertDtldCVLedgEntry(DtldCVLedgEntryBuf,CVLedgEntryBuf,FALSE);
      END;
    END;

    BEGIN
    END.
  }
}

