OBJECT Page 52112423 Branch Information
{
  OBJECT-PROPERTIES
  {
    Date=16/01/15;
    Time=15:12:35;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Branch Information;
               PTB=Informa��o Filial];
    SourceTable=Table52112423;
    PageType=Card;
    ActionList=ACTIONS
    {
      { 52006550;    ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 52006589;1   ;ActionGroup;
                      CaptionML=[ENU=&Setup;
                                 PTB=Configura��e&s] }
      { 52006571;2   ;Action    ;
                      CaptionML=[ENU=Territory Inscriptions of the Tributary Subst.;
                                 PTB=Inscri��es Estaduais do Subst. Tribut�rio];
                      RunObject=Page 52112501;
                      RunPageLink=Branch Code=FIELD(Code);
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=CodesList;
                      PromotedCategory=Process }
      { 52006551;2   ;Action    ;
                      CaptionML=[ENU=NF-e Setup;
                                 PTB=Configura�oes NF-e];
                      RunObject=Page 52112626;
                      RunPageLink=Branch Code=FIELD(Code);
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=ElectronicDoc;
                      PromotedCategory=Process }
    }
  }
  CONTROLS
  {
    { 52006588;0;Container;
                ContainerType=ContentArea }

    { 52006587;1;Group    ;
                CaptionML=[ENU=General;
                           PTB=Geral] }

    { 52006505;2;Field    ;
                SourceExpr=Code }

    { 52006586;2;Field    ;
                SourceExpr=Name }

    { 52006549;2;Field    ;
                SourceExpr="Name 2" }

    { 52006585;2;Field    ;
                SourceExpr=Address }

    { 52006584;2;Field    ;
                SourceExpr=Number }

    { 52006583;2;Field    ;
                SourceExpr="Address 2" }

    { 52006582;2;Field    ;
                SourceExpr="Post Code" }

    { 52006581;2;Field    ;
                SourceExpr=District }

    { 52006580;2;Field    ;
                SourceExpr=City }

    { 52006579;2;Field    ;
                SourceExpr="Territory Code" }

    { 52006578;2;Field    ;
                SourceExpr="Country Code" }

    { 52006577;2;Field    ;
                SourceExpr="Phone No." }

    { 52006576;2;Field    ;
                SourceExpr="VAT Registration No." }

    { 52006575;2;Field    ;
                SourceExpr="Industrial Classification" }

    { 52006574;2;Field    ;
                SourceExpr="C.N.P.J." }

    { 52006573;2;Field    ;
                SourceExpr="I.E." }

    { 52006572;2;Field    ;
                SourceExpr="C.C.M." }

    { 52006570;2;Field    ;
                SourceExpr=CNAES }

    { 52006569;2;Field    ;
                SourceExpr=CEI }

    { 52006568;2;Field    ;
                SourceExpr=NIT }

    { 52006567;2;Field    ;
                SourceExpr=SUFRAMA }

    { 52006566;2;Field    ;
                SourceExpr="Legal Nature Code" }

    { 52006503;2;Group    ;
                CaptionML=[ENU=Dimension;
                           PTB=Dimens�o];
                GroupType=Group }

    { 52006504;3;Field    ;
                SourceExpr="Dimension Value Code" }

    { 52006565;2;Group    ;
                CaptionML=[ENU=ECD;
                           PTB=ECD];
                GroupType=Group }

    { 52006564;3;Field    ;
                SourceExpr="Bookkeeping Indicator" }

    { 52006563;3;Field    ;
                SourceExpr="Data Arq. Atos Constitutivos" }

    { 52006562;3;Field    ;
                SourceExpr="Data Arq. Ato Conversao" }

    { 52006561;2;Group    ;
                CaptionML=[ENU=Simples;
                           PTB=Simples];
                GroupType=Group }

    { 52006560;3;Field    ;
                SourceExpr="Included in Simples" }

    { 52006559;3;Field    ;
                SourceExpr="Inclusion Date" }

    { 52006558;2;Field     }

    { 52006557;2;Field    ;
                SourceExpr=Picture }

    { 52006556;1;Group    ;
                CaptionML=[ENU=Communication;
                           PTB=Comunica��o] }

    { 52006555;2;Field    ;
                Name=Phone No.2;
                SourceExpr="Phone No." }

    { 52006554;2;Field    ;
                SourceExpr="Fax No." }

    { 52006553;2;Field    ;
                SourceExpr="E-Mail" }

    { 52006552;2;Field    ;
                SourceExpr="Home Page" }

    { 52006548;1;Group    ;
                CaptionML=[ENU=Payments;
                           PTB=Pagamentos] }

    { 52006547;2;Field    ;
                SourceExpr="Bank Name" }

    { 52006546;2;Field    ;
                SourceExpr="Bank Branch No." }

    { 52006545;2;Field    ;
                SourceExpr="Bank Account No." }

    { 52006544;2;Field    ;
                SourceExpr="Payment Routing No." }

    { 52006543;2;Field    ;
                SourceExpr="Giro No." }

    { 52006542;2;Field    ;
                SourceExpr="SWIFT Code" }

    { 52006541;2;Field    ;
                SourceExpr=IBAN }

    { 52006540;1;Group    ;
                CaptionML=[ENU=Shipping;
                           PTB=Envio] }

    { 52006539;2;Field    ;
                SourceExpr="Ship-to Name" }

    { 52006538;2;Field    ;
                SourceExpr="Ship-to Address" }

    { 52006537;2;Field    ;
                SourceExpr="Ship-to Number" }

    { 52006536;2;Field    ;
                SourceExpr="Ship-to Address 2" }

    { 52006535;2;Field    ;
                SourceExpr="Ship-to District" }

    { 52006534;2;Field    ;
                SourceExpr="Ship-to Post Code" }

    { 52006533;2;Field    ;
                SourceExpr="Ship-to City" }

    { 52006532;2;Field    ;
                SourceExpr="Ship-to Territory Code" }

    { 52006531;2;Field    ;
                SourceExpr="Ship-to Country Code" }

    { 52006530;2;Field    ;
                SourceExpr="Ship-to Contact" }

    { 52006529;2;Field    ;
                SourceExpr="Location Code" }

    { 52006528;2;Field    ;
                SourceExpr="Responsibility Center" }

    { 52006527;2;Field    ;
                SourceExpr="Check-Avail. Period Calc." }

    { 52006526;2;Field    ;
                SourceExpr="Check-Avail. Time Bucket" }

    { 52006525;2;Field    ;
                DrillDown=No;
                SourceExpr="Base Calendar Code" }

    { 52006524;2;Field    ;
                Name=Customized Calendar;
                DrillDown=Yes;
                CaptionML=[ENU=Customized Calendar;
                           PTB=Calend�rio Customizado];
                SourceExpr=CalendarMgmt.CustomizedCalendarExistText(CustomizedCalendar."Source Type"::Company,'','',"Base Calendar Code");
                Editable=FALSE;
                OnDrillDown=BEGIN
                              CurrPage.SAVERECORD;
                              TESTFIELD("Base Calendar Code");
                              CalendarMgmt.ShowCustomizedCalendar(CustomizedCalEntry."Source Type"::Company,'','',"Base Calendar Code");
                            END;
                             }

    { 52006523;2;Field    ;
                SourceExpr="Cal. Convergence Time Frame" }

    { 52006522;1;Group    ;
                CaptionML=[ENU=Taxes;
                           PTB=Impostos];
                GroupType=Group }

    { 52006519;2;Field    ;
                SourceExpr="NF-e Tributary Regime" }

    { 52006521;2;Field    ;
                SourceExpr="Industry Segment" }

    { 52006520;2;Field    ;
                SourceExpr="Law Id." }

    { 52006506;2;Field    ;
                SourceExpr="Operation Nature Operations" }

    { 52006518;2;Field    ;
                SourceExpr="Tributary Reg." }

    { 52006517;2;Field    ;
                SourceExpr=Contact }

    { 52006516;1;Group    ;
                CaptionML=[ENU=Legally Responsible;
                           PTB=Respons�vel Legal];
                GroupType=Group }

    { 52006515;2;Field    ;
                SourceExpr="CPF IT Responsable" }

    { 52006514;2;Field    ;
                SourceExpr="IT Responsable Name" }

    { 52006513;2;Field    ;
                SourceExpr="IT Responsable CNPJ" }

    { 52006512;2;Field    ;
                SourceExpr="IT Responsable Phone" }

    { 52006511;2;Field    ;
                SourceExpr="IT Responsable Fax" }

    { 52006510;2;Field    ;
                SourceExpr="IT Responsable E-Mail" }

    { 52006509;2;Field    ;
                SourceExpr="Start Date Service" }

    { 52006508;2;Field    ;
                SourceExpr="End Date Service" }

    { 52006507;2;Field    ;
                SourceExpr="Position Held" }

    { 52006502;0;Container;
                ContainerType=FactBoxArea }

    { 52006501;1;Part     ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 52006500;1;Part     ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      Text001@1000 : TextConst 'ENU=Do you want to replace the existing picture?;PTB=Confirma que deseja substituir a imagem anterior?';
      Text002@1001 : TextConst 'ENU=Do you want to delete the picture?;PTB=Confirma que deseja eliminar a imagem?';
      CustomizedCalEntry@1007 : Record 7603;
      CustomizedCalendar@1005 : Record 7602;
      CalendarMgmt@1004 : Codeunit 7600;
      Mail@1002 : Codeunit 397;
      PictureExists@1003 : Boolean;

    BEGIN
    END.
  }
}

