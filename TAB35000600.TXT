OBJECT Table 35000600 SPED Fiscal Setup.2
{
  OBJECT-PROPERTIES
  {
    Date=29/02/12;
    Time=18:14:18;
    Version List=EFD5.08.09;
  }
  PROPERTIES
  {
    CaptionML=[ENU=SPED Fiscal Setup;
               PTB=Config. SPED Fiscal];
  }
  FIELDS
  {
    { 1   ;   ;Primary Key         ;Integer       ;CaptionML=[ENU=Primary Key;
                                                              PTB=Primary Key] }
    { 2   ;   ;Layout Version      ;Text3         ;CaptionML=[ENU=Layout Version;
                                                              PTB=Vers�o Layout] }
    { 3   ;   ;Sped Mov Nos.       ;Code10        ;TableRelation="No. Series";
                                                   CaptionML=[ENU=Sped Mov Nos.;
                                                              PTB=N� S�rie Mov. SPED] }
    { 4   ;   ;Profile Apresentation;Option       ;CaptionML=[ENU=Profile Apresentation;
                                                              PTB=Perfil Apresenta��o];
                                                   OptionString=Perfil A,Perfil B,Perfil C }
    { 5   ;   ;Activity Type       ;Option        ;CaptionML=[ENU=Activity Type;
                                                              PTB=Tipo Atividade];
                                                   OptionString=0-Industrial ou equiparado a industrial,1-Outros }
    { 13  ;   ;File Separator Character;Text1     ;CaptionML=[ENU=File Separator Character;
                                                              PTB=Caracter Separador Arquivo] }
    { 14  ;   ;ICMS Period Results ;Option        ;CaptionML=[ENU=ICMS Period Results;
                                                              PTB=Per�odo Apura��o ICMS];
                                                   OptionString=[ ,0-Mensal,1-Decendial] }
    { 15  ;   ;IPI Period Results  ;Option        ;CaptionML=[ENU=IPI Period Results;
                                                              PTB=Per�odo Apura��o IPI];
                                                   OptionString=[ ,0-Mensal,1-Decendial] }
    { 16  ;   ;Inv. Synthetic Account No.;Code20  ;TableRelation="G/L Account";
                                                   CaptionML=[ENU=Inv. Synthetic Account No.;
                                                              PTB=N� Conta Sint�tica Invent�rio] }
    { 20  ;   ;Item Code           ;Code20        ;CaptionML=[ENU=Item Code;
                                                              PTB=C�d. Item] }
    { 21  ;   ;Description         ;Text100       ;CaptionML=[ENU=Description;
                                                              PTB=Descri��o] }
    { 22  ;   ;Unite Measure Code  ;Code10        ;CaptionML=[ENU=Unite Measure Code;
                                                              PTB=C�d. Unid. Medida Estoque] }
    { 23  ;   ;Item Type           ;Code10        ;TableRelation="Item Type2";
                                                   CaptionML=[ENU=Item Type;
                                                              PTB=Tipo Item] }
    { 24  ;   ;NCM Code            ;Code20        ;TableRelation="NCM Codes2";
                                                   CaptionML=[ENU=NCM Code;
                                                              PTB=C�digo NCM] }
    { 25  ;   ;CST Code            ;Code10        ;TableRelation="CST Codes2";
                                                   CaptionML=[ENU=CST Code;
                                                              PTB=C�digo CST] }
  }
  KEYS
  {
    {    ;Primary Key                             ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

