OBJECT Table 52112457 Vend. Ledger Entry Tmp
{
  OBJECT-PROPERTIES
  {
    Date=17/04/15;
    Time=13:34:08;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Accounts Payable;
               PTB=Contas a Pagar];
    LookupPageID=Page29;
    DrillDownPageID=Page29;
  }
  FIELDS
  {
    { 1   ;   ;Entry No.           ;Integer       ;CaptionML=[ENU=Entry No.;
                                                              PTB=N� Mov.] }
    { 3   ;   ;Vendor No.          ;Code20        ;TableRelation=Vendor;
                                                   CaptionML=[ENU=Vendor No.;
                                                              PTB=N� Fornecedor] }
    { 4   ;   ;Posting Date        ;Date          ;CaptionML=[ENU=Posting Date;
                                                              PTB=Data Registo] }
    { 5   ;   ;Document Type       ;Option        ;CaptionML=[ENU=Document Type;
                                                              PTB=Tipo Documento];
                                                   OptionCaptionML=[ENU=" ,Payment,Invoice,Credit Memo,Finance Charge Memo,Reminder,Refund";
                                                                    PTB=" ,Pagamento,Nota Fiscal,Nota de Cr�dito,Nota de Encargo Financeiro,Lembrete,Reembolso"];
                                                   OptionString=[ ,Payment,Invoice,Credit Memo,Finance Charge Memo,Reminder,Refund] }
    { 6   ;   ;Document No.        ;Code20        ;CaptionML=[ENU=Document No.;
                                                              PTB=No. Documento] }
    { 7   ;   ;Description         ;Text50        ;CaptionML=[ENU=Description;
                                                              PTB=Descri��o] }
    { 11  ;   ;Currency Code       ;Code10        ;TableRelation=Currency;
                                                   CaptionML=[ENU=Currency Code;
                                                              PTB=Cod. Moeda] }
    { 13  ;   ;Amount              ;Decimal       ;CaptionML=[ENU=Amount;
                                                              PTB=Valor];
                                                   Editable=No;
                                                   AutoFormatType=1;
                                                   AutoFormatExpr="Currency Code" }
    { 14  ;   ;Remaining Amount    ;Decimal       ;CaptionML=[ENU=Remaining Amount;
                                                              PTB=Valor Pendente];
                                                   Editable=No;
                                                   AutoFormatType=1;
                                                   AutoFormatExpr="Currency Code" }
    { 15  ;   ;Original Amt. (LCY) ;Decimal       ;CaptionML=[ENU=Original Amt. (LCY);
                                                              PTB=Valor inicial (ML)];
                                                   Editable=No;
                                                   AutoFormatType=1 }
    { 16  ;   ;Remaining Amt. (LCY);Decimal       ;CaptionML=[ENU=Remaining Amt. (LCY);
                                                              PTB=Valor Pendente (ML)];
                                                   Editable=No;
                                                   AutoFormatType=1 }
    { 17  ;   ;Amount (LCY)        ;Decimal       ;CaptionML=[ENU=Amount (LCY);
                                                              PTB=Valor (ML)];
                                                   Editable=No;
                                                   AutoFormatType=1 }
    { 18  ;   ;Purchase (LCY)      ;Decimal       ;CaptionML=[ENU=Purchase (LCY);
                                                              PTB=Compra (ML)];
                                                   AutoFormatType=1 }
    { 20  ;   ;Inv. Discount (LCY) ;Decimal       ;CaptionML=[ENU=Inv. Discount (LCY);
                                                              PTB=Desconto N.Fiscal (ML)];
                                                   AutoFormatType=1 }
    { 21  ;   ;Buy-from Vendor No. ;Code20        ;TableRelation=Vendor;
                                                   CaptionML=[ENU=Buy-from Vendor No.;
                                                              PTB=Compra a-N� Fornecedor] }
    { 22  ;   ;Vendor Posting Group;Code10        ;TableRelation="Vendor Posting Group";
                                                   CaptionML=[ENU=Vendor Posting Group;
                                                              PTB=Gr. Cont�bil Fornecedor] }
    { 23  ;   ;Global Dimension 1 Code;Code20     ;TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(1));
                                                   CaptionML=[ENU=Global Dimension 1 Code;
                                                              PTB=Cod. Dimens�o 1 Global];
                                                   CaptionClass='1,1,1' }
    { 24  ;   ;Global Dimension 2 Code;Code20     ;TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(2));
                                                   CaptionML=[ENU=Global Dimension 2 Code;
                                                              PTB=Cod. Dimens�o 2 Global];
                                                   CaptionClass='1,1,2' }
    { 25  ;   ;Purchaser Code      ;Code10        ;TableRelation=Salesperson/Purchaser;
                                                   CaptionML=[ENU=Purchaser Code;
                                                              PTB=Cod. Comprador] }
    { 27  ;   ;User ID             ;Code50        ;TableRelation=Table2000000002;
                                                   OnLookup=VAR
                                                              LoginMgt@1000 : Codeunit 418;
                                                            BEGIN
                                                              LoginMgt.LookupUserID("User ID");
                                                            END;

                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=User ID;
                                                              PTB=ID Usu�rio] }
    { 28  ;   ;Source Code         ;Code10        ;TableRelation="Source Code";
                                                   CaptionML=[ENU=Source Code;
                                                              PTB=Cod. Origem] }
    { 33  ;   ;On Hold             ;Code3         ;CaptionML=[ENU=On Hold;
                                                              PTB=Retido] }
    { 34  ;   ;Applies-to Doc. Type;Option        ;CaptionML=[ENU=Applies-to Doc. Type;
                                                              PTB=Aplicado por Tipo Doc.];
                                                   OptionCaptionML=[ENU=" ,Payment,Invoice,Credit Memo,Finance Charge Memo,Reminder,Refund";
                                                                    PTB=" ,Pagamento,Nota Fiscal,Nota de Cr�dito,Nota de Encargo Financeiro,Lembrete,Reembolso"];
                                                   OptionString=[ ,Payment,Invoice,Credit Memo,Finance Charge Memo,Reminder,Refund] }
    { 35  ;   ;Applies-to Doc. No. ;Code20        ;CaptionML=[ENU=Applies-to Doc. No.;
                                                              PTB=Aplicado por N� Doc.] }
    { 36  ;   ;Open                ;Boolean       ;CaptionML=[ENU=Open;
                                                              PTB=Pendente] }
    { 37  ;   ;Due Date            ;Date          ;OnValidate=BEGIN
                                                                TESTFIELD(Open,TRUE);
                                                              END;

                                                   CaptionML=[ENU=Due Date;
                                                              PTB=Data Vencimento] }
    { 38  ;   ;Pmt. Discount Date  ;Date          ;OnValidate=BEGIN
                                                                TESTFIELD(Open,TRUE);
                                                              END;

                                                   CaptionML=[ENU=Pmt. Discount Date;
                                                              PTB=Data Desconto Pagamento] }
    { 39  ;   ;Original Pmt. Disc. Possible;Decimal;
                                                   CaptionML=[ENU=Original Pmt. Disc. Possible;
                                                              PTB=Original Desc. Pag. Poss�vel];
                                                   Editable=No;
                                                   AutoFormatType=1;
                                                   AutoFormatExpr="Currency Code" }
    { 40  ;   ;Pmt. Disc. Rcd.(LCY);Decimal       ;CaptionML=[ENU=Pmt. Disc. Rcd.(LCY);
                                                              PTB=Desc. Pagamento Recebido (ML)];
                                                   AutoFormatType=1 }
    { 43  ;   ;Positive            ;Boolean       ;CaptionML=[ENU=Positive;
                                                              PTB=Positivo] }
    { 44  ;   ;Closed by Entry No. ;Integer       ;TableRelation="Vendor Ledger Entry";
                                                   CaptionML=[ENU=Closed by Entry No.;
                                                              PTB=Fechado p/ N� Ordem] }
    { 45  ;   ;Closed at Date      ;Date          ;CaptionML=[ENU=Closed at Date;
                                                              PTB=Fechado � Data] }
    { 46  ;   ;Closed by Amount    ;Decimal       ;CaptionML=[ENU=Closed by Amount;
                                                              PTB=Fechado p/ Valor];
                                                   AutoFormatType=1;
                                                   AutoFormatExpr="Currency Code" }
    { 47  ;   ;Applies-to ID       ;Code50        ;OnValidate=BEGIN
                                                                TESTFIELD(Open,TRUE);
                                                              END;

                                                   CaptionML=[ENU=Applies-to ID;
                                                              PTB=Aplicado por ID.] }
    { 49  ;   ;Journal Batch Name  ;Code10        ;TestTableRelation=No;
                                                   CaptionML=[ENU=Journal Batch Name;
                                                              PTB=Nome Se��o Di�rio] }
    { 50  ;   ;Reason Code         ;Code10        ;TableRelation="Reason Code";
                                                   CaptionML=[ENU=Reason Code;
                                                              PTB=Cod. Raz�o] }
    { 51  ;   ;Bal. Account Type   ;Option        ;CaptionML=[ENU=Bal. Account Type;
                                                              PTB=Tipo Contrapartida];
                                                   OptionCaptionML=[ENU=G/L Account,Customer,Vendor,Bank Account,Fixed Asset;
                                                                    PTB=Conta,Cliente,Fornecedor,Banco,Ativo Fixo];
                                                   OptionString=G/L Account,Customer,Vendor,Bank Account,Fixed Asset }
    { 52  ;   ;Bal. Account No.    ;Code20        ;TableRelation=IF (Bal. Account Type=CONST(G/L Account)) "G/L Account"
                                                                 ELSE IF (Bal. Account Type=CONST(Customer)) Customer
                                                                 ELSE IF (Bal. Account Type=CONST(Vendor)) Vendor
                                                                 ELSE IF (Bal. Account Type=CONST(Bank Account)) "Bank Account"
                                                                 ELSE IF (Bal. Account Type=CONST(Fixed Asset)) "Fixed Asset";
                                                   CaptionML=[ENU=Bal. Account No.;
                                                              PTB=N� Conta Contrap.] }
    { 53  ;   ;Transaction No.     ;Integer       ;CaptionML=[ENU=Transaction No.;
                                                              PTB=N� Lan�amento] }
    { 54  ;   ;Closed by Amount (LCY);Decimal     ;CaptionML=[ENU=Closed by Amount (LCY);
                                                              PTB=Fechado p/ Valor (ML)];
                                                   AutoFormatType=1 }
    { 58  ;   ;Debit Amount        ;Decimal       ;CaptionML=[ENU=Debit Amount;
                                                              PTB=Valor D�bito];
                                                   BlankZero=Yes;
                                                   Editable=No;
                                                   AutoFormatType=1;
                                                   AutoFormatExpr="Currency Code" }
    { 59  ;   ;Credit Amount       ;Decimal       ;CaptionML=[ENU=Credit Amount;
                                                              PTB=Valor Cr�dito];
                                                   BlankZero=Yes;
                                                   Editable=No;
                                                   AutoFormatType=1;
                                                   AutoFormatExpr="Currency Code" }
    { 60  ;   ;Debit Amount (LCY)  ;Decimal       ;CaptionML=[ENU=Debit Amount (LCY);
                                                              PTB=Valor D�bito (ML)];
                                                   BlankZero=Yes;
                                                   Editable=No;
                                                   AutoFormatType=1 }
    { 61  ;   ;Credit Amount (LCY) ;Decimal       ;CaptionML=[ENU=Credit Amount (LCY);
                                                              PTB=Valor Cr�dito (ML)];
                                                   BlankZero=Yes;
                                                   Editable=No;
                                                   AutoFormatType=1 }
    { 62  ;   ;Document Date       ;Date          ;CaptionML=[ENU=Document Date;
                                                              PTB=Data Documento] }
    { 63  ;   ;External Document No.;Code20       ;CaptionML=[ENU=External Document No.;
                                                              PTB=N� Documento Externo] }
    { 64  ;   ;No. Series          ;Code10        ;TableRelation="No. Series";
                                                   CaptionML=[ENU=No. Series;
                                                              PTB=N� S�ries] }
    { 65  ;   ;Closed by Currency Code;Code10     ;TableRelation=Currency;
                                                   CaptionML=[ENU=Closed by Currency Code;
                                                              PTB=Fechado por Cod. Moeda] }
    { 66  ;   ;Closed by Currency Amount;Decimal  ;CaptionML=[ENU=Closed by Currency Amount;
                                                              PTB=Fechado p/ Valor Moeda];
                                                   AutoFormatType=1;
                                                   AutoFormatExpr="Closed by Currency Code" }
    { 73  ;   ;Adjusted Currency Factor;Decimal   ;CaptionML=[ENU=Adjusted Currency Factor;
                                                              PTB=Fator Ajuste Moeda];
                                                   DecimalPlaces=0:15 }
    { 74  ;   ;Original Currency Factor;Decimal   ;CaptionML=[ENU=Original Currency Factor;
                                                              PTB=Fator Moeda Original];
                                                   DecimalPlaces=0:15 }
    { 75  ;   ;Original Amount     ;Decimal       ;CaptionML=[ENU=Original Amount;
                                                              PTB=Valor Original];
                                                   Editable=No;
                                                   AutoFormatType=1;
                                                   AutoFormatExpr="Currency Code" }
    { 76  ;   ;Date Filter         ;Date          ;CaptionML=[ENU=Date Filter;
                                                              PTB=Filtro Data] }
    { 77  ;   ;Remaining Pmt. Disc. Possible;Decimal;
                                                   OnValidate=BEGIN
                                                                TESTFIELD(Open,TRUE);
                                                                CALCFIELDS(Amount,"Original Amount");

                                                                IF "Remaining Pmt. Disc. Possible" * Amount < 0 THEN
                                                                  FIELDERROR("Remaining Pmt. Disc. Possible",STRSUBSTNO(Text000,FIELDCAPTION(Amount)));

                                                                IF ABS("Remaining Pmt. Disc. Possible") > ABS("Original Amount") THEN
                                                                  FIELDERROR("Remaining Pmt. Disc. Possible",STRSUBSTNO(Text001,FIELDCAPTION("Original Amount")));
                                                              END;

                                                   CaptionML=[ENU=Remaining Pmt. Disc. Possible;
                                                              PTB=Desc. Pag. Poss�vel Pendente];
                                                   AutoFormatType=1;
                                                   AutoFormatExpr="Currency Code" }
    { 78  ;   ;Pmt. Disc. Tolerance Date;Date     ;OnValidate=BEGIN
                                                                TESTFIELD(Open,TRUE);
                                                              END;

                                                   CaptionML=[ENU=Pmt. Disc. Tolerance Date;
                                                              PTB=Data Toler�ncia Desc. Pag.] }
    { 79  ;   ;Max. Payment Tolerance;Decimal     ;OnValidate=BEGIN
                                                                TESTFIELD(Open,TRUE);
                                                                CALCFIELDS(Amount,"Remaining Amount");

                                                                IF "Max. Payment Tolerance" * Amount < 0 THEN
                                                                  FIELDERROR("Max. Payment Tolerance",STRSUBSTNO(Text000,FIELDCAPTION(Amount)));

                                                                IF ABS("Max. Payment Tolerance") > ABS("Remaining Amount") THEN
                                                                  FIELDERROR("Max. Payment Tolerance",STRSUBSTNO(Text001,FIELDCAPTION("Remaining Amount")));
                                                              END;

                                                   CaptionML=[ENU=Max. Payment Tolerance;
                                                              PTB=Max. Toler�ncia de Pagamento];
                                                   AutoFormatType=1;
                                                   AutoFormatExpr="Currency Code" }
    { 81  ;   ;Accepted Payment Tolerance;Decimal ;CaptionML=[ENU=Accepted Payment Tolerance;
                                                              PTB=Toler�ncia Pagamento Aceitada];
                                                   AutoFormatType=1;
                                                   AutoFormatExpr="Currency Code" }
    { 82  ;   ;Accepted Pmt. Disc. Tolerance;Boolean;
                                                   CaptionML=[ENU=Accepted Pmt. Disc. Tolerance;
                                                              PTB=Toler�ncia Desc. Pag. Aceitada] }
    { 83  ;   ;Pmt. Tolerance (LCY);Decimal       ;CaptionML=[ENU=Pmt. Tolerance (LCY);
                                                              PTB=Toler�ncia de Pagamento (ML)];
                                                   AutoFormatType=1 }
    { 84  ;   ;Amount to Apply     ;Decimal       ;OnValidate=BEGIN
                                                                TESTFIELD(Open,TRUE);
                                                                CALCFIELDS("Remaining Amount");

                                                                IF "Amount to Apply" * "Remaining Amount" < 0 THEN
                                                                  FIELDERROR("Amount to Apply",STRSUBSTNO(Text000,FIELDCAPTION("Remaining Amount")));

                                                                IF ABS("Amount to Apply") > ABS("Remaining Amount") THEN
                                                                  FIELDERROR("Amount to Apply",STRSUBSTNO(Text001,FIELDCAPTION("Remaining Amount")));
                                                              END;

                                                   CaptionML=[ENU=Amount to Apply;
                                                              PTB=Valor a Aplicar];
                                                   AutoFormatType=1;
                                                   AutoFormatExpr="Currency Code" }
    { 85  ;   ;IC Partner Code     ;Code20        ;TableRelation="IC Partner";
                                                   CaptionML=[ENU=IC Partner Code;
                                                              PTB=IC C�digo Parceiro] }
    { 86  ;   ;Applying Entry      ;Boolean       ;CaptionML=[ENU=Applying Entry;
                                                              PTB=Aplicar Lan�amento] }
    { 87  ;   ;Reversed            ;Boolean       ;CaptionML=[ENU=Reversed;
                                                              PTB=Reservado] }
    { 88  ;   ;Reversed by Entry No.;Integer      ;TableRelation="Vendor Ledger Entry";
                                                   CaptionML=[ENU=Reversed by Entry No.;
                                                              PTB=Reservado pelo Movimento No.];
                                                   BlankZero=Yes }
    { 89  ;   ;Reversed Entry No.  ;Integer       ;TableRelation="Vendor Ledger Entry";
                                                   CaptionML=[ENU=Reversed Entry No.;
                                                              PTB=Movimento No. Reservado];
                                                   BlankZero=Yes }
    { 90  ;   ;Prepayment          ;Boolean       ;CaptionML=ENU=Prepayment }
    { 480 ;   ;Dimension Set ID    ;Integer       ;TableRelation="Dimension Set Entry";
                                                   CaptionML=ENU=Dimension Set ID;
                                                   Editable=No }
    { 52112430;;Applies to-Installment;Boolean    ;CaptionML=[ENU=Applies to-Installment;
                                                              PTB=Aplicado por Parcelas] }
    { 52112440;;Installment No.    ;Integer       ;CaptionML=[ENU=Installment No.;
                                                              PTB=N� Parcela] }
    { 52112450;;Branch Code (Old)  ;Code20        ;TableRelation="Branch Information";
                                                   CaptionML=[ENU=Branch Code;
                                                              PTB=C�d. Filial] }
    { 52112460;;Installment Ret. Base;Decimal     ;CaptionML=[ENU=Installment Ret. Base;
                                                              PTB=Base Reten��o Parcela] }
    { 52112470;;Retention Base     ;Decimal       ;CaptionML=[ENU=Retention Base;
                                                              PTB=Base Reten��o] }
    { 52112480;;BR Prepayment      ;Boolean       ;CaptionML=PTB=Adiantamento }
    { 52112500;;CNAB - Net Amount  ;Decimal       ;CaptionML=[ENU=Net Amount;
                                                              PTB=Valor L�quido] }
    { 52112510;;Branch Code        ;Code20        ;TableRelation="Branch Information";
                                                   CaptionML=[ENU=Branch Code;
                                                              PTB=C�d. Filial] }
    { 52112540;;Tax Payment Bill No.;Code20       ;TableRelation=IF (Positive=CONST(No)) "Tax Payment Bill";
                                                   OnValidate=VAR
                                                                text001@52006501 : TextConst 'ENU=DARF %1 is already associated with entry no. %2.;PTB=O DARF %1 j� est� associado ao movimento n� %2.';
                                                              BEGIN
                                                                TESTFIELD(Open, TRUE);
                                                                TESTFIELD(Positive, FALSE);
                                                              END;

                                                   CaptionML=[ENU=Tax Payment Bill No.;
                                                              PTB=N� Guia Pagamento Imposto] }
    { 52112550;;Tax Jurisdiction Code;Code20      ;TableRelation=IF (Positive=CONST(No)) "Tax Jurisdiction";
                                                   OnValidate=BEGIN
                                                                TESTFIELD(Positive, FALSE);
                                                              END;

                                                   CaptionML=[ENU=Tax Jurisdiction Code;
                                                              PTB=C�d. Jurisdi��o Imposto] }
    { 52112560;;Additional Description;Text100    ;CaptionML=[ENU=Additional Description;
                                                              PTB=Descri��o Adicional] }
    { 52112580;;Tax Entry          ;Boolean       ;CaptionML=[ENU=Tax Entry;
                                                              PTB=Mov. Imposto] }
  }
  KEYS
  {
    {    ;Entry No.                               ;Clustered=Yes }
    {    ;Vendor No.,Posting Date,Due Date         }
    {    ;Vendor No.,Due Date,Posting Date         }
    {    ;Vendor No.,External Document No.         }
    {    ;Vendor No.,Open,Positive,Due Date,Currency Code }
    {    ;Posting Date,Due Date,Vendor No.         }
    {    ;Due Date,Posting Date,Vendor No.         }
  }
  FIELDGROUPS
  {
    { 1   ;DropDown            ;Entry No.,Description,Vendor No.,Posting Date,Document Type,Document No. }
  }
  CODE
  {
    VAR
      Text000@1000 : TextConst 'ENU=must have the same sign as %1;PTB=deve ser do mesmo sinal que %1';
      Text001@1001 : TextConst 'ENU=must not be larger than %1;PTB=n�o tem que ser maior que %1';

    BEGIN
    END.
  }
}

