OBJECT Table 52112502 CST Priority
{
  OBJECT-PROPERTIES
  {
    Date=12/12/14;
    Time=09:13:04;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=CST Priority;
               PTB=Prioridade CST];
  }
  FIELDS
  {
    { 10  ;   ;Tax Identification  ;Option        ;OnValidate=BEGIN
                                                                "CST Code" := '';
                                                              END;

                                                   CaptionML=[ENU=Tax Identification;
                                                              PTB=Identifica��o Imposto];
                                                   OptionCaptionML=[ENU=" ,IPI,ICMS,PIS,COFINS";
                                                                    PTB=" ,IPI,ICMS,PIS,COFINS"];
                                                   OptionString=[ ,IPI,ICMS,PIS,COFINS,II,IRRF,INSS,ISS,CSL Ret.,PIS Ret.,COFINS Ret.,ISS Ret.,INSS Ret.,Dif. Aliq. ICMS,ST,PCC,CIDE,Funrural,Simples] }
    { 20  ;   ;CST Code            ;Code10        ;TableRelation=IF (Tax Identification=CONST(ICMS)) "CST Code".Code
                                                                 ELSE IF (Tax Identification=CONST(IPI)) "CST Impostos".Code WHERE (Tax Type=CONST(IPI))
                                                                 ELSE IF (Tax Identification=CONST(PIS)) "CST Impostos".Code WHERE (Tax Type=CONST(PIS))
                                                                 ELSE IF (Tax Identification=CONST(COFINS)) "CST Impostos".Code WHERE (Tax Type=CONST(COFINS));
                                                   CaptionML=[ENU=CST Code;
                                                              PTB=C�digo CST] }
    { 40  ;   ;Priority            ;Integer       ;CaptionML=[ENU=Priority;
                                                              PTB=Prioridade];
                                                   MinValue=0;
                                                   MaxValue=99 }
  }
  KEYS
  {
    {    ;Tax Identification,CST Code             ;Clustered=Yes }
    {    ;Tax Identification,Priority              }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    PROCEDURE GetDescription@52006508() : Text;
    VAR
      cstCode@52006500 : Record 52112430;
      cstImpostos@52006501 : Record 52112431;
    BEGIN
      IF ("Tax Identification" = "Tax Identification"::" ") OR ("CST Code" = '') THEN
        EXIT('');

      IF "Tax Identification" = "Tax Identification"::ICMS THEN BEGIN
        cstCode.GET("CST Code");
        EXIT(cstCode.Description);
      END ELSE BEGIN
        cstImpostos.GET(GetTaxType, "CST Code");
        EXIT(cstImpostos.Description);
      END;
    END;

    PROCEDURE GetTaxType@52006500() : Integer;
    VAR
      cstImpostos@52006500 : Record 52112431;
    BEGIN
      CASE "Tax Identification" OF
        "Tax Identification"::IPI: EXIT(cstImpostos."Tax Type"::IPI);
        "Tax Identification"::PIS: EXIT(cstImpostos."Tax Type"::PIS);
        "Tax Identification"::COFINS: EXIT(cstImpostos."Tax Type"::COFINS);
      ELSE
        EXIT(0);
      END;
    END;

    PROCEDURE OlderHasPriority@52006527(TaxID@52006500 : Integer;OldCSTCode@52006502 : Code[10];NewCSTCode@52006501 : Code[10]) : Boolean;
    VAR
      CSTPriority@52006503 : Record 52112502;
    BEGIN
      IF OldCSTCode = '' THEN
        EXIT(FALSE);

      IF NewCSTCode = '' THEN
        EXIT(TRUE);

      CSTPriority.SETCURRENTKEY("Tax Identification",Priority);
      CSTPriority.SETRANGE("Tax Identification", TaxID);
      IF CSTPriority.ISEMPTY THEN
        EXIT(FALSE);

      CSTPriority.SETRANGE("CST Code", OldCSTCode);
      IF NOT CSTPriority.FINDFIRST THEN
        EXIT(FALSE);

      CSTPriority.SETRANGE("CST Code", NewCSTCode);
      CSTPriority.SETFILTER(Priority, '<=%1', CSTPriority.Priority);
      EXIT(CSTPriority.ISEMPTY);
    END;

    PROCEDURE Apply@52006501(TaxID@52006500 : Integer;NewCSTCode@52006501 : Code[10];VAR CSTCode@52006504 : Code[10]);
    VAR
      CSTPriority@52006503 : Record 52112502;
    BEGIN
      IF NOT OlderHasPriority(TaxID, CSTCode, NewCSTCode) THEN
        CSTCode := NewCSTCode;
    END;

    BEGIN
    END.
  }
}

