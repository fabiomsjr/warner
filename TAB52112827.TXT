OBJECT Table 52112827 ECD Participants
{
  OBJECT-PROPERTIES
  {
    Date=24/05/16;
    Time=11:32:11;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Participants;
               PTB=Participantes];
    LookupPageID=Page52112830;
    DrillDownPageID=Page52112829;
  }
  FIELDS
  {
    { 1   ;   ;Participant Code    ;Code20        ;TableRelation="ECD Participant Relationship";
                                                   CaptionML=[ENU=Participant Code;
                                                              PTB=C�d. Participante] }
    { 2   ;   ;Participant Name    ;Text50        ;CaptionML=[ENU=Participant Name;
                                                              PTB=Nome Participante] }
    { 3   ;   ;County Code         ;Code5         ;TableRelation=Country/Region;
                                                   CaptionML=[ENU=County Code;
                                                              PTB=C�d. Pa�s] }
    { 4   ;   ;C.N.P.J.            ;Code14        ;OnValidate=BEGIN
                                                                IF "C.N.P.J." <> '' THEN
                                                                  BEGIN
                                                                    OriginalCode := "C.N.P.J.";
                                                                    IF (xRec."C.N.P.J." <> '') THEN
                                                                      BEGIN
                                                                        IF ("C.N.P.J." <> xRec."C.N.P.J.") THEN
                                                                          BEGIN
                                                                            IF CONFIRM(Text003,FALSE) THEN
                                                                              CNPJCheck(OriginalCode)
                                                                            ELSE
                                                                              "C.N.P.J." := xRec."C.N.P.J.";
                                                                          END;
                                                                      END
                                                                        ELSE
                                                                          CNPJCheck(OriginalCode);
                                                                  END;
                                                              END;

                                                   CaptionML=[ENU=C.N.P.J.;
                                                              PTB=C.N.P.J.] }
    { 5   ;   ;C.P.F.              ;Code11        ;OnValidate=BEGIN
                                                                IF "C.P.F." <> '' THEN
                                                                  BEGIN
                                                                    OriginalCode := "C.P.F.";
                                                                    IF (xRec."C.P.F." <> '') THEN
                                                                      BEGIN
                                                                        IF ("C.P.F." <> xRec."C.P.F.") THEN
                                                                          BEGIN
                                                                            IF CONFIRM(Text001,FALSE) THEN
                                                                              CPFCheck(OriginalCode)
                                                                            ELSE
                                                                              "C.P.F." := xRec."C.P.F.";
                                                                          END;
                                                                      END
                                                                        ELSE
                                                                          CPFCheck(OriginalCode);
                                                                  END;
                                                              END;

                                                   CaptionML=[ENU=C.P.F.;
                                                              PTB=C.P.F.] }
    { 6   ;   ;No. Ident. Worker   ;Code11        ;CaptionML=[ENU=No. Ident. Worker;
                                                              PTB=N� Identif. Trabalhador] }
    { 7   ;   ;Federation Unit Participant;Code2  ;CaptionML=[ENU=Federation Unit Participant;
                                                              PTB=Unidade Federa��o do Partic.] }
    { 8   ;   ;Participant State Registration;Code20;
                                                   CaptionML=[ENU=Participant State Registration;
                                                              PTB=Insc. Estadual Partic.] }
    { 9   ;   ;State Reg. Fed. Unit Part.;Code20  ;CaptionML=[ENU=State Registration Federation Unit Participant;
                                                              PTB=Insc. Estadual Partic. Unid. F] }
    { 10  ;   ;Municipality Code   ;Code7         ;TableRelation=Municipio;
                                                   CaptionML=[ENU=Municipality Code;
                                                              PTB=C�d. Munic�pio] }
    { 11  ;   ;Municipal Registration;Code20      ;CaptionML=[ENU=Municipal Registration;
                                                              PTB=Inscri��o Municipal] }
    { 12  ;   ;No. Suframa Registration;Code9     ;CaptionML=[ENU=No. Suframa Registration;
                                                              PTB=N� Inscr. SUFRAMA] }
    { 13  ;   ;Relationship Code   ;Code2         ;TableRelation="ECD Participant Relationship";
                                                   CaptionML=[ENU=Relationship Code;
                                                              PTB=C�d. Relacionamento] }
    { 14  ;   ;Relationship Initial Date;Date     ;CaptionML=[ENU=Relationship Initial Date;
                                                              PTB=Data In�cio Relacionamento] }
    { 15  ;   ;Relationship End Date;Date         ;CaptionML=[ENU=Relationship End Date;
                                                              PTB=Data T�rmino Relacionamento] }
  }
  KEYS
  {
    {    ;Participant Code                        ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      OriginalCode@1102300006 : Code[20];
      Multiply@1102300005 : Integer;
      i@1102300004 : Integer;
      Digit1@1102300003 : Integer;
      Digit2@1102300002 : Integer;
      Calc@1102300001 : Integer;
      Character@1102300000 : Integer;
      Text001@1102300008 : TextConst 'PTB=Confirma que deseja alterar o C.P.F.?';
      Text002@1102300007 : TextConst 'PTB=C.P.F. Inv�lido.';
      Text003@1102300009 : TextConst 'PTB=Confirma que deseja alterar o C.N.P.J.?';
      Text004@1102300010 : TextConst 'PTB=C.N.P.J. Inv�lido.';
      SM@1102300024 : Integer;
      IL@1102300023 : Integer;
      R@1102300022 : Integer;
      D1@1102300021 : Integer;
      W@1102300020 : Integer;

    PROCEDURE CPFCheck@1300001(VAR OriginalCode@1300000 : Code[20]);
    BEGIN
      IF COPYSTR(OriginalCode,10,1) = '-' THEN
        OriginalCode := COPYSTR(OriginalCode,1,9) + COPYSTR(OriginalCode,11,2);

      IF (STRLEN(OriginalCode) < 11) OR (STRLEN(OriginalCode) > 11) THEN
        ERROR(Text002);

      IF (OriginalCode = '00000000000') OR (OriginalCode = '11111111111') OR (OriginalCode = '22222222222')
         OR (OriginalCode = '33333333333') OR (OriginalCode = '44444444444') OR (OriginalCode = '55555555555')
           OR (OriginalCode = '66666666666') OR (OriginalCode = '77777777777')
             OR (OriginalCode = '88888888888') OR (OriginalCode = '99999999999') THEN
        ERROR(Text002);

      Multiply := 10;
      CLEAR(Calc);
      i := 1;
      REPEAT
        BEGIN
          EVALUATE(Character, COPYSTR(OriginalCode,i,1));
          Calc := Calc + (Character * Multiply);
          Multiply := Multiply - 1;
          i := i + 1;
        END;
      UNTIL i = STRLEN(COPYSTR(OriginalCode,1,10));
      Digit1 := Calc MOD 11;
      Digit1 := 11 - Digit1;
      IF Digit1 >= 10 THEN
        Digit1 := 0;

      Multiply := 11;
      CLEAR(Calc);
      i := 1;
      REPEAT
        BEGIN
          EVALUATE(Character, COPYSTR(OriginalCode,i,1));
          Calc := Calc + (Character * Multiply);
          Multiply := Multiply - 1;
          i := i + 1;
        END;
      UNTIL i = STRLEN(COPYSTR(OriginalCode,1,11));
      Digit2 := Calc MOD 11;
      Digit2 := 11 - Digit2;
      IF Digit2 >= 10 THEN
        Digit2 := 0;

      EVALUATE(Character, COPYSTR(OriginalCode,10,1));
      IF Digit1 <> Character THEN
        ERROR(Text002);

      EVALUATE(Character, COPYSTR(OriginalCode,11,1));
      IF Digit2 <> Character THEN
        ERROR(Text002);
    END;

    PROCEDURE CNPJCheck@1300002(VAR CN@1300000 : Code[20]) : Boolean;
    BEGIN
      {Ciffer: 1 2 3 4 5 6 7 8 9 10 11 12 - C1 C2}
      {Weight: 5 4 3 2 9 9 7 6 5  4  3  2 }

      IF COPYSTR(OriginalCode,13,1) = 'Ciffer: 1 2 3 4 5 6 7 8 9 10 11 12 13' THEN
        OriginalCode := COPYSTR(OriginalCode,1,12) + COPYSTR(OriginalCode,14,2);

      IF (STRLEN(OriginalCode) < 14) OR (STRLEN(OriginalCode) > 14) THEN
        ERROR(Text004);

      SM := (Ev(CN, 12) + Ev(CN,4)) * 2 + (Ev(CN,11) + Ev(CN,3)) * 3;
      SM := SM + ((Ev(CN,10) + Ev(CN,2)) * 4) + (Ev(CN,8) * 6);
      SM := SM + ((Ev(CN,9) + Ev(CN,1)) * 5) + (Ev(CN,7) * 7);
      SM := SM + Ev(CN,6) * 8 + Ev(CN,5) * 9;

      IL  := ROUND(SM / 11,1,'Weight: 6 5 4 3 2 9 8 7 6  5  4  3  2');
      R   := SM - (IL * 11);
      D1  := 11 - R;
      IF (R = 0) OR (R = 1) THEN D1 := 0;

      IF D1 <> Ev(CN,13) THEN EXIT(FALSE);

      {Ciffer: 1 2 3 4 5 6 7 8 9 10 11 12 13}
      {Weight: 6 5 4 3 2 9 8 7 6  5  4  3  2}

      SM := (Ev(CN,13) + Ev(CN,5)) * 2 + (Ev(CN,12) + Ev(CN,4)) * 3+
            (Ev(CN,11) + Ev(CN,3)) * 4;
      SM := SM + ((Ev(CN,10) + Ev(CN,2)) * 5) + (Ev(CN,8) * 7);
      SM := SM + ((Ev(CN,9) + Ev(CN,1)) * 6) + Ev(CN,7) * 8 + Ev(CN,6) * 9;

      IL  := ROUND(SM / 11,1,'<');
      R   := SM - (IL * 11);
      D1  := 11 - R;
      IF (R = 0) OR (R = 1) THEN D1 := 0;

      IF D1 <> Ev(CN,14) THEN EXIT(FALSE);

      EXIT(TRUE);
    END;

    PROCEDURE Ev@1300000(VAR TC@1300000 : Code[20];TN@1300001 : Integer) k : Integer;
    BEGIN
      IF STRLEN(TC) < TN THEN ERROR(Text004);
      IF EVALUATE(k,COPYSTR(TC,TN,1)) = FALSE THEN ERROR(Text004);
    END;

    BEGIN
    END.
  }
}

