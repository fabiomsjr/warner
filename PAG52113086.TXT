OBJECT Page 52113086 Document Approval Rules
{
  OBJECT-PROPERTIES
  {
    Date=20/03/15;
    Time=14:42:12;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Document Approval Rules;
               PTB=Regras Aprova��o Documento];
    SourceTable=Table52113083;
    DelayedInsert=Yes;
    SourceTableView=SORTING(Dimension 1 Value Code,Dimension 2 Value Code);
    PageType=List;
    OnOpenPage=VAR
                 DocApprovSetup@52006500 : Record 52113082;
                 TypeFilter@52006501 : Integer;
                 DocTypeFilter@52006502 : Integer;
               BEGIN
                 TypeFilter := GETRANGEMIN(Type);
                 DocTypeFilter := GETRANGEMIN("Document Type");

                 IF DocApprovSetup.GET(TypeFilter, DocTypeFilter) THEN BEGIN
                   UpdateDimension1Column(DocApprovSetup);
                   UpdateDimension2Column(DocApprovSetup);

                   SalesColumnsVisible := (TypeFilter = Type::Sale) AND
                                          (DocTypeFilter IN ["Document Type"::Quote,"Document Type"::Order,"Document Type"::Invoice]);
                 END;
               END;

    OnAfterGetCurrRecord=BEGIN
                           CALCFIELDS("Dimension 1 Code");
                           CALCFIELDS("Dimension 2 Code");
                         END;

    ActionList=ACTIONS
    {
      { 52006508;    ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 52006509;1   ;Action    ;
                      CaptionML=[ENU=Open &Rule;
                                 PTB=Abrir &Regra];
                      RunObject=Page 52113078;
                      RunPageLink=Code=FIELD(Approval Flow Rule Code);
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=CheckRulesSyntax;
                      PromotedCategory=Process }
    }
  }
  CONTROLS
  {
    { 52006500;0;Container;
                ContainerType=ContentArea }

    { 52006501;1;Group    ;
                Name=Group;
                GroupType=Repeater }

    { 52006504;2;Field    ;
                SourceExpr="Dimension 1 Value Code";
                CaptionClass=Dim1Caption;
                Visible=Dim1Visible }

    { 52006506;2;Field    ;
                SourceExpr="Dimension 2 Value Code";
                CaptionClass=Dim2Caption;
                Visible=Dim2Visible }

    { 52006507;2;Field    ;
                SourceExpr="Approval Flow Rule Code" }

    { 52006502;2;Field    ;
                SourceExpr="Sales Credit Limit";
                Visible=SalesColumnsVisible }

  }
  CODE
  {
    VAR
      Dim1Visible@52006500 : Boolean;
      Dim2Visible@52006501 : Boolean;
      Dim1Caption@52006502 : Text;
      Dim2Caption@52006503 : Text;
      SalesColumnsVisible@52006504 : Boolean;

    LOCAL PROCEDURE UpdateDimension1Column@52006502(SalesApprovSetup@52006500 : Record 52113082);
    BEGIN
      UpdateDimensionColumn(SalesApprovSetup, 1);
    END;

    LOCAL PROCEDURE UpdateDimension2Column@52006503(SalesApprovSetup@52006500 : Record 52113082);
    BEGIN
      UpdateDimensionColumn(SalesApprovSetup, 2);
    END;

    LOCAL PROCEDURE UpdateDimensionColumn@35000006(SalesApprovSetup@52006501 : Record 52113082;WhichDim@52006500 : Integer);
    VAR
      Dimension@35000000 : Record 348;
    BEGIN
      IF WhichDim = 1 THEN BEGIN
        IF NOT Dimension.GET(SalesApprovSetup."Dimension 1 Code") THEN
          Dim1Caption := FIELDCAPTION("Dimension 1 Value Code")
        ELSE BEGIN
          Dim1Visible := TRUE;
          Dim1Caption := Dimension."Code Caption";
        END;
      END ELSE BEGIN
        IF NOT Dimension.GET(SalesApprovSetup."Dimension 2 Code") THEN
          Dim2Caption := FIELDCAPTION("Dimension 2 Value Code")
        ELSE BEGIN
          Dim2Visible := TRUE;
          Dim2Caption := Dimension."Code Caption";
        END;
      END;
    END;

    BEGIN
    END.
  }
}

