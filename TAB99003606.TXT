OBJECT Table 99003606 Field/Control
{
  OBJECT-PROPERTIES
  {
    Date=22/09/04;
    Time=11:52:42;
    Version List=NDT2.00.01;
  }
  PROPERTIES
  {
  }
  FIELDS
  {
    { 1   ;   ;Entry No.           ;Integer        }
    { 2   ;   ;Version No.         ;Integer        }
    { 3   ;   ;Reference No.       ;Integer        }
    { 4   ;   ;Object Type         ;Option        ;OptionString=Table,Form,Report,Dataport,Codeunit,XMLport,MenuSuite }
    { 5   ;   ;Object ID           ;Integer        }
    { 6   ;   ;No.                 ;Integer        }
    { 7   ;   ;SubItem No.         ;Integer        }
    { 8   ;   ;Enabled             ;Boolean        }
    { 9   ;   ;Name                ;Text80         }
    { 10  ;   ;Type                ;Option        ;OptionString=Binary,BLOB,Boolean,CheckBox,Code,Control,CommandButton,DataportField,Date,Decimal,Frame,Integer,Label,MatrixBox,MenuButton,Option,OptionButton,PictureBox,SubForm,TabControl,TableBox,Text,TextBox,Time,Image,Shape,Indicator,DateFormula,TableFilter,Table,Field,Literal,RecordID,MenuNode,BigInteger,BigText }
    { 11  ;   ;Type Name           ;Text15         }
    { 12  ;   ;X                   ;Integer        }
    { 13  ;   ;Y                   ;Integer        }
    { 14  ;   ;Width               ;Integer        }
    { 15  ;   ;Height              ;Integer        }
    { 16  ;   ;Property Reference No.;Integer      }
    { 17  ;   ;Function Reference No.;Integer      }
    { 18  ;   ;Menu Level          ;Integer        }
    { 19  ;   ;Object Consecutive No.;Integer      }
    { 20  ;   ;TagType             ;Option        ;OptionString=,Element,Attribute }
    { 21  ;   ;Indentation         ;Integer        }
  }
  KEYS
  {
    {    ;Entry No.                               ;MaintainSIFTIndex=No;
                                                   Clustered=Yes }
    {    ;Reference No.,No.,SubItem No.,Type      ;MaintainSIFTIndex=No }
    {    ;Reference No.,Type                      ;MaintainSIFTIndex=No }
    {    ;Version No.,Object Type,Object ID,Property Reference No.,Function Reference No.;
                                                   MaintainSIFTIndex=No }
    {    ;Version No.,Property Reference No.      ;MaintainSIFTIndex=No }
    {    ;Version No.,Function Reference No.      ;MaintainSIFTIndex=No }
    {    ;Reference No.,Entry No.                 ;MaintainSIFTIndex=No }
    {    ;Version No.,Object Type,Object ID,Name  ;MaintainSIFTIndex=No }
    {    ;Version No.,Object Type,Object ID,No.,SubItem No.,Name,Type;
                                                   MaintainSIFTIndex=No }
    {    ;Reference No.,No.,SubItem No.           ;MaintainSIFTIndex=No }
    {    ;Version No.,Reference No.,Object Type,Object ID,Name,Type;
                                                   MaintainSIFTIndex=No }
    {    ;Version No.,Object Type,Object ID,No.   ;MaintainSIFTIndex=No }
    {    ;Version No.,Object Type,Object ID,SubItem No.;
                                                   MaintainSIFTIndex=No }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

