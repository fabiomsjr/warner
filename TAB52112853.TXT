OBJECT Table 52112853 Relation Between Accounts
{
  OBJECT-PROPERTIES
  {
    Date=15/03/17;
    Time=17:15:09;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Relation Between Accounts;
               PTB=Relacao Entre Contas];
  }
  FIELDS
  {
    { 1   ;   ;Account Plan Reference;Code20      ;TableRelation="Reference Chart of Accounts";
                                                   CaptionML=[ENU=Account Plan Reference;
                                                              PTB=Conta Plano Referencial] }
    { 2   ;   ;Chart of Account    ;Code20        ;TableRelation="ECD Chart of Accounts";
                                                   OnValidate=BEGIN
                                                                IF "Chart of Account" = '' THEN
                                                                  VALIDATE("Cost Center Code", '');
                                                              END;

                                                   CaptionML=[ENU=Chart of Account;
                                                              PTB=Conta Plano] }
    { 3   ;   ;Name Chart of Account;Text50       ;FieldClass=FlowField;
                                                   CalcFormula=Lookup("ECD Chart of Accounts".Name WHERE (No=FIELD(Chart of Account)));
                                                   CaptionML=[ENU=Name Chart of Account;
                                                              PTB=Nome Conta Plano];
                                                   Editable=No }
    { 4   ;   ;Cost Center Code    ;Code20        ;TableRelation="ECD Account Cost Center"."Cost Center Code" WHERE (Account No.=FIELD(Chart of Account));
                                                   CaptionML=[ENU=Cost Center Code;
                                                              PTB=C�d. Centro de Custo] }
  }
  KEYS
  {
    {    ;Account Plan Reference,Chart of Account,Cost Center Code;
                                                   Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

