OBJECT Page 358 Objects
{
  OBJECT-PROPERTIES
  {
    Date=14/08/09;
    Time=12:00:00;
    Version List=NAVW16.00.01;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Objects;
               PTB=Objetos];
    SourceTable=Table2000000001;
    PageType=List;
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 11  ;2   ;Field     ;
                SourceExpr=Type;
                Visible=FALSE }

    { 2   ;2   ;Field     ;
                CaptionML=[ENU=ID;
                           PTB=ID];
                SourceExpr=ID }

    { 4   ;2   ;Field     ;
                DrillDown=No;
                CaptionML=[ENU=Name;
                           PTB=Name];
                SourceExpr=Caption }

    { 12  ;2   ;Field     ;
                CaptionML=[ENU=Object Name;
                           PTB=Object Name];
                SourceExpr=Name;
                Visible=FALSE }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

