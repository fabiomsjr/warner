OBJECT Table 5847 Average Cost Calc. Overview
{
  OBJECT-PROPERTIES
  {
    Date=26/10/15;
    Time=11:36:41;
    Version List=NAVW17.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Average Cost Calc. Overview;
               PTB=Visualiza��o Calc. Custo M�dio];
  }
  FIELDS
  {
    { 1   ;   ;Entry No.           ;Integer       ;CaptionML=[ENU=Entry No.;
                                                              PTB=N� Mov.] }
    { 2   ;   ;Type                ;Option        ;CaptionML=[ENU=Type;
                                                              PTB=Tipo];
                                                   OptionCaptionML=[ENU=Closing Entry,Increase,Applied Increase,Applied Decrease,Decrease,Revaluation;
                                                                    PTB=Fechamento,Entrada,Entrada Aplicada,Sa�da Aplicada,Sa�da,Valoriza��o];
                                                   OptionString=Closing Entry,Increase,Applied Increase,Applied Decrease,Decrease,Revaluation }
    { 3   ;   ;Valuation Date      ;Date          ;CaptionML=[ENU=Valuation Date;
                                                              PTB=Data Avalia��o] }
    { 4   ;   ;Item No.            ;Code20        ;TableRelation=Item;
                                                   CaptionML=[ENU=Item No.;
                                                              PTB=N� Produto] }
    { 5   ;   ;Location Code       ;Code10        ;TableRelation=Location;
                                                   CaptionML=[ENU=Location Code;
                                                              PTB=Cod. Dep�sito] }
    { 6   ;   ;Variant Code        ;Code10        ;TableRelation="Item Variant".Code WHERE (Item No.=FIELD(Item No.));
                                                   CaptionML=[ENU=Variant Code;
                                                              PTB=C�digo Variante] }
    { 7   ;   ;Cost is Adjusted    ;Boolean       ;CaptionML=[ENU=Cost is Adjusted;
                                                              PTB=Custo est� Ajustado] }
    { 11  ;   ;Attached to Entry No.;Integer      ;TableRelation="Item Ledger Entry";
                                                   CaptionML=[ENU=Attached to Entry No.;
                                                              PTB=Anexado ao N� Mov.] }
    { 12  ;   ;Attached to Valuation Date;Date    ;CaptionML=[ENU=Attached to Valuation Date;
                                                              PTB=Anexado a Data de Avalia��o] }
    { 13  ;   ;Level               ;Integer       ;CaptionML=[ENU=Level;
                                                              PTB=N�vel] }
    { 21  ;   ;Item Ledger Entry No.;Integer      ;TableRelation="Item Ledger Entry";
                                                   CaptionML=[ENU=Item Ledger Entry No.;
                                                              PTB=N� Mov. Produto] }
    { 22  ;   ;Posting Date        ;Date          ;CaptionML=[ENU=Posting Date;
                                                              PTB=Data Registo] }
    { 23  ;   ;Entry Type          ;Option        ;CaptionML=[ENU=Entry Type;
                                                              PTB=Tipo Mov.];
                                                   OptionCaptionML=[ENU=Purchase,Sale,Positive Adjmt.,Negative Adjmt.,Transfer,Consumption,Output, ,Assembly Consumption,Assembly Output;
                                                                    PTB=Compra,Venda,Ajuste Positivo,Ajuste Negativo,Transf.,Consumo,Sa�da, ,Consumo Montagem,Sa�da Montagem];
                                                   OptionString=Purchase,Sale,Positive Adjmt.,Negative Adjmt.,Transfer,Consumption,Output, ,Assembly Consumption,Assembly Output }
    { 24  ;   ;Document Type       ;Option        ;CaptionML=[ENU=Document Type;
                                                              PTB=Tipo Documento];
                                                   OptionCaptionML=[ENU=" ,Sales Shipment,Sales Invoice,Sales Return Receipt,Sales Credit Memo,Purchase Receipt,Purchase Invoice,Purchase Return Shipment,Purchase Credit Memo,Transfer Shipment,Transfer Receipt,Service Shipment,Service Invoice,Service Credit Memo";
                                                                    PTB=" ,Envio Vendas,Nota Fiscal Vendas,Recebimento Devolu��o,Nota Cr�dito Venda,Recebimento Compra,Nota Fiscal Compra,Envio Retorno Compra,Nota Cr�dito Compra,Envio Transf.,Recebimento Transfer.,Envio Servi�o,Nota Fiscal de Servi�o,Nota Cr�dito Servi�o"];
                                                   OptionString=[ ,Sales Shipment,Sales Invoice,Sales Return Receipt,Sales Credit Memo,Purchase Receipt,Purchase Invoice,Purchase Return Shipment,Purchase Credit Memo,Transfer Shipment,Transfer Receipt,Service Shipment,Service Invoice,Service Credit Memo] }
    { 25  ;   ;Document No.        ;Code20        ;CaptionML=[ENU=Document No.;
                                                              PTB=N� Documento] }
    { 26  ;   ;Document Line No.   ;Integer       ;CaptionML=[ENU=Document Line No.;
                                                              PTB=N� Linha Documento] }
    { 27  ;   ;Description         ;Text50        ;CaptionML=[ENU=Description;
                                                              PTB=Descri��o] }
    { 31  ;   ;Quantity            ;Decimal       ;CaptionML=[ENU=Quantity;
                                                              PTB=Quantidade];
                                                   DecimalPlaces=0:5 }
    { 32  ;   ;Applied Quantity    ;Integer       ;CaptionML=[ENU=Applied Quantity;
                                                              PTB=Quantidade Aplicada] }
    { 33  ;   ;Cost Amount (Expected);Decimal     ;CaptionML=[ENU=Cost Amount (Expected);
                                                              PTB=Valor Custo (Esperado)] }
    { 34  ;   ;Cost Amount (Actual);Decimal       ;CaptionML=[ENU=Cost Amount (Actual);
                                                              PTB=Valor Custo (Real)] }
  }
  KEYS
  {
    {    ;Entry No.                               ;Clustered=Yes }
    {    ;Attached to Valuation Date,Attached to Entry No.,Type }
    {    ;Item Ledger Entry No.                    }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      InvtSetup@1001 : Record 313;
      ValueEntry@1003 : Record 5802;
      InvtSetupRead@1002 : Boolean;

    PROCEDURE CalculateAverageCost@5801() AverageCost : Decimal;
    BEGIN
      AverageCost := 0;
      IF Type = Type::"Closing Entry" THEN BEGIN
        SetItemFilters;
        ValueEntry.SumCostsTillValuationDate(ValueEntry);
        IF ValueEntry."Item Ledger Entry Quantity" = 0 THEN
          EXIT(AverageCost);
        AverageCost :=
          (ValueEntry."Cost Amount (Actual)" + ValueEntry."Cost Amount (Expected)") /
          ValueEntry."Item Ledger Entry Quantity";
        EXIT(ROUND(AverageCost));
      END;
      IF Quantity = 0 THEN
        EXIT(AverageCost);
      AverageCost := ("Cost Amount (Actual)" + "Cost Amount (Expected)") / Quantity;
      EXIT(ROUND(AverageCost));
    END;

    PROCEDURE CalculateRemainingQty@1() : Decimal;
    BEGIN
      IF Type <> Type::"Closing Entry" THEN
        EXIT(0);
      SetItemFilters;
      ValueEntry.SumCostsTillValuationDate(ValueEntry);
      EXIT(ValueEntry."Item Ledger Entry Quantity");
    END;

    PROCEDURE CalculateCostAmt@4(Actual@1000 : Boolean) : Decimal;
    BEGIN
      IF Type <> Type::"Closing Entry" THEN
        EXIT(0);
      SetItemFilters;
      ValueEntry.SumCostsTillValuationDate(ValueEntry);
      IF Actual THEN
        EXIT(ValueEntry."Cost Amount (Actual)");
      EXIT(ValueEntry."Cost Amount (Expected)");
    END;

    PROCEDURE SetItemFilters@8();
    BEGIN
      ValueEntry."Item No." := "Item No.";
      ValueEntry."Valuation Date" := "Valuation Date";
      ValueEntry."Location Code" := "Location Code";
      ValueEntry."Variant Code" := "Variant Code";
    END;

    PROCEDURE GetInvtSetup@3();
    BEGIN
      IF InvtSetupRead THEN
        EXIT;
      InvtSetup.GET;
      InvtSetupRead := TRUE;
    END;

    BEGIN
    END.
  }
}

