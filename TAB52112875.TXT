OBJECT Table 52112875 ECF A-Part Related Amount
{
  OBJECT-PROPERTIES
  {
    Date=25/06/15;
    Time=15:31:45;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=A-Part Related Amount;
               PTB=Valor Relacionado Parte A];
    DrillDownPageID=Page52112889;
  }
  FIELDS
  {
    { 10  ;   ;Posting Code        ;Code10        ;TableRelation="ECF Posting Code".Code;
                                                   CaptionML=[ENU=Posting Code;
                                                              PTB=C�d. Lan�amento] }
    { 20  ;   ;Line No.            ;Integer       ;CaptionML=[ENU=Line No.;
                                                              PTB=N� Linha] }
    { 30  ;   ;Date                ;Date          ;CaptionML=[ENU=Date;
                                                              PTB=Data];
                                                   NotBlank=Yes }
    { 40  ;   ;Amount              ;Decimal       ;CaptionML=[ENU=Amount;
                                                              PTB=Valor];
                                                   DecimalPlaces=2:2;
                                                   NotBlank=Yes;
                                                   BlankNumbers=BlankZero }
  }
  KEYS
  {
    {    ;Posting Code,Line No.,Date              ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

