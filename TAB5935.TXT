OBJECT Table 5935 Service E-Mail Queue
{
  OBJECT-PROPERTIES
  {
    Date=07/09/12;
    Time=12:00:00;
    Version List=NAVW17.00;
  }
  PROPERTIES
  {
    OnInsert=BEGIN
               IF "Entry No." = 0 THEN BEGIN
                 ServEMailQueue.RESET;
                 IF ServEMailQueue.FINDLAST THEN
                   "Entry No." := ServEMailQueue."Entry No." + 1
                 ELSE
                   "Entry No." := 1;
               END;

               "Sending Date" := TODAY;
               "Sending Time" := TIME;
             END;

    CaptionML=[ENU=Service E-Mail Queue;
               PTB=Fila E-Mail Servi�o];
  }
  FIELDS
  {
    { 1   ;   ;Entry No.           ;Integer       ;CaptionML=[ENU=Entry No.;
                                                              PTB=N� Mov.] }
    { 2   ;   ;To Address          ;Text80        ;CaptionML=[ENU=To Address;
                                                              PTB=Endere�o Para] }
    { 3   ;   ;Copy-to Address     ;Text80        ;CaptionML=[ENU=Copy-to Address;
                                                              PTB=Endere�o C�pia-para] }
    { 4   ;   ;Subject Line        ;Text250       ;CaptionML=[ENU=Subject Line;
                                                              PTB=Linha Assunto] }
    { 5   ;   ;Body Line           ;Text250       ;CaptionML=[ENU=Body Line;
                                                              PTB=Linha Mensagem] }
    { 6   ;   ;Attachment Filename ;Text80        ;CaptionML=[ENU=Attachment Filename;
                                                              PTB=Arquivo Anexo] }
    { 7   ;   ;Sending Date        ;Date          ;CaptionML=[ENU=Sending Date;
                                                              PTB=Data Envio] }
    { 8   ;   ;Sending Time        ;Time          ;CaptionML=[ENU=Sending Time;
                                                              PTB=Hora Envio] }
    { 9   ;   ;Status              ;Option        ;CaptionML=[ENU=Status;
                                                              PTB=Status];
                                                   OptionCaptionML=[ENU=" ,Processed,Error";
                                                                    PTB=" ,Processado,Erro"];
                                                   OptionString=[ ,Processed,Error];
                                                   Editable=No }
    { 10  ;   ;Document Type       ;Option        ;CaptionML=[ENU=Document Type;
                                                              PTB=Tipo Documento];
                                                   OptionCaptionML=[ENU=" ,Service Order";
                                                                    PTB=" ,Ordem de Servi�o"];
                                                   OptionString=[ ,Service Order] }
    { 11  ;   ;Document No.        ;Code20        ;CaptionML=[ENU=Document No.;
                                                              PTB=No. Documento] }
  }
  KEYS
  {
    {    ;Entry No.                               ;Clustered=Yes }
    {    ;Status,Sending Date,Document Type,Document No. }
    {    ;Document Type,Document No.,Status,Sending Date }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      ServEMailQueue@1000 : Record 5935;

    BEGIN
    END.
  }
}

