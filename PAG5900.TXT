OBJECT Page 5900 Service Order
{
  OBJECT-PROPERTIES
  {
    Date=09/09/14;
    Time=12:00:00;
    Version List=NAVW18.00,NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Service Order;
               PTB=Ordem Servi�o];
    SourceTable=Table5900;
    SourceTableView=WHERE(Document Type=FILTER(Order));
    PageType=Document;
    RefreshOnActivate=Yes;
    PromotedActionCategoriesML=ENU=New,Process,Report,Warehouse;
    OnOpenPage=VAR
                 ">NAVBR"@52006500 : Integer;
                 servMgtSetup@52006501 : Record 5911;
               BEGIN
                 IF UserMgt.GetServiceFilter <> '' THEN BEGIN
                   FILTERGROUP(2);
                   SETRANGE("Responsibility Center",UserMgt.GetServiceFilter);
                   FILTERGROUP(0);
                 END;

                 // > NAVBR
                 servMgtSetup.GET;
                 AllowInvoicing := servMgtSetup."Allow Service Order Invoicing";
                 // < NAVBR
               END;

    OnNewRecord=BEGIN
                  "Document Type" := "Document Type"::Order;
                  "Responsibility Center" := UserMgt.GetServiceFilter;
                END;

    OnDeleteRecord=BEGIN
                     CurrPage.SAVERECORD;
                     CLEAR(ServLogMgt);
                     ServLogMgt.ServHeaderManualDelete(Rec);
                     EXIT(ConfirmDeletion);
                   END;

    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 138     ;1   ;ActionGroup;
                      CaptionML=[ENU=O&rder;
                                 PTB=O&rdem];
                      Image=Order }
      { 74      ;2   ;Separator  }
      { 137     ;2   ;Action    ;
                      CaptionML=[ENU=Demand Overview;
                                 PTB=Vis�o Geral Demanda];
                      Image=Forecast;
                      OnAction=VAR
                                 DemandOverview@1000 : Page 5830;
                               BEGIN
                                 DemandOverview.SetCalculationParameter(TRUE);
                                 DemandOverview.Initialize(0D,4,"No.",'','');
                                 DemandOverview.RUNMODAL;
                               END;
                                }
      { 3       ;2   ;Separator  }
      { 7       ;2   ;Action    ;
                      Name=<Action7>;
                      AccessByPermission=TableData 99000880=R;
                      CaptionML=[ENU=Order Promising;
                                 PTB=Compromisso Ordem];
                      Image=OrderPromising;
                      OnAction=VAR
                                 OrderPromisingLine@1001 : Record 99000880;
                                 OrderPromisingLines@1000 : Page 99000959;
                               BEGIN
                                 CLEAR(OrderPromisingLines);
                                 OrderPromisingLines.SetSourceType(12); // Service order
                                 CLEAR(OrderPromisingLine);
                                 OrderPromisingLine.SETRANGE("Source Type",OrderPromisingLine."Source Type"::"Service Order");
                                 OrderPromisingLine.SETRANGE("Source ID","No.");
                                 OrderPromisingLines.SETTABLEVIEW(OrderPromisingLine);
                                 OrderPromisingLines.RUNMODAL;
                               END;
                                }
      { 166     ;2   ;Separator ;
                      CaptionML=ENU="" }
      { 131     ;2   ;Action    ;
                      ShortCutKey=Shift+F7;
                      CaptionML=[ENU=&Customer Card;
                                 PTB=Cart�o &Cliente];
                      RunObject=Page 21;
                      RunPageLink=No.=FIELD(Customer No.);
                      Image=Customer }
      { 206     ;2   ;Action    ;
                      AccessByPermission=TableData 348=R;
                      ShortCutKey=Shift+Ctrl+D;
                      CaptionML=[ENU=&Dimensions;
                                 PTB=&Dimens�es];
                      Image=Dimensions;
                      OnAction=BEGIN
                                 ShowDocDim;
                               END;
                                }
      { 52006548;2   ;Action    ;
                      CaptionML=[ENU=Additional Text;
                                 PTB=Textos Adicionais];
                      RunObject=Page 52112436;
                      RunPageView=SORTING(Type,Document Type,Document No.,Order No.);
                      RunPageLink=Type=CONST(Service),
                                  Document Type=CONST(Order),
                                  Document No.=FIELD(No.);
                      Image=Text }
      { 170     ;2   ;Separator ;
                      CaptionML=ENU="" }
      { 20      ;2   ;Action    ;
                      CaptionML=[ENU=Service Document Lo&g;
                                 PTB=Lo&g Documento Servi�o];
                      Image=Log;
                      OnAction=VAR
                                 ServDocLog@1001 : Record 5912;
                               BEGIN
                                 ServDocLog.ShowServDocLog(Rec);
                               END;
                                }
      { 9       ;2   ;Action    ;
                      CaptionML=[ENU=E-Mail &Queue;
                                 PTB=Fila E-Mail];
                      RunObject=Page 5961;
                      RunPageView=SORTING(Document Type,Document No.);
                      RunPageLink=Document Type=CONST(Service Order),
                                  Document No.=FIELD(No.);
                      Image=Email }
      { 21      ;2   ;Action    ;
                      CaptionML=[ENU=Co&mments;
                                 PTB=Co&ment�rios];
                      RunObject=Page 5911;
                      RunPageLink=Table Name=CONST(Service Header),
                                  Table Subtype=FIELD(Document Type),
                                  No.=FIELD(No.),
                                  Type=CONST(General);
                      Image=ViewComments }
      { 36      ;1   ;ActionGroup;
                      Name=<Action36>;
                      CaptionML=[ENU=Statistics;
                                 PTB=Estat�sticas];
                      Image=Statistics }
      { 5       ;2   ;Separator ;
                      CaptionML=ENU="" }
      { 102     ;2   ;Action    ;
                      Name=Statistics;
                      ShortCutKey=F7;
                      CaptionML=[ENU=Statistics;
                                 PTB=Estat�sticas];
                      Promoted=Yes;
                      Image=Statistics;
                      PromotedCategory=Process;
                      OnAction=VAR
                                 SalesSetup@1000 : Record 311;
                                 ServLine@1001 : Record 5902;
                                 ServLines@1003 : Page 5905;
                               BEGIN
                                 SalesSetup.GET;
                                 IF SalesSetup."Calc. Inv. Discount" THEN BEGIN
                                   ServLine.RESET;
                                   ServLine.SETRANGE("Document Type","Document Type");
                                   ServLine.SETRANGE("Document No.","No.");
                                   IF ServLine.FINDFIRST THEN BEGIN
                                     ServLines.SETTABLEVIEW(ServLine);
                                     ServLines.CalcInvDisc(ServLine);
                                     COMMIT
                                   END;
                                 END;
                                 PAGE.RUNMODAL(PAGE::"Service Order Statistics",Rec);
                               END;
                                }
      { 49      ;2   ;Separator ;
                      CaptionML=ENU="" }
      { 37      ;1   ;ActionGroup;
                      CaptionML=[ENU=Documents;
                                 PTB=Documentos];
                      Image=Documents }
      { 41      ;2   ;Action    ;
                      CaptionML=[ENU=S&hipments;
                                 PTB=Envios];
                      RunObject=Page 5974;
                      RunPageView=SORTING(Order No.);
                      RunPageLink=Order No.=FIELD(No.);
                      Promoted=Yes;
                      Image=Shipment;
                      PromotedCategory=Process }
      { 60      ;2   ;Action    ;
                      CaptionML=[ENU=Invoices;
                                 PTB=Notas Fiscais];
                      RunObject=Page 5977;
                      RunPageView=SORTING(Order No.);
                      RunPageLink=Order No.=FIELD(No.);
                      Image=Invoice }
      { 29      ;1   ;ActionGroup;
                      CaptionML=[ENU=W&arehouse;
                                 PTB=Armaz�m];
                      Image=Warehouse }
      { 11      ;2   ;Action    ;
                      CaptionML=[ENU=Whse. Shipment Lines;
                                 PTB=Linhas Envio Armaz�m];
                      RunObject=Page 7341;
                      RunPageView=SORTING(Source Type,Source Subtype,Source No.,Source Line No.);
                      RunPageLink=Source Type=CONST(5902),
                                  Source Subtype=FIELD(Document Type),
                                  Source No.=FIELD(No.);
                      Promoted=Yes;
                      Image=ShipmentLines;
                      PromotedCategory=Category4 }
      { 35      ;1   ;ActionGroup;
                      CaptionML=[ENU=History;
                                 PTB=Hist�rico];
                      Image=History }
      { 155     ;2   ;Action    ;
                      ShortCutKey=Ctrl+F7;
                      CaptionML=[ENU=Service Ledger E&ntries;
                                 PTB=Movime&ntos Servi�o];
                      RunObject=Page 5912;
                      RunPageView=SORTING(Service Order No.,Service Item No. (Serviced),Entry Type,Moved from Prepaid Acc.,Posting Date,Open,Type);
                      RunPageLink=Service Order No.=FIELD(No.);
                      Image=ServiceLedger }
      { 142     ;2   ;Action    ;
                      CaptionML=[ENU=&Warranty Ledger Entries;
                                 PTB=Movimentos &Garantia];
                      RunObject=Page 5913;
                      RunPageView=SORTING(Service Order No.,Posting Date,Document No.);
                      RunPageLink=Service Order No.=FIELD(No.);
                      Image=WarrantyLedger }
      { 105     ;2   ;Action    ;
                      CaptionML=[ENU=&Job Ledger Entries;
                                 PTB=Movimentos &Projeto];
                      RunObject=Page 92;
                      RunPageView=SORTING(Service Order No.,Posting Date)
                                  WHERE(Entry Type=CONST(Usage));
                      RunPageLink=Service Order No.=FIELD(No.);
                      Image=JobLedger }
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 18      ;1   ;ActionGroup;
                      CaptionML=[ENU=F&unctions;
                                 PTB=F&un��es];
                      Image=Action }
      { 152     ;2   ;Action    ;
                      Name=Create Customer;
                      CaptionML=[ENU=&Create Customer;
                                 PTB=&Criar Cliente];
                      Image=NewCustomer;
                      OnAction=BEGIN
                                 CLEAR(ServOrderMgt);
                                 ServOrderMgt.CreateNewCustomer(Rec);
                                 CurrPage.UPDATE(TRUE);
                               END;
                                }
      { 27      ;1   ;ActionGroup;
                      CaptionML=[ENU=W&arehouse;
                                 PTB=Armaz�m];
                      Image=Warehouse }
      { 12      ;2   ;Action    ;
                      ShortCutKey=Ctrl+F9;
                      CaptionML=[ENU=Release to Ship;
                                 PTB=&Liberar para Envio];
                      Promoted=Yes;
                      Image=ReleaseShipment;
                      PromotedCategory=Category4;
                      OnAction=VAR
                                 ReleaseServiceDocument@1000 : Codeunit 416;
                               BEGIN
                                 ReleaseServiceDocument.PerformManualRelease(Rec);
                               END;
                                }
      { 13      ;2   ;Action    ;
                      Name=Reopen;
                      CaptionML=[ENU=Reopen;
                                 PTB=Re&abrir];
                      Promoted=Yes;
                      Image=ReOpen;
                      PromotedCategory=Category4;
                      OnAction=VAR
                                 ReleaseServiceDocument@1000 : Codeunit 416;
                               BEGIN
                                 ReleaseServiceDocument.PerformManualReopen(Rec);
                               END;
                                }
      { 14      ;2   ;Action    ;
                      Name=Create Whse Shipment;
                      AccessByPermission=TableData 7320=R;
                      CaptionML=[ENU=Create Whse. Shipment;
                                 PTB=Criar Envio Armaz�m];
                      Promoted=Yes;
                      Image=NewShipment;
                      PromotedCategory=Category4;
                      OnAction=VAR
                                 GetSourceDocOutbound@1000 : Codeunit 5752;
                               BEGIN
                                 GetSourceDocOutbound.CreateWhseShptFromServiceOrder(Rec);
                                 IF NOT FIND('=><') THEN
                                   INIT;
                               END;
                                }
      { 33      ;1   ;ActionGroup;
                      CaptionML=[ENU=P&osting;
                                 PTB=Re&gistro];
                      Image=Post }
      { 52006500;2   ;Action    ;
                      CaptionML=[ENU=Posting Preview;
                                 PTB=Verificar Contabiliza��o];
                      RunObject=Codeunit 52112436;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=TestReport;
                      PromotedCategory=Report }
      { 86      ;2   ;Action    ;
                      Name=TestReport;
                      Ellipsis=Yes;
                      CaptionML=[ENU=Test Report;
                                 PTB=Relat�rio Verifica��o];
                      Image=TestReport;
                      OnAction=VAR
                                 ReportPrint@1001 : Codeunit 228;
                               BEGIN
                                 ReportPrint.PrintServiceHeader(Rec);
                               END;
                                }
      { 87      ;2   ;Action    ;
                      Name=Post;
                      ShortCutKey=F9;
                      Ellipsis=Yes;
                      CaptionML=[ENU=P&ost;
                                 PTB=Registrar];
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=PostOrder;
                      PromotedCategory=Process;
                      OnAction=VAR
                                 ServPostYesNo@1002 : Codeunit 5981;
                               BEGIN
                                 ServHeader.GET("Document Type","No.");
                                 ServPostYesNo.PostDocument(ServHeader);
                               END;
                                }
      { 114     ;2   ;Action    ;
                      ShortCutKey=Shift+F9;
                      Ellipsis=Yes;
                      CaptionML=[ENU=Post and &Print;
                                 PTB=Registrar e Im&primir];
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=PostPrint;
                      PromotedCategory=Process;
                      OnAction=VAR
                                 ServPostPrint@1001 : Codeunit 5982;
                               BEGIN
                                 ServHeader.GET("Document Type","No.");
                                 ServPostPrint.PostDocument(ServHeader);
                               END;
                                }
      { 115     ;2   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[ENU=Post &Batch;
                                 PTB=Registrar por Lote];
                      Image=PostBatch;
                      OnAction=BEGIN
                                 CLEAR(ServHeader);
                                 ServHeader.SETRANGE(Status,ServHeader.Status::Finished);
                                 REPORT.RUNMODAL(REPORT::"Batch Post Service Orders",TRUE,TRUE,ServHeader);
                                 CurrPage.UPDATE(FALSE);
                               END;
                                }
      { 4       ;1   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[ENU=&Print;
                                 PTB=Im&primir];
                      Promoted=Yes;
                      Image=Print;
                      PromotedCategory=Process;
                      OnAction=VAR
                                 DocPrint@1001 : Codeunit 229;
                               BEGIN
                                 CurrPage.UPDATE(TRUE);
                                 DocPrint.PrintServiceHeader(Rec);
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                CaptionML=[ENU=General;
                           PTB=Geral];
                GroupType=Group }

    { 34  ;2   ;Field     ;
                SourceExpr="No.";
                Importance=Promoted;
                OnAssistEdit=BEGIN
                               IF AssistEdit(xRec) THEN
                                 CurrPage.UPDATE;
                             END;
                              }

    { 99  ;2   ;Field     ;
                SourceExpr=Description }

    { 2   ;2   ;Field     ;
                SourceExpr="Customer No.";
                Importance=Promoted;
                OnValidate=BEGIN
                             CustomerNoOnAfterValidate;
                           END;
                            }

    { 146 ;2   ;Field     ;
                SourceExpr="Contact No.";
                OnValidate=BEGIN
                             IF GETFILTER("Contact No.") = xRec."Contact No." THEN
                               IF "Contact No." <> xRec."Contact No." THEN
                                 SETRANGE("Contact No.");
                           END;
                            }

    { 42  ;2   ;Field     ;
                SourceExpr=Name }

    { 82  ;2   ;Field     ;
                SourceExpr=Address }

    { 52006536;2;Field    ;
                SourceExpr="Sell-to Number" }

    { 89  ;2   ;Field     ;
                SourceExpr="Address 2";
                Importance=Additional }

    { 52006535;2;Field    ;
                SourceExpr="Sell-To District" }

    { 120 ;2   ;Field     ;
                SourceExpr="Post Code" }

    { 52006528;2;Field    ;
                SourceExpr="Sell-to Territory Code" }

    { 126 ;2   ;Field     ;
                SourceExpr="Contact Name" }

    { 28  ;2   ;Field     ;
                SourceExpr="Phone No." }

    { 63  ;2   ;Field     ;
                SourceExpr="E-Mail" }

    { 156 ;2   ;Field     ;
                SourceExpr=City }

    { 97  ;2   ;Field     ;
                SourceExpr="Phone No. 2";
                Importance=Additional }

    { 50  ;2   ;Field     ;
                SourceExpr="Notify Customer";
                Importance=Additional }

    { 10  ;2   ;Field     ;
                SourceExpr="Service Order Type" }

    { 190 ;2   ;Field     ;
                SourceExpr="Contract No." }

    { 44  ;2   ;Field     ;
                SourceExpr="Response Date";
                Importance=Promoted }

    { 134 ;2   ;Field     ;
                SourceExpr="Response Time" }

    { 25  ;2   ;Field     ;
                SourceExpr=Priority;
                Importance=Promoted }

    { 47  ;2   ;Field     ;
                SourceExpr=Status }

    { 52006550;2;Field    ;
                Name=BR Your Reference;
                SourceExpr="Your Reference" }

    { 52006540;2;Field    ;
                SourceExpr="External Document No.";
                Importance=Promoted }

    { 98  ;2   ;Field     ;
                SourceExpr="Responsibility Center";
                Importance=Additional }

    { 217 ;2   ;Field     ;
                SourceExpr="Assigned User ID";
                Importance=Additional }

    { 15  ;2   ;Field     ;
                SourceExpr="Release Status";
                Importance=Promoted }

    { 52006551;2;Field     }

    { 52006552;2;Field    ;
                SourceExpr="Created-By User ID";
                Editable=FALSE }

    { 46  ;1   ;Part      ;
                Name=ServItemLines;
                SubPageLink=Document No.=FIELD(No.);
                PagePartID=Page5902 }

    { 52006516;1;Group    ;
                CaptionML=[ENU=National;
                           PTB=Nacional];
                Visible=AllowInvoicing;
                GroupType=Group }

    { 52006515;2;Field    ;
                SourceExpr="Branch Code";
                Importance=Promoted }

    { 52006514;2;Field    ;
                SourceExpr="Taxes Matrix Code" }

    { 52006513;2;Field    ;
                SourceExpr="Tax Area Code";
                Importance=Promoted }

    { 52006512;2;Field    ;
                SourceExpr="Auto NFe";
                Importance=Additional }

    { 52006511;2;Field    ;
                SourceExpr="Fiscal Document Type";
                Importance=Promoted }

    { 52006510;2;Field    ;
                SourceExpr="Print Serie" }

    { 52006509;2;Field    ;
                SourceExpr="Print Sub Serie";
                Importance=Additional }

    { 52006508;2;Field    ;
                SourceExpr="CFOP Code" }

    { 52006507;2;Field    ;
                SourceExpr="Operation Nature" }

    { 52006506;2;Field    ;
                SourceExpr="Complementary Invoice Type";
                Importance=Additional }

    { 52006505;2;Field    ;
                SourceExpr="Invoice to Complement";
                Importance=Additional }

    { 52006504;2;Field    ;
                SourceExpr="Operation Type" }

    { 52006503;2;Field    ;
                SourceExpr="Fiscal Book Remarks";
                Importance=Additional }

    { 52006502;2;Field    ;
                SourceExpr="NFe Chave Acesso" }

    { 52006501;2;Field    ;
                SourceExpr="End User" }

    { 52006542;2;Field    ;
                SourceExpr="Posting Description";
                Importance=Additional }

    { 52006541;2;Field    ;
                SourceExpr="Additional Description";
                Importance=Additional }

    { 52006546;2;Field    ;
                SourceExpr="Buyer Presence" }

    { 52006543;1;Part     ;
                SubPageLink=Document Type=FIELD(Document Type),
                            Document No.=FIELD(No.);
                PagePartID=Page52112497;
                PartType=Page }

    { 1905885101;1;Group  ;
                CaptionML=[ENU=Invoicing;
                           PTB=Faturamento];
                GroupType=Group }

    { 75  ;2   ;Field     ;
                SourceExpr="Bill-to Customer No.";
                Importance=Promoted;
                OnValidate=BEGIN
                             BilltoCustomerNoOnAfterValidat;
                           END;
                            }

    { 164 ;2   ;Field     ;
                SourceExpr="Bill-to Contact No." }

    { 92  ;2   ;Field     ;
                SourceExpr="Bill-to Name" }

    { 96  ;2   ;Field     ;
                SourceExpr="Bill-to Address" }

    { 52006517;2;Field    ;
                SourceExpr="Bill-to Number" }

    { 103 ;2   ;Field     ;
                SourceExpr="Bill-to Address 2";
                Importance=Additional }

    { 52006518;2;Field    ;
                SourceExpr="Bill-to District" }

    { 107 ;2   ;Field     ;
                SourceExpr="Bill-to Post Code" }

    { 109 ;2   ;Field     ;
                SourceExpr="Bill-to City" }

    { 65  ;2   ;Field     ;
                SourceExpr="Bill-to Contact" }

    { 135 ;2   ;Field     ;
                SourceExpr="Your Reference";
                Importance=Additional }

    { 175 ;2   ;Field     ;
                SourceExpr="Salesperson Code" }

    { 79  ;2   ;Field     ;
                SourceExpr="Max. Labor Unit Price";
                Importance=Additional;
                OnValidate=BEGIN
                             MaxLaborUnitPriceOnAfterValida;
                           END;
                            }

    { 113 ;2   ;Field     ;
                SourceExpr="Posting Date" }

    { 118 ;2   ;Field     ;
                SourceExpr="Document Date" }

    { 180 ;2   ;Field     ;
                SourceExpr="Shortcut Dimension 1 Code" }

    { 17  ;2   ;Field     ;
                SourceExpr="Shortcut Dimension 2 Code" }

    { 195 ;2   ;Field     ;
                SourceExpr="Payment Terms Code";
                Importance=Promoted }

    { 196 ;2   ;Field     ;
                SourceExpr="Due Date";
                Importance=Promoted }

    { 197 ;2   ;Field     ;
                SourceExpr="Payment Discount %" }

    { 198 ;2   ;Field     ;
                SourceExpr="Pmt. Discount Date" }

    { 200 ;2   ;Field     ;
                SourceExpr="Payment Method Code" }

    { 202 ;2   ;Field     ;
                SourceExpr="Prices Including VAT";
                Visible=FALSE;
                OnValidate=BEGIN
                             PricesIncludingVATOnAfterValid;
                           END;
                            }

    { 205 ;2   ;Field     ;
                SourceExpr="VAT Bus. Posting Group";
                Visible=FALSE }

    { 1906801201;1;Group  ;
                CaptionML=[ENU=Shipping;
                           PTB=Envio];
                GroupType=Group }

    { 6   ;2   ;Field     ;
                SourceExpr="Ship-to Code";
                Importance=Promoted;
                OnValidate=BEGIN
                             ShiptoCodeOnAfterValidate;
                           END;
                            }

    { 51  ;2   ;Field     ;
                SourceExpr="Ship-to Name" }

    { 133 ;2   ;Field     ;
                SourceExpr="Ship-to Address" }

    { 52006519;2;Field    ;
                SourceExpr="Ship-to Number" }

    { 141 ;2   ;Field     ;
                SourceExpr="Ship-to Address 2";
                Importance=Additional }

    { 52006520;2;Field    ;
                SourceExpr="Ship-to District" }

    { 147 ;2   ;Field     ;
                SourceExpr="Ship-to Post Code";
                Importance=Promoted }

    { 149 ;2   ;Field     ;
                SourceExpr="Ship-to City" }

    { 157 ;2   ;Field     ;
                SourceExpr="Ship-to Contact";
                Importance=Promoted }

    { 159 ;2   ;Field     ;
                CaptionML=ENU=Ship-to Phone;
                SourceExpr="Ship-to Phone" }

    { 123 ;2   ;Field     ;
                SourceExpr="Ship-to Phone 2";
                Importance=Additional }

    { 8   ;2   ;Field     ;
                SourceExpr="Ship-to E-Mail" }

    { 207 ;2   ;Field     ;
                SourceExpr="Location Code" }

    { 161 ;2   ;Field     ;
                SourceExpr="Shipping Advice" }

    { 16  ;2   ;Field     ;
                SourceExpr="Shipment Method Code" }

    { 19  ;2   ;Field     ;
                SourceExpr="Shipping Agent Code" }

    { 22  ;2   ;Field     ;
                SourceExpr="Shipping Agent Service Code" }

    { 23  ;2   ;Field     ;
                SourceExpr="Shipping Time" }

    { 52006544;2;Group    ;
                CaptionML=[ENU=NF-e;
                           PTB=NF-e];
                Visible=AllowInvoicing;
                GroupType=Group }

    { 52006545;3;Field    ;
                SourceExpr="BR Shipping Agent Code" }

    { 52006539;3;Field    ;
                SourceExpr="Freight Billed To" }

    { 52006538;3;Field    ;
                SourceExpr="Transported Quantity" }

    { 52006537;3;Field    ;
                SourceExpr=Species }

    { 52006534;3;Field    ;
                SourceExpr=Marks }

    { 52006533;3;Field    ;
                SourceExpr="Transport Number" }

    { 52006532;3;Field    ;
                SourceExpr="Gross Weight" }

    { 52006531;3;Field    ;
                SourceExpr="Net Weight" }

    { 52006530;3;Field    ;
                SourceExpr="License Plate" }

    { 52006529;3;Field    ;
                SourceExpr="UF Vehicle" }

    { 1901902601;1;Group  ;
                CaptionML=[ENU=Details;
                           PTB=Detalhes];
                GroupType=Group }

    { 26  ;2   ;Field     ;
                SourceExpr="Warning Status";
                Importance=Promoted }

    { 78  ;2   ;Field     ;
                SourceExpr="Link Service to Service Item" }

    { 124 ;2   ;Field     ;
                SourceExpr="Allocated Hours" }

    { 24  ;2   ;Field     ;
                SourceExpr="No. of Allocations" }

    { 71  ;2   ;Field     ;
                SourceExpr="No. of Unallocated Items" }

    { 110 ;2   ;Field     ;
                SourceExpr="Service Zone Code" }

    { 68  ;2   ;Field     ;
                SourceExpr="Order Date";
                OnValidate=BEGIN
                             OrderDateOnAfterValidate;
                           END;
                            }

    { 85  ;2   ;Field     ;
                SourceExpr="Order Time";
                OnValidate=BEGIN
                             OrderTimeOnAfterValidate;
                           END;
                            }

    { 39  ;2   ;Field     ;
                SourceExpr="Expected Finishing Date" }

    { 53  ;2   ;Field     ;
                SourceExpr="Starting Date";
                Importance=Promoted }

    { 55  ;2   ;Field     ;
                SourceExpr="Starting Time" }

    { 31  ;2   ;Field     ;
                SourceExpr="Actual Response Time (Hours)" }

    { 182 ;2   ;Field     ;
                SourceExpr="Finishing Date" }

    { 184 ;2   ;Field     ;
                SourceExpr="Finishing Time";
                OnValidate=BEGIN
                             FinishingTimeOnAfterValidate;
                           END;
                            }

    { 77  ;2   ;Field     ;
                SourceExpr="Service Time (Hours)" }

    { 1903873101;1;Group  ;
                CaptionML=[ENU=" Foreign Trade";
                           PTB=Internacional];
                GroupType=Group }

    { 168 ;2   ;Field     ;
                SourceExpr="Currency Code";
                Importance=Promoted;
                OnAssistEdit=BEGIN
                               CLEAR(ChangeExchangeRate);
                               ChangeExchangeRate.SetParameter("Currency Code","Currency Factor","Posting Date");
                               IF ChangeExchangeRate.RUNMODAL = ACTION::OK THEN BEGIN
                                 VALIDATE("Currency Factor",ChangeExchangeRate.GetParameter);
                                 CurrPage.UPDATE;
                               END;
                               CLEAR(ChangeExchangeRate);
                             END;
                              }

    { 179 ;2   ;Field     ;
                SourceExpr="EU 3-Party Trade" }

    { 186 ;2   ;Field     ;
                SourceExpr="Transaction Type" }

    { 189 ;2   ;Field     ;
                SourceExpr="Transaction Specification" }

    { 187 ;2   ;Field     ;
                SourceExpr="Transport Method" }

    { 188 ;2   ;Field     ;
                SourceExpr="Exit Point" }

    { 192 ;2   ;Field     ;
                SourceExpr=Area }

    { 52006523;2;Group    ;
                CaptionML=[ENU=Exportation;
                           PTB=Exporta��o];
                Visible=AllowInvoicing;
                GroupType=Group }

    { 52006522;3;Field    ;
                SourceExpr="UF Boarding" }

    { 52006521;3;Field    ;
                SourceExpr="Boarding Locate" }

    { 52006527;1;Group    ;
                CaptionML=PTB=NFS-e;
                Visible=AllowInvoicing;
                GroupType=Group }

    { 52006526;2;Field    ;
                SourceExpr="Service E-Invoice";
                Editable=FALSE }

    { 52006549;2;Field    ;
                SourceExpr="Service Delivery Date" }

    { 52006525;2;Field    ;
                SourceExpr="Service Delivery City" }

    { 52006524;2;Field    ;
                SourceExpr="NFS-e Tributation Code" }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 52006547;1;Part     ;
                SubPageLink=Type=CONST(Service),
                            Document Type=CONST(Order),
                            Document No.=FIELD(No.);
                PagePartID=Page52112498;
                PartType=Page }

    { 1902018507;1;Part   ;
                SubPageLink=No.=FIELD(Bill-to Customer No.);
                PagePartID=Page9082;
                Visible=FALSE;
                PartType=Page }

    { 1900316107;1;Part   ;
                SubPageLink=No.=FIELD(Customer No.);
                PagePartID=Page9084;
                Visible=FALSE;
                PartType=Page }

    { 1907829707;1;Part   ;
                SubPageLink=No.=FIELD(Customer No.);
                PagePartID=Page9085;
                Visible=TRUE;
                PartType=Page }

    { 1902613707;1;Part   ;
                SubPageLink=No.=FIELD(Bill-to Customer No.);
                PagePartID=Page9086;
                Visible=FALSE;
                PartType=Page }

    { 1906530507;1;Part   ;
                SubPageLink=Document Type=FIELD(Document Type),
                            Document No.=FIELD(Document No.),
                            Line No.=FIELD(Line No.);
                PagePartID=Page9088;
                ProviderID=46;
                Visible=TRUE;
                PartType=Page }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=TRUE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      ServHeader@1004 : Record 5900;
      ChangeExchangeRate@1007 : Page 511;
      ServOrderMgt@1008 : Codeunit 5900;
      ServLogMgt@1009 : Codeunit 5906;
      UserMgt@1013 : Codeunit 5700;
      ">NAVBR"@52006500 : Integer;
      AllowInvoicing@52006501 : Boolean;

    LOCAL PROCEDURE CustomerNoOnAfterValidate@19016267();
    BEGIN
      IF GETFILTER("Customer No.") = xRec."Customer No." THEN
        IF "Customer No." <> xRec."Customer No." THEN
          SETRANGE("Customer No.");
      CurrPage.UPDATE;
    END;

    LOCAL PROCEDURE BilltoCustomerNoOnAfterValidat@19044114();
    BEGIN
      CurrPage.UPDATE;
    END;

    LOCAL PROCEDURE MaxLaborUnitPriceOnAfterValida@19060830();
    BEGIN
      CurrPage.SAVERECORD;
    END;

    LOCAL PROCEDURE PricesIncludingVATOnAfterValid@19009096();
    BEGIN
      CurrPage.UPDATE;
    END;

    LOCAL PROCEDURE ShiptoCodeOnAfterValidate@19065015();
    BEGIN
      CurrPage.UPDATE;
    END;

    LOCAL PROCEDURE OrderTimeOnAfterValidate@19056033();
    BEGIN
      UpdateResponseDateTime;
      CurrPage.UPDATE;
    END;

    LOCAL PROCEDURE OrderDateOnAfterValidate@19077772();
    BEGIN
      UpdateResponseDateTime;
      CurrPage.UPDATE;
    END;

    LOCAL PROCEDURE FinishingTimeOnAfterValidate@19010371();
    BEGIN
      CurrPage.UPDATE(TRUE);
    END;

    BEGIN
    END.
  }
}

