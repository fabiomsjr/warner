OBJECT Table 99000756 Work Center Group
{
  OBJECT-PROPERTIES
  {
    Date=07/09/12;
    Time=12:00:00;
    Version List=NAVW17.00;
  }
  PROPERTIES
  {
    DataCaptionFields=Code,Name;
    CaptionML=[ENU=Work Center Group;
               PTB=Grupo Centros Trab.];
    LookupPageID=Page99000758;
    DrillDownPageID=Page99000758;
  }
  FIELDS
  {
    { 1   ;   ;Code                ;Code10        ;CaptionML=[ENU=Code;
                                                              PTB=C�digo];
                                                   NotBlank=Yes }
    { 2   ;   ;Name                ;Text50        ;CaptionML=[ENU=Name;
                                                              PTB=Nome] }
    { 20  ;   ;Date Filter         ;Date          ;FieldClass=FlowFilter;
                                                   CaptionML=[ENU=Date Filter;
                                                              PTB=Filtro Data] }
    { 21  ;   ;Work Shift Filter   ;Code10        ;FieldClass=FlowFilter;
                                                   TableRelation="Work Shift";
                                                   CaptionML=[ENU=Work Shift Filter;
                                                              PTB=Filtro Turno] }
    { 22  ;   ;Capacity (Total)    ;Decimal       ;FieldClass=FlowField;
                                                   CalcFormula=Sum("Calendar Entry"."Capacity (Total)" WHERE (Capacity Type=CONST(Work Center),
                                                                                                              Work Center Group Code=FIELD(Code),
                                                                                                              Work Shift Code=FIELD(Work Shift Filter),
                                                                                                              Date=FIELD(Date Filter)));
                                                   CaptionML=[ENU=Capacity (Total);
                                                              PTB=Capacidade (Total)];
                                                   DecimalPlaces=0:5;
                                                   Editable=No }
    { 23  ;   ;Capacity (Effective);Decimal       ;FieldClass=FlowField;
                                                   CalcFormula=Sum("Calendar Entry"."Capacity (Effective)" WHERE (Capacity Type=CONST(Work Center),
                                                                                                                  Work Center Group Code=FIELD(Code),
                                                                                                                  Work Shift Code=FIELD(Work Shift Filter),
                                                                                                                  Date=FIELD(Date Filter)));
                                                   CaptionML=[ENU=Capacity (Effective);
                                                              PTB=Capacidade (Efetiva)];
                                                   DecimalPlaces=0:5;
                                                   Editable=No }
    { 24  ;   ;Prod. Order Need (Qty.);Decimal    ;FieldClass=FlowField;
                                                   CalcFormula=Sum("Prod. Order Capacity Need"."Allocated Time" WHERE (Status=FIELD(Prod. Order Status Filter),
                                                                                                                       Work Center Group Code=FIELD(Code),
                                                                                                                       Date=FIELD(Date Filter),
                                                                                                                       Requested Only=CONST(No)));
                                                   CaptionML=[ENU=Prod. Order Need (Qty.);
                                                              PTB=Nec. Ordem Prod. (Qtd.)];
                                                   DecimalPlaces=0:5;
                                                   Editable=No }
    { 47  ;   ;Prod. Order Status Filter;Option   ;FieldClass=FlowFilter;
                                                   CaptionML=[ENU=Prod. Order Status Filter;
                                                              PTB=Filtro Status Ordem Prod.];
                                                   OptionCaptionML=[ENU=Simulated,Planned,Firm Planned,Released,Finished;
                                                                    PTB=Simulada,Planejada,Planejada Firme,Lan�ada,Terminada];
                                                   OptionString=Simulated,Planned,Firm Planned,Released,Finished }
  }
  KEYS
  {
    {    ;Code                                    ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

