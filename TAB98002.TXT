OBJECT Table 98002 Data Table
{
  OBJECT-PROPERTIES
  {
    Date=10/05/16;
    Time=11:38:38;
    Modified=Yes;
    Version List=;
  }
  PROPERTIES
  {
  }
  FIELDS
  {
    { 1   ;   ;Entry No.           ;Integer        }
    { 21  ;   ;PK                  ;Text30         }
    { 22  ;   ;PK3                 ;Text30         }
    { 23  ;   ;PK4                 ;Text30         }
    { 24  ;   ;PK5                 ;Text30         }
    { 25  ;   ;PK6                 ;Text30         }
    { 26  ;   ;PK7                 ;Text30         }
    { 27  ;   ;PK8                 ;Text30         }
    { 28  ;   ;PK9                 ;Text30         }
    { 29  ;   ;PK10                ;Text30         }
    { 40  ;   ;Value               ;Text250        }
  }
  KEYS
  {
    {    ;Entry No.                               ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

