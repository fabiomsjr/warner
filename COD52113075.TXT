OBJECT Codeunit 52113075 Sales Approval Mgt
{
  OBJECT-PROPERTIES
  {
    Date=23/02/16;
    Time=14:20:47;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    Permissions=TableData 232=im;
    OnRun=BEGIN
          END;

  }
  CODE
  {
    VAR
      DocMustBeApprovedErr@35100000 : TextConst 'ENU=The document must be approved before posting.;PTB=O documento deve ser aprovado antes do registro.';
      ApprovalsDisabled@35100001 : TextConst 'ENU=The approval process is not enabeld for the document.;PTB=O processo de aprova��o n�o est� ativo para o documento.';
      NothingToApprove@35100002 : TextConst 'ENU=There is nothing to be approved.;PTB=N�o h� nada para ser aprovado.';
      ApprovRequestCreated@35100007 : TextConst 'ENU=Sales %1 %2 requires further approval.\\Approval request entries have been created.;PTB=%1 Venda %2 requer aprova��o.\\Movimentos de requisi��o de aprova��o foram criados.';
      ApprovRequestCancelled@35100006 : TextConst 'ENU=Sales %1 %2 approval request cancelled.;PTB=%1 Venda %2 teve sua requisi��o de aprova��o cancelada.';
      ApprovRequestApproved@35100005 : TextConst 'ENU=Sales %1 %2 has been automatically approved and released.;PTB=%1 Venda %2 j� foi automaticamente aprovado e liberado.';
      ApprovalDescription@35100008 : TextConst 'ENU=%1 %2 %3;PTB=%2 %1 %3';
      ApprovalNotStarted@35100009 : TextConst 'ENU=The approval process was not started for this document.;PTB=O processo de aprova��o n�o foi iniciado para esse documento.';
      RuleNotFound@52006503 : TextConst 'ENU=An Approval Rule was not found with the following criteria:\\Type: %1\Document Type: %2;PTB=N�o foi encontrada uma Regra de Aprova��o com os crit�rios abaixo:\\Tipo: %1\Tipo Documento: %2';
      CreditLimitExceededErr@52006501 : TextConst 'ENU=The Credit Limit for customer %1 would by exceeded by %2.;PTB=O Limite de Cr�dito do cliente %1 seria excedido em %2.';
      DiscountErr@52006502 : TextConst 'ENU=The Discount of %1% was not allowed for item %2.;PTB=O Desconto de %1% n�o foi permitido para o item %2.';
      CreditLimitMsg@52006500 : TextConst 'ENU=Credit Limit;PTB=Limite de Cr�dito';
      DiscountMsg@52006504 : TextConst 'ENU=Discount;PTB=Desconto';

    PROCEDURE SendApprovalRequest@35100001(VAR SalesHeader@35100000 : Record 36);
    VAR
      SalesSetup@52006509 : Record 311;
      Cust@52006506 : Record 18;
      ApprovFlow@35100004 : Record 52113073;
      DocApprovSetup@52006500 : Record 52113082;
      SalesMgt@52006507 : Codeunit 52112432;
      ApprovFlowMgt@35100005 : Codeunit 52113073;
      NewAmtForCreditLimit@52006510 : Decimal;
      CreditLimitExceeded@52006508 : Decimal;
      ApprovNo@52006503 : Integer;
      ApprovFlowRuleCode@52006502 : Code[20];
      ApprovAmount@52006501 : Decimal;
      DocApprovBuffer@52006504 : TEMPORARY Record 52113084;
      Description@52006505 : Text;
    BEGIN
      CheckApprovalEnabled(SalesHeader);
      CheckHeaderReadyForApproval(SalesHeader);

      SalesSetup.GET;
      IF SalesSetup."Released Orders as Outstanding" THEN BEGIN
        SalesHeader.CALCFIELDS("Amount Including VAT");
        NewAmtForCreditLimit := SalesHeader."Amount Including VAT";
      END;
      Cust.GET(SalesHeader."Bill-to Customer No.");
      CreditLimitExceeded := SalesMgt.CustCreditLimitExceeded(Cust, NewAmtForCreditLimit, SalesHeader."Currency Code");

      DocApprovSetup.GET(DocApprovSetup.Type::Sale, SalesHeader."Document Type");
      ApprovFlow.Type := ApprovFlow.Type::SalesDoc;
      Description := STRSUBSTNO(ApprovalDescription, DocApprovSetup.Type, DocApprovSetup."Document Type", SalesHeader."No.");
      CreateBuffer(DocApprovBuffer, SalesHeader, DocApprovSetup);
      CreateApprovals(DocApprovBuffer, SalesHeader, ApprovAmount, ApprovFlow.Type, Description,
                      DocApprovSetup."Limit Currency Code", DocApprovSetup."Include Taxes", CreditLimitExceeded);
      SalesHeader.MODIFY;

      ApprovFlow.GET(SalesHeader."Approval Flow No.");
      ApprovFlow.Amount := ApprovAmount;
      ApprovFlowMgt.FinishApprovalEntries(ApprovFlow, DocApprovSetup."Addit. Approvers List Code");
      ApprovFlow.UpdateDocument;
      ApprovFlow.MODIFY;

      SalesHeader.FIND;
      CASE SalesHeader.Status OF
        SalesHeader.Status::Released: MESSAGE(ApprovRequestApproved, SalesHeader."Document Type", SalesHeader."No.");
        SalesHeader.Status::"Pending Approval": MESSAGE(ApprovRequestCreated, SalesHeader."Document Type", SalesHeader."No.");
      END;
    END;

    PROCEDURE CancelApprovalRequest@35100002(VAR SalesHeader@35100000 : Record 36);
    VAR
      ApprovFlow@52006500 : Record 52113073;
      ApprovFlowMgt@35100003 : Codeunit 52113073;
    BEGIN
      CheckApprovalEnabled(SalesHeader);
      SalesHeader.TESTFIELD(Status, SalesHeader.Status::"Pending Approval");
      ApprovFlow.GET(SalesHeader."Approval Flow No.");
      ApprovFlowMgt.CancelApproval(ApprovFlow."No.");
      ApprovFlow.FIND;
      ApprovFlow.UpdateDocument;
      MESSAGE(ApprovRequestCancelled, SalesHeader."Document Type", SalesHeader."No.");
    END;

    PROCEDURE Reopen@52006501(SalesHeader@52006500 : Record 36);
    VAR
      SalesLine@52006501 : Record 37;
      ApprovFlowMgt@52006502 : Codeunit 52113073;
    BEGIN
      SalesLine.SETRANGE("Document Type", SalesHeader."Document Type");
      SalesLine.SETRANGE("Document No.", SalesHeader."No.");
      SalesLine.MODIFYALL(Status, SalesLine.Status::Open);
      IF SalesHeader."Approval Flow No." <> '' THEN
        ApprovFlowMgt.CancelApproval(SalesHeader."Approval Flow No.");
    END;

    PROCEDURE CheckApprovalEnabled@52006506(SalesHeader@52006500 : Record 36);
    BEGIN
      IF NOT IsApprovalEnabled(SalesHeader) THEN
        ERROR(ApprovalsDisabled);
    END;

    PROCEDURE IsApprovalEnabled@52006504(SalesHeader@52006500 : Record 36) : Boolean;
    VAR
      ApprovFlowSetup@52006501 : Record 52113076;
      DocApprovSetup@1001 : Record 52113082;
    BEGIN
      IF NOT ApprovFlowSetup.GET THEN
        EXIT(FALSE);
      IF NOT ApprovFlowSetup."Sales Approval" THEN
        EXIT(FALSE);
      IF NOT DocApprovSetup.GET(DocApprovSetup.Type::Sale, SalesHeader."Document Type") THEN
        EXIT(FALSE);

      EXIT(DocApprovSetup.Enabled);
    END;

    LOCAL PROCEDURE CreateBuffer@52006510(VAR DocApprovBuffer@52006501 : TEMPORARY Record 52113084;SalesHeader@52006500 : Record 36;DocApprovSetup@52006506 : Record 52113082) : Boolean;
    VAR
      SalesLine@52006504 : Record 37;
      DocApprovRule@52006503 : Record 52113083;
      ApprovNo@52006502 : Integer;
    BEGIN
      WITH SalesLine DO BEGIN
        SETRANGE("Document Type", SalesHeader."Document Type");
        SETRANGE("Document No.", SalesHeader."No.");
        SETFILTER(Type, '<>%1', Type::" ");
        MODIFYALL(Status, Status::Open);
        IF FINDSET(TRUE) THEN
          REPEAT
            CheckLineReadyForApproval(SalesLine);
            GetRule(DocApprovRule, DocApprovSetup, SalesLine);
            DocApprovRule.TESTFIELD("Approval Flow Rule Code");
            DocApprovBuffer.INIT;
            DocApprovBuffer."Rule Code" := DocApprovRule."Approval Flow Rule Code";
            DocApprovBuffer."Sales Credit Limit" := DocApprovRule."Sales Credit Limit";
            IF NOT DocApprovBuffer.FIND THEN BEGIN
              ApprovNo += 1;
              DocApprovBuffer."Approval No" := ApprovNo;
              DocApprovBuffer.INSERT;
            END;

            SalesLine."Approval No." := DocApprovBuffer."Approval No";
            SalesLine.MODIFY;
          UNTIL NEXT = 0;
      END;
    END;

    LOCAL PROCEDURE GetDimValue@52006503(SalesLine@52006500 : Record 37;DimCode@52006504 : Code[20]) : Code[20];
    VAR
      DimSetEntry@52006503 : Record 480;
    BEGIN
      IF DimCode = '' THEN
        EXIT;

      DimSetEntry.SETRANGE("Dimension Set ID", SalesLine."Dimension Set ID");
      DimSetEntry.SETRANGE("Dimension Code", DimCode);
      IF DimSetEntry.FINDFIRST THEN
        EXIT(DimSetEntry."Dimension Value Code")
      ELSE
        EXIT('');
    END;

    LOCAL PROCEDURE GetRule@52006502(VAR DocApprovRule@52006501 : Record 52113083;DocApprovSetup@52006500 : Record 52113082;SalesLine@52006508 : Record 37);
    VAR
      Dimension@52006502 : Record 348;
      Dim1Val@52006507 : Code[20];
      Dim2Val@52006506 : Code[20];
      Err@52006505 : Text;
    BEGIN
      IF DocApprovSetup."Dimension 1 Code" <> '' THEN
        Dim1Val := GetDimValue(SalesLine, DocApprovSetup."Dimension 1 Code");
      IF DocApprovSetup."Dimension 2 Code" <> '' THEN
        Dim2Val := GetDimValue(SalesLine, DocApprovSetup."Dimension 2 Code");

      WITH DocApprovRule DO BEGIN
        SETRANGE(Type, DocApprovSetup.Type);
        SETRANGE("Document Type", DocApprovSetup."Document Type");
        SETRANGE("Dimension 1 Value Code", Dim1Val);
        SETRANGE("Dimension 2 Value Code", Dim2Val);
        IF FINDFIRST THEN
          EXIT;

        Err := STRSUBSTNO(RuleNotFound, DocApprovSetup.Type, DocApprovSetup."Document Type");
        IF DocApprovSetup."Dimension 1 Code" <> '' THEN BEGIN
          Dimension.GET(DocApprovSetup."Dimension 1 Code");
          Err += STRSUBSTNO('\%1: %2', Dimension."Code Caption", Dim1Val);
        END;
        IF DocApprovSetup."Dimension 2 Code" <> '' THEN BEGIN
          Dimension.GET(DocApprovSetup."Dimension 2 Code");
          Err += STRSUBSTNO('\%1: %2', Dimension."Code Caption", Dim2Val);
        END;

        ERROR(Err);
      END;
    END;

    LOCAL PROCEDURE CreateApprovals@52006519(VAR DocApprovBuffer@52006502 : TEMPORARY Record 52113084;VAR SalesHeader@52006500 : Record 36;VAR TotalAmount@52006517 : Decimal;ApprovType@52006509 : Integer;Description@52006504 : Text;CurrencyCode@52006505 : Code[10];IncludeTaxes@52006513 : Boolean;CreditLimitExceeded@52006510 : Decimal);
    VAR
      SalesLine@52006501 : Record 37;
      Customer@52006508 : Record 18;
      TmpTaxAmtLine@52006512 : TEMPORARY Record 52112449;
      BRTaxCalc@52006511 : Codeunit 52112426;
      SalesMgt@52006507 : Codeunit 52112432;
      ApprovFlowMgt@52006503 : Codeunit 52113073;
      Amount@52006506 : Decimal;
      TaxesAmtLCY@52006514 : Decimal;
      Reason@52006515 : Text;
      ReasonList@52006516 : DotNet "'mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.Collections.Hashtable";
    BEGIN
      IF IncludeTaxes THEN
        BRTaxCalc.CalculateSalesDoc(SalesHeader, TmpTaxAmtLine);

      DocApprovBuffer.RESET;
      DocApprovBuffer.SETCURRENTKEY("Approval No");
      IF DocApprovBuffer.FINDSET THEN
        REPEAT
          Amount := 0;
          ReasonList := ReasonList.Hashtable;

          IF (CreditLimitExceeded > 0) AND (DocApprovBuffer."Sales Credit Limit" = DocApprovBuffer."Sales Credit Limit"::BlockSale) THEN
            ERROR(CreditLimitExceededErr, SalesHeader."Bill-to Customer No.", CreditLimitExceeded);

          SalesLine.SETRANGE("Document Type", SalesHeader."Document Type");
          SalesLine.SETRANGE("Document No.", SalesHeader."No.");
          SalesLine.SETRANGE("Approval No.", DocApprovBuffer."Approval No");
          IF SalesLine.FINDSET THEN
            REPEAT
              IF ShouldIncludeLineAmount(ReasonList, SalesLine, DocApprovBuffer, CreditLimitExceeded) THEN BEGIN
                Amount += SalesLine."Line Amount";
                TaxesAmtLCY += TmpTaxAmtLine.GetLineNetFinancialAmount(SalesLine."Line No.", 0, 0);
              END;
            UNTIL SalesLine.NEXT = 0;

          Amount := ConvertCurrency(SalesHeader."Currency Code", CurrencyCode, Amount);
          Amount += ConvertCurrency('', CurrencyCode, TaxesAmtLCY);

          Reason := GetFlatReasonList(ReasonList);
          ApprovFlowMgt.CreateApproval(SalesHeader."Approval Flow No.", ApprovType, DocApprovBuffer."Approval No",
                                       DocApprovBuffer."Rule Code", Amount, Description, Reason);
          SalesLine.MODIFYALL("Approval Flow No.", SalesHeader."Approval Flow No.");

          TotalAmount += Amount;
        UNTIL DocApprovBuffer.NEXT = 0;
    END;

    LOCAL PROCEDURE CheckHeaderReadyForApproval@35100033(SalesHeader@35100000 : Record 36);
    BEGIN
      WITH SalesHeader DO BEGIN
        TESTFIELD(Status, Status::Open);
        TESTFIELD("Sell-to Customer No.");
        TESTFIELD("Bill-to Customer No.");
      END;
    END;

    LOCAL PROCEDURE CheckLineReadyForApproval@52006509(SalesLine@52006500 : Record 37);
    VAR
      Item@52006501 : Record 27;
    BEGIN
      WITH SalesLine DO BEGIN
        TESTFIELD("No.");
        TESTFIELD(Quantity);
        TESTFIELD("Line Amount");
        IF Type = Type::Item THEN BEGIN
          Item.GET("No.");
          IF NOT Item."Non-Stock Item" THEN
            TESTFIELD("Location Code");
        END;
        IF Type IN [Type::Item, Type::"Fixed Asset"] THEN
          TESTFIELD("Unit of Measure Code");
      END;
    END;

    LOCAL PROCEDURE ConvertCurrency@52006505(FromCurrencyCode@52006500 : Code[10];ToCurrencyCode@52006501 : Code[10];OrigAmount@52006502 : Decimal) : Decimal;
    VAR
      CurrExchRate@52006503 : Record 330;
      Factor@52006504 : Decimal;
    BEGIN
      IF FromCurrencyCode = ToCurrencyCode THEN
        EXIT(OrigAmount);

      EXIT(ROUND(CurrExchRate.ExchangeAmtFCYToFCY(TODAY, FromCurrencyCode, ToCurrencyCode, OrigAmount), 0.01));
    END;

    LOCAL PROCEDURE ShouldIncludeLineAmount@52006500(VAR ReasonList@52006506 : DotNet "'mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.Collections.Hashtable";SalesLine@52006500 : Record 37;DocApprovBuffer@52006501 : Record 52113084;CreditLimitExceeded@52006502 : Decimal) Include : Boolean;
    BEGIN
      IF CreditLimitExceeded > 0 THEN
        IF DocApprovBuffer."Sales Credit Limit" = DocApprovBuffer."Sales Credit Limit"::StartApproval THEN BEGIN
          ReasonList.Item(CreditLimitMsg, TRUE);
          Include := TRUE;
        END;
    END;

    LOCAL PROCEDURE GetFlatReasonList@52006536(ReasonList@52006500 : DotNet "'mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.Collections.Hashtable") : Text;
    VAR
      ICollection@52006503 : DotNet "'mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.Collections.ICollection";
      IEnumerator@52006502 : DotNet "'mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.Collections.IEnumerator";
      Reason@52006501 : Text;
    BEGIN
      ICollection := ReasonList.Keys;
      IEnumerator := ICollection.GetEnumerator;
      WHILE IEnumerator.MoveNext DO
        Reason += FORMAT(IEnumerator.Current) + ', ';
      EXIT(DELCHR(Reason, '>', ', '));
    END;

    BEGIN
    END.
  }
}

