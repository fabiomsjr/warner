OBJECT Table 276 Bank Account Statement Line
{
  OBJECT-PROPERTIES
  {
    Date=09/09/14;
    Time=12:00:00;
    Version List=NAVW18.00;
  }
  PROPERTIES
  {
    OnRename=BEGIN
               ERROR(Text000,TABLECAPTION);
             END;

    CaptionML=[ENU=Bank Account Statement Line;
               PTB=Linha extrato conta banco];
  }
  FIELDS
  {
    { 1   ;   ;Bank Account No.    ;Code20        ;TableRelation="Bank Account";
                                                   CaptionML=[ENU=Bank Account No.;
                                                              PTB=Cod. Conta Banco] }
    { 2   ;   ;Statement No.       ;Code20        ;TableRelation="Bank Account Statement"."Statement No." WHERE (Bank Account No.=FIELD(Bank Account No.));
                                                   CaptionML=[ENU=Statement No.;
                                                              PTB=N� Extrato] }
    { 3   ;   ;Statement Line No.  ;Integer       ;CaptionML=[ENU=Statement Line No.;
                                                              PTB=N� Linha Extrato Banc�rio] }
    { 4   ;   ;Document No.        ;Code20        ;CaptionML=[ENU=Document No.;
                                                              PTB=No. Documento] }
    { 5   ;   ;Transaction Date    ;Date          ;CaptionML=[ENU=Transaction Date;
                                                              PTB=Data Transa��o] }
    { 6   ;   ;Description         ;Text50        ;CaptionML=[ENU=Description;
                                                              PTB=Descri��o] }
    { 7   ;   ;Statement Amount    ;Decimal       ;CaptionML=[ENU=Statement Amount;
                                                              PTB=Valor Extrato];
                                                   AutoFormatType=1;
                                                   AutoFormatExpr=GetCurrencyCode }
    { 8   ;   ;Difference          ;Decimal       ;CaptionML=[ENU=Difference;
                                                              PTB=Diferen�a];
                                                   AutoFormatType=1;
                                                   AutoFormatExpr=GetCurrencyCode }
    { 9   ;   ;Applied Amount      ;Decimal       ;OnLookup=BEGIN
                                                              DisplayApplication;
                                                            END;

                                                   CaptionML=[ENU=Applied Amount;
                                                              PTB=Valor Aplicado];
                                                   Editable=No;
                                                   AutoFormatType=1;
                                                   AutoFormatExpr=GetCurrencyCode }
    { 10  ;   ;Type                ;Option        ;CaptionML=[ENU=Type;
                                                              PTB=Tipo];
                                                   OptionCaptionML=[ENU=Bank Account Ledger Entry,Check Ledger Entry,Difference;
                                                                    PTB=Mov. banco,Mov. cheque,Diferen�a];
                                                   OptionString=Bank Account Ledger Entry,Check Ledger Entry,Difference }
    { 11  ;   ;Applied Entries     ;Integer       ;OnLookup=BEGIN
                                                              DisplayApplication;
                                                            END;

                                                   CaptionML=[ENU=Applied Entries;
                                                              PTB=Aplicado por Mov.];
                                                   Editable=No }
    { 12  ;   ;Value Date          ;Date          ;CaptionML=[ENU=Value Date;
                                                              PTB=Data Valor] }
    { 14  ;   ;Check No.           ;Code20        ;AccessByPermission=TableData 272=R;
                                                   CaptionML=[ENU=Check No.;
                                                              PTB=N� Cheque] }
  }
  KEYS
  {
    {    ;Bank Account No.,Statement No.,Statement Line No.;
                                                   SumIndexFields=Statement Amount,Difference;
                                                   MaintainSIFTIndex=No;
                                                   Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      Text000@1000 : TextConst 'ENU=You cannot rename a %1.;PTB=N�o pode modificar o nome de %1';
      BankAccLedgEntry@1001 : Record 271;
      CheckLedgEntry@1002 : Record 272;

    LOCAL PROCEDURE DisplayApplication@1();
    BEGIN
      CASE Type OF
        Type::"Bank Account Ledger Entry":
          BEGIN
            BankAccLedgEntry.RESET;
            BankAccLedgEntry.SETCURRENTKEY("Bank Account No.",Open);
            BankAccLedgEntry.SETRANGE("Bank Account No.","Bank Account No.");
            BankAccLedgEntry.SETRANGE(Open,FALSE);
            BankAccLedgEntry.SETRANGE("Statement Status",BankAccLedgEntry."Statement Status"::Closed);
            BankAccLedgEntry.SETRANGE("Statement No.","Statement No.");
            BankAccLedgEntry.SETRANGE("Statement Line No.","Statement Line No.");
            PAGE.RUN(0,BankAccLedgEntry);
          END;
        Type::"Check Ledger Entry":
          BEGIN
            CheckLedgEntry.RESET;
            CheckLedgEntry.SETCURRENTKEY("Bank Account No.",Open);
            CheckLedgEntry.SETRANGE("Bank Account No.","Bank Account No.");
            CheckLedgEntry.SETRANGE(Open,FALSE);
            CheckLedgEntry.SETRANGE("Statement Status",CheckLedgEntry."Statement Status"::Closed);
            CheckLedgEntry.SETRANGE("Statement No.","Statement No.");
            CheckLedgEntry.SETRANGE("Statement Line No.","Statement Line No.");
            PAGE.RUN(0,CheckLedgEntry);
          END;
      END;
    END;

    PROCEDURE GetCurrencyCode@2() : Code[10];
    VAR
      BankAcc@1000 : Record 270;
    BEGIN
      IF "Bank Account No." = BankAcc."No." THEN
        EXIT(BankAcc."Currency Code");

      IF BankAcc.GET("Bank Account No.") THEN
        EXIT(BankAcc."Currency Code");

      EXIT('');
    END;

    BEGIN
    END.
  }
}

