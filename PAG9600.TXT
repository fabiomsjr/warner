OBJECT Page 9600 XML Schemas
{
  OBJECT-PROPERTIES
  {
    Date=09/09/14;
    Time=12:00:00;
    Version List=NAVW18.00;
  }
  PROPERTIES
  {
    CaptionML=ENU=XML Schemas;
    SourceTable=Table9600;
    PageType=List;
    ActionList=ACTIONS
    {
      { 10      ;    ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 11      ;1   ;Action    ;
                      CaptionML=ENU=Load Schema;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=Import;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 LoadSchema;
                               END;
                                }
      { 12      ;1   ;Action    ;
                      CaptionML=ENU=Export Schema;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=Export;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 ExportSchema(TRUE);
                               END;
                                }
      { 13      ;1   ;Action    ;
                      CaptionML=ENU=Open SEPA Schema Viewer;
                      Promoted=Yes;
                      Image=ViewWorksheet;
                      PromotedCategory=Process;
                      OnAction=VAR
                                 SEPASchemaViewer@1000 : Page 9610;
                               BEGIN
                                 SEPASchemaViewer.SetXMLSchemaCode(Code);
                                 SEPASchemaViewer.RUN;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1   ;0   ;Container ;
                ContainerType=ContentArea }

    { 2   ;1   ;Group     ;
                Name=Group;
                GroupType=Repeater }

    { 3   ;2   ;Field     ;
                SourceExpr=Code }

    { 4   ;2   ;Field     ;
                SourceExpr=Description }

    { 5   ;2   ;Field     ;
                SourceExpr="Target Namespace" }

    { 9   ;2   ;Field     ;
                CaptionML=ENU=Schema is Loaded;
                SourceExpr=XSD.HASVALUE }

    { 6   ;0   ;Container ;
                ContainerType=FactBoxArea }

    { 7   ;1   ;Part      ;
                PartType=System;
                SystemPartID=Notes }

    { 8   ;1   ;Part      ;
                PartType=System;
                SystemPartID=RecordLinks }

  }
  CODE
  {

    BEGIN
    END.
  }
}

