OBJECT Table 52112459 Exportation Record
{
  OBJECT-PROPERTIES
  {
    Date=06/12/13;
    Time=13:41:28;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Exportation Record;
               PTB=Registro Exporta��o];
    LookupPageID=Page52112470;
    DrillDownPageID=Page52112470;
  }
  FIELDS
  {
    { 10  ;   ;Exportation Register No.;Code20    ;CaptionML=[ENU=Exportation Register No.;
                                                              PTB=N� Registro Exporta��o] }
    { 20  ;   ;Exportation Register Date;Date     ;CaptionML=[ENU=Exportation Register Date;
                                                              PTB=Data Registro Exporta��o] }
    { 30  ;   ;Declaration No.     ;Code20        ;CaptionML=[ENU=Declaration No.;
                                                              PTB=N� Declara��o];
                                                   NotBlank=Yes }
    { 40  ;   ;Simple Export Document Type;Boolean;CaptionML=[ENU=Sample Export Document Type;
                                                              PTB=Doc. Exporta��o Simplificada] }
    { 50  ;   ;Declaration Date    ;Date          ;CaptionML=[ENU=Declaration Date;
                                                              PTB=Data Declara��o] }
    { 60  ;   ;Direct Exportation  ;Boolean       ;CaptionML=[ENU=Direct Exportation;
                                                              PTB=Exporta��o Direta] }
    { 70  ;   ;Ship-To Know No.    ;Code20        ;CaptionML=[ENU=Ship-To Know No.;
                                                              PTB=N� Conhecimento Embarque] }
    { 80  ;   ;Ship-To Know Date   ;Date          ;CaptionML=[ENU=Ship-To Know Date;
                                                              PTB=Data Conhecimento Embarque] }
    { 90  ;   ;Averb. Export. Decl. Date;Date     ;CaptionML=[ENU=Averb. Export. Decl. Date;
                                                              PTB=Data Declar. Averba��o Export.] }
    { 100 ;   ;Transport Ship-To Type;Code10      ;TableRelation="Transport Knowledge Type";
                                                   CaptionML=[ENU=Transport Knowledge Type;
                                                              PTB=Tipo Conhecimento Transporte] }
    { 110 ;   ;Country Code        ;Code10        ;TableRelation=Country/Region;
                                                   CaptionML=[ENU=Country Code;
                                                              PTB=C�d. Pa�s] }
    { 120 ;   ;Branch Code         ;Code20        ;TableRelation="Branch Information";
                                                   CaptionML=[ENU=Branch Code;
                                                              PTB=C�d. Filial] }
  }
  KEYS
  {
    {    ;Exportation Register No.                ;Clustered=Yes }
  }
  FIELDGROUPS
  {
    { 1   ;DropDown            ;Exportation Register No.,Exportation Register Date,Declaration No.,Simple Export Document Type,Declaration Date,Direct Exportation,Ship-To Know No.,Ship-To Know Date,Averb. Export. Decl. Date,Transport Ship-To Type,Country Code }
  }
  CODE
  {

    BEGIN
    {
      --- FUM0069 ---
      rafaelr,190711,01,Nova tabela para dados exporta��o SPED

      --- FUM0072 ---
      rafaelr,280711,04,Altera��o do conceito de dados exporta��o
              010811,05,Altera��o da chave de dados exporta��o
    }
    END.
  }
}

