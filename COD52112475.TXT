OBJECT Codeunit 52112475 Purchase Post Validation
{
  OBJECT-PROPERTIES
  {
    Date=09/10/15;
    Time=09:35:20;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    OnRun=BEGIN
          END;

  }
  CODE
  {
    VAR
      ReferencedKeyNotFound@52006500 : TextConst 'ENU=No referenced NF-e was provided for %1: %2.;PTB=N�o foi informada nenhuma NF-e referenciada para %1: %2.';
      ValidationMgt@52006501 : Codeunit 52112473;
      GLSetup@52006502 : Record 98;
      CompInfo@52006503 : Record 79;
      Header@52006504 : TextConst 'ENU=Header;PTB=Cabe�alho';
      Vendor@52006507 : Record 23;
      FiscalDocType@52006505 : Record 52112425;
      Line@52006506 : TextConst 'ENU=Line;PTB=Linha;EUQ=Line';
      DocumentAlreadyExists@52006508 : TextConst 'ENU=The %1 document no. already exists for Vendor %2 - %3.;PTB=O documento n� %1 j� existe para o Fornecedor %2 - %3.';
      DICantHaveInstallments@52006509 : TextConst 'ENU=Direct importations can not have multiple installments.;PTB=Importa��es diretas n�o podem ser parceladas.';
      DocVsPostingDateErr@52006510 : TextConst 'ENU=The document date cannot be higher than the posting date.;PTB=A data de documento n�o pode ser maior que a data de registro.';

    PROCEDURE Validate@52006503(PurchHeader@52006500 : Record 38);
    BEGIN
      GetGLSetup;
      GetCompanyInformation(PurchHeader."Branch Code");
      GetVendor(PurchHeader."Pay-to Vendor No.");
      GetFiscalDocumentType(PurchHeader."Fiscal Document Type");
      ValidateHeaderData(PurchHeader);
      ValidateFiscalData(PurchHeader);
      ValidateLines(PurchHeader);
      ThrowErrors;
    END;

    PROCEDURE ThrowOnFirstError@52006501(value@52006500 : Boolean);
    BEGIN
      ValidationMgt.ThrowOnFirstError := value;
    END;

    LOCAL PROCEDURE GetGLSetup@52006504();
    BEGIN
      GLSetup.GET;
    END;

    LOCAL PROCEDURE GetCompanyInformation@52006507(branchCode@52006500 : Code[20]);
    BEGIN
      CompInfo.GetCompanyBranchInfo(branchCode);
    END;

    LOCAL PROCEDURE GetVendor@52006500(VendorNo@52006500 : Code[20]);
    BEGIN
      Vendor.GET(VendorNo);
    END;

    LOCAL PROCEDURE GetFiscalDocumentType@52006508(FiscalDocTypeCode@52006500 : Code[20]);
    BEGIN
      IF FiscalDocTypeCode <> '' THEN
        FiscalDocType.GET(FiscalDocTypeCode);
    END;

    LOCAL PROCEDURE ValidateHeaderData@52006511(PurchHeader@52006500 : Record 38);
    BEGIN
      WITH PurchHeader DO BEGIN
        IF NOT Invoice THEN
          EXIT;

        SetRecord(PurchHeader, Header);

        IF GLSetup."Branch Dimension Code" <> '' THEN
          TestFieldNotBlank(FIELDNO("Branch Code"));

        TestFieldNotBlank(FIELDNO("Document Type"));
        TestFieldNotBlank(FIELDNO("Buy-from Vendor No."));
        TestFieldNotBlank(FIELDNO("Buy-from Vendor Name"));
        TestFieldNotBlank(FIELDNO("Pay-to Vendor No."));
        TestFieldNotBlank(FIELDNO("Pay-to Name"));
        TestFieldNotBlank(FIELDNO("Posting Date"));
        TestFieldNotBlank(FIELDNO("Document Date"));
        IF "Document Date" > "Posting Date" THEN
          AddError(DocVsPostingDateErr);
        IF "Document Type" IN ["Document Type"::Order, "Document Type"::Invoice] THEN
          IF NOT ("Complementary Invoice Type" IN [PurchHeader."Complementary Invoice Type"::CIAPCredit]) THEN
            TestFieldNotBlank(FIELDNO("Payment Terms Code"));

        TestFieldNotBlank(FIELDNO("Buy-from Territory Code"));
        TestFieldNotBlank(FIELDNO("Buy-from Address"));
        TestFieldNotBlank(FIELDNO("Buy-from City"));
        TestFieldNotBlank(FIELDNO("Buy-from Post Code"));
        TestFieldNotBlank(FIELDNO("Pay-to Address"));
        TestFieldNotBlank(FIELDNO("Pay-to City"));
        TestFieldNotBlank(FIELDNO("Pay-to Territory Code"));
        TestFieldNotBlank(FIELDNO("Pay-to Post Code"));

        CASE "Document Type"  OF
          "Document Type"::"Return Order":
            BEGIN
              TestFieldNotBlank(FIELDNO("Posting Type"));
            END;
          "Document Type"::"Credit Memo":
            BEGIN
              TestFieldNotBlank(FIELDNO("Applies-to Doc. Type"));
              TestFieldNotBlank(FIELDNO("Applies-to Doc. No."));
            END;
        END;
      END;
    END;

    LOCAL PROCEDURE ValidateFiscalData@52006570(PurchHeader@52006500 : Record 38);
    VAR
      AdditInvTextLine@52006502 : Record 52112436;
      NFeKeyMgt@52006503 : Codeunit 52112444;
      PostCode@52006505 : Record 225;
      NFeNo@52006501 : Text;
      PaymentTerms@52006504 : Record 3;
      TranspMethod@52006506 : Record 259;
    BEGIN
      WITH PurchHeader DO BEGIN
        IF NOT Invoice THEN
          EXIT;

        SetRecord(PurchHeader, Header);
        TestFieldNotBlank(FIELDNO("Fiscal Document Type"));
        IF "Fiscal Document Type" = '' THEN
          EXIT;
        FiscalDocType.GET("Fiscal Document Type");
        CASE FiscalDocType."Document Issuer" OF
          FiscalDocType."Document Issuer"::Own: BEGIN
            IF FiscalDocType."Invoice No. Series" = '' THEN BEGIN
              TestFieldNotBlank(FIELDNO("Vendor Invoice No."));
              AddErrorifVendorInvoiceNoAlreadyExists(PurchHeader);
            END ELSE BEGIN
              IF "Document Type" IN ["Document Type"::Order, "Document Type"::Invoice] THEN
                TestFieldBlank(FIELDNO("Vendor Invoice No."));
            END;
            IF FiscalDocType."Document Model" = '55' THEN BEGIN
              TestFieldNotBlank(FIELDNO("Freight Billed To"));
              TestFieldNotBlank(FIELDNO("Operation Nature"));
              IF "Complementary Invoice Type" IN ["Complementary Invoice Type"::Regular, "Complementary Invoice Type"::Return] THEN
                TestFieldNotBlank(FIELDNO("Buyer Presence"));

              IF "Complementary Invoice Type" IN ["Complementary Invoice Type"::Quantity, "Complementary Invoice Type"::Taxes,
                                                  "Complementary Invoice Type"::Price, "Complementary Invoice Type"::Return]
              THEN BEGIN
                AdditInvTextLine.FilterByPurchHeader(PurchHeader);
                AdditInvTextLine.SETFILTER("Referenced NF-e Key", '<>%1', '');
                IF AdditInvTextLine.ISEMPTY THEN
                  AddError(STRSUBSTNO(ReferencedKeyNotFound, FIELDCAPTION("Complementary Invoice Type"), "Complementary Invoice Type"));
              END;
            END;
          END;
          FiscalDocType."Document Issuer"::Third: BEGIN
            IF "Document Type" IN ["Document Type"::Order, "Document Type"::Invoice] THEN
              TestFieldNotBlank(FIELDNO("Vendor Invoice No."))
            ELSE IF "Document Type" IN ["Document Type"::"Return Order", "Document Type"::"Credit Memo"] THEN
              TestFieldNotBlank(FIELDNO("Vendor Cr. Memo No."));

            AddErrorifVendorInvoiceNoAlreadyExists(PurchHeader);
            IF FiscalDocType."Document Model" IN ['55', '57'] THEN BEGIN
              IF GLSetup."Validate Others NF-e Key" THEN BEGIN
                PurchHeader.CALCFIELDS("NFe Chave Acesso");
                TestFieldNotBlank(FIELDNO("NFe Chave Acesso"));
                IF "NFe Chave Acesso" <> '' THEN BEGIN
                  IF PurchHeader."Document Type" IN [PurchHeader."Document Type"::"Return Order", PurchHeader."Document Type"::"Credit Memo"] THEN
                    NFeNo := PurchHeader."Vendor Cr. Memo No."
                  ELSE
                    NFeNo := PurchHeader."Vendor Invoice No.";
                  IF NFeNo <> '' THEN
                    AddError(NFeKeyMgt.GetKeyErrors("NFe Chave Acesso", "Document Date", "Print Serie", NFeNo, Vendor."C.N.P.J./C.P.F."));
                END;
              END;
            END;
          END;
          FiscalDocType."Document Issuer"::"No Fiscal Value": BEGIN
            AddErrorifVendorInvoiceNoAlreadyExists(PurchHeader);
          END;
        END;

        IF FiscalDocType."Document Issuer" <> FiscalDocType."Document Issuer"::"No Fiscal Value" THEN BEGIN
          IF FiscalDocType."Series Mandatory" THEN
            TestFieldNotBlank(FIELDNO("Print Serie"));

          IF "Direct Import" THEN BEGIN
            TestFieldNotBlank(FIELDNO("Other Vendor No."));
            TestFieldNotBlank(FIELDNO("Exporter Code"));
            TestFieldNotBlank(FIELDNO("Foreign Manufacturer Code"));
            TestFieldNotBlank(FIELDNO("DI No."));
            TestFieldNotBlank(FIELDNO("DI Posting Date"));
            TestFieldNotBlank(FIELDNO("Disengagement Date"));
            TestFieldNotBlank(FIELDNO("Disengagement Territory Code"));
            TestFieldNotBlank(FIELDNO("Disengagement Location"));
            TestFieldNotBlank(FIELDNO("DI Invoice No."));
            TestFieldNotBlank(FIELDNO("Transport Method"));

            TestFieldNotBlank(FIELDNO("Payment Terms Code"));
            IF "Payment Terms Code" <> '' THEN BEGIN
              PaymentTerms.GET("Payment Terms Code");
              PaymentTerms.CALCFIELDS("Number of Due Dates");
              IF PaymentTerms."Number of Due Dates" > 0 THEN
                AddError(DICantHaveInstallments);
            END;

            TestFieldNotBlank(FIELDNO("Transport Method"));
            IF "Transport Method" <> '' THEN BEGIN
              TranspMethod.GET("Transport Method");
              SetRecord(TranspMethod, STRSUBSTNO('%1 %2', TranspMethod.TABLECAPTION, TranspMethod.Code));
              TestFieldNotBlank(TranspMethod.FIELDNO("NF-e Code"));
            END;
          END;

          SetRecord(Vendor, STRSUBSTNO('%1 %2', Vendor.TABLECAPTION, Vendor."No."));
          CASE Vendor.Category OF
            Vendor.Category::"1.- Person": BEGIN
              TestFieldNotBlank(Vendor.FIELDNO("C.N.P.J./C.P.F."));
            END;
            Vendor.Category::"2.- Company": BEGIN
              TestFieldNotBlank(Vendor.FIELDNO("C.N.P.J./C.P.F."));
              TestFieldNotBlank(Vendor.FIELDNO("I.E."));
            END;
          END;
          TestFieldNotBlank(Vendor.FIELDNO("Country/Region Code"));
        END;
      END;
    END;

    LOCAL PROCEDURE ValidateLines@52006519(PurchHeader@52006500 : Record 38);
    VAR
      PurchLine@52006501 : Record 39;
      DimMgt@52006502 : Codeunit 408;
      OperationType@52006503 : Record 52112435;
      CSTCode@52006505 : Record 52112431;
      Item@52006506 : Record 27;
      ChargeWithItemNo@52006504 : Boolean;
      ItemCharge@52006507 : Record 5800;
    BEGIN
      WITH PurchLine DO BEGIN
        SETRANGE("Document Type", PurchHeader."Document Type");
        SETRANGE("Document No.", PurchHeader."No.");
        SETFILTER(Type,'<>%1', Type::" ");
        SETFILTER("Qty. to Invoice", '>0');
        IF FINDSET THEN
          REPEAT
            CLEAR(Item);
            CLEAR(ChargeWithItemNo);
            DimMgt.CheckDocLineBranchCode(PurchHeader."Dimension Set ID", "Dimension Set ID");

            IF "Operation Type" <> '' THEN BEGIN
              OperationType.GET("Operation Type");
              IF PurchHeader."Posting from Whse. Ref." <> 0 THEN BEGIN
                SetRecord(OperationType, STRSUBSTNO('%1 %2', OperationType.TABLECAPTION, OperationType.Code));
                TestFieldNotBlank(OperationType.FIELDNO("Item Entry"));
              END;
              IF OperationType."Third-Party Stock Entry" THEN BEGIN
                Vendor.GET("Buy-from Vendor No.");
                SetRecord(Vendor, STRSUBSTNO('%1 %2', Vendor.TABLECAPTION, Vendor."No."));
                TestFieldNotBlank(Vendor.FIELDNO("Third-Party Location Code"));
                IF Type = Type::"Charge (Item)" THEN BEGIN
                  ItemCharge.GET("No.");
                  IF ItemCharge."Item Charge Type" = ItemCharge."Item Charge Type"::Item THEN BEGIN
                    SetRecord(PurchLine, STRSUBSTNO('%1 %2', Line, "Line No."));
                    TestFieldNotBlank(FIELDNO("Charge Item No."));
                    ChargeWithItemNo := TRUE;
                  END;
                END;
              END;
            END;

            IF PurchHeader.Invoice AND ("Qty. to Invoice" > 0) THEN BEGIN
              IF FiscalDocType."Document Issuer" <> FiscalDocType."Document Issuer"::"No Fiscal Value" THEN BEGIN
                SetRecord(PurchLine, STRSUBSTNO('%1 %2', Line, "Line No."));
                VALIDATE("PIS CST Code");
                VALIDATE("COFINS CST Code");
                VALIDATE("IPI CST Code");

                IF (Type IN [Type::Item, Type::"Fixed Asset"]) OR (ChargeWithItemNo) OR
                   ("Fiscal Item No." <> '')
                THEN BEGIN
                  TestFieldNotBlank(FIELDNO("Unit of Measure Code"));
                  IF FiscalDocType."CFOP Mandatory" THEN BEGIN
                    TestFieldNotBlank(FIELDNO("CFOP Code"));
                    VALIDATE("CFOP Code");
                  END;
                  IF FiscalDocType."NCM Code Mandatory" THEN
                    TestFieldNotBlank(FIELDNO("NCM Code"));
                  IF PurchHeader."Direct Import" THEN
                    TestFieldNotBlank(FIELDNO("Addition Number"));

                  TestFieldNotBlank(FIELDNO("PIS CST Code"));
                  TestFieldNotBlank(FIELDNO("COFINS CST Code"));
                  IF CSTCode.GET(CSTCode."Tax Type"::PIS, "PIS CST Code") THEN BEGIN
                    IF CSTCode."Tax Credit" AND FiscalDocType.Service THEN
                      TestFieldNotBlank(FIELDNO("Base Calculation Credit Code"));
                    IF CSTCode."Income Nature Mandatory" THEN
                      TestFieldNotBlank(FIELDNO("PIS CST Income Nature"));
                  END;
                  IF CSTCode.GET(CSTCode."Tax Type"::COFINS, "COFINS CST Code") THEN BEGIN
                    IF CSTCode."Tax Credit" AND FiscalDocType.Service THEN
                      TestFieldNotBlank(FIELDNO("Base Calculation Credit Code"));
                    IF CSTCode."Income Nature Mandatory" THEN
                      TestFieldNotBlank(FIELDNO("COFINS CST Income Nature"));
                  END;

                  IF "Fiscal Item No." <> '' THEN
                    Item.GET("Fiscal Item No.")
                  ELSE
                    IF Type = Type::Item THEN
                      Item.GET("No.");

                  IF (NOT Item.Service) AND (NOT FiscalDocType.Service) THEN BEGIN
                    TestFieldNotBlank(FIELDNO("ICMS CST Code"));
                    IF STRLEN("ICMS CST Code") < 3 THEN
                      TestFieldNotBlank(FIELDNO("Origin Code"));
                  END;
                END;
              END;
            END;
          UNTIL NEXT = 0;
      END;
    END;

    LOCAL PROCEDURE TestFieldNotBlank@52006534(FieldNo@52006500 : Integer);
    BEGIN
      ValidationMgt.AddRuleFieldNotBlank(FieldNo);
    END;

    LOCAL PROCEDURE TestFieldBlank@52006576(FieldNo@52006500 : Integer);
    BEGIN
      ValidationMgt.AddRuleFieldBlank(FieldNo);
    END;

    LOCAL PROCEDURE AddError@52006514(Msg@52006500 : Text);
    BEGIN
      IF Msg <> '' THEN
        ValidationMgt.AddError(Msg + '\');
    END;

    LOCAL PROCEDURE SetRecord@52006533(RecOrRecRef@52006500 : Variant;Name@52006501 : Text);
    BEGIN
      ValidationMgt.Validate;
      ValidationMgt.SetRecord(RecOrRecRef, Name);
    END;

    LOCAL PROCEDURE ThrowErrors@52006565();
    BEGIN
      ValidationMgt.Validate;
      ValidationMgt.ThrowErrorIfInvalid;
    END;

    LOCAL PROCEDURE AddErrorifVendorInvoiceNoAlreadyExists@52006510(purchHeader@52006500 : Record 38);
    VAR
      purchInvHeader@52006501 : Record 122;
    BEGIN
      WITH purchHeader DO BEGIN
        IF "Vendor Invoice No." = '' THEN
          EXIT;
        IF "Document Type" IN ["Document Type"::Order, "Document Type"::Invoice] THEN BEGIN
          purchInvHeader.SETCURRENTKEY("Pay-to Vendor No.", "Vendor Invoice No.", "Print Serie");
          purchInvHeader.SETRANGE("Pay-to Vendor No.", "Pay-to Vendor No.");
          purchInvHeader.SETRANGE("Print Serie", "Print Serie");
          purchInvHeader.SETRANGE("Credit Memos", FALSE);
          purchInvHeader.SETRANGE("Vendor Invoice No.", "Vendor Invoice No.");
          IF purchInvHeader.COUNT > 0 THEN
            AddError(STRSUBSTNO(DocumentAlreadyExists, "Vendor Invoice No.", "Pay-to Vendor No.", "Pay-to Name"));
        END;
      END;
    END;

    BEGIN
    END.
  }
}

