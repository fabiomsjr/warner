OBJECT Table 99000866 Capacity Constrained Resource
{
  OBJECT-PROPERTIES
  {
    Date=07/09/12;
    Time=12:00:00;
    Version List=NAVW17.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Capacity Constrained Resource;
               PTB=Restri��o Capacidade Recurso];
  }
  FIELDS
  {
    { 1   ;   ;Capacity No.        ;Code20        ;TableRelation=IF (Capacity Type=CONST(Work Center)) "Work Center"
                                                                 ELSE IF (Capacity Type=CONST(Machine Center)) "Machine Center";
                                                   OnValidate=BEGIN
                                                                IF "Capacity No." = '' THEN
                                                                  EXIT;

                                                                CASE "Capacity Type" OF
                                                                  "Capacity Type"::"Work Center":
                                                                    BEGIN
                                                                      WorkCenter.GET("Capacity No.");
                                                                      WorkCenter.TESTFIELD(Blocked,FALSE);
                                                                      Name := WorkCenter.Name;
                                                                      "Work Center No." := WorkCenter."No.";
                                                                    END;
                                                                  "Capacity Type"::"Machine Center":
                                                                    BEGIN
                                                                      MachineCenter.GET("Capacity No.");
                                                                      MachineCenter.TESTFIELD(Blocked,FALSE);
                                                                      Name := MachineCenter.Name;
                                                                      "Work Center No." := MachineCenter."Work Center No.";
                                                                    END
                                                                END;

                                                                "Critical Load %" := 100;
                                                                "Dampener (% of Total Capacity)" := 0;
                                                              END;

                                                   CaptionML=[ENU=Capacity No.;
                                                              PTB=N� Capacidade] }
    { 2   ;   ;Capacity Type       ;Option        ;OnValidate=BEGIN
                                                                "Capacity No." := '';
                                                              END;

                                                   CaptionML=[ENU=Capacity Type;
                                                              PTB=Tipo Capacidade];
                                                   OptionCaptionML=[ENU=Work Center,Machine Center;
                                                                    PTB=Centro Trabalho,Centro M�quina];
                                                   OptionString=Work Center,Machine Center }
    { 3   ;   ;Name                ;Text50        ;CaptionML=[ENU=Name;
                                                              PTB=Nome] }
    { 10  ;   ;Critical Load %     ;Decimal       ;OnValidate=BEGIN
                                                                IF "Critical Load %" < 0 THEN
                                                                  "Critical Load %" := 0;
                                                                IF "Critical Load %" > 100 THEN
                                                                  "Critical Load %" := 100;
                                                                IF "Critical Load %" + "Dampener (% of Total Capacity)" > 100 THEN
                                                                  "Dampener (% of Total Capacity)" := 100 - "Critical Load %";
                                                              END;

                                                   CaptionML=[ENU=Critical Load %;
                                                              PTB=Carga Cr�tica %];
                                                   DecimalPlaces=1:1 }
    { 11  ;   ;Dampener (% of Total Capacity);Decimal;
                                                   OnValidate=BEGIN
                                                                IF "Dampener (% of Total Capacity)" < 0 THEN
                                                                  "Dampener (% of Total Capacity)" := 0;
                                                                IF "Dampener (% of Total Capacity)" > 100 THEN
                                                                  "Dampener (% of Total Capacity)" := 100;
                                                                IF "Dampener (% of Total Capacity)" + "Critical Load %" > 100 THEN
                                                                  "Critical Load %" := 100 - "Dampener (% of Total Capacity)";
                                                              END;

                                                   CaptionML=[ENU=Dampener (% of Total Capacity);
                                                              PTB=Amortecedor (% Capac. Total)];
                                                   DecimalPlaces=1:1 }
    { 14  ;   ;Work Center No.     ;Code20        ;TableRelation="Work Center";
                                                   CaptionML=[ENU=Work Center No.;
                                                              PTB=N� Centro Trabalho] }
    { 39  ;   ;Date Filter         ;Date          ;FieldClass=FlowFilter;
                                                   CaptionML=[ENU=Date Filter;
                                                              PTB=Filtro Data] }
    { 40  ;   ;Work Shift Filter   ;Code10        ;FieldClass=FlowFilter;
                                                   TableRelation="Work Shift";
                                                   CaptionML=[ENU=Work Shift Filter;
                                                              PTB=Filtro Turno] }
    { 42  ;   ;Capacity (Effective);Decimal       ;FieldClass=FlowField;
                                                   CalcFormula=Sum("Calendar Entry"."Capacity (Effective)" WHERE (Capacity Type=FIELD(Capacity Type),
                                                                                                                  No.=FIELD(Capacity No.),
                                                                                                                  Work Shift Code=FIELD(Work Shift Filter),
                                                                                                                  Date=FIELD(Date Filter)));
                                                   CaptionML=[ENU=Capacity (Effective);
                                                              PTB=Capacidade (Efetiva)];
                                                   DecimalPlaces=0:5;
                                                   Editable=No }
    { 44  ;   ;Prod. Order Need Qty.;Decimal      ;FieldClass=FlowField;
                                                   CalcFormula=Sum("Prod. Order Capacity Need"."Allocated Time" WHERE (Type=FIELD(Capacity Type),
                                                                                                                       No.=FIELD(Capacity No.),
                                                                                                                       Date=FIELD(Date Filter)));
                                                   CaptionML=[ENU=Prod. Order Need Qty.;
                                                              PTB=Qtd. Nec. Ord. Produ��o];
                                                   DecimalPlaces=0:5;
                                                   Editable=No }
    { 46  ;   ;Work Center Load Qty.;Decimal      ;FieldClass=FlowField;
                                                   CalcFormula=Sum("Prod. Order Capacity Need"."Allocated Time" WHERE (Work Center No.=FIELD(Work Center No.),
                                                                                                                       Date=FIELD(Date Filter)));
                                                   CaptionML=[ENU=Work Center Load Qty.;
                                                              PTB=Qtd. carga Work Center];
                                                   DecimalPlaces=0:5;
                                                   Editable=No }
    { 48  ;   ;Prod. Order Need Qty. for Plan;Decimal;
                                                   FieldClass=FlowField;
                                                   CalcFormula=Sum("Prod. Order Capacity Need"."Allocated Time" WHERE (Type=FIELD(Capacity Type),
                                                                                                                       No.=FIELD(Capacity No.),
                                                                                                                       Date=FIELD(Date Filter),
                                                                                                                       Active=CONST(Yes)));
                                                   CaptionML=[ENU=Prod. Order Need Qty. for Plan;
                                                              PTB=Qtd. Planmnto Nec. Ord. Prod.];
                                                   DecimalPlaces=0:5;
                                                   Editable=No }
    { 49  ;   ;Work Center Load Qty. for Plan;Decimal;
                                                   FieldClass=FlowField;
                                                   CalcFormula=Sum("Prod. Order Capacity Need"."Allocated Time" WHERE (Work Center No.=FIELD(Work Center No.),
                                                                                                                       Date=FIELD(Date Filter),
                                                                                                                       Active=CONST(Yes)));
                                                   CaptionML=[ENU=Work Center Load Qty. for Plan;
                                                              PTB=Qtd. Plamnto carga Work Center];
                                                   DecimalPlaces=0:5;
                                                   Editable=No }
  }
  KEYS
  {
    {    ;Capacity Type,Capacity No.              ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      WorkCenter@1000 : Record 99000754;
      MachineCenter@1001 : Record 99000758;

    BEGIN
    END.
  }
}

