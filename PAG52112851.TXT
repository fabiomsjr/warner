OBJECT Page 52112851 Accounting Posted Mov. Related
{
  OBJECT-PROPERTIES
  {
    Date=30/04/13;
    Time=18:16:38;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Accounting Posted Mov. Related;
               PTB=Hist. Mov. Cont. Relacionado];
    SourceTable=Table52112840;
    PageType=Card;
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1102300000;1;Group  ;
                GroupType=Repeater }

    { 1102300001;2;Field  ;
                SourceExpr="Entry No." }

    { 1102300003;2;Field  ;
                SourceExpr="G/L Account No." }

    { 1102300005;2;Field  ;
                SourceExpr="Posting Date" }

    { 1102300007;2;Field  ;
                SourceExpr="Document No." }

    { 1102300009;2;Field  ;
                SourceExpr=Description }

    { 1102300011;2;Field  ;
                SourceExpr="Bal. Account No." }

    { 1102300013;2;Field  ;
                SourceExpr=Amount }

    { 1102300015;2;Field  ;
                SourceExpr="Global Dimension 1 Code" }

    { 1102300017;2;Field  ;
                SourceExpr="Debit Amount" }

    { 1102300019;2;Field  ;
                SourceExpr="Credit Amount" }

    { 1102300021;2;Field  ;
                SourceExpr="G/L Account No. Rel." }

    { 1102300023;2;Field  ;
                SourceExpr="Release Type" }

  }
  CODE
  {

    BEGIN
    END.
  }
}

