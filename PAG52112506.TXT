OBJECT Page 52112506 Invoices Lines List
{
  OBJECT-PROPERTIES
  {
    Date=25/06/15;
    Time=10:31:22;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Invoices Lines List (Lines);
               PTB=Lista Notas Fiscais (Linhas)];
    InsertAllowed=No;
    DeleteAllowed=No;
    ModifyAllowed=No;
    SourceTable=Table52112471;
    SourceTableView=SORTING(Type,Document No.,Line No.)
                    ORDER(Ascending);
    PageType=List;
    OnOpenPage=VAR
                 CustomerNoCaption@52006500 : TextConst 'ENU=Customer No.;PTB=N� Cliente';
                 CustomerNameCaption@52006501 : TextConst 'ENU=Customer Name;PTB=Nome Cliente';
                 CustomerCNPJCaption@52006502 : TextConst 'ENU=Customer C.N.P.J.;PTB=C.N.P.J Cliente';
                 VendorNoCaption@52006505 : TextConst 'ENU=Vendor No.;PTB=N� Fornecedor';
                 VendorNameCaption@52006504 : TextConst 'ENU=Vendor Name;PTB=Nome Fornecedor';
                 VendorCNPJCaption@52006503 : TextConst 'ENU=Vendor C.N.P.J.;PTB=C.N.P.J Fornecedor';
               BEGIN
                 CASE Type OF
                   Type::Sales: BEGIN
                     CustVenNo := CustomerNoCaption;
                     CustVenName := CustomerNameCaption;
                     CustVenCNPJ := CustomerCNPJCaption;
                   END;
                   Type::Purchase: BEGIN
                     CustVenNo := VendorNoCaption;
                     CustVenName := VendorNameCaption;
                     CustVenCNPJ := VendorCNPJCaption;
                   END;
                 END;
               END;

  }
  CONTROLS
  {
    { 52006500;0;Container;
                ContainerType=ContentArea }

    { 52006501;1;Group    ;
                Name=Group;
                GroupType=Repeater }

    { 52006502;2;Field    ;
                SourceExpr="Document No." }

    { 52006503;2;Field    ;
                SourceExpr="Invoice No." }

    { 52006504;2;Field    ;
                SourceExpr="Line No.";
                Visible=FALSE }

    { 52006505;2;Field    ;
                SourceExpr="Cust/Vendor No.";
                CaptionClass=CustVenNo }

    { 52006506;2;Field    ;
                SourceExpr="Cust/Vendor Name";
                CaptionClass=CustVenName }

    { 52006507;2;Field    ;
                SourceExpr="Cust/Vendor CNPJ";
                CaptionClass=CustVenCNPJ }

    { 52006508;2;Field    ;
                SourceExpr="Posting Date" }

    { 52006509;2;Field    ;
                SourceExpr="Item Code" }

    { 52006510;2;Field    ;
                SourceExpr=Description }

    { 52006511;2;Field    ;
                SourceExpr=Quantity }

    { 52006512;2;Field    ;
                SourceExpr="Currency Code" }

    { 52006513;2;Field    ;
                SourceExpr="Unit Value" }

    { 52006514;2;Field    ;
                SourceExpr="Total Amount" }

    { 52006520;2;Field    ;
                SourceExpr="Gross Amount" }

    { 52006521;2;Field    ;
                SourceExpr="Net Amount";
                Visible=FALSE }

    { 52006515;2;Field    ;
                SourceExpr=IPI }

    { 52006516;2;Field    ;
                SourceExpr=PIS }

    { 52006517;2;Field    ;
                SourceExpr=ICMS }

    { 52006519;2;Field    ;
                SourceExpr=ST }

    { 52006518;2;Field    ;
                SourceExpr=COFINS }

    { 52006522;2;Field    ;
                SourceExpr=ISS }

    { 52006523;2;Field    ;
                SourceExpr=DIFAL }

  }
  CODE
  {
    VAR
      CustVenNo@52006502 : Text;
      CustVenName@52006503 : Text;
      CustVenCNPJ@52006500 : Text;

    BEGIN
    END.
  }
}

