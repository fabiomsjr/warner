OBJECT Page 52112735 Request Inventory by Location
{
  OBJECT-PROPERTIES
  {
    Date=29/10/14;
    Time=13:15:41;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Inventory by Location;
               PTB=Estoque por Dep�sito];
    InsertAllowed=No;
    DeleteAllowed=No;
    SourceTable=Table52112728;
    PageType=Worksheet;
    ActionList=ACTIONS
    {
      { 52006505;    ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 52006506;1   ;Action    ;
                      Name=ItemTrackingLines;
                      ShortCutKey=Shift+Ctrl+I;
                      CaptionML=[ENU=Item &Tracking Lines;
                                 PTB=Linhas &Rastreabilidade Produto];
                      Promoted=Yes;
                      Image=ItemTrackingLines;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 CallItemTracking;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 52006500;0;Container;
                ContainerType=ContentArea }

    { 52006501;1;Group    ;
                Name=Group;
                GroupType=Repeater }

    { 52006502;2;Field    ;
                SourceExpr="Location Code";
                Editable=FALSE }

    { 52006507;2;Field    ;
                SourceExpr="Bin Code";
                Visible=FALSE }

    { 52006503;2;Field    ;
                SourceExpr="Qty. to be Fulfilled" }

    { 52006504;2;Field    ;
                SourceExpr=Balance;
                CaptionClass=FIELDCAPTION("Inventory Balance");
                Editable=FALSE }

  }
  CODE
  {

    BEGIN
    END.
  }
}

