OBJECT Table 77 Report Selections
{
  OBJECT-PROPERTIES
  {
    Date=23/09/13;
    Time=12:00:00;
    Version List=NAVW17.10,NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Report Selections;
               PTB=Sele��o Relat�rios];
  }
  FIELDS
  {
    { 1   ;   ;Usage               ;Option        ;CaptionML=[ENU=Usage;
                                                              PTB=Utiliza��o];
                                                   OptionCaptionML=[ENU=S.Quote,S.Order,S.Invoice,S.Cr.Memo,S.Test,P.Quote,P.Order,P.Invoice,P.Cr.Memo,P.Receipt,P.Ret.Shpt.,P.Test,B.Stmt,B.Recon.Test,B.Check,Reminder,Fin.Charge,Rem.Test,F.C.Test,Prod. Order,S.Blanket,P.Blanket,M1,M2,M3,M4,Inv1,Inv2,Inv3,SM.Quote,SM.Order,SM.Invoice,SM.Credit Memo,SM.Contract Quote,SM.Contract,SM.Test,S.Return,P.Return,S.Shipment,S.Ret.Rcpt.,S.Work Order,Invt. Period Test,SM.Shipment,S.Test Prepmt.,P.Test Prepmt.,S.Arch. Quote,S.Arch. Order,P.Arch. Quote,P.Arch. Order,S. Arch. Return Order,P. Arch. Return Order,Asm. Order,P.Assembly Order,S.Order Pick Instruction;
                                                                    PTB=Cota��o V.,Pedido V.,Nota Fiscal V.,Nota Cr�dito V.,Teste V.,Cota��o C.,Pedido C,Nota Fiscal C.,Nota Cr�dito C.,Recebimento C.,Devolu��o C.,Teste C.,Decl. B.,Teste Reconc. B.,Ver. B.,Lembrete,Encargo Fin.,Teste Rem.,Teste F.C.,OP,S. Cobertura,P. Cobertura,M1,M2,M3,M4,Inv1,Inv2,Inv3,Cota��o SM,Pedido SM,Nota Fiscal SM,Nota Cr�dito SM,Cota��o Contrato SM,Contrato SM.,Teste SM,Devolu��o V,Retono C,Envio Vendas,Rec. Dev. V,V. Pedido Trabalho,Teste Per�odo Inv.,Envio SM,Adiantamento Teste V.,Adiantamento Teste C.,Cot. Venda Arq.,Ped. Venda Arq.];
                                                   OptionString=S.Quote,S.Order,S.Invoice,S.Cr.Memo,S.Test,P.Quote,P.Order,P.Invoice,P.Cr.Memo,P.Receipt,P.Ret.Shpt.,P.Test,B.Stmt,B.Recon.Test,B.Check,Reminder,Fin.Charge,Rem.Test,F.C.Test,Prod. Order,S.Blanket,P.Blanket,M1,M2,M3,M4,Inv1,Inv2,Inv3,SM.Quote,SM.Order,SM.Invoice,SM.Credit Memo,SM.Contract Quote,SM.Contract,SM.Test,S.Return,P.Return,S.Shipment,S.Ret.Rcpt.,S.Work Order,Invt. Period Test,SM.Shipment,S.Test Prepmt.,P.Test Prepmt.,S.Arch. Quote,S.Arch. Order,P.Arch. Quote,P.Arch. Order,S. Arch. Return Order,P. Arch. Return Order,Asm. Order,P.Assembly Order,S.Order Pick Instruction }
    { 2   ;   ;Sequence            ;Code10        ;CaptionML=[ENU=Sequence;
                                                              PTB=Sequ�ncia];
                                                   Numeric=Yes }
    { 3   ;   ;Report ID           ;Integer       ;TableRelation=Object.ID WHERE (Type=CONST(Report));
                                                   OnValidate=BEGIN
                                                                CALCFIELDS("Report Caption");
                                                              END;

                                                   CaptionML=[ENU=Report ID;
                                                              PTB=N� Relat�rio] }
    { 4   ;   ;Report Caption      ;Text250       ;FieldClass=FlowField;
                                                   CalcFormula=Lookup(AllObjWithCaption."Object Caption" WHERE (Object Type=CONST(Report),
                                                                                                                Object ID=FIELD(Report ID)));
                                                   CaptionML=[ENU=Report Caption;
                                                              PTB=Nome Relat�rio];
                                                   Editable=No }
    { 52112430;;Fiscal Document Type;Code20       ;TableRelation="Fiscal Document Type";
                                                   CaptionML=[ENU=Fiscal Document Type;
                                                              PTB=Tipo Documento Fiscal] }
  }
  KEYS
  {
    {    ;Usage,Sequence                          ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      ReportSelection2@1000 : Record 77;

    PROCEDURE NewRecord@1();
    BEGIN
      ReportSelection2.SETRANGE(Usage,Usage);
      IF ReportSelection2.FINDLAST AND (ReportSelection2.Sequence <> '') THEN
        Sequence := INCSTR(ReportSelection2.Sequence)
      ELSE
        Sequence := '1';
    END;

    BEGIN
    END.
  }
}

