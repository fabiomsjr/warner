OBJECT Table 35000708 Cnab - Charge Instructions2
{
  OBJECT-PROPERTIES
  {
    Date=29/02/12;
    Time=16:21:35;
    Version List=FNX001.00;
  }
  PROPERTIES
  {
  }
  FIELDS
  {
    { 1   ;   ;Cod_Charge-Instruction;Code3       ;CaptionML=[ENU=Charge Instructions Code;
                                                              PTB=C�digo Instru��es Cobran�a] }
    { 2   ;   ;Description         ;Text60        ;CaptionML=[ENU=Description;
                                                              PTB=Descri��o] }
  }
  KEYS
  {
    {    ;Cod_Charge-Instruction                  ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    {
      --- THBR2.82 ---
      thiagoAS,101209, Changed bank layout

      --- THBR147.00 ---
      rafaelr,130411,Electronic Receipts from NAV 3.7 to 5.0
    }
    END.
  }
}

