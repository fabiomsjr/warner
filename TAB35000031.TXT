OBJECT Table 35000031 Tax Settlement Amounts 20092
{
  OBJECT-PROPERTIES
  {
    Date=29/02/12;
    Time=13:33:35;
    Version List=FNX033.00;
  }
  PROPERTIES
  {
    CaptionML=PTB=Vl. Apura��o de Impostos;
    LookupPageID=Page35000023;
    DrillDownPageID=Page35000023;
  }
  FIELDS
  {
    { 1   ;   ;Tax Identification  ;Option        ;OptionCaptionML=[ENU=" ,IPI,ICMS,PIS,COFINS,II,IRRF,INSS,ISS,CSL Ret.,PIS Ret.,COFINS Ret.,ISS Ret.,INSS Ret.,Dif. Aliq. ICMS,ST";
                                                                    PTB=" ,IPI,ICMS,PIS,COFINS,II,IRRF,INSS,ISS,CSL Ret.,PIS Ret.,COFINS Ret.,ISS Ret.,INSS Ret.,Dif. Aliq. ICMS,ST"];
                                                   OptionString=[ ,IPI,ICMS,PIS,COFINS,II,IRRF,INSS,ISS,CSL Ret.,PIS Ret.,COFINS Ret.,ISS Ret.,INSS Ret.,Dif. Aliq. ICMS,ST] }
    { 2   ;   ;Tax Settlement No.  ;Code20        ;TableRelation="Tax Settlement 20092";
                                                   CaptionML=[ENU=Settlement Code;
                                                              PTB=Cod. Apura��o] }
    { 3   ;   ;Settlement Amount Type;Option      ;CaptionML=[ENU=Settlement Amount Type;
                                                              PTB=Tipo de Valores Apura��o];
                                                   OptionCaptionML=[ENU=Other Debits,Reversal Credits,Other Credits,Reversal Debits,Deductions;
                                                                    PTB=Outros D�bitos,Estorno Cr�ditos,Outros Cr�ditos,Estorno D�bitos,Dedu��es];
                                                   OptionString=Other Debits,Reversal Credits,Other Credits,Reversal Debits,Deductions }
    { 4   ;   ;Description         ;Text100       ;CaptionML=[ENU=Description;
                                                              PTB=Descri��o] }
    { 5   ;   ;Amount              ;Decimal       ;CaptionML=[ENU=Amount;
                                                              PTB=Valor] }
    { 6   ;   ;Base Amount         ;Decimal       ;CaptionML=[ENU=Basis Amount;
                                                              PTB=Base C�lculo] }
    { 7   ;   ;Est. Settlement Group Code;Code20  ;TableRelation="Est. Settlement Group.2";
                                                   CaptionML=[ENU=Est. Settlement Group;
                                                              PTB=C�digo Apura��o ICMS] }
    { 8   ;   ;Line No.            ;Integer        }
    { 1000;   ;CIAP Credit         ;Boolean        }
    { 1001;   ;FECP                ;Boolean        }
    { 35000600;;Adjust Ap. Code    ;Code10        ;CaptionML=PTB=C�d. Ajuste Apura��o EFD }
    { 35000601;;Document No.       ;Code20        ;CaptionML=PTB=N� Documento }
    { 35000602;;Referrence Date    ;Date           }
  }
  KEYS
  {
    {    ;Tax Identification,Tax Settlement No.,Settlement Amount Type,Line No.;
                                                   SumIndexFields=Amount,Base Amount;
                                                   Clustered=Yes }
    {    ;Est. Settlement Group Code              ;SumIndexFields=Amount,Base Amount }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    {
      NAVBR5.01.0109

      No.   dd.mm.yy   Developer   Company   DocNo.   Description
      -----------------------------------------------------------------------------------------------------------------
      00    01.12.06   MK          MICROSOFT LOC001   The reference Documentation file LOCALIZATIONBR.DOC
    }
    END.
  }
}

