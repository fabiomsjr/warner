OBJECT Table 5094 Close Opportunity Code
{
  OBJECT-PROPERTIES
  {
    Date=07/09/12;
    Time=12:00:00;
    Version List=NAVW17.00;
  }
  PROPERTIES
  {
    DataCaptionFields=Code,Description;
    CaptionML=[ENU=Close Opportunity Code;
               PTB=Cod. Fech. Oportunidade];
    LookupPageID=Page5133;
  }
  FIELDS
  {
    { 1   ;   ;Code                ;Code10        ;CaptionML=[ENU=Code;
                                                              PTB=C�digo];
                                                   NotBlank=Yes }
    { 2   ;   ;Description         ;Text50        ;CaptionML=[ENU=Description;
                                                              PTB=Descri��o] }
    { 3   ;   ;No. of Opportunities;Integer       ;FieldClass=FlowField;
                                                   CalcFormula=Count("Opportunity Entry" WHERE (Close Opportunity Code=FIELD(Code)));
                                                   CaptionML=[ENU=No. of Opportunities;
                                                              PTB=N� de Oportunidades];
                                                   Editable=No }
    { 4   ;   ;Type                ;Option        ;CaptionML=[ENU=Type;
                                                              PTB=Tipo];
                                                   OptionCaptionML=[ENU=Won,Lost;
                                                                    PTB=Ganho,Perdido];
                                                   OptionString=Won,Lost }
  }
  KEYS
  {
    {    ;Code                                    ;Clustered=Yes }
  }
  FIELDGROUPS
  {
    { 1   ;DropDown            ;Code,Description,Type                    }
  }
  CODE
  {

    BEGIN
    END.
  }
}

