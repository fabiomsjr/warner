OBJECT Table 35000728 CST Impostos2
{
  OBJECT-PROPERTIES
  {
    Date=16/04/12;
    Time=16:48:55;
    Version List=FNX048.00;
  }
  PROPERTIES
  {
    DataPerCompany=No;
    CaptionML=[ENU=CST Codes;
               PTB=CST Impostos];
    LookupPageID=Page35000058;
    DrillDownPageID=Page35000058;
  }
  FIELDS
  {
    { 1   ;   ;Type Tax            ;Option        ;CaptionML=[ENU=Tipo;
                                                              PTB=Tipo Imposto];
                                                   OptionCaptionML=ENU=,IPI,PIS,COFINS;
                                                   OptionString=,IPI,PIS,COFINS;
                                                   BlankZero=Yes }
    { 2   ;   ;Code                ;Code10        ;CaptionML=[ENU=Code;
                                                              PTB=C�digo] }
    { 3   ;   ;Description         ;Text200       ;CaptionML=[ENU=Description;
                                                              PTB=Descri��o] }
    { 4   ;   ;Type                ;Option        ;CaptionML=PTB=Tipo;
                                                   OptionCaptionML=[ENU=,Imput,output;
                                                                    PTB=,Entrada,Sa�da,Entrada/Sa�da];
                                                   OptionString=,Entrada,Sa�da,Entrada/Sa�da;
                                                   BlankZero=Yes }
  }
  KEYS
  {
    {    ;Code,Type Tax,Type                      ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

