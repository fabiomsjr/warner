OBJECT Table 6832 EP WPR Table Filter Field
{
  OBJECT-PROPERTIES
  {
    Date=29/02/12;
    Time=18:14:16;
    Version List=NAVW16.00;
  }
  PROPERTIES
  {
    OnInsert=VAR
               Field@1000 : Record 2000000041;
             BEGIN
               Field.GET("Table No.","Filter Field No.");
             END;

    OnRename=VAR
               Field@1000 : Record 2000000041;
             BEGIN
               Field.GET("Table No.","Filter Field No.");
             END;

    CaptionML=[ENU=EP WPR Table Filter Field;
               PTB=EP WPR Campo Filtro Tabela];
  }
  FIELDS
  {
    { 1   ;   ;Group Code          ;Code20        ;TableRelation="EP Group".Code;
                                                   CaptionML=[ENU=Group Code;
                                                              PTB=Cod. Grupo] }
    { 2   ;   ;WP Request Code     ;Code20        ;TableRelation=Table6805.Field2 WHERE (Field1=FIELD(Group Code));
                                                   CaptionML=[ENU=WP Request Code;
                                                              PTB=C�d. Requisi��o WP] }
    { 3   ;   ;Filter Field No.    ;Integer       ;TableRelation=Field.No. WHERE (TableNo=FIELD(Table No.));
                                                   OnValidate=BEGIN
                                                                CALCFIELDS("Filter Field Name");
                                                              END;

                                                   CaptionML=[ENU=Filter Field No.;
                                                              PTB=Filtro Campo No.] }
    { 4   ;   ;Filter Field Name   ;Text30        ;FieldClass=FlowField;
                                                   CalcFormula=Lookup(Field."Field Caption" WHERE (TableNo=FIELD(Table No.),
                                                                                                   No.=FIELD(Filter Field No.)));
                                                   CaptionML=[ENU=Filter Field Name;
                                                              PTB=Filtro Campo Nome];
                                                   Editable=No }
    { 5   ;   ;Filter Field Value  ;Text80        ;CaptionML=[ENU=Filter Field Value;
                                                              PTB=Filtro Campo Valor] }
    { 6   ;   ;Table No.           ;Integer       ;CaptionML=[ENU=Table No.;
                                                              PTB=N� Tabela] }
  }
  KEYS
  {
    {    ;Group Code,WP Request Code,Table No.,Filter Field No.;
                                                   Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

