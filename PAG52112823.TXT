OBJECT Page 52112823 ECD Setup
{
  OBJECT-PROPERTIES
  {
    Date=28/03/16;
    Time=08:13:32;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=ECD Setup;
               PTB=Configura��o ECD];
    InsertAllowed=No;
    DeleteAllowed=No;
    SourceTable=Table52112823;
    PageType=Card;
    OnOpenPage=BEGIN
                 RESET;
                 IF NOT GET THEN BEGIN
                   INIT;
                   INSERT;
                 END;
               END;

  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1102300000;1;Group  ;
                CaptionML=[ENU=General;
                           PTB=Geral] }

    { 52006502;2;Field    ;
                SourceExpr="Company Name" }

    { 1102300001;2;Field  ;
                SourceExpr="Ident. Code Acc. Bookkeeping" }

    { 1102300012;2;Field  ;
                SourceExpr="Bookkeeping Type" }

    { 1102300004;2;Field  ;
                SourceExpr="Dimension Code Cost Center" }

    { 1102300014;2;Field  ;
                SourceExpr="Resp. Admin. Register" }

    { 1102300016;2;Field  ;
                SourceExpr="Month End" }

    { 52006500;2;Group    ;
                CaptionML=[ENU=FCont;
                           PTB=FCont];
                GroupType=Group }

    { 52006501;3;Field    ;
                SourceExpr="FCont Posting Type Dim." }

    { 1900604001;1;Group  ;
                CaptionML=[ENU=Numeration;
                           PTB=Numera��o] }

    { 1102300006;2;Field  ;
                SourceExpr="Sped Mov Nos." }

  }
  CODE
  {

    BEGIN
    END.
  }
}

