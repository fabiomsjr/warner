OBJECT Page 5116 Salesperson/Purchaser Card
{
  OBJECT-PROPERTIES
  {
    Date=09/09/14;
    Time=12:00:00;
    Version List=NAVW18.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Salesperson/Purchaser Card;
               PTB=Cart�o Vendedor/Comprador];
    SourceTable=Table13;
    PageType=Card;
    OnInsertRecord=BEGIN
                     IF xRec.Code = '' THEN
                       RESET;
                   END;

    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 19      ;1   ;ActionGroup;
                      CaptionML=[ENU=&Salesperson;
                                 PTB=&Vendedores];
                      Image=SalesPerson }
      { 28      ;2   ;Action    ;
                      CaptionML=[ENU=Tea&ms;
                                 PTB=Tea&ms];
                      RunObject=Page 5107;
                      RunPageView=SORTING(Salesperson Code);
                      RunPageLink=Salesperson Code=FIELD(Code);
                      Image=TeamSales }
      { 25      ;2   ;Action    ;
                      CaptionML=[ENU=Con&tacts;
                                 PTB=Con&tacts];
                      RunObject=Page 5052;
                      RunPageView=SORTING(Salesperson Code);
                      RunPageLink=Salesperson Code=FIELD(Code);
                      Image=CustomerContact }
      { 23      ;2   ;Action    ;
                      ShortCutKey=Shift+Ctrl+D;
                      CaptionML=[ENU=Dimensions;
                                 PTB=Dimens�es];
                      RunObject=Page 540;
                      RunPageLink=Table ID=CONST(13),
                                  No.=FIELD(Code);
                      Image=Dimensions }
      { 21      ;2   ;Action    ;
                      ShortCutKey=F7;
                      CaptionML=[ENU=Statistics;
                                 PTB=Estatisticas];
                      RunObject=Page 5117;
                      RunPageLink=Code=FIELD(Code);
                      Promoted=Yes;
                      Image=Statistics;
                      PromotedCategory=Process }
      { 26      ;2   ;Action    ;
                      CaptionML=[ENU=C&ampaigns;
                                 PTB=C&ampanhas];
                      RunObject=Page 5087;
                      RunPageView=SORTING(Salesperson Code);
                      RunPageLink=Salesperson Code=FIELD(Code);
                      Image=Campaign }
      { 27      ;2   ;Action    ;
                      CaptionML=[ENU=S&egments;
                                 PTB=S&egmentos];
                      RunObject=Page 5093;
                      RunPageView=SORTING(Salesperson Code);
                      RunPageLink=Salesperson Code=FIELD(Code);
                      Image=Segment }
      { 33      ;2   ;Separator ;
                      CaptionML=[ENU="";
                                 PTB=""] }
      { 32      ;2   ;Action    ;
                      ShortCutKey=Ctrl+F7;
                      CaptionML=[ENU=Interaction Log E&ntries;
                                 PTB=Interaction Log E&ntries];
                      RunObject=Page 5076;
                      RunPageView=SORTING(Salesperson Code);
                      RunPageLink=Salesperson Code=FIELD(Code);
                      Image=InteractionLog }
      { 76      ;2   ;Action    ;
                      CaptionML=[ENU=Postponed &Interactions;
                                 PTB=Postponed &Interactions];
                      RunObject=Page 5082;
                      RunPageView=SORTING(Salesperson Code);
                      RunPageLink=Salesperson Code=FIELD(Code);
                      Image=PostponedInteractions }
      { 35      ;2   ;Action    ;
                      CaptionML=[ENU=T&o-dos;
                                 PTB=T&o-dos];
                      RunObject=Page 5096;
                      RunPageView=SORTING(Salesperson Code);
                      RunPageLink=Salesperson Code=FIELD(Code),
                                  System To-do Type=FILTER(Organizer|Salesperson Attendee);
                      Image=TaskList }
      { 78      ;2   ;ActionGroup;
                      CaptionML=[ENU=Oppo&rtunities;
                                 PTB=Opo&rtunidades];
                      Image=OpportunityList }
      { 34      ;3   ;Action    ;
                      CaptionML=[ENU=List;
                                 PTB=Lista];
                      RunObject=Page 5123;
                      RunPageView=SORTING(Salesperson Code);
                      RunPageLink=Salesperson Code=FIELD(Code);
                      Image=OpportunitiesList }
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 36      ;1   ;Action    ;
                      AccessByPermission=TableData 5062=R;
                      CaptionML=[ENU=Create &Interact;
                                 PTB=Create &Interact];
                      Promoted=Yes;
                      Image=CreateInteraction;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 CreateInteraction;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                CaptionML=[ENU=General;
                           PTB=Geral] }

    { 2   ;2   ;Field     ;
                SourceExpr=Code }

    { 4   ;2   ;Field     ;
                SourceExpr=Name }

    { 22  ;2   ;Field     ;
                SourceExpr="Job Title" }

    { 6   ;2   ;Field     ;
                SourceExpr="Commission %" }

    { 10  ;2   ;Field     ;
                SourceExpr="Phone No." }

    { 8   ;2   ;Field     ;
                SourceExpr="E-Mail" }

    { 12  ;2   ;Field     ;
                SourceExpr="Next To-do Date" }

    { 1905885101;1;Group  ;
                CaptionML=[ENU=Invoicing;
                           PTB=Faturar] }

    { 14  ;2   ;Field     ;
                SourceExpr="Global Dimension 1 Code" }

    { 16  ;2   ;Field     ;
                SourceExpr="Global Dimension 2 Code" }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

