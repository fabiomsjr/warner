OBJECT Table 1019 Job Difference Buffer
{
  OBJECT-PROPERTIES
  {
    Date=05/11/08;
    Time=12:00:00;
    Version List=NAVW16.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Job Difference Buffer;
               PTB=Diferen�a Buffer Tarefa];
  }
  FIELDS
  {
    { 1   ;   ;Job No.             ;Code20        ;CaptionML=[ENU=Job No.;
                                                              PTB=N� Projeto] }
    { 2   ;   ;Job Task No.        ;Code20        ;CaptionML=[ENU=Job Task No.;
                                                              PTB=No. Processamento Tarefa] }
    { 3   ;   ;Type                ;Option        ;CaptionML=[ENU=Type;
                                                              PTB=Tipo];
                                                   OptionCaptionML=[ENU=Resource,Item,G/L Account;
                                                                    PTB=Recurso,Item,Conta Cont�bil];
                                                   OptionString=Resource,Item,G/L Account }
    { 4   ;   ;Location Code       ;Code10        ;CaptionML=[ENU=Location Code;
                                                              PTB=Cod. Dep�sito] }
    { 5   ;   ;Variant Code        ;Code10        ;CaptionML=[ENU=Variant Code;
                                                              PTB=C�digo Variante] }
    { 6   ;   ;Unit of Measure code;Code10        ;CaptionML=[ENU=Unit of Measure code;
                                                              PTB=Cod. Unidade Medida] }
    { 7   ;   ;Entry type          ;Option        ;CaptionML=[ENU=Entry type;
                                                              PTB=Tipo Mov.];
                                                   OptionCaptionML=[ENU=Schedule,Usage;
                                                                    PTB=Programa��o,Uso];
                                                   OptionString=Schedule,Usage }
    { 8   ;   ;Work Type Code      ;Code10        ;CaptionML=[ENU=Work Type Code;
                                                              PTB=Cod. Tipo Trabalho] }
    { 9   ;   ;No.                 ;Code20        ;CaptionML=[ENU=No.;
                                                              PTB=N�] }
    { 10  ;   ;Quantity            ;Decimal       ;CaptionML=[ENU=Quantity;
                                                              PTB=Quantidade] }
    { 11  ;   ;Total Cost          ;Decimal       ;CaptionML=[ENU=Total Cost;
                                                              PTB=Custo Total] }
    { 12  ;   ;Line Amount         ;Decimal       ;CaptionML=[ENU=Line Amount;
                                                              PTB=Valor Linha] }
  }
  KEYS
  {
    {    ;Job No.,Job Task No.,Type,Entry type,No.,Location Code,Variant Code,Unit of Measure code,Work Type Code;
                                                   Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

