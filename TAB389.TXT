OBJECT Table 389 Service Contract Dimension
{
  OBJECT-PROPERTIES
  {
    Date=29/04/15;
    Time=12:00:00;
    Modified=Yes;
    Version List=Old Unused Table - marked for deletion.;
  }
  PROPERTIES
  {
  }
  FIELDS
  {
    { 1   ;   ;Table ID            ;Integer       ;TableRelation=AllObj."Object ID" WHERE (Object Type=CONST(Table));
                                                   NotBlank=Yes }
    { 2   ;   ;Type                ;Option        ;OptionString=Quote,Contract }
    { 3   ;   ;No.                 ;Code20         }
    { 4   ;   ;Line No.            ;Integer        }
    { 5   ;   ;Dimension Code      ;Code20        ;TableRelation=Dimension;
                                                   NotBlank=Yes }
    { 6   ;   ;Dimension Value Code;Code20        ;TableRelation="Dimension Value".Code WHERE (Dimension Code=FIELD(Dimension Code));
                                                   NotBlank=Yes }
  }
  KEYS
  {
    {    ;Table ID,Type,No.,Line No.,Dimension Code;
                                                   Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

