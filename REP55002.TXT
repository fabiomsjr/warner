OBJECT Report 55002 Corrige VAT Entry Migrada
{
  OBJECT-PROPERTIES
  {
    Date=03/10/14;
    Time=08:54:14;
    Version List=;
  }
  PROPERTIES
  {
    ProcessingOnly=Yes;
  }
  DATASET
  {
    { 52006500;;DataItem;VATEntry            ;
               DataItemTable=Table254;
               DataItemTableView=SORTING(Entry No.);
               OnAfterGetRecord=BEGIN
                                  IF (Type = Type::Purchase) THEN BEGIN
                                    IF "WithHolding Tax" THEN BEGIN
                                      "Calculation Type" := "Calculation Type"::Paym;
                                      "Payment - Financial" := "Payment - Financial"::Subtract;
                                      "Payment - Payable Tax" := TRUE;
                                    END;
                                    IF "Post WithHolding Tax" THEN BEGIN
                                      "Calculation Type" := "Calculation Type"::Default;
                                      "Posting - Financial" := "Payment - Financial"::Subtract;
                                      "Posting - Payable Tax" := TRUE;
                                    END;
                                    IF "Tax Include" AND (NOT "Expense/Capitalize") THEN BEGIN
                                      "Calculation Type" := "Calculation Type"::Default;
                                      "Posting - Tax Credit" := TRUE;
                                      "Posting - Item Cost/Price" := "Posting - Item Cost/Price"::Subtract;
                                    END;
                                    IF (NOT "Tax Include") AND (NOT "WithHolding Tax") AND (NOT "Post WithHolding Tax") AND
                                       (NOT "Posting - Payable Tax") AND (NOT "Posting - Expense") AND (NOT "Posting - Tax Credit") AND
                                       (NOT "Payment - Payable Tax") AND (NOT "Payment - Expense") AND (NOT "Payment - Tax Credit") AND
                                       (Amount > 0) AND ("Tax Identification" = "Tax Identification"::IPI)
                                    THEN BEGIN
                                      "Calculation Type" := "Calculation Type"::Default;
                                      "Posting - Tax Credit" := TRUE;
                                      "Posting - Financial" := "Posting - Financial"::Add;
                                    END;
                                  END ELSE BEGIN
                                    IF "WithHolding Tax" THEN BEGIN
                                      "Calculation Type" := "Calculation Type"::Paym;
                                      "Payment - Financial" := "Payment - Financial"::Subtract;
                                      "Payment - Tax Credit" := TRUE;
                                    END;
                                    IF "Post WithHolding Tax" THEN BEGIN
                                      "Calculation Type" := "Calculation Type"::Default;
                                      "Posting - Financial" := "Payment - Financial"::Subtract;
                                      "Posting - Tax Credit" := TRUE;
                                    END;
                                    IF "Tax Include" AND (NOT "Expense/Capitalize") THEN BEGIN
                                      "Calculation Type" := "Calculation Type"::Default;
                                      "Posting - Payable Tax" := TRUE;
                                      "Posting - Expense" := TRUE;
                                    END;
                                    IF (NOT "Tax Include") AND (NOT "WithHolding Tax") AND (NOT "Post WithHolding Tax") AND
                                       (NOT "Posting - Payable Tax") AND (NOT "Posting - Expense") AND (NOT "Posting - Tax Credit") AND
                                       (NOT "Payment - Payable Tax") AND (NOT "Payment - Expense") AND (NOT "Payment - Tax Credit") AND
                                       (Amount > 0) AND ("Tax Identification" = "Tax Identification"::IPI)
                                    THEN BEGIN
                                      "Calculation Type" := "Calculation Type"::Default;
                                      "Posting - Expense" := TRUE;
                                      "Posting - Payable Tax" := TRUE;
                                      "Posting - Item Cost/Price" := "Posting - Item Cost/Price"::Add;
                                      "Posting - Financial" := "Posting - Financial"::Add;
                                    END;
                                  END;

                                  MODIFY;
                                END;

               OnPostDataItem=BEGIN
                                MESSAGE('OK');
                              END;
                               }

  }
  REQUESTPAGE
  {
    PROPERTIES
    {
    }
    CONTROLS
    {
    }
  }
  LABELS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
  RDLDATA
  {
  }
}

