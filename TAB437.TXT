OBJECT Table 437 IC Inbox Purchase Line
{
  OBJECT-PROPERTIES
  {
    Date=09/09/14;
    Time=12:00:00;
    Version List=NAVW18.00.00.41370;
  }
  PROPERTIES
  {
    OnDelete=VAR
               DimMgt@1000 : Codeunit 408;
             BEGIN
               DimMgt.DeleteICDocDim(
                 DATABASE::"IC Inbox Purchase Line","IC Transaction No.","IC Partner Code","Transaction Source","Line No.");
             END;

    CaptionML=[ENU=IC Inbox Purchase Line;
               PTB=IC Caixa Entrada Linha Compra];
  }
  FIELDS
  {
    { 1   ;   ;Document Type       ;Option        ;CaptionML=[ENU=Document Type;
                                                              PTB=Tipo Documento];
                                                   OptionCaptionML=[ENU=Order,Invoice,Credit Memo,Return Order;
                                                                    PTB=Pedido,Nota Fiscal,Nota Cr�dito,Nota Devolu��o];
                                                   OptionString=Order,Invoice,Credit Memo,Return Order;
                                                   Editable=No }
    { 3   ;   ;Document No.        ;Code20        ;CaptionML=[ENU=Document No.;
                                                              PTB=No. Documento];
                                                   Editable=No }
    { 4   ;   ;Line No.            ;Integer       ;CaptionML=[ENU=Line No.;
                                                              PTB=N� Linha];
                                                   Editable=No }
    { 11  ;   ;Description         ;Text50        ;CaptionML=[ENU=Description;
                                                              PTB=Descri��o] }
    { 15  ;   ;Quantity            ;Decimal       ;CaptionML=[ENU=Quantity;
                                                              PTB=Quantidade];
                                                   DecimalPlaces=0:5;
                                                   Editable=No }
    { 22  ;   ;Direct Unit Cost    ;Decimal       ;CaptionML=[ENU=Direct Unit Cost;
                                                              PTB=Custo Unit. Direto];
                                                   Editable=No;
                                                   AutoFormatType=2;
                                                   AutoFormatExpr="Currency Code" }
    { 27  ;   ;Line Discount %     ;Decimal       ;CaptionML=ENU=Line Discount %;
                                                   DecimalPlaces=0:5;
                                                   MinValue=0;
                                                   MaxValue=100 }
    { 28  ;   ;Line Discount Amount;Decimal       ;CaptionML=[ENU=Line Discount Amount;
                                                              PTB=Valor Desconto Linha];
                                                   AutoFormatType=1;
                                                   AutoFormatExpr="Currency Code" }
    { 30  ;   ;Amount Including VAT;Decimal       ;CaptionML=[ENU=Amount Including VAT;
                                                              PTB=Valor Bruto];
                                                   Editable=No;
                                                   AutoFormatType=1;
                                                   AutoFormatExpr="Currency Code" }
    { 45  ;   ;Job No.             ;Code20        ;AccessByPermission=TableData 167=R;
                                                   CaptionML=[ENU=Job No.;
                                                              PTB=N� Projeto];
                                                   Editable=No }
    { 54  ;   ;Indirect Cost %     ;Decimal       ;CaptionML=[ENU=Indirect Cost %;
                                                              PTB=% Custo Indireto];
                                                   Editable=No }
    { 63  ;   ;Receipt No.         ;Code20        ;CaptionML=ENU=Receipt No.;
                                                   Editable=No }
    { 64  ;   ;Receipt Line No.    ;Integer       ;CaptionML=ENU=Receipt Line No.;
                                                   Editable=No }
    { 73  ;   ;Drop Shipment       ;Boolean       ;AccessByPermission=TableData 223=R;
                                                   CaptionML=[ENU=Drop Shipment;
                                                              PTB=Envio Direto];
                                                   Editable=No }
    { 91  ;   ;Currency Code       ;Code10        ;TableRelation=Currency;
                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=Currency Code;
                                                              PTB=Cod. Moeda];
                                                   Editable=No }
    { 99  ;   ;VAT Base Amount     ;Decimal       ;CaptionML=[ENU=VAT Base Amount;
                                                              PTB=Valor Base IPI];
                                                   Editable=No;
                                                   AutoFormatType=1;
                                                   AutoFormatExpr="Currency Code" }
    { 100 ;   ;Unit Cost           ;Decimal       ;CaptionML=[ENU=Unit Cost;
                                                              PTB=Custo Unit�rio];
                                                   Editable=No;
                                                   AutoFormatType=2;
                                                   AutoFormatExpr="Currency Code" }
    { 103 ;   ;Line Amount         ;Decimal       ;CaptionML=[ENU=Line Amount;
                                                              PTB=Valor Linha];
                                                   Editable=No;
                                                   AutoFormatType=1;
                                                   AutoFormatExpr="Currency Code" }
    { 107 ;   ;IC Partner Ref. Type;Option        ;CaptionML=[ENU=IC Partner Ref. Type;
                                                              PTB=IC Tipo Refer�ncia Parceiro];
                                                   OptionCaptionML=[ENU=" ,G/L Account,Item,,,Charge (Item),Cross reference,Common Item No.,Vendor Item No.";
                                                                    PTB=" ,Conta Cont�bil,Item,,,Encargo (Item),Refer�ncia Cruzada,No. de Item Comum,No. Item no Fornecedor"];
                                                   OptionString=[ ,G/L Account,Item,,,Charge (Item),Cross reference,Common Item No.,Vendor Item No.];
                                                   Editable=No }
    { 108 ;   ;IC Partner Reference;Code20        ;TableRelation=IF (IC Partner Ref. Type=CONST(" ")) "Standard Text"
                                                                 ELSE IF (IC Partner Ref. Type=CONST(G/L Account)) "IC G/L Account"
                                                                 ELSE IF (IC Partner Ref. Type=CONST(Item)) Item
                                                                 ELSE IF (IC Partner Ref. Type=CONST("Charge (Item)")) "Item Charge"
                                                                 ELSE IF (IC Partner Ref. Type=CONST(Cross reference)) "Item Cross Reference";
                                                   CaptionML=[ENU=IC Partner Reference;
                                                              PTB=IC Refer�ncia Parceiro] }
    { 125 ;   ;IC Partner Code     ;Code20        ;TableRelation="IC Partner";
                                                   CaptionML=[ENU=IC Partner Code;
                                                              PTB=IC C�digo Parceiro];
                                                   Editable=No }
    { 126 ;   ;IC Transaction No.  ;Integer       ;CaptionML=[ENU=IC Transaction No.;
                                                              PTB=IC No. Transa��o];
                                                   Editable=No }
    { 127 ;   ;Transaction Source  ;Option        ;CaptionML=[ENU=Transaction Source;
                                                              PTB=Fonte Transa��o];
                                                   OptionCaptionML=[ENU=Returned by Partner,Created by Partner;
                                                                    PTB=Retornado pelo Parcero,Criado pelo Parceiro];
                                                   OptionString=Returned by Partner,Created by Partner;
                                                   Editable=No }
    { 128 ;   ;Item Ref.           ;Option        ;CaptionML=[ENU=Item Ref.;
                                                              PTB=Ref. Produto];
                                                   OptionCaptionML=[ENU=Local Item No.,Cross Reference,Vendor Item No.;
                                                                    PTB=No. Produto Local,Refer�ncia Local,No. Produto Fornecedor];
                                                   OptionString=Local Item No.,Cross Reference,Vendor Item No.;
                                                   Editable=No }
    { 5407;   ;Unit of Measure Code;Code10        ;CaptionML=ENU=Unit of Measure Code;
                                                   Editable=No }
    { 5790;   ;Requested Receipt Date;Date        ;AccessByPermission=TableData 99000880=R;
                                                   CaptionML=ENU=Requested Receipt Date;
                                                   Editable=No }
    { 5791;   ;Promised Receipt Date;Date         ;CaptionML=[ENU=Promised Receipt Date;
                                                              PTB=Data Recep��o Requerida] }
    { 6600;   ;Return Shipment No. ;Code20        ;CaptionML=ENU=Return Shipment No.;
                                                   Editable=No }
    { 6601;   ;Return Shipment Line No.;Integer   ;CaptionML=ENU=Return Shipment Line No.;
                                                   Editable=No }
  }
  KEYS
  {
    {    ;IC Transaction No.,IC Partner Code,Transaction Source,Line No.;
                                                   Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    PROCEDURE ShowDimensions@1();
    VAR
      ICDocDim@1001 : Record 442;
      ICDocDimensions@1000 : Page 652;
    BEGIN
      TESTFIELD("IC Transaction No.");
      TESTFIELD("Line No.");
      ICDocDim.SETRANGE("Table ID",DATABASE::"IC Inbox Purchase Line");
      ICDocDim.SETRANGE("Transaction No.","IC Transaction No.");
      ICDocDim.SETRANGE("IC Partner Code","IC Partner Code");
      ICDocDim.SETRANGE("Transaction Source","Transaction Source");
      ICDocDim.SETRANGE("Line No.","Line No.");
      ICDocDimensions.SETTABLEVIEW(ICDocDim);
      ICDocDimensions.RUNMODAL;
    END;

    BEGIN
    END.
  }
}

