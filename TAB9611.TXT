OBJECT Table 9611 XML Schema Restriction
{
  OBJECT-PROPERTIES
  {
    Date=09/09/14;
    Time=12:00:00;
    Version List=NAVW18.00;
  }
  PROPERTIES
  {
    CaptionML=ENU=XML Schema Restriction;
  }
  FIELDS
  {
    { 1   ;   ;XML Schema Code     ;Code20        ;TableRelation="XML Schema Element"."XML Schema Code";
                                                   CaptionML=ENU=XML Schema Code }
    { 2   ;   ;Element ID          ;Integer       ;TableRelation="XML Schema Element".ID WHERE (XML Schema Code=FIELD(XML Schema Code));
                                                   CaptionML=ENU=Element ID }
    { 3   ;   ;ID                  ;Integer       ;CaptionML=ENU=ID }
    { 4   ;   ;Value               ;Text250       ;CaptionML=ENU=Value }
  }
  KEYS
  {
    {    ;XML Schema Code,Element ID,ID           ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

