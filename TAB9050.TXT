OBJECT Table 9050 Warehouse Basic Cue
{
  OBJECT-PROPERTIES
  {
    Date=09/09/14;
    Time=12:00:00;
    Version List=NAVW18.00;
  }
  PROPERTIES
  {
    CaptionML=ENU=Warehouse Basic Cue;
  }
  FIELDS
  {
    { 1   ;   ;Primary Key         ;Code10        ;CaptionML=ENU=Primary Key }
    { 2   ;   ;Released Sales Orders - Today;Integer;
                                                   FieldClass=FlowField;
                                                   CalcFormula=Count("Sales Header" WHERE (Document Type=FILTER(Order),
                                                                                           Status=FILTER(Released),
                                                                                           Shipment Date=FIELD(Date Filter),
                                                                                           Location Code=FIELD(Location Filter)));
                                                   AccessByPermission=TableData 110=R;
                                                   CaptionML=[ENU=Released Sales Orders - Today;
                                                              PTB=Pedidos Venda Liberados - Hoje];
                                                   Editable=No }
    { 3   ;   ;Posted Sales Shipments - Today;Integer;
                                                   FieldClass=FlowField;
                                                   CalcFormula=Count("Sales Shipment Header" WHERE (Posting Date=FIELD(Date Filter2),
                                                                                                    Location Code=FIELD(Location Filter)));
                                                   CaptionML=[ENU=Posted Sales Shipments - Today;
                                                              PTB=Hist�rico Envio Vendas - Hoje];
                                                   Editable=No }
    { 4   ;   ;Expected Purch. Orders - Today;Integer;
                                                   FieldClass=FlowField;
                                                   CalcFormula=Count("Purchase Header" WHERE (Document Type=FILTER(Order),
                                                                                              Status=FILTER(Released),
                                                                                              Expected Receipt Date=FIELD(Date Filter),
                                                                                              Location Code=FIELD(Location Filter)));
                                                   AccessByPermission=TableData 120=R;
                                                   CaptionML=[ENU=Expected Purchase Orders - Today;
                                                              PTB=Pedidos Compra Esperados - Hoje];
                                                   Editable=No }
    { 5   ;   ;Posted Purch. Receipts - Today;Integer;
                                                   FieldClass=FlowField;
                                                   CalcFormula=Count("Purch. Rcpt. Header" WHERE (Posting Date=FIELD(Date Filter2),
                                                                                                  Location Code=FIELD(Location Filter)));
                                                   CaptionML=[ENU=Posted Purchase Receipts - Today;
                                                              PTB=Hist�rico Rec. Compras - Hoje];
                                                   Editable=No }
    { 6   ;   ;Inventory Picks - Today;Integer    ;FieldClass=FlowField;
                                                   CalcFormula=Count("Warehouse Activity Header" WHERE (Type=FILTER(Invt. Pick),
                                                                                                        Shipment Date=FIELD(Date Filter),
                                                                                                        Location Code=FIELD(Location Filter)));
                                                   CaptionML=[ENU=Inventory Picks - Today;
                                                              PTB=Invent�rios Picks - Hoje];
                                                   Editable=No }
    { 7   ;   ;Inventory Put-aways - Today;Integer;FieldClass=FlowField;
                                                   CalcFormula=Count("Warehouse Activity Header" WHERE (Type=FILTER(Invt. Put-away),
                                                                                                        Shipment Date=FIELD(Date Filter),
                                                                                                        Location Code=FIELD(Location Filter)));
                                                   CaptionML=[ENU=Inventory Put-aways - Today;
                                                              PTB=Invent�rio Put-aways - Hoje];
                                                   Editable=No }
    { 20  ;   ;Date Filter         ;Date          ;FieldClass=FlowFilter;
                                                   CaptionML=[ENU=Date Filter;
                                                              PTB=Filtro Data];
                                                   Editable=No }
    { 21  ;   ;Date Filter2        ;Date          ;FieldClass=FlowFilter;
                                                   CaptionML=[ENU=Date Filter2;
                                                              PTB=Filtro Data 2];
                                                   Editable=No }
    { 22  ;   ;Location Filter     ;Code10        ;FieldClass=FlowFilter;
                                                   CaptionML=[ENU=Location Filter;
                                                              PTB=Localiza��o Filtro] }
  }
  KEYS
  {
    {    ;Primary Key                             ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

