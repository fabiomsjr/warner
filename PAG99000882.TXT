OBJECT Page 99000882 Change Status on Prod. Order
{
  OBJECT-PROPERTIES
  {
    Date=05/06/15;
    Time=14:20:18;
    Version List=NAVW18.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Change Status on Prod. Order;
               PTB=Mudan�a Status em Pedido Prod.];
    InsertAllowed=No;
    DeleteAllowed=No;
    ModifyAllowed=No;
    DataCaptionExpr='';
    PageType=ConfirmationDialog;
    InstructionalTextML=[ENU=Do you want to change the status of this production order?;
                         PTB=Voc� quer modificar o status dessa ordem de produ��o?];
    OnInit=BEGIN
             FinishedStatusEditable := TRUE;
             ReleasedStatusEditable := TRUE;
             FirmPlannedStatusEditable := TRUE;
           END;

  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 8   ;1   ;Field     ;
                Name=FirmPlannedStatus;
                CaptionML=[ENU=New Status;
                           PTB=Novo Status];
                OptionCaptionML=PTB=,,Firm Planejada,Liberada,Finished;
                SourceExpr=ProdOrderStatus.Status;
                ValuesAllowed=[Firm Planned;Released;Finished];
                OnValidate=BEGIN
                             CASE ProdOrderStatus.Status OF
                               ProdOrderStatus.Status::Finished:
                                 CheckStatus(FinishedStatusEditable);
                               ProdOrderStatus.Status::Released:
                                 CheckStatus(ReleasedStatusEditable);
                               ProdOrderStatus.Status::"Firm Planned":
                                 CheckStatus(FirmPlannedStatusEditable);
                             END;
                           END;
                            }

    { 6   ;1   ;Field     ;
                CaptionML=[ENU=Posting Date;
                           PTB=Data Registro];
                SourceExpr=PostingDate }

    { 9   ;1   ;Field     ;
                CaptionML=[ENU=Update Unit Cost;
                           PTB=Atualizar Custo Unit�rio];
                SourceExpr=ReqUpdUnitCost }

  }
  CODE
  {
    VAR
      ProdOrderStatus@1000 : Record 5405;
      PostingDate@1001 : Date;
      ReqUpdUnitCost@1002 : Boolean;
      FirmPlannedStatusEditable@19025196 : Boolean INDATASET;
      ReleasedStatusEditable@19039198 : Boolean INDATASET;
      FinishedStatusEditable@19021340 : Boolean INDATASET;
      Text666@19003950 : TextConst 'ENU=%1 is not a valid selection.;PTB=%1 n�o � uma sele��o v�lida.';

    PROCEDURE Set@1(ProdOrder@1000 : Record 5405);
    BEGIN
      IF ProdOrder.Status = ProdOrder.Status::Finished THEN
        ProdOrder.FIELDERROR(Status);
      FirmPlannedStatusEditable := ProdOrder.Status < ProdOrder.Status::"Firm Planned";
      ReleasedStatusEditable := ProdOrder.Status <> ProdOrder.Status::Released;
      FinishedStatusEditable := ProdOrder.Status = ProdOrder.Status::Released;

      IF ProdOrder.Status > ProdOrder.Status::Simulated THEN
        ProdOrderStatus.Status := ProdOrder.Status + 1
      ELSE
        ProdOrderStatus.Status := ProdOrderStatus.Status::"Firm Planned";

      PostingDate := WORKDATE;
    END;

    PROCEDURE ReturnPostingInfo@4(VAR Status@1000 : 'Simulated,Planned,Firm Planned,Released,Finished';VAR PostingDate2@1001 : Date;VAR UpdUnitCost@1002 : Boolean);
    BEGIN
      Status := ProdOrderStatus.Status;
      PostingDate2 := PostingDate;
      UpdUnitCost := ReqUpdUnitCost;
    END;

    PROCEDURE CheckStatus@19071396(StatusEditable@19000001 : Boolean);
    BEGIN
      IF NOT StatusEditable THEN
        ERROR(Text666,ProdOrderStatus.Status);
    END;

    BEGIN
    END.
  }
}

