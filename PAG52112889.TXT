OBJECT Page 52112889 ECF A-Part Related Amounts
{
  OBJECT-PROPERTIES
  {
    Date=25/06/15;
    Time=15:20:56;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=A-Part Related Amounts;
               PTB=Valores Relacionados Parte A];
    SourceTable=Table52112875;
    PageType=List;
  }
  CONTROLS
  {
    { 52006500;0;Container;
                ContainerType=ContentArea }

    { 52006501;1;Group    ;
                Name=Group;
                GroupType=Repeater }

    { 52006502;2;Field    ;
                SourceExpr=Date }

    { 52006503;2;Field    ;
                SourceExpr=Amount }

  }
  CODE
  {

    BEGIN
    END.
  }
}

