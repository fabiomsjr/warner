OBJECT Table 390 Availability at Date
{
  OBJECT-PROPERTIES
  {
    Date=05/11/08;
    Time=12:00:00;
    Version List=NAVW16.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Availability at Date;
               PTB=Disponibilidade � Data];
  }
  FIELDS
  {
    { 1   ;   ;Period Start        ;Date          ;CaptionML=[ENU=Period Start;
                                                              PTB=In�cio Per�odo] }
    { 2   ;   ;Scheduled Receipt   ;Decimal       ;CaptionML=[ENU=Scheduled Receipt;
                                                              PTB=Recep��o Programada];
                                                   DecimalPlaces=0:5 }
    { 3   ;   ;Gross Requirement   ;Decimal       ;CaptionML=[ENU=Gross Requirement;
                                                              PTB=Necessidades Brutas];
                                                   DecimalPlaces=0:5 }
    { 4   ;   ;Period End          ;Date          ;CaptionML=[ENU=Period End;
                                                              PTB=Per�odo Final] }
  }
  KEYS
  {
    {    ;Period Start                            ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

