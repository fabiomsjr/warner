OBJECT Table 35000035 Details Services2
{
  OBJECT-PROPERTIES
  {
    Date=29/02/12;
    Time=18:14:18;
    Version List=NAVBR5.01.0109;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Detalhes Servi�os;
               PTB=Detalhes Servi�os];
    LookupPageID=Page35000035;
    DrillDownPageID=Page35000035;
  }
  FIELDS
  {
    { 1   ;   ;Retention Group Code;Code20        ;TableRelation="Retention Groups.2";
                                                   CaptionML=[ENU=Retention Group Code;
                                                              PTB=C�d. Grupo Reten��o] }
    { 2   ;   ;Retention Code      ;Code20        ;TableRelation="Retention Code.2";
                                                   CaptionML=[ENU=C�digo Reten��o;
                                                              PTB=C�digo Reten��o] }
    { 3   ;   ;Tax Identification  ;Option        ;CaptionML=ENU=Identifica��o do imposto;
                                                   OptionCaptionML=[ENU=" ,IPI,ICMS,PIS,COFINS,II,IRRF,INSS,ISS,CSL Ret.,PIS Ret.,COFINS Ret.,ISS Ret.,INSS Ret.,Dif. Aliq. ICMS";
                                                                    PTB=" ,IPI,ICMS,PIS,COFINS,II,IRRF,INSS,ISS,CSL Ret.,PIS Ret.,COFINS Ret.,ISS Ret.,INSS Ret.,Dif. Aliq. ICMS"];
                                                   OptionString=[ ,IPI,ICMS,PIS,COFINS,II,IRRF,INSS,ISS,CSL Ret.,PIS Ret.,COFINS Ret.,ISS Ret.,INSS Ret.,Dif. Aliq. ICMS];
                                                   Description=NAVBR5.01.0109.01 }
  }
  KEYS
  {
    {    ;Retention Group Code,Retention Code,Tax Identification;
                                                   Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

