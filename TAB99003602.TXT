OBJECT Table 99003602 Property
{
  OBJECT-PROPERTIES
  {
    Date=22/09/04;
    Time=11:35:50;
    Version List=NDT2.00.01;
  }
  PROPERTIES
  {
  }
  FIELDS
  {
    { 1   ;   ;Entry No.           ;Integer        }
    { 2   ;   ;Version No.         ;Integer        }
    { 3   ;   ;Reference No.       ;Integer        }
    { 4   ;   ;Object Type         ;Option        ;OptionString=Table,Form,Report,Dataport,Codeunit,XMLport,MenuSuite }
    { 5   ;   ;Object ID           ;Integer        }
    { 6   ;   ;Property            ;Text30         }
    { 7   ;   ;ShortValue          ;Text50         }
    { 8   ;   ;Line No.            ;Integer        }
    { 9   ;   ;Value               ;Text250        }
    { 10  ;   ;Reference Area      ;Option        ;OptionString=None,ObjectReference,FldCntrl,Key,Variable }
    { 11  ;   ;Property No.        ;Integer        }
    { 12  ;   ;Object Consecutive No.;Integer      }
    { 13  ;   ;Ref.Prop.           ;Boolean        }
  }
  KEYS
  {
    {    ;Entry No.                               ;MaintainSIFTIndex=No;
                                                   Clustered=Yes }
    {    ;Reference No.,Property,Line No.         ;MaintainSIFTIndex=No }
    {    ;Object ID,Object Type,Version No.,Property,Entry No.,ShortValue;
                                                   MaintainSQLIndex=Yes;
                                                   MaintainSIFTIndex=No }
    {    ;Property,Entry No.,Version No.,ShortValue;
                                                   MaintainSIFTIndex=No }
    {    ;Reference No.,Version No.,Property No.  ;MaintainSIFTIndex=No }
    {    ;Reference No.,Entry No.                 ;MaintainSIFTIndex=No }
    {    ;Object ID,Version No.,Object Type,Property,Line No.,Reference Area;
                                                   MaintainSIFTIndex=No }
    {    ;Reference No.,Object ID,Object Type,Version No.,Property,Line No.;
                                                   MaintainSIFTIndex=No }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

