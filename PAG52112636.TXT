OBJECT Page 52112636 NavNFe Justification
{
  OBJECT-PROPERTIES
  {
    Date=01/06/13;
    Time=16:51:30;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Justification;
               PTB=Justificativa];
    PageType=Card;
    ActionList=ACTIONS
    {
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 1102300001;1 ;Action    ;
                      CaptionML=[ENU=OK;
                                 PTB=OK];
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=Confirm;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 CurrPage.CLOSE;
                               END;
                                }
      { 1102300000;1 ;Action    ;
                      CaptionML=[ENU=Cancel;
                                 PTB=Cancelar];
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=Cancel;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 xJust := '';
                                 CurrPage.CLOSE;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1102300004;1;Group   }

    { 1102300002;2;Field  ;
                CaptionML=[ENU=Justification;
                           PTB=Justificativa];
                SourceExpr=xJust }

  }
  CODE
  {
    VAR
      xJust@1102300000 : Text[250];

    PROCEDURE GetJust@1102300008() : Text[250];
    BEGIN
      EXIT(xJust);
    END;

    BEGIN
    {
      --- THBR191 ---
      rafaelr,311011,(00) New justification dialog
    }
    END.
  }
}

