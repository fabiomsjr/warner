OBJECT Page 9093 Vendor Details FactBox
{
  OBJECT-PROPERTIES
  {
    Date=07/09/12;
    Time=12:00:00;
    Version List=NAVW17.00,NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Vendor Details;
               PTB=Detalhes Fornecedor];
    SourceTable=Table23;
    PageType=CardPart;
    ActionList=ACTIONS
    {
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 9       ;1   ;ActionGroup;
                      CaptionML=[ENU=Actions;
                                 PTB=A��es];
                      Image=Action }
      { 21      ;2   ;Action    ;
                      CaptionML=[ENU=Comments;
                                 PTB=Coment�rios];
                      RunObject=Page 124;
                      RunPageLink=Table Name=CONST(Vendor),
                                  No.=FIELD(No.);
                      Image=ViewComments }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 13  ;1   ;Field     ;
                CaptionML=[ENU=Vendor No.;
                           PTB=N� Fornecedor];
                SourceExpr="No.";
                OnDrillDown=BEGIN
                              ShowDetails;
                            END;
                             }

    { 1   ;1   ;Field     ;
                SourceExpr=Name }

    { 3   ;1   ;Field     ;
                SourceExpr="Phone No." }

    { 5   ;1   ;Field     ;
                SourceExpr="E-Mail" }

    { 7   ;1   ;Field     ;
                SourceExpr="Fax No." }

    { 11  ;1   ;Field     ;
                SourceExpr=Contact }

    { 52006500;1;Field    ;
                SourceExpr="C.N.P.J./C.P.F." }

  }
  CODE
  {

    PROCEDURE ShowDetails@1102601000();
    BEGIN
      PAGE.RUN(PAGE::"Vendor Card",Rec);
    END;

    BEGIN
    END.
  }
}

