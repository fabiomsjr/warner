OBJECT Table 99008502 Inbound Purch. Document Header
{
  OBJECT-PROPERTIES
  {
    Date=29/02/12;
    Time=18:14:20;
    Version List=NAVW16.00;
  }
  PROPERTIES
  {
    DataCaptionFields=BizTalk Document Type;
    OnInsert=BEGIN
               InboundPurchaseDocumentHeader.LOCKTABLE;
               InboundPurchaseDocumentHeader.RESET;
               IF InboundPurchaseDocumentHeader.FIND('+') THEN
                 "Inbound Document No." := InboundPurchaseDocumentHeader."Inbound Document No." + 1
               ELSE
                 "Inbound Document No." := 1;
             END;

    OnDelete=BEGIN
               InboundPurchaseDocumentLine.LOCKTABLE;
               InboundPurchaseDocumentLine.SETRANGE("Inbound Document No.","Inbound Document No.");
               InboundPurchaseDocumentLine.DELETEALL(TRUE);
             END;

    CaptionML=[ENU=Inbound Purch. Document Header;
               PTB=Cab. Documento Compra Entrada];
    LookupPageID=Page99008517;
  }
  FIELDS
  {
    { 1   ;   ;Document Type       ;Option        ;CaptionML=[ENU=Document Type;
                                                              PTB=Tipo Documento];
                                                   OptionCaptionML=[ENU=Quote,Order,Invoice,Credit Memo,Blanket Order,Return Order;
                                                                    PTB=Cota��o,Pedido,Nota Fiscal,Nota Cr�dito,Ordem Cobertura,Ordem de Retorno];
                                                   OptionString=Quote,Order,Invoice,Credit Memo,Blanket Order,Return Order }
    { 2   ;   ;Buy-from Vendor No. ;Code20        ;CaptionML=[ENU=Buy-from Vendor No.;
                                                              PTB=Compra a-N� Fornecedor] }
    { 3   ;   ;No.                 ;Code20        ;CaptionML=[ENU=No.;
                                                              PTB=N�] }
    { 4   ;   ;Pay-to Vendor No.   ;Code20        ;CaptionML=[ENU=Pay-to Vendor No.;
                                                              PTB=Pagto.-a N� Forn.];
                                                   NotBlank=Yes }
    { 5   ;   ;Pay-to Name         ;Text50        ;CaptionML=[ENU=Pay-to Name;
                                                              PTB=Pagto.-a Nome] }
    { 6   ;   ;Pay-to Name 2       ;Text50        ;CaptionML=[ENU=Pay-to Name 2;
                                                              PTB=Pagto.-a Nome 2] }
    { 7   ;   ;Pay-to Address      ;Text50        ;CaptionML=[ENU=Pay-to Address;
                                                              PTB=Pagto-a Endere�o] }
    { 8   ;   ;Pay-to Address 2    ;Text50        ;CaptionML=[ENU=Pay-to Address 2;
                                                              PTB=Pagto.-a Endere�o Complementar] }
    { 9   ;   ;Pay-to City         ;Text30        ;CaptionML=[ENU=Pay-to City;
                                                              PTB=Pagto.-a Cidade] }
    { 10  ;   ;Pay-to Contact      ;Text50        ;CaptionML=[ENU=Pay-to Contact;
                                                              PTB=Pagto.-a Contato] }
    { 11  ;   ;Your Reference      ;Text30        ;CaptionML=[ENU=Your Reference;
                                                              PTB=Sua Refer�ncia] }
    { 12  ;   ;Ship-to Code        ;Code10        ;CaptionML=[ENU=Ship-to Code;
                                                              PTB=Cod. Endere�o Envio] }
    { 13  ;   ;Ship-to Name        ;Text50        ;CaptionML=[ENU=Ship-to Name;
                                                              PTB=Envio-a Nome] }
    { 14  ;   ;Ship-to Name 2      ;Text50        ;CaptionML=[ENU=Ship-to Name 2;
                                                              PTB=Envio-a Nome 2] }
    { 15  ;   ;Ship-to Address     ;Text50        ;CaptionML=[ENU=Ship-to Address;
                                                              PTB=Envio-a Endere�o] }
    { 16  ;   ;Ship-to Address 2   ;Text50        ;CaptionML=[ENU=Ship-to Address 2;
                                                              PTB=Envio-a Endere�o Complemantar] }
    { 17  ;   ;Ship-to City        ;Text30        ;CaptionML=[ENU=Ship-to City;
                                                              PTB=Envio-a Cidade] }
    { 18  ;   ;Ship-to Contact     ;Text50        ;CaptionML=[ENU=Ship-to Contact;
                                                              PTB=Envio-a Contato] }
    { 19  ;   ;Order Date          ;Date          ;CaptionML=[ENU=Order Date;
                                                              PTB=Data Pedido] }
    { 20  ;   ;Posting Date        ;Date          ;CaptionML=[ENU=Posting Date;
                                                              PTB=Data Registo] }
    { 21  ;   ;Expected Receipt Date;Date         ;CaptionML=[ENU=Expected Receipt Date;
                                                              PTB=Data Recep��o Esperada] }
    { 22  ;   ;Posting Description ;Text50        ;CaptionML=[ENU=Posting Description;
                                                              PTB=Texto Registro] }
    { 23  ;   ;Payment Terms Code  ;Code10        ;CaptionML=[ENU=Payment Terms Code;
                                                              PTB=Cod. Termos Pagto.] }
    { 24  ;   ;Due Date            ;Date          ;CaptionML=[ENU=Due Date;
                                                              PTB=Data Vencimento] }
    { 25  ;   ;Payment Discount %  ;Decimal       ;CaptionML=[ENU=Payment Discount %;
                                                              PTB=% Desconto P.P.];
                                                   DecimalPlaces=0:5 }
    { 26  ;   ;Pmt. Discount Date  ;Date          ;CaptionML=[ENU=Pmt. Discount Date;
                                                              PTB=Data Desconto Pagamento] }
    { 27  ;   ;Shipment Method Code;Code10        ;CaptionML=[ENU=Shipment Method Code;
                                                              PTB=Cod. Condi��es Envio] }
    { 28  ;   ;Location Code       ;Code10        ;CaptionML=[ENU=Location Code;
                                                              PTB=Cod. Dep�sito] }
    { 29  ;   ;Shortcut Dimension 1 Code;Code20   ;TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(1));
                                                   CaptionML=[ENU=Shortcut Dimension 1 Code;
                                                              PTB=Cod. Atalho Dimens�o 1];
                                                   CaptionClass='1,2,1' }
    { 30  ;   ;Shortcut Dimension 2 Code;Code20   ;TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(2));
                                                   CaptionML=[ENU=Shortcut Dimension 2 Code;
                                                              PTB=Cod. Atalho Dimens�o 2];
                                                   CaptionClass='1,2,2' }
    { 31  ;   ;Vendor Posting Group;Code10        ;CaptionML=[ENU=Vendor Posting Group;
                                                              PTB=Gr. Cont�bil Fornecedor];
                                                   Editable=Yes }
    { 32  ;   ;Currency Code       ;Code10        ;CaptionML=[ENU=Currency Code;
                                                              PTB=Cod. Moeda] }
    { 33  ;   ;Currency Factor     ;Decimal       ;CaptionML=[ENU=Currency Factor;
                                                              PTB=Fator Moeda];
                                                   DecimalPlaces=0:15;
                                                   MinValue=0;
                                                   Editable=Yes }
    { 37  ;   ;Invoice Disc. Code  ;Code20        ;CaptionML=[ENU=Invoice Disc. Code;
                                                              PTB=Cod. Desconto N.Fiscal] }
    { 41  ;   ;Language Code       ;Code10        ;CaptionML=[ENU=Language Code;
                                                              PTB=Cod. Idioma] }
    { 43  ;   ;Purchaser Code      ;Code10        ;CaptionML=[ENU=Purchaser Code;
                                                              PTB=Cod. Comprador] }
    { 45  ;   ;Order Class         ;Code10        ;CaptionML=[ENU=Order Class;
                                                              PTB=Classe Pedido] }
    { 46  ;   ;Comment             ;Boolean       ;FieldClass=Normal;
                                                   CaptionML=[ENU=Comment;
                                                              PTB=Coment�rio];
                                                   Editable=Yes }
    { 47  ;   ;No. Printed         ;Integer       ;CaptionML=[ENU=No. Printed;
                                                              PTB=N� Impress�es];
                                                   Editable=Yes }
    { 51  ;   ;On Hold             ;Code3         ;CaptionML=[ENU=On Hold;
                                                              PTB=Retido] }
    { 52  ;   ;Applies-to Doc. Type;Option        ;CaptionML=[ENU=Applies-to Doc. Type;
                                                              PTB=Aplicado por Tipo Doc.];
                                                   OptionCaptionML=[ENU=" ,Payment,Invoice,Credit Memo,Finance Charge Memo,Reminder,Refund";
                                                                    PTB=" ,Pagamento,Nota Fiscal,Nota de Cr�dito,Nota de Encargo Financeiro,Lembrete,Reembolso"];
                                                   OptionString=[ ,Payment,Invoice,Credit Memo,Finance Charge Memo,Reminder,Refund] }
    { 53  ;   ;Applies-to Doc. No. ;Code20        ;CaptionML=[ENU=Applies-to Doc. No.;
                                                              PTB=Aplicado por N� Doc.] }
    { 55  ;   ;Bal. Account No.    ;Code20        ;CaptionML=[ENU=Bal. Account No.;
                                                              PTB=N� Conta Contrap.] }
    { 56  ;   ;Job No.             ;Code20        ;CaptionML=[ENU=Job No.;
                                                              PTB=N� Projeto] }
    { 57  ;   ;Receive             ;Boolean       ;CaptionML=[ENU=Receive;
                                                              PTB=Receber] }
    { 58  ;   ;Invoice             ;Boolean       ;CaptionML=[ENU=Invoice;
                                                              PTB=Proforma Invoice] }
    { 60  ;   ;Amount              ;Decimal       ;FieldClass=Normal;
                                                   CaptionML=[ENU=Amount;
                                                              PTB=Valor];
                                                   Editable=Yes;
                                                   AutoFormatType=1 }
    { 61  ;   ;Amount Including VAT;Decimal       ;CaptionML=[ENU=Amount Including VAT;
                                                              PTB=Valor Bruto];
                                                   Editable=Yes;
                                                   AutoFormatType=1 }
    { 62  ;   ;Receiving No.       ;Code20        ;CaptionML=[ENU=Receiving No.;
                                                              PTB=N� Remessa Compra] }
    { 63  ;   ;Posting No.         ;Code20        ;CaptionML=[ENU=Posting No.;
                                                              PTB=N� Registro] }
    { 64  ;   ;Last Receiving No.  ;Code20        ;CaptionML=[ENU=Last Receiving No.;
                                                              PTB=�ltimo N� Remessa Compra];
                                                   Editable=Yes }
    { 65  ;   ;Last Posting No.    ;Code20        ;CaptionML=[ENU=Last Posting No.;
                                                              PTB=�ltimo N� N.Fiscal];
                                                   Editable=Yes }
    { 66  ;   ;Vendor Order No.    ;Code20        ;CaptionML=[ENU=Vendor Order No.;
                                                              PTB=N� Ped. Fornecedor] }
    { 67  ;   ;Vendor Shipment No. ;Code20        ;CaptionML=[ENU=Vendor Shipment No.;
                                                              PTB=N� Remessa Forn.] }
    { 68  ;   ;Vendor Invoice No.  ;Code20        ;CaptionML=[ENU=Vendor Invoice No.;
                                                              PTB=N� N.Fiscal Fornecedor] }
    { 69  ;   ;Vendor Cr. Memo No. ;Code20        ;CaptionML=[ENU=Vendor Cr. Memo No.;
                                                              PTB=N� Nota Cr�dito Forn.] }
    { 70  ;   ;VAT Registration No.;Text20        ;CaptionML=[ENU=VAT Registration No.;
                                                              PTB=N� Contribuinte] }
    { 72  ;   ;Sell-to Customer No.;Code20        ;CaptionML=[ENU=Sell-to Customer No.;
                                                              PTB=Venda-a N� Cliente] }
    { 73  ;   ;Reason Code         ;Code10        ;CaptionML=[ENU=Reason Code;
                                                              PTB=Cod. Raz�o] }
    { 74  ;   ;Gen. Bus. Posting Group;Code10     ;CaptionML=[ENU=Gen. Bus. Posting Group;
                                                              PTB=Gr. Cont�bil Neg�cio] }
    { 76  ;   ;Transaction Type    ;Code10        ;CaptionML=[ENU=Transaction Type;
                                                              PTB=Natureza Transa��o] }
    { 77  ;   ;Transport Method    ;Code10        ;CaptionML=[ENU=Transport Method;
                                                              PTB=Modo Transporte] }
    { 78  ;   ;VAT Country/Region Code;Code10     ;CaptionML=[ENU=VAT Country/Region Code;
                                                              PTB=C�digo Imposto Pa�s/Regi�o] }
    { 79  ;   ;Buy-from Vendor Name;Text50        ;CaptionML=[ENU=Buy-from Vendor Name;
                                                              PTB=Compra-a Nome Fornecedor] }
    { 80  ;   ;Buy-from Vendor Name 2;Text50      ;CaptionML=[ENU=Buy-from Vendor Name 2;
                                                              PTB=Compra-a Nome Fornecedor 2] }
    { 81  ;   ;Buy-from Address    ;Text50        ;CaptionML=[ENU=Buy-from Address;
                                                              PTB=Compra-a Endere�o] }
    { 82  ;   ;Buy-from Address 2  ;Text50        ;CaptionML=[ENU=Buy-from Address 2;
                                                              PTB=Compra a-Endere�o Complementar] }
    { 83  ;   ;Buy-from City       ;Text30        ;CaptionML=[ENU=Buy-from City;
                                                              PTB=Compra-a Cidade] }
    { 84  ;   ;Buy-from Contact    ;Text50        ;CaptionML=[ENU=Buy-from Contact;
                                                              PTB=Compra-a Contato] }
    { 85  ;   ;Pay-to Post Code    ;Code20        ;ValidateTableRelation=No;
                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=Pay-to Post Code;
                                                              PTB=Pagamento-a CEP] }
    { 86  ;   ;Pay-to County       ;Text30        ;CaptionML=[ENU=Pay-to County;
                                                              PTB=Pagto. a-Distrito] }
    { 87  ;   ;Pay-to Country/Region Code;Code10  ;CaptionML=[ENU=Pay-to Country/Region Code;
                                                              PTB=Pagar a C�d. Pa�s/Regi�o] }
    { 88  ;   ;Buy-from Post Code  ;Code20        ;ValidateTableRelation=No;
                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=Buy-from Post Code;
                                                              PTB=Compra-a CEP] }
    { 89  ;   ;Buy-from County     ;Text30        ;CaptionML=[ENU=Buy-from County;
                                                              PTB=Compra-a Distrito] }
    { 90  ;   ;Buy-from Country/Region Code;Code10;CaptionML=[ENU=Buy-from Country/Region Code;
                                                              PTB=Comprar de C�d. Pa�s/Regi�o] }
    { 91  ;   ;Ship-to Post Code   ;Code20        ;ValidateTableRelation=No;
                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=Ship-to Post Code;
                                                              PTB=Envio-a CEP] }
    { 92  ;   ;Ship-to County      ;Text30        ;CaptionML=[ENU=Ship-to County;
                                                              PTB=Envio-a Distrito] }
    { 93  ;   ;Ship-to Country/Region Code;Code10 ;CaptionML=[ENU=Ship-to Country/Region Code;
                                                              PTB=C�d. Enviar - para Pa�s/Regi�o] }
    { 94  ;   ;Bal. Account Type   ;Option        ;CaptionML=[ENU=Bal. Account Type;
                                                              PTB=Tipo Contrapartida];
                                                   OptionCaptionML=[ENU=G/L Account,Bank Account;
                                                                    PTB=Conta,Banco];
                                                   OptionString=G/L Account,Bank Account }
    { 95  ;   ;Order Address Code  ;Code10        ;CaptionML=[ENU=Order Address Code;
                                                              PTB=Cod. Endere�o Ped. Forn.] }
    { 97  ;   ;Entry Point         ;Code10        ;CaptionML=[ENU=Entry Point;
                                                              PTB=Porto/Aerop. Entrada] }
    { 98  ;   ;Correction          ;Boolean       ;CaptionML=[ENU=Correction;
                                                              PTB=Corre��o] }
    { 99  ;   ;Document Date       ;Date          ;CaptionML=[ENU=Document Date;
                                                              PTB=Data Documento] }
    { 101 ;   ;Area                ;Code10        ;CaptionML=[ENU=Area;
                                                              PTB=�rea] }
    { 102 ;   ;Transaction Specification;Code10   ;CaptionML=[ENU=Transaction Specification;
                                                              PTB=Especifica�ao Transa��o] }
    { 104 ;   ;Payment Method Code ;Code10        ;CaptionML=[ENU=Payment Method Code;
                                                              PTB=Cod. Forma Pagamento] }
    { 107 ;   ;No. Series          ;Code10        ;CaptionML=[ENU=No. Series;
                                                              PTB=N� S�ries];
                                                   Editable=Yes }
    { 108 ;   ;Posting No. Series  ;Code10        ;CaptionML=[ENU=Posting No. Series;
                                                              PTB=N� S�ries Registro] }
    { 109 ;   ;Receiving No. Series;Code10        ;CaptionML=[ENU=Receiving No. Series;
                                                              PTB=N� S�rie Remessa Compra] }
    { 114 ;   ;Tax Area Code       ;Code20        ;CaptionML=[ENU=Tax Area Code;
                                                              PTB=C�d. �rea Imposto] }
    { 115 ;   ;Tax Liable          ;Boolean       ;CaptionML=[ENU=Tax Liable;
                                                              PTB=Sujeito a Imposto] }
    { 116 ;   ;VAT Bus. Posting Group;Code10      ;CaptionML=[ENU=VAT Bus. Posting Group;
                                                              PTB=Gr. Registro Imp. Neg�cio] }
    { 117 ;   ;Reserve             ;Option        ;CaptionML=[ENU=Reserve;
                                                              PTB=Reserva];
                                                   OptionCaptionML=[ENU=Never,Optional,Always;
                                                                    PTB=Nunca,Opcional,Sempre];
                                                   OptionString=Never,Optional,Always }
    { 118 ;   ;Applies-to ID       ;Code20        ;CaptionML=[ENU=Applies-to ID;
                                                              PTB=Aplicado por ID.] }
    { 119 ;   ;VAT Base Discount % ;Decimal       ;CaptionML=[ENU=VAT Base Discount %;
                                                              PTB=% Desc. Base IPI];
                                                   DecimalPlaces=0:5;
                                                   MinValue=0;
                                                   MaxValue=100 }
    { 99008500;;Inbound Document No.;Integer      ;CaptionML=[ENU=Inbound Document No.;
                                                              PTB=N� Documento Entrada] }
    { 99008501;;Date Received      ;Date          ;CaptionML=[ENU=Date Received;
                                                              PTB=Data recep��o] }
    { 99008502;;Time Received      ;Time          ;CaptionML=[ENU=Time Received;
                                                              PTB=Hora Recep��o] }
    { 99008503;;Date Processed     ;Date          ;CaptionML=[ENU=Date Processed;
                                                              PTB=Data Processamento] }
    { 99008504;;Time Processed     ;Time          ;CaptionML=[ENU=Time Processed;
                                                              PTB=Hora Processamento] }
    { 99008505;;Status             ;Option        ;CaptionML=[ENU=Status;
                                                              PTB=Status];
                                                   OptionCaptionML=[ENU=Pending,Accepted,Rejected,Processing;
                                                                    PTB=Pendente,Aceite,Rejeitado,Processamento];
                                                   OptionString=Pending,Accepted,Rejected,Processing }
    { 99008506;;Tracking ID        ;Text50        ;CaptionML=[ENU=Tracking ID;
                                                              PTB=ID Rastreabilidade] }
    { 99008507;;Auto. Accept Failed;Boolean       ;CaptionML=[ENU=Auto. Accept Failed;
                                                              PTB=Aceita��o Auto. Falhada] }
    { 99008508;;BizTalk Document Type;Option      ;CaptionML=[ENU=BizTalk Document Type;
                                                              PTB=Tipo Documento BizTalk];
                                                   OptionCaptionML=[ENU=Quote,Order Confirmation,Invoice,Receipt,Credit Memo;
                                                                    PTB=Cota��o,Confirma��o de Pedido,Nota Fiscal,Recebimento,Nota de Cr�dito];
                                                   OptionString=Quote,Order Confirmation,Invoice,Receipt,Credit Memo }
    { 99008509;;Reference Quote No.;Code20        ;CaptionML=[ENU=Reference Quote No.;
                                                              PTB=No Cota�. Refer�ncia] }
    { 99008510;;Reference Order No.;Code20        ;CaptionML=[ENU=Reference Order No.;
                                                              PTB=N� Ped. Refer�ncia] }
    { 99008511;;Vendor Quote No.   ;Code20        ;CaptionML=[ENU=Vendor Quote No.;
                                                              PTB=N� Cota��o Fornecedor] }
    { 99008512;;Corresp. Accepted Document No.;Code20;
                                                   CaptionML=[ENU=Corresp. Accepted Document No.;
                                                              PTB=N� Documento Aceite Corresp.] }
    { 99008513;;Doc. Transformation Succeeded;Boolean;
                                                   CaptionML=[ENU=Doc. Transformation Succeeded;
                                                              PTB=Transforma��o Doc. c/ Sucesso] }
    { 99008514;;Document Selected  ;Boolean       ;CaptionML=[ENU=Document Selected;
                                                              PTB=Documento Selecionado] }
  }
  KEYS
  {
    {    ;Inbound Document No.                    ;Clustered=Yes }
    {    ;Status                                   }
    {    ;Tracking ID                              }
  }
  FIELDGROUPS
  {
    { 1   ;DropDown            ;Inbound Document No.,Document Type,Buy-from Vendor No.,Status }
  }
  CODE
  {
    VAR
      Text000@1001 : TextConst 'ENU=The BizTalk purchase document number %1 has been rejected.;PTB=O documento compra BizTalk n�mero %1 foi rejeitado.';
      Text001@1002 : TextConst 'ENU=The information in the document has been rejected and may no longer be valid.;PTB=O informa��o no documento foi rejeiitada e n�o � mais v�lida.';
      Text002@1003 : TextConst 'ENU=Do you want to continue?;PTB=Deseja continuar?';
      InboundPurchaseDocumentHeader@1004 : Record 99008502;
      InboundPurchaseDocumentLine@1000 : Record 99008503;

    PROCEDURE RejectPurchaseDocument@1();
    BEGIN
      Status := Status::Rejected;
      IF MODIFY THEN
        MESSAGE(Text000,"Inbound Document No.");
    END;

    PROCEDURE AcceptPurchaseDocument@2();
    BEGIN
      IF Status = Status::Rejected THEN
        IF NOT CONFIRM(Text001 +
                       Text002,TRUE) THEN
          EXIT;
      CASE "BizTalk Document Type" OF
        "BizTalk Document Type"::Quote:
          CODEUNIT.RUN(99008501,Rec);
        "BizTalk Document Type"::"Order Confirmation":
          CODEUNIT.RUN(99008503,Rec);
        "BizTalk Document Type"::Invoice:
          CODEUNIT.RUN(99008504,Rec);
        "BizTalk Document Type"::Receipt:
          CODEUNIT.RUN(99008505,Rec);
        "BizTalk Document Type"::"Credit Memo":
          CODEUNIT.RUN(99008506,Rec);
      END;
    END;

    BEGIN
    END.
  }
}

