OBJECT Table 423 IC Inbox/Outbox Jnl. Line Dim.
{
  OBJECT-PROPERTIES
  {
    Date=07/09/12;
    Time=12:00:00;
    Version List=NAVW17.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=IC Inbox/Outbox Jnl. Line Dim.;
               PTB=IC Caixa Entrada/Sa�da Linha Di�rio Dim.];
  }
  FIELDS
  {
    { 1   ;   ;Table ID            ;Integer       ;CaptionML=[ENU=Table ID;
                                                              PTB=N� Tabela] }
    { 2   ;   ;IC Partner Code     ;Code20        ;TableRelation="IC Partner";
                                                   CaptionML=[ENU=IC Partner Code;
                                                              PTB=IC C�digo Parceiro] }
    { 3   ;   ;Transaction No.     ;Integer       ;CaptionML=[ENU=Transaction No.;
                                                              PTB=N� Lan�amento] }
    { 4   ;   ;Line No.            ;Integer       ;CaptionML=[ENU=Line No.;
                                                              PTB=N� Linha] }
    { 5   ;   ;Dimension Code      ;Code20        ;TableRelation="IC Dimension";
                                                   OnValidate=BEGIN
                                                                IF NOT DimMgt.CheckICDim("Dimension Code") THEN
                                                                  ERROR(DimMgt.GetDimErr);
                                                                "Dimension Value Code" := '';
                                                              END;

                                                   CaptionML=[ENU=Dimension Code;
                                                              PTB=C�digo Dimens�o] }
    { 6   ;   ;Dimension Value Code;Code20        ;TableRelation="IC Dimension Value".Code WHERE (Dimension Code=FIELD(Dimension Code));
                                                   OnValidate=BEGIN
                                                                IF NOT DimMgt.CheckICDimValue("Dimension Code","Dimension Value Code") THEN
                                                                  ERROR(DimMgt.GetDimErr);
                                                              END;

                                                   CaptionML=[ENU=Dimension Value Code;
                                                              PTB=Cod. Valor Dimens�o] }
    { 7   ;   ;Transaction Source  ;Option        ;CaptionML=[ENU=Transaction Source;
                                                              PTB=Fonte Transa��o];
                                                   OptionCaptionML=[ENU=Rejected,Created;
                                                                    PTB=Rejeitado,Criado];
                                                   OptionString=Rejected,Created }
  }
  KEYS
  {
    {    ;Table ID,Transaction No.,IC Partner Code,Transaction Source,Line No.,Dimension Code;
                                                   Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      DimMgt@1000 : Codeunit 408;

    BEGIN
    END.
  }
}

