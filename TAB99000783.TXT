OBJECT Table 99000783 Standard Task Description
{
  OBJECT-PROPERTIES
  {
    Date=05/11/08;
    Time=12:00:00;
    Version List=NAVW16.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Standard Task Description;
               PTB=Descri��o de Tarefa Padr�o];
  }
  FIELDS
  {
    { 1   ;   ;Standard Task Code  ;Code10        ;TableRelation="Standard Task";
                                                   CaptionML=[ENU=Standard Task Code;
                                                              PTB=Cod. Tarefa Padr�o];
                                                   NotBlank=Yes }
    { 2   ;   ;Line No.            ;Integer       ;CaptionML=[ENU=Line No.;
                                                              PTB=N� Linha] }
    { 10  ;   ;Text                ;Text50        ;CaptionML=[ENU=Text;
                                                              PTB=Texto] }
  }
  KEYS
  {
    {    ;Standard Task Code,Line No.             ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

