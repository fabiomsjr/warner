OBJECT Table 5973 Service Contract Account Group
{
  OBJECT-PROPERTIES
  {
    Date=07/09/12;
    Time=12:00:00;
    Version List=NAVW17.00,NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Service Contract Account Group;
               PTB=Grupo Conta Contrato Servi�o];
    LookupPageID=Page6070;
    DrillDownPageID=Page6070;
  }
  FIELDS
  {
    { 1   ;   ;Code                ;Code10        ;CaptionML=[ENU=Code;
                                                              PTB=C�digo] }
    { 2   ;   ;Description         ;Text50        ;CaptionML=[ENU=Description;
                                                              PTB=Descri��o] }
    { 3   ;   ;Non-Prepaid Contract Acc.;Code20   ;TableRelation=Item WHERE (Service=CONST(Yes));
                                                   OnValidate=VAR
                                                                ">NAVBR"@52006501 : Integer;
                                                                item@52006500 : Record 27;
                                                              BEGIN
                                                                // > NAVBR
                                                                IF "Non-Prepaid Contract Acc." <> '' THEN BEGIN
                                                                  item.GET("Non-Prepaid Contract Acc.");
                                                                  item.TESTFIELD("Gen. Prod. Posting Group");
                                                                  item.TESTFIELD("Sales Unit of Measure");
                                                                END;
                                                                EXIT;
                                                                // < NAVBR
                                                                IF "Non-Prepaid Contract Acc." <> '' THEN BEGIN
                                                                  GlAcc.GET("Non-Prepaid Contract Acc.");
                                                                  GlAcc.TESTFIELD("Gen. Prod. Posting Group");
                                                                  GlAcc.TESTFIELD("VAT Prod. Posting Group");
                                                                END;
                                                              END;

                                                   CaptionML=[ENU=Non-Prepaid Contract Acc.;
                                                              PTB=Conta Contrato N�o Pr�-Pago] }
    { 4   ;   ;Prepaid Contract Acc.;Code20       ;TableRelation=Item WHERE (Service=CONST(Yes));
                                                   OnValidate=VAR
                                                                ">NAVBR"@52006500 : Integer;
                                                                item@52006501 : Record 27;
                                                              BEGIN
                                                                // > NAVBR
                                                                IF "Prepaid Contract Acc." <> '' THEN BEGIN
                                                                  item.GET("Prepaid Contract Acc.");
                                                                  item.TESTFIELD("Gen. Prod. Posting Group");
                                                                  item.TESTFIELD("Sales Unit of Measure");
                                                                END;
                                                                EXIT;
                                                                // < NAVBR
                                                                IF "Prepaid Contract Acc." <> '' THEN BEGIN
                                                                  GlAcc.GET("Prepaid Contract Acc.");
                                                                  GlAcc.TESTFIELD("Gen. Prod. Posting Group");
                                                                  GlAcc.TESTFIELD("VAT Prod. Posting Group");
                                                                END;
                                                              END;

                                                   CaptionML=[ENU=Prepaid Contract Acc.;
                                                              PTB=Conta Contrato Pr�-Pago] }
    { 52112430;;Service Code       ;Code20        ;TableRelation=Item WHERE (Service=CONST(Yes));
                                                   CaptionML=[ENU=Service Code;
                                                              PTB=C�digo Servi�o] }
  }
  KEYS
  {
    {    ;Code                                    ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      GlAcc@1000 : Record 15;

    BEGIN
    END.
  }
}

