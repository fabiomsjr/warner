OBJECT Table 319 Tax Area Line
{
  OBJECT-PROPERTIES
  {
    Date=22/10/15;
    Time=14:19:52;
    Version List=NAVW17.00,NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Tax Area Line;
               PTB=Linha �rea Impostos];
  }
  FIELDS
  {
    { 1   ;   ;Tax Area            ;Code20        ;TableRelation="Tax Area";
                                                   CaptionML=[ENU=Tax Area;
                                                              PTB=�rea Impostos] }
    { 2   ;   ;Tax Jurisdiction Code;Code20       ;TableRelation="Tax Jurisdiction";
                                                   OnValidate=VAR
                                                                ">NAVBR"@52006501 : Integer;
                                                                taxJurisdiction@52006500 : Record 320;
                                                              BEGIN
                                                                // > NAVBR
                                                                IF "Tax Jurisdiction Code" <> '' THEN BEGIN
                                                                  taxJurisdiction.GET("Tax Jurisdiction Code");
                                                                  "Tax Posting Code" := taxJurisdiction."Default Tax Posting Code";
                                                                END;
                                                                // < NAVBR
                                                              END;

                                                   CaptionML=[ENU=Tax Jurisdiction Code;
                                                              PTB=Cod. Jurisdi��o Imposto];
                                                   NotBlank=Yes }
    { 3   ;   ;Jurisdiction Description;Text50    ;FieldClass=FlowField;
                                                   CalcFormula=Lookup("Tax Jurisdiction".Description WHERE (Code=FIELD(Tax Jurisdiction Code)));
                                                   CaptionML=[ENU=Jurisdiction Description;
                                                              PTB=Descri��o Jurisdi��o];
                                                   Editable=No }
    { 4   ;   ;Calculation Order   ;Integer       ;CaptionML=[ENU=Calculation Order;
                                                              PTB=Ordem c�lculo];
                                                   BlankNumbers=BlankZero }
    { 52112430;;Tax Identification ;Option        ;FieldClass=FlowField;
                                                   CalcFormula=Lookup("Tax Jurisdiction"."Tax Identification" WHERE (Code=FIELD(Tax Jurisdiction Code)));
                                                   CaptionML=[ENU=Classification;
                                                              PTB=Classifica��o];
                                                   OptionCaptionML=[ENU=" ,IPI,ICMS,PIS,COFINS,II,IRRF,INSS,ISS,CSL Ret.,PIS Ret.,COFINS Ret.,ISS Ret.,INSS Ret.,Dif. Aliq. ICMS,ST,PCC,CIDE,Funrural,Simples";
                                                                    PTB=" ,IPI,ICMS,PIS,COFINS,II,IRRF,INSS,ISS,CSL Ret.,PIS Ret.,COFINS Ret.,ISS Ret.,INSS Ret.,Dif. Aliq. ICMS,ST,PCC,CIDE,Funrural,Simples"];
                                                   OptionString=[ ,IPI,ICMS,PIS,COFINS,II,IRRF,INSS,ISS,CSL Ret.,PIS Ret.,COFINS Ret.,ISS Ret.,INSS Ret.,Dif. Aliq. ICMS,ST,PCC,CIDE,Funrural,Simples];
                                                   Editable=No }
    { 52112440;;Branch Code        ;Code20        ;TableRelation="Branch Information";
                                                   CaptionML=[ENU=Branch Code;
                                                              PTB=C�d. Filial] }
    { 52112450;;Tax Posting Code   ;Code20        ;TableRelation="Tax Posting";
                                                   CaptionML=[ENU=Tax Posting Code;
                                                              PTB=C�d. Contabiliza��o] }
    { 52112460;;CST Code           ;Code10        ;TableRelation=IF (Tax Identification=FILTER(ICMS)) "CST Code".Code
                                                                 ELSE IF (Tax Identification=CONST(IPI)) "CST Impostos".Code WHERE (Tax Type=CONST(IPI))
                                                                 ELSE IF (Tax Identification=CONST(PIS)) "CST Impostos".Code WHERE (Tax Type=CONST(PIS))
                                                                 ELSE IF (Tax Identification=CONST(COFINS)) "CST Impostos".Code WHERE (Tax Type=CONST(COFINS));
                                                   CaptionML=[ENU=CST Code;
                                                              PTB=C�d. CST] }
    { 52112465;;Tax Statute Code   ;Code3         ;TableRelation="Tax Statute Code".Code WHERE (Tax Identification=FIELD(Tax Identification));
                                                   CaptionML=[ENU=Tax Statute Code;
                                                              PTB=C�d. Enquadramento Legal] }
  }
  KEYS
  {
    {    ;Tax Area,Tax Jurisdiction Code,Branch Code;
                                                   Clustered=Yes }
    {    ;Tax Jurisdiction Code                    }
    {    ;Tax Area,Calculation Order               }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

