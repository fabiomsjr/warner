OBJECT Table 52112835 GL Acc Resp Institute.
{
  OBJECT-PROPERTIES
  {
    Date=30/04/13;
    Time=18:14:18;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=GL Acc Resp Institute;
               PTB=Inst. Respons. Plano de Contas];
    LookupPageID=Page52112845;
    DrillDownPageID=Page52112845;
  }
  FIELDS
  {
    { 1   ;   ;No                  ;Code20        ;CaptionML=[ENU=No;
                                                              PTB=N�] }
    { 2   ;   ;Description         ;Text50        ;CaptionML=[ENU=Description;
                                                              PTB=Descri��o] }
  }
  KEYS
  {
    {    ;No                                      ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

