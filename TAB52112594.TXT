OBJECT Table 52112594 DACON Settlement
{
  OBJECT-PROPERTIES
  {
    Date=21/10/13;
    Time=15:36:31;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=DACON Settlement;
               PTB=Apura��o DACON];
  }
  FIELDS
  {
    { 10  ;   ;DACON No.           ;Code20        ;TableRelation=DACON;
                                                   CaptionML=[ENU=DACON No.;
                                                              PTB=N� DACON] }
    { 20  ;   ;Line No             ;Integer       ;CaptionML=[ENU=Line No.;
                                                              PTB=N� Linha] }
    { 30  ;   ;Company Name        ;Text30        ;TableRelation=Company;
                                                   OnValidate=BEGIN
                                                                VALIDATE("Company Settlement No.", '');
                                                              END;

                                                   CaptionML=[ENU=Company Name;
                                                              PTB=Nome Empresa] }
    { 40  ;   ;Active              ;Boolean       ;CaptionML=[ENU=Active;
                                                              PTB=Ativo] }
    { 50  ;   ;Company Settlement No.;Code20      ;OnValidate=VAR
                                                                taxSettlementCompany@35000000 : Record 52112523;
                                                                dacon@52006500 : Record 52112593;
                                                              BEGIN
                                                                IF "Company Settlement No." = '' THEN BEGIN
                                                                  Active := FALSE;
                                                                  EXIT;
                                                                END;

                                                                dacon.GET("DACON No.");
                                                                dacon.TESTFIELD("Start Date");
                                                                dacon.TESTFIELD("End Date");
                                                                taxSettlementCompany.CHANGECOMPANY("Company Name");
                                                                taxSettlementCompany.SETRANGE("Start Date", dacon."Start Date");
                                                                taxSettlementCompany.SETRANGE("End Date", dacon."End Date");
                                                                taxSettlementCompany.SETRANGE("No.", "Company Settlement No.");
                                                                taxSettlementCompany.FINDFIRST;
                                                                Active := TRUE;
                                                              END;

                                                   OnLookup=VAR
                                                              form@35000000 : Page 52112523;
                                                              taxSettlementCompany@52006500 : Record 52112523;
                                                              dacon@52006501 : Record 52112593;
                                                            BEGIN
                                                              TESTFIELD("Company Name");

                                                              dacon.GET("DACON No.");
                                                              dacon.TESTFIELD("Start Date");
                                                              dacon.TESTFIELD("End Date");
                                                              taxSettlementCompany.SETRANGE("Start Date", dacon."Start Date");
                                                              taxSettlementCompany.SETRANGE("End Date", dacon."End Date");
                                                              form.SETTABLEVIEW(taxSettlementCompany);
                                                              form.ChangeCompany("Company Name");
                                                              form.LOOKUPMODE := TRUE;
                                                              IF form.RUNMODAL = ACTION::LookupOK THEN BEGIN
                                                                form.GETRECORD(taxSettlementCompany);
                                                                VALIDATE("Company Settlement No.", taxSettlementCompany."No.");
                                                              END;
                                                            END;

                                                   CaptionML=[ENU=Statement No.;
                                                              PTB=N� Apura��o] }
  }
  KEYS
  {
    {    ;DACON No.,Line No                       ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

