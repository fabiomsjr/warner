OBJECT Codeunit 52112525 Create Retained Taxes Data
{
  OBJECT-PROPERTIES
  {
    Date=30/09/14;
    Time=14:30:11;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    OnRun=VAR
            r@52006500 : Record 52112586;
          BEGIN
          END;

  }
  CODE
  {
    VAR
      EntryNo@52006500 : Integer;

    PROCEDURE GetRetTaxesData@52006501(startDate@52006500 : Date;endDate@52006501 : Date;branchCode@52006504 : Code[20];purchOrSale@52006505 : ' ,Purchase,Sale';joinPCC@52006508 : Boolean;forceSplitPCC@52006507 : Boolean;VAR retTaxData@52006502 : TEMPORARY Record 52112586);
    VAR
      vatEntry@52006503 : Record 254;
      tmpVATEntry@52006506 : Record 254;
    BEGIN
      retTaxData.RESET;
      IF retTaxData.FINDLAST THEN
        EntryNo := retTaxData."Entry No.";
      retTaxData.SETCURRENTKEY(Type, "Document No.", "Posting Date", "Tax Identification");

      WITH vatEntry DO BEGIN
        //On Payment
        SETCURRENTKEY("Payment Date", Type, "Bill-to/Pay-to No.", "Document No.", "Tax Identification");
        SETRANGE("Payment Date", startDate, endDate);
        SETFILTER("Tax Identification", '%1|%2|%3|%4|%5|%6|%7',
                  "Tax Identification"::"PIS Ret.", "Tax Identification"::"COFINS Ret.", "Tax Identification"::"CSL Ret.", "Tax Identification"::IRRF,
                  "Tax Identification"::"INSS Ret.", "Tax Identification"::"ISS Ret.", "Tax Identification"::PCC);
        SETRANGE(Type, purchOrSale);
        SETRANGE("Document Type", "Document Type"::Invoice);
        SETFILTER(Amount, '<>0');
        SETFILTER("Payment/Receipt Base", '<>0');
        IF branchCode <> '' THEN
          SETRANGE("Branch Code", branchCode);
        IF FINDSET THEN
          REPEAT
            IF (forceSplitPCC) AND ("Tax Identification" = "Tax Identification"::PCC) AND ("Tax %" = 4.65) THEN BEGIN
              //Gambiarra, converte PCC de 4,65 para PIS/COFINS/CSL separados (DACON)
              tmpVATEntry.TRANSFERFIELDS(vatEntry);

              tmpVATEntry.Amount := ROUND((Amount / 0.0465) * 0.0065, 0.01);
              tmpVATEntry."Tax Identification" := "Tax Identification"::"PIS Ret.";
              ProcessTax(tmpVATEntry, retTaxData);

              tmpVATEntry.Amount := ROUND((Amount / 0.0465) * 0.03, 0.01);
              tmpVATEntry."Tax Identification" := "Tax Identification"::"COFINS Ret.";
              ProcessTax(tmpVATEntry, retTaxData);

              tmpVATEntry.Amount := ROUND((Amount / 0.0465) * 0.01, 0.01);
              tmpVATEntry."Tax Identification" := "Tax Identification"::"CSL Ret.";
              ProcessTax(tmpVATEntry, retTaxData);
            END ELSE
              ProcessTax(vatEntry, retTaxData);
          UNTIL NEXT = 0;

        //On Invoicing
        RESET;
        SETCURRENTKEY("Posting Date", Type, "Tax Identification");
        SETRANGE("Posting Date", startDate, endDate);
        SETFILTER("Tax Identification", '%1|%2|%3|%4|%5|%6|%7',
                  "Tax Identification"::"PIS Ret.", "Tax Identification"::"COFINS Ret.", "Tax Identification"::"CSL Ret.", "Tax Identification"::IRRF,
                  "Tax Identification"::"INSS Ret.", "Tax Identification"::"ISS Ret.", "Tax Identification"::PCC);
        SETRANGE(Type, purchOrSale);
        SETRANGE("Document Type", "Document Type"::Invoice);
        SETFILTER(Amount, '<>0');
        SETRANGE("Payment/Receipt Base", 0);
        IF branchCode <> '' THEN
          SETRANGE("Branch Code", branchCode);
        IF FINDSET THEN
          REPEAT
            ProcessTax(vatEntry, retTaxData);
          UNTIL NEXT = 0;
      END;
    END;

    LOCAL PROCEDURE ProcessTax@52006504(VAR vatEntry@52006500 : Record 254;VAR retTaxData@52006504 : TEMPORARY Record 52112586);
    VAR
      taxJurisdiction@52006503 : Record 320;
      customer@52006502 : Record 18;
      vendor@52006501 : Record 23;
      baseAmt@52006505 : Decimal;
    BEGIN
      IF IsInvoiceCancelled(vatEntry) THEN
        EXIT;

      WITH retTaxData DO BEGIN
        IF taxJurisdiction.GET(vatEntry."Tax Jurisdiction Code") THEN BEGIN
          RESET;
          SETRANGE(Type, GetType(vatEntry.Type));
          SETRANGE("Document No.", vatEntry."Document No.");
          SETRANGE("Posting Date", vatEntry."Posting Date");
          SETRANGE("Tax Identification", vatEntry."Tax Identification");
          IF NOT FINDFIRST THEN BEGIN
            EntryNo += 1;
            INIT;
            "Entry No." := EntryNo;
            Type := GetType(vatEntry.Type);
            "Document No." := vatEntry."Document No.";
            "Tax Identification" := vatEntry."Tax Identification";
            "Tax Jurisdiction Code" := vatEntry."Tax Jurisdiction Code";
            "Tax Revenue Code" := taxJurisdiction."Tax Revenue Code";
            "Customer/Vendor No." := vatEntry."Bill-to/Pay-to No.";
            "Beneficiary CPFCNPJ" := vatEntry."Beneficiary CPFCNPJ";
            CASE Type OF
              Type::Sale: BEGIN
                customer.GET("Customer/Vendor No.");
                "CNPJ/CPF" := customer."C.N.P.J./C.P.F.";
                "Company Nature" := customer."Company Nature";
              END;
              Type::Purchase: BEGIN
                vendor.GET("Customer/Vendor No.");
                "CNPJ/CPF" := vendor."C.N.P.J./C.P.F.";
                "Company Nature" := vendor."Company Nature";
              END;
            END;
            "Posting Date" := vatEntry."Posting Date";
            "Retention Date" := vatEntry."Payment Date";
            IF "Retention Date" = 0D THEN
              "Retention Date" := "Posting Date";
            "Date Period" := CALCDATE('<CM>', "Retention Date");
            INSERT;
          END;

          baseAmt := ABS(vatEntry.Base);
          IF baseAmt = 0 THEN
            baseAmt := ABS(vatEntry."Payment/Receipt Base");
          "Base Amount" += baseAmt;
          "Tax Amount" += ABS(vatEntry.Amount);
          MODIFY;
          RESET;
        END;
      END;
    END;

    PROCEDURE IsInvoiceCancelled@52006514(vatEntry@52006500 : Record 254) : Boolean;
    VAR
      salesInvHeader@52006501 : Record 112;
      purchInvHeader@52006502 : Record 122;
    BEGIN
      WITH vatEntry DO BEGIN
        CASE Type OF
          Type::Purchase: BEGIN
            IF NOT purchInvHeader.GET("Document No.") THEN
              EXIT(TRUE);
            purchInvHeader.CALCFIELDS("Credit Memos");
            EXIT(purchInvHeader."Credit Memos");
          END;
          Type::Sale: BEGIN
            IF NOT salesInvHeader.GET("Document No.") THEN
              EXIT(TRUE);
            salesInvHeader.CALCFIELDS("Credit Memos");
            EXIT(salesInvHeader."Credit Memos");
          END;
        ELSE
          EXIT(TRUE);
        END;
      END;
    END;

    LOCAL PROCEDURE GetType@52006513(vatEntryType@52006500 : Integer) : Integer;
    BEGIN
      IF vatEntryType = 1 THEN
        EXIT(1)
      ELSE
        EXIT(0);
    END;

    PROCEDURE CreateEFDContribRecords@52006500(VAR taxSettlement@52006500 : Record 52112523);
    VAR
      efdF600@52006501 : Record 52112549;
      efd1300@52006502 : Record 52112556;
      efd1700@52006503 : Record 52112555;
      retTaxData@52006504 : TEMPORARY Record 52112586;
      retTaxDataCOFINS@52006505 : TEMPORARY Record 52112586;
      period@52006506 : Date;
    BEGIN
      taxSettlement.TESTFIELD("Start Date");
      taxSettlement.TESTFIELD("End Date");
      DeleteEFDContribRecords(taxSettlement."No.");

      GetRetTaxesData(taxSettlement."Start Date", taxSettlement."End Date", taxSettlement."Branch Code", 2, FALSE, TRUE, retTaxData);
      WITH retTaxData DO BEGIN
        SETRANGE(Type, Type::Sale);
        SETRANGE("Tax Identification", "Tax Identification"::"COFINS Ret.");
        IF FINDSET THEN
          REPEAT
            retTaxDataCOFINS.INIT;
            retTaxDataCOFINS.TRANSFERFIELDS(retTaxData);
            retTaxDataCOFINS.INSERT;
          UNTIL NEXT = 0;
        retTaxDataCOFINS.SETCURRENTKEY(Type, "Document No.", "Posting Date", "Tax Identification");

        SETRANGE(Type, Type::Sale);
        SETRANGE("Tax Identification", "Tax Identification"::"PIS Ret.");
        IF FINDSET THEN
          REPEAT
            efdF600.INIT;
            efdF600."Tax Settlement No." := taxSettlement."No.";
            efdF600.Type := efdF600.Type::F600;
            efdF600.CNPJ := "CNPJ/CPF";
            efdF600."Retention Date" := "Retention Date";
            CASE "Company Nature" OF
              "Company Nature"::Private: efdF600."Indicator Nature Retention" := efdF600."Indicator Nature Retention"::"03";
              "Company Nature"::FederalOrgan: efdF600."Indicator Nature Retention" := efdF600."Indicator Nature Retention"::"01";
              "Company Nature"::OtherFederalOrgans: efdF600."Indicator Nature Retention" := efdF600."Indicator Nature Retention"::"02";
              "Company Nature"::Collective: efdF600."Indicator Nature Retention" := efdF600."Indicator Nature Retention"::"04";
              "Company Nature"::MachineVehicleFactory: efdF600."Indicator Nature Retention" := efdF600."Indicator Nature Retention"::"05";
            ELSE
              efdF600."Indicator Nature Retention" := efdF600."Indicator Nature Retention"::"99";
            END;
            IF Type = Type::Sale THEN BEGIN
              efdF600."Condition Indicator" := efdF600."Condition Indicator"::"0";
              efdF600."Income Nature" := efdF600."Income Nature"::"0-Non-Cumulative";
            END ELSE
              efdF600."Condition Indicator" := efdF600."Condition Indicator"::"1";
            efdF600."Tax Revenue Code" := "Tax Revenue Code";
            IF NOT efdF600.FIND THEN
              efdF600.INSERT;

            retTaxDataCOFINS.SETRANGE(Type, Type);
            retTaxDataCOFINS.SETRANGE("Customer/Vendor No.", "Customer/Vendor No.");
            retTaxDataCOFINS.SETRANGE("Posting Date", "Posting Date");
            retTaxDataCOFINS.SETRANGE("Document No.", "Document No.");
            retTaxDataCOFINS.SETRANGE("Tax Revenue Code", "Tax Revenue Code");
            IF retTaxDataCOFINS.FINDFIRST THEN
              efdF600."COFINS Amount" += retTaxDataCOFINS."Tax Amount";

            efdF600."Basis Amount" += "Base Amount";
            efdF600."PIS Amount" += "Tax Amount";

            efdF600."Retention Amount" := efdF600."PIS Amount" + efdF600."COFINS Amount";
            efdF600.MODIFY;
          UNTIL NEXT = 0;
      END;

      WITH efdF600 DO BEGIN
        SETRANGE("Tax Settlement No.", taxSettlement."No.");
        SETRANGE(Type, Type::F600);
        SETRANGE("Condition Indicator", "Condition Indicator"::"0");
        IF FINDSET THEN
          REPEAT
            period := CALCDATE('<CM>', "Retention Date");

            efd1300.INIT;
            efd1300."Tax Settlement No." := taxSettlement."No.";
            efd1300."Ind. Nature of Retention" := "Indicator Nature Retention";
            efd1300."Period of Receipt of Retention" := period;
            IF NOT efd1300.FIND THEN
              efd1300.INSERT;
            efd1300."Total Value of Retention" += "PIS Amount";
            efd1300."Amount of Contrib Deduction" += "PIS Amount";
            efd1300.MODIFY;

            efd1700.INIT;
            efd1700."Tax Settlement No." := taxSettlement."No.";
            efd1700."Ind. Nature of Retention" := "Indicator Nature Retention";
            efd1700."Period of Receipt of Retention" := period;
            IF NOT efd1700.FIND THEN
              efd1700.INSERT;
            efd1700."Total Value of Retention" += "COFINS Amount";
            efd1700."Amount of Contrib Deduction" += "COFINS Amount";
            efd1700.MODIFY;
          UNTIL NEXT = 0;
      END;
    END;

    LOCAL PROCEDURE DeleteEFDContribRecords@52006502(taxSettlementNo@52006500 : Code[20]);
    VAR
      efdF600@52006503 : Record 52112549;
      efd1300@52006502 : Record 52112556;
      efd1700@52006501 : Record 52112555;
    BEGIN
      efdF600.SETRANGE("Tax Settlement No.", taxSettlementNo);
      efdF600.SETRANGE(Type, efdF600.Type::F600);
      efdF600.DELETEALL;
      efd1300.SETRANGE("Tax Settlement No.", taxSettlementNo);
      efd1300.DELETEALL;
      efd1700.SETRANGE("Tax Settlement No.", taxSettlementNo);
      efd1700.DELETEALL;
    END;

    BEGIN
    END.
  }
}

