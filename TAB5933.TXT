OBJECT Table 5933 Service Order Posting Buffer
{
  OBJECT-PROPERTIES
  {
    Date=07/09/12;
    Time=12:00:00;
    Version List=NAVW17.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Service Order Posting Buffer;
               PTB=Mem. Int. Reg. Ordem Servi�o];
  }
  FIELDS
  {
    { 1   ;   ;Service Order No.   ;Code20        ;CaptionML=[ENU=Service Order No.;
                                                              PTB=N� Ordem Servi�o] }
    { 2   ;   ;Entry Type          ;Option        ;CaptionML=[ENU=Entry Type;
                                                              PTB=Tipo Mov.];
                                                   OptionCaptionML=[ENU=Usage,Sale;
                                                                    PTB=Consumo,Venda];
                                                   OptionString=Usage,Sale }
    { 3   ;   ;Posting Group Type  ;Option        ;CaptionML=[ENU=Posting Group Type;
                                                              PTB=Tipo Gr. Cont�bil];
                                                   OptionCaptionML=[ENU=" ,Resource,Item,Service Cost,Service Contract";
                                                                    PTB=" ,Recurso,Item,Custo de Servi�o,Contrato de Servi�o"];
                                                   OptionString=[ ,Resource,Item,Service Cost,Service Contract] }
    { 4   ;   ;No.                 ;Code20        ;CaptionML=[ENU=No.;
                                                              PTB=N�] }
    { 6   ;   ;Global Dimension 1 Code;Code20     ;TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(1));
                                                   CaptionML=[ENU=Global Dimension 1 Code;
                                                              PTB=Cod. Dimens�o 1 Global];
                                                   CaptionClass='1,1,1' }
    { 7   ;   ;Global Dimension 2 Code;Code20     ;TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(2));
                                                   CaptionML=[ENU=Global Dimension 2 Code;
                                                              PTB=Cod. Dimens�o 2 Global];
                                                   CaptionClass='1,1,2' }
    { 8   ;   ;Gen. Bus. Posting Group;Code10     ;TableRelation="Gen. Business Posting Group";
                                                   CaptionML=[ENU=Gen. Bus. Posting Group;
                                                              PTB=Gr. Cont�bil Neg�cio] }
    { 9   ;   ;Gen. Prod. Posting Group;Code10    ;TableRelation="Gen. Product Posting Group";
                                                   CaptionML=[ENU=Gen. Prod. Posting Group;
                                                              PTB=Gr. Cont�bil Produto] }
    { 10  ;   ;Unit of Measure Code;Code10        ;CaptionML=[ENU=Unit of Measure Code;
                                                              PTB=Cod. Unidade Medida] }
    { 11  ;   ;Work Type Code      ;Code10        ;TableRelation="Work Type";
                                                   CaptionML=[ENU=Work Type Code;
                                                              PTB=Cod. Tipo Trabalho] }
    { 13  ;   ;Quantity            ;Decimal       ;CaptionML=[ENU=Quantity;
                                                              PTB=Quantidade];
                                                   DecimalPlaces=0:5 }
    { 14  ;   ;Total Cost          ;Decimal       ;CaptionML=[ENU=Total Cost;
                                                              PTB=Custo Total];
                                                   AutoFormatType=1 }
    { 15  ;   ;Total Price         ;Decimal       ;CaptionML=[ENU=Total Price;
                                                              PTB=Pre�o Total];
                                                   AutoFormatType=1 }
    { 16  ;   ;Appl.-to Service Entry;Integer     ;CaptionML=[ENU=Appl.-to Service Entry;
                                                              PTB=Aplic. a Mov. Servi�o] }
    { 17  ;   ;Service Contract No.;Code20        ;CaptionML=[ENU=Service Contract No.;
                                                              PTB=N� Contrato Servi�o] }
    { 18  ;   ;Service Item No.    ;Code20        ;CaptionML=[ENU=Service Item No.;
                                                              PTB=N� Item Servi�o] }
    { 21  ;   ;Qty. to Invoice     ;Decimal       ;CaptionML=[ENU=Qty. to Invoice;
                                                              PTB=Qtd. a Faturar];
                                                   AutoFormatType=1 }
    { 22  ;   ;Location Code       ;Code20        ;CaptionML=[ENU=Location Code;
                                                              PTB=Cod. Dep�sito] }
    { 23  ;   ;Dimension Entry No. ;Integer       ;CaptionML=[ENU=Dimension Entry No.;
                                                              PTB=N� Mov. Dimens�o] }
    { 24  ;   ;Line Discount %     ;Decimal       ;CaptionML=[ENU=Line Discount %;
                                                              PTB=% Desconto Linha] }
    { 480 ;   ;Dimension Set ID    ;Integer       ;TableRelation="Dimension Set Entry";
                                                   CaptionML=ENU=Dimension Set ID;
                                                   Editable=No }
  }
  KEYS
  {
    {    ;Service Order No.,Entry Type,Posting Group Type,No.,Gen. Bus. Posting Group,Gen. Prod. Posting Group,Global Dimension 1 Code,Global Dimension 2 Code,Unit of Measure Code,Service Item No.,Location Code,Appl.-to Service Entry;
                                                   Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

