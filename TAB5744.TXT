OBJECT Table 5744 Transfer Shipment Header
{
  OBJECT-PROPERTIES
  {
    Date=09/09/14;
    Time=12:00:00;
    Version List=NAVW18.00;
  }
  PROPERTIES
  {
    DataCaptionFields=No.;
    OnDelete=VAR
               InvtCommentLine@1000 : Record 5748;
               TransShptLine@1001 : Record 5745;
               MoveEntries@1002 : Codeunit 361;
             BEGIN
               TransShptLine.SETRANGE("Document No.","No.");
               IF TransShptLine.FIND('-') THEN
                 REPEAT
                   TransShptLine.DELETE;
                 UNTIL TransShptLine.NEXT = 0;

               InvtCommentLine.SETRANGE("Document Type",InvtCommentLine."Document Type"::"Posted Transfer Shipment");
               InvtCommentLine.SETRANGE("No.","No.");
               InvtCommentLine.DELETEALL;

               ItemTrackingMgt.DeleteItemEntryRelation(
                 DATABASE::"Transfer Shipment Line",0,"No.",'',0,0,TRUE);

               MoveEntries.MoveDocRelatedEntries(DATABASE::"Transfer Shipment Header","No.");
             END;

    CaptionML=[ENU=Transfer Shipment Header;
               PTB=Cab. Envio Transf.];
    LookupPageID=Page5752;
  }
  FIELDS
  {
    { 1   ;   ;No.                 ;Code20        ;CaptionML=[ENU=No.;
                                                              PTB=N�] }
    { 2   ;   ;Transfer-from Code  ;Code10        ;TableRelation=Location WHERE (Use As In-Transit=CONST(No));
                                                   CaptionML=[ENU=Transfer-from Code;
                                                              PTB=Transf.-de Cod.] }
    { 3   ;   ;Transfer-from Name  ;Text50        ;CaptionML=[ENU=Transfer-from Name;
                                                              PTB=Transf.-de Nome] }
    { 4   ;   ;Transfer-from Name 2;Text50        ;CaptionML=[ENU=Transfer-from Name 2;
                                                              PTB=Transf.-de Nome 2] }
    { 5   ;   ;Transfer-from Address;Text50       ;CaptionML=[ENU=Transfer-from Address;
                                                              PTB=Transf.-de Endere�o] }
    { 6   ;   ;Transfer-from Address 2;Text50     ;CaptionML=[ENU=Transfer-from Address 2;
                                                              PTB=Transf.-de Endere�o Complementar] }
    { 7   ;   ;Transfer-from Post Code;Code20     ;TableRelation="Post Code";
                                                   ValidateTableRelation=No;
                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=Transfer-from Post Code;
                                                              PTB=Transf.-de CEP] }
    { 8   ;   ;Transfer-from City  ;Text30        ;TableRelation="Post Code".City;
                                                   ValidateTableRelation=No;
                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=Transfer-from City;
                                                              PTB=Transf.-de Cidade] }
    { 9   ;   ;Transfer-from County;Text30        ;CaptionML=[ENU=Transfer-from County;
                                                              PTB=Transf.-de Distrito] }
    { 10  ;   ;Trsf.-from Country/Region Code;Code10;
                                                   TableRelation=Country/Region;
                                                   CaptionML=[ENU=Trsf.-from Country/Region Code;
                                                              PTB=Trsf.-de C�d. Pa�s/Regi�o] }
    { 11  ;   ;Transfer-to Code    ;Code10        ;TableRelation=Location WHERE (Use As In-Transit=CONST(No));
                                                   CaptionML=[ENU=Transfer-to Code;
                                                              PTB=Transf.-para Cod.] }
    { 12  ;   ;Transfer-to Name    ;Text50        ;CaptionML=[ENU=Transfer-to Name;
                                                              PTB=Transf.-para Nome] }
    { 13  ;   ;Transfer-to Name 2  ;Text50        ;CaptionML=[ENU=Transfer-to Name 2;
                                                              PTB=Transf.-to Nome 2] }
    { 14  ;   ;Transfer-to Address ;Text50        ;CaptionML=[ENU=Transfer-to Address;
                                                              PTB=Transf.-para Endere�o] }
    { 15  ;   ;Transfer-to Address 2;Text50       ;CaptionML=[ENU=Transfer-to Address 2;
                                                              PTB=Transf.-para Endere�o Complementar] }
    { 16  ;   ;Transfer-to Post Code;Code20       ;TableRelation="Post Code";
                                                   ValidateTableRelation=No;
                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=Transfer-to Post Code;
                                                              PTB=Transf.-para CEP] }
    { 17  ;   ;Transfer-to City    ;Text30        ;TableRelation="Post Code".City;
                                                   ValidateTableRelation=No;
                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=Transfer-to City;
                                                              PTB=Transf.-para Clidade] }
    { 18  ;   ;Transfer-to County  ;Text30        ;CaptionML=[ENU=Transfer-to County;
                                                              PTB=Transf.-para Distrito] }
    { 19  ;   ;Trsf.-to Country/Region Code;Code10;TableRelation=Country/Region;
                                                   CaptionML=[ENU=Trsf.-to Country/Region Code;
                                                              PTB=Trsf.-para C�d. Pa�s/Regi�o] }
    { 20  ;   ;Transfer Order Date ;Date          ;CaptionML=[ENU=Transfer Order Date;
                                                              PTB=Data Ordem Transfer�ncia] }
    { 21  ;   ;Posting Date        ;Date          ;CaptionML=[ENU=Posting Date;
                                                              PTB=Data Registo] }
    { 22  ;   ;Comment             ;Boolean       ;FieldClass=FlowField;
                                                   CalcFormula=Exist("Inventory Comment Line" WHERE (Document Type=CONST(Posted Transfer Shipment),
                                                                                                     No.=FIELD(No.)));
                                                   CaptionML=[ENU=Comment;
                                                              PTB=Coment�rio];
                                                   Editable=No }
    { 23  ;   ;Shortcut Dimension 1 Code;Code20   ;TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(1));
                                                   CaptionML=[ENU=Shortcut Dimension 1 Code;
                                                              PTB=Cod. Atalho Dimens�o 1];
                                                   CaptionClass='1,2,1' }
    { 24  ;   ;Shortcut Dimension 2 Code;Code20   ;TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(2));
                                                   CaptionML=[ENU=Shortcut Dimension 2 Code;
                                                              PTB=Cod. Atalho Dimens�o 2];
                                                   CaptionClass='1,2,2' }
    { 25  ;   ;Transfer Order No.  ;Code20        ;TableRelation="Transfer Header";
                                                   ValidateTableRelation=No;
                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=Transfer Order No.;
                                                              PTB=N� Ordem Transf.] }
    { 26  ;   ;No. Series          ;Code10        ;TableRelation="No. Series";
                                                   CaptionML=[ENU=No. Series;
                                                              PTB=N� S�ries] }
    { 27  ;   ;Shipment Date       ;Date          ;CaptionML=[ENU=Shipment Date;
                                                              PTB=Data Envio] }
    { 28  ;   ;Receipt Date        ;Date          ;CaptionML=[ENU=Receipt Date;
                                                              PTB=Data Remessa Compra] }
    { 29  ;   ;In-Transit Code     ;Code10        ;TableRelation=Location.Code WHERE (Use As In-Transit=CONST(Yes));
                                                   CaptionML=[ENU=In-Transit Code;
                                                              PTB=Cod. Em Tr�nsito] }
    { 30  ;   ;Transfer-from Contact;Text50       ;CaptionML=[ENU=Transfer-from Contact;
                                                              PTB=Transf.-de Contato] }
    { 31  ;   ;Transfer-to Contact ;Text50        ;CaptionML=[ENU=Transfer-to Contact;
                                                              PTB=Transf.-para Contato] }
    { 32  ;   ;External Document No.;Code35       ;CaptionML=[ENU=External Document No.;
                                                              PTB=N� Documento Externo] }
    { 33  ;   ;Shipping Agent Code ;Code10        ;TableRelation="Shipping Agent";
                                                   AccessByPermission=TableData 5790=R;
                                                   CaptionML=[ENU=Shipping Agent Code;
                                                              PTB=Cod. Transportador] }
    { 34  ;   ;Shipping Agent Service Code;Code10 ;TableRelation="Shipping Agent Services".Code WHERE (Shipping Agent Code=FIELD(Shipping Agent Code));
                                                   CaptionML=[ENU=Shipping Agent Service Code;
                                                              PTB=Cod. Servi�o Transportador] }
    { 35  ;   ;Shipment Method Code;Code10        ;TableRelation="Shipment Method";
                                                   CaptionML=[ENU=Shipment Method Code;
                                                              PTB=Cod. Condi��es Envio] }
    { 47  ;   ;Transaction Type    ;Code10        ;TableRelation="Transaction Type";
                                                   CaptionML=[ENU=Transaction Type;
                                                              PTB=Natureza Transa��o] }
    { 48  ;   ;Transport Method    ;Code10        ;TableRelation="Transport Method";
                                                   CaptionML=[ENU=Transport Method;
                                                              PTB=Modo Transporte] }
    { 59  ;   ;Entry/Exit Point    ;Code10        ;TableRelation="Entry/Exit Point";
                                                   CaptionML=[ENU=Entry/Exit Point;
                                                              PTB=Porto/Aeroporto] }
    { 63  ;   ;Area                ;Code10        ;TableRelation=Area;
                                                   CaptionML=[ENU=Area;
                                                              PTB=�rea] }
    { 64  ;   ;Transaction Specification;Code10   ;TableRelation="Transaction Specification";
                                                   CaptionML=[ENU=Transaction Specification;
                                                              PTB=Especifica�ao Transa��o] }
    { 480 ;   ;Dimension Set ID    ;Integer       ;TableRelation="Dimension Set Entry";
                                                   OnLookup=BEGIN
                                                              ShowDimensions;
                                                            END;

                                                   CaptionML=ENU=Dimension Set ID;
                                                   Editable=No }
  }
  KEYS
  {
    {    ;No.                                     ;Clustered=Yes }
  }
  FIELDGROUPS
  {
    { 1   ;DropDown            ;No.,Transfer-from Code,Transfer-to Code,Posting Date,Transfer Order Date }
  }
  CODE
  {
    VAR
      DimMgt@1000 : Codeunit 408;
      ItemTrackingMgt@1001 : Codeunit 6500;

    PROCEDURE Navigate@2();
    VAR
      NavigateForm@1000 : Page 344;
    BEGIN
      NavigateForm.SetDoc("Posting Date","No.");
      NavigateForm.RUN;
    END;

    PROCEDURE PrintRecords@3(ShowRequestForm@1000 : Boolean);
    VAR
      ReportSelection@1001 : Record 77;
      TransShptHeader@1002 : Record 5744;
    BEGIN
      WITH TransShptHeader DO BEGIN
        COPY(Rec);
        ReportSelection.SETRANGE(Usage,ReportSelection.Usage::Inv2);
        ReportSelection.SETFILTER("Report ID",'<>0');
        ReportSelection.FIND('-');
        REPEAT
          REPORT.RUNMODAL(ReportSelection."Report ID",ShowRequestForm,FALSE,TransShptHeader);
        UNTIL ReportSelection.NEXT = 0;
      END;
    END;

    PROCEDURE ShowDimensions@1();
    BEGIN
      DimMgt.ShowDimensionSet("Dimension Set ID",STRSUBSTNO('%1 %2',TABLECAPTION,"No."));
    END;

    BEGIN
    END.
  }
}

