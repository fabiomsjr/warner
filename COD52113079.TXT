OBJECT Codeunit 52113079 Override Post. Approv. Entries
{
  OBJECT-PROPERTIES
  {
    Date=20/05/15;
    Time=09:21:53;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    TableNo=456;
    OnRun=VAR
            SalesApprovMgt@52006500 : Codeunit 52113075;
            TableID@52006501 : Integer;
            DocNo@52006502 : Text;
          BEGIN
            FILTERGROUP(2);

            IF GETFILTER("Table ID") = '' THEN
              EXIT;

            IF GETFILTER("Document No.") = '' THEN
              EXIT;

            TableID := GETRANGEMIN("Table ID");
            DocNo := GETRANGEMIN("Document No.");
            CASE TableID OF
              DATABASE::"Sales Invoice Header": OverrPostedSalesInvoice(DocNo);
              DATABASE::"Sales Cr.Memo Header": OverrPostedSalesCrMemo(DocNo);
              DATABASE::"Purch. Inv. Header": OverrPostedPurchInvoice(DocNo);
              DATABASE::"Purch. Cr. Memo Hdr.": OverrPostedPurchCrMemo(DocNo);
            END;
          END;

  }
  CODE
  {

    LOCAL PROCEDURE OverrPostedSalesInvoice@52006519(DocNo@52006500 : Text);
    VAR
      SalesInvHeader@52006501 : Record 112;
    BEGIN
      IF SalesInvHeader.GET(DocNo) AND (SalesInvHeader."Approval Flow No." <> '') THEN
        OpenApprovalFlow(SalesInvHeader."Approval Flow No.");
    END;

    LOCAL PROCEDURE OverrPostedSalesCrMemo@52006520(DocNo@52006500 : Text);
    VAR
      SalesCrMemoHeader@52006501 : Record 114;
    BEGIN
      IF SalesCrMemoHeader.GET(DocNo) AND (SalesCrMemoHeader."Approval Flow No." <> '') THEN
        OpenApprovalFlow(SalesCrMemoHeader."Approval Flow No.");
    END;

    LOCAL PROCEDURE OverrPostedPurchInvoice@52006547(DocNo@52006500 : Text);
    VAR
      PurchInvHeader@52006501 : Record 122;
    BEGIN
      IF PurchInvHeader.GET(DocNo) AND (PurchInvHeader."Approval Flow No." <> '') THEN
        OpenApprovalFlow(PurchInvHeader."Approval Flow No.");
    END;

    LOCAL PROCEDURE OverrPostedPurchCrMemo@52006546(DocNo@52006500 : Text);
    VAR
      PurchCrMemoHeader@52006501 : Record 124;
    BEGIN
      IF PurchCrMemoHeader.GET(DocNo) AND (PurchCrMemoHeader."Approval Flow No." <> '') THEN
        OpenApprovalFlow(PurchCrMemoHeader."Approval Flow No.");
    END;

    LOCAL PROCEDURE OpenApprovalFlow@52006525(No@52006500 : Code[20]);
    VAR
      ApprovFlow@52006501 : Record 52113073;
    BEGIN
      IF No = '' THEN
        EXIT;

      ApprovFlow.GET(No);
      PAGE.RUN(PAGE::"Approval Flow Card", ApprovFlow);
      ERROR('');
    END;

    BEGIN
    END.
  }
}

