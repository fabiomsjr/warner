OBJECT Page 52112548 GIA Occurrence Codes
{
  OBJECT-PROPERTIES
  {
    Date=06/12/13;
    Time=14:11:15;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=GIA Occurrence Codes;
               PTB=C�digos Ocorr�ncia GIA];
    SourceTable=Table52112587;
    DelayedInsert=Yes;
    PageType=List;
  }
  CONTROLS
  {
    { 52006500;0;Container;
                ContainerType=ContentArea }

    { 52006501;1;Group    ;
                Name=Group;
                GroupType=Repeater }

    { 52006505;2;Field    ;
                SourceExpr="Territory Code" }

    { 52006504;2;Field    ;
                SourceExpr="Settlement Amount Type" }

    { 52006502;2;Field    ;
                SourceExpr=Code }

    { 52006503;2;Field    ;
                SourceExpr=Description }

  }
  CODE
  {

    BEGIN
    END.
  }
}

