OBJECT Table 35000610 Doc. Situation2
{
  OBJECT-PROPERTIES
  {
    Date=29/02/12;
    Time=18:14:18;
    Version List=EFD5.08.09;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Doc. Situation;
               PTB=Situa��o Documento];
    LookupPageID=Page35000710;
    DrillDownPageID=Page35000710;
  }
  FIELDS
  {
    { 1   ;   ;Code                ;Code10        ;CaptionML=PTB=C�d. Situa��o Documento }
    { 2   ;   ;Description         ;Text100       ;CaptionML=PTB=Descri��o }
  }
  KEYS
  {
    {    ;Code                                    ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

