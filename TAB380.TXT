OBJECT Table 380 Detailed Vendor Ledg. Entry
{
  OBJECT-PROPERTIES
  {
    Date=09/09/14;
    Time=12:00:00;
    Version List=NAVW18.00,NAVBR7;
  }
  PROPERTIES
  {
    Permissions=TableData 380=m;
    DataCaptionFields=Vendor No.;
    OnInsert=BEGIN
               SetLedgerEntryAmount;
             END;

    CaptionML=[ENU=Detailed Vendor Ledg. Entry;
               PTB=Mov. Detalhado Fornecedor];
    LookupPageID=Page574;
    DrillDownPageID=Page574;
  }
  FIELDS
  {
    { 1   ;   ;Entry No.           ;Integer       ;CaptionML=[ENU=Entry No.;
                                                              PTB=N� Mov.] }
    { 2   ;   ;Vendor Ledger Entry No.;Integer    ;TableRelation="Vendor Ledger Entry";
                                                   CaptionML=[ENU=Vendor Ledger Entry No.;
                                                              PTB=N� Mov. Forn.] }
    { 3   ;   ;Entry Type          ;Option        ;CaptionML=[ENU=Entry Type;
                                                              PTB=Tipo Mov.];
                                                   OptionCaptionML=[ENU=,Initial Entry,Application,Unrealized Loss,Unrealized Gain,Realized Loss,Realized Gain,Payment Discount,Payment Discount (VAT Excl.),Payment Discount (VAT Adjustment),Appln. Rounding,Correction of Remaining Amount,Payment Tolerance,Payment Discount Tolerance,Payment Tolerance (VAT Excl.),Payment Tolerance (VAT Adjustment),Payment Discount Tolerance (VAT Excl.),Payment Discount Tolerance (VAT Adjustment);
                                                                    PTB=,Mov. Inicial,Liquida��o,Perda n�o Real.,Ganho n�o Real.,Perda Real.,Ganho Real.,Desconto Pagamento,Desconto Pagamento (Excl. IPI),Desconto Pagamento (Ajuste IPI),Arredond. Liq.,Corre��o do Valor Pendente,Toler�ncia Pagamento,Toler�ncia Desconto Pagamento,Toler�ncia Pagamento (Excl. IPI),Toler�ncia Pagamento (Ajuste IPI),Toler�ncia Desconto Pagamento (Excl. IPI),Toler�ncia Desconto Pagamento (Ajuste IPI)];
                                                   OptionString=,Initial Entry,Application,Unrealized Loss,Unrealized Gain,Realized Loss,Realized Gain,Payment Discount,Payment Discount (VAT Excl.),Payment Discount (VAT Adjustment),Appln. Rounding,Correction of Remaining Amount,Payment Tolerance,Payment Discount Tolerance,Payment Tolerance (VAT Excl.),Payment Tolerance (VAT Adjustment),Payment Discount Tolerance (VAT Excl.),Payment Discount Tolerance (VAT Adjustment) }
    { 4   ;   ;Posting Date        ;Date          ;CaptionML=[ENU=Posting Date;
                                                              PTB=Data Registo] }
    { 5   ;   ;Document Type       ;Option        ;CaptionML=[ENU=Document Type;
                                                              PTB=Tipo Documento];
                                                   OptionCaptionML=[ENU=" ,Payment,Invoice,Credit Memo,Finance Charge Memo,Reminder,Refund";
                                                                    PTB=" ,Pagamento,Nota Fiscal,Nota de Cr�dito,Nota de Encargo Financeiro,Lembrete,Reembolso"];
                                                   OptionString=[ ,Payment,Invoice,Credit Memo,Finance Charge Memo,Reminder,Refund] }
    { 6   ;   ;Document No.        ;Code20        ;CaptionML=[ENU=Document No.;
                                                              PTB=No. Documento] }
    { 7   ;   ;Amount              ;Decimal       ;CaptionML=[ENU=Amount;
                                                              PTB=Valor];
                                                   AutoFormatType=1;
                                                   AutoFormatExpr="Currency Code" }
    { 8   ;   ;Amount (LCY)        ;Decimal       ;CaptionML=[ENU=Amount (LCY);
                                                              PTB=Valor (ML)];
                                                   AutoFormatType=1 }
    { 9   ;   ;Vendor No.          ;Code20        ;TableRelation=Vendor;
                                                   CaptionML=[ENU=Vendor No.;
                                                              PTB=N� Fornecedor] }
    { 10  ;   ;Currency Code       ;Code10        ;TableRelation=Currency;
                                                   CaptionML=[ENU=Currency Code;
                                                              PTB=Cod. Moeda] }
    { 11  ;   ;User ID             ;Code50        ;TableRelation=User."User Name";
                                                   OnLookup=VAR
                                                              UserMgt@1000 : Codeunit 418;
                                                            BEGIN
                                                              UserMgt.LookupUserID("User ID");
                                                            END;

                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=User ID;
                                                              PTB=ID Usu�rio] }
    { 12  ;   ;Source Code         ;Code10        ;TableRelation="Source Code";
                                                   CaptionML=[ENU=Source Code;
                                                              PTB=Cod. Origem] }
    { 13  ;   ;Transaction No.     ;Integer       ;CaptionML=[ENU=Transaction No.;
                                                              PTB=N� Lan�amento] }
    { 14  ;   ;Journal Batch Name  ;Code10        ;TestTableRelation=No;
                                                   CaptionML=[ENU=Journal Batch Name;
                                                              PTB=Nome Se��o Di�rio] }
    { 15  ;   ;Reason Code         ;Code10        ;TableRelation="Reason Code";
                                                   CaptionML=[ENU=Reason Code;
                                                              PTB=Cod. Raz�o] }
    { 16  ;   ;Debit Amount        ;Decimal       ;CaptionML=[ENU=Debit Amount;
                                                              PTB=Valor D�bito];
                                                   BlankZero=Yes;
                                                   AutoFormatType=1;
                                                   AutoFormatExpr="Currency Code" }
    { 17  ;   ;Credit Amount       ;Decimal       ;CaptionML=[ENU=Credit Amount;
                                                              PTB=Valor Cr�dito];
                                                   BlankZero=Yes;
                                                   AutoFormatType=1;
                                                   AutoFormatExpr="Currency Code" }
    { 18  ;   ;Debit Amount (LCY)  ;Decimal       ;CaptionML=[ENU=Debit Amount (LCY);
                                                              PTB=Valor D�bito (ML)];
                                                   BlankZero=Yes;
                                                   AutoFormatType=1 }
    { 19  ;   ;Credit Amount (LCY) ;Decimal       ;CaptionML=[ENU=Credit Amount (LCY);
                                                              PTB=Valor Cr�dito (ML)];
                                                   BlankZero=Yes;
                                                   AutoFormatType=1 }
    { 20  ;   ;Initial Entry Due Date;Date        ;CaptionML=[ENU=Initial Entry Due Date;
                                                              PTB=Data Vencimento] }
    { 21  ;   ;Initial Entry Global Dim. 1;Code20 ;TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(1));
                                                   CaptionML=[ENU=Initial Entry Global Dim. 1;
                                                              PTB=Mov. Inicial Dimens�o Global 1] }
    { 22  ;   ;Initial Entry Global Dim. 2;Code20 ;TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(2));
                                                   CaptionML=[ENU=Initial Entry Global Dim. 2;
                                                              PTB=Mov. Inicial Dimens�o Global 2] }
    { 24  ;   ;Gen. Bus. Posting Group;Code10     ;TableRelation="Gen. Business Posting Group";
                                                   CaptionML=[ENU=Gen. Bus. Posting Group;
                                                              PTB=Gr. Cont�bil Neg�cio] }
    { 25  ;   ;Gen. Prod. Posting Group;Code10    ;TableRelation="Gen. Product Posting Group";
                                                   CaptionML=[ENU=Gen. Prod. Posting Group;
                                                              PTB=Gr. Cont�bil Produto] }
    { 29  ;   ;Use Tax             ;Boolean       ;CaptionML=[ENU=Use Tax;
                                                              PTB=Utiliza Imposto] }
    { 30  ;   ;VAT Bus. Posting Group;Code10      ;TableRelation="VAT Business Posting Group";
                                                   CaptionML=[ENU=VAT Bus. Posting Group;
                                                              PTB=Gr. Registro Imp. Neg�cio] }
    { 31  ;   ;VAT Prod. Posting Group;Code10     ;TableRelation="VAT Product Posting Group";
                                                   CaptionML=[ENU=VAT Prod. Posting Group;
                                                              PTB=Gr. Registo Imp. Produto] }
    { 35  ;   ;Initial Document Type;Option       ;CaptionML=[ENU=Initial Document Type;
                                                              PTB=Tipo Documento Inicial];
                                                   OptionCaptionML=[ENU=" ,Payment,Invoice,Credit Memo,Finance Charge Memo,Reminder,Refund";
                                                                    PTB=" ,Pagamento,Nota Fiscal,Nota de Cr�dito,Nota de Encargo Financeiro,Lembrete,Reembolso"];
                                                   OptionString=[ ,Payment,Invoice,Credit Memo,Finance Charge Memo,Reminder,Refund] }
    { 36  ;   ;Applied Vend. Ledger Entry No.;Integer;
                                                   CaptionML=[ENU=Applied Vend. Ledger Entry No.;
                                                              PTB=N� Mov. Fornecedor Aplicado] }
    { 37  ;   ;Unapplied           ;Boolean       ;CaptionML=[ENU=Unapplied;
                                                              PTB=Desaplicado] }
    { 38  ;   ;Unapplied by Entry No.;Integer     ;TableRelation="Detailed Vendor Ledg. Entry";
                                                   CaptionML=[ENU=Unapplied by Entry No.;
                                                              PTB=Desaplicado-por N� Mov.] }
    { 39  ;   ;Remaining Pmt. Disc. Possible;Decimal;
                                                   CaptionML=[ENU=Remaining Pmt. Disc. Possible;
                                                              PTB=Desc. Pag. Poss�vel Pendente];
                                                   AutoFormatType=1;
                                                   AutoFormatExpr="Currency Code" }
    { 40  ;   ;Max. Payment Tolerance;Decimal     ;CaptionML=[ENU=Max. Payment Tolerance;
                                                              PTB=Max. Toler�ncia de Pagamento];
                                                   AutoFormatType=1;
                                                   AutoFormatExpr="Currency Code" }
    { 41  ;   ;Tax Jurisdiction Code;Code20       ;TableRelation="Tax Jurisdiction";
                                                   CaptionML=[ENU=Tax Jurisdiction Code;
                                                              PTB=Cod. Jurisdi��o Imposto];
                                                   Editable=No }
    { 42  ;   ;Application No.     ;Integer       ;CaptionML=ENU=Application No.;
                                                   Editable=No }
    { 43  ;   ;Ledger Entry Amount ;Boolean       ;CaptionML=ENU=Ledger Entry Amount;
                                                   Editable=No }
    { 52112430;;Branch Code (Old)  ;Code20        ;TableRelation="Branch Information";
                                                   CaptionML=[ENU=Branch Code;
                                                              PTB=C�d. Filial] }
    { 52112440;;BR Prepayment      ;Boolean       ;CaptionML=PTB=Adiantamento }
    { 52112450;;Vend. Ledg. Document No.;Code20   ;FieldClass=FlowField;
                                                   CalcFormula=Lookup("Vendor Ledger Entry"."Document No." WHERE (Entry No.=FIELD(Vendor Ledger Entry No.)));
                                                   CaptionML=[ENU=Vend. Ledg. Document No.;
                                                              PTB=N� Documento Mov. Forn.];
                                                   Editable=No }
    { 52112460;;Vend. Ledg. Ext. Doc. No.;Code35  ;FieldClass=FlowField;
                                                   CalcFormula=Lookup("Vendor Ledger Entry"."External Document No." WHERE (Entry No.=FIELD(Vendor Ledger Entry No.)));
                                                   CaptionML=[ENU=Vend. Ledg. Ext. Doc. No.;
                                                              PTB=N� Doc. Externo Mov. Forn.];
                                                   Editable=No }
    { 52112470;;Vend. Ledg. Dim. Set ID;Integer   ;FieldClass=FlowField;
                                                   CalcFormula=Lookup("Vendor Ledger Entry"."Dimension Set ID" WHERE (Entry No.=FIELD(Vendor Ledger Entry No.)));
                                                   CaptionML=[ENU=Vend. Ledg. Dim. Set ID;
                                                              PTB=ID Dimens�o Mov. Forn.];
                                                   Editable=No }
    { 52112480;;Branch Code        ;Code20        ;FieldClass=FlowField;
                                                   CalcFormula=Lookup("Vendor Ledger Entry"."Branch Code" WHERE (Entry No.=FIELD(Vendor Ledger Entry No.)));
                                                   CaptionML=[ENU=Branch Code;
                                                              PTB=C�d. Filial];
                                                   Editable=No }
  }
  KEYS
  {
    {    ;Entry No.                               ;Clustered=Yes }
    {    ;Vendor Ledger Entry No.,Posting Date     }
    {    ;Vendor Ledger Entry No.,Entry Type,Posting Date;
                                                   SumIndexFields=Amount (LCY);
                                                   MaintainSQLIndex=No }
    {    ;Ledger Entry Amount,Vendor Ledger Entry No.,Posting Date;
                                                   SumIndexFields=Amount,Amount (LCY),Debit Amount,Credit Amount,Debit Amount (LCY),Credit Amount (LCY);
                                                   MaintainSQLIndex=No;
                                                   MaintainSIFTIndex=No }
    {    ;Initial Document Type,Entry Type,Vendor No.,Currency Code,Initial Entry Global Dim. 1,Initial Entry Global Dim. 2,Posting Date;
                                                   SumIndexFields=Amount,Amount (LCY);
                                                   MaintainSIFTIndex=No }
    {    ;Vendor No.,Currency Code,Initial Entry Global Dim. 1,Initial Entry Global Dim. 2,Initial Entry Due Date,Posting Date;
                                                   SumIndexFields=Amount,Amount (LCY) }
    {    ;Document No.,Document Type,Posting Date  }
    {    ;Applied Vend. Ledger Entry No.,Entry Type }
    {    ;Transaction No.,Vendor No.,Entry Type    }
    {    ;Application No.,Vendor No.,Entry Type    }
  }
  FIELDGROUPS
  {
    { 1   ;DropDown            ;Entry No.,Vendor Ledger Entry No.,Vendor No.,Posting Date,Document Type,Document No. }
  }
  CODE
  {

    PROCEDURE UpdateDebitCredit@47(Correction@1000 : Boolean);
    BEGIN
      IF ((Amount > 0) OR ("Amount (LCY)" > 0)) AND NOT Correction OR
         ((Amount < 0) OR ("Amount (LCY)" < 0)) AND Correction
      THEN BEGIN
        "Debit Amount" := Amount;
        "Credit Amount" := 0;
        "Debit Amount (LCY)" := "Amount (LCY)";
        "Credit Amount (LCY)" := 0;
      END ELSE BEGIN
        "Debit Amount" := 0;
        "Credit Amount" := -Amount;
        "Debit Amount (LCY)" := 0;
        "Credit Amount (LCY)" := -"Amount (LCY)";
      END;
    END;

    PROCEDURE SetZeroTransNo@3(TransactionNo@1000 : Integer);
    VAR
      DtldVendLedgEntry@1001 : Record 380;
      ApplicationNo@1002 : Integer;
    BEGIN
      DtldVendLedgEntry.SETCURRENTKEY("Transaction No.");
      DtldVendLedgEntry.SETRANGE("Transaction No.",TransactionNo);
      IF DtldVendLedgEntry.FINDSET(TRUE) THEN BEGIN
        ApplicationNo := DtldVendLedgEntry."Entry No.";
        REPEAT
          DtldVendLedgEntry."Transaction No." := 0;
          DtldVendLedgEntry."Application No." := ApplicationNo;
          DtldVendLedgEntry.MODIFY;
        UNTIL DtldVendLedgEntry.NEXT = 0;
      END;
    END;

    LOCAL PROCEDURE SetLedgerEntryAmount@1();
    BEGIN
      "Ledger Entry Amount" :=
        NOT (("Entry Type" = "Entry Type"::Application) OR ("Entry Type" = "Entry Type"::"Appln. Rounding"));
    END;

    PROCEDURE GetUnrealizedGainLossAmount@2(EntryNo@1000 : Integer) : Decimal;
    BEGIN
      SETCURRENTKEY("Vendor Ledger Entry No.","Entry Type");
      SETRANGE("Vendor Ledger Entry No.",EntryNo);
      SETRANGE("Entry Type","Entry Type"::"Unrealized Loss","Entry Type"::"Unrealized Gain");
      CALCSUMS("Amount (LCY)");
      EXIT("Amount (LCY)");
    END;

    BEGIN
    END.
  }
}

