OBJECT Page 52113051 CNAB Bank Account FactBox
{
  OBJECT-PROPERTIES
  {
    Date=01/08/13;
    Time=14:33:11;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Bank Account;
               PTB=Conta Banc ria];
    InsertAllowed=No;
    DeleteAllowed=No;
    LinksAllowed=No;
    SourceTable=Table270;
    DataCaptionFields=No.,Name;
    PageType=CardPart;
    OnAfterGetRecord=BEGIN
                       IF CurrentDate <> WORKDATE THEN BEGIN
                         CurrentDate := WORKDATE;
                         DateFilterCalc.CreateAccountingPeriodFilter(BankAccDateFilter[1],BankAccDateName[1],CurrentDate,0);
                         DateFilterCalc.CreateFiscalYearFilter(BankAccDateFilter[2],BankAccDateName[2],CurrentDate,0);
                         DateFilterCalc.CreateFiscalYearFilter(BankAccDateFilter[3],BankAccDateName[3],CurrentDate,-1);
                       END;

                       SETRANGE("Date Filter",0D,CurrentDate);
                       CALCFIELDS(Balance,"Balance (LCY)");

                       FOR i := 1 TO 4 DO BEGIN
                         SETFILTER("Date Filter",BankAccDateFilter[i]);
                         CALCFIELDS("Net Change","Net Change (LCY)");
                         BankAccNetChange[i] := "Net Change";
                         BankAccNetChangeLCY[i] := "Net Change (LCY)";
                       END;
                       SETRANGE("Date Filter",0D,CurrentDate);
                     END;

  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 4   ;1   ;Field     ;
                CaptionML=[ENU=Balance (LCY);
                           PTB=Saldo (ML)];
                SourceExpr="Balance (LCY)";
                AutoFormatType=1 }

  }
  CODE
  {
    VAR
      DateFilterCalc@1000 : Codeunit 358;
      BankAccDateFilter@1001 : ARRAY [4] OF Text[30];
      BankAccDateName@1002 : ARRAY [4] OF Text[30];
      CurrentDate@1003 : Date;
      BankAccNetChange@1004 : ARRAY [4] OF Decimal;
      BankAccNetChangeLCY@1005 : ARRAY [4] OF Decimal;
      i@1006 : Integer;
      Text000@1007 : TextConst 'ENU=Placeholder;PTB=Placeholder';

    BEGIN
    END.
  }
}

