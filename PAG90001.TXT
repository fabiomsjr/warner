OBJECT Page 90001 MappedTables
{
  OBJECT-PROPERTIES
  {
    Date=30/05/16;
    Time=19:43:10;
    Modified=Yes;
    Version List=;
  }
  PROPERTIES
  {
    SourceTable=Table98006;
    PageType=List;
    OnAfterGetRecord=VAR
                       RecRef@1000000000 : RecordRef;
                     BEGIN
                       RecRef.OPEN("From Table ID");
                       RecCount := RecRef.COUNT;
                     END;

  }
  CONTROLS
  {
    { 1000000000;0;Container;
                ContainerType=ContentArea }

    { 1000000001;1;Group  ;
                Name=Group;
                GroupType=Repeater }

    { 1000000002;2;Field  ;
                SourceExpr="From Table ID" }

    { 1000000003;2;Field  ;
                SourceExpr="To Table ID" }

    { 1000000004;2;Field  ;
                SourceExpr=Type }

    { 1000000005;2;Field  ;
                SourceExpr=Skip }

    { 1000000006;2;Field  ;
                SourceExpr="Data Per Company" }

    { 1000000007;2;Field  ;
                SourceExpr="Data Per Company Migrated" }

    { 1000000008;2;Field  ;
                SourceExpr=Migrated }

    { 1000000009;2;Field  ;
                CaptionML=[ENU=Rec Count;
                           ENG=Rec Count];
                SourceExpr=RecCount }

  }
  CODE
  {
    VAR
      RecCount@1000000000 : Integer;

    BEGIN
    END.
  }
}

