OBJECT Page 52112742 Purch. Request Quote Vendors
{
  OBJECT-PROPERTIES
  {
    Date=30/11/13;
    Time=09:12:05;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Vendors;
               PTB=Fornecedores];
    InsertAllowed=No;
    DeleteAllowed=No;
    LinksAllowed=No;
    SourceTable=Table2000000026;
    DataCaptionExpr=STRSUBSTNO(Text001, ReqQuoteNo);
    PageType=List;
    ShowFilter=No;
    OnOpenPage=BEGIN
                 FILTERGROUP(2);
                 Rec.SETRANGE(Number, 1);
                 FILTERGROUP(0);
               END;

    OnAfterGetRecord=BEGIN
                       IF Number = 1 THEN
                         UpdateVars;

                       VendorNo := VendorNos[Number];
                       VendorName := VendorNames[Number];
                     END;

  }
  CONTROLS
  {
    { 35000000;0;Container;
                ContainerType=ContentArea }

    { 52006500;1;Group    ;
                CaptionML=[ENU=Vendors;
                           PTB=Fornecedores];
                GroupType=Repeater }

    { 52006502;2;Field    ;
                Name=VendorNo;
                CaptionML=[ENU=Vendor No. 1;
                           PTB=N� Fornecedor];
                SourceExpr=VendorNo;
                TableRelation=Vendor;
                OnValidate=VAR
                             vendor@52006500 : Record 23;
                           BEGIN
                             IF VendorNo <> '' THEN
                               vendor.GET(VendorNo);
                             VendorNos[Number] := vendor."No.";
                             VendorNames[Number] := vendor.Name;
                             VendorName := vendor.Name;
                           END;
                            }

    { 52006503;2;Field    ;
                Name=VendorName;
                CaptionML=[ENU=Name;
                           PTB=Nome];
                SourceExpr=VendorName;
                Editable=FALSE }

  }
  CODE
  {
    VAR
      VendorNos@52006510 : ARRAY [10] OF Code[20];
      VendorNames@52006511 : ARRAY [10] OF Text;
      Index@52006500 : Integer;
      VendorNo@52006502 : Code[20];
      VendorName@52006501 : Text;
      ReqQuoteNo@52006503 : Code[20];
      Text001@52006504 : TextConst 'ENU=Request Quote %1;PTB=Cota��o Requisi��o %1';

    PROCEDURE SetReqQuoteNo@52006580(no@52006500 : Code[20]);
    BEGIN
      ReqQuoteNo := no;
      UpdateVars;
    END;

    PROCEDURE UpdateVars@52006512();
    VAR
      purchSetup@52006501 : Record 312;
      reqQuote@52006500 : Record 52112730;
    BEGIN
      WITH purchSetup DO BEGIN
        GET;
        TESTFIELD("Request Quote Vendor Limit");
        Rec.FILTERGROUP(2);
        Rec.SETRANGE(Number, 1, "Request Quote Vendor Limit");
        Rec.FILTERGROUP(0);
      END;

      WITH reqQuote DO BEGIN
        GET(ReqQuoteNo);
        CALCFIELDS("Vendor Name 1", "Vendor Name 2", "Vendor Name 3", "Vendor Name 4", "Vendor Name 5",
                   "Vendor Name 6", "Vendor Name 7", "Vendor Name 8", "Vendor Name 9", "Vendor Name 10");
        VendorNos[1] := "Vendor No. 1";
        VendorNames[1] := "Vendor Name 1";
        VendorNos[2] := "Vendor No. 2";
        VendorNames[2] := "Vendor Name 2";
        VendorNos[3] := "Vendor No. 3";
        VendorNames[3] := "Vendor Name 3";
        VendorNos[4] := "Vendor No. 4";
        VendorNames[4] := "Vendor Name 4";
        VendorNos[5] := "Vendor No. 5";
        VendorNames[5] := "Vendor Name 5";
        VendorNos[6] := "Vendor No. 6";
        VendorNames[6] := "Vendor Name 6";
        VendorNos[7] := "Vendor No. 7";
        VendorNames[7] := "Vendor Name 7";
        VendorNos[8] := "Vendor No. 8";
        VendorNames[8] := "Vendor Name 8";
        VendorNos[9] := "Vendor No. 9";
        VendorNames[9] := "Vendor Name 9";
        VendorNos[10] := "Vendor No. 10";
        VendorNames[10] := "Vendor Name 10";
      END;
    END;

    PROCEDURE UpdateReqQuote@52006500(VAR reqQuote@52006500 : Record 52112730);
    VAR
      i@52006501 : Integer;
      y@52006503 : Integer;
      vendors@52006502 : ARRAY [10] OF Code[20];
    BEGIN
      FOR i := 1 TO 10 DO
        IF VendorNos[i] <> '' THEN BEGIN
          y += 1;
          vendors[y] := VendorNos[i];
        END;

      reqQuote.VALIDATE("Vendor No. 1", vendors[1]);
      reqQuote.VALIDATE("Vendor No. 2", vendors[2]);
      reqQuote.VALIDATE("Vendor No. 3", vendors[3]);
      reqQuote.VALIDATE("Vendor No. 4", vendors[4]);
      reqQuote.VALIDATE("Vendor No. 5", vendors[5]);
      reqQuote.VALIDATE("Vendor No. 6", vendors[6]);
      reqQuote.VALIDATE("Vendor No. 7", vendors[7]);
      reqQuote.VALIDATE("Vendor No. 8", vendors[8]);
      reqQuote.VALIDATE("Vendor No. 9", vendors[9]);
      reqQuote.VALIDATE("Vendor No. 10", vendors[10]);
    END;

    BEGIN
    END.
  }
}

