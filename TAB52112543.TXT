OBJECT Table 52112543 EFD Analytic Document Entry
{
  OBJECT-PROPERTIES
  {
    Date=01/06/13;
    Time=15:10:41;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=PTB=Registro Anal�tico Documento;
  }
  FIELDS
  {
    { 1   ;   ;Tax Settlement No.  ;Code20        ;CaptionML=[ENU=Tax Settlement Code;
                                                              PTB=C�d. Apura��o Imp.] }
    { 2   ;   ;Document ID         ;Integer        }
    { 3   ;   ;CST Code            ;Code10        ;CaptionML=PTB=C�d. CST }
    { 4   ;   ;CFOP Code           ;Code10        ;CaptionML=[ENU=CFOP Code;
                                                              PTB=C�d. CFOP] }
    { 5   ;   ;ICMS %              ;Decimal       ;CaptionML=[ENU=ICMS %;
                                                              PTB=% ICMS] }
    { 6   ;   ;G/L Amount          ;Decimal       ;CaptionML=[ENU=G/L Amount;
                                                              PTB=Valor Cont�bil] }
    { 7   ;   ;ICMS Base Amount    ;Decimal       ;CaptionML=[ENU=ICMS Basis Amount;
                                                              PTB=Valor Base ICMS] }
    { 8   ;   ;ICMS Exempt Amount  ;Decimal       ;CaptionML=[ENU=ICMS Exempt Amount;
                                                              PTB=Valor ICMS Isento] }
    { 9   ;   ;ICMS Others Amount  ;Decimal       ;CaptionML=[ENU=ICMS Others Amount;
                                                              PTB=Valor ICMS Outros] }
    { 10  ;   ;IPI Base Amount     ;Decimal       ;CaptionML=[ENU=IPI Basis Amount;
                                                              PTB=Valor Base IPI] }
    { 11  ;   ;IPI Exempt Amount   ;Decimal       ;CaptionML=[ENU=IPI Exempt Amount;
                                                              PTB=Valor IPI Isento] }
    { 12  ;   ;IPI Others Amount   ;Decimal       ;CaptionML=[ENU=IPI Others Amount;
                                                              PTB=Valor IPI Outros] }
    { 13  ;   ;ICMS Amount         ;Decimal       ;CaptionML=[ENU=ICMS Amount;
                                                              PTB=Valor ICMS] }
    { 14  ;   ;IPI Amount          ;Decimal       ;CaptionML=[ENU=IPI Amount;
                                                              PTB=Valor IPI] }
    { 18  ;   ;Fiscal Book         ;Boolean       ;CaptionML=[ENU=Fiscal Book;
                                                              PTB=Livro Fiscal] }
    { 19  ;   ;ICMS ST Base Amount ;Decimal        }
    { 20  ;   ;ICMS ST Amount      ;Decimal        }
    { 60  ;   ;ICMS Reduction %    ;Decimal        }
    { 62  ;   ;ICMS Reduction Base ;Decimal        }
    { 70  ;   ;ST Base Amount      ;Decimal       ;CaptionML=[ENU=ST Base Amount;
                                                              PTB=Valor Base ST] }
    { 72  ;   ;ST Amount           ;Decimal       ;CaptionML=[ENU=ST Amount;
                                                              PTB=Valor ST] }
  }
  KEYS
  {
    {    ;Tax Settlement No.,Document ID,CST Code,CFOP Code,ICMS %;
                                                   Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

