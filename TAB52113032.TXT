OBJECT Table 52113032 CNAB Charge Instruction
{
  OBJECT-PROPERTIES
  {
    Date=31/10/14;
    Time=11:34:19;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Charge Instruction;
               PTB=Instru��o Cobran�a];
  }
  FIELDS
  {
    { 1   ;   ;Code                ;Code3         ;CaptionML=[ENU=Code;
                                                              PTB=C�digo];
                                                   NotBlank=Yes }
    { 2   ;   ;Description         ;Text60        ;CaptionML=[ENU=Description;
                                                              PTB=Descri��o];
                                                   Title=Yes }
  }
  KEYS
  {
    {    ;Code                                    ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    {
      --- THBR2.82 ---
      thiagoAS,101209, Changed bank layout

      --- THBR147.00 ---
      rafaelr,130411,Electronic Receipts from NAV 3.7 to 5.0
    }
    END.
  }
}

