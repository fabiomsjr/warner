OBJECT Table 52112927 DELETED DIRF Header
{
  OBJECT-PROPERTIES
  {
    Date=01/01/13;
    Time=09:45:59;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    DataCaptionFields=Code;
    OnInsert=BEGIN
               IF Code = '' THEN BEGIN
                 GeneralSetup.GET;
                 GeneralSetup.TESTFIELD("DIRF Nos.");
                 NoSeriesMgt.InitSeries(GeneralSetup."DIRF Nos.",xRec."Serie No.",0D,Code,"Serie No.");
               END;


               CompanyInformation.GET;
               "Company C.N.P.J." := FillWith(CompanyInformation."C.N.P.J.",MAXSTRLEN("Company C.N.P.J."),FALSE,TRUE,FALSE);
               "Company Name" := FillWith(CompanyInformation.Name + ' ' + CompanyInformation."Name 2",MAXSTRLEN("Company Name"),FALSE,TRUE,FALSE)
               ;

               //"Valor Minimo Reten��o IR" := GeneralSetup."IRRF Calc. Min. Amount" + 0.01;
             END;

    OnDelete=BEGIN
               DIRFLines.RESET;
               DIRFLines.SETRANGE("DIRF Header Code",Code);
               DIRFLines.DELETEALL;
             END;

    LookupPageID=Page52112930;
    DrillDownPageID=Page52112930;
  }
  FIELDS
  {
    { 1   ;   ;Code                ;Code10        ;OnValidate=BEGIN
                                                                IF Code <> xRec.Code THEN BEGIN
                                                                  GeneralSetup.GET;
                                                                  NoSeriesMgt.TestManual(GeneralSetup."DIRF Nos.");
                                                                  "Serie No." := '';
                                                                END;
                                                              END;

                                                   CaptionML=[ENU=Code;
                                                              PTB=C�digo] }
    { 2   ;   ;Company C.N.P.J.    ;Code14        ;CaptionML=[ENU=Company C.N.P.J.;
                                                              PTB=C.N.P.J. Empresa] }
    { 3   ;   ;Fiscal Year         ;Integer       ;OnValidate=BEGIN
                                                                IF "Fiscal Year" <> xRec."Fiscal Year" THEN BEGIN
                                                                  DIRFLines.RESET;
                                                                  DIRFLines.SETRANGE("DIRF Header Code",Code);
                                                                  IF DIRFLines.FIND('-') THEN
                                                                    ERROR('It is not allowed to modify this field.\' +
                                                                          'Please, delete before the lines.');
                                                                END;
                                                              END;

                                                   CaptionML=[ENU=Fiscal Year;
                                                              PTB=Ano Calend�rio];
                                                   MaxValue=3000 }
    { 4   ;   ;DIRF Type           ;Option        ;CaptionML=[ENU=DIRF Type;
                                                              PTB=Tipo de DIRF];
                                                   OptionCaptionML=[ENU=" ,O - Original,R - Modification";
                                                                    PTB=" ,O - Original,R - Modifica��o"];
                                                   OptionString=[ ,O - Original,R - Modification] }
    { 6   ;   ;Declarant Type      ;Option        ;CaptionML=[ENU=Declarant Type;
                                                              PTB=Tipo Declarante];
                                                   OptionCaptionML=[ENU=" ,1. Person,2. Company";
                                                                    PTB=" ,1. Pessoa F�sica,2. Pessoa Jur�dica"];
                                                   OptionString=[ ,1. Person,2. Company] }
    { 7   ;   ;Nature of the Company;Option       ;CaptionML=[ENU=Nature of the Company;
                                                              PTB=Natureza da Empresa];
                                                   OptionCaptionML=[ENU=0.Pessoa f�sica ou Jur�dica,1.�rg�os ou Autarquias e Funda��es Federais de Administra��o P�blica,2.�rg�os ou Autarquias e Funda��es Municipais ou Estaduais de Administra��o P�blica,3.Institui��o Administrativa de Fundos ou Clube de Investimento;
                                                                    PTB=0.Person or Company,1.Federal Public Administration,2.State Public Administration,3.Club of Investments Administration];
                                                   OptionString=0.Person or Company,1.Federal Public Administration,2.State Public Administration,3.Club of Investments Administration }
    { 8   ;   ;Type of Income/Tax  ;Option        ;CaptionML=[ENU=Type of Income/Tax;
                                                              PTB=Tipo de Rendimento/Imposto];
                                                   OptionCaptionML=[ENU=0. Profit/Tax/Deduction,1. Lucro/Imposto sem resgate/ Compensa��o por virtude de decis�o judicial / dep�sito judicial;
                                                                    PTB=0. Lucro/Imposto/Dedu��o,1. Lucro/Imposto sem resgate/ Compensa��o por virtude de decis�o judicial / dep�sito judicial];
                                                   OptionString=0. Profit/Tax/Deduction,1. Lucro/Imposto sem resgate/ Compensa��o por virtude de decis�o judicial / dep�sito judicial }
    { 9   ;   ;Reference Year      ;Integer       ;CaptionML=[ENU=Reference Year;
                                                              PTB=Ano Refer�ncia];
                                                   MaxValue=3000 }
    { 10  ;   ;Company Name        ;Text60        ;CaptionML=[ENU=Company Name;
                                                              PTB=Nome Empresa] }
    { 11  ;   ;Responsible CPF/CNPJ;Code11        ;CaptionML=[ENU=Responsible CPF/CNPJ;
                                                              PTB=CPF/CNPJ Respons�vel Perante] }
    { 12  ;   ;Delivery Responsible CNPJ;Code14   ;CaptionML=[ENU=Delivery Responsible CNPJ;
                                                              PTB=CNPJ Respons�vel Entrega] }
    { 13  ;   ;Fulfilling Responsible CPF;Code11  ;CaptionML=[ENU=Fulfilling Responsible CPF;
                                                              PTB=CPF Repons�vel Preenchimento] }
    { 14  ;   ;Responsible Name    ;Text60        ;CaptionML=[ENU=Responsible Name;
                                                              PTB=Nome Roespons�vel] }
    { 15  ;   ;Responsible DDD     ;Code4         ;CaptionML=[ENU=Responsible DDD;
                                                              PTB=DDD Respons�vel] }
    { 16  ;   ;Responsible Phone No.;Code8        ;CaptionML=[ENU=Responsible Phone No.;
                                                              PTB=Telefone do Respons�vel] }
    { 17  ;   ;Responsible Phone Branch No.;Code6 ;CaptionML=[ENU=Responsible Phone Branch No.;
                                                              PTB=Ramal do Respons�vel] }
    { 18  ;   ;Responsible Fax No. ;Code8         ;CaptionML=[ENU=Responsible Fax No.;
                                                              PTB=Fax do Respons�vel] }
    { 19  ;   ;Responsible E-mail  ;Text50        ;CaptionML=[ENU=Responsible E-mail;
                                                              PTB=E-mail do Respons�vel] }
    { 20  ;   ;For Declarant Use   ;Text13        ;CaptionML=[ENU=For Declarant Use;
                                                              PTB=Para Uso do Declarante] }
    { 21  ;   ;Retention Code      ;Code4         ;TableRelation="DELETED Retention Code";
                                                   CaptionML=[ENU=Retention Code;
                                                              PTB=C�d. de Reten��o] }
    { 22  ;   ;Serie No.           ;Code10        ;TableRelation="No. Series";
                                                   CaptionML=[ENU=Serie No.;
                                                              PTB=No.S�rie] }
    { 23  ;   ;Valor Minimo Retencao IR;Decimal   ;CaptionML=[ENU=Minimum  Retention IR Amount;
                                                              PTB=Valor Minimo Reten��o IR] }
    { 24  ;   ;Declaring Depositary of Credit;Boolean;
                                                   CaptionML=[ENU=Declaring Depositary of Credit;
                                                              PTB=Declarante Deposit�rio Cr�dito] }
    { 25  ;   ;Event Date          ;Date          ;CaptionML=[ENU=Event Date;
                                                              PTB=Data Evento] }
    { 26  ;   ;Event Type          ;Option        ;CaptionML=[ENU=Event Type;
                                                              PTB=Tipo de Evento];
                                                   OptionCaptionML=[ENU=" ,1. Finished,2. Ending";
                                                                    PTB=" ,1. Encerramento de Esp�lio,2. Sa�da Definitiva do Pa�s"];
                                                   OptionString=[ ,1. Finished,2. Ending] }
    { 27  ;   ;Last Declaration Receipt No.;Text12;CaptionML=[ENU=Last Declaration Receipt No.;
                                                              PTB=N� Recibo Ult. Decl. Entregue] }
    { 28  ;   ;Responsable-Ostensible Partner;Boolean;
                                                   CaptionML=PTB=Respons�vel-S�cio Ostensivo }
    { 29  ;   ;Income paid to resident abroad;Boolean;
                                                   CaptionML=PTB=Rendimentos pagos a domiciliados no exterior }
    { 30  ;   ;Payment to Health Plan;Boolean     ;CaptionML=PTB=Pagamento a Plano de Sa�de }
    { 31  ;   ;Seeling             ;Decimal       ;CaptionML=PTB=Teto Salarial }
  }
  KEYS
  {
    {    ;Code                                    ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      GeneralSetup@1000000001 : Record 98;
      CompanyInformation@1000000002 : Record 79;
      DIRFLines@1000000006 : Record 52112928;
      NoSeriesMgt@1000000007 : Codeunit 396;
      DIRF@1102300000 : Record 52112927;

    PROCEDURE FillWith@1102300001(Value@1000000000 : Text[250];Length@1000000001 : Integer;Caracteres@1102300000 : Boolean;Rigth@1102300001 : Boolean;Zeros@1102300002 : Boolean) : Text[250];
    VAR
      Aux@1000000002 : Text[1];
      I@1000000003 : Integer;
      Text@1000000004 : Text[250];
      Teste@1000000005 : Integer;
      ExtitValue_Aux@1102300003 : Text[1];
    BEGIN
      IF Value='' THEN
        Value:=' ';

      IF Zeros THEN
        ExtitValue_Aux := '0'
      ELSE
        ExtitValue_Aux := ' ';


      Value := COPYSTR(Value,1,STRLEN(Value));
      I := 0;

      REPEAT
        I := I + 1;
        Aux := COPYSTR(Value,I,1);

        CASE Caracteres OF
          TRUE:
            BEGIN
              Text := Text + Aux;
            END;
          FALSE:
            BEGIN
              IF NOT (Aux IN ['.',',','-','(',')','/','/','_','[',']','{','}','"','?','!','@','#','$','%','&','*',' ']) THEN
                Text := Text + Aux;
            END;
        END;
      UNTIL I=STRLEN(Value);

      Text := COPYSTR(Text,1,Length);

      IF Text = ' ' THEN
        Text := '';


      IF Rigth THEN
        Value := Text + PADSTR('',Length-STRLEN(Text),ExtitValue_Aux)
      ELSE
        Value := PADSTR('',Length-STRLEN(Text),ExtitValue_Aux) + Text;

      EXIT(Value);
    END;

    PROCEDURE AssistEdit@2(OldDIRF@1000000000 : Record 52112927) : Boolean;
    BEGIN
      WITH DIRF DO BEGIN
        DIRF := Rec;
        GeneralSetup.GET;
        GeneralSetup.TESTFIELD("DIRF Nos.");
        IF NoSeriesMgt.SelectSeries(GeneralSetup."DIRF Nos.",OldDIRF."Serie No.","Serie No.") THEN BEGIN
          GeneralSetup.GET;
          GeneralSetup.TESTFIELD("DIRF Nos.");
          NoSeriesMgt.SetSeries(Code);
          Rec := DIRF;
          EXIT(TRUE);
        END;
      END;
    END;

    BEGIN
    END.
  }
}

