OBJECT Page 52112623 NFS Service Codes
{
  OBJECT-PROPERTIES
  {
    Date=20/07/15;
    Time=09:42:19;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=NFS Service Codes;
               PTB=C�digos de Servi�o NFS];
    SourceTable=Table52112623;
    PageType=List;
    ActionList=ACTIONS
    {
      { 52006500;    ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 52006501;1   ;Action    ;
                      CaptionML=[ENU=Codes by Branch;
                                 PTB=C�digos por Filial];
                      RunObject=Page 52112646;
                      RunPageLink=NFS Service Code=FIELD(Code);
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=ServiceCode;
                      PromotedCategory=Process }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1102300000;1;Group  ;
                GroupType=Repeater }

    { 1102300001;2;Field  ;
                SourceExpr=Code }

    { 1102300003;2;Field  ;
                SourceExpr=Description }

    { 1102300016;2;Field  ;
                SourceExpr="Tributation Code" }

    { 35000000;2;Field    ;
                SourceExpr="Service Item Code" }

    { 52006503;2;Field    ;
                SourceExpr="CNAE Code" }

    { 52006508;2;Field    ;
                SourceExpr="NBS Code" }

    { 52006504;2;Field    ;
                SourceExpr="IBPT Code Type" }

    { 52006505;2;Field    ;
                SourceExpr="IBPT Code" }

    { 52006506;2;Field    ;
                SourceExpr="Construction Code" }

    { 52006507;2;Field    ;
                SourceExpr="ART Code" }

    { 52006502;2;Field    ;
                SourceExpr="Codes by Branch" }

  }
  CODE
  {

    BEGIN
    END.
  }
}

