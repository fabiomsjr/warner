OBJECT Table 99008522 BizTalk Doc. Receipt Check
{
  OBJECT-PROPERTIES
  {
    Date=29/02/12;
    Time=18:14:20;
    Version List=NAVW16.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=BizTalk Doc. Receipt Check;
               PTB=Verf. Rec. Doc. BizTalk];
  }
  FIELDS
  {
    { 1   ;   ;Doc. Receipt ID     ;Integer       ;CaptionML=[ENU=Doc. Receipt ID;
                                                              PTB=ID Recep��o Doc.] }
    { 2   ;   ;Company             ;Text30        ;CaptionML=[ENU=Company;
                                                              PTB=Empresa] }
    { 3   ;   ;Document Received   ;Boolean       ;CaptionML=[ENU=Document Received;
                                                              PTB=Documento Recebido] }
  }
  KEYS
  {
    {    ;Doc. Receipt ID                         ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

