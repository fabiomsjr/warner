OBJECT Table 5636 Insurance Register
{
  OBJECT-PROPERTIES
  {
    Date=07/09/12;
    Time=12:00:00;
    Version List=NAVW17.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Insurance Register;
               PTB=Registro Mov. Seguro];
    LookupPageID=Page5656;
  }
  FIELDS
  {
    { 1   ;   ;No.                 ;Integer       ;CaptionML=[ENU=No.;
                                                              PTB=N�] }
    { 2   ;   ;From Entry No.      ;Integer       ;TableRelation="Ins. Coverage Ledger Entry";
                                                   CaptionML=[ENU=From Entry No.;
                                                              PTB=Do N� Mov.] }
    { 3   ;   ;To Entry No.        ;Integer       ;TableRelation="Ins. Coverage Ledger Entry";
                                                   CaptionML=[ENU=To Entry No.;
                                                              PTB=At� N� Mov.] }
    { 4   ;   ;Creation Date       ;Date          ;CaptionML=[ENU=Creation Date;
                                                              PTB=Data Cria��o] }
    { 5   ;   ;Source Code         ;Code10        ;TableRelation="Source Code";
                                                   CaptionML=[ENU=Source Code;
                                                              PTB=Cod. Origem] }
    { 6   ;   ;User ID             ;Code50        ;TableRelation=User."User Name";
                                                   OnLookup=VAR
                                                              UserMgt@1000 : Codeunit 418;
                                                            BEGIN
                                                              UserMgt.LookupUserID("User ID");
                                                            END;

                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=User ID;
                                                              PTB=ID Usu�rio] }
    { 7   ;   ;Journal Batch Name  ;Code10        ;CaptionML=[ENU=Journal Batch Name;
                                                              PTB=Nome Se��o Di�rio] }
  }
  KEYS
  {
    {    ;No.                                     ;Clustered=Yes }
    {    ;Creation Date                            }
    {    ;Source Code,Journal Batch Name,Creation Date }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

