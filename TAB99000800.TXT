OBJECT Table 99000800 Sales Planning Line
{
  OBJECT-PROPERTIES
  {
    Date=27/11/14;
    Time=12:00:00;
    Version List=NAVW18.00.00.38798;
  }
  PROPERTIES
  {
    DataCaptionFields=Sales Order No.;
    CaptionML=[ENU=Sales Planning Line;
               PTB=Linha Planejamento Venda];
  }
  FIELDS
  {
    { 1   ;   ;Sales Order No.     ;Code20        ;TableRelation="Sales Header".No. WHERE (Document Type=CONST(Order));
                                                   CaptionML=[ENU=Sales Order No.;
                                                              PTB=N� Pedido Venda] }
    { 2   ;   ;Sales Order Line No.;Integer       ;TableRelation="Sales Line"."Line No." WHERE (Document Type=CONST(Order),
                                                                                                Document No.=FIELD(Sales Order No.));
                                                   CaptionML=[ENU=Sales Order Line No.;
                                                              PTB=N� Lin. Pedido Venda] }
    { 3   ;   ;Item No.            ;Code20        ;TableRelation=Item;
                                                   OnValidate=VAR
                                                                Item@1000 : Record 27;
                                                              BEGIN
                                                                Item.GET("Item No.");
                                                                "Low-Level Code" := Item."Low-Level Code";
                                                              END;

                                                   CaptionML=[ENU=Item No.;
                                                              PTB=No. Produto] }
    { 4   ;   ;Description         ;Text50        ;CaptionML=[ENU=Description;
                                                              PTB=Descri��o] }
    { 5   ;   ;Shipment Date       ;Date          ;CaptionML=[ENU=Shipment Date;
                                                              PTB=Data Envio] }
    { 6   ;   ;Available           ;Decimal       ;CaptionML=[ENU=Available;
                                                              PTB=Dispon�vel];
                                                   DecimalPlaces=0:5 }
    { 7   ;   ;Next Planning Date  ;Date          ;CaptionML=[ENU=Next Planning Date;
                                                              PTB=Pr�xima Data de Planejamento] }
    { 8   ;   ;Expected Delivery Date;Date        ;CaptionML=[ENU=Expected Delivery Date;
                                                              PTB=Data Esperada de Entrega] }
    { 9   ;   ;Planning Status     ;Option        ;CaptionML=[ENU=Planning Status;
                                                              PTB=Status Planejamento];
                                                   OptionCaptionML=[ENU=None,Simulated,Planned,Firm Planned,Released,Inventory;
                                                                    PTB=Nenhuma,Simulada,Planejada,Planejada Firme,Lan�ada,Invent�rio];
                                                   OptionString=None,Simulated,Planned,Firm Planned,Released,Inventory }
    { 10  ;   ;Needs Replanning    ;Boolean       ;CaptionML=[ENU=Needs Replanning;
                                                              PTB=Necessita Replanejamento] }
    { 11  ;   ;Variant Code        ;Code10        ;TableRelation="Item Variant".Code WHERE (Item No.=FIELD(Item No.),
                                                                                            Code=FIELD(Variant Code));
                                                   CaptionML=[ENU=Variant Code;
                                                              PTB=C�digo Variante] }
    { 12  ;   ;Planned Quantity    ;Decimal       ;CaptionML=[ENU=Planned Quantity;
                                                              PTB=Quantidade Planejada];
                                                   DecimalPlaces=0:5 }
    { 15  ;   ;Low-Level Code      ;Integer       ;CaptionML=[ENU=Low-Level Code;
                                                              PTB=Cod. do N�vel Abaixo];
                                                   Editable=No }
  }
  KEYS
  {
    {    ;Sales Order No.,Sales Order Line No.    ;Clustered=Yes }
    {    ;Low-Level Code                           }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

