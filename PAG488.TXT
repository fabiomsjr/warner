OBJECT Page 488 Column Layout Names
{
  OBJECT-PROPERTIES
  {
    Date=04/05/16;
    Time=15:34:46;
    Modified=Yes;
    Version List=NAVW17.00,WCMW1601_02_2;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Column Layout Names;
               PTB=Nomes Formato Colunas];
    SourceTable=Table333;
    PageType=List;
    ActionList=ACTIONS
    {
      { 1102300005;  ;ActionContainer;
                      Name=<Action1000000003>;
                      ActionContainerType=ActionItems }
      { 1102300004;1 ;ActionGroup;
                      Name=<Action1000000004>;
                      CaptionML=ENU=F&unctions;
                      ActionContainerType=NewDocumentItems;
                      Image=GL }
      { 1102300003;2 ;Action    ;
                      Name=<Action1000000104>;
                      CaptionML=ENU=Column Name & Layout;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=GL;
                      PromotedCategory=Category4;
                      OnAction=VAR
                                 Var_ColumnLayoutName@1000000000 : Record 333;
                                 Var_Utilities@1000000001 : Codeunit 60000;
                               BEGIN
                                 // -002
                                 CurrPage.SETSELECTIONFILTER(Var_ColumnLayoutName);
                                 Var_Utilities.UpdateColumnNamesandLayout(Var_ColumnLayoutName);
                                 // +002
                               END;
                                }
      { 5       ;2   ;Action    ;
                      Name=EditColumnLayoutSetup;
                      Ellipsis=Yes;
                      CaptionML=ENU=Edit Column Layout Setup;
                      Promoted=Yes;
                      Image=SetupColumns;
                      PromotedCategory=Process;
                      OnAction=VAR
                                 ColumnLayout@1000 : Page 489;
                               BEGIN
                                 ColumnLayout.SetColumnLayoutName(Name);
                                 ColumnLayout.RUN;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                SourceExpr=Name }

    { 4   ;2   ;Field     ;
                SourceExpr=Description }

    { 9   ;2   ;Field     ;
                SourceExpr="Analysis View Name" }

    { 1102300002;2;Field  ;
                SourceExpr="Source Company";
                Editable=FALSE }

    { 1102300001;2;Field  ;
                SourceExpr="Sync Date" }

    { 1102300000;2;Field  ;
                SourceExpr="Sync User" }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    {
      001  01/06/10  JJB  WCMFSD020  Add new fields to the page to indicate sync data
                                     - "Source Company"
                                     - "Sync Date"
                                     - "Sync User"
      002  01/06/10  JJB  WCMFSD020  Add new Action to sync Column Layout Name
    }
    END.
  }
}

