OBJECT Table 35000635 EFD Register Totals2
{
  OBJECT-PROPERTIES
  {
    Date=29/02/12;
    Time=14:47:33;
    Version List=FNX023.00;
  }
  PROPERTIES
  {
    OnDelete=VAR
               USText001@1240470002 : TextConst 'ENU=The Unit of Measure - %1  is attached to a Payroll Rate record and therefore cannot be deleted.;ESM=La unidad de medida - %1  est� asociada a un registro de retribuci�n y no puede eliminarse.;FRC=L''unit� de mesure - %1 est associ�e � un enregistrement de type Taux salarial et ne peut pas �tre supprim�e.;ENC=The Unit of Measure - %1  is attached to a Payroll Rate record and therefore cannot be deleted.';
             BEGIN
               UnitOfMeasureTranslation.SETRANGE(Code,"Tax Settlement No.");
               UnitOfMeasureTranslation.DELETEALL;
             END;

    CaptionML=[ENU=Register Totals;
               PTB=Total Registros];
  }
  FIELDS
  {
    { 1   ;   ;Tax Settlement No.  ;Code20        ;CaptionML=[ENU=No.;
                                                              PTB=No.];
                                                   Description=Geral }
    { 3   ;   ;Code                ;Code10        ;CaptionML=[ENU=Code;
                                                              PTB=C�digo;
                                                              ESM=C�digo;
                                                              FRC="Code  ";
                                                              ENC=Code];
                                                   NotBlank=Yes }
    { 4   ;   ;Quantity            ;Integer       ;CaptionML=[ENU=Description;
                                                              PTB=Descri��o;
                                                              ESM=Descripci�n;
                                                              FRC=Description;
                                                              ENC=Description] }
  }
  KEYS
  {
    {    ;Tax Settlement No.,Code                 ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      UnitOfMeasureTranslation@1000 : Record 5402;

    BEGIN
    {
      MBS/BR

      No.   dd.mm.yy   Developer   Company   DocNo.   Description
      -----------------------------------------------------------------------------------------------------------------
      00    01.12.06   MK          MICROSOFT LOC001   The reference Documentation file LOCALIZATIONBR.DOC
    }
    END.
  }
}

