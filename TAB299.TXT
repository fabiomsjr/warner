OBJECT Table 299 Reminder Comment Line
{
  OBJECT-PROPERTIES
  {
    Date=07/09/12;
    Time=12:00:00;
    Version List=NAVW17.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Reminder Comment Line;
               PTB=Linha coment. carta aviso];
    LookupPageID=Page443;
    DrillDownPageID=Page443;
  }
  FIELDS
  {
    { 1   ;   ;Type                ;Option        ;CaptionML=[ENU=Type;
                                                              PTB=Tipo];
                                                   OptionCaptionML=[ENU=Reminder,Issued Reminder;
                                                                    PTB=Carta Aviso,Carta Aviso Emitida];
                                                   OptionString=Reminder,Issued Reminder }
    { 2   ;   ;No.                 ;Code20        ;TableRelation=IF (Type=CONST(Reminder)) "Reminder Header"
                                                                 ELSE IF (Type=CONST(Issued Reminder)) "Issued Reminder Header";
                                                   CaptionML=[ENU=No.;
                                                              PTB=N�];
                                                   NotBlank=Yes }
    { 3   ;   ;Line No.            ;Integer       ;CaptionML=[ENU=Line No.;
                                                              PTB=N� Linha] }
    { 4   ;   ;Date                ;Date          ;CaptionML=[ENU=Date;
                                                              PTB=Data] }
    { 5   ;   ;Code                ;Code10        ;CaptionML=[ENU=Code;
                                                              PTB=C�digo] }
    { 6   ;   ;Comment             ;Text80        ;CaptionML=[ENU=Comment;
                                                              PTB=Coment�rio] }
  }
  KEYS
  {
    {    ;Type,No.,Line No.                       ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    PROCEDURE SetUpNewLine@1();
    VAR
      ReminderCommentLine@1000 : Record 299;
    BEGIN
      ReminderCommentLine.SETRANGE(Type,Type);
      ReminderCommentLine.SETRANGE("No.","No.");
      ReminderCommentLine.SETRANGE(Date,WORKDATE);
      IF NOT ReminderCommentLine.FINDFIRST THEN
        Date := WORKDATE;
    END;

    BEGIN
    END.
  }
}

