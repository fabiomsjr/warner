OBJECT Codeunit 52112483 Fiscal Codes Mgt
{
  OBJECT-PROPERTIES
  {
    Date=12/08/15;
    Time=13:50:50;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    OnRun=BEGIN
          END;

  }
  CODE
  {

    PROCEDURE UpdateSalesLineCST@52006503(VAR SalesHeader@52006503 : Record 36;VAR SalesLine@52006500 : Record 37);
    VAR
      salesLineCopy@52006501 : Record 37;
      cstPriority@52006502 : Record 52112502;
    BEGIN
      WITH SalesLine DO BEGIN
        salesLineCopy.COPY(SalesLine);
        ClearCSTCodes("ICMS CST Code", "IPI CST Code", "PIS CST Code", "COFINS CST Code");
        UpdateSalesLineCSTByTaxArea(SalesLine);
        UpdateSalesLineCSTByNCM(SalesLine);
        UpdateSalesLineCSTByException(SalesLine);
        ApplySalesLineTaxesMatrixForCSTs(SalesHeader, SalesLine);
        RecoverOldCSTCodesIfNewerAreBlank(salesLineCopy."ICMS CST Code", salesLineCopy."IPI CST Code", salesLineCopy."PIS CST Code", salesLineCopy."COFINS CST Code",
                                          "ICMS CST Code", "IPI CST Code", "PIS CST Code", "COFINS CST Code");
      END;
    END;

    PROCEDURE UpdateSalesLineCSTByNCM@52006502(VAR salesLine@52006500 : Record 37);
    VAR
      salesHeader@52006501 : Record 36;
      compInfo@52006502 : Record 79;
      cust@52006506 : Record 18;
      brTaxCalc@52006503 : Codeunit 52112426;
      ncmCodeTaxPerc@52006504 : Record 52112428;
      isOutbound@52006505 : Boolean;
      territoryCode@52006507 : Code[10];
    BEGIN
      WITH salesLine DO BEGIN
        IF "NCM Code" = '' THEN
          EXIT;

        salesHeader.GET("Document Type", "Document No.");
        compInfo.GetCompanyBranchInfo(salesHeader."Branch Code");
        cust.GET(salesHeader."Bill-to Customer No.");
        IF cust."Not ICMS Contributor" THEN
          territoryCode := compInfo."Territory Code"
        ELSE
          territoryCode := salesHeader."Bill-to Territory Code";

        isOutbound := NOT ("Document Type" IN ["Document Type"::"Credit Memo", "Document Type"::"Return Order"]);

        ncmCodeTaxPerc.ApplyCSTCode("ICMS CST Code", ncmCodeTaxPerc."Tax Identification"::ICMS, "NCM Code", "NCM Exception Code",
                                    territoryCode, salesHeader."Posting Date", isOutbound);
        ncmCodeTaxPerc.ApplyCSTCode("ICMS CST Code", ncmCodeTaxPerc."Tax Identification"::ST, "NCM Code", "NCM Exception Code",
                                    territoryCode, salesHeader."Posting Date", isOutbound);
        ncmCodeTaxPerc.ApplyCSTCode("IPI CST Code", ncmCodeTaxPerc."Tax Identification"::IPI, "NCM Code", "NCM Exception Code",
                                    territoryCode, salesHeader."Posting Date", isOutbound);
        ncmCodeTaxPerc.ApplyCSTCode("PIS CST Code", ncmCodeTaxPerc."Tax Identification"::PIS, "NCM Code", "NCM Exception Code",
                                    territoryCode, salesHeader."Posting Date", isOutbound);
        ncmCodeTaxPerc.ApplyCSTCode("COFINS CST Code", ncmCodeTaxPerc."Tax Identification"::COFINS, "NCM Code", "NCM Exception Code",
                                    territoryCode, salesHeader."Posting Date", isOutbound);
      END;
    END;

    PROCEDURE UpdateSalesLineCSTByException@52006507(VAR salesLine@52006500 : Record 37);
    VAR
      salesHeader@52006501 : Record 36;
      taxExcepPerc@52006504 : Record 52112474;
      isOutbound@52006505 : Boolean;
      compInfo@52006507 : Record 79;
      cust@52006506 : Record 18;
      brTaxCalc@52006503 : Codeunit 52112426;
      territoryCode@52006502 : Code[10];
    BEGIN
      WITH salesLine DO BEGIN
        IF "Tax Exception Code" = '' THEN
          EXIT;

        salesHeader.GET("Document Type", "Document No.");
        compInfo.GetCompanyBranchInfo(salesHeader."Branch Code");
        cust.GET(salesHeader."Bill-to Customer No.");
        IF cust."Not ICMS Contributor" THEN
          territoryCode := compInfo."Territory Code"
        ELSE
          territoryCode := salesHeader."Bill-to Territory Code";
        isOutbound := NOT ("Document Type" IN ["Document Type"::"Credit Memo", "Document Type"::"Return Order"]);

        taxExcepPerc.ApplyCSTCode("ICMS CST Code", taxExcepPerc."Tax Identification"::ICMS, "Tax Exception Code", territoryCode,
                                  salesHeader."Posting Date", isOutbound);
        taxExcepPerc.ApplyCSTCode("ICMS CST Code", taxExcepPerc."Tax Identification"::ST, "Tax Exception Code", territoryCode,
                                  salesHeader."Posting Date", isOutbound);
        taxExcepPerc.ApplyCSTCode("IPI CST Code", taxExcepPerc."Tax Identification"::IPI, "Tax Exception Code", territoryCode,
                                  salesHeader."Posting Date", isOutbound);
        taxExcepPerc.ApplyCSTCode("PIS CST Code", taxExcepPerc."Tax Identification"::PIS, "Tax Exception Code", territoryCode,
                                  salesHeader."Posting Date", isOutbound);
        taxExcepPerc.ApplyCSTCode("COFINS CST Code", taxExcepPerc."Tax Identification"::COFINS, "Tax Exception Code", territoryCode,
                                  salesHeader."Posting Date", isOutbound);
      END;
    END;

    PROCEDURE UpdateSalesLineCSTByTaxArea@1102300003(VAR salesLine@52006501 : Record 37);
    VAR
      taxAreaLine@1102300000 : Record 319;
      cstCode@52006500 : Code[20];
      cstPriority@52006502 : Record 52112502;
    BEGIN
      WITH salesLine DO BEGIN
        IF "Tax Area Code" = '' THEN
          EXIT;

        taxAreaLine.SETAUTOCALCFIELDS("Tax Identification");
        taxAreaLine.SETRANGE("Tax Area", "Tax Area Code");
        taxAreaLine.SETFILTER("Tax Identification", '%1|%2|%3|%4|%5', taxAreaLine."Tax Identification"::ICMS, taxAreaLine."Tax Identification"::ST, taxAreaLine."Tax Identification"::IPI,
                                                                      taxAreaLine."Tax Identification"::PIS, taxAreaLine."Tax Identification"::COFINS);
        IF taxAreaLine.FINDSET THEN
          REPEAT
            cstCode := GetTaxAreaLineCSTCode(taxAreaLine, NOT ("Document Type" IN ["Document Type"::"Return Order", "Document Type"::"Credit Memo"]));
            IF cstCode <> '' THEN
              CASE taxAreaLine."Tax Identification" OF
                taxAreaLine."Tax Identification"::ICMS: cstPriority.Apply(taxAreaLine."Tax Identification"::ICMS, cstCode, "ICMS CST Code");
                taxAreaLine."Tax Identification"::ST: cstPriority.Apply(taxAreaLine."Tax Identification"::ICMS, cstCode, "ICMS CST Code");
                taxAreaLine."Tax Identification"::IPI: cstPriority.Apply(taxAreaLine."Tax Identification"::IPI, cstCode, "IPI CST Code");
                taxAreaLine."Tax Identification"::PIS: cstPriority.Apply(taxAreaLine."Tax Identification"::PIS, cstCode, "PIS CST Code");
                taxAreaLine."Tax Identification"::COFINS: cstPriority.Apply(taxAreaLine."Tax Identification"::COFINS, cstCode, "COFINS CST Code");
              END;
          UNTIL taxAreaLine.NEXT = 0;
      END;
    END;

    PROCEDURE UpdatePurchLineCST@52006504(VAR PurchHeader@52006502 : Record 38;VAR PurchLine@52006500 : Record 39);
    VAR
      purchLineCopy@52006501 : Record 39;
    BEGIN
      WITH PurchLine DO BEGIN
        purchLineCopy.COPY(PurchLine);
        ClearCSTCodes("ICMS CST Code", "IPI CST Code", "PIS CST Code", "COFINS CST Code");
        UpdatePurchLineCSTByTaxArea(PurchLine);
        UpdatePurchLineCSTByNCM(PurchLine);
        UpdatePurchLineCSTByException(PurchLine);
        ApplyPurchLineTaxesMatrixForCSTs(PurchHeader, PurchLine);
        RecoverOldCSTCodesIfNewerAreBlank(purchLineCopy."ICMS CST Code", purchLineCopy."IPI CST Code", purchLineCopy."PIS CST Code", purchLineCopy."COFINS CST Code",
                                          "ICMS CST Code", "IPI CST Code", "PIS CST Code", "COFINS CST Code");
      END;
    END;

    PROCEDURE UpdatePurchLineCSTByNCM@52006501(VAR purchLine@52006500 : Record 39);
    VAR
      purchHeader@52006501 : Record 38;
      compInfo@52006502 : Record 79;
      brTaxCalc@52006503 : Codeunit 52112426;
      ncmCodeTaxPerc@52006504 : Record 52112428;
      isOutbound@52006505 : Boolean;
      territoryCode@52006506 : Code[10];
    BEGIN
      WITH purchLine DO BEGIN
        IF "NCM Code" = '' THEN
          EXIT;

        purchHeader.GET("Document Type", "Document No.");
        compInfo.GetCompanyBranchInfo(purchHeader."Branch Code");
        territoryCode := compInfo."Territory Code";
        isOutbound := "Document Type" IN ["Document Type"::"Credit Memo", "Document Type"::"Return Order"];

        ncmCodeTaxPerc.ApplyCSTCode("ICMS CST Code", ncmCodeTaxPerc."Tax Identification"::ICMS, "NCM Code", "NCM Exception Code",
                                    territoryCode, purchHeader."Posting Date", isOutbound);
        ncmCodeTaxPerc.ApplyCSTCode("ICMS CST Code", ncmCodeTaxPerc."Tax Identification"::ST, "NCM Code", "NCM Exception Code",
                                    territoryCode, purchHeader."Posting Date", isOutbound);
        ncmCodeTaxPerc.ApplyCSTCode("IPI CST Code", ncmCodeTaxPerc."Tax Identification"::IPI, "NCM Code", "NCM Exception Code",
                                    territoryCode, purchHeader."Posting Date", isOutbound);
        ncmCodeTaxPerc.ApplyCSTCode("PIS CST Code", ncmCodeTaxPerc."Tax Identification"::PIS, "NCM Code", "NCM Exception Code",
                                    territoryCode, purchHeader."Posting Date", isOutbound);
        ncmCodeTaxPerc.ApplyCSTCode("COFINS CST Code", ncmCodeTaxPerc."Tax Identification"::COFINS, "NCM Code", "NCM Exception Code",
                                    territoryCode, purchHeader."Posting Date", isOutbound);
      END;
    END;

    PROCEDURE UpdatePurchLineCSTByException@52006518(VAR purchLine@52006500 : Record 39);
    VAR
      purchHeader@52006501 : Record 38;
      taxExcepPerc@52006504 : Record 52112474;
      brTaxCalc@52006506 : Codeunit 52112426;
      isOutbound@52006503 : Boolean;
      territoryCode@52006502 : Code[10];
      compInfo@52006505 : Record 79;
    BEGIN
      WITH purchLine DO BEGIN
        IF "Tax Exception Code" = '' THEN
          EXIT;

        purchHeader.GET("Document Type", "Document No.");
        compInfo.GetCompanyBranchInfo(purchHeader."Branch Code");
        territoryCode := compInfo."Territory Code";
        isOutbound := "Document Type" IN ["Document Type"::"Credit Memo", "Document Type"::"Return Order"];

        isOutbound := NOT ("Document Type" IN ["Document Type"::"Credit Memo", "Document Type"::"Return Order"]);

        taxExcepPerc.ApplyCSTCode("ICMS CST Code", taxExcepPerc."Tax Identification"::ICMS, "Tax Exception Code", territoryCode,
                                  purchHeader."Posting Date", isOutbound);
        taxExcepPerc.ApplyCSTCode("ICMS CST Code", taxExcepPerc."Tax Identification"::ST, "Tax Exception Code", territoryCode,
                                  purchHeader."Posting Date", isOutbound);
        taxExcepPerc.ApplyCSTCode("IPI CST Code", taxExcepPerc."Tax Identification"::IPI, "Tax Exception Code", territoryCode,
                                  purchHeader."Posting Date", isOutbound);
        taxExcepPerc.ApplyCSTCode("PIS CST Code", taxExcepPerc."Tax Identification"::PIS, "Tax Exception Code", territoryCode,
                                  purchHeader."Posting Date", isOutbound);
        taxExcepPerc.ApplyCSTCode("COFINS CST Code", taxExcepPerc."Tax Identification"::COFINS, "Tax Exception Code", territoryCode,
                                  purchHeader."Posting Date", isOutbound);
      END;
    END;

    PROCEDURE UpdatePurchLineCSTByTaxArea@52006500(VAR purchLine@52006501 : Record 39);
    VAR
      taxAreaLine@35000001 : Record 319;
      cstCode@52006500 : Code[20];
      cstPriority@52006502 : Record 52112502;
    BEGIN
      WITH purchLine DO BEGIN
        IF "Tax Area Code" = '' THEN
          EXIT;

        taxAreaLine.SETAUTOCALCFIELDS("Tax Identification");
        taxAreaLine.SETRANGE("Tax Area", "Tax Area Code");
        taxAreaLine.SETFILTER("Tax Identification", '%1|%2|%3|%4|%5',
                              taxAreaLine."Tax Identification"::ICMS, taxAreaLine."Tax Identification"::IPI, taxAreaLine."Tax Identification"::ST,
                              taxAreaLine."Tax Identification"::PIS, taxAreaLine."Tax Identification"::COFINS);
        IF taxAreaLine.FINDFIRST THEN
          REPEAT
            cstCode := GetTaxAreaLineCSTCode(taxAreaLine, ("Document Type" IN ["Document Type"::"Return Order", "Document Type"::"Credit Memo"]));
            IF cstCode <> '' THEN
              CASE taxAreaLine."Tax Identification" OF
                taxAreaLine."Tax Identification"::ICMS: cstPriority.Apply(taxAreaLine."Tax Identification"::ICMS, cstCode, "ICMS CST Code");
                taxAreaLine."Tax Identification"::ST: cstPriority.Apply(taxAreaLine."Tax Identification"::ICMS, cstCode, "ICMS CST Code");
                taxAreaLine."Tax Identification"::IPI: cstPriority.Apply(taxAreaLine."Tax Identification"::IPI, cstCode, "IPI CST Code");
                taxAreaLine."Tax Identification"::PIS: cstPriority.Apply(taxAreaLine."Tax Identification"::PIS, cstCode, "PIS CST Code");
                taxAreaLine."Tax Identification"::COFINS: cstPriority.Apply(taxAreaLine."Tax Identification"::COFINS, cstCode, "COFINS CST Code");
              END;
          UNTIL taxAreaLine.NEXT = 0;
      END;
    END;

    PROCEDURE UpdateServLineCST@52006511(VAR ServHeader@52006502 : Record 5900;VAR servLine@52006500 : Record 5902);
    VAR
      servLineCopy@52006501 : Record 5902;
    BEGIN
      WITH servLine DO BEGIN
        servLineCopy.COPY(servLine);
        ClearCSTCodes("ICMS CST Code", "IPI CST Code", "PIS CST Code", "COFINS CST Code");
        UpdateServLineCSTByTaxArea(servLine);
        UpdateServLineCSTByNCM(servLine);
        UpdateServLineCSTByException(servLine);
        ApplyServLineTaxesMatrixForCSTs(ServHeader, servLine);
        RecoverOldCSTCodesIfNewerAreBlank(servLineCopy."ICMS CST Code", servLineCopy."IPI CST Code", servLineCopy."PIS CST Code", servLineCopy."COFINS CST Code",
                                          "ICMS CST Code", "IPI CST Code", "PIS CST Code", "COFINS CST Code");
      END;
    END;

    PROCEDURE UpdateServLineCSTByNCM@52006510(VAR servLine@52006500 : Record 5902);
    VAR
      servHeader@52006501 : Record 5900;
      compInfo@52006502 : Record 79;
      cust@52006506 : Record 18;
      brTaxCalc@52006503 : Codeunit 52112426;
      ncmCodeTaxPerc@52006504 : Record 52112428;
      isOutbound@52006505 : Boolean;
      territoryCode@52006507 : Code[10];
    BEGIN
      WITH servLine DO BEGIN
        IF "NCM Code" = '' THEN
          EXIT;

        servHeader.GET("Document Type", "Document No.");
        compInfo.GetCompanyBranchInfo(servHeader."Branch Code");
        cust.GET(servHeader."Bill-to Customer No.");
        IF cust."Not ICMS Contributor" THEN
          territoryCode := compInfo."Territory Code"
        ELSE
          territoryCode := servHeader."Bill-to Territory Code";

        isOutbound := NOT ("Document Type" = "Document Type"::"Credit Memo");

        ncmCodeTaxPerc.ApplyCSTCode("ICMS CST Code", ncmCodeTaxPerc."Tax Identification"::ICMS, "NCM Code", "NCM Exception Code",
                                    territoryCode, servHeader."Posting Date", isOutbound);
        ncmCodeTaxPerc.ApplyCSTCode("ICMS CST Code", ncmCodeTaxPerc."Tax Identification"::ST, "NCM Code", "NCM Exception Code",
                                    territoryCode, servHeader."Posting Date", isOutbound);
        ncmCodeTaxPerc.ApplyCSTCode("IPI CST Code", ncmCodeTaxPerc."Tax Identification"::IPI, "NCM Code", "NCM Exception Code",
                                    territoryCode, servHeader."Posting Date", isOutbound);
        ncmCodeTaxPerc.ApplyCSTCode("PIS CST Code", ncmCodeTaxPerc."Tax Identification"::PIS, "NCM Code", "NCM Exception Code",
                                    territoryCode, servHeader."Posting Date", isOutbound);
        ncmCodeTaxPerc.ApplyCSTCode("COFINS CST Code", ncmCodeTaxPerc."Tax Identification"::COFINS, "NCM Code", "NCM Exception Code",
                                    territoryCode, servHeader."Posting Date", isOutbound);
      END;
    END;

    PROCEDURE UpdateServLineCSTByException@52006509(VAR servLine@52006500 : Record 5902);
    VAR
      servHeader@52006501 : Record 5900;
      taxExcepPerc@52006504 : Record 52112474;
      isOutbound@52006505 : Boolean;
      compInfo@52006507 : Record 79;
      cust@52006506 : Record 18;
      brTaxCalc@52006503 : Codeunit 52112426;
      territoryCode@52006502 : Code[10];
    BEGIN
      WITH servLine DO BEGIN
        IF "Tax Exception Code" = '' THEN
          EXIT;

        servHeader.GET("Document Type", "Document No.");
        compInfo.GetCompanyBranchInfo(servHeader."Branch Code");
        cust.GET(servHeader."Bill-to Customer No.");
        IF cust."Not ICMS Contributor" THEN
          territoryCode := compInfo."Territory Code"
        ELSE
          territoryCode := servHeader."Bill-to Territory Code";
        isOutbound := NOT ("Document Type" = "Document Type"::"Credit Memo");

        taxExcepPerc.ApplyCSTCode("ICMS CST Code", taxExcepPerc."Tax Identification"::ICMS, "Tax Exception Code", territoryCode,
                                  servHeader."Posting Date", isOutbound);
        taxExcepPerc.ApplyCSTCode("ICMS CST Code", taxExcepPerc."Tax Identification"::ST, "Tax Exception Code", territoryCode,
                                  servHeader."Posting Date", isOutbound);
        taxExcepPerc.ApplyCSTCode("IPI CST Code", taxExcepPerc."Tax Identification"::IPI, "Tax Exception Code", territoryCode,
                                  servHeader."Posting Date", isOutbound);
        taxExcepPerc.ApplyCSTCode("PIS CST Code", taxExcepPerc."Tax Identification"::PIS, "Tax Exception Code", territoryCode,
                                  servHeader."Posting Date", isOutbound);
        taxExcepPerc.ApplyCSTCode("COFINS CST Code", taxExcepPerc."Tax Identification"::COFINS, "Tax Exception Code", territoryCode,
                                  servHeader."Posting Date", isOutbound);
      END;
    END;

    PROCEDURE UpdateServLineCSTByTaxArea@52006508(VAR servLine@52006501 : Record 5902);
    VAR
      taxAreaLine@1102300000 : Record 319;
      cstCode@52006500 : Code[20];
      cstPriority@52006502 : Record 52112502;
    BEGIN
      WITH servLine DO BEGIN
        IF "Tax Area Code" = '' THEN
          EXIT;

        taxAreaLine.SETAUTOCALCFIELDS("Tax Identification");
        taxAreaLine.SETRANGE("Tax Area", "Tax Area Code");
        taxAreaLine.SETFILTER("Tax Identification", '%1|%2|%3|%4|%5', taxAreaLine."Tax Identification"::ICMS, taxAreaLine."Tax Identification"::ST, taxAreaLine."Tax Identification"::IPI,
                                                                      taxAreaLine."Tax Identification"::PIS, taxAreaLine."Tax Identification"::COFINS);
        IF taxAreaLine.FINDSET THEN
          REPEAT
            cstCode := GetTaxAreaLineCSTCode(taxAreaLine, ("Document Type" <> "Document Type"::"Credit Memo"));
            IF cstCode <> '' THEN
              CASE taxAreaLine."Tax Identification" OF
                taxAreaLine."Tax Identification"::ICMS: cstPriority.Apply(taxAreaLine."Tax Identification"::ICMS, cstCode, "ICMS CST Code");
                taxAreaLine."Tax Identification"::ST: cstPriority.Apply(taxAreaLine."Tax Identification"::ICMS, cstCode, "ICMS CST Code");
                taxAreaLine."Tax Identification"::IPI: cstPriority.Apply(taxAreaLine."Tax Identification"::IPI, cstCode, "IPI CST Code");
                taxAreaLine."Tax Identification"::PIS: cstPriority.Apply(taxAreaLine."Tax Identification"::PIS, cstCode, "PIS CST Code");
                taxAreaLine."Tax Identification"::COFINS: cstPriority.Apply(taxAreaLine."Tax Identification"::COFINS, cstCode, "COFINS CST Code");
              END;
          UNTIL taxAreaLine.NEXT = 0;
      END;
    END;

    PROCEDURE ApplySalesLineTaxesMatrix@52006549(VAR SalesHeader@52006502 : Record 36;VAR SalesLine@52006500 : Record 37);
    BEGIN
      _ApplySalesLineTaxesMatrix(SalesHeader, SalesLine, FALSE);
    END;

    PROCEDURE ApplySalesLineTaxesMatrixForCSTs@52006547(VAR SalesHeader@52006500 : Record 36;VAR SalesLine@52006501 : Record 37);
    BEGIN
      _ApplySalesLineTaxesMatrix(SalesHeader, SalesLine, TRUE);
    END;

    LOCAL PROCEDURE _ApplySalesLineTaxesMatrix@52006505(VAR SalesHeader@52006505 : Record 36;VAR SalesLine@52006501 : Record 37;CSTOnly@52006502 : Boolean);
    VAR
      TaxesMatrix@52006500 : Record 52112481;
      CSTPriority@52006504 : Record 52112502;
    BEGIN
      WITH SalesLine DO BEGIN
        IF SalesHeader."Taxes Matrix Code" = '' THEN
          EXIT;

        TaxesMatrix.GET(SalesHeader."Taxes Matrix Code");

        IF NOT CSTOnly THEN BEGIN
          IF TaxesMatrix."Tax Area Code" <> '' THEN
            SalesLine.VALIDATE("Tax Area Code", TaxesMatrix."Tax Area Code");
          TaxesMatrix.SetCFOPCode("CFOP Code", "Customer Situation", "End User");
          VALIDATE("CFOP Code");
          IF TaxesMatrix."ICMS Origin Code" <> '' THEN
            VALIDATE("Origin Code", TaxesMatrix."ICMS Origin Code");
          IF TaxesMatrix."Tax Exception Code" <> '' THEN
            VALIDATE("Tax Exception Code", TaxesMatrix."Tax Exception Code");
          IF TaxesMatrix."Base Calculation Credit Code" <> '' THEN
            VALIDATE("Base Calculation Credit Code", TaxesMatrix."Base Calculation Credit Code");
          IF TaxesMatrix."PIS CST Income Nature" <> '' THEN
            VALIDATE("PIS CST Income Nature", TaxesMatrix."PIS CST Income Nature");
          IF TaxesMatrix."COFINS CST Income Nature" <> '' THEN
            VALIDATE("COFINS CST Income Nature", TaxesMatrix."COFINS CST Income Nature");
        END;

        CSTPriority.Apply(CSTPriority."Tax Identification"::ICMS, TaxesMatrix."ICMS CST Code", "ICMS CST Code");
        CSTPriority.Apply(CSTPriority."Tax Identification"::IPI, TaxesMatrix."IPI CST Code", "IPI CST Code");
        CSTPriority.Apply(CSTPriority."Tax Identification"::PIS, TaxesMatrix."PIS CST Code", "PIS CST Code");
        CSTPriority.Apply(CSTPriority."Tax Identification"::COFINS, TaxesMatrix."COFINS CST Code", "COFINS CST Code");
      END;
    END;

    PROCEDURE ApplyPurchLineTaxesMatrix@52006506(VAR PurchHeader@52006500 : Record 38;VAR PurchLine@52006501 : Record 39);
    BEGIN
      _ApplyPurchLineTaxesMatrix(PurchHeader, PurchLine, FALSE);
    END;

    PROCEDURE ApplyPurchLineTaxesMatrixForCSTs@52006524(VAR PurchHeader@52006500 : Record 38;VAR PurchLine@52006501 : Record 39);
    BEGIN
      _ApplyPurchLineTaxesMatrix(PurchHeader, PurchLine, TRUE);
    END;

    LOCAL PROCEDURE _ApplyPurchLineTaxesMatrix@52006522(VAR PurchHeader@52006505 : Record 38;VAR PurchLine@52006501 : Record 39;CSTOnly@52006503 : Boolean);
    VAR
      TaxesMatrix@52006500 : Record 52112481;
      CSTPriority@52006504 : Record 52112502;
    BEGIN
      WITH PurchLine DO BEGIN
        IF PurchHeader."Taxes Matrix Code" = '' THEN
          EXIT;

        TaxesMatrix.GET(PurchHeader."Taxes Matrix Code");

        IF NOT CSTOnly THEN BEGIN
          IF TaxesMatrix."Tax Area Code" <> '' THEN
            PurchLine.VALIDATE("Tax Area Code", TaxesMatrix."Tax Area Code");
          TaxesMatrix.SetCFOPCode("CFOP Code", "Vendor Situation", "End User");
          VALIDATE("CFOP Code");
          IF TaxesMatrix."ICMS Origin Code" <> '' THEN
            VALIDATE("Origin Code", TaxesMatrix."ICMS Origin Code");
          IF TaxesMatrix."Tax Exception Code" <> '' THEN
            VALIDATE("Tax Exception Code", TaxesMatrix."Tax Exception Code");
          IF TaxesMatrix."Base Calculation Credit Code" <> '' THEN
            VALIDATE("Base Calculation Credit Code", TaxesMatrix."Base Calculation Credit Code");
          IF TaxesMatrix."PIS CST Income Nature" <> '' THEN
            VALIDATE("PIS CST Income Nature", TaxesMatrix."PIS CST Income Nature");
          IF TaxesMatrix."COFINS CST Income Nature" <> '' THEN
            VALIDATE("COFINS CST Income Nature", TaxesMatrix."COFINS CST Income Nature");
        END;

        CSTPriority.Apply(CSTPriority."Tax Identification"::ICMS, TaxesMatrix."ICMS CST Code", "ICMS CST Code");
        CSTPriority.Apply(CSTPriority."Tax Identification"::IPI, TaxesMatrix."IPI CST Code", "IPI CST Code");
        CSTPriority.Apply(CSTPriority."Tax Identification"::PIS, TaxesMatrix."PIS CST Code", "PIS CST Code");
        CSTPriority.Apply(CSTPriority."Tax Identification"::COFINS, TaxesMatrix."COFINS CST Code", "COFINS CST Code");
      END;
    END;

    PROCEDURE ApplyServLineTaxesMatrix@52006529(VAR ServHeader@52006502 : Record 5900;VAR ServLine@52006501 : Record 5902);
    BEGIN
      _ApplyServLineTaxesMatrix(ServHeader, ServLine, FALSE);
    END;

    PROCEDURE ApplyServLineTaxesMatrixForCSTs@52006530(VAR ServHeader@52006500 : Record 5900;VAR ServLine@52006501 : Record 5902);
    BEGIN
      _ApplyServLineTaxesMatrix(ServHeader, ServLine, TRUE);
    END;

    LOCAL PROCEDURE _ApplyServLineTaxesMatrix@52006512(VAR ServHeader@52006504 : Record 5900;VAR servLine@52006501 : Record 5902;cstOnly@52006502 : Boolean);
    VAR
      taxesMatrix@52006500 : Record 52112481;
      cstPriority@52006505 : Record 52112502;
    BEGIN
      WITH servLine DO BEGIN
        IF ServHeader."Taxes Matrix Code" = '' THEN
          EXIT;

        taxesMatrix.GET(ServHeader."Taxes Matrix Code");

        IF NOT cstOnly THEN BEGIN
          IF taxesMatrix."Tax Area Code" <> '' THEN
            servLine.VALIDATE("Tax Area Code", taxesMatrix."Tax Area Code");
          taxesMatrix.SetCFOPCode("CFOP Code", "Customer Situation", "End User");
          VALIDATE("CFOP Code");
          IF taxesMatrix."ICMS Origin Code" <> '' THEN
            VALIDATE("Origin Code", taxesMatrix."ICMS Origin Code");
          IF taxesMatrix."Tax Exception Code" <> '' THEN
            VALIDATE("Tax Exception Code", taxesMatrix."Tax Exception Code");
          IF taxesMatrix."Base Calculation Credit Code" <> '' THEN
            VALIDATE("Base Calculation Credit Code", taxesMatrix."Base Calculation Credit Code");
          IF taxesMatrix."PIS CST Income Nature" <> '' THEN
            VALIDATE("PIS CST Income Nature", taxesMatrix."PIS CST Income Nature");
          IF taxesMatrix."COFINS CST Income Nature" <> '' THEN
            VALIDATE("COFINS CST Income Nature", taxesMatrix."COFINS CST Income Nature");
        END;

        cstPriority.Apply(cstPriority."Tax Identification"::ICMS, taxesMatrix."ICMS CST Code", "ICMS CST Code");
        cstPriority.Apply(cstPriority."Tax Identification"::IPI, taxesMatrix."IPI CST Code", "IPI CST Code");
        cstPriority.Apply(cstPriority."Tax Identification"::PIS, taxesMatrix."PIS CST Code", "PIS CST Code");
        cstPriority.Apply(cstPriority."Tax Identification"::COFINS, taxesMatrix."COFINS CST Code", "COFINS CST Code");
      END;
    END;

    PROCEDURE GetTaxAreaLineCSTCode@52006520(taxAreaLine@52006500 : Record 319;isOutbound@52006501 : Boolean) cstCode : Code[10];
    VAR
      taxJurisdiction@52006502 : Record 320;
    BEGIN
      cstCode := taxAreaLine."CST Code";
      IF cstCode = '' THEN BEGIN
        taxJurisdiction.GET(taxAreaLine."Tax Jurisdiction Code");
        IF isOutbound THEN
          cstCode := taxJurisdiction."CST Code (Sales)"
        ELSE
          cstCode := taxJurisdiction."CST Code (Purch.)";
      END;
    END;

    LOCAL PROCEDURE ClearCSTCodes@52006523(VAR ICMSCST@52006500 : Code[10];VAR IPICST@52006501 : Code[10];VAR PISCST@52006502 : Code[10];VAR COFINSCST@52006503 : Code[10]);
    BEGIN
      ICMSCST := '';
      IPICST := '';
      PISCST := '';
      COFINSCST := '';
    END;

    LOCAL PROCEDURE RecoverOldCSTCodesIfNewerAreBlank@52006531(OldICMSCST@52006507 : Code[10];OldIPICST@52006506 : Code[10];OldPISCST@52006505 : Code[10];OldCOFINSCST@52006504 : Code[10];VAR ICMSCST@52006503 : Code[10];VAR IPICST@52006502 : Code[10];VAR PISCST@52006501 : Code[10];VAR COFINSCST@52006500 : Code[10]);
    BEGIN
      RecoverCSTCode(OldICMSCST, ICMSCST);
      RecoverCSTCode(OldIPICST, IPICST);
      RecoverCSTCode(OldPISCST, PISCST);
      RecoverCSTCode(OldCOFINSCST, COFINSCST);
    END;

    LOCAL PROCEDURE RecoverCSTCode@52006532(OldCST@52006507 : Code[10];VAR CST@52006500 : Code[10]);
    BEGIN
      IF CST = '' THEN
        CST := OldCST;
    END;

    BEGIN
    END.
  }
}

