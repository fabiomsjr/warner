OBJECT Page 52112743 Purch. Request Quote Subform
{
  OBJECT-PROPERTIES
  {
    Date=11/09/15;
    Time=11:24:10;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Items;
               PTB=Produtos];
    LinksAllowed=No;
    SourceTable=Table52112731;
    DelayedInsert=Yes;
    SourceTableView=SORTING(Purch Req. Quote No.,Line No.);
    PageType=ListPart;
    AutoSplitKey=Yes;
    ShowFilter=No;
    OnOpenPage=BEGIN
                 PurchSetup.GET;
                 PurchSetup.TESTFIELD("Request Quote Vendor Limit");
                 ItemEditable := PurchSetup."Allow Manual Item Quote";
               END;

    OnAfterGetRecord=BEGIN
                       IF NOT FirstColumnUpdateDone THEN BEGIN
                         UpdateColumns;
                         FirstColumnUpdateDone := TRUE;
                       END;
                       UpdateBestPrice;
                     END;

    OnNewRecord=BEGIN
                  "Item Type" := "Item Type"::Item;
                END;

    OnInsertRecord=BEGIN
                     IF NOT ItemEditable THEN
                       ERROR(NewLineNowAllowedErr);
                   END;

    OnAfterGetCurrRecord=BEGIN
                           OpenOrderEnabled := "Purch. Order No." <> '';
                         END;

    ActionList=ACTIONS
    {
      { 52006516;    ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 52006517;1   ;Action    ;
                      Name=SelWinner;
                      Ellipsis=Yes;
                      CaptionML=[ENU=Select Winner;
                                 PTB=Selecionar Vencedor];
                      Image=SuggestVendorBills;
                      OnAction=BEGIN
                                 SelectWinner;
                               END;
                                }
      { 52006544;1   ;Action    ;
                      Name=Requests;
                      CaptionML=[ENU=Requests;
                                 PTB=Requisi��es];
                      RunObject=Page 52112749;
                      RunPageView=SORTING(Quote No.,Item No.);
                      RunPageLink=Quote No.=FIELD(Purch Req. Quote No.);
                      Image=Document }
      { 52006521;1   ;Action    ;
                      Name=OpenOrd;
                      CaptionML=[ENU=Open Order;
                                 PTB=Abrir Pedido];
                      Enabled=OpenOrderEnabled;
                      Image=Order;
                      OnAction=BEGIN
                                 OpenOrder;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 35000000;0;Container;
                ContainerType=ContentArea }

    { 52006500;1;Group    ;
                CaptionML=[ENU=Vendors;
                           PTB=Fornecedores];
                GroupType=Repeater;
                FreezeColumnID=Unit of Measure }

    { 52006543;2;Field    ;
                SourceExpr="Item Type" }

    { 52006501;2;Field    ;
                NotBlank=Yes;
                SourceExpr="Item No." }

    { 52006502;2;Field    ;
                SourceExpr=Description }

    { 52006504;2;Field    ;
                SourceExpr=Quantity }

    { 52006518;2;Field    ;
                SourceExpr="Unit of Measure";
                Editable=FALSE }

    { 52006503;2;Field    ;
                SourceExpr="Vendor 1 Price";
                Visible=ShowVendor1;
                OnValidate=BEGIN
                             UpdateBestPrice;
                           END;
                            }

    { 52006505;2;Field    ;
                SourceExpr="Vendor 2 Price";
                Visible=ShowVendor2;
                OnValidate=BEGIN
                             UpdateBestPrice;
                           END;
                            }

    { 52006506;2;Field    ;
                SourceExpr="Vendor 3 Price";
                Visible=ShowVendor3;
                OnValidate=BEGIN
                             UpdateBestPrice;
                           END;
                            }

    { 52006507;2;Field    ;
                SourceExpr="Vendor 4 Price";
                Visible=ShowVendor4;
                OnValidate=BEGIN
                             UpdateBestPrice;
                           END;
                            }

    { 52006508;2;Field    ;
                SourceExpr="Vendor 5 Price";
                Visible=ShowVendor5;
                OnValidate=BEGIN
                             UpdateBestPrice;
                           END;
                            }

    { 52006509;2;Field    ;
                SourceExpr="Vendor 6 Price";
                Visible=ShowVendor6;
                OnValidate=BEGIN
                             UpdateBestPrice;
                           END;
                            }

    { 52006510;2;Field    ;
                SourceExpr="Vendor 7 Price";
                Visible=ShowVendor7;
                OnValidate=BEGIN
                             UpdateBestPrice;
                           END;
                            }

    { 52006511;2;Field    ;
                SourceExpr="Vendor 8 Price";
                Visible=ShowVendor8;
                OnValidate=BEGIN
                             UpdateBestPrice;
                           END;
                            }

    { 52006512;2;Field    ;
                SourceExpr="Vendor 9 Price";
                Visible=ShowVendor9;
                OnValidate=BEGIN
                             UpdateBestPrice;
                           END;
                            }

    { 52006513;2;Field    ;
                SourceExpr="Vendor 10 Price";
                Visible=ShowVendor10;
                OnValidate=BEGIN
                             UpdateBestPrice;
                           END;
                            }

    { 52006514;2;Field    ;
                CaptionML=[ENU=Best Price;
                           PTB=Melhor Pre�o];
                SourceExpr=BestPriceVendorName;
                Editable=FALSE }

    { 52006515;2;Field    ;
                CaptionML=[ENU=Winner;
                           PTB=Vencedor];
                SourceExpr="Winning Vendor Name";
                Style=StandardAccent;
                StyleExpr=TRUE }

    { 52006519;2;Field    ;
                SourceExpr="Purch. Order No.";
                Visible=FALSE;
                Editable=FALSE }

    { 52006520;2;Field    ;
                SourceExpr="Purch. Order Line No.";
                Visible=FALSE;
                Editable=FALSE }

    { 52006546;2;Field    ;
                SourceExpr="From Request";
                Visible=FALSE;
                Editable=FALSE }

    { 52006545;2;Field    ;
                SourceExpr="Line No.";
                Visible=FALSE;
                Editable=FALSE }

    { 52006522;1;Group    ;
                CaptionML=[ENU=Totals by Vendor;
                           PTB=Totais por Fornecedor];
                GroupType=FixedLayout }

    { 52006525;2;Group    ;
                GroupType=Group }

    { 52006523;3;Field    ;
                SourceExpr=GetTotal(1);
                CaptionClass=FIELDCAPTION("Vendor 1 Price") }

    { 52006526;2;Group    ;
                GroupType=Group }

    { 52006524;3;Field    ;
                SourceExpr=GetTotal(2);
                CaptionClass=FIELDCAPTION("Vendor 2 Price") }

    { 52006528;2;Group    ;
                GroupType=Group }

    { 52006527;3;Field    ;
                SourceExpr=GetTotal(3);
                CaptionClass=FIELDCAPTION("Vendor 3 Price") }

    { 52006530;2;Group    ;
                GroupType=Group }

    { 52006529;3;Field    ;
                SourceExpr=GetTotal(4);
                CaptionClass=FIELDCAPTION("Vendor 4 Price") }

    { 52006532;2;Group    ;
                GroupType=Group }

    { 52006531;3;Field    ;
                SourceExpr=GetTotal(5);
                CaptionClass=FIELDCAPTION("Vendor 5 Price") }

    { 52006534;2;Group    ;
                GroupType=Group }

    { 52006533;3;Field    ;
                SourceExpr=GetTotal(6);
                CaptionClass=FIELDCAPTION("Vendor 6 Price") }

    { 52006536;2;Group    ;
                GroupType=Group }

    { 52006535;3;Field    ;
                SourceExpr=GetTotal(7);
                CaptionClass=FIELDCAPTION("Vendor 7 Price") }

    { 52006538;2;Group    ;
                GroupType=Group }

    { 52006537;3;Field    ;
                SourceExpr=GetTotal(8);
                CaptionClass=FIELDCAPTION("Vendor 8 Price") }

    { 52006540;2;Group    ;
                GroupType=Group }

    { 52006539;3;Field    ;
                SourceExpr=GetTotal(9);
                CaptionClass=FIELDCAPTION("Vendor 9 Price") }

    { 52006542;2;Group    ;
                GroupType=Group }

    { 52006541;3;Field    ;
                SourceExpr=GetTotal(10);
                CaptionClass=FIELDCAPTION("Vendor 10 Price") }

  }
  CODE
  {
    VAR
      ShowVendor1@52006514 : Boolean;
      ShowVendor2@52006508 : Boolean;
      ShowVendor3@52006507 : Boolean;
      ShowVendor4@52006506 : Boolean;
      ShowVendor5@52006505 : Boolean;
      ShowVendor6@52006504 : Boolean;
      ShowVendor7@52006503 : Boolean;
      ShowVendor8@52006502 : Boolean;
      ShowVendor9@52006501 : Boolean;
      ShowVendor10@52006500 : Boolean;
      PurchSetup@52006509 : Record 312;
      FirstColumnUpdateDone@52006511 : Boolean;
      BestPriceVendorName@52006512 : Text;
      SelectedVendorName@52006513 : Text;
      OpenOrderEnabled@52006515 : Boolean;
      ItemEditable@52006516 : Boolean;
      NewLineNowAllowedErr@52006510 : TextConst 'ENU=Manually creating new lines is now allowed.;PTB=N�o � permitido criar novas linhas manualmente.';

    PROCEDURE DoUpdate@52006500();
    BEGIN
      UpdateColumns;
      CurrPage.UPDATE(FALSE);
    END;

    PROCEDURE UpdateColumns@52006501();
    VAR
      reqQuote@52006502 : Record 52112730;
      showVendor@52006501 : ARRAY [10] OF Boolean;
      i@52006500 : Integer;
    BEGIN
      FILTERGROUP(2);
      reqQuote.GET(GETRANGEMIN("Purch Req. Quote No."));
      FILTERGROUP(0);

      FOR i := 1 TO PurchSetup."Request Quote Vendor Limit" DO
        showVendor[i] := TRUE;
      ShowVendor1 := showVendor[1] AND (reqQuote."Vendor No. 1" <> '');
      ShowVendor2 := showVendor[2] AND (reqQuote."Vendor No. 2" <> '');
      ShowVendor3 := showVendor[3] AND (reqQuote."Vendor No. 3" <> '');
      ShowVendor4 := showVendor[4] AND (reqQuote."Vendor No. 4" <> '');
      ShowVendor5 := showVendor[5] AND (reqQuote."Vendor No. 5" <> '');
      ShowVendor6 := showVendor[6] AND (reqQuote."Vendor No. 6" <> '');
      ShowVendor7 := showVendor[7] AND (reqQuote."Vendor No. 7" <> '');
      ShowVendor8 := showVendor[8] AND (reqQuote."Vendor No. 8" <> '');
      ShowVendor9 := showVendor[9] AND (reqQuote."Vendor No. 9" <> '');
      ShowVendor10 := showVendor[10] AND (reqQuote."Vendor No. 10" <> '');
    END;

    PROCEDURE UpdateBestPrice@52006508();
    VAR
      vendors@52006501 : ARRAY [10] OF Code[20];
      prices@52006500 : ARRAY [10] OF Decimal;
      bestPrice@52006502 : Decimal;
      i@52006503 : Integer;
      vendor@52006504 : Record 23;
    BEGIN
      CLEAR(BestPriceVendorName);
      bestPrice :=  2147483647;
      GetPricesVector(vendors, prices);
      FOR i := 1 TO 10 DO
        IF (vendors[i] <> '') AND (prices[i] <> 0) AND (prices[i] < bestPrice) THEN BEGIN
          bestPrice := prices[i];
          vendor.GET(vendors[i]);
          BestPriceVendorName := vendor.Name;
        END;
    END;

    PROCEDURE OpenOrder@52006502();
    VAR
      purchHeader@52006500 : Record 38;
      purchOrder@52006501 : Page 50;
    BEGIN
      purchHeader.GET(purchHeader."Document Type"::Order, "Purch. Order No.");
      purchOrder.SETRECORD(purchHeader);
      purchOrder.RUN;
    END;

    PROCEDURE GetTotal@52006503(i@52006500 : Integer) : Text;
    VAR
      quoteLine@52006501 : Record 52112731;
    BEGIN
      quoteLine.SETRANGE("Purch Req. Quote No.", "Purch Req. Quote No.");
      CASE i OF
        1: BEGIN
          quoteLine.CALCSUMS("Vendor 1 Total");
          EXIT(FormatTotal(GetCaption(1), quoteLine."Vendor 1 Total"));
        END;
        2: BEGIN
          quoteLine.CALCSUMS("Vendor 2 Total");
          EXIT(FormatTotal(GetCaption(2), quoteLine."Vendor 2 Total"));
        END;
        3: BEGIN
          quoteLine.CALCSUMS("Vendor 3 Total");
          EXIT(FormatTotal(GetCaption(3), quoteLine."Vendor 3 Total"));
        END;
        4: BEGIN
          quoteLine.CALCSUMS("Vendor 4 Total");
          EXIT(FormatTotal(GetCaption(4), quoteLine."Vendor 4 Total"));
        END;
        5: BEGIN
          quoteLine.CALCSUMS("Vendor 5 Total");
          EXIT(FormatTotal(GetCaption(5), quoteLine."Vendor 5 Total"));
        END;
        6: BEGIN
          quoteLine.CALCSUMS("Vendor 6 Total");
          EXIT(FormatTotal(GetCaption(6), quoteLine."Vendor 6 Total"));
        END;
        7: BEGIN
          quoteLine.CALCSUMS("Vendor 7 Total");
          EXIT(FormatTotal(GetCaption(7), quoteLine."Vendor 7 Total"));
        END;
        8: BEGIN
          quoteLine.CALCSUMS("Vendor 8 Total");
          EXIT(FormatTotal(GetCaption(8), quoteLine."Vendor 8 Total"));
        END;
        9: BEGIN
          quoteLine.CALCSUMS("Vendor 9 Total");
          EXIT(FormatTotal(GetCaption(9), quoteLine."Vendor 9 Total"));
        END;
        10: BEGIN
          quoteLine.CALCSUMS("Vendor 10 Total");
          EXIT(FormatTotal(GetCaption(10), quoteLine."Vendor 10 Total"));
        END;
      ELSE
        EXIT('');
      END;
    END;

    PROCEDURE FormatTotal@52006568(name@52006500 : Text;amt@52006501 : Decimal) : Text;
    BEGIN
      IF name <> '' THEN
        EXIT(STRSUBSTNO('%1 - %2', FORMAT(amt, 0, '<Precision,2:2><Standard Format,0>'), name))
      ELSE
        EXIT('');
    END;

    BEGIN
    END.
  }
}

