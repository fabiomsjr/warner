OBJECT Table 35000665 Tax Ident. Document CIAP.2
{
  OBJECT-PROPERTIES
  {
    Date=29/02/12;
    Time=18:14:19;
    Version List=EFD2.0.8;
  }
  PROPERTIES
  {
    CaptionML=PTB=Identifica��o Documento Fiscal;
  }
  FIELDS
  {
    { 1   ;   ;Tax Settlement Code ;Code20         }
    { 2   ;   ;User ID             ;Code20         }
    { 3   ;   ;Branch Code         ;Code20         }
    { 4   ;   ;Issuer Indication   ;Integer       ;CaptionML=PTB=Indica��o Emitente }
    { 5   ;   ;Participant Code    ;Code20        ;CaptionML=PTB=C�d. Participante }
    { 6   ;   ;Fiscal Doc. Model   ;Code2         ;CaptionML=PTB=Modelo Documento Fiscal }
    { 7   ;   ;Series              ;Code3         ;CaptionML=PTB=Serie }
    { 8   ;   ;Invoice No.         ;Code10        ;CaptionML=PTB=N� Documento Fiscal }
    { 9   ;   ;e-Invoice Key       ;Text60        ;CaptionML=PTB=Chave NF-e }
    { 10  ;   ;Document Date       ;Date          ;CaptionML=PTB=Data Emiss�o Documento }
    { 11  ;   ;Fixed Asset Code    ;Code20         }
    { 12  ;   ;Sequencial No.      ;Integer        }
  }
  KEYS
  {
    {    ;Tax Settlement Code,User ID,Branch Code,Issuer Indication,Participant Code,e-Invoice Key,Fixed Asset Code;
                                                   Clustered=Yes }
    {    ;Tax Settlement Code,Fixed Asset Code     }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    {
      MBS/BR

      ------------------------------------------------------------------------------------------------------------------------------------
      No.   dd.mm.yy   Developer   Company    DocNo.        Description
      ------------------------------------------------------------------------------------------------------------------------------------
      00    03.02.11   KVS         MICROSOFT  LOC001        Add Field "Fixed Asset Code" on primary key
    }
    END.
  }
}

