OBJECT Table 47 Aging Band Buffer
{
  OBJECT-PROPERTIES
  {
    Date=05/11/08;
    Time=12:00:00;
    Version List=NAVW16.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Aging Band Buffer;
               PTB=Intervalo de Per�odo de Vencimento];
  }
  FIELDS
  {
    { 1   ;   ;Currency Code       ;Code20        ;CaptionML=[ENU=Currency Code;
                                                              PTB=Cod. Moeda] }
    { 2   ;   ;Column 1 Amt.       ;Decimal       ;CaptionML=[ENU=Column 1 Amt.;
                                                              PTB=Valor Coluna 1] }
    { 3   ;   ;Column 2 Amt.       ;Decimal       ;CaptionML=[ENU=Column 2 Amt.;
                                                              PTB=Valor Coluna 2] }
    { 4   ;   ;Column 3 Amt.       ;Decimal       ;CaptionML=[ENU=Column 3 Amt.;
                                                              PTB=Valor Coluna 3] }
    { 5   ;   ;Column 4 Amt.       ;Decimal       ;CaptionML=[ENU=Column 4 Amt.;
                                                              PTB=Valor Coluna 4] }
    { 6   ;   ;Column 5 Amt.       ;Decimal       ;CaptionML=[ENU=Column 5 Amt.;
                                                              PTB=Valor Coluna 5] }
  }
  KEYS
  {
    {    ;Currency Code                           ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

