OBJECT Page 9080 Sales Hist. Sell-to FactBox
{
  OBJECT-PROPERTIES
  {
    Date=29/01/15;
    Time=12:00:00;
    Version List=NAVW18.00.00.39663;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Sell-to Customer Sales History;
               PTB=Hist�rico Vendas Clientes - Vendas-a Cliente];
    SourceTable=Table18;
    PageType=CardPart;
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Field     ;
                CaptionML=[ENU=Customer No.;
                           PTB=N� Cliente];
                SourceExpr="No.";
                OnDrillDown=BEGIN
                              ShowDetails;
                            END;
                             }

    { 3   ;1   ;Field     ;
                CaptionML=[ENU=Quotes;
                           PTB=Cota��es];
                SourceExpr="No. of Quotes";
                DrillDownPageID=Sales Quotes }

    { 5   ;1   ;Field     ;
                CaptionML=[ENU=Blanket Orders;
                           PTB=Pedidos em Aberto];
                SourceExpr="No. of Blanket Orders";
                DrillDownPageID=Blanket Sales Orders }

    { 7   ;1   ;Field     ;
                CaptionML=[ENU=Orders;
                           PTB=Pedidos];
                SourceExpr="No. of Orders";
                DrillDownPageID=Sales Order List }

    { 9   ;1   ;Field     ;
                CaptionML=[ENU=Invoices;
                           PTB=Notas Fiscais];
                SourceExpr="No. of Invoices";
                DrillDownPageID=Sales Invoice List }

    { 11  ;1   ;Field     ;
                CaptionML=[ENU=Return Orders;
                           PTB=Devolu��es];
                SourceExpr="No. of Return Orders";
                DrillDownPageID=Sales Return Order List }

    { 13  ;1   ;Field     ;
                CaptionML=[ENU=Credit Memos;
                           PTB=Notas Cr�dito];
                SourceExpr="No. of Credit Memos";
                DrillDownPageID=Sales Credit Memos }

    { 15  ;1   ;Field     ;
                CaptionML=[ENU=Pstd. Shipments;
                           PTB=Envios Registrados];
                SourceExpr="No. of Pstd. Shipments";
                DrillDownPageID=Posted Sales Shipments }

    { 17  ;1   ;Field     ;
                CaptionML=[ENU=Pstd. Invoices;
                           PTB=Notas Fiscais Registradas];
                SourceExpr="No. of Pstd. Invoices";
                DrillDownPageID=Posted Sales Invoices }

    { 19  ;1   ;Field     ;
                CaptionML=[ENU=Pstd. Return Receipts;
                           PTB=Devolu��es Registradas];
                SourceExpr="No. of Pstd. Return Receipts";
                DrillDownPageID=Posted Return Receipts }

    { 21  ;1   ;Field     ;
                CaptionML=[ENU=Pstd. Credit Memos;
                           PTB=Notas Cr�dito Registradas];
                SourceExpr="No. of Pstd. Credit Memos";
                DrillDownPageID=Posted Sales Credit Memos }

  }
  CODE
  {

    PROCEDURE ShowDetails@1102601000();
    BEGIN
      PAGE.RUN(PAGE::"Customer Card",Rec);
    END;

    BEGIN
    END.
  }
}

