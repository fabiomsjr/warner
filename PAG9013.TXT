OBJECT Page 9013 Machine Operator Role Center
{
  OBJECT-PROPERTIES
  {
    Date=09/09/14;
    Time=12:00:00;
    Version List=NAVW18.00,NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Role Center;
               PTB=Centro de Fun��o];
    PageType=RoleCenter;
    ActionList=ACTIONS
    {
      { 1900000006;0 ;ActionContainer;
                      ActionContainerType=Reports }
      { 52      ;1   ;Action    ;
                      CaptionML=[ENU=&Capacity Task List;
                                 PTB=Lista Tarefas Capacidade];
                      RunObject=Report 99000780;
                      Image=Report }
      { 16      ;1   ;Action    ;
                      CaptionML=[ENU=Prod. Order - &Job Card;
                                 PTB=Ordem Prod. - Ficha Trabalho];
                      RunObject=Report 99000762;
                      Image=Report }
      { 1900000011;0 ;ActionContainer;
                      ActionContainerType=HomeItems }
      { 34      ;1   ;Action    ;
                      CaptionML=[ENU=Released Production Orders;
                                 PTB=Pedidos Produ��o Liberada];
                      RunObject=Page 9326 }
      { 35      ;1   ;Action    ;
                      CaptionML=[ENU=Finished Production Orders;
                                 PTB=Pedidos Produ��o Acabada];
                      RunObject=Page 9327 }
      { 12      ;1   ;Action    ;
                      CaptionML=[ENU=Items;
                                 PTB=Produtos];
                      RunObject=Page 31;
                      Image=Item }
      { 37      ;1   ;Action    ;
                      CaptionML=[ENU=Produced;
                                 PTB=Produzido];
                      RunObject=Page 31;
                      RunPageView=WHERE(Replenishment System=CONST(Prod. Order)) }
      { 94      ;1   ;Action    ;
                      CaptionML=[ENU=Raw Materials;
                                 PTB=Mat�rias Primas];
                      RunObject=Page 31;
                      RunPageView=WHERE(Low-Level Code=FILTER(>0),
                                        Replenishment System=CONST(Purchase),
                                        Production BOM No.=FILTER(='')) }
      { 95      ;1   ;Action    ;
                      CaptionML=[ENU=Stockkeeping Units;
                                 PTB=Unidades Armazenagem];
                      RunObject=Page 5701;
                      Image=SKU }
      { 13      ;1   ;Action    ;
                      CaptionML=[ENU=Capacity Ledger Entries;
                                 PTB=Movimento Capacidade];
                      RunObject=Page 5832;
                      Image=CapacityLedger }
      { 10      ;1   ;Action    ;
                      CaptionML=[ENU=Inventory Put-aways;
                                 PTB=Invent�rio Put-aways];
                      RunObject=Page 9315 }
      { 11      ;1   ;Action    ;
                      CaptionML=[ENU=Inventory Picks;
                                 PTB=Invent�rio Picks];
                      RunObject=Page 9316 }
      { 14      ;1   ;Action    ;
                      CaptionML=[ENU=Consumption Journals;
                                 PTB=Di�rios Consumo];
                      RunObject=Page 262;
                      RunPageView=WHERE(Template Type=CONST(Consumption),
                                        Recurring=CONST(No)) }
      { 15      ;1   ;Action    ;
                      CaptionML=[ENU=Output Journals;
                                 PTB=Di�rios Sa�da];
                      RunObject=Page 262;
                      RunPageView=WHERE(Template Type=CONST(Output),
                                        Recurring=CONST(No)) }
      { 19      ;1   ;Action    ;
                      CaptionML=[ENU=Capacity Journals;
                                 PTB=Di�rios Capacidade];
                      RunObject=Page 262;
                      RunPageView=WHERE(Template Type=CONST(Capacity),
                                        Recurring=CONST(No)) }
      { 20      ;1   ;Action    ;
                      CaptionML=[ENU=Recurring Capacity Journals;
                                 PTB=Di�rios Capacidade Recorrente];
                      RunObject=Page 262;
                      RunPageView=WHERE(Template Type=CONST(Capacity),
                                        Recurring=CONST(Yes)) }
      { 1900000012;0 ;ActionContainer;
                      ActionContainerType=ActivityButtons }
      { 1       ;0   ;ActionContainer;
                      ActionContainerType=NewDocumentItems }
      { 4       ;1   ;Action    ;
                      CaptionML=[ENU=Inventory P&ick;
                                 PTB=Inv. Picking];
                      RunObject=Page 7377;
                      Promoted=No;
                      Image=CreateInventoryPickup;
                      PromotedCategory=Process;
                      RunPageMode=Create }
      { 2       ;1   ;Action    ;
                      CaptionML=[ENU=Inventory Put-&away;
                                 PTB=Novo];
                      RunObject=Page 7375;
                      Promoted=No;
                      Image=CreatePutAway;
                      PromotedCategory=Process;
                      RunPageMode=Create }
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 67      ;1   ;Separator ;
                      CaptionML=[ENU=Tasks;
                                 PTB=Tarefas];
                      IsHeader=Yes }
      { 7       ;1   ;Action    ;
                      CaptionML=[ENU=Consumptio&n Journal;
                                 PTB=Di�rios Consumo];
                      RunObject=Page 99000846;
                      Image=ConsumptionJournal }
      { 8       ;1   ;Action    ;
                      CaptionML=[ENU=Output &Journal;
                                 PTB=Di�rios Sa�da];
                      RunObject=Page 99000823;
                      Image=OutputJournal }
      { 9       ;1   ;Action    ;
                      CaptionML=[ENU=&Capacity Journal;
                                 PTB=Di�rios Capacidade];
                      RunObject=Page 99000773;
                      Image=CapacityJournal }
      { 6       ;1   ;Separator  }
      { 25      ;1   ;Action    ;
                      CaptionML=[ENU=Register Absence - &Machine Center;
                                 PTB=Registrar Aus�ncia - Centro M�quina];
                      RunObject=Report 99003800;
                      Image=CalendarMachine }
      { 26      ;1   ;Action    ;
                      CaptionML=[ENU=Register Absence - &Work Center;
                                 PTB=Registrar Aus�ncia - Centro Trabalho];
                      RunObject=Report 99003805;
                      Image=CalendarWorkcenter }
    }
  }
  CONTROLS
  {
    { 1900000008;0;Container;
                ContainerType=RoleCenterArea }

    { 1900724808;1;Group   }

    { 1900316508;2;Part   ;
                PagePartID=Page9047;
                PartType=Page }

    { 1900724708;1;Group   }

    { 52006501;2;Part     ;
                Name=ApproveEntriesBR;
                PagePartID=Page52113087;
                PartType=Page }

    { 3   ;2   ;Part      ;
                PagePartID=Page675;
                Visible=false;
                PartType=Page }

    { 1905989608;2;Part   ;
                PagePartID=Page9152;
                PartType=Page }

    { 5   ;2   ;Part      ;
                PagePartID=Page681;
                Visible=FALSE;
                PartType=Page }

    { 1901377608;2;Part   ;
                PartType=System;
                SystemPartID=MyNotes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

