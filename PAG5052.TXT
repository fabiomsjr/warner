OBJECT Page 5052 Contact List
{
  OBJECT-PROPERTIES
  {
    Date=09/09/14;
    Time=12:00:00;
    Version List=NAVW18.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Contact List;
               PTB=Lista Contatos];
    SourceTable=Table5050;
    SourceTableView=SORTING(Company Name,Company No.,Type,Name);
    DataCaptionFields=Company No.;
    PageType=List;
    CardPageID=Contact Card;
    OnAfterGetRecord=BEGIN
                       EnableFields;
                       StyleIsStrong := Type = Type::Company;

                       NameIndent := 0;
                       IF Type <> Type::Company THEN BEGIN
                         Cont.SETCURRENTKEY("Company Name","Company No.",Type,Name);
                         IF ("Company No." <> '') AND (NOT HASFILTER) AND (NOT MARKEDONLY) AND (CURRENTKEY = Cont.CURRENTKEY) THEN
                           NameIndent := 1
                       END;
                     END;

    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 29      ;1   ;ActionGroup;
                      CaptionML=[ENU=C&ontact;
                                 PTB=C&ontato];
                      Image=ContactPerson }
      { 34      ;2   ;ActionGroup;
                      CaptionML=[ENU=Comp&any;
                                 PTB=Empresa];
                      Enabled=CompanyGroupEnabled;
                      Image=Company }
      { 35      ;3   ;Action    ;
                      CaptionML=[ENU=Business Relations;
                                 PTB=Rela��es de Neg�cios];
                      RunObject=Page 5061;
                      RunPageLink=Contact No.=FIELD(Company No.);
                      Image=BusinessRelation }
      { 36      ;3   ;Action    ;
                      CaptionML=[ENU=Industry Groups;
                                 PTB=Grupos Industriais];
                      RunObject=Page 5067;
                      RunPageLink=Contact No.=FIELD(Company No.);
                      Image=IndustryGroups }
      { 37      ;3   ;Action    ;
                      CaptionML=[ENU=Web Sources;
                                 PTB=Web];
                      RunObject=Page 5070;
                      RunPageLink=Contact No.=FIELD(Company No.);
                      Image=Web }
      { 38      ;2   ;ActionGroup;
                      CaptionML=[ENU=P&erson;
                                 PTB=Contato];
                      Enabled=PersonGroupEnabled;
                      Image=User }
      { 39      ;3   ;Action    ;
                      CaptionML=[ENU=Job Responsibilities;
                                 PTB=Responsabilidades Projeto];
                      Image=Job;
                      OnAction=VAR
                                 ContJobResp@1001 : Record 5067;
                               BEGIN
                                 TESTFIELD(Type,Type::Person);
                                 ContJobResp.SETRANGE("Contact No.","No.");
                                 PAGE.RUNMODAL(PAGE::"Contact Job Responsibilities",ContJobResp);
                               END;
                                }
      { 41      ;2   ;Action    ;
                      CaptionML=[ENU=Pro&files;
                                 PTB=Perfis];
                      Image=Answers;
                      OnAction=VAR
                                 ProfileManagement@1001 : Codeunit 5059;
                               BEGIN
                                 ProfileManagement.ShowContactQuestionnaireCard(Rec,'',0);
                               END;
                                }
      { 43      ;2   ;Action    ;
                      CaptionML=[ENU=&Picture;
                                 PTB=Imagem];
                      RunObject=Page 5104;
                      RunPageLink=No.=FIELD(No.);
                      Image=Picture }
      { 44      ;2   ;Action    ;
                      CaptionML=[ENU=Co&mments;
                                 PTB=Co&ment�rios];
                      RunObject=Page 5072;
                      RunPageLink=Table Name=CONST(Contact),
                                  No.=FIELD(No.),
                                  Sub No.=CONST(0);
                      Image=ViewComments }
      { 45      ;2   ;ActionGroup;
                      CaptionML=[ENU=Alternati&ve Address;
                                 PTB=Endere�o Alternativo];
                      Image=Addresses }
      { 46      ;3   ;Action    ;
                      CaptionML=[ENU=Card;
                                 PTB=Cart�o];
                      RunObject=Page 5057;
                      RunPageLink=Contact No.=FIELD(No.);
                      Image=EditLines }
      { 47      ;3   ;Action    ;
                      CaptionML=[ENU=Date Ranges;
                                 PTB=Intervalo de Datas];
                      RunObject=Page 5059;
                      RunPageLink=Contact No.=FIELD(No.);
                      Image=DateRange }
      { 48      ;2   ;Separator ;
                      CaptionML=[ENU="";
                                 PTB=""] }
      { 5       ;1   ;ActionGroup;
                      CaptionML=[ENU=Related Information;
                                 PTB=Informa��o Relacionada];
                      Image=Users }
      { 33      ;2   ;Action    ;
                      CaptionML=[ENU=Relate&d Contacts;
                                 PTB=Contatos Relacionados];
                      RunObject=Page 5052;
                      RunPageLink=Company No.=FIELD(Company No.);
                      Image=Users }
      { 55      ;2   ;Action    ;
                      CaptionML=[ENU=Segmen&ts;
                                 PTB=Segmentos];
                      RunObject=Page 5150;
                      RunPageView=SORTING(Contact No.,Segment No.);
                      RunPageLink=Contact Company No.=FIELD(Company No.),
                                  Contact No.=FILTER(<>''),
                                  Contact No.=FIELD(FILTER(Lookup Contact No.));
                      Image=Segment }
      { 40      ;2   ;Action    ;
                      CaptionML=[ENU=Mailing &Groups;
                                 PTB=Grupos de Contatos];
                      RunObject=Page 5064;
                      RunPageLink=Contact No.=FIELD(No.);
                      Image=DistributionGroup }
      { 53      ;2   ;Action    ;
                      CaptionML=[ENU=C&ustomer/Vendor/Bank Acc.;
                                 PTB=Cliente/Fornecedor/Cta Banc�ria];
                      Image=ContactReference;
                      OnAction=BEGIN
                                 ShowCustVendBank;
                               END;
                                }
      { 7       ;1   ;ActionGroup;
                      CaptionML=[ENU=Tasks;
                                 PTB=Tarefas];
                      Image=Task }
      { 50      ;2   ;Action    ;
                      CaptionML=[ENU=T&o-dos;
                                 PTB=Tarefas];
                      RunObject=Page 5096;
                      RunPageView=SORTING(Contact Company No.,Contact No.);
                      RunPageLink=Contact Company No.=FIELD(Company No.),
                                  Contact No.=FIELD(FILTER(Lookup Contact No.)),
                                  System To-do Type=FILTER(Contact Attendee);
                      Image=TaskList }
      { 3       ;2   ;Action    ;
                      CaptionML=[ENU=Oppo&rtunities;
                                 PTB=Opo&rtunidades];
                      RunObject=Page 5123;
                      RunPageView=SORTING(Contact Company No.,Contact No.);
                      RunPageLink=Contact Company No.=FIELD(Company No.),
                                  Contact No.=FILTER(<>''),
                                  Contact No.=FIELD(FILTER(Lookup Contact No.));
                      Image=OpportunityList }
      { 52      ;2   ;Separator ;
                      CaptionML=[ENU="";
                                 PTB=""] }
      { 9       ;1   ;ActionGroup;
                      CaptionML=ENU=Documents;
                      Image=Documents }
      { 68      ;2   ;Action    ;
                      CaptionML=[ENU=Sales &Quotes;
                                 PTB=Cota��es Vendas];
                      RunObject=Page 9300;
                      RunPageView=SORTING(Document Type,Sell-to Contact No.);
                      RunPageLink=Sell-to Contact No.=FIELD(No.);
                      Image=Quote }
      { 69      ;2   ;Separator  }
      { 11      ;1   ;ActionGroup;
                      CaptionML=[ENU=History;
                                 PTB=Hist�rico];
                      Image=History }
      { 76      ;2   ;Action    ;
                      CaptionML=[ENU=Postponed &Interactions;
                                 PTB=Intera��es Registradas];
                      RunObject=Page 5082;
                      RunPageView=SORTING(Contact Company No.,Contact No.);
                      RunPageLink=Contact Company No.=FIELD(Company No.),
                                  Contact No.=FILTER(<>''),
                                  Contact No.=FIELD(FILTER(Lookup Contact No.));
                      Image=PostponedInteractions }
      { 49      ;2   ;Action    ;
                      ShortCutKey=Ctrl+F7;
                      CaptionML=[ENU=Interaction Log E&ntries;
                                 PTB=Log de Intera��es];
                      RunObject=Page 5076;
                      RunPageView=SORTING(Contact Company No.,Contact No.);
                      RunPageLink=Contact Company No.=FIELD(Company No.),
                                  Contact No.=FILTER(<>''),
                                  Contact No.=FIELD(FILTER(Lookup Contact No.));
                      Image=InteractionLog }
      { 42      ;2   ;Action    ;
                      ShortCutKey=F7;
                      CaptionML=[ENU=Statistics;
                                 PTB=Estat�sticas];
                      RunObject=Page 5053;
                      RunPageLink=No.=FIELD(No.);
                      Promoted=Yes;
                      Image=Statistics;
                      PromotedCategory=Process }
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 30      ;1   ;ActionGroup;
                      CaptionML=[ENU=F&unctions;
                                 PTB=F&un��es];
                      Image=Action }
      { 54      ;2   ;Action    ;
                      CaptionML=[ENU=Make &Phone Call;
                                 PTB=Liga��es Telef�nicas Realzadas];
                      Image=Calls;
                      OnAction=VAR
                                 TAPIManagement@1001 : Codeunit 5053;
                               BEGIN
                                 TAPIManagement.DialContCustVendBank(DATABASE::Contact,"No.","Phone No.",'');
                               END;
                                }
      { 56      ;2   ;Action    ;
                      CaptionML=[ENU=Launch &Web Source;
                                 PTB=Iniciar Busca Web];
                      Image=LaunchWeb;
                      OnAction=VAR
                                 ContactWebSource@1001 : Record 5060;
                               BEGIN
                                 ContactWebSource.SETRANGE("Contact No.","Company No.");
                                 IF PAGE.RUNMODAL(PAGE::"Web Source Launch",ContactWebSource) = ACTION::LookupOK THEN
                                   ContactWebSource.Launch;
                               END;
                                }
      { 57      ;2   ;Action    ;
                      CaptionML=[ENU=Print Cover &Sheet;
                                 PTB=Imprimir Capa Trabalho];
                      Image=PrintCover;
                      OnAction=VAR
                                 Cont@1001 : Record 5050;
                               BEGIN
                                 Cont := Rec;
                                 Cont.SETRECFILTER;
                                 REPORT.RUN(REPORT::"Contact - Cover Sheet",TRUE,FALSE,Cont);
                               END;
                                }
      { 58      ;2   ;ActionGroup;
                      CaptionML=[ENU=Create as;
                                 PTB=Criar Como];
                      Image=CustomerContact }
      { 59      ;3   ;Action    ;
                      CaptionML=[ENU=Customer;
                                 PTB=Cliente];
                      Image=Customer;
                      OnAction=BEGIN
                                 CreateCustomer(ChooseCustomerTemplate);
                               END;
                                }
      { 60      ;3   ;Action    ;
                      Name=Vendor;
                      CaptionML=[ENU=Vendor;
                                 PTB=Fornecedor];
                      Image=Vendor;
                      OnAction=BEGIN
                                 CreateVendor;
                               END;
                                }
      { 61      ;3   ;Action    ;
                      AccessByPermission=TableData 270=R;
                      CaptionML=[ENU=Bank;
                                 PTB=Banco];
                      Image=Bank;
                      OnAction=BEGIN
                                 CreateBankAccount;
                               END;
                                }
      { 62      ;2   ;ActionGroup;
                      CaptionML=[ENU=Link with existing;
                                 PTB=Ligar com Cadastro Existente];
                      Image=Links }
      { 63      ;3   ;Action    ;
                      CaptionML=[ENU=Customer;
                                 PTB=Cliente];
                      Image=Customer;
                      OnAction=BEGIN
                                 CreateCustomerLink;
                               END;
                                }
      { 64      ;3   ;Action    ;
                      CaptionML=[ENU=Vendor;
                                 PTB=Fornecedor];
                      Image=Vendor;
                      OnAction=BEGIN
                                 CreateVendorLink;
                               END;
                                }
      { 65      ;3   ;Action    ;
                      AccessByPermission=TableData 270=R;
                      CaptionML=[ENU=Bank;
                                 PTB=Banco];
                      Image=Bank;
                      OnAction=BEGIN
                                 CreateBankAccountLink;
                               END;
                                }
      { 31      ;1   ;Action    ;
                      AccessByPermission=TableData 5062=R;
                      CaptionML=[ENU=Create &Interact;
                                 PTB=Nova Intera��o];
                      Promoted=Yes;
                      Image=CreateInteraction;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 CreateInteraction;
                               END;
                                }
      { 1900000005;0 ;ActionContainer;
                      ActionContainerType=NewDocumentItems }
      { 1900900305;1 ;Action    ;
                      CaptionML=[ENU=New Sales Quote;
                                 PTB=Nova Cota��o Vendas];
                      RunObject=Page 41;
                      RunPageLink=Sell-to Contact No.=FIELD(No.);
                      Promoted=Yes;
                      Image=Quote;
                      PromotedCategory=New;
                      RunPageMode=Create }
      { 1900000006;0 ;ActionContainer;
                      ActionContainerType=Reports }
      { 1907415106;1 ;Action    ;
                      CaptionML=[ENU=Contact Cover Sheet;
                                 PTB=Capa de Trabalho Contato];
                      Promoted=No;
                      Image=Report;
                      PromotedCategory=Report;
                      OnAction=BEGIN
                                 Cont := Rec;
                                 Cont.SETRECFILTER;
                                 REPORT.RUN(REPORT::"Contact - Cover Sheet",TRUE,FALSE,Cont);
                               END;
                                }
      { 1907778706;1 ;Action    ;
                      CaptionML=[ENU=Contact Company Summary;
                                 PTB=Resumo da Empresa];
                      RunObject=Report 5051;
                      Promoted=Yes;
                      Image=Report;
                      PromotedCategory=Report }
      { 1904205506;1 ;Action    ;
                      CaptionML=[ENU=Contact Labels;
                                 PTB=Etiquetas de Contato];
                      RunObject=Report 5056;
                      Promoted=No;
                      Image=Report;
                      PromotedCategory=Report }
      { 1905922906;1 ;Action    ;
                      CaptionML=[ENU=Questionnaire Handout;
                                 PTB=Question�rio];
                      RunObject=Report 5066;
                      Promoted=No;
                      Image=Report;
                      PromotedCategory=Report }
      { 1900800206;1 ;Action    ;
                      CaptionML=[ENU=Sales Cycle Analysis;
                                 PTB=An�lise Ciclo de Vendas];
                      RunObject=Report 5062;
                      Promoted=Yes;
                      Image=Report;
                      PromotedCategory=Report }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                IndentationColumnName=NameIndent;
                IndentationControls=Name;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                SourceExpr="No.";
                Style=Strong;
                StyleExpr=StyleIsStrong }

    { 4   ;2   ;Field     ;
                SourceExpr=Name;
                Style=Strong;
                StyleExpr=StyleIsStrong }

    { 66  ;2   ;Field     ;
                SourceExpr="Company Name";
                Visible=FALSE }

    { 6   ;2   ;Field     ;
                SourceExpr="Post Code";
                Visible=FALSE }

    { 8   ;2   ;Field     ;
                SourceExpr="Country/Region Code";
                Visible=FALSE }

    { 10  ;2   ;Field     ;
                SourceExpr="Phone No." }

    { 24  ;2   ;Field     ;
                SourceExpr="Mobile Phone No.";
                Visible=FALSE }

    { 12  ;2   ;Field     ;
                SourceExpr="Fax No.";
                Visible=FALSE }

    { 14  ;2   ;Field     ;
                SourceExpr="Salesperson Code" }

    { 16  ;2   ;Field     ;
                SourceExpr="Territory Code" }

    { 18  ;2   ;Field     ;
                SourceExpr="Currency Code";
                Visible=FALSE }

    { 20  ;2   ;Field     ;
                SourceExpr="Language Code";
                Visible=FALSE }

    { 22  ;2   ;Field     ;
                SourceExpr="Search Name" }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=TRUE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=TRUE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      Cont@1000 : Record 5050;
      StyleIsStrong@1001 : Boolean INDATASET;
      NameIndent@1002 : Integer INDATASET;
      CompanyGroupEnabled@1004 : Boolean;
      PersonGroupEnabled@1003 : Boolean;

    LOCAL PROCEDURE EnableFields@1();
    BEGIN
      CompanyGroupEnabled := Type = Type::Company;
      PersonGroupEnabled := Type = Type::Person;
    END;

    BEGIN
    END.
  }
}

