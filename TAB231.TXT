OBJECT Table 231 Reason Code
{
  OBJECT-PROPERTIES
  {
    Date=07/09/12;
    Time=12:00:00;
    Version List=NAVW17.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Reason Code;
               PTB=Cod. Raz�o];
    LookupPageID=Page259;
  }
  FIELDS
  {
    { 1   ;   ;Code                ;Code10        ;CaptionML=[ENU=Code;
                                                              PTB=C�digo];
                                                   NotBlank=Yes }
    { 2   ;   ;Description         ;Text50        ;CaptionML=[ENU=Description;
                                                              PTB=Descri��o] }
    { 5900;   ;Date Filter         ;Date          ;FieldClass=FlowFilter;
                                                   CaptionML=[ENU=Date Filter;
                                                              PTB=Filtro Data] }
    { 5901;   ;Contract Gain/Loss Amount;Decimal  ;FieldClass=FlowField;
                                                   CalcFormula=Sum("Contract Gain/Loss Entry".Amount WHERE (Reason Code=FIELD(Code),
                                                                                                            Change Date=FIELD(Date Filter)));
                                                   CaptionML=[ENU=Contract Gain/Loss Amount;
                                                              PTB=Ganhos/Perdas Valor Contrato];
                                                   Editable=No;
                                                   AutoFormatType=1 }
  }
  KEYS
  {
    {    ;Code                                    ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

