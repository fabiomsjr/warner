OBJECT Page 9307 Purchase Order List
{
  OBJECT-PROPERTIES
  {
    Date=13/08/15;
    Time=14:49:15;
    Version List=NAVW18.00.00.40459,NAVBR7;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Purchase Orders;
               PTB=Lista Pedido Compra];
    SourceTable=Table38;
    SourceTableView=WHERE(Document Type=CONST(Order),
                          Prepayment Order=CONST(No));
    PageType=List;
    CardPageID=Purchase Order;
    OnOpenPage=VAR
                 PurchasesPayablesSetup@1000 : Record 312;
               BEGIN
                 SetSecurityFilterOnRespCenter;

                 JobQueueActive := PurchasesPayablesSetup.JobQueueActive;
               END;

    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 1102601026;1 ;ActionGroup;
                      CaptionML=[ENU=O&rder;
                                 PTB=&Pedido];
                      Image=Order }
      { 1102601035;2 ;Action    ;
                      AccessByPermission=TableData 348=R;
                      ShortCutKey=Shift+Ctrl+D;
                      CaptionML=[ENU=Dimensions;
                                 PTB=Dimens�es];
                      Promoted=No;
                      PromotedIsBig=No;
                      Image=Dimensions;
                      OnAction=BEGIN
                                 ShowDocDim;
                               END;
                                }
      { 1102601028;2 ;Action    ;
                      Name=Statistics;
                      ShortCutKey=F7;
                      CaptionML=[ENU=Statistics;
                                 PTB=Estat�sticas];
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=Statistics;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 OpenPurchaseOrderStatistics;
                               END;
                                }
      { 1102601036;2 ;Action    ;
                      CaptionML=[ENU=Approvals;
                                 PTB=Aprova��es];
                      Promoted=No;
                      PromotedIsBig=No;
                      Image=Approvals;
                      OnAction=VAR
                                 ApprovalEntries@1001 : Page 658;
                               BEGIN
                                 ApprovalEntries.Setfilters(DATABASE::"Purchase Header","Document Type","No.");
                                 ApprovalEntries.RUN;
                               END;
                                }
      { 1102601030;2 ;Action    ;
                      CaptionML=[ENU=Co&mments;
                                 PTB=Co&ment�rios];
                      RunObject=Page 66;
                      RunPageLink=Document Type=FIELD(Document Type),
                                  No.=FIELD(No.),
                                  Document Line No.=CONST(0);
                      Image=ViewComments }
      { 7       ;1   ;ActionGroup;
                      CaptionML=[ENU=Documents;
                                 PTB=Documentos];
                      Image=Documents }
      { 1102601031;2 ;Action    ;
                      CaptionML=[ENU=Receipts;
                                 PTB=Recebimentos];
                      RunObject=Page 145;
                      RunPageView=SORTING(Order No.);
                      RunPageLink=Order No.=FIELD(No.);
                      Promoted=No;
                      PromotedIsBig=No;
                      Image=PostedReceipts }
      { 1102601032;2 ;Action    ;
                      CaptionML=[ENU=Invoices;
                                 PTB=Notas Fiscais];
                      RunObject=Page 146;
                      RunPageView=SORTING(Order No.);
                      RunPageLink=Order No.=FIELD(No.);
                      Promoted=No;
                      PromotedIsBig=No;
                      Image=Invoice }
      { 1102601033;2 ;Action    ;
                      CaptionML=[ENU=Prepa&yment Invoices;
                                 PTB=Notas Fiscais Adiantamento];
                      RunObject=Page 146;
                      RunPageView=SORTING(Prepayment Order No.);
                      RunPageLink=Prepayment Order No.=FIELD(No.);
                      Image=PrepaymentInvoice }
      { 1102601034;2 ;Action    ;
                      CaptionML=[ENU=Prepayment Credi&t Memos;
                                 PTB=Adiantamento Notas Cr�dito];
                      RunObject=Page 147;
                      RunPageView=SORTING(Prepayment Order No.);
                      RunPageLink=Prepayment Order No.=FIELD(No.);
                      Image=PrepaymentCreditMemo }
      { 1102601037;2 ;Separator  }
      { 8       ;1   ;ActionGroup;
                      CaptionML=[ENU=Warehouse;
                                 PTB=Armaz�m];
                      Image=Warehouse }
      { 1102601039;2 ;Action    ;
                      CaptionML=[ENU=In&vt. Put-away/Pick Lines;
                                 PTB=Linhas Inv.Sa�da / Picking];
                      RunObject=Page 5774;
                      RunPageView=SORTING(Source Document,Source No.,Location Code);
                      RunPageLink=Source Document=CONST(Purchase Order),
                                  Source No.=FIELD(No.);
                      Image=PickLines }
      { 1102601038;2 ;Action    ;
                      CaptionML=[ENU=Whse. Receipt Lines;
                                 PTB=Linhas Recep��o Dep�sito];
                      RunObject=Page 7342;
                      RunPageView=SORTING(Source Type,Source Subtype,Source No.,Source Line No.);
                      RunPageLink=Source Type=CONST(39),
                                  Source Subtype=FIELD(Document Type),
                                  Source No.=FIELD(No.);
                      Image=ReceiptLines }
      { 1102601040;2 ;Separator  }
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 9       ;1   ;ActionGroup;
                      CaptionML=[ENU=General;
                                 PTB=Geral];
                      Image=Print }
      { 55      ;2   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[ENU=&Print;
                                 PTB=&Imprimir];
                      Promoted=Yes;
                      Image=Print;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 DocPrint.PrintPurchHeader(Rec);
                               END;
                                }
      { 10      ;1   ;ActionGroup;
                      CaptionML=[ENU=Release;
                                 PTB=Liberar];
                      Image=ReleaseDoc }
      { 1102601021;2 ;Action    ;
                      ShortCutKey=Ctrl+F9;
                      CaptionML=[ENU=Re&lease;
                                 PTB=&Liberar];
                      Image=ReleaseDoc;
                      OnAction=VAR
                                 ReleasePurchDoc@1000 : Codeunit 415;
                               BEGIN
                                 ReleasePurchDoc.PerformManualRelease(Rec);
                               END;
                                }
      { 1102601022;2 ;Action    ;
                      CaptionML=[ENU=Re&open;
                                 PTB=Re&abrir];
                      Image=ReOpen;
                      OnAction=VAR
                                 ReleasePurchDoc@1001 : Codeunit 415;
                               BEGIN
                                 ReleasePurchDoc.PerformManualReopen(Rec);
                               END;
                                }
      { 1102601023;2 ;Separator  }
      { 1102601000;1 ;ActionGroup;
                      CaptionML=[ENU=F&unctions;
                                 PTB=F&un��es];
                      Image=Action }
      { 1102601018;2 ;Action    ;
                      CaptionML=[ENU=Send A&pproval Request;
                                 PTB=Enviar Requisi��o A&prova��o];
                      Image=SendApprovalRequest;
                      OnAction=VAR
                                 ApprovalMgt@1001 : Codeunit 439;
                               BEGIN
                                 IF ApprovalMgt.SendPurchaseApprovalRequest(Rec) THEN;
                               END;
                                }
      { 1102601019;2 ;Action    ;
                      CaptionML=[ENU=Cancel Approval Re&quest;
                                 PTB=Cancelar Re&quisi��o Aprova��o];
                      Image=Cancel;
                      OnAction=VAR
                                 ApprovalMgt@1001 : Codeunit 439;
                               BEGIN
                                 IF ApprovalMgt.CancelPurchaseApprovalRequest(Rec,TRUE,TRUE) THEN;
                               END;
                                }
      { 1102601020;2 ;Separator  }
      { 1102601025;2 ;Action    ;
                      AccessByPermission=TableData 410=R;
                      CaptionML=[ENU=Send IC Purchase Order;
                                 PTB=Enviar Pedido Compra IC];
                      Image=IntercompanyOrder;
                      OnAction=VAR
                                 ICInOutboxMgt@1000 : Codeunit 427;
                                 SalesHeader@1002 : Record 36;
                                 ApprovalMgt@1003 : Codeunit 439;
                               BEGIN
                                 IF ApprovalMgt.PrePostApprovalCheck(SalesHeader,Rec) THEN
                                   ICInOutboxMgt.SendPurchDoc(Rec,FALSE);
                               END;
                                }
      { 12      ;1   ;ActionGroup;
                      CaptionML=[ENU=Warehouse;
                                 PTB=Armaz�m];
                      Image=Warehouse }
      { 1102601015;2 ;Action    ;
                      AccessByPermission=TableData 7316=R;
                      CaptionML=[ENU=Create &Whse. Receipt;
                                 PTB=Criar Recibo Dep�sito];
                      Image=NewReceipt;
                      OnAction=VAR
                                 GetSourceDocInbound@1001 : Codeunit 5751;
                               BEGIN
                                 GetSourceDocInbound.CreateFromPurchOrder(Rec);

                                 IF NOT FIND('=><') THEN
                                   INIT;
                               END;
                                }
      { 1102601016;2 ;Action    ;
                      AccessByPermission=TableData 7340=R;
                      Ellipsis=Yes;
                      CaptionML=[ENU=Create Inventor&y Put-away/Pick;
                                 PTB=Criar Armazenamento / Requisi��o Invent�rio];
                      Image=CreatePutawayPick;
                      OnAction=BEGIN
                                 CreateInvtPutAwayPick;

                                 IF NOT FIND('=><') THEN
                                   INIT;
                               END;
                                }
      { 1102601017;2 ;Separator  }
      { 50      ;1   ;ActionGroup;
                      CaptionML=[ENU=P&osting;
                                 PTB=Re&gistro];
                      Image=Post }
      { 51      ;2   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[ENU=Test Report;
                                 PTB=Relat�rio Verifica��o];
                      Image=TestReport;
                      OnAction=BEGIN
                                 ReportPrint.PrintPurchHeader(Rec);
                               END;
                                }
      { 52      ;2   ;Action    ;
                      ShortCutKey=F9;
                      Ellipsis=Yes;
                      CaptionML=[ENU=P&ost;
                                 PTB=Re&gistrar];
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=PostOrder;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 SendToPosting(CODEUNIT::"Purch.-Post (Yes/No)");
                               END;
                                }
      { 53      ;2   ;Action    ;
                      ShortCutKey=Shift+F9;
                      Ellipsis=Yes;
                      CaptionML=[ENU=Post and &Print;
                                 PTB=Registrar e &Imprimir];
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=PostPrint;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 SendToPosting(CODEUNIT::"Purch.-Post + Print");
                               END;
                                }
      { 54      ;2   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[ENU=Post &Batch;
                                 PTB=Registrar por Lote];
                      Promoted=Yes;
                      Image=PostBatch;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 REPORT.RUNMODAL(REPORT::"Batch Post Purchase Orders",TRUE,TRUE,Rec);
                                 CurrPage.UPDATE(FALSE);
                               END;
                                }
      { 3       ;2   ;Action    ;
                      CaptionML=[ENU=Remove From Job Queue;
                                 PTB=Remover da Job Queue];
                      Visible=JobQueueActive;
                      Image=RemoveLine;
                      OnAction=BEGIN
                                 CancelBackgroundPosting;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                SourceExpr="No." }

    { 4   ;2   ;Field     ;
                SourceExpr="Buy-from Vendor No." }

    { 52006500;2;Field    ;
                SourceExpr="Quote No.";
                Visible=FALSE }

    { 13  ;2   ;Field     ;
                SourceExpr="Order Address Code";
                Visible=FALSE }

    { 6   ;2   ;Field     ;
                SourceExpr="Buy-from Vendor Name" }

    { 52006502;2;Field    ;
                SourceExpr="Branch Code" }

    { 52006504;2;Field    ;
                SourceExpr="Fiscal Document Type" }

    { 52006503;2;Field    ;
                SourceExpr="Vendor Invoice No." }

    { 15  ;2   ;Field     ;
                SourceExpr="Vendor Authorization No." }

    { 27  ;2   ;Field     ;
                SourceExpr="Buy-from Post Code";
                Visible=FALSE }

    { 23  ;2   ;Field     ;
                SourceExpr="Buy-from Country/Region Code";
                Visible=FALSE }

    { 35  ;2   ;Field     ;
                SourceExpr="Buy-from Contact";
                Visible=FALSE }

    { 163 ;2   ;Field     ;
                SourceExpr="Pay-to Vendor No.";
                Visible=FALSE }

    { 161 ;2   ;Field     ;
                SourceExpr="Pay-to Name";
                Visible=FALSE }

    { 33  ;2   ;Field     ;
                SourceExpr="Pay-to Post Code";
                Visible=FALSE }

    { 29  ;2   ;Field     ;
                SourceExpr="Pay-to Country/Region Code";
                Visible=FALSE }

    { 151 ;2   ;Field     ;
                SourceExpr="Pay-to Contact";
                Visible=FALSE }

    { 147 ;2   ;Field     ;
                SourceExpr="Ship-to Code";
                Visible=FALSE }

    { 145 ;2   ;Field     ;
                SourceExpr="Ship-to Name";
                Visible=FALSE }

    { 21  ;2   ;Field     ;
                SourceExpr="Ship-to Post Code";
                Visible=FALSE }

    { 17  ;2   ;Field     ;
                SourceExpr="Ship-to Country/Region Code";
                Visible=FALSE }

    { 135 ;2   ;Field     ;
                SourceExpr="Ship-to Contact";
                Visible=FALSE }

    { 131 ;2   ;Field     ;
                SourceExpr="Posting Date";
                Visible=FALSE }

    { 113 ;2   ;Field     ;
                SourceExpr="Shortcut Dimension 1 Code";
                Visible=FALSE;
                OnLookup=BEGIN
                           DimMgt.LookupDimValueCodeNoUpdate(1);
                         END;
                          }

    { 111 ;2   ;Field     ;
                SourceExpr="Shortcut Dimension 2 Code";
                Visible=FALSE;
                OnLookup=BEGIN
                           DimMgt.LookupDimValueCodeNoUpdate(2);
                         END;
                          }

    { 115 ;2   ;Field     ;
                SourceExpr="Location Code";
                Visible=TRUE }

    { 99  ;2   ;Field     ;
                SourceExpr="Purchaser Code";
                Visible=FALSE }

    { 31  ;2   ;Field     ;
                SourceExpr="Assigned User ID" }

    { 11  ;2   ;Field     ;
                SourceExpr="Currency Code";
                Visible=FALSE }

    { 52006501;2;Field    ;
                SourceExpr="Order Date";
                Visible=FALSE }

    { 1102601001;2;Field  ;
                SourceExpr="Document Date";
                Visible=FALSE }

    { 1102601003;2;Field  ;
                SourceExpr=Status;
                Visible=TRUE }

    { 52006505;2;Field    ;
                SourceExpr="Release Date" }

    { 1102601005;2;Field  ;
                SourceExpr="Payment Terms Code";
                Visible=FALSE }

    { 1102601007;2;Field  ;
                SourceExpr="Due Date";
                Visible=FALSE }

    { 1102601009;2;Field  ;
                SourceExpr="Payment Discount %";
                Visible=FALSE }

    { 1102601011;2;Field  ;
                SourceExpr="Payment Method Code";
                Visible=FALSE }

    { 1102601013;2;Field  ;
                SourceExpr="Shipment Method Code";
                Visible=FALSE }

    { 1102601027;2;Field  ;
                SourceExpr="Requested Receipt Date";
                Visible=FALSE }

    { 5   ;2   ;Field     ;
                CaptionML=PTB=Status Job Queue;
                SourceExpr="Job Queue Status";
                Visible=JobQueueActive }

    { 35000000;2;Field    ;
                SourceExpr=Amount }

    { 52006506;2;Field    ;
                SourceExpr="Vendor Order No.";
                Visible=FALSE }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1901138007;1;Part   ;
                SubPageLink=No.=FIELD(Buy-from Vendor No.),
                            Date Filter=FIELD(Date Filter);
                PagePartID=Page9093;
                Visible=TRUE;
                PartType=Page }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=TRUE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      DimMgt@1000 : Codeunit 408;
      ReportPrint@1102601001 : Codeunit 228;
      DocPrint@1102601000 : Codeunit 229;
      JobQueueActive@1003 : Boolean INDATASET;

    BEGIN
    END.
  }
}

