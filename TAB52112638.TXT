OBJECT Table 52112638 NFe XML Authorization
{
  OBJECT-PROPERTIES
  {
    Date=23/10/15;
    Time=15:47:25;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    OnInsert=BEGIN
               ValidateRec;
             END;

    OnModify=BEGIN
               ValidateRec;
             END;

    CaptionML=[ENU=NF-e XML Authorization;
               PTB=Autoriza��o XML NF-e];
    LookupPageID=Page52112647;
    DrillDownPageID=Page52112647;
  }
  FIELDS
  {
    { 10  ;   ;Branch Code         ;Code20        ;TableRelation="Branch Information";
                                                   CaptionML=[ENU=Branch Code;
                                                              PTB=C�d. Filial] }
    { 20  ;   ;Line No.            ;Integer       ;CaptionML=[ENU=Line No.;
                                                              PTB=N� Linha] }
    { 30  ;   ;CNPJCPF             ;Text20        ;OnValidate=VAR
                                                                FiscFunc@52006500 : Codeunit 52112424;
                                                              BEGIN
                                                                IF CNPJCPF <> '' THEN
                                                                  FiscFunc.CheckCNPJCPF(CNPJCPF);
                                                              END;

                                                   CaptionML=[ENU=CNPJ/CPF;
                                                              PTB=CNPJ/CPF] }
    { 40  ;   ;Name                ;Text50        ;CaptionML=[ENU=Name;
                                                              PTB=Nome] }
  }
  KEYS
  {
    {    ;Branch Code,Line No.                    ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    LOCAL PROCEDURE ValidateRec@52006500();
    BEGIN
      TESTFIELD(CNPJCPF);
    END;

    BEGIN
    END.
  }
}

