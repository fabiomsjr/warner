OBJECT Table 52112955 Std. Inv. Legal Text Line
{
  OBJECT-PROPERTIES
  {
    Date=16/05/13;
    Time=10:07:09;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Std. Inv. Legal Text Line;
               PTB="Linhas Textos Adicionais NF "];
    LookupPageID=Page52112924;
    DrillDownPageID=Page52112924;
  }
  FIELDS
  {
    { 1   ;   ;Code                ;Code10        ;TableRelation="Std. Inv. Legal Text Header";
                                                   CaptionML=[ENU=Code;
                                                              PTB=C�digo] }
    { 2   ;   ;Line No.            ;Integer       ;CaptionML=[ENU=Line No.;
                                                              PTB=N� Linha] }
    { 3   ;   ;Text                ;Text100       ;CaptionML=[ENU=Text;
                                                              PTB=Texto] }
  }
  KEYS
  {
    {    ;Code,Line No.                           ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    {
      NAVBR5.01.0109

      No.   dd.mm.yy   Developer   Company   DocNo.   Description
      -----------------------------------------------------------------------------------------------------------------
      00    01.12.06   MK          MICROSOFT LOC001   The reference Documentation file LOCALIZATIONBR.DOC
    }
    END.
  }
}

