OBJECT Table 99003614 Merge Wizard
{
  OBJECT-PROPERTIES
  {
    Date=22/09/04;
    Time=11:55:21;
    Version List=NDT2.00.01;
  }
  PROPERTIES
  {
  }
  FIELDS
  {
    { 1   ;   ;Entry No.           ;Integer        }
    { 2   ;   ;Current Custom Name ;Text30         }
    { 3   ;   ;Current Custom Version No.;Integer  }
    { 4   ;   ;Old Base Name       ;Text30         }
    { 5   ;   ;Old Base Version No.;Integer        }
    { 6   ;   ;New Base Name       ;Text30         }
    { 7   ;   ;New Base Version No.;Integer        }
    { 8   ;   ;New Custom Name     ;Text30         }
    { 9   ;   ;New Custom Version No.;Integer      }
    { 10  ;   ;New Custom Version List;Text80      }
    { 11  ;   ;New Date            ;Date           }
    { 12  ;   ;New Time            ;Time           }
    { 13  ;   ;Version List Option ;Option        ;OptionString=Add,Merge,New }
    { 14  ;   ;Action              ;Option        ;OptionString=Merge,Compare Objects }
    { 15  ;   ;Current Custom File Name;Text250    }
    { 16  ;   ;Old Base File Name  ;Text250        }
    { 17  ;   ;New Base File Name  ;Text250        }
    { 18  ;   ;New Custom Description;Text50       }
    { 19  ;   ;New Custom Description 2;Text50     }
    { 20  ;   ;Merge Process Interrupted;Boolean   }
    { 21  ;   ;Focus on Objects    ;Option        ;OptionString=all Versions,Current Custom Version,New Base Version }
    { 22  ;   ;Import all Objects  ;Option        ;OptionString=No,Yes }
    { 23  ;   ;Changed CC Only     ;Boolean        }
  }
  KEYS
  {
    {    ;Entry No.                               ;MaintainSIFTIndex=No;
                                                   Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

