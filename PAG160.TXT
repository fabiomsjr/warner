OBJECT Page 160 Sales Statistics
{
  OBJECT-PROPERTIES
  {
    Date=07/09/12;
    Time=12:00:00;
    Version List=NAVW17.00,NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Sales Statistics;
               PTB=Estat�sticas Vendas];
    InsertAllowed=No;
    DeleteAllowed=No;
    LinksAllowed=No;
    SourceTable=Table36;
    PageType=ListPlus;
    OnOpenPage=BEGIN
                 SalesSetup.GET;
                 AllowInvDisc :=
                   NOT (SalesSetup."Calc. Inv. Discount" AND CustInvDiscRecExists("Invoice Disc. Code"));
                 AllowVATDifference :=
                   SalesSetup."Allow VAT Difference" AND
                   NOT ("Document Type" IN ["Document Type"::Quote,"Document Type"::"Blanket Order"]);
                 CurrPage.EDITABLE :=
                   AllowVATDifference OR AllowInvDisc;
                 SetVATSpecification;
               END;

    OnAfterGetRecord=VAR
                       SalesLine@1000 : Record 37;
                       TempSalesLine@1001 : TEMPORARY Record 37;
                     BEGIN
                       CurrPage.CAPTION(STRSUBSTNO(Text000,"Document Type"));
                       IF PrevNo = "No." THEN
                         EXIT;
                       PrevNo := "No.";
                       FILTERGROUP(2);
                       SETRANGE("No.",PrevNo);
                       FILTERGROUP(0);
                       CLEAR(SalesLine);
                       CLEAR(TotalSalesLine);
                       CLEAR(TotalSalesLineLCY);
                       CLEAR(SalesPost);

                       SalesPost.GetSalesLines(Rec,TempSalesLine,0);
                       CLEAR(SalesPost);
                       SalesPost.SumSalesLinesTemp(
                         Rec,TempSalesLine,0,TotalSalesLine,TotalSalesLineLCY,
                         VATAmount,VATAmountText,ProfitLCY,ProfitPct,TotalAdjCostLCY);

                       AdjProfitLCY := TotalSalesLineLCY.Amount - TotalAdjCostLCY;
                       IF TotalSalesLineLCY.Amount <> 0 THEN
                         AdjProfitPct := ROUND(AdjProfitLCY / TotalSalesLineLCY.Amount * 100,0.1);

                       IF "Prices Including VAT" THEN BEGIN
                         TotalAmount2 := TotalSalesLine.Amount;
                         TotalAmount1 := TotalAmount2 + VATAmount;
                         TotalSalesLine."Line Amount" := TotalAmount1 + TotalSalesLine."Inv. Discount Amount";
                       END ELSE BEGIN
                         TotalAmount1 := TotalSalesLine.Amount;
                         TotalAmount2 := TotalSalesLine."Amount Including VAT";
                       END;

                       IF Cust.GET("Bill-to Customer No.") THEN
                         Cust.CALCFIELDS("Balance (LCY)")
                       ELSE
                         CLEAR(Cust);
                       IF Cust."Credit Limit (LCY)" = 0 THEN
                         CreditLimitLCYExpendedPct := 0
                       ELSE
                         IF Cust."Balance (LCY)" / Cust."Credit Limit (LCY)" < 0 THEN
                           CreditLimitLCYExpendedPct := 0
                         ELSE
                           IF Cust."Balance (LCY)" / Cust."Credit Limit (LCY)" > 1 THEN
                             CreditLimitLCYExpendedPct := 10000
                           ELSE
                             CreditLimitLCYExpendedPct := ROUND(Cust."Balance (LCY)" / Cust."Credit Limit (LCY)" * 10000,1);

                       SalesLine.CalcVATAmountLines(0,Rec,TempSalesLine,TempVATAmountLine);
                       TempVATAmountLine.MODIFYALL(Modified,FALSE);
                       SetVATSpecification;

                       // > NAVBR
                       CalcTaxes
                       // < NAVBR
                     END;

    OnQueryClosePage=BEGIN
                       GetVATSpecification;
                       IF TempVATAmountLine.GetAnyLineModified THEN
                         UpdateVATOnSalesLines;
                       EXIT(TRUE);
                     END;

  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                CaptionML=[ENU=General;
                           PTB=Geral] }

    { 129 ;2   ;Field     ;
                Name=Amount;
                CaptionML=[ENU=Amount;
                           PTB=Valor];
                SourceExpr=TotalSalesLine."Line Amount";
                AutoFormatType=1;
                AutoFormatExpr="Currency Code";
                CaptionClass=GetCaptionClass(Text002,FALSE);
                Editable=FALSE }

    { 93  ;2   ;Field     ;
                Name=InvDiscountAmount;
                CaptionML=[ENU=Inv. Discount Amount;
                           PTB=Valor Desconto N. Fiscal];
                SourceExpr=TotalSalesLine."Inv. Discount Amount";
                AutoFormatType=1;
                AutoFormatExpr="Currency Code";
                OnValidate=BEGIN
                             UpdateInvDiscAmount;
                           END;
                            }

    { 80  ;2   ;Field     ;
                CaptionML=[ENU=Total;
                           PTB=Total];
                SourceExpr=TotalAmount1;
                AutoFormatType=1;
                AutoFormatExpr="Currency Code";
                CaptionClass=GetCaptionClass(Text001,FALSE);
                OnValidate=BEGIN
                             UpdateTotalAmount;
                           END;
                            }

    { 75  ;2   ;Field     ;
                CaptionML=[ENU=VAT Amount;
                           PTB=Valor IVA];
                SourceExpr=VATAmount;
                AutoFormatType=1;
                AutoFormatExpr="Currency Code";
                CaptionClass=FORMAT(VATAmountText);
                Visible=FALSE;
                Editable=FALSE }

    { 76  ;2   ;Field     ;
                CaptionML=[ENU=Total Incl. VAT;
                           PTB=Total Incl. IVA];
                SourceExpr=TotalAmount2;
                AutoFormatType=1;
                AutoFormatExpr="Currency Code";
                CaptionClass=GetCaptionClass(Text001,TRUE);
                Visible=FALSE;
                Editable=FALSE }

    { 2   ;2   ;Field     ;
                CaptionML=[ENU=Sales (LCY);
                           PTB=Venda (ML)];
                SourceExpr=TotalSalesLineLCY.Amount;
                AutoFormatType=1;
                Editable=FALSE }

    { 79  ;2   ;Field     ;
                CaptionML=[ENU=Original Profit (LCY);
                           PTB=Lucro Original (ML)];
                SourceExpr=ProfitLCY;
                AutoFormatType=1;
                Editable=FALSE }

    { 10  ;2   ;Field     ;
                CaptionML=[ENU=Adjusted Profit (LCY);
                           PTB=Lucro Ajustado (ML)];
                SourceExpr=AdjProfitLCY;
                AutoFormatType=1;
                Editable=FALSE }

    { 81  ;2   ;Field     ;
                CaptionML=[ENU=Original Profit %;
                           PTB=% Lucro Original];
                DecimalPlaces=1:1;
                SourceExpr=ProfitPct;
                Editable=FALSE }

    { 12  ;2   ;Field     ;
                CaptionML=[ENU=Adjusted Profit %;
                           PTB=% Lucro Ajustado];
                DecimalPlaces=1:1;
                SourceExpr=AdjProfitPct;
                Editable=FALSE }

    { 95  ;2   ;Field     ;
                CaptionML=[ENU=Quantity;
                           PTB=Quantidade];
                DecimalPlaces=0:5;
                SourceExpr=TotalSalesLine.Quantity;
                Editable=FALSE }

    { 73  ;2   ;Field     ;
                CaptionML=[ENU=Parcels;
                           PTB=Parcelas];
                DecimalPlaces=0:5;
                SourceExpr=TotalSalesLine."Units per Parcel";
                Editable=FALSE }

    { 91  ;2   ;Field     ;
                CaptionML=[ENU=Net Weight;
                           PTB=Peso Liquido];
                DecimalPlaces=0:5;
                SourceExpr=TotalSalesLine."Net Weight";
                Editable=FALSE }

    { 82  ;2   ;Field     ;
                CaptionML=[ENU=Gross Weight;
                           PTB=Peso Bruto];
                DecimalPlaces=0:5;
                SourceExpr=TotalSalesLine."Gross Weight";
                Editable=FALSE }

    { 71  ;2   ;Field     ;
                CaptionML=[ENU=Volume;
                           PTB=Volume];
                DecimalPlaces=0:5;
                SourceExpr=TotalSalesLine."Unit Volume";
                Editable=FALSE }

    { 78  ;2   ;Field     ;
                CaptionML=[ENU=Original Cost (LCY);
                           PTB=Custo Original (ML)];
                SourceExpr=TotalSalesLineLCY."Unit Cost (LCY)";
                AutoFormatType=1;
                Editable=FALSE }

    { 8   ;2   ;Field     ;
                CaptionML=[ENU=Adjusted Cost (LCY);
                           PTB=Custo Ajustado (ML)];
                SourceExpr=TotalAdjCostLCY;
                AutoFormatType=1;
                Editable=FALSE }

    { 7   ;2   ;Field     ;
                CaptionML=[ENU=Cost Adjmt. Amount (LCY);
                           PTB=Valor Custo Ajustado (ML)];
                SourceExpr=TotalAdjCostLCY - TotalSalesLineLCY."Unit Cost (LCY)";
                AutoFormatType=1;
                Editable=FALSE;
                OnLookup=BEGIN
                           LookupAdjmtValueEntries(0);
                         END;
                          }

    { 52006500;2;Field    ;
                DrillDown=Yes;
                DecimalPlaces=2:2;
                BlankNumbers=BlankZero;
                SourceExpr=EstTaxAmt;
                CaptionClass=EstTaxCaption;
                Editable=FALSE;
                OnDrillDown=VAR
                              salesMgt@52006501 : Codeunit 52112432;
                            BEGIN
                              salesMgt.ShowDetailedSalesHeaderEstimatedTaxAmt(Rec);
                            END;
                             }

    { 5   ;1   ;Part      ;
                Name=SubForm;
                PagePartID=Page576;
                Visible=FALSE }

    { 35000000;1;Part     ;
                Name=Taxes;
                PagePartID=Page52112449;
                PartType=Page }

    { 1903289601;1;Group  ;
                CaptionML=[ENU=Customer;
                           PTB=Cliente] }

    { 67  ;2   ;Field     ;
                CaptionML=[ENU=Balance (LCY);
                           PTB=Saldo (ML)];
                SourceExpr=Cust."Balance (LCY)";
                AutoFormatType=1;
                Editable=FALSE }

    { 68  ;2   ;Field     ;
                CaptionML=[ENU=Credit Limit (LCY);
                           PTB=Limite Cr�dito (ML)];
                SourceExpr=Cust."Credit Limit (LCY)";
                AutoFormatType=1;
                Editable=FALSE }

    { 4   ;2   ;Field     ;
                ExtendedDatatype=Ratio;
                CaptionML=[ENU=Expended % of Credit Limit (LCY);
                           PTB=% Gasta Limite Cr�dito (ML)];
                ToolTipML=[ENU=Expended % of Credit Limit (LCY);
                           PTB=% Gasta do Limite de Cr�dito (ML)];
                SourceExpr=CreditLimitLCYExpendedPct }

  }
  CODE
  {
    VAR
      Text000@1000 : TextConst 'ENU=Sales %1 Statistics;PTB=Estat�sticas %1 Venda';
      Text001@1001 : TextConst 'ENU=Total;PTB=Total';
      Text002@1002 : TextConst 'ENU=Amount;PTB=Valor';
      Text003@1003 : TextConst 'ENU=%1 must not be 0.;PTB=%1 n�o deve ser 0.';
      Text004@1004 : TextConst 'ENU=%1 must not be greater than %2.;PTB=%1 deve ser maior que %2.';
      Text005@1005 : TextConst '@@@=You cannot change the invoice discount because there is a Cust. Invoice Disc. record for Invoice Disc. Code 30000.;ENU=You cannot change the invoice discount because there is a %1 record for %2 %3.;PTB=Voc� n�o pode modificar o desconto nota fiscal porque j� existe um registro %1 para %2 %3.';
      TotalSalesLine@1006 : Record 37;
      TotalSalesLineLCY@1007 : Record 37;
      Cust@1008 : Record 18;
      TempVATAmountLine@1009 : TEMPORARY Record 290;
      SalesSetup@1010 : Record 311;
      SalesPost@1011 : Codeunit 80;
      TotalAmount1@1012 : Decimal;
      TotalAmount2@1013 : Decimal;
      VATAmount@1014 : Decimal;
      VATAmountText@1015 : Text[30];
      ProfitLCY@1016 : Decimal;
      ProfitPct@1017 : Decimal;
      AdjProfitLCY@1024 : Decimal;
      AdjProfitPct@1023 : Decimal;
      TotalAdjCostLCY@1022 : Decimal;
      CreditLimitLCYExpendedPct@1018 : Decimal;
      PrevNo@1019 : Code[20];
      AllowInvDisc@1020 : Boolean;
      AllowVATDifference@1021 : Boolean;
      ">NAVBR"@52006500 : Integer;
      EstTaxAmt@52006501 : Decimal;
      EstTaxCaption@52006502 : Text;

    LOCAL PROCEDURE UpdateHeaderInfo@5();
    VAR
      CurrExchRate@1000 : Record 330;
      UseDate@1001 : Date;
    BEGIN
      TotalSalesLine."Inv. Discount Amount" := TempVATAmountLine.GetTotalInvDiscAmount;
      TotalAmount1 :=
        TotalSalesLine."Line Amount" - TotalSalesLine."Inv. Discount Amount";
      VATAmount := TempVATAmountLine.GetTotalVATAmount;
      IF "Prices Including VAT" THEN BEGIN
        TotalAmount1 := TempVATAmountLine.GetTotalAmountInclVAT;
        TotalAmount2 := TotalAmount1 - VATAmount;
        TotalSalesLine."Line Amount" := TotalAmount1 + TotalSalesLine."Inv. Discount Amount";
      END ELSE
        TotalAmount2 := TotalAmount1 + VATAmount;

      IF "Prices Including VAT" THEN
        TotalSalesLineLCY.Amount := TotalAmount2
      ELSE
        TotalSalesLineLCY.Amount := TotalAmount1;
      IF "Currency Code" <> '' THEN BEGIN
        IF ("Document Type" IN ["Document Type"::"Blanket Order","Document Type"::Quote]) AND
           ("Posting Date" = 0D)
        THEN
          UseDate := WORKDATE
        ELSE
          UseDate := "Posting Date";

        TotalSalesLineLCY.Amount :=
          CurrExchRate.ExchangeAmtFCYToLCY(
            UseDate,"Currency Code",TotalSalesLineLCY.Amount,"Currency Factor");
      END;
      ProfitLCY := TotalSalesLineLCY.Amount - TotalSalesLineLCY."Unit Cost (LCY)";
      IF TotalSalesLineLCY.Amount = 0 THEN
        ProfitPct := 0
      ELSE
        ProfitPct := ROUND(100 * ProfitLCY / TotalSalesLineLCY.Amount,0.01);

      AdjProfitLCY := TotalSalesLineLCY.Amount - TotalAdjCostLCY;
      IF TotalSalesLineLCY.Amount = 0 THEN
        AdjProfitPct := 0
      ELSE
        AdjProfitPct := ROUND(100 * AdjProfitLCY / TotalSalesLineLCY.Amount,0.01);
    END;

    LOCAL PROCEDURE GetVATSpecification@21();
    BEGIN
      CurrPage.SubForm.PAGE.GetTempVATAmountLine(TempVATAmountLine);
      IF TempVATAmountLine.GetAnyLineModified THEN
        UpdateHeaderInfo;
    END;

    LOCAL PROCEDURE SetVATSpecification@11();
    BEGIN
      CurrPage.SubForm.PAGE.SetTempVATAmountLine(TempVATAmountLine);
      CurrPage.SubForm.PAGE.InitGlobals(
        "Currency Code",AllowVATDifference,AllowVATDifference,
        "Prices Including VAT",AllowInvDisc,"VAT Base Discount %");
    END;

    LOCAL PROCEDURE UpdateTotalAmount@16();
    VAR
      SaveTotalAmount@1000 : Decimal;
    BEGIN
      CheckAllowInvDisc;
      IF "Prices Including VAT" THEN BEGIN
        SaveTotalAmount := TotalAmount1;
        UpdateInvDiscAmount;
        TotalAmount1 := SaveTotalAmount;
      END;
      WITH TotalSalesLine DO
        "Inv. Discount Amount" := "Line Amount" - TotalAmount1;
      UpdateInvDiscAmount;
    END;

    LOCAL PROCEDURE UpdateInvDiscAmount@3();
    VAR
      InvDiscBaseAmount@1000 : Decimal;
    BEGIN
      CheckAllowInvDisc;
      InvDiscBaseAmount := TempVATAmountLine.GetTotalInvDiscBaseAmount(FALSE,"Currency Code");
      IF InvDiscBaseAmount = 0 THEN
        ERROR(Text003,TempVATAmountLine.FIELDCAPTION("Inv. Disc. Base Amount"));

      IF TotalSalesLine."Inv. Discount Amount" / InvDiscBaseAmount > 1 THEN
        ERROR(
          Text004,
          TotalSalesLine.FIELDCAPTION("Inv. Discount Amount"),
          TempVATAmountLine.FIELDCAPTION("Inv. Disc. Base Amount"));

      TempVATAmountLine.SetInvoiceDiscountAmount(
        TotalSalesLine."Inv. Discount Amount","Currency Code","Prices Including VAT","VAT Base Discount %");
      CurrPage.SubForm.PAGE.SetTempVATAmountLine(TempVATAmountLine);
      UpdateHeaderInfo;

      "Invoice Discount Calculation" := "Invoice Discount Calculation"::Amount;
      "Invoice Discount Value" := TotalSalesLine."Inv. Discount Amount";
      MODIFY;
      UpdateVATOnSalesLines;
    END;

    LOCAL PROCEDURE GetCaptionClass@2(FieldCaption@1000 : Text[100];ReverseCaption@1001 : Boolean) : Text[80];
    BEGIN
      IF "Prices Including VAT" XOR ReverseCaption THEN
        EXIT('2,1,' + FieldCaption);

      EXIT('2,0,' + FieldCaption);
    END;

    LOCAL PROCEDURE UpdateVATOnSalesLines@1();
    VAR
      SalesLine@1000 : Record 37;
    BEGIN
      GetVATSpecification;
      IF TempVATAmountLine.GetAnyLineModified THEN BEGIN
        SalesLine.UpdateVATOnLines(0,Rec,SalesLine,TempVATAmountLine);
        SalesLine.UpdateVATOnLines(1,Rec,SalesLine,TempVATAmountLine);
      END;
      PrevNo := '';
    END;

    LOCAL PROCEDURE CustInvDiscRecExists@4(InvDiscCode@1000 : Code[20]) : Boolean;
    VAR
      CustInvDisc@1001 : Record 19;
    BEGIN
      CustInvDisc.SETRANGE(Code,InvDiscCode);
      EXIT(CustInvDisc.FINDFIRST);
    END;

    LOCAL PROCEDURE CheckAllowInvDisc@8();
    VAR
      CustInvDisc@1000 : Record 19;
    BEGIN
      IF NOT AllowInvDisc THEN
        ERROR(
          Text005,
          CustInvDisc.TABLECAPTION,FIELDCAPTION("Invoice Disc. Code"),"Invoice Disc. Code");
    END;

    PROCEDURE "> NAVBR"@35000002();
    BEGIN
    END;

    PROCEDURE CalcTaxes@35000001();
    VAR
      salesMgt@52006500 : Codeunit 52112432;
      brText001@52006501 : TextConst 'ENU=Estimated Tax Amount (%1%);PTB=Valor Aprox. Tributos (%1%)';
    BEGIN
      CurrPage.Taxes.PAGE.CalcSalesDoc(Rec);
      EstTaxAmt := salesMgt.GetSalesHeaderEstimatedTaxAmt(Rec);
      IF EstTaxAmt <> 0 THEN
        EstTaxCaption := STRSUBSTNO(brText001, ROUND(EstTaxAmt / TotalSalesLine."Line Amount" * 100, 0.01));
    END;

    BEGIN
    END.
  }
}

