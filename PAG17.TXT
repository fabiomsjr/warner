OBJECT Page 17 G/L Account Card
{
  OBJECT-PROPERTIES
  {
    Date=21/03/12;
    Time=13:09:57;
    Modified=Yes;
    Version List=NAVW18.00,NAVBR7,WCMW1601_02_3;
  }
  PROPERTIES
  {
    CaptionML=[ENU=G/L Account Card;
               PTB=Cart�o Conta Cont�bil];
    SourceTable=Table15;
    PageType=Card;
    RefreshOnActivate=Yes;
    OnNewRecord=BEGIN
                  SetupNewGLAcc(xRec,BelowxRec);
                END;

    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 36      ;1   ;ActionGroup;
                      CaptionML=[ENU=A&ccount;
                                 PTB=&Conta];
                      Image=ChartOfAccounts }
      { 41      ;2   ;Action    ;
                      ShortCutKey=Ctrl+F7;
                      CaptionML=[ENU=Ledger E&ntries;
                                 PTB=Movimentos];
                      RunObject=Page 20;
                      RunPageView=SORTING(G/L Account No.);
                      RunPageLink=G/L Account No.=FIELD(No.);
                      Promoted=No;
                      Image=GLRegisters;
                      PromotedCategory=Process }
      { 38      ;2   ;Action    ;
                      CaptionML=[ENU=Co&mments;
                                 PTB=Coment�rios];
                      RunObject=Page 124;
                      RunPageLink=Table Name=CONST(G/L Account),
                                  No.=FIELD(No.);
                      Image=ViewComments }
      { 84      ;2   ;Action    ;
                      ShortCutKey=Shift+Ctrl+D;
                      CaptionML=[ENU=Dimensions;
                                 PTB=Dimens�es];
                      RunObject=Page 540;
                      RunPageLink=Table ID=CONST(15),
                                  No.=FIELD(No.);
                      Image=Dimensions }
      { 166     ;2   ;Action    ;
                      CaptionML=[ENU=E&xtended Texts;
                                 PTB=Textos Adicionais];
                      RunObject=Page 391;
                      RunPageView=SORTING(Table Name,No.,Language Code,All Language Codes,Starting Date,Ending Date);
                      RunPageLink=Table Name=CONST(G/L Account),
                                  No.=FIELD(No.);
                      Image=Text }
      { 40      ;2   ;Action    ;
                      CaptionML=[ENU=Receivables-Payables;
                                 PTB=Cobran�as-Pagamentos];
                      RunObject=Page 159;
                      Image=ReceivablesPayables }
      { 47      ;2   ;Action    ;
                      CaptionML=[ENU=Where-Used List;
                                 PTB=Lista Onde � Usado];
                      Image=Track;
                      OnAction=VAR
                                 CalcGLAccWhereUsed@1000 : Codeunit 100;
                               BEGIN
                                 CalcGLAccWhereUsed.CheckGLAcc("No.");
                               END;
                                }
      { 136     ;1   ;ActionGroup;
                      CaptionML=[ENU=&Balance;
                                 PTB=&Saldo];
                      Image=Balance }
      { 46      ;2   ;Action    ;
                      CaptionML=[ENU=G/L &Account Balance;
                                 PTB=S&aldo Conta C/G];
                      RunObject=Page 415;
                      RunPageLink=No.=FIELD(No.),
                                  Global Dimension 1 Filter=FIELD(Global Dimension 1 Filter),
                                  Global Dimension 2 Filter=FIELD(Global Dimension 2 Filter),
                                  Business Unit Filter=FIELD(Business Unit Filter);
                      Image=GLAccountBalance }
      { 154     ;2   ;Action    ;
                      CaptionML=[ENU=G/L &Balance;
                                 PTB=&Saldo Contas];
                      RunObject=Page 414;
                      RunPageOnRec=Yes;
                      Image=GLBalance }
      { 138     ;2   ;Action    ;
                      CaptionML=[ENU=G/L Balance by &Dimension;
                                 PTB=Saldo por &Dimens�o];
                      RunObject=Page 408;
                      Image=GLBalanceDimension }
      { 1906497203;1 ;Action    ;
                      CaptionML=[ENU=General Posting Setup;
                                 PTB=Conf. Registro Geral];
                      RunObject=Page 314;
                      Promoted=Yes;
                      Image=GeneralPostingSetup;
                      PromotedCategory=Process }
      { 1900660103;1 ;Action    ;
                      CaptionML=[ENU=VAT Posting Setup;
                                 PTB=Conf. Registro IVA];
                      RunObject=Page 472;
                      Promoted=No;
                      Image=VATPostingSetup;
                      PromotedCategory=Process }
      { 1900210203;1 ;Action    ;
                      CaptionML=[ENU=G/L Register;
                                 PTB=Registros];
                      RunObject=Page 116;
                      Promoted=Yes;
                      Image=GLRegisters;
                      PromotedCategory=Process }
      { 1900000006;0 ;ActionContainer;
                      ActionContainerType=Reports }
      { 1900670506;1 ;Action    ;
                      CaptionML=[ENU=Detail Trial Balance;
                                 PTB=Balancete Detalhado];
                      RunObject=Report 4;
                      Promoted=Yes;
                      Image=Report;
                      PromotedCategory=Report }
      { 1904082706;1 ;Action    ;
                      CaptionML=[ENU=Trial Balance;
                                 PTB=Balancete];
                      RunObject=Report 6;
                      Promoted=Yes;
                      Image=Report;
                      PromotedCategory=Report }
      { 1902174606;1 ;Action    ;
                      CaptionML=[ENU=Trial Balance by Period;
                                 PTB=Balancete por Per�odo];
                      RunObject=Report 38;
                      Promoted=No;
                      Image=Report;
                      PromotedCategory=Report }
      { 52006504;1   ;Action    ;
                      CaptionML=[ENU=Analytic Journal;
                                 PTB=Raz�o An�litico];
                      RunObject=Report 52112424;
                      Promoted=Yes;
                      Image=Report;
                      PromotedCategory=Report }
      { 52006502;1   ;Action    ;
                      CaptionML=[ENU=Trial Balance (Brazil);
                                 PTB=Balancete (Brasil)];
                      RunObject=Report 52112423;
                      Promoted=Yes;
                      Image=Report;
                      PromotedCategory=Report }
      { 1900210206;1 ;Action    ;
                      CaptionML=[ENU=G/L Register;
                                 PTB=Registros];
                      RunObject=Report 3;
                      Promoted=Yes;
                      Image=GLRegisters;
                      PromotedCategory=Report }
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 61      ;1   ;ActionGroup;
                      CaptionML=[ENU=F&unctions;
                                 PTB=F&un��es];
                      Image=Action }
      { 62      ;2   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[ENU=Apply Template;
                                 PTB=Aplicar Modelo];
                      Image=ApplyTemplate;
                      OnAction=VAR
                                 ConfigTemplateMgt@1000 : Codeunit 8612;
                                 RecRef@1001 : RecordRef;
                               BEGIN
                                 RecRef.GETTABLE(Rec);
                                 ConfigTemplateMgt.UpdateFromTemplateSelection(RecRef);
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                CaptionML=[ENU=General;
                           PTB=Geral] }

    { 2   ;2   ;Field     ;
                SourceExpr="No.";
                Importance=Promoted }

    { 4   ;2   ;Field     ;
                SourceExpr=Name;
                Importance=Promoted }

    { 6   ;2   ;Field     ;
                SourceExpr="Income/Balance";
                Importance=Promoted }

    { 8   ;2   ;Field     ;
                SourceExpr="Debit/Credit" }

    { 10  ;2   ;Field     ;
                SourceExpr="Account Type" }

    { 16  ;2   ;Field     ;
                SourceExpr=Totaling;
                OnLookup=VAR
                           GLAccountList@1000 : Page 18;
                           OldText@1002 : Text;
                         BEGIN
                           OldText := Text;
                           GLAccountList.LOOKUPMODE(TRUE);
                           IF NOT (GLAccountList.RUNMODAL = ACTION::LookupOK) THEN
                             EXIT(FALSE);

                           Text := OldText + GLAccountList.GetSelectionFilter;
                           EXIT(TRUE);
                         END;
                          }

    { 18  ;2   ;Field     ;
                SourceExpr="No. of Blank Lines" }

    { 20  ;2   ;Field     ;
                SourceExpr="New Page" }

    { 22  ;2   ;Field     ;
                SourceExpr="Search Name" }

    { 24  ;2   ;Field     ;
                SourceExpr=Balance;
                Importance=Promoted }

    { 12  ;2   ;Field     ;
                SourceExpr="Reconciliation Account" }

    { 57  ;2   ;Field     ;
                SourceExpr="Automatic Ext. Texts" }

    { 14  ;2   ;Field     ;
                SourceExpr="Direct Posting" }

    { 30  ;2   ;Field     ;
                SourceExpr=Blocked }

    { 32  ;2   ;Field     ;
                SourceExpr="Last Date Modified" }

    { 1102300014;1;Group  ;
                CaptionML=ENU=WCM;
                GroupType=Group }

    { 1102300013;2;Field  ;
                SourceExpr="Group Description";
                Importance=Promoted }

    { 1102300012;2;Field  ;
                SourceExpr="HFM Custom4";
                Importance=Promoted }

    { 1102300011;2;Field  ;
                CaptionML=[ENU=Hyperion No.;
                           ENG=No. 2];
                SourceExpr="No. 2";
                Importance=Promoted }

    { 1102300010;2;Field  ;
                SourceExpr="Group Account";
                Importance=Promoted;
                Editable=GlobalMasterComp }

    { 1102300009;2;Field  ;
                SourceExpr="Group Consolidation" }

    { 1102300008;2;Field  ;
                SourceExpr="HFM Dim Code" }

    { 1102300007;2;Field  ;
                SourceExpr="Old Account Code" }

    { 1102300006;2;Field  ;
                SourceExpr="Source Company";
                Editable=FALSE }

    { 1102300005;2;Field  ;
                SourceExpr="Sync Date";
                Editable=FALSE }

    { 1102300004;2;Field  ;
                SourceExpr="Sync User";
                Editable=FALSE }

    { 1102300003;2;Field  ;
                Name=Balance Account;
                SourceExpr="Balance Account" }

    { 1102300002;2;Field  ;
                SourceExpr="Creation Date" }

    { 7   ;2   ;Field     ;
                SourceExpr="Omit Default Descr. in Jnl." }

    { 1904784501;1;Group  ;
                CaptionML=[ENU=Posting;
                           PTB=Registro];
                GroupType=Group }

    { 26  ;2   ;Field     ;
                SourceExpr="Gen. Posting Type" }

    { 55  ;2   ;Field     ;
                SourceExpr="Gen. Bus. Posting Group";
                Importance=Promoted }

    { 28  ;2   ;Field     ;
                SourceExpr="Gen. Prod. Posting Group";
                Importance=Promoted }

    { 39  ;2   ;Field     ;
                SourceExpr="VAT Bus. Posting Group";
                Importance=Promoted;
                Visible=FALSE }

    { 54  ;2   ;Field     ;
                SourceExpr="VAT Prod. Posting Group";
                Importance=Promoted;
                Visible=FALSE }

    { 59  ;2   ;Field     ;
                SourceExpr="Default IC Partner G/L Acc. No" }

    { 1904602201;1;Group  ;
                CaptionML=[ENU=Consolidation;
                           PTB=Consolidar] }

    { 50  ;2   ;Field     ;
                SourceExpr="Consol. Debit Acc.";
                Importance=Promoted }

    { 52  ;2   ;Field     ;
                SourceExpr="Consol. Credit Acc.";
                Importance=Promoted }

    { 48  ;2   ;Field     ;
                SourceExpr="Consol. Translation Method";
                Importance=Promoted }

    { 1904488501;1;Group  ;
                CaptionML=[ENU=Reporting;
                           PTB=Relat�rios];
                GroupType=Group }

    { 64  ;2   ;Field     ;
                SourceExpr="Exchange Rate Adjustment";
                Importance=Promoted }

    { 3   ;1   ;Group     ;
                CaptionML=[ENU=Cost Accounting;
                           PTB=Contabiliza��o Custos];
                GroupType=Group }

    { 5   ;2   ;Field     ;
                SourceExpr="Cost Type No.";
                Importance=Promoted }

    { 1102300000;1;Group  ;
                CaptionML=[ENU=National;
                           PTB=Nacional];
                GroupType=Group }

    { 1102300001;2;Field  ;
                SourceExpr="Global Account No." }

    { 52006500;2;Group    ;
                CaptionML=[ENU=ECD;
                           PTB=ECD];
                GroupType=Group }

    { 52006501;3;Field    ;
                SourceExpr="Nature Code/Account Group" }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1905532107;1;Part   ;
                SubPageLink=Table ID=CONST(15),
                            No.=FIELD(No.);
                PagePartID=Page9083;
                Visible=FALSE;
                PartType=Page }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=TRUE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      Utilities@1102300001 : Codeunit 60000;
      GlobalMasterComp@1102300000 : Boolean INDATASET;

    BEGIN
    {
      001  09/09/09  JJB WCMFSD003  Group accounts can only be set within the Master Company
      002  27/05/10  JJB WCMFSD020  Add fields to WCM Group
                                    - Source Company
                                    - Sync User
                                    - Sync Date
      003  23/09/10  LDS  WCMFSD026  Added new field 'Balance Account'.
    }
    END.
  }
}

