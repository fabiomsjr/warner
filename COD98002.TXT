OBJECT Codeunit 98002 Data Migration Step2
{
  OBJECT-PROPERTIES
  {
    Date=30/05/16;
    Time=21:28:57;
    Modified=Yes;
    Version List=;
  }
  PROPERTIES
  {
    OnRun=VAR
            GLEntry_UPG@1000000000 : Record 80003;
            GLEntry@1000000001 : Record 17;
            CustLedgerEntry_UPG@1000000002 : Record 80005;
            CustLedgerEntry@1000000003 : Record 21;
            VendorLedgerEntry_UPG@1000000004 : Record 80007;
            VendorLedgerEntry@1000000005 : Record 25;
            PostCode_UPG@1000000006 : Record 80028;
            PostCode@1000000007 : Record 225;
            DetailedVendorLedgEntry_UP@1000000008 : Record 80035;
            DetailedVendorLedgEntry@1000000009 : Record 380;
            RelatedAccountingMov2@1000000010 : Record 35000506;
            RelatedAccountingMov@1000000011 : Record 52112829;
            SPEDAccountingMovLines2@1000000012 : Record 35000519;
            SPEDAccountingMovLines@1000000013 : Record 52112839;
            MemoJournalEntry2@1000000014 : Record 35000527;
            MemoJournalEntry@1000000015 : Record 52112847;
          BEGIN
            IF GLEntry_UPG.FINDSET THEN BEGIN
              REPEAT
                GLEntry.GET(GLEntry_UPG."Entry No.");
                GLEntry."Branch Code" := GLEntry_UPG."Branch Code";
                GLEntry."BR Prepayment" := GLEntry_UPG."BR Prepayment";
                GLEntry.MODIFY;
              UNTIL GLEntry_UPG.NEXT = 0;
            END;

            IF CustLedgerEntry_UPG.FINDSET THEN BEGIN
              REPEAT
                CustLedgerEntry.GET(CustLedgerEntry_UPG."Entry No.");
                CustLedgerEntry."Installment No." := CustLedgerEntry_UPG."Installment No.";
                CustLedgerEntry."BR Prepayment" := CustLedgerEntry_UPG."BR Prepayment";
                CustLedgerEntry.MODIFY;
              UNTIL CustLedgerEntry_UPG.NEXT = 0;
            END;

            IF VendorLedgerEntry_UPG.FINDSET THEN BEGIN
              REPEAT
                VendorLedgerEntry.GET(VendorLedgerEntry_UPG."Entry No.");
                VendorLedgerEntry."Installment No." := VendorLedgerEntry_UPG."Installment No.";
                VendorLedgerEntry."BR Prepayment" := VendorLedgerEntry_UPG."BR Prepayment";
                VendorLedgerEntry."CNAB - Net Amount" := VendorLedgerEntry_UPG."CNAB - Net Amount";
                VendorLedgerEntry.MODIFY;
              UNTIL VendorLedgerEntry_UPG.NEXT = 0;
            END;

            IF PostCode_UPG.FINDSET THEN BEGIN
              REPEAT
                PostCode.GET(PostCode_UPG.Code);
                PostCode.District := PostCode_UPG.District;
                PostCode."Territory Code" := PostCode_UPG."Territory Code";
                PostCode.Address := PostCode_UPG.Address;
                PostCode."Country/Region Code" := PostCode_UPG."Country Code";
                PostCode."IBGE City Code" := PostCode_UPG."IBGE City Code";
                PostCode.MODIFY;
              UNTIL PostCode_UPG.NEXT = 0;
            END;

            IF DetailedVendorLedgEntry_UP.FINDSET THEN BEGIN
              REPEAT
                DetailedVendorLedgEntry.GET(DetailedVendorLedgEntry_UP."Entry No.");
                DetailedVendorLedgEntry."BR Prepayment" := DetailedVendorLedgEntry_UP."BR Prepayment";
                DetailedVendorLedgEntry.MODIFY;
              UNTIL DetailedVendorLedgEntry_UP.NEXT = 0;
            END;

            IF RelatedAccountingMov2.FINDSET THEN BEGIN
              REPEAT
                RelatedAccountingMov.INIT;
                RelatedAccountingMov."Entry No." := RelatedAccountingMov2."Entry No.";
                RelatedAccountingMov."G/L Account No." := RelatedAccountingMov2."G/L Account No.";
                RelatedAccountingMov."Posting Date" := RelatedAccountingMov2."Posting Date";
                RelatedAccountingMov."Document No." := RelatedAccountingMov2."Document No.";
                RelatedAccountingMov.Description := RelatedAccountingMov2.Description;
                RelatedAccountingMov."Bal. Account No." := RelatedAccountingMov2."Bal. Account No.";
                RelatedAccountingMov.Amount := RelatedAccountingMov2.Amount;
                RelatedAccountingMov."Global Dimension 1 Code" := RelatedAccountingMov2."Global Dimension 1 Code";
                RelatedAccountingMov."Debit Amount" := RelatedAccountingMov2."Debit Amount";
                RelatedAccountingMov."Credit Amount" := RelatedAccountingMov2."Credit Amount";
                RelatedAccountingMov."G/L Account No. Rel." := RelatedAccountingMov2."G/L Account No. Rel.";
                RelatedAccountingMov."Transaction Type" := RelatedAccountingMov2."Transaction Type";
                RelatedAccountingMov.INSERT;
              UNTIL RelatedAccountingMov2.NEXT = 0;
            END;

            IF SPEDAccountingMovLines2.FINDSET THEN BEGIN
              REPEAT
                SPEDAccountingMovLines.INIT;
                SPEDAccountingMovLines."Sped No." := SPEDAccountingMovLines2."Sped No.";
                SPEDAccountingMovLines.Level := SPEDAccountingMovLines2.Level;
                SPEDAccountingMovLines.Registered := SPEDAccountingMovLines2.Registered;
                SPEDAccountingMovLines.Text := SPEDAccountingMovLines2.Text;
                SPEDAccountingMovLines."Line No." := SPEDAccountingMovLines2."Line No.";
                SPEDAccountingMovLines.INSERT;
              UNTIL SPEDAccountingMovLines2.NEXT = 0;
            END;

            IF MemoJournalEntry2.FINDSET THEN BEGIN
              REPEAT
                MemoJournalEntry.INIT;
                MemoJournalEntry."Document No." := MemoJournalEntry2."Document No.";
                MemoJournalEntry."Release Date" := MemoJournalEntry2."Release Date";
                MemoJournalEntry."Release Amount" := MemoJournalEntry2."Release Amount";
                MemoJournalEntry."Release Indicator" := MemoJournalEntry2."Release Indicator";
                MemoJournalEntry.INSERT;
              UNTIL MemoJournalEntry2.NEXT = 0;
            END;

            MESSAGE('Completed');
          END;

  }
  CODE
  {

    BEGIN
    END.
  }
}

