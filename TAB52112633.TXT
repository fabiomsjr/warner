OBJECT Table 52112633 NFS-E Batch
{
  OBJECT-PROPERTIES
  {
    Date=18/12/14;
    Time=10:00:00;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=NFS-E Batch;
               PTB=Lote NFS-E];
  }
  FIELDS
  {
    { 10  ;   ;Batch No.           ;Code20        ;CaptionML=[ENU=Batch No.;
                                                              PTB=N� Lote] }
    { 15  ;   ;Branch Code         ;Code20        ;TableRelation="Branch Information";
                                                   CaptionML=[ENU=Branch Code;
                                                              PTB=C�d. Filial] }
    { 20  ;   ;External Batch No.  ;Text30        ;CaptionML=[ENU=External Batch No.;
                                                              PTB=N� Lote Externo] }
    { 30  ;   ;Protocol            ;Text100       ;CaptionML=[ENU=Protocol;
                                                              PTB=Protocolo] }
    { 40  ;   ;Status              ;Option        ;CaptionML=[ENU=Status;
                                                              PTB=Status];
                                                   OptionCaptionML=[ENU=Not sent,Sent,Success,Error;
                                                                    PTB=N�o-enviado,Enviado,Sucesso,Erro];
                                                   OptionString=NaoEnviado,Enviado,Sucesso,Erro }
    { 50  ;   ;Errors              ;Integer       ;FieldClass=FlowField;
                                                   CalcFormula=Count("NFS-E Batch Message" WHERE (Batch No.=FIELD(Batch No.),
                                                                                                  Type=CONST(Error),
                                                                                                  Branch Code=FIELD(Branch Code)));
                                                   CaptionML=[ENU=Errors;
                                                              PTB=Erros];
                                                   Editable=No }
    { 60  ;   ;Warnings            ;Integer       ;FieldClass=FlowField;
                                                   CalcFormula=Count("NFS-E Batch Message" WHERE (Batch No.=FIELD(Batch No.),
                                                                                                  Type=CONST(Warning),
                                                                                                  Branch Code=FIELD(Branch Code)));
                                                   CaptionML=[ENU=Warnings;
                                                              PTB=Avisos];
                                                   Editable=No }
    { 1050;   ;Invoices            ;Integer       ;FieldClass=FlowField;
                                                   CalcFormula=Count("Sales Invoice Header" WHERE (E-Invoice Batch No.=FIELD(Batch No.),
                                                                                                   Branch Code=FIELD(Branch Code)));
                                                   CaptionML=[ENU=Invoices;
                                                              PTB=Notas Fiscais];
                                                   Editable=No }
    { 1051;   ;Posting Date        ;Date          ;CaptionML=[ENU=Posting Date;
                                                              PTB=Data Registro] }
  }
  KEYS
  {
    {    ;Batch No.,Branch Code                   ;Clustered=Yes }
    {    ;Status                                   }
    {    ;Posting Date,External Batch No.          }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

