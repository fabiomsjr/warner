OBJECT Page 52112730 Request Subform - Closed
{
  OBJECT-PROPERTIES
  {
    Date=28/10/14;
    Time=16:56:48;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Lines;
               PTB=Linhas];
    InsertAllowed=No;
    DeleteAllowed=No;
    ModifyAllowed=No;
    SourceTable=Table52112724;
    PageType=ListPart;
    AutoSplitKey=Yes;
    OnOpenPage=VAR
                 reqPurpose@52006500 : Record 52112734;
               BEGIN
                 PurposeCodeVisible := NOT reqPurpose.ISEMPTY;
               END;

    ActionList=ACTIONS
    {
      { 35000009;    ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 35000007;1   ;Action    ;
                      CaptionML=[ENU=Quotations;
                                 PTB=Cota��es];
                      Image=Quote;
                      OnAction=BEGIN
                                 ShowQuote(FALSE);
                               END;
                                }
      { 52006512;1   ;Action    ;
                      ShortCutKey=Shift+Ctrl+D;
                      CaptionML=[ENU=Dimensions;
                                 PTB=Dimens�es];
                      Image=Dimensions;
                      OnAction=BEGIN
                                 ShowDimensions;
                                 CurrPage.SAVERECORD;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 35000000;0;Container;
                ContainerType=ContentArea }

    { 35000001;1;Group    ;
                Name=Group;
                GroupType=Repeater }

    { 35000002;2;Field    ;
                SourceExpr="Item Description" }

    { 35000003;2;Field    ;
                SourceExpr="Item Type" }

    { 35000004;2;Field    ;
                SourceExpr="Item No." }

    { 35000005;2;Field    ;
                SourceExpr=Quantity }

    { 35000006;2;Field    ;
                SourceExpr="Unit of Measure" }

    { 52006508;2;Field    ;
                CaptionML=[ENU=Available Qty.;
                           PTB=Qtd. Dispon�vel];
                DecimalPlaces=0:10;
                SourceExpr=AvailableQty }

    { 52006513;2;Field    ;
                SourceExpr="Purchased Qty.";
                Editable=FALSE }

    { 52006509;2;Field    ;
                SourceExpr="Open Quantity";
                Editable=FALSE }

    { 52006510;2;Field    ;
                SourceExpr="Entry Type";
                Editable=FALSE }

    { 52006511;2;Field    ;
                SourceExpr="Transfer Location Code";
                Editable=FALSE }

    { 52006514;2;Field    ;
                SourceExpr="Purpose Code";
                Visible=PurposeCodeVisible;
                Editable=FALSE }

    { 35000008;2;Field    ;
                SourceExpr="Use and Application" }

    { 52006507;2;Field    ;
                Name=ShortcutDim1;
                SourceExpr=ShortcutDimCode[1];
                CaptionClass='1,2,1';
                Visible=FALSE;
                OnValidate=BEGIN
                             ValidateShortcutDimCode(1,ShortcutDimCode[1]);
                           END;

                OnLookup=BEGIN
                           LookupShortcutDimCode(1,ShortcutDimCode[1]);
                         END;
                          }

    { 52006506;2;Field    ;
                Name=ShortcutDim2;
                SourceExpr=ShortcutDimCode[2];
                CaptionClass='1,2,2';
                Visible=FALSE;
                OnValidate=BEGIN
                             ValidateShortcutDimCode(2,ShortcutDimCode[2]);
                           END;

                OnLookup=BEGIN
                           LookupShortcutDimCode(2,ShortcutDimCode[2]);
                         END;
                          }

    { 52006505;2;Field    ;
                Name=ShortcutDim3;
                SourceExpr=ShortcutDimCode[3];
                CaptionClass='1,2,3';
                Visible=FALSE;
                OnValidate=BEGIN
                             ValidateShortcutDimCode(3,ShortcutDimCode[3]);
                           END;

                OnLookup=BEGIN
                           LookupShortcutDimCode(3,ShortcutDimCode[3]);
                         END;
                          }

    { 52006504;2;Field    ;
                Name=ShortcutDim4;
                SourceExpr=ShortcutDimCode[4];
                CaptionClass='1,2,4';
                Visible=FALSE;
                OnValidate=BEGIN
                             ValidateShortcutDimCode(4,ShortcutDimCode[4]);
                           END;

                OnLookup=BEGIN
                           LookupShortcutDimCode(4,ShortcutDimCode[4]);
                         END;
                          }

    { 52006503;2;Field    ;
                Name=ShortcutDim5;
                SourceExpr=ShortcutDimCode[5];
                CaptionClass='1,2,5';
                Visible=FALSE;
                OnValidate=BEGIN
                             ValidateShortcutDimCode(5,ShortcutDimCode[5]);
                           END;

                OnLookup=BEGIN
                           LookupShortcutDimCode(5,ShortcutDimCode[5]);
                         END;
                          }

    { 52006502;2;Field    ;
                Name=ShortcutDim6;
                SourceExpr=ShortcutDimCode[6];
                CaptionClass='1,2,6';
                Visible=FALSE;
                OnValidate=BEGIN
                             ValidateShortcutDimCode(6,ShortcutDimCode[6]);
                           END;

                OnLookup=BEGIN
                           LookupShortcutDimCode(6,ShortcutDimCode[6]);
                         END;
                          }

    { 52006501;2;Field    ;
                Name=ShortcutDim7;
                SourceExpr=ShortcutDimCode[7];
                CaptionClass='1,2,7';
                Visible=FALSE;
                OnValidate=BEGIN
                             ValidateShortcutDimCode(7,ShortcutDimCode[7]);
                           END;

                OnLookup=BEGIN
                           LookupShortcutDimCode(7,ShortcutDimCode[7]);
                         END;
                          }

    { 52006500;2;Field    ;
                Name=ShortcutDim8;
                SourceExpr=ShortcutDimCode[8];
                CaptionClass='1,2,8';
                Visible=FALSE;
                OnValidate=BEGIN
                             ValidateShortcutDimCode(8,ShortcutDimCode[8]);
                           END;

                OnLookup=BEGIN
                           LookupShortcutDimCode(8,ShortcutDimCode[8]);
                         END;
                          }

  }
  CODE
  {
    VAR
      ShortcutDimCode@52006500 : ARRAY [8] OF Code[20];
      PurposeCodeVisible@52006501 : Boolean;

    BEGIN
    END.
  }
}

