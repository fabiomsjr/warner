OBJECT Table 49 Invoice Post. Buffer
{
  OBJECT-PROPERTIES
  {
    Date=13/10/14;
    Time=11:51:05;
    Modified=Yes;
    Version List=NAVW17.00,NAVBR7,WAR003.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Invoice Post. Buffer;
               PTB=Mem. Int. N.Fiscal];
  }
  FIELDS
  {
    { 1   ;   ;Type                ;Option        ;CaptionML=[ENU=Type;
                                                              PTB=Tipo];
                                                   OptionCaptionML=[ENU=Prepmt. Exch. Rate Difference,G/L Account,Item,Resource,Fixed Asset;
                                                                    PTB=,Conta,Produto,Recurso,Ativo Fixo];
                                                   OptionString=Prepmt. Exch. Rate Difference,G/L Account,Item,Resource,Fixed Asset }
    { 2   ;   ;G/L Account         ;Code20        ;TableRelation="G/L Account";
                                                   CaptionML=[ENU=G/L Account;
                                                              PTB=Conta C/G] }
    { 4   ;   ;Global Dimension 1 Code;Code20     ;TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(1));
                                                   CaptionML=[ENU=Global Dimension 1 Code;
                                                              PTB=Cod. Dimens�o 1 Global];
                                                   CaptionClass='1,1,1' }
    { 5   ;   ;Global Dimension 2 Code;Code20     ;TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(2));
                                                   CaptionML=[ENU=Global Dimension 2 Code;
                                                              PTB=Cod. Dimens�o 2 Global];
                                                   CaptionClass='1,1,2' }
    { 6   ;   ;Job No.             ;Code20        ;TableRelation=Job;
                                                   CaptionML=[ENU=Job No.;
                                                              PTB=N� Projeto] }
    { 7   ;   ;Amount              ;Decimal       ;CaptionML=[ENU=Amount;
                                                              PTB=Valor];
                                                   AutoFormatType=1 }
    { 8   ;   ;VAT Amount          ;Decimal       ;CaptionML=[ENU=VAT Amount;
                                                              PTB=Valor IPI];
                                                   AutoFormatType=1 }
    { 10  ;   ;Gen. Bus. Posting Group;Code10     ;TableRelation="Gen. Business Posting Group";
                                                   CaptionML=[ENU=Gen. Bus. Posting Group;
                                                              PTB=Gr. Cont�bil Neg�cio] }
    { 11  ;   ;Gen. Prod. Posting Group;Code10    ;TableRelation="Gen. Product Posting Group";
                                                   CaptionML=[ENU=Gen. Prod. Posting Group;
                                                              PTB=Gr. Cont�bil Produto] }
    { 12  ;   ;VAT Calculation Type;Option        ;CaptionML=[ENU=VAT Calculation Type;
                                                              PTB=Tipo C�lculo IPI];
                                                   OptionCaptionML=[ENU=Normal VAT,Reverse Charge VAT,Full VAT,Sales Tax;
                                                                    PTB=IPI Normal,IPI Conta Autoliquida��o,IPI Total,Impostos Vendas];
                                                   OptionString=Normal VAT,Reverse Charge VAT,Full VAT,Sales Tax }
    { 14  ;   ;VAT Base Amount     ;Decimal       ;CaptionML=[ENU=VAT Base Amount;
                                                              PTB=Valor Base IPI];
                                                   AutoFormatType=1 }
    { 17  ;   ;System-Created Entry;Boolean       ;CaptionML=[ENU=System-Created Entry;
                                                              PTB=Lan�amento Autom�tico] }
    { 18  ;   ;Tax Area Code       ;Code20        ;TableRelation="Tax Area";
                                                   CaptionML=[ENU=Tax Area Code;
                                                              PTB=C�d. �rea Imposto] }
    { 19  ;   ;Tax Liable          ;Boolean       ;CaptionML=[ENU=Tax Liable;
                                                              PTB=Sujeito a Imposto] }
    { 20  ;   ;Tax Group Code      ;Code10        ;TableRelation="Tax Group";
                                                   CaptionML=[ENU=Tax Group Code;
                                                              PTB=Cod. Gr. Imposto] }
    { 21  ;   ;Quantity            ;Decimal       ;CaptionML=[ENU=Quantity;
                                                              PTB=Quantidade];
                                                   DecimalPlaces=1:5 }
    { 22  ;   ;Use Tax             ;Boolean       ;CaptionML=[ENU=Use Tax;
                                                              PTB=Utiliza Imposto] }
    { 23  ;   ;VAT Bus. Posting Group;Code10      ;TableRelation="VAT Business Posting Group";
                                                   CaptionML=[ENU=VAT Bus. Posting Group;
                                                              PTB=Gr. Registro Imp. Neg�cio] }
    { 24  ;   ;VAT Prod. Posting Group;Code10     ;TableRelation="VAT Product Posting Group";
                                                   CaptionML=[ENU=VAT Prod. Posting Group;
                                                              PTB=Gr. Registo Imp. Produto] }
    { 25  ;   ;Amount (ACY)        ;Decimal       ;CaptionML=[ENU=Amount (ACY);
                                                              PTB=Valor (MA)];
                                                   AutoFormatType=1 }
    { 26  ;   ;VAT Amount (ACY)    ;Decimal       ;CaptionML=[ENU=VAT Amount (ACY);
                                                              PTB=Valor IPI (MA)];
                                                   AutoFormatType=1 }
    { 29  ;   ;VAT Base Amount (ACY);Decimal      ;CaptionML=[ENU=VAT Base Amount (ACY);
                                                              PTB=Valor Base IPI (MA)];
                                                   AutoFormatType=1 }
    { 31  ;   ;VAT Difference      ;Decimal       ;CaptionML=[ENU=VAT Difference;
                                                              PTB=Diferen�a IPI];
                                                   AutoFormatType=1 }
    { 32  ;   ;VAT %               ;Decimal       ;CaptionML=[ENU=VAT %;
                                                              PTB=% IPI];
                                                   DecimalPlaces=1:1 }
    { 480 ;   ;Dimension Set ID    ;Integer       ;TableRelation="Dimension Set Entry";
                                                   CaptionML=ENU=Dimension Set ID;
                                                   Editable=No }
    { 5600;   ;FA Posting Date     ;Date          ;CaptionML=[ENU=FA Posting Date;
                                                              PTB=Data Registo AF] }
    { 5601;   ;FA Posting Type     ;Option        ;CaptionML=[ENU=FA Posting Type;
                                                              PTB=Tipo Registo AF];
                                                   OptionCaptionML=[ENU=" ,Acquisition Cost,Maintenance";
                                                                    PTB=" ,Custo de aquisi��o,Manuten��o"];
                                                   OptionString=[ ,Acquisition Cost,Maintenance] }
    { 5602;   ;Depreciation Book Code;Code10      ;TableRelation="Depreciation Book";
                                                   CaptionML=[ENU=Depreciation Book Code;
                                                              PTB=Cod. Livro Deprecia��o] }
    { 5603;   ;Salvage Value       ;Decimal       ;CaptionML=[ENU=Salvage Value;
                                                              PTB=Valor Residual];
                                                   AutoFormatType=1 }
    { 5605;   ;Depr. until FA Posting Date;Boolean;CaptionML=[ENU=Depr. until FA Posting Date;
                                                              PTB=Deprec. AF at� a Data Reg.] }
    { 5606;   ;Depr. Acquisition Cost;Boolean     ;CaptionML=[ENU=Depr. Acquisition Cost;
                                                              PTB=Deprecia��o at� Custo] }
    { 5609;   ;Maintenance Code    ;Code10        ;TableRelation=Maintenance;
                                                   CaptionML=[ENU=Maintenance Code;
                                                              PTB=Cod. Manuten��o] }
    { 5610;   ;Insurance No.       ;Code20        ;TableRelation=Insurance;
                                                   CaptionML=[ENU=Insurance No.;
                                                              PTB=N� Seguro] }
    { 5611;   ;Budgeted FA No.     ;Code20        ;TableRelation="Fixed Asset";
                                                   CaptionML=[ENU=Budgeted FA No.;
                                                              PTB=N� AF Or�ado] }
    { 5612;   ;Duplicate in Depreciation Book;Code10;
                                                   TableRelation="Depreciation Book";
                                                   CaptionML=[ENU=Duplicate in Depreciation Book;
                                                              PTB=Duplicado em Livro Deprecia��o] }
    { 5613;   ;Use Duplication List;Boolean       ;CaptionML=[ENU=Use Duplication List;
                                                              PTB=Usa Lista Duplicados] }
    { 5614;   ;Fixed Asset Line No.;Integer       ;CaptionML=[ENU=Fixed Asset Line No.;
                                                              PTB=N� Linha Ativo Fixo] }
    { 50000;  ;Posting Text        ;Text250       ;CaptionML=[ENU=Posting Text;
                                                              PTB=Texto Registro] }
    { 52112430;;Dimension Allocation ID;Integer   ;TableRelation="Dimensions Allocation";
                                                   CaptionML=[ENU=Dimension Allocation ID;
                                                              PTB=ID Rateio Dimens�o] }
  }
  KEYS
  {
    {    ;Type,G/L Account,Gen. Bus. Posting Group,Gen. Prod. Posting Group,VAT Bus. Posting Group,VAT Prod. Posting Group,Tax Area Code,Tax Group Code,Tax Liable,Use Tax,Dimension Set ID,Job No.,Fixed Asset Line No.,Dimension Allocation ID;
                                                   Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    PROCEDURE PrepareSales@1(VAR SalesLine@1000 : Record 37);
    VAR
      ">NAVBR"@52006501 : Integer;
      salesMgt@52006500 : Codeunit 52112432;
    BEGIN
      CLEAR(Rec);
      Type := SalesLine.Type;
      "System-Created Entry" := TRUE;
      "Gen. Bus. Posting Group" := SalesLine."Gen. Bus. Posting Group";
      "Gen. Prod. Posting Group" := SalesLine."Gen. Prod. Posting Group";
      "VAT Bus. Posting Group" := SalesLine."VAT Bus. Posting Group";
      "VAT Prod. Posting Group" := SalesLine."VAT Prod. Posting Group";
      "VAT Calculation Type" := SalesLine."VAT Calculation Type";
      "Global Dimension 1 Code" := SalesLine."Shortcut Dimension 1 Code";
      "Global Dimension 2 Code" := SalesLine."Shortcut Dimension 2 Code";
      "Dimension Set ID" := SalesLine."Dimension Set ID";
      "Job No." := SalesLine."Job No.";
      "VAT %" := SalesLine."VAT %";
      "VAT Difference" := SalesLine."VAT Difference";
      IF Type = Type::"Fixed Asset" THEN BEGIN
        "FA Posting Date" := SalesLine."FA Posting Date";
        "Depreciation Book Code" := SalesLine."Depreciation Book Code";
        "Depr. until FA Posting Date" := SalesLine."Depr. until FA Posting Date";
        "Duplicate in Depreciation Book" := SalesLine."Duplicate in Depreciation Book";
        "Use Duplication List" := SalesLine."Use Duplication List";
      END;

      IF "VAT Calculation Type" = "VAT Calculation Type"::"Sales Tax" THEN BEGIN
        "Tax Area Code" := SalesLine."Tax Area Code";
        "Tax Group Code" := SalesLine."Tax Group Code";
        "Tax Liable" := SalesLine."Tax Liable";
        "Use Tax" := FALSE;
        Quantity := SalesLine."Qty. to Invoice (Base)";
      END;

      // > NAVBR
      IF SalesLine."Dimension Allocation ID" <> 0 THEN BEGIN
        salesMgt.CheckLineDimAllocationForPosting(SalesLine);
        "Dimension Allocation ID" := SalesLine."Dimension Allocation ID";
      END;
      // < NAVBR
    END;

    PROCEDURE CalcDiscount@2(PricesInclVAT@1000 : Boolean;DiscountAmount@1001 : Decimal;DiscountAmountACY@1002 : Decimal);
    VAR
      CurrencyLCY@1003 : Record 4;
      CurrencyACY@1004 : Record 4;
      GLSetup@1005 : Record 98;
    BEGIN
      CurrencyLCY.InitRoundingPrecision;
      GLSetup.GET;
      IF GLSetup."Additional Reporting Currency" <> '' THEN
        CurrencyACY.GET(GLSetup."Additional Reporting Currency")
      ELSE
        CurrencyACY := CurrencyLCY;
      "VAT Amount" := ROUND(
          CalcVATAmount(PricesInclVAT,DiscountAmount,"VAT %"),
          CurrencyLCY."Amount Rounding Precision",
          CurrencyLCY.VATRoundingDirection);
      "VAT Amount (ACY)" := ROUND(
          CalcVATAmount(PricesInclVAT,DiscountAmountACY,"VAT %"),
          CurrencyACY."Amount Rounding Precision",
          CurrencyACY.VATRoundingDirection);

      IF PricesInclVAT AND ("VAT %" <> 0) THEN BEGIN
        "VAT Base Amount" := DiscountAmount - "VAT Amount";
        "VAT Base Amount (ACY)" := DiscountAmountACY - "VAT Amount (ACY)";
      END ELSE BEGIN
        "VAT Base Amount" := DiscountAmount;
        "VAT Base Amount (ACY)" := DiscountAmountACY;
      END;
      Amount := "VAT Base Amount";
      "Amount (ACY)" := "VAT Base Amount (ACY)";
    END;

    LOCAL PROCEDURE CalcVATAmount@3(ValueInclVAT@1000 : Boolean;Value@1001 : Decimal;"VAT%"@1002 : Decimal) : Decimal;
    BEGIN
      IF "VAT%" = 0 THEN
        EXIT(0);
      IF ValueInclVAT THEN
        EXIT(Value / (1 + ("VAT%" / 100)) * ("VAT%" / 100));

      EXIT(Value * ("VAT%" / 100));
    END;

    PROCEDURE SetAccount@4(Account@1000 : Code[20];VAR TotalVAT@1004 : Decimal;VAR TotalVATACY@1003 : Decimal;VAR TotalAmount@1002 : Decimal;VAR TotalAmountACY@1001 : Decimal);
    BEGIN
      TotalVAT := TotalVAT - "VAT Amount";
      TotalVATACY := TotalVATACY - "VAT Amount (ACY)";
      TotalAmount := TotalAmount - Amount;
      TotalAmountACY := TotalAmountACY - "Amount (ACY)";
      "G/L Account" := Account;
    END;

    PROCEDURE SetAmounts@5(TotalVAT@1003 : Decimal;TotalVATACY@1002 : Decimal;TotalAmount@1001 : Decimal;TotalAmountACY@1000 : Decimal;VATDifference@1004 : Decimal);
    BEGIN
      Amount := TotalAmount;
      "VAT Base Amount" := TotalAmount;
      "VAT Amount" := TotalVAT;
      "Amount (ACY)" := TotalAmountACY;
      "VAT Base Amount (ACY)" := TotalAmountACY;
      "VAT Amount (ACY)" := TotalVATACY;
      "VAT Difference" := "VAT Difference";
    END;

    PROCEDURE PreparePurchase@6(VAR PurchLine@1000 : Record 39);
    VAR
      ">NAVBR"@52006501 : Integer;
      purchaseMgt@52006500 : Codeunit 52112431;
      appliedPurchLine@52006502 : Record 39;
    BEGIN
      CLEAR(Rec);
      Type := PurchLine.Type;
      "System-Created Entry" := TRUE;
      "Gen. Bus. Posting Group" := PurchLine."Gen. Bus. Posting Group";
      "Gen. Prod. Posting Group" := PurchLine."Gen. Prod. Posting Group";
      "VAT Bus. Posting Group" := PurchLine."VAT Bus. Posting Group";
      "VAT Prod. Posting Group" := PurchLine."VAT Prod. Posting Group";
      "VAT Calculation Type" := PurchLine."VAT Calculation Type";
      "Global Dimension 1 Code" := PurchLine."Shortcut Dimension 1 Code";
      "Global Dimension 2 Code" := PurchLine."Shortcut Dimension 2 Code";
      "Dimension Set ID" := PurchLine."Dimension Set ID";
      "Job No." := PurchLine."Job No.";
      "VAT %" := PurchLine."VAT %";
      "VAT Difference" := PurchLine."VAT Difference";
      IF Type = Type::"Fixed Asset" THEN BEGIN
        "FA Posting Date" := PurchLine."FA Posting Date";
        "Depreciation Book Code" := PurchLine."Depreciation Book Code";
        "Depr. until FA Posting Date" := PurchLine."Depr. until FA Posting Date";
        "Duplicate in Depreciation Book" := PurchLine."Duplicate in Depreciation Book";
        "Use Duplication List" := PurchLine."Use Duplication List";
        "FA Posting Type" := PurchLine."FA Posting Type";
        "Depreciation Book Code" := PurchLine."Depreciation Book Code";
        "Salvage Value" := PurchLine."Salvage Value";
        "Depr. Acquisition Cost" := PurchLine."Depr. Acquisition Cost";
        "Maintenance Code" := PurchLine."Maintenance Code";
        "Insurance No." := PurchLine."Insurance No.";
        "Budgeted FA No." := PurchLine."Budgeted FA No.";
      END;

      IF "VAT Calculation Type" = "VAT Calculation Type"::"Sales Tax" THEN BEGIN
        "Tax Area Code" := PurchLine."Tax Area Code";
        "Tax Group Code" := PurchLine."Tax Group Code";
        "Tax Liable" := PurchLine."Tax Liable";
        "Use Tax" := FALSE;
        Quantity := PurchLine."Qty. to Invoice (Base)";
      END;

      // > NAVBR
      IF PurchLine."Dimension Allocation ID" <> 0 THEN BEGIN
        purchaseMgt.CheckLineDimAllocationForPosting(PurchLine);
        "Dimension Allocation ID" := PurchLine."Dimension Allocation ID";
      END;
      CASE Type OF
        Type::"Fixed Asset":
          "Fixed Asset Line No." := PurchLine."Line No.";
        ELSE
          IF appliedPurchLine.GET(PurchLine."Document Type", PurchLine."Document No.", PurchLine."Relation Charge Item Line No.") THEN BEGIN
            IF appliedPurchLine.Type = appliedPurchLine.Type::"Fixed Asset" THEN BEGIN
              Type := Type::"Fixed Asset";
              "G/L Account" := appliedPurchLine."No.";
              "FA Posting Date" := appliedPurchLine."FA Posting Date";
              "Depreciation Book Code" := appliedPurchLine."Depreciation Book Code";
              "Depr. until FA Posting Date" := appliedPurchLine."Depr. until FA Posting Date";
              "Duplicate in Depreciation Book" := appliedPurchLine."Duplicate in Depreciation Book";
              "Use Duplication List" := appliedPurchLine."Use Duplication List";
              "FA Posting Type" := appliedPurchLine."FA Posting Type";
              "Depreciation Book Code" := appliedPurchLine."Depreciation Book Code";
              "Salvage Value" := appliedPurchLine."Salvage Value";
              "Depr. Acquisition Cost" := appliedPurchLine."Depr. Acquisition Cost";
              "Maintenance Code" := appliedPurchLine."Maintenance Code";
              "Insurance No." := appliedPurchLine."Insurance No.";
              "Budgeted FA No." := appliedPurchLine."Budgeted FA No.";
              "Fixed Asset Line No." := appliedPurchLine."Line No.";
            END;
          END;
      END;
      // < NAVBR
    END;

    PROCEDURE CalcDiscountNoVAT@7(DiscountAmount@1001 : Decimal;DiscountAmountACY@1002 : Decimal);
    BEGIN
      "VAT Base Amount" := DiscountAmount;
      "VAT Base Amount (ACY)" := DiscountAmountACY;
      Amount := "VAT Base Amount";
      "Amount (ACY)" := "VAT Base Amount (ACY)";
    END;

    PROCEDURE SetSalesTax@8(PurchaseLine@1000 : Record 39);
    BEGIN
      "Tax Area Code" := PurchaseLine."Tax Area Code";
      "Tax Liable" := PurchaseLine."Tax Liable";
      "Tax Group Code" := PurchaseLine."Tax Group Code";
      "Use Tax" := PurchaseLine."Use Tax";
      Quantity := PurchaseLine."Qty. to Invoice (Base)";
    END;

    PROCEDURE ReverseAmounts@9();
    BEGIN
      Amount := -Amount;
      "VAT Base Amount" := -"VAT Base Amount";
      "Amount (ACY)" := -"Amount (ACY)";
      "VAT Base Amount (ACY)" := -"VAT Base Amount (ACY)";
      "VAT Amount" := -"VAT Amount";
      "VAT Amount (ACY)" := -"VAT Amount (ACY)";
    END;

    PROCEDURE SetAmountsNoVAT@10(TotalAmount@1001 : Decimal;TotalAmountACY@1000 : Decimal;VATDifference@1004 : Decimal);
    BEGIN
      Amount := TotalAmount;
      "VAT Base Amount" := TotalAmount;
      "VAT Amount" := 0;
      "Amount (ACY)" := TotalAmountACY;
      "VAT Base Amount (ACY)" := TotalAmountACY;
      "VAT Amount (ACY)" := 0;
      "VAT Difference" := "VAT Difference";
    END;

    PROCEDURE PrepareService@11(VAR ServiceLine@1000 : Record 5902);
    BEGIN
      CLEAR(Rec);
      CASE ServiceLine.Type OF
        ServiceLine.Type::Item:
          Type := Type::Item;
        ServiceLine.Type::Resource:
          Type := Type::Resource;
        ServiceLine.Type::"G/L Account":
          Type := Type::"G/L Account";
      END;
      "System-Created Entry" := TRUE;
      "Gen. Bus. Posting Group" := ServiceLine."Gen. Bus. Posting Group";
      "Gen. Prod. Posting Group" := ServiceLine."Gen. Prod. Posting Group";
      "VAT Bus. Posting Group" := ServiceLine."VAT Bus. Posting Group";
      "VAT Prod. Posting Group" := ServiceLine."VAT Prod. Posting Group";
      "VAT Calculation Type" := ServiceLine."VAT Calculation Type";
      "Global Dimension 1 Code" := ServiceLine."Shortcut Dimension 1 Code";
      "Global Dimension 2 Code" := ServiceLine."Shortcut Dimension 2 Code";
      "Dimension Set ID" := ServiceLine."Dimension Set ID";
      "Job No." := ServiceLine."Job No.";
      "VAT %" := ServiceLine."VAT %";
      "VAT Difference" := ServiceLine."VAT Difference";
      IF "VAT Calculation Type" = "VAT Calculation Type"::"Sales Tax" THEN BEGIN
        "Tax Area Code" := ServiceLine."Tax Area Code";
        "Tax Group Code" := ServiceLine."Tax Group Code";
        "Tax Liable" := ServiceLine."Tax Liable";
        "Use Tax" := FALSE;
        Quantity := ServiceLine."Qty. to Invoice (Base)";
      END;

      // > NAVBR
      //"dimension allocation id" := serviceline."dimension allocation id";
      // < NAVBR
    END;

    BEGIN
    END.
  }
}

