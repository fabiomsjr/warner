OBJECT Table 80027 Unit of Measure_UPG
{
  OBJECT-PROPERTIES
  {
    Date=13/05/16;
    Time=16:17:11;
    Modified=Yes;
    Version List=UPG;
  }
  PROPERTIES
  {
    DataCaptionFields=Code,Description;
    OnDelete=BEGIN
               UnitOfMeasureTranslation.SETRANGE(Code,Code);
               UnitOfMeasureTranslation.DELETEALL;
             END;

    CaptionML=[ENU=Unit of Measure;
               PTB=Unidade Medida];
    LookupPageID=Page209;
    DrillDownPageID=Page209;
  }
  FIELDS
  {
    { 1   ;   ;Code                ;Code10        ;CaptionML=[ENU=Code;
                                                              PTB=C�digo];
                                                   NotBlank=Yes }
    { 2   ;   ;Description         ;Text10        ;CaptionML=[ENU=Description;
                                                              PTB=Descri��o] }
    { 35000000;;E-Files Code       ;Code3         ;CaptionML=[ENU=E-Files Code;
                                                              PTB=Codifica�ao Arquivos];
                                                   Description=MBS/BR.01 }
    { 35000001;;E-Files Service    ;Boolean       ;CaptionML=PTB=Servi�o - SINTEGRA;
                                                   Description=NAVBR6.00.0211 }
  }
  KEYS
  {
    {    ;Code                                    ;Clustered=Yes }
    {    ;Description                             ;KeyGroups=SearchCol }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      UnitOfMeasureTranslation@1000 : Record 5402;

    BEGIN
    END.
  }
}

