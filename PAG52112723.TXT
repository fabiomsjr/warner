OBJECT Page 52112723 Request
{
  OBJECT-PROPERTIES
  {
    Date=30/10/14;
    Time=08:30:09;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Request;
               PTB=Requisi��o];
    SourceTable=Table52112723;
    SourceTableView=SORTING(No.)
                    WHERE(Status=FILTER(Open|Pending Approval|Purchase|Inventory));
    PageType=Card;
    OnOpenPage=VAR
                 glSetup@52006500 : Record 98;
               BEGIN
                 glSetup.GET;
                 ShowDim1 := glSetup."Shortcut Dimension 1 Code" <> '';
                 ShowDim2 := glSetup."Shortcut Dimension 2 Code" <> '';
                 ShowDim3 := glSetup."Shortcut Dimension 3 Code" <> '';
                 ShowDim4 := glSetup."Shortcut Dimension 4 Code" <> '';
                 ShowDim5 := glSetup."Shortcut Dimension 5 Code" <> '';
                 ShowDim6 := glSetup."Shortcut Dimension 6 Code" <> '';
                 ShowDim7 := glSetup."Shortcut Dimension 7 Code" <> '';
                 ShowDim8 := glSetup."Shortcut Dimension 8 Code" <> '';
                 UpdateEditable;
               END;

    OnAfterGetRecord=BEGIN
                       ShowShortcutDimCode(ShortcutDimCode);
                       UpdateEditable;
                     END;

    ActionList=ACTIONS
    {
      { 35000011;    ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 52006510;1   ;Action    ;
                      ShortCutKey=Shift+Ctrl+D;
                      CaptionML=[ENU=Dimensions;
                                 PTB=Dimens�es];
                      Image=Dimensions;
                      OnAction=BEGIN
                                 ShowDocDim;
                                 CurrPage.SAVERECORD;
                               END;
                                }
      { 35000016;1   ;Action    ;
                      CaptionML=[ENU=Aprovals;
                                 PTB=Aprova��es];
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=Approvals;
                      PromotedCategory=Process;
                      OnAction=VAR
                                 approvEntries@35000001 : Page 658;
                                 approvEntry@35000000 : Record 454;
                               BEGIN
                                 approvEntries.Setfilters(DATABASE::Request, approvEntry."Document Type"::Request, "No.");
                                 approvEntries.RUN;
                               END;
                                }
      { 52006500;1   ;Action    ;
                      Name=OpenFiles;
                      CaptionML=[ENU=Files;
                                 PTB=Arquivos];
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=ExternalDocument;
                      PromotedCategory=Process;
                      OnAction=VAR
                                 docFileMgt@52006500 : Codeunit 52112428;
                               BEGIN
                                 docFileMgt.OpenFiles(Rec);
                               END;
                                }
      { 52006513;1   ;Action    ;
                      Name=PurchTracking;
                      CaptionML=[ENU=Purchase Tracking;
                                 PTB=Rastreabilidade Compras];
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=Track;
                      PromotedCategory=Process;
                      OnAction=VAR
                                 purchTracking@52006500 : Record 52112733;
                               BEGIN
                                 purchTracking.TrackByRequest(Rec);
                               END;
                                }
      { 52006511;1   ;Action    ;
                      Name=Print;
                      Ellipsis=Yes;
                      CaptionML=[ENU=&Print;
                                 PTB=Im&primir];
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=Print;
                      PromotedCategory=Report;
                      OnAction=VAR
                                 req@52006501 : Record 52112723;
                                 report@52006500 : Report 52112723;
                               BEGIN
                                 req := Rec;
                                 req.SETRECFILTER;
                                 report.SETTABLEVIEW(req);
                                 report.RUN;
                               END;
                                }
      { 52006514;1   ;Action    ;
                      Name=Navigate;
                      CaptionML=[ENU=&Navigate;
                                 PTB=&Navegar];
                      Promoted=Yes;
                      Image=Navigate;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 Navigate;
                               END;
                                }
      { 35000013;    ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 35000014;1   ;Action    ;
                      CaptionML=[ENU=Send Approval Request;
                                 PTB=Enviar Requisi��o Aprova��o];
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=SendApprovalRequest;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 SendApprovalRequest;
                                 CurrPage.UPDATE;
                               END;
                                }
      { 35000015;1   ;Action    ;
                      CaptionML=[ENU=Cancel Approval Request;
                                 PTB=Cancelar Requisi��o Aprova��o];
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=Cancel;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 CancelApprovalRequest;
                                 CurrPage.UPDATE;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 35000000;0;Container;
                ContainerType=ContentArea }

    { 35000001;1;Group    ;
                Name=Geral;
                Editable=UserEditable;
                GroupType=Group }

    { 35000002;2;Field    ;
                AssistEdit=Yes;
                SourceExpr="No.";
                OnAssistEdit=BEGIN
                               IF AssistEdit(xRec) THEN
                                 CurrPage.UPDATE;
                             END;
                              }

    { 35000003;2;Field    ;
                SourceExpr=Description }

    { 35000004;2;Field    ;
                SourceExpr=Status;
                Editable=FALSE }

    { 52006512;2;Field    ;
                SourceExpr="Release Date";
                Editable=FALSE }

    { 35000005;2;Field    ;
                SourceExpr="Priority Degree" }

    { 35000006;2;Field    ;
                SourceExpr=Date;
                Editable=FALSE }

    { 35000007;2;Field    ;
                SourceExpr="User ID";
                Editable=FALSE }

    { 35000008;2;Field    ;
                SourceExpr="User Name" }

    { 35000009;2;Field    ;
                SourceExpr="Purchaser Code" }

    { 52006509;1;Group    ;
                CaptionML=[ENU=Dimensions;
                           PTB=Dimens�es];
                Editable=UserEditable;
                GroupType=Group }

    { 52006507;2;Field    ;
                Name=ShortcutDim1;
                CaptionML=[ENU=Dimension 1 Code;
                           PTB=C�d. Dimens�o 1];
                SourceExpr=ShortcutDimCode[1];
                CaptionClass='1,2,1';
                Visible=ShowDim1;
                OnValidate=BEGIN
                             ValidateShortcutDimCode(1,ShortcutDimCode[1]);
                           END;

                OnLookup=BEGIN
                           LookupShortcutDimCode(1,ShortcutDimCode[1]);
                         END;
                          }

    { 52006508;2;Field    ;
                Name=ShortcutDim2;
                CaptionML=[ENU=Dimension 2 Code;
                           PTB=C�d. Dimens�o 2];
                SourceExpr=ShortcutDimCode[2];
                CaptionClass='1,2,2';
                Visible=ShowDim2;
                OnValidate=BEGIN
                             ValidateShortcutDimCode(2,ShortcutDimCode[2]);
                           END;

                OnLookup=BEGIN
                           LookupShortcutDimCode(2,ShortcutDimCode[2]);
                         END;
                          }

    { 52006505;2;Field    ;
                Name=ShortcutDim3;
                CaptionML=[ENU=Dimension 3 Code;
                           PTB=C�d. Dimens�o 3];
                SourceExpr=ShortcutDimCode[3];
                CaptionClass='1,2,3';
                Visible=ShowDim3;
                OnValidate=BEGIN
                             ValidateShortcutDimCode(3,ShortcutDimCode[3]);
                           END;

                OnLookup=BEGIN
                           LookupShortcutDimCode(3,ShortcutDimCode[3]);
                         END;
                          }

    { 52006506;2;Field    ;
                Name=ShortcutDim4;
                CaptionML=[ENU=Dimension 4 Code;
                           PTB=C�d. Dimens�o 4];
                SourceExpr=ShortcutDimCode[4];
                CaptionClass='1,2,4';
                Visible=ShowDim4;
                OnValidate=BEGIN
                             ValidateShortcutDimCode(4,ShortcutDimCode[4]);
                           END;

                OnLookup=BEGIN
                           LookupShortcutDimCode(4,ShortcutDimCode[4]);
                         END;
                          }

    { 52006503;2;Field    ;
                Name=ShortcutDim5;
                CaptionML=[ENU=Dimension 5 Code;
                           PTB=C�d. Dimens�o 5];
                SourceExpr=ShortcutDimCode[5];
                CaptionClass='1,2,5';
                Visible=ShowDim5;
                OnValidate=BEGIN
                             ValidateShortcutDimCode(5,ShortcutDimCode[5]);
                           END;

                OnLookup=BEGIN
                           LookupShortcutDimCode(5,ShortcutDimCode[5]);
                         END;
                          }

    { 52006504;2;Field    ;
                Name=ShortcutDim6;
                CaptionML=[ENU=Dimension 6 Code;
                           PTB=C�d. Dimens�o 6];
                SourceExpr=ShortcutDimCode[6];
                CaptionClass='1,2,6';
                Visible=ShowDim6;
                OnValidate=BEGIN
                             ValidateShortcutDimCode(6,ShortcutDimCode[6]);
                           END;

                OnLookup=BEGIN
                           LookupShortcutDimCode(6,ShortcutDimCode[6]);
                         END;
                          }

    { 52006501;2;Field    ;
                Name=ShortcutDim7;
                CaptionML=[ENU=Dimension 7 Code;
                           PTB=C�d. Dimens�o 7];
                SourceExpr=ShortcutDimCode[7];
                CaptionClass='1,2,7';
                Visible=ShowDim7;
                OnValidate=BEGIN
                             ValidateShortcutDimCode(7,ShortcutDimCode[7]);
                           END;

                OnLookup=BEGIN
                           LookupShortcutDimCode(7,ShortcutDimCode[7]);
                         END;
                          }

    { 52006502;2;Field    ;
                Name=ShortcutDim8;
                CaptionML=[ENU=Dimension 8 Code;
                           PTB=C�d. Dimens�o 8];
                SourceExpr=ShortcutDimCode[8];
                CaptionClass='1,2,8';
                Visible=ShowDim8;
                OnValidate=BEGIN
                             ValidateShortcutDimCode(8,ShortcutDimCode[8]);
                           END;

                OnLookup=BEGIN
                           LookupShortcutDimCode(8,ShortcutDimCode[8]);
                         END;
                          }

    { 35000010;1;Part     ;
                SubPageView=SORTING(Request No.,Line No.);
                SubPageLink=Request No.=FIELD(No.);
                PagePartID=Page52112724;
                Editable=LinesEditable;
                PartType=Page }

  }
  CODE
  {
    VAR
      UserEditable@35000000 : Boolean;
      LinesEditable@35000001 : Boolean;
      ShortcutDimCode@52006500 : ARRAY [8] OF Code[20];
      ShowDim1@52006501 : Boolean;
      ShowDim2@52006502 : Boolean;
      ShowDim3@52006503 : Boolean;
      ShowDim4@52006504 : Boolean;
      ShowDim5@52006505 : Boolean;
      ShowDim6@52006506 : Boolean;
      ShowDim7@52006507 : Boolean;
      ShowDim8@52006508 : Boolean;

    PROCEDURE UpdateEditable@52006501();
    BEGIN
      IF ("User ID" <> USERID) AND ("User ID" <> '') THEN
        UserEditable := FALSE
      ELSE
        UserEditable := (Status IN [Status::Open]);

      LinesEditable := UserEditable;
    END;

    BEGIN
    END.
  }
}

