OBJECT Page 9905 Data Encryption Management
{
  OBJECT-PROPERTIES
  {
    Date=09/09/14;
    Time=12:00:00;
    Version List=NAVW18.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=ENU=Data Encryption Management;
    PageType=Card;
    OnOpenPage=BEGIN
                 RefreshEncryptionStatus;
               END;

    ActionList=ACTIONS
    {
      { 5       ;    ;ActionContainer;
                      ActionContainerType=NewDocumentItems }
      { 6       ;1   ;Action    ;
                      Name=Enable Encryption;
                      CaptionML=ENU=Enable Encryption;
                      Promoted=Yes;
                      Enabled=EnableEncryptionActionEnabled;
                      Image=CreateDocument;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 EncryptionManagement.EnableEncryption;
                                 RefreshEncryptionStatus;
                               END;
                                }
      { 7       ;1   ;Action    ;
                      Name=Import Encryption Key;
                      AccessByPermission=System 5420=X;
                      CaptionML=ENU=Import Encryption Key;
                      Promoted=Yes;
                      Enabled=ImportKeyActionEnabled;
                      Image=Import;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 EncryptionManagement.ImportKey;
                                 RefreshEncryptionStatus;
                               END;
                                }
      { 1       ;1   ;Action    ;
                      Name=Export Encryption Key;
                      AccessByPermission=System 5410=X;
                      CaptionML=ENU=Export Encryption Key;
                      Promoted=Yes;
                      Enabled=ExportKeyActionEnabled;
                      Image=Export;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 EncryptionManagement.ExportKey;
                               END;
                                }
      { 8       ;1   ;Action    ;
                      Name=Disable Encryption;
                      AccessByPermission=System 5420=X;
                      CaptionML=ENU=Disable Encryption;
                      Promoted=Yes;
                      Enabled=DisableEncryptionActionEnabled;
                      Image=Delete;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 EncryptionManagement.DisableEncryption(FALSE);
                                 RefreshEncryptionStatus;
                               END;
                                }
      { 9       ;1   ;Action    ;
                      Name=Delete Encrypted Data;
                      CaptionML=ENU=Delete Encrypted Data;
                      Enabled=DeleteEncryptionDataActionEnabled;
                      RunPageMode=View;
                      OnAction=BEGIN
                                 EncryptionManagement.DeleteEncryptedData;
                                 RefreshEncryptionStatus;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 4   ;0   ;Container ;
                ContainerType=ContentArea }

    { 2   ;1   ;Field     ;
                CaptionML=ENU=Encryption Enabled;
                SourceExpr=EncryptionEnabledState;
                Editable=FALSE }

    { 3   ;1   ;Field     ;
                CaptionML=ENU=Encryption Key Exists;
                SourceExpr=EncryptionKeyExistsState }

  }
  CODE
  {
    VAR
      EncryptionManagement@1002 : Codeunit 1266;
      EncryptionEnabledState@1000 : Boolean;
      EncryptionKeyExistsState@1001 : Boolean;
      EnableEncryptionActionEnabled@1003 : Boolean;
      ImportKeyActionEnabled@1004 : Boolean;
      ExportKeyActionEnabled@1005 : Boolean;
      DisableEncryptionActionEnabled@1006 : Boolean;
      DeleteEncryptionDataActionEnabled@1007 : Boolean;

    LOCAL PROCEDURE RefreshEncryptionStatus@1();
    BEGIN
      EncryptionEnabledState := ENCRYPTIONENABLED;
      EncryptionKeyExistsState := ENCRYPTIONKEYEXISTS;

      IF EncryptionManagement.IsEncryptionEnabled THEN BEGIN
        EnableEncryptionActionEnabled := FALSE;
        ImportKeyActionEnabled := TRUE;
        ExportKeyActionEnabled := TRUE;
        DisableEncryptionActionEnabled := TRUE;
      END ELSE BEGIN
        EnableEncryptionActionEnabled := TRUE;
        ImportKeyActionEnabled := TRUE;
        ExportKeyActionEnabled := FALSE;
        DisableEncryptionActionEnabled := FALSE;
      END;

      DeleteEncryptionDataActionEnabled := FALSE;
      IF ENCRYPTIONENABLED AND NOT ENCRYPTIONKEYEXISTS THEN
        DeleteEncryptionDataActionEnabled := TRUE;
    END;

    BEGIN
    END.
  }
}

