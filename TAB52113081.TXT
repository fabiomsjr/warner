OBJECT Table 52113081 Request Approval Setup
{
  OBJECT-PROPERTIES
  {
    Date=15/07/15;
    Time=14:38:58;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Request Approval Setup;
               PTB=Conf. Aprova��o Requisi��o];
  }
  FIELDS
  {
    { 10  ;   ;Purpose Code        ;Code20        ;TableRelation="Request Purpose";
                                                   CaptionML=[ENU=Purpose Code;
                                                              PTB=C�d. Finalidade] }
    { 20  ;   ;Approval Flow Rule Code;Code20     ;TableRelation="Approval Flow Rule";
                                                   CaptionML=[ENU=Approval Flow Rule Code;
                                                              PTB=C�d. Regra Fluxo Aprova��o] }
    { 400 ;   ;Addit. Approvers List Code;Code20  ;TableRelation="AF Additional Approvers List";
                                                   CaptionML=[ENU=Addit. Approvers List Code;
                                                              PTB=C�d. Lista Aprov. Adicionais];
                                                   NotBlank=Yes }
  }
  KEYS
  {
    {    ;Purpose Code                            ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

