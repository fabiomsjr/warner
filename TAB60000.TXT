OBJECT Table 60000 Dictionary Table
{
  OBJECT-PROPERTIES
  {
    Date=21/03/12;
    Time=11:33:38;
    Modified=Yes;
    Version List=WCMW1601_01_3;
  }
  PROPERTIES
  {
    DataPerCompany=No;
  }
  FIELDS
  {
    { 1   ;   ;Option              ;Option        ;CaptionML=[ENU=Option;
                                                              ENG=Option];
                                                   OptionString=[ ,Local Account Ranges] }
    { 10  ;   ;Code                ;Code20        ;CaptionML=[ENU=Code;
                                                              ENG=Code] }
    { 15  ;   ;G/L Account From    ;Code20        ;CaptionML=[ENU=G/L Account From;
                                                              ENG=G/L Account From] }
    { 20  ;   ;G/L Account To      ;Code20        ;CaptionML=[ENU=G/L Account To;
                                                              ENG=G/L Account To] }
  }
  KEYS
  {
    {    ;Option,Code                             ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    {
      001  11/09/09  JJB  WCMFSD003  Create Dictionary table for multiple use.
    }
    END.
  }
}

