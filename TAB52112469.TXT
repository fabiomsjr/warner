OBJECT Table 52112469 Tax Payment Bill
{
  OBJECT-PROPERTIES
  {
    Date=16/12/14;
    Time=17:08:47;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    Permissions=TableData 25=rm;
    OnInsert=VAR
               compInfo@52006500 : Record 79;
             BEGIN
               PurchSetup.GET;
               PurchSetup.TESTFIELD("Tax Payment Bill Nos.");

               IF "No." = '' THEN
                 NoSeriesMgt.InitSeries(PurchSetup."Tax Payment Bill Nos.", PurchSetup."Tax Payment Bill Nos.", WORKDATE, "No.", "No. Series");

               compInfo.GET;
               compInfo.TESTFIELD("C.N.P.J.");
               CNPJCPF := compInfo."C.N.P.J.";
             END;

    OnDelete=VAR
               vendLedgEntry@52006500 : Record 25;
             BEGIN
               ThrowErrorIfInvalidForDelete;

               vendLedgEntry.SETRANGE("Tax Payment Bill No.", "No.");
               vendLedgEntry.MODIFYALL("Tax Payment Bill No.", '');
             END;

    CaptionML=[ENU=Tax Payment Bill;
               PTB=Guia Pagamento Imposto];
    LookupPageID=Page52112487;
    DrillDownPageID=Page52112487;
  }
  FIELDS
  {
    { 10  ;   ;No.                 ;Code20        ;OnValidate=BEGIN
                                                                IF "No." <> xRec."No." THEN BEGIN
                                                                  PurchSetup.GET;
                                                                  NoSeriesMgt.TestManual(PurchSetup."Tax Payment Bill Nos.");
                                                                  "No. Series" := '';
                                                                END;
                                                              END;

                                                   CaptionML=[ENU=No.;
                                                              PTB=N�] }
    { 15  ;   ;Bill Type           ;Option        ;FieldClass=FlowField;
                                                   CalcFormula=Lookup("Tax Jurisdiction"."Payment Bill Type" WHERE (Code=FIELD(Tax Jurisdiction Code)));
                                                   CaptionML=[ENU=Bill Type;
                                                              PTB=Tipo Guia];
                                                   OptionCaptionML=[ENU=" ,DARF";
                                                                    PTB=" ,DARF"];
                                                   OptionString=[ ,DARF];
                                                   Editable=No }
    { 17  ;   ;Status              ;Option        ;CaptionML=[ENU=Status;
                                                              PTB=Status];
                                                   OptionCaptionML=[ENU=Open,Released;
                                                                    PTB=Aberto,Liberado];
                                                   OptionString=Open,Released }
    { 20  ;   ;Settlement Period   ;Date          ;OnValidate=BEGIN
                                                                ThrowErrorIfInvalidForModify;

                                                                IF "Settlement Period" = 0D THEN BEGIN
                                                                  "Due Date" := 0D;
                                                                  EXIT;
                                                                END;

                                                                ValidationMgt.ThrowErrorIf(IsSettlementPeriodInvalid);
                                                                CalcDueDate;
                                                              END;

                                                   CaptionML=[ENU=Settlement Period;
                                                              PTB=Per�odo Apura��o] }
    { 30  ;   ;Vendor No.          ;Code20        ;TableRelation=Vendor;
                                                   CaptionML=[ENU=Vendor No.;
                                                              PTB=N� Fornecedor] }
    { 40  ;   ;Tax Jurisdiction Code;Code20       ;TableRelation="Tax Jurisdiction" WHERE (Tax Revenue Code=FILTER(<>''),
                                                                                           Payment Bill Type=FILTER(<>' '));
                                                   OnValidate=VAR
                                                                taxJurisdiction@52006500 : Record 320;
                                                              BEGIN
                                                                ThrowErrorIfInvalidForModify;

                                                                "Settlement Period" := 0D;
                                                                "Due Date" := 0D;
                                                                IF "Tax Jurisdiction Code" = '' THEN BEGIN
                                                                  "Tax Revenue Code" := '';
                                                                  EXIT;
                                                                END;

                                                                CalcDueDate;

                                                                taxJurisdiction.GET("Tax Jurisdiction Code");
                                                                taxJurisdiction.TESTFIELD("Tax Revenue Code");
                                                                "Tax Revenue Code" := taxJurisdiction."Tax Revenue Code";
                                                              END;

                                                   CaptionML=[ENU=Tax Jurisdiction Code;
                                                              PTB=C�d. Jurisdi��o Imposto] }
    { 44  ;   ;Tax Revenue Code    ;Code10        ;TableRelation="Tax Revenue Code";
                                                   CaptionML=[ENU=Tax Revenue Code;
                                                              PTB=C�d. Receita Tributo] }
    { 48  ;   ;Due Date            ;Date          ;OnValidate=BEGIN
                                                                ThrowErrorIfInvalidForModify;
                                                              END;

                                                   CaptionML=[ENU=Due Date;
                                                              PTB=Data Vencimento] }
    { 64  ;   ;No. Series          ;Code10        ;TableRelation="No. Series";
                                                   CaptionML=[ENU=No. Series;
                                                              PTB=N� S�ries] }
    { 70  ;   ;Reference No.       ;Integer       ;OnValidate=BEGIN
                                                                ThrowErrorIfInvalidForModify;
                                                              END;

                                                   CaptionML=[ENU=Reference No.;
                                                              PTB=N� Refer�ncia];
                                                   MinValue=0;
                                                   BlankNumbers=BlankZero }
    { 80  ;   ;Alternative Payee Type;Option      ;OnValidate=BEGIN
                                                                VALIDATE("Alternative Payee No.", '');
                                                              END;

                                                   CaptionML=[ENU=Alternative Payee Type;
                                                              PTB=Tipo Emitente Alternativo];
                                                   OptionCaptionML=[ENU=" ,Branch,Customer,Vendor";
                                                                    PTB=" ,Filial,Cliente,Fornecedor"];
                                                   OptionString=[ ,Branch,Customer,Vendor] }
    { 84  ;   ;Alternative Payee No.;Code20       ;TableRelation=IF (Alternative Payee Type=CONST(Branch)) "Branch Information"
                                                                 ELSE IF (Alternative Payee Type=CONST(Customer)) Customer
                                                                 ELSE IF (Alternative Payee Type=CONST(Vendor)) Vendor;
                                                   OnValidate=VAR
                                                                branchInfo@52006500 : Record 52112423;
                                                                customer@52006501 : Record 18;
                                                                vendor@52006502 : Record 23;
                                                              BEGIN
                                                                ThrowErrorIfInvalidForModify;

                                                                IF "Alternative Payee No." = '' THEN BEGIN
                                                                  CNPJCPF := '';
                                                                  EXIT;
                                                                END;
                                                                CASE "Alternative Payee Type" OF
                                                                  "Alternative Payee Type"::Branch: BEGIN
                                                                    branchInfo.GET("Alternative Payee No.");
                                                                    branchInfo.TESTFIELD("C.N.P.J.");
                                                                    CNPJCPF := branchInfo."C.N.P.J.";
                                                                  END;
                                                                  "Alternative Payee Type"::Customer: BEGIN
                                                                    customer.GET("Alternative Payee No.");
                                                                    customer.TESTFIELD("C.N.P.J./C.P.F.");
                                                                    CNPJCPF := customer."C.N.P.J./C.P.F.";
                                                                  END;
                                                                  "Alternative Payee Type"::Vendor: BEGIN
                                                                    vendor.GET("Alternative Payee No.");
                                                                    vendor.TESTFIELD("C.N.P.J./C.P.F.");
                                                                    CNPJCPF := vendor."C.N.P.J./C.P.F.";
                                                                  END;
                                                                END;
                                                              END;

                                                   CaptionML=[ENU=Alternative Payee No.;
                                                              PTB=N� Emitente Alternativo] }
    { 86  ;   ;CNPJCPF             ;Text30        ;CaptionML=[ENU=Payee CNPJ/CPF;
                                                              PTB=CNPJ/CPF Emitente] }
    { 100 ;   ;Description         ;Text250       ;OnValidate=BEGIN
                                                                ThrowErrorIfInvalidForAddingEntries;
                                                              END;

                                                   CaptionML=[ENU=Description;
                                                              PTB=Descri��o] }
    { 110 ;   ;Principal Amount    ;Decimal       ;OnValidate=BEGIN
                                                                UpdateTotal;
                                                              END;

                                                   CaptionML=[ENU=Principal Amount;
                                                              PTB=Valor Principal];
                                                   DecimalPlaces=2:2;
                                                   MinValue=0;
                                                   BlankNumbers=BlankZero }
    { 120 ;   ;Interest Amount     ;Decimal       ;OnValidate=BEGIN
                                                                ThrowErrorIfInvalidForReleasing;
                                                                UpdateTotal;
                                                              END;

                                                   CaptionML=[ENU=Interest Amount;
                                                              PTB=Valor Juros];
                                                   DecimalPlaces=2:2;
                                                   MinValue=0;
                                                   BlankNumbers=BlankZero }
    { 130 ;   ;Charge Amount       ;Decimal       ;OnValidate=BEGIN
                                                                ThrowErrorIfInvalidForReleasing;
                                                                UpdateTotal;
                                                              END;

                                                   CaptionML=[ENU=Charge Amount;
                                                              PTB=Valor Multa];
                                                   DecimalPlaces=2:2;
                                                   MinValue=0;
                                                   BlankNumbers=BlankZero }
    { 140 ;   ;Total Amount        ;Decimal       ;CaptionML=[ENU=Total Amount;
                                                              PTB=Valor Total];
                                                   DecimalPlaces=2:2;
                                                   MinValue=0;
                                                   BlankNumbers=BlankZero }
    { 1000;   ;Vend. Ledg. Entries ;Integer       ;FieldClass=FlowField;
                                                   CalcFormula=Count("Vendor Ledger Entry" WHERE (Tax Payment Bill No.=FIELD(No.)));
                                                   CaptionML=[ENU=No. of Vend. Ledg. Entries;
                                                              PTB=N� Movs. Fornecedor];
                                                   Editable=No }
    { 1001;   ;Closed Vend. Ledg. Entries;Integer ;FieldClass=FlowField;
                                                   CalcFormula=Count("Vendor Ledger Entry" WHERE (Tax Payment Bill No.=FIELD(No.),
                                                                                                  Open=CONST(No)));
                                                   CaptionML=[ENU=Closed Vend. Ledg. Entries;
                                                              PTB=N� Movs. Fornecedor Fechados];
                                                   Editable=No }
    { 1010;   ;Open                ;Boolean       ;FieldClass=FlowField;
                                                   CalcFormula=-Exist("Vendor Ledger Entry" WHERE (Tax Payment Bill No.=FIELD(No.),
                                                                                                   Open=CONST(No)));
                                                   CaptionML=[ENU=Open;
                                                              PTB=Pendente];
                                                   Editable=No }
  }
  KEYS
  {
    {    ;No.                                     ;Clustered=Yes }
    {    ;Vendor No.,Settlement Period             }
    {    ;Status,Tax Revenue Code,CNPJCPF,Settlement Period }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      PurchSetup@35000000 : Record 312;
      NoSeriesMgt@35000001 : Codeunit 396;
      Text001@35000002 : TextConst 'ENU=DARF %1 already exists;PTB=Guia %1 j� existe.';
      ValidationMgt@52006500 : Codeunit 52112473;
      Text002@52006501 : TextConst 'ENU=Invalid Settlement Period %1 for %2 interval.;PTB=Per�odo de Apura��o %1 inv�lido para periodicidade %2.';
      Text003@52006502 : TextConst 'ENU=Period interval is not supported.;PTB=Periodicidade n�o suportada.';
      Text004@52006503 : TextConst 'ENU=Entries must be added to the payment bill before releasing.;PTB=Movimentos devem ser adicionados � guia antes da libera��o.';

    PROCEDURE AssistEdit@1(OldDARF@1000 : Record 52112469) : Boolean;
    VAR
      darf@35000000 : Record 52112469;
      darf2@1001 : Record 52112469;
    BEGIN
      WITH darf DO BEGIN
        COPY(Rec);
        PurchSetup.GET;
        PurchSetup.TESTFIELD("Tax Payment Bill Nos.");
        IF NoSeriesMgt.SelectSeries(PurchSetup."Tax Payment Bill Nos.", OldDARF."No. Series", "No. Series") THEN BEGIN
          NoSeriesMgt.SetSeries("No.");
          IF darf2.GET("No.") THEN
            ERROR(Text001, "No.");
          Rec := darf;
          EXIT(TRUE);
        END;
      END;
    END;

    PROCEDURE UpdateTotal@52006500();
    BEGIN
      "Total Amount" := "Principal Amount" + "Interest Amount" + "Charge Amount";
    END;

    PROCEDURE CalcDueDate@52006503();
    VAR
      taxDueDateCalc@52006500 : Record 52112442;
    BEGIN
      TESTFIELD("Tax Jurisdiction Code");

      IF "Settlement Period" = 0D THEN BEGIN
        "Due Date" := 0D;
        EXIT;
      END;

      "Due Date" := taxDueDateCalc.Calculate("Tax Jurisdiction Code", "Settlement Period");
    END;

    PROCEDURE Print@52006501();
    VAR
      taxPaymentBill@52006500 : Record 52112469;
      report@52006501 : Report 52112540;
    BEGIN
      taxPaymentBill.GET("No.");
      taxPaymentBill.SETRECFILTER;
      report.SETTABLEVIEW(taxPaymentBill);
      report.RUN;
    END;

    PROCEDURE GetSettlementPeriodStartDate@52006554(periodInterval@52006501 : Integer;refDate@52006500 : Date) : Date;
    VAR
      taxJurisdiction@52006503 : Record 320;
    BEGIN
      CASE periodInterval OF
        taxJurisdiction."Period Interval"::Diario:
          EXIT(refDate);
        taxJurisdiction."Period Interval"::Quinzenal:
          IF DATE2DMY(refDate, 1) <= 15 THEN
            EXIT(DMY2DATE(1, DATE2DMY(refDate, 2), DATE2DMY(refDate, 3)))
          ELSE
            EXIT(DMY2DATE(16, DATE2DMY(refDate, 2), DATE2DMY(refDate, 3)));
        taxJurisdiction."Period Interval"::Mensal:
          EXIT(DMY2DATE(1, DATE2DMY(refDate, 2), DATE2DMY(refDate, 3)))
      ELSE
        ERROR(Text003);
      END;
    END;

    PROCEDURE GetSettlementPeriodEndingDate@52006530(periodInterval@52006501 : Integer;refDate@52006500 : Date) : Date;
    VAR
      taxJurisdiction@52006503 : Record 320;
    BEGIN
      CASE periodInterval OF
        taxJurisdiction."Period Interval"::Diario:
          EXIT(refDate);
        taxJurisdiction."Period Interval"::Quinzenal:
          IF DATE2DMY(refDate, 1) <= 15 THEN
            EXIT(DMY2DATE(15, DATE2DMY(refDate, 2), DATE2DMY(refDate, 3)))
          ELSE
            EXIT(CALCDATE('<CM>', refDate));
        taxJurisdiction."Period Interval"::Mensal:
          EXIT(CALCDATE('<CM>', refDate));
      ELSE
        ERROR(Text003);
      END;
    END;

    PROCEDURE IsSettlementPeriodInvalid@52006506() : Text;
    VAR
      taxJurisdiction@52006500 : Record 320;
    BEGIN
      IF ("Tax Jurisdiction Code" = '') OR ("Settlement Period" = 0D) THEN
        EXIT;
      taxJurisdiction.GET("Tax Jurisdiction Code");
      IF "Settlement Period" <> GetSettlementPeriodEndingDate(taxJurisdiction."Period Interval", "Settlement Period") THEN
        EXIT(STRSUBSTNO(Text002, "Settlement Period", taxJurisdiction."Period Interval"));
    END;

    LOCAL PROCEDURE ValidateForRelease@52006502();
    VAR
      compInfo@52006500 : Record 79;
    BEGIN
      IF "Alternative Payee Type" = "Alternative Payee Type"::" " THEN BEGIN
        compInfo.GET;
        ValidationMgt.SetRecord(compInfo, compInfo.TABLECAPTION);
        ValidationMgt.AddRuleFieldNotBlank(compInfo.FIELDNO("C.N.P.J."));
        ValidationMgt.Validate;
      END;

      ValidationMgt.SetRecord(Rec, TABLECAPTION);
      ValidationMgt.AddRuleFieldValue(FIELDNO(Status), FORMAT(Status::Open));
      ValidationMgt.AddRuleFieldNotBlank(FIELDNO(Description));
      ValidationMgt.AddRuleFieldNotBlank(FIELDNO("Vendor No."));
      ValidationMgt.AddRuleFieldNotBlank(FIELDNO("Tax Jurisdiction Code"));
      ValidationMgt.AddRuleFieldNotBlank(FIELDNO("Tax Revenue Code"));
      ValidationMgt.AddRuleFieldNotBlank(FIELDNO("Settlement Period"));
      ValidationMgt.AddRuleFieldNotBlank(FIELDNO("Due Date"));
      IF "Principal Amount" = 0 THEN
        ValidationMgt.AddError(Text004);

      IF "Alternative Payee Type" <> "Alternative Payee Type"::" " THEN BEGIN
        ValidationMgt.AddRuleFieldNotBlank(FIELDNO("Alternative Payee No."));
        ValidationMgt.AddRuleFieldNotBlank(FIELDNO(CNPJCPF));
      END;

      ValidationMgt.AddErrorIf(IsSettlementPeriodInvalid);
      ValidationMgt.Validate;
    END;

    PROCEDURE ValidateForReopening@52006517();
    BEGIN
      ValidationMgt.SetRecord(Rec, TABLECAPTION);
      ValidationMgt.AddRuleFieldValue(FIELDNO("Closed Vend. Ledg. Entries"), FORMAT(0));
      ValidationMgt.Validate;
    END;

    PROCEDURE ValidateForAddingEntries@52006520();
    VAR
      taxJurisdiction@52006500 : Record 320;
    BEGIN
      ValidationMgt.SetRecord(Rec, TABLECAPTION);
      ValidationMgt.AddRuleFieldValue(FIELDNO(Status), FORMAT(Status::Open));
      ValidationMgt.AddRuleFieldNotBlank(FIELDNO("Vendor No."));
      ValidationMgt.AddRuleFieldNotBlank(FIELDNO("Tax Jurisdiction Code"));
      ValidationMgt.AddRuleFieldNotBlank(FIELDNO("Tax Revenue Code"));
      ValidationMgt.AddRuleFieldNotBlank(FIELDNO("Settlement Period"));
      ValidationMgt.Validate;
      IF "Tax Jurisdiction Code" <> '' THEN BEGIN
        taxJurisdiction.GET("Tax Jurisdiction Code");
        ValidationMgt.SetRecord(taxJurisdiction, TABLECAPTION);
        ValidationMgt.AddRuleFieldNotBlank(taxJurisdiction.FIELDNO("Period Interval"));
        ValidationMgt.Validate;
      END;
    END;

    LOCAL PROCEDURE ValidateForModify@52006504();
    BEGIN
      ValidationMgt.SetRecord(Rec, TABLECAPTION);
      ValidationMgt.AddRuleFieldValue(FIELDNO(Status), FORMAT(Status::Open));
      ValidationMgt.AddRuleFieldValue(FIELDNO("Vend. Ledg. Entries"), FORMAT('0'));
      ValidationMgt.Validate;
    END;

    LOCAL PROCEDURE ValidateForDelete@52006523();
    BEGIN
      ValidationMgt.SetRecord(Rec, TABLECAPTION);
      ValidationMgt.AddRuleFieldValue(FIELDNO(Status), FORMAT(Status::Open));
      ValidationMgt.Validate;
    END;

    PROCEDURE ThrowErrorIfInvalidForReleasing@52006507();
    BEGIN
      CLEAR(ValidationMgt);
      ValidateForRelease;
      ValidationMgt.ThrowErrorIfInvalid;
    END;

    PROCEDURE ThrowErrorIfInvalidForReopening@52006513();
    BEGIN
      CLEAR(ValidationMgt);
      ValidateForReopening;
      ValidationMgt.ThrowErrorIfInvalid;
    END;

    PROCEDURE ThrowErrorIfInvalidForAddingEntries@52006521();
    BEGIN
      CLEAR(ValidationMgt);
      ValidateForAddingEntries;
      ValidationMgt.ThrowErrorIfInvalid;
    END;

    PROCEDURE ThrowErrorIfInvalidForModify@52006505();
    BEGIN
      CLEAR(ValidationMgt);
      ValidateForModify;
      ValidationMgt.ThrowErrorIfInvalid;
    END;

    PROCEDURE ThrowErrorIfInvalidForDelete@52006525();
    BEGIN
      CLEAR(ValidationMgt);
      ValidateForDelete;
      ValidationMgt.ThrowErrorIfInvalid;
    END;

    PROCEDURE Release@52006508();
    BEGIN
      ThrowErrorIfInvalidForReleasing;
      Status := Status::Released;
      MODIFY;
    END;

    PROCEDURE Reopen@52006509();
    BEGIN
      ThrowErrorIfInvalidForReopening;
      Status := Status::Open;
      MODIFY;
    END;

    PROCEDURE AddEntries@52006510();
    VAR
      taxJurisdiction@52006501 : Record 320;
      vendLedgEntry@52006500 : Record 25;
      vendLedgEntries@52006502 : Page 29;
    BEGIN
      ThrowErrorIfInvalidForAddingEntries;
      taxJurisdiction.GET("Tax Jurisdiction Code");

      vendLedgEntry.FILTERGROUP(2);
      vendLedgEntry.SETCURRENTKEY("Vendor No.", "Tax Jurisdiction Code", Open, Positive, "Posting Date");
      vendLedgEntry.SETRANGE("Vendor No.", "Vendor No.");
      vendLedgEntry.SETRANGE("Tax Jurisdiction Code", "Tax Jurisdiction Code");
      vendLedgEntry.SETRANGE(Open, TRUE);
      vendLedgEntry.SETRANGE(Positive, FALSE);
      vendLedgEntry.SETRANGE("On Hold", '');
      vendLedgEntry.SETRANGE("Tax Payment Bill No.", '');
      vendLedgEntry.SETFILTER("Remaining Amt. (LCY)", '<0');
      vendLedgEntries.SETTABLEVIEW(vendLedgEntry);
      vendLedgEntries.LOOKUPMODE(TRUE);
      IF vendLedgEntries.RUNMODAL <> ACTION::LookupOK THEN
        EXIT;
      vendLedgEntry.RESET;
      vendLedgEntries.SetPageSelectionFilter(vendLedgEntry);
      vendLedgEntry.SETAUTOCALCFIELDS("Remaining Amt. (LCY)");
      IF vendLedgEntry.FINDSET(TRUE) THEN
        REPEAT
          vendLedgEntry."Tax Payment Bill No." := "No.";
          vendLedgEntry.MODIFY;
          VALIDATE("Principal Amount", "Principal Amount" + ABS(vendLedgEntry."Remaining Amt. (LCY)"));
        UNTIL vendLedgEntry.NEXT = 0;
      MODIFY;
    END;

    PROCEDURE RemoveEntry@52006518(VAR vendLedgEntry@52006500 : Record 25);
    BEGIN
      GET(vendLedgEntry."Tax Payment Bill No.");
      ThrowErrorIfInvalidForAddingEntries;

      vendLedgEntry."Tax Payment Bill No." := '';
      vendLedgEntry.MODIFY;

      vendLedgEntry.CALCFIELDS("Remaining Amt. (LCY)");
      VALIDATE("Principal Amount", "Principal Amount" + vendLedgEntry."Remaining Amt. (LCY)");
      MODIFY;
    END;

    BEGIN
    END.
  }
}

