OBJECT Table 35000511 Group Accounts/Native Account2
{
  OBJECT-PROPERTIES
  {
    Date=29/02/12;
    Time=18:14:18;
    Version List=SPED-ECD5.01.0109;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Group Accounts/Native Accounts;
               PTB=Natureza Conta/Grupo Contas];
    LookupPageID=Page35000522;
    DrillDownPageID=Page35000522;
  }
  FIELDS
  {
    { 1   ;   ;Code                ;Code20        ;OnValidate=BEGIN
                                                                IF AccontRef.GET(Code) THEN
                                                                  Description := AccontRef.Nome
                                                                ELSE
                                                                  Description := '';
                                                              END;

                                                   CaptionML=[ENU=Code;
                                                              PTB=C�digo] }
    { 2   ;   ;Description         ;Text80        ;CaptionML=[ENU=Description;
                                                              PTB=Descri��o] }
    { 5   ;   ;Totaling            ;Text250       ;ValidateTableRelation=No;
                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=Totaling;
                                                              PTB=Intervalo Contas] }
  }
  KEYS
  {
    {    ;Code                                    ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      AccontRef@1102300000 : Record 35000507;

    BEGIN
    END.
  }
}

