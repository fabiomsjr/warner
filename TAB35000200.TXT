OBJECT Table 35000200 Tax Posting 2
{
  OBJECT-PROPERTIES
  {
    Date=31/10/12;
    Time=11:01:17;
    Version List=FNX068.00;
  }
  PROPERTIES
  {
    OnDelete=VAR
               line@35000000 : Record 35000201;
             BEGIN
               line.SETRANGE("Tax Posting Code", Code);
               line.DELETEALL;
             END;

    CaptionML=[ENU=Tax Posting;
               PTB=Contabiliza��o Imposto];
    LookupPageID=Page35000202;
  }
  FIELDS
  {
    { 10  ;   ;Code                ;Code20        ;CaptionML=[ENU=Code;
                                                              PTB=C�digo] }
    { 20  ;   ;Description         ;Text250       ;CaptionML=[ENU=Description;
                                                              PTB=Descri��o] }
    { 30  ;   ;Item Cost/Price     ;Option        ;CaptionML=[ENU=Item Cost/Price;
                                                              PTB=Custo/Pre�o Item];
                                                   OptionCaptionML=[ENU=" ,Add,Subtract";
                                                                    PTB=" ,Somar,Subtrair"];
                                                   OptionString=[ ,Add,Subtract] }
    { 40  ;   ;Financial           ;Option        ;CaptionML=[ENU=Financial;
                                                              PTB=Financeiro];
                                                   OptionCaptionML=[ENU=" ,Add,Subtract,Add to Other Vendor";
                                                                    PTB=" ,Somar,Subtrair,Somar a Outro Fornecedor"];
                                                   OptionString=[ ,Add,Subtract,Add Other Vendor] }
    { 50  ;   ;Post Payable Tax    ;Boolean       ;CaptionML=[ENU=Post Payable Tax;
                                                              PTB=Imposto a Pagar] }
    { 60  ;   ;Post Expense        ;Boolean       ;CaptionML=[ENU=Post Expense;
                                                              PTB=Despesa] }
    { 70  ;   ;Post Tax Credit     ;Boolean       ;CaptionML=[ENU=Post Recoverable Tax;
                                                              PTB=Imposto a Recuperar] }
  }
  KEYS
  {
    {    ;Code                                    ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

