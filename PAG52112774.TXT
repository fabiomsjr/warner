OBJECT Page 52112774 Critical Analysis Value
{
  OBJECT-PROPERTIES
  {
    Date=04/12/13;
    Time=16:55:02;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Critical Analysis Value;
               PTB=An�lise Cr�tica Valor];
    SourceTable=Table52112774;
    DelayedInsert=Yes;
    PageType=List;
  }
  CONTROLS
  {
    { 1000000000;0;Container;
                ContainerType=ContentArea }

    { 1000000001;1;Group  ;
                Name=Group;
                GroupType=Repeater }

    { 1000000002;2;Field  ;
                SourceExpr=Value }

    { 1000000003;2;Field  ;
                SourceExpr=Description }

    { 1000000004;2;Field  ;
                SourceExpr="Rate %" }

    { 52006500;2;Field    ;
                SourceExpr=Flag }

  }
  CODE
  {

    BEGIN
    END.
  }
}

