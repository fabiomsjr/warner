OBJECT Table 99008504 Inbound Product Catalog Hdr.
{
  OBJECT-PROPERTIES
  {
    Date=29/02/12;
    Time=18:14:20;
    Version List=NAVW16.00;
  }
  PROPERTIES
  {
    DataCaptionFields=Inbound Document No.;
    OnInsert=BEGIN
               InboundProductCatalogHdr.RESET;
               IF InboundProductCatalogHdr.FIND('+') THEN
                 "Inbound Document No." := InboundProductCatalogHdr."Inbound Document No." + 1
               ELSE
                 "Inbound Document No." := 1;
             END;

    OnDelete=BEGIN
               InbProductCatalogLine.LOCKTABLE;
               InbProductCatalogLine.SETRANGE("Inbound Document No.","Inbound Document No.");
               InbProductCatalogLine.DELETEALL;
             END;

    CaptionML=[ENU=Inbound Product Catalog Hdr.;
               PTB=Cab. Cat�logo Produto Entrada];
    LookupPageID=Page99008522;
  }
  FIELDS
  {
    { 1   ;   ;Vendor No.          ;Code20        ;AltSearchField=Search Name;
                                                   OnValidate=VAR
                                                                Vend@1000 : Record 23;
                                                              BEGIN
                                                                Vend.GET("Vendor No.");
                                                                "Vendor Name" := Vend.Name;
                                                                "Vendor Address" := Vend.Address;
                                                                "Vendor Address 2" := Vend."Address 2";
                                                                "Vendor City" := Vend.City;
                                                                "Vendor Post Code" := Vend."Post Code";
                                                                "Vendor County" := Vend.County;
                                                                "Vendor Contact" := Vend.Contact;
                                                              END;

                                                   CaptionML=[ENU=Vendor No.;
                                                              PTB=N� Fornecedor] }
    { 2   ;   ;Vendor Name         ;Text50        ;CaptionML=[ENU=Vendor Name;
                                                              PTB=Nome Fornecedor] }
    { 3   ;   ;Search Name         ;Code50        ;CaptionML=[ENU=Search Name;
                                                              PTB=Nome Fantasia] }
    { 4   ;   ;Name 2              ;Text50        ;CaptionML=[ENU=Name 2;
                                                              PTB=Nome Complementar] }
    { 5   ;   ;Vendor Address      ;Text50        ;CaptionML=[ENU=Vendor Address;
                                                              PTB=Endere�o Fornecedor] }
    { 6   ;   ;Vendor Address 2    ;Text50        ;CaptionML=[ENU=Vendor Address 2;
                                                              PTB=Endere�o Complementar Fornecedor] }
    { 7   ;   ;Vendor City         ;Text30        ;CaptionML=[ENU=Vendor City;
                                                              PTB=Cidade Fornecedor] }
    { 8   ;   ;Vendor Contact      ;Text50        ;CaptionML=[ENU=Vendor Contact;
                                                              PTB=Contato Fornecedor] }
    { 9   ;   ;Vendor Phone No.    ;Text30        ;ExtendedDatatype=Phone No.;
                                                   CaptionML=[ENU=Vendor Phone No.;
                                                              PTB=Telefone Fornecedor] }
    { 10  ;   ;Telex No.           ;Text20        ;CaptionML=[ENU=Telex No.;
                                                              PTB=N� Telex] }
    { 14  ;   ;Our Account No.     ;Text20        ;CaptionML=[ENU=Our Account No.;
                                                              PTB=Nosso N� Conta] }
    { 15  ;   ;Territory Code      ;Code10        ;CaptionML=[ENU=Territory Code;
                                                              PTB=Cod. Unidade Federal] }
    { 19  ;   ;Budgeted Amount     ;Decimal       ;CaptionML=[ENU=Budgeted Amount;
                                                              PTB=Valor Or�ado];
                                                   AutoFormatType=1 }
    { 21  ;   ;Vendor Posting Group;Code10        ;CaptionML=[ENU=Vendor Posting Group;
                                                              PTB=Gr. Cont�bil Fornecedor] }
    { 22  ;   ;Currency Code       ;Code10        ;CaptionML=[ENU=Currency Code;
                                                              PTB=Cod. Moeda] }
    { 24  ;   ;Language Code       ;Code10        ;CaptionML=[ENU=Language Code;
                                                              PTB=Cod. Idioma] }
    { 26  ;   ;Statistics Group    ;Integer       ;CaptionML=[ENU=Statistics Group;
                                                              PTB=Gr. Estat�stico] }
    { 27  ;   ;Payment Terms Code  ;Code10        ;CaptionML=[ENU=Payment Terms Code;
                                                              PTB=Cod. Termos Pagto.] }
    { 28  ;   ;Fin. Charge Terms Code;Code10      ;CaptionML=[ENU=Fin. Charge Terms Code;
                                                              PTB=Cod. Juros] }
    { 29  ;   ;Purchaser Code      ;Code10        ;CaptionML=[ENU=Purchaser Code;
                                                              PTB=Cod. Comprador] }
    { 30  ;   ;Shipment Method Code;Code10        ;CaptionML=[ENU=Shipment Method Code;
                                                              PTB=Cod. Condi��es Envio] }
    { 31  ;   ;Shipping Agent Code ;Code10        ;CaptionML=[ENU=Shipping Agent Code;
                                                              PTB=Cod. Transportador] }
    { 33  ;   ;Invoice Disc. Code  ;Code20        ;ValidateTableRelation=No;
                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=Invoice Disc. Code;
                                                              PTB=Cod. Desconto N.Fiscal] }
    { 35  ;   ;Vendor Country/Region Code;Code10  ;CaptionML=[ENU=Vendor Country/Region Code;
                                                              PTB=C�digo Pa�s/Regi�o Fornecedor] }
    { 38  ;   ;Comment             ;Boolean       ;CaptionML=[ENU=Comment;
                                                              PTB=Coment�rio];
                                                   Editable=Yes }
    { 39  ;   ;Blocked             ;Boolean       ;CaptionML=[ENU=Blocked;
                                                              PTB=Bloqueado] }
    { 45  ;   ;Pay-to Vendor No.   ;Code20        ;CaptionML=[ENU=Pay-to Vendor No.;
                                                              PTB=Pagto.-a N� Forn.] }
    { 46  ;   ;Priority            ;Integer       ;CaptionML=[ENU=Priority;
                                                              PTB=Prioridade] }
    { 47  ;   ;Payment Method Code ;Code10        ;CaptionML=[ENU=Payment Method Code;
                                                              PTB=Cod. Forma Pagamento] }
    { 54  ;   ;Last Date Modified  ;Date          ;CaptionML=[ENU=Last Date Modified;
                                                              PTB=�ltima Data Modifica��o];
                                                   Editable=Yes }
    { 55  ;   ;Date Filter         ;Date          ;CaptionML=[ENU=Date Filter;
                                                              PTB=Filtro Data] }
    { 56  ;   ;Department Filter   ;Code10        ;CaptionML=[ENU=Department Filter;
                                                              PTB=Filtro Departamento] }
    { 57  ;   ;Project Filter      ;Code10        ;CaptionML=[ENU=Project Filter;
                                                              PTB=Filtro Projeto] }
    { 58  ;   ;Balance             ;Decimal       ;CaptionML=[ENU=Balance;
                                                              PTB=Saldo];
                                                   AutoFormatType=1 }
    { 59  ;   ;Balance (LCY)       ;Decimal       ;CaptionML=[ENU=Balance (LCY);
                                                              PTB=Saldo (ML)];
                                                   AutoFormatType=1 }
    { 60  ;   ;Net Change          ;Decimal       ;CaptionML=[ENU=Net Change;
                                                              PTB=Saldo Per�odo];
                                                   Editable=No;
                                                   AutoFormatType=1 }
    { 61  ;   ;Net Change (LCY)    ;Decimal       ;CaptionML=[ENU=Net Change (LCY);
                                                              PTB=Saldo Per�odo (ML)];
                                                   AutoFormatType=1 }
    { 62  ;   ;Purchases (LCY)     ;Decimal       ;CaptionML=[ENU=Purchases (LCY);
                                                              PTB=Compras (ML)];
                                                   AutoFormatType=1 }
    { 64  ;   ;Inv. Discounts (LCY);Decimal       ;CaptionML=[ENU=Inv. Discounts (LCY);
                                                              PTB=Descontos N.Fiscal (ML)];
                                                   AutoFormatType=1 }
    { 65  ;   ;Pmt. Discounts (LCY);Decimal       ;CaptionML=[ENU=Pmt. Discounts (LCY);
                                                              PTB=Desconto no Pagamento (ML)];
                                                   AutoFormatType=1 }
    { 66  ;   ;Balance Due         ;Decimal       ;CaptionML=[ENU=Balance Due;
                                                              PTB=Saldo Vencido];
                                                   AutoFormatType=1 }
    { 67  ;   ;Balance Due (LCY)   ;Decimal       ;CaptionML=[ENU=Balance Due (LCY);
                                                              PTB=Saldo Vencido (ML)];
                                                   AutoFormatType=1 }
    { 69  ;   ;Payments            ;Decimal       ;CaptionML=[ENU=Payments;
                                                              PTB=Pagamentos];
                                                   AutoFormatType=1 }
    { 70  ;   ;Invoice Amounts     ;Decimal       ;CaptionML=[ENU=Invoice Amounts;
                                                              PTB=Valores Faturados];
                                                   AutoFormatType=1 }
    { 71  ;   ;Cr. Memo Amounts    ;Decimal       ;CaptionML=[ENU=Cr. Memo Amounts;
                                                              PTB=Valor Nota de Cr�dito];
                                                   AutoFormatType=1 }
    { 72  ;   ;Finance Charge Memo Amounts;Decimal;CaptionML=[ENU=Finance Charge Memo Amounts;
                                                              PTB=Valor Notas de Juros];
                                                   AutoFormatType=1 }
    { 74  ;   ;Payments (LCY)      ;Decimal       ;CaptionML=[ENU=Payments (LCY);
                                                              PTB=Pagtos. (ML)];
                                                   Editable=Yes;
                                                   AutoFormatType=1 }
    { 75  ;   ;Inv. Amounts (LCY)  ;Decimal       ;CaptionML=[ENU=Inv. Amounts (LCY);
                                                              PTB=Faturado (ML)];
                                                   AutoFormatType=1 }
    { 76  ;   ;Cr. Memo Amounts (LCY);Decimal     ;CaptionML=[ENU=Cr. Memo Amounts (LCY);
                                                              PTB=Nota de Cr�dito (ML)];
                                                   AutoFormatType=1 }
    { 77  ;   ;Fin. Charge Memo Amounts (LCY);Decimal;
                                                   CaptionML=[ENU=Fin. Charge Memo Amounts (LCY);
                                                              PTB=Valores Notas de Juros (ML)];
                                                   AutoFormatType=1 }
    { 78  ;   ;Outstanding Orders  ;Decimal       ;CaptionML=[ENU=Outstanding Orders;
                                                              PTB=Valores Ped. Pendentes];
                                                   AutoFormatType=1 }
    { 79  ;   ;Amt. Rcd. Not Invoiced;Decimal     ;CaptionML=[ENU=Amt. Rcd. Not Invoiced;
                                                              PTB=Valor Rec. N�o Faturado];
                                                   AutoFormatType=1 }
    { 80  ;   ;Application Method  ;Option        ;CaptionML=[ENU=Application Method;
                                                              PTB=Forma Liquida��o];
                                                   OptionCaptionML=[ENU=Manual,Apply to Oldest;
                                                                    PTB=Manual,Por Antiguidade];
                                                   OptionString=Manual,Apply to Oldest }
    { 84  ;   ;Fax No.             ;Text30        ;CaptionML=[ENU=Fax No.;
                                                              PTB=N� Fax] }
    { 85  ;   ;Telex Answer Back   ;Text20        ;CaptionML=[ENU=Telex Answer Back;
                                                              PTB=N� Telex Resposta] }
    { 86  ;   ;VAT Registration No.;Text20        ;CaptionML=[ENU=VAT Registration No.;
                                                              PTB=N� Contribuinte] }
    { 88  ;   ;Gen. Bus. Posting Group;Code10     ;CaptionML=[ENU=Gen. Bus. Posting Group;
                                                              PTB=Gr. Cont�bil Neg�cio] }
    { 89  ;   ;Picture             ;BLOB          ;CaptionML=[ENU=Picture;
                                                              PTB=Imagem];
                                                   SubType=Bitmap }
    { 91  ;   ;Vendor Post Code    ;Code20        ;ValidateTableRelation=No;
                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=Vendor Post Code;
                                                              PTB=CEP Fornecedor] }
    { 92  ;   ;Vendor County       ;Text30        ;CaptionML=[ENU=Vendor County;
                                                              PTB=Distrito Fornecedor] }
    { 97  ;   ;Debit Amount        ;Decimal       ;CaptionML=[ENU=Debit Amount;
                                                              PTB=Valor D�bito];
                                                   BlankZero=Yes;
                                                   AutoFormatType=1 }
    { 98  ;   ;Credit Amount       ;Decimal       ;CaptionML=[ENU=Credit Amount;
                                                              PTB=Valor Cr�dito];
                                                   BlankZero=Yes;
                                                   AutoFormatType=1 }
    { 99  ;   ;Debit Amount (LCY)  ;Decimal       ;CaptionML=[ENU=Debit Amount (LCY);
                                                              PTB=Valor D�bito (ML)];
                                                   BlankZero=Yes;
                                                   AutoFormatType=1 }
    { 100 ;   ;Credit Amount (LCY) ;Decimal       ;CaptionML=[ENU=Credit Amount (LCY);
                                                              PTB=Valor Cr�dito (ML)];
                                                   BlankZero=Yes;
                                                   AutoFormatType=1 }
    { 102 ;   ;E-Mail              ;Text80        ;ExtendedDatatype=E-Mail;
                                                   CaptionML=[ENU=E-Mail;
                                                              PTB=E-Mail] }
    { 103 ;   ;Home Page           ;Text80        ;ExtendedDatatype=URL;
                                                   CaptionML=[ENU=Home Page;
                                                              PTB=Home Page] }
    { 104 ;   ;Reminder Amounts    ;Decimal       ;CaptionML=[ENU=Reminder Amounts;
                                                              PTB=Valor Carta Aviso];
                                                   AutoFormatType=1 }
    { 105 ;   ;Reminder Amounts (LCY);Decimal     ;CaptionML=[ENU=Reminder Amounts (LCY);
                                                              PTB=Valor Carta Aviso (ML)];
                                                   AutoFormatType=1 }
    { 107 ;   ;No. Series          ;Code10        ;CaptionML=[ENU=No. Series;
                                                              PTB=N� S�ries] }
    { 108 ;   ;VAT Area Code       ;Code20        ;CaptionML=[ENU=VAT Area Code;
                                                              PTB=Cod �rea IPI] }
    { 109 ;   ;Tax Liable          ;Boolean       ;CaptionML=[ENU=Tax Liable;
                                                              PTB=Sujeito a Imposto] }
    { 110 ;   ;Tax Bus. Posting Group;Code10      ;CaptionML=[ENU=Tax Bus. Posting Group;
                                                              PTB=Grupo Registro Imposto Neg.] }
    { 111 ;   ;Currency Filter     ;Code10        ;FieldClass=FlowFilter;
                                                   CaptionML=[ENU=Currency Filter;
                                                              PTB=Filtro Moeda] }
    { 113 ;   ;Outstanding Orders (LCY);Decimal   ;CaptionML=[ENU=Outstanding Orders (LCY);
                                                              PTB=Pedidos Pendentes (ML)];
                                                   AutoFormatType=1 }
    { 114 ;   ;Amt. Rcd. Not Invoiced (LCY);Decimal;
                                                   CaptionML=[ENU=Amt. Rcd. Not Invoiced (LCY);
                                                              PTB=Valor Rec. N�o Faturado (ML)];
                                                   AutoFormatType=1 }
    { 115 ;   ;Reserve             ;Option        ;CaptionML=[ENU=Reserve;
                                                              PTB=Reserva];
                                                   OptionCaptionML=[ENU=Never,Optional,Always;
                                                                    PTB=Nunca,Opcional,Sempre];
                                                   OptionString=Never,Optional,Always }
    { 99008500;;Inbound Document No.;Integer      ;CaptionML=[ENU=Inbound Document No.;
                                                              PTB=N� Documento Entrada] }
    { 99008501;;Date Received      ;Date          ;CaptionML=[ENU=Date Received;
                                                              PTB=Data recep��o] }
    { 99008502;;Time Received      ;Time          ;CaptionML=[ENU=Time Received;
                                                              PTB=Hora Recep��o] }
    { 99008503;;Date Processed     ;Date          ;CaptionML=[ENU=Date Processed;
                                                              PTB=Data Processamento] }
    { 99008504;;Time Processed     ;Time          ;CaptionML=[ENU=Time Processed;
                                                              PTB=Hora Processamento] }
    { 99008505;;Status             ;Option        ;CaptionML=[ENU=Status;
                                                              PTB=Status];
                                                   OptionCaptionML=[ENU=Pending,Accepted,Rejected,Processing;
                                                                    PTB=Pendente,Aceite,Rejeitado,Processamento];
                                                   OptionString=Pending,Accepted,Rejected,Processing }
    { 99008506;;Tracking ID        ;Text50        ;CaptionML=[ENU=Tracking ID;
                                                              PTB=ID Rastreabilidade] }
    { 99008507;;Auto. Accept Failed;Boolean       ;CaptionML=[ENU=Auto. Accept Failed;
                                                              PTB=Aceita��o Auto. Falhada] }
    { 99008508;;BizTalk Document Type;Option      ;CaptionML=[ENU=BizTalk Document Type;
                                                              PTB=Tipo Documento BizTalk];
                                                   OptionCaptionML=[ENU=Product Catalog;
                                                                    PTB=Cat�logo Produto];
                                                   OptionString=Product Catalog }
    { 99008509;;Doc. Transformation Succeeded;Boolean;
                                                   CaptionML=[ENU=Doc. Transformation Succeeded;
                                                              PTB=Transforma��o Doc. c/ Sucesso] }
    { 99008510;;Document Selected  ;Boolean       ;CaptionML=[ENU=Document Selected;
                                                              PTB=Documento Selecionado] }
  }
  KEYS
  {
    {    ;Inbound Document No.                    ;Clustered=Yes }
    {    ;Status                                   }
    {    ;Tracking ID                              }
  }
  FIELDGROUPS
  {
    { 1   ;DropDown            ;Inbound Document No.,Vendor No.,Statistics Group }
  }
  CODE
  {
    VAR
      Text000@1000 : TextConst 'ENU="The information in the document has been rejected and may no longer be valid. ";PTB=O informa��o no documento foi rejeiitada e n�o � mais v�lida.';
      Text001@1001 : TextConst 'ENU=Do you want to continue?;PTB=Deseja continuar?';
      InboundProductCatalogHdr@1002 : Record 99008504;
      InbProductCatalogLine@1003 : Record 99008505;

    PROCEDURE RejectProductCatalog@1();
    BEGIN
      Status := Status::Rejected;
      MODIFY;
    END;

    PROCEDURE AcceptProductCatalog@2();
    BEGIN
      IF Status = Status::Rejected THEN
        IF NOT CONFIRM(Text000 +
                       Text001,TRUE) THEN
          EXIT;
      CODEUNIT.RUN(99008507,Rec);
    END;

    BEGIN
    END.
  }
}

