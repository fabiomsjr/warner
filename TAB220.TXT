OBJECT Table 220 Business Unit
{
  OBJECT-PROPERTIES
  {
    Date=16/09/15;
    Time=13:22:52;
    Version List=NAVW18.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Business Unit;
               PTB=Empresa];
    LookupPageID=Page240;
  }
  FIELDS
  {
    { 1   ;   ;Code                ;Code10        ;CaptionML=[ENU=Code;
                                                              PTB=C�digo];
                                                   NotBlank=Yes }
    { 2   ;   ;Consolidate         ;Boolean       ;InitValue=Yes;
                                                   CaptionML=[ENU=Consolidate;
                                                              PTB=Consolidar] }
    { 3   ;   ;Consolidation %     ;Decimal       ;InitValue=100;
                                                   CaptionML=[ENU=Consolidation %;
                                                              PTB=Consolida��o %];
                                                   DecimalPlaces=1:1;
                                                   MinValue=0;
                                                   MaxValue=100 }
    { 4   ;   ;Starting Date       ;Date          ;CaptionML=[ENU=Starting Date;
                                                              PTB=Data Inicial] }
    { 5   ;   ;Ending Date         ;Date          ;CaptionML=[ENU=Ending Date;
                                                              PTB=Data Final] }
    { 6   ;   ;Income Currency Factor;Decimal     ;InitValue=1;
                                                   CaptionML=[ENU=Income Currency Factor;
                                                              PTB=Fator Moeda Receita/Despesa];
                                                   DecimalPlaces=0:15;
                                                   MinValue=0;
                                                   Editable=No }
    { 7   ;   ;Balance Currency Factor;Decimal    ;InitValue=1;
                                                   CaptionML=[ENU=Balance Currency Factor;
                                                              PTB=Fator Moeda Saldo];
                                                   DecimalPlaces=0:15;
                                                   MinValue=0;
                                                   Editable=No }
    { 8   ;   ;Exch. Rate Losses Acc.;Code20      ;TableRelation="G/L Account";
                                                   OnValidate=BEGIN
                                                                CheckGLAcc("Exch. Rate Losses Acc.");
                                                              END;

                                                   CaptionML=[ENU=Exch. Rate Losses Acc.;
                                                              PTB=Conta Perda Taxa C�mbio] }
    { 9   ;   ;Exch. Rate Gains Acc.;Code20       ;TableRelation="G/L Account";
                                                   OnValidate=BEGIN
                                                                CheckGLAcc("Exch. Rate Gains Acc.");
                                                              END;

                                                   CaptionML=[ENU=Exch. Rate Gains Acc.;
                                                              PTB=Conta Dif. C�mbio Fav.] }
    { 10  ;   ;Residual Account    ;Code20        ;TableRelation="G/L Account";
                                                   OnValidate=BEGIN
                                                                CheckGLAcc("Residual Account");
                                                              END;

                                                   CaptionML=[ENU=Residual Account;
                                                              PTB=Conta Aj. Residual] }
    { 11  ;   ;Last Balance Currency Factor;Decimal;
                                                   InitValue=1;
                                                   CaptionML=[ENU=Last Balance Currency Factor;
                                                              PTB=�ltimo Fator Saldo Moeda];
                                                   DecimalPlaces=0:15;
                                                   Editable=No }
    { 12  ;   ;Name                ;Text30        ;CaptionML=[ENU=Name;
                                                              PTB=Nome] }
    { 13  ;   ;Company Name        ;Text30        ;TableRelation=Company.Name;
                                                   ValidateTableRelation=No;
                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=Company Name;
                                                              PTB=Nome Empresa] }
    { 14  ;   ;Currency Code       ;Code10        ;TableRelation=Currency;
                                                   OnValidate=BEGIN
                                                                "Income Currency Factor" := CurrExchRate.ExchangeRate(WORKDATE,"Currency Code");
                                                                "Balance Currency Factor" := CurrExchRate.ExchangeRate(WORKDATE,"Currency Code");
                                                              END;

                                                   CaptionML=[ENU=Currency Code;
                                                              PTB=Cod. Moeda] }
    { 15  ;   ;Comp. Exch. Rate Gains Acc.;Code20 ;TableRelation="G/L Account";
                                                   OnValidate=BEGIN
                                                                CheckGLAcc("Comp. Exch. Rate Gains Acc.");
                                                              END;

                                                   CaptionML=[ENU=Comp. Exch. Rate Gains Acc.;
                                                              PTB=Conta Ganhos Taxa de C�mbio] }
    { 16  ;   ;Comp. Exch. Rate Losses Acc.;Code20;TableRelation="G/L Account";
                                                   OnValidate=BEGIN
                                                                CheckGLAcc("Comp. Exch. Rate Losses Acc.");
                                                              END;

                                                   CaptionML=[ENU=Comp. Exch. Rate Losses Acc.;
                                                              PTB=Conta Perdas Taxa de C�mbio] }
    { 17  ;   ;Equity Exch. Rate Gains Acc.;Code20;TableRelation="G/L Account";
                                                   OnValidate=BEGIN
                                                                CheckGLAcc("Equity Exch. Rate Gains Acc.");
                                                              END;

                                                   CaptionML=[ENU=Equity Exch. Rate Gains Acc.;
                                                              PTB=Conta de Equidade Ganho Taxa C�mbio] }
    { 18  ;   ;Equity Exch. Rate Losses Acc.;Code20;
                                                   TableRelation="G/L Account";
                                                   OnValidate=BEGIN
                                                                CheckGLAcc("Equity Exch. Rate Losses Acc.");
                                                              END;

                                                   CaptionML=[ENU=Equity Exch. Rate Losses Acc.;
                                                              PTB=Conta de Equidade Perda Taxa C�mbio] }
    { 19  ;   ;Minority Exch. Rate Gains Acc.;Code20;
                                                   TableRelation="G/L Account";
                                                   OnValidate=BEGIN
                                                                CheckGLAcc("Minority Exch. Rate Gains Acc.");
                                                              END;

                                                   CaptionML=[ENU=Minority Exch. Rate Gains Acc.;
                                                              PTB=Conta Ganho Minoria Taxa C�mbio] }
    { 20  ;   ;Minority Exch. Rate Losses Acc;Code20;
                                                   TableRelation="G/L Account";
                                                   OnValidate=BEGIN
                                                                CheckGLAcc("Minority Exch. Rate Losses Acc");
                                                              END;

                                                   CaptionML=[ENU=Minority Exch. Rate Losses Acc;
                                                              PTB=Conta Perda Minoria Taxa C�mbio] }
    { 21  ;   ;Currency Exchange Rate Table;Option;AccessByPermission=TableData 4=R;
                                                   CaptionML=[ENU=Currency Exchange Rate Table;
                                                              PTB=Tabela Taxa Cambio];
                                                   OptionCaptionML=[ENU=Local,Business Unit;
                                                                    PTB=Local,Unidade Neg�cio];
                                                   OptionString=Local,Business Unit }
    { 22  ;   ;Data Source         ;Option        ;CaptionML=[ENU=Data Source;
                                                              PTB=Origem Dados];
                                                   OptionCaptionML=[ENU=Local Curr. (LCY),Add. Rep. Curr. (ACY);
                                                                    PTB=Moeda Local (ML),Moeda Reporte Adicional];
                                                   OptionString=Local Curr. (LCY),Add. Rep. Curr. (ACY) }
    { 23  ;   ;File Format         ;Option        ;CaptionML=[ENU=File Format;
                                                              PTB=Formato Arquivo];
                                                   OptionCaptionML=[ENU=Version 4.00 or Later (.xml),Version 3.70 or Earlier (.txt);
                                                                    PTB=Vers�o 4.00 ou maior (.xml),Vers�o 3.70 ou anterior (.txt)];
                                                   OptionString=Version 4.00 or Later (.xml),Version 3.70 or Earlier (.txt) }
  }
  KEYS
  {
    {    ;Code                                    ;Clustered=Yes }
    {    ;Company Name                             }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      CurrExchRate@1000 : Record 330;

    LOCAL PROCEDURE CheckGLAcc@2(AccNo@1000 : Code[20]);
    VAR
      GLAcc@1001 : Record 15;
    BEGIN
      IF AccNo <> '' THEN BEGIN
        GLAcc.GET(AccNo);
        GLAcc.CheckGLAcc;
      END;
    END;

    BEGIN
    END.
  }
}

