OBJECT Page 52112477 Document Files
{
  OBJECT-PROPERTIES
  {
    Date=02/06/15;
    Time=09:01:45;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Document Files;
               PTB=Arquivos Documento];
    SourceTable=Table52112463;
    DelayedInsert=Yes;
    PageType=List;
    AutoSplitKey=Yes;
    OnOpenPage=BEGIN
                 CurrPage.EDITABLE(Editable);
               END;

    ActionList=ACTIONS
    {
      { 52006505;    ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 52006506;1   ;Action    ;
                      Name=Send;
                      CaptionML=[ENU=Open...;
                                 PTB=Enviar...];
                      Promoted=Yes;
                      Enabled=Editable;
                      PromotedIsBig=Yes;
                      Image=Import;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 UploadFile;
                                 CurrPage.UPDATE(TRUE);
                               END;
                                }
      { 52006503;1   ;Action    ;
                      Name=Open;
                      CaptionML=[ENU=Open...;
                                 PTB=Abrir...];
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=Save;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 SaveAs;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 52006500;0;Container;
                ContainerType=ContentArea }

    { 52006501;1;Group    ;
                Name=Group;
                Editable=Editable;
                GroupType=Repeater }

    { 52006507;2;Field    ;
                CaptionML=[ENU=Record ID;
                           PTB=ID Registro];
                SourceExpr=FORMAT("Record ID");
                Visible=FALSE;
                Editable=FALSE }

    { 52006502;2;Field    ;
                AssistEdit=Yes;
                SourceExpr=Filename;
                OnAssistEdit=BEGIN
                               UploadFile;
                             END;
                              }

    { 52006504;2;Field    ;
                SourceExpr=Description }

  }
  CODE
  {
    VAR
      Editable@52006500 : Boolean;

    PROCEDURE SetEditable@52006503(_Editable@52006500 : Boolean);
    BEGIN
      Editable := _Editable;
    END;

    BEGIN
    END.
  }
}

