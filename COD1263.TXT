OBJECT Codeunit 1263 Imp. Bank Conv.-Pre-Mapping
{
  OBJECT-PROPERTIES
  {
    Date=09/09/14;
    Time=12:00:00;
    Version List=NAVW18.00;
  }
  PROPERTIES
  {
    TableNo=274;
    OnRun=VAR
            PostingExch@1001 : Record 1220;
            PrePostProcessXMLImport@1000 : Codeunit 1262;
          BEGIN
            PostingExch.GET("Posting Exch. Entry No.");
            PrePostProcessXMLImport.PreProcessFile(PostingExch,StmtNoPathFilterTxt);
            PrePostProcessXMLImport.PreProcessBankAccount(PostingExch,"Bank Account No.",StmtBankAccNoPathFilterTxt,CurrCodePathFilterTxt);
          END;

  }
  CODE
  {
    VAR
      StmtBankAccNoPathFilterTxt@1000 : TextConst '@@@={Locked};ENU=/reportExportResponse/return/finsta/ownbankaccountidentification/bankaccount';
      CurrCodePathFilterTxt@1001 : TextConst '@@@={Locked};ENU="=''/reportExportResponse/return/finsta/statementdetails/amountdetails/currency''|=''/reportExportResponse/return/finsta/transactions/posting/currency''"';
      StmtNoPathFilterTxt@1007 : TextConst '@@@={Locked};ENU=/reportExportResponse/return/finsta/statementdetails/statementno';

    BEGIN
    END.
  }
}

