OBJECT Page 5600 Fixed Asset Card
{
  OBJECT-PROPERTIES
  {
    Date=23/09/13;
    Time=12:00:00;
    Version List=NAVW17.10,NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Fixed Asset Card;
               PTB=Cart�o Ativo Fixo];
    SourceTable=Table5600;
    PageType=ListPlus;
    RefreshOnActivate=Yes;
    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 47      ;1   ;ActionGroup;
                      CaptionML=[ENU=Fixed &Asset;
                                 PTB=&Ativo Fixo];
                      Image=FixedAssets }
      { 51      ;2   ;Action    ;
                      CaptionML=[ENU=Depreciation &Books;
                                 PTB=Livros Deprecia��o];
                      RunObject=Page 5619;
                      RunPageLink=FA No.=FIELD(No.);
                      Promoted=Yes;
                      Image=DepreciationBooks;
                      PromotedCategory=Process }
      { 40      ;2   ;Action    ;
                      ShortCutKey=F7;
                      CaptionML=[ENU=Statistics;
                                 PTB=Estat�sticas];
                      RunObject=Page 5602;
                      RunPageLink=FA No.=FIELD(No.);
                      Promoted=Yes;
                      Image=Statistics;
                      PromotedCategory=Process }
      { 84      ;2   ;Action    ;
                      ShortCutKey=Shift+Ctrl+D;
                      CaptionML=[ENU=Dimensions;
                                 PTB=Dimens�es];
                      RunObject=Page 540;
                      RunPageLink=Table ID=CONST(5600),
                                  No.=FIELD(No.);
                      Image=Dimensions }
      { 35      ;2   ;Action    ;
                      CaptionML=[ENU=Maintenance &Registration;
                                 PTB=Registro Manuten��o];
                      RunObject=Page 5625;
                      RunPageLink=FA No.=FIELD(No.);
                      Promoted=Yes;
                      Image=MaintenanceRegistrations;
                      PromotedCategory=Process }
      { 36      ;2   ;Action    ;
                      CaptionML=[ENU=Picture;
                                 PTB=Imagem];
                      RunObject=Page 5620;
                      RunPageLink=No.=FIELD(No.);
                      Image=Picture }
      { 57      ;2   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[ENU=C&opy Fixed Asset;
                                 PTB=C&opiar Ativo Fixo];
                      Promoted=Yes;
                      Image=CopyFixedAssets;
                      PromotedCategory=Process;
                      OnAction=VAR
                                 CopyFA@1000 : Report 5685;
                               BEGIN
                                 CopyFA.SetFANo("No.");
                                 CopyFA.RUNMODAL;
                               END;
                                }
      { 42      ;2   ;Action    ;
                      CaptionML=[ENU=FA Posting Types Overview;
                                 PTB=Vis�o Geral Tipos Registro];
                      RunObject=Page 5662;
                      Image=ShowMatrix }
      { 50      ;2   ;Action    ;
                      CaptionML=[ENU=Co&mments;
                                 PTB=Co&ment�rios];
                      RunObject=Page 124;
                      RunPageLink=Table Name=CONST(Fixed Asset),
                                  No.=FIELD(No.);
                      Image=ViewComments }
      { 3       ;1   ;ActionGroup;
                      CaptionML=[ENU=Main Asset;
                                 PTB=Ativo Principal] }
      { 29      ;2   ;Action    ;
                      CaptionML=[ENU=M&ain Asset Components;
                                 PTB=Componentes];
                      RunObject=Page 5658;
                      RunPageLink=Main Asset No.=FIELD(No.);
                      Promoted=Yes;
                      Image=Components;
                      PromotedCategory=Process }
      { 41      ;2   ;Action    ;
                      CaptionML=[ENU=Ma&in Asset Statistics;
                                 PTB=Estat�sticas];
                      RunObject=Page 5603;
                      RunPageLink=FA No.=FIELD(No.);
                      Image=StatisticsDocument }
      { 39      ;2   ;Separator ;
                      CaptionML=[ENU="";
                                 PTB=""] }
      { 5       ;1   ;ActionGroup;
                      CaptionML=[ENU=Insurance;
                                 PTB=Seguro];
                      Image=TotalValueInsured }
      { 68      ;2   ;Action    ;
                      CaptionML=[ENU=Total Value Ins&ured;
                                 PTB=Valor Total Seg&urado];
                      RunObject=Page 5649;
                      RunPageLink=No.=FIELD(No.);
                      Image=TotalValueInsured }
      { 11      ;1   ;ActionGroup;
                      CaptionML=[ENU=History;
                                 PTB=Hist�rico];
                      Image=History }
      { 7       ;2   ;Action    ;
                      ShortCutKey=Ctrl+F7;
                      CaptionML=[ENU=Ledger E&ntries;
                                 PTB=Movime&ntos];
                      RunObject=Page 5604;
                      RunPageView=SORTING(FA No.);
                      RunPageLink=FA No.=FIELD(No.);
                      Promoted=No;
                      Image=FixedAssetLedger;
                      PromotedCategory=Process }
      { 8       ;2   ;Action    ;
                      CaptionML=[ENU=Error Ledger Entries;
                                 PTB=Movimentos Erros];
                      RunObject=Page 5605;
                      RunPageView=SORTING(Canceled from FA No.);
                      RunPageLink=Canceled from FA No.=FIELD(No.);
                      Image=ErrorFALedgerEntries }
      { 9       ;2   ;Action    ;
                      CaptionML=[ENU=Main&tenance Ledger Entries;
                                 PTB=Movimentos Manuten��o];
                      RunObject=Page 5641;
                      RunPageView=SORTING(FA No.);
                      RunPageLink=FA No.=FIELD(No.);
                      Promoted=Yes;
                      Image=MaintenanceLedgerEntries;
                      PromotedCategory=Process }
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 1900145704;1 ;Action    ;
                      Name=CalculateDepreciation;
                      CaptionML=[ENU=Calculate Depreciation;
                                 PTB=Calcular Deprecia��o];
                      Promoted=Yes;
                      Image=CalculateDepreciation;
                      PromotedCategory=Process;
                      OnAction=VAR
                                 FixedAsset@1000 : Record 5600;
                               BEGIN
                                 FixedAsset.SETRANGE("No.","No.");
                                 REPORT.RUNMODAL(REPORT::"Calculate Depreciation",TRUE,FALSE,FixedAsset);
                               END;
                                }
      { 1900000006;0 ;ActionContainer;
                      ActionContainerType=Reports }
      { 1907091306;1 ;Action    ;
                      CaptionML=[ENU=Fixed Assets List;
                                 PTB=Lista Ativos Fixos];
                      RunObject=Report 5601;
                      Promoted=Yes;
                      Image=Report;
                      PromotedCategory=Report }
      { 1903109606;1 ;Action    ;
                      CaptionML=[ENU=Acquisition List;
                                 PTB=Lista Aquisi��es];
                      RunObject=Report 5608;
                      Promoted=No;
                      Image=Report;
                      PromotedCategory=Report }
      { 1901902606;1 ;Action    ;
                      CaptionML=[ENU=Details;
                                 PTB=Detalhes];
                      RunObject=Report 5604;
                      Promoted=Yes;
                      Image=View;
                      PromotedCategory=Report }
      { 1905598506;1 ;Action    ;
                      CaptionML=[ENU=Book Value 01;
                                 PTB=Valor L�quido 01];
                      RunObject=Report 5605;
                      Promoted=No;
                      Image=Report;
                      PromotedCategory=Report }
      { 1905598606;1 ;Action    ;
                      CaptionML=[ENU=Book Value 02;
                                 PTB=Valor L�quido 02];
                      RunObject=Report 5606;
                      Promoted=No;
                      Image=Report;
                      PromotedCategory=Report }
      { 1901105406;1 ;Action    ;
                      CaptionML=[ENU=Analysis;
                                 PTB=An�lise];
                      RunObject=Report 5600;
                      Promoted=Yes;
                      Image=Report;
                      PromotedCategory=Report }
      { 1902048606;1 ;Action    ;
                      CaptionML=[ENU=Projected Value;
                                 PTB=Valor Projetado];
                      RunObject=Report 5607;
                      Promoted=Yes;
                      Image=Report;
                      PromotedCategory=Report }
      { 1903345906;1 ;Action    ;
                      CaptionML=[ENU=G/L Analysis;
                                 PTB=An�lise Cont�bil];
                      RunObject=Report 5610;
                      Promoted=No;
                      Image=Report;
                      PromotedCategory=Report }
      { 1903807106;1 ;Action    ;
                      CaptionML=[ENU=Register;
                                 PTB=Registro];
                      RunObject=Report 5603;
                      Promoted=Yes;
                      Image=Confirm;
                      PromotedCategory=Report }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                CaptionML=[ENU=General;
                           PTB=Geral] }

    { 2   ;2   ;Field     ;
                SourceExpr="No.";
                Importance=Promoted;
                OnAssistEdit=BEGIN
                               IF AssistEdit(xRec) THEN
                                 CurrPage.UPDATE;
                             END;
                              }

    { 4   ;2   ;Field     ;
                SourceExpr=Description;
                Importance=Promoted }

    { 52006500;2;Field    ;
                SourceExpr="Description 2" }

    { 52006502;2;Field    ;
                SourceExpr="Plaque No." }

    { 26  ;2   ;Field     ;
                SourceExpr="Serial No.";
                Importance=Promoted }

    { 52006501;2;Field    ;
                SourceExpr="Function Description" }

    { 22  ;2   ;Field     ;
                SourceExpr="Main Asset/Component" }

    { 20  ;2   ;Field     ;
                SourceExpr="Component of Main Asset";
                Editable=FALSE }

    { 18  ;2   ;Field     ;
                SourceExpr="Search Description" }

    { 63  ;2   ;Field     ;
                SourceExpr="Responsible Employee";
                Importance=Promoted }

    { 65  ;2   ;Field     ;
                SourceExpr=Inactive }

    { 30  ;2   ;Field     ;
                SourceExpr=Blocked }

    { 32  ;2   ;Field     ;
                SourceExpr="Last Date Modified" }

    { 6   ;1   ;Part      ;
                Name=DepreciationBook;
                SubPageLink=FA No.=FIELD(No.);
                PagePartID=Page5666 }

    { 1904784501;1;Group  ;
                CaptionML=[ENU=Posting;
                           PTB=Registro];
                GroupType=Group }

    { 43  ;2   ;Field     ;
                SourceExpr="FA Class Code";
                Importance=Promoted }

    { 45  ;2   ;Field     ;
                SourceExpr="FA Subclass Code";
                Importance=Promoted }

    { 52  ;2   ;Field     ;
                SourceExpr="FA Location Code";
                Importance=Promoted }

    { 16  ;2   ;Field     ;
                SourceExpr="Budgeted Asset";
                Importance=Promoted }

    { 1903524101;1;Group  ;
                CaptionML=[ENU=Maintenance;
                           PTB=Manuten��o];
                GroupType=Group }

    { 10  ;2   ;Field     ;
                SourceExpr="Vendor No.";
                Importance=Promoted }

    { 12  ;2   ;Field     ;
                SourceExpr="Maintenance Vendor No.";
                Importance=Promoted }

    { 14  ;2   ;Field     ;
                SourceExpr="Under Maintenance" }

    { 24  ;2   ;Field     ;
                SourceExpr="Next Service Date";
                Importance=Promoted }

    { 37  ;2   ;Field     ;
                SourceExpr="Warranty Date" }

    { 28  ;2   ;Field     ;
                SourceExpr=Insured }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=TRUE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

