OBJECT Table 52112450 Modified Tax Amount Line
{
  OBJECT-PROPERTIES
  {
    Date=22/10/15;
    Time=08:37:38;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Modified Tax Line;
               PTB=Linha Imposto Modificada];
  }
  FIELDS
  {
    { 10  ;   ;Table ID            ;Integer       ;CaptionML=[ENU=Table ID;
                                                              PTB=ID Tabela] }
    { 20  ;   ;Document Type       ;Option        ;CaptionML=[ENU=Document Type;
                                                              PTB=Tipo Documento];
                                                   OptionCaptionML=[ENU=Quote,Order,Invoice,Credit Memo,Blanket Order,Return Order;
                                                                    PTB=Cota��o,Pedido,Nota Fiscal,Nota Cr�dito,Pedido Aberto,Devolu��o];
                                                   OptionString=Quote,Order,Invoice,Credit Memo,Blanket Order,Return Order }
    { 30  ;   ;Document No.        ;Code20        ;TableRelation=IF (Table ID=CONST(37)) "Sales Header".No. WHERE (Document Type=FIELD(Document Type))
                                                                 ELSE IF (Table ID=CONST(39)) "Purchase Header".No. WHERE (Document Type=FIELD(Document Type));
                                                   CaptionML=[ENU=Document No.;
                                                              PTB=N. Documento] }
    { 40  ;   ;Document Line No.   ;Integer       ;TableRelation=IF (Table ID=CONST(37)) "Sales Line"."Line No." WHERE (Document Type=FIELD(Document Type),
                                                                                                                        Document No.=FIELD(Document No.))
                                                                                                                        ELSE IF (Table ID=CONST(39)) "Purchase Line"."Line No." WHERE (Document Type=FIELD(Document Type),
                                                                                                                                                                                       Document No.=FIELD(Document No.));
                                                   CaptionML=[ENU=Document Line No.;
                                                              PTB=N. Linha Documento] }
    { 44  ;   ;Tax Identification  ;Option        ;CaptionML=[ENU=Identification;
                                                              PTB=Identifica��o];
                                                   OptionCaptionML=[ENU=" ,IPI,ICMS,PIS,COFINS,II,IRRF,INSS,ISS,CSL Ret.,PIS Ret.,COFINS Ret.,ISS Ret.,INSS Ret.,Dif. Aliq. ICMS,ST,PCC,CIDE,Funrural,Simples";
                                                                    PTB=" ,IPI,ICMS,PIS,COFINS,II,IRRF,INSS,ISS,CSL Ret.,PIS Ret.,COFINS Ret.,ISS Ret.,INSS Ret.,Dif. Aliq. ICMS,ST,PCC,CIDE,Funrural,Simples"];
                                                   OptionString=[ ,IPI,ICMS,PIS,COFINS,II,IRRF,INSS,ISS,CSL Ret.,PIS Ret.,COFINS Ret.,ISS Ret.,INSS Ret.,Dif. Aliq. ICMS,ST,PCC,CIDE,Funrural,Simples];
                                                   Editable=No }
    { 50  ;   ;Tax Area Code       ;Code20        ;TableRelation="Tax Area";
                                                   CaptionML=[ENU=Tax Area Code;
                                                              PTB=C�d. imposto �rea] }
    { 60  ;   ;Tax Jurisdiction Code;Code20       ;TableRelation="Tax Jurisdiction";
                                                   CaptionML=[ENU=Tax Jurisdiction Code;
                                                              PTB=C�d. imposto jurisd.] }
    { 70  ;   ;Tax Posting Code    ;Code20        ;TableRelation="Tax Posting";
                                                   CaptionML=[ENU=Tax Posting Code;
                                                              PTB=C�d. Contabiliza��o] }
    { 100 ;   ;Tax Base Amount     ;Decimal       ;CaptionML=[ENU=Base Amount;
                                                              PTB=Valor Base];
                                                   BlankNumbers=BlankZero;
                                                   AutoFormatType=1 }
    { 110 ;   ;Tax %               ;Decimal       ;CaptionML=[ENU=Tax %;
                                                              PTB=Al�quota;
                                                              ESM=% Impto.;
                                                              FRC=% taxe;
                                                              ENC=Tax %];
                                                   DecimalPlaces=0:5 }
    { 120 ;   ;Tax Amount          ;Decimal       ;CaptionML=[ENU=Amount;
                                                              PTB=Valor];
                                                   BlankNumbers=BlankZero }
    { 130 ;   ;Reduction Factor    ;Decimal       ;CaptionML=[ENU=Reduction Factor;
                                                              PTB=Fator de Redu��o de Base];
                                                   BlankNumbers=BlankZero;
                                                   Editable=No }
    { 140 ;   ;Exempt Basis Amount ;Decimal       ;CaptionML=[ENU=Exempt Basis Amount;
                                                              PTB=Valor Base Isenta];
                                                   BlankNumbers=BlankZero;
                                                   Editable=No }
    { 150 ;   ;Others Basis Amount ;Decimal       ;CaptionML=[ENU=Exempt Basis Amount;
                                                              PTB=Valor Base Outras];
                                                   BlankNumbers=BlankZero;
                                                   Editable=No }
    { 160 ;   ;Deferred %          ;Decimal       ;CaptionML=[ENU=Deferred %;
                                                              PTB=% Diferido];
                                                   BlankNumbers=BlankZero }
    { 170 ;   ;Deferred Amount     ;Decimal       ;CaptionML=[ENU=Deferred Amount;
                                                              PTB=Valor Diferido];
                                                   BlankNumbers=BlankZero }
    { 180 ;   ;Discounted %        ;Decimal       ;CaptionML=[ENU=Discounted %;
                                                              PTB=% Desonerado];
                                                   BlankNumbers=BlankZero }
    { 182 ;   ;Discounted Amount   ;Decimal       ;CaptionML=[ENU=Discounted Amount;
                                                              PTB=Valor Desonerado];
                                                   BlankNumbers=BlankZero }
    { 200 ;   ;User ID             ;Code50        ;TableRelation=User."User Name";
                                                   OnValidate=VAR
                                                                UserMgt@1000 : Codeunit 418;
                                                              BEGIN
                                                              END;

                                                   OnLookup=VAR
                                                              UserMgt@1000 : Codeunit 418;
                                                            BEGIN
                                                            END;

                                                   ValidateTableRelation=No;
                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=User ID;
                                                              PTB=ID Usu�rio];
                                                   NotBlank=Yes }
    { 460 ;   ;Tax Statute Code    ;Code3         ;TableRelation="Tax Statute Code".Code WHERE (Tax Identification=FIELD(Tax Identification));
                                                   CaptionML=[ENU=Tax Statute Code;
                                                              PTB=C�d. Enquadramento Legal] }
  }
  KEYS
  {
    {    ;Table ID,Document Type,Document No.,Document Line No.,Tax Area Code,Tax Jurisdiction Code;
                                                   Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

