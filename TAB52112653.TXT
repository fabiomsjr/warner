OBJECT Table 52112653 NFe XML Import Setup
{
  OBJECT-PROPERTIES
  {
    Date=11/11/14;
    Time=13:51:47;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=NF-e XML Importation Setup;
               PTB=Conf. Importa��o XML NF-e];
  }
  FIELDS
  {
    { 10  ;   ;Primary Key         ;Code1         ;CaptionML=[ENU=Primary Key;
                                                              PTB=Chave Prim�ria] }
    { 20  ;   ;XML Files Folder    ;Text250       ;OnValidate=BEGIN
                                                                IF "XML Files Folder" <> '' THEN
                                                                  ThrowErrorIfInvalidFolder("XML Files Folder");
                                                              END;

                                                   CaptionML=[ENU=XML Files Folder;
                                                              PTB=Pasta Arquivos XML] }
    { 22  ;   ;Imported XML Files Folder;Text250  ;OnValidate=BEGIN
                                                                IF "Imported XML Files Folder" <> '' THEN
                                                                  ThrowErrorIfInvalidFolder("Imported XML Files Folder");
                                                              END;

                                                   CaptionML=[ENU=Imported XML Files Folder;
                                                              PTB=Pasta Arquivos XML Importados] }
    { 30  ;   ;Fiscal Document Type;Code20        ;TableRelation="Fiscal Document Type";
                                                   CaptionML=[ENU=Fiscal Document Type;
                                                              PTB=Tipo Documento Fiscal] }
    { 40  ;   ;Item Tax Area Code  ;Code20        ;TableRelation="Tax Area";
                                                   CaptionML=[ENU=Item Tax Area Code;
                                                              PTB=C�d. �rea Impostos Produto] }
    { 44  ;   ;Item Charge Tax Area Code;Code20   ;TableRelation="Tax Area";
                                                   CaptionML=[ENU=Item Charge Tax Area Code (No Taxes);
                                                              PTB=C�d. �rea Impostos Encargo (Sem Impostos)] }
    { 50  ;   ;Freight Item Charge Code;Code20    ;TableRelation="Item Charge" WHERE (Item Charge Type=CONST(Freight));
                                                   CaptionML=[ENU=Freight Item Charge Code;
                                                              PTB=C�d. Encargo Produto Frete] }
    { 60  ;   ;Insurance Item Charge Code;Code20  ;TableRelation="Item Charge" WHERE (Item Charge Type=CONST(Insurance));
                                                   CaptionML=[ENU=Insurance Item Charge Code;
                                                              PTB=C�d. Encargo Produto Seguro] }
    { 70  ;   ;Other Exp. Item Charge Code;Code20 ;TableRelation="Item Charge" WHERE (Item Charge Type=CONST(Other Expenses));
                                                   CaptionML=[ENU=Other Exp. Item Charge Code;
                                                              PTB=C�d. Encargo Produto Outras Desp.] }
    { 80  ;   ;Location Code       ;Code10        ;TableRelation=Location;
                                                   CaptionML=[ENU=Location Code;
                                                              PTB=C�d. Dep�sito] }
    { 90  ;   ;Taxes Matrix Code   ;Code20        ;TableRelation="Taxes Matrix" WHERE (Type=CONST(Purchase));
                                                   CaptionML=[ENU=Taxes Matrix Code;
                                                              PTB=C�d. Matriz Impostos] }
    { 100 ;   ;Payment Terms Code  ;Code10        ;TableRelation="Payment Terms";
                                                   CaptionML=[ENU=Payment Terms Code;
                                                              PTB=C�d. Termos Pagamento] }
    { 110 ;   ;Open Order Reconciliation;Boolean  ;CaptionML=[ENU=Open Order Reconciliation;
                                                              PTB=Abrir Reconcilia��o Pedidos] }
  }
  KEYS
  {
    {    ;Primary Key                             ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      Text001@52006500 : TextConst 'ENU=Folder %1 does not exist.;PTB=A pasta %1 n�o existe.';

    PROCEDURE ThrowErrorIfInvalidFolder@52006502(folderName@52006500 : Text);
    VAR
      directory@52006501 : DotNet "'mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.IO.Directory";
    BEGIN
      IF NOT directory.Exists(folderName) THEN
        ERROR(Text001, folderName);
    END;

    BEGIN
    END.
  }
}

