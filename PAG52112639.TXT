OBJECT Page 52112639 Folder Selector
{
  OBJECT-PROPERTIES
  {
    Date=30/04/13;
    Time=11:13:09;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Select Folder;
               PTB=Selecionar Pasta];
    PageType=Card;
    OnOpenPage=BEGIN
                 CurrPage.EDITABLE := FALSE;
                 SelectDirectory();
                 CurrPage.CLOSE;
               END;

  }
  CONTROLS
  {
  }
  CODE
  {
    VAR
      GTPath@1102300000 : Text[250];
      GTTitle@1102300001 : Text[250];
      GBSuccess@1102300002 : Boolean;
      Text001@1102300003 : TextConst 'ENU=Select a folder;PTB=Selecione uma pasta';

    PROCEDURE SetTitle@1102300001(title@1102300000 : Text[250]);
    BEGIN
      GTTitle := title;
    END;

    PROCEDURE IsSuccessfull@1102300002() : Boolean;
    BEGIN
      EXIT(GBSuccess);
    END;

    PROCEDURE GetPath@1102300003() : Text[1024];
    BEGIN
      EXIT(GTPath);
    END;

    PROCEDURE SelectDirectory@1102300004();
    VAR
      lclsShellControl@1102300000 : Automation "{50A7E9B0-70EF-11D1-B75A-00A0C90564FE} 1.0:{13709620-C279-11CE-A49E-444553540000}:'Microsoft Shell Controls And Automation'.Shell";
      lclsFolder@1102300001 : Automation "{50A7E9B0-70EF-11D1-B75A-00A0C90564FE} 1.0:{A7AE5F64-C4D7-4D7F-9307-4D24EE54B841}:'Microsoft Shell Controls And Automation'.Folder3";
      lbSuccess@1102300002 : Boolean;
    BEGIN
      CREATE(lclsShellControl, FALSE, TRUE);

      IF (GTTitle = '') THEN
        GTTitle := Text001;

      lclsFolder := lclsShellControl.BrowseForFolder(0, GTTitle, 0);
      GBSuccess  := NOT ISCLEAR(lclsFolder);

      IF GBSuccess THEN
        BEGIN
          GTPath := lclsFolder.Items().Item.Path;
          IF (COPYSTR(GTPath, STRLEN(GTPath), 1) <> '\') THEN
            GTPath := GTPath + '\';
          CLEAR(lclsFolder);
        END;

      CLEAR(lclsShellControl);
    END;

    BEGIN
    {
      --- FNX002.00 ---
      edvandro,051108,Form for folder selection
    }
    END.
  }
}

