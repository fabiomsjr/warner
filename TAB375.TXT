OBJECT Table 375 Dimension Code Amount Buffer
{
  OBJECT-PROPERTIES
  {
    Date=05/11/08;
    Time=12:00:00;
    Version List=NAVW16.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Dimension Code Amount Buffer;
               PTB=Mem. Int. Valor Cod. Dimens�o];
  }
  FIELDS
  {
    { 1   ;   ;Line Code           ;Code20        ;CaptionML=[ENU=Line Code;
                                                              PTB=Cod. Linha] }
    { 2   ;   ;Column Code         ;Code20        ;CaptionML=[ENU=Column Code;
                                                              PTB=Cod. Coluna] }
    { 3   ;   ;Amount              ;Decimal       ;CaptionML=[ENU=Amount;
                                                              PTB=Valor];
                                                   AutoFormatType=1 }
  }
  KEYS
  {
    {    ;Line Code,Column Code                   ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

