OBJECT Table 7709 XMLQueue
{
  OBJECT-PROPERTIES
  {
    Date=29/02/12;
    Time=18:14:17;
    Version List=NAVW16.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=XMLQueue;
               PTB=XMLFila];
  }
  FIELDS
  {
    { 1   ;   ;Integer             ;Integer       ;CaptionML=[ENU=Integer;
                                                              PTB=Integer];
                                                   NotBlank=Yes }
    { 2   ;   ;Job                 ;BLOB          ;CaptionML=[ENU=Job;
                                                              PTB=PROJETO] }
  }
  KEYS
  {
    {    ;Integer                                 ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

