OBJECT Table 52112849 Agglutination Accounts
{
  OBJECT-PROPERTIES
  {
    Date=16/05/16;
    Time=14:56:31;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    OnDelete=VAR
               details@52006500 : Record 52112854;
             BEGIN
               details.SETRANGE("Agglutination Code", "Agglutination Code");
               details.DELETEALL;
             END;

    CaptionML=[ENU=Agglutination Accounts;
               PTB=Aglutina��o Contas];
    LookupPageID=Page52112862;
    DrillDownPageID=Page52112862;
  }
  FIELDS
  {
    { 1   ;   ;Agglutination Code  ;Text20        ;OnValidate=VAR
                                                                planoContas@52006500 : Record 52112830;
                                                              BEGIN
                                                                IF planoContas.GET("Agglutination Code") THEN BEGIN
                                                                  Description := planoContas.Name;
                                                                  Totaling := planoContas.Totaling;
                                                                END;
                                                              END;

                                                   CaptionML=[ENU=Agglutination Code;
                                                              PTB=C�d. Aglutina��o] }
    { 2   ;   ;Description         ;Text80        ;CaptionML=[ENU=Description;
                                                              PTB=Descri��o] }
    { 3   ;   ;Level               ;Integer       ;CaptionML=[ENU=Level;
                                                              PTB=N�vel] }
    { 4   ;   ;Type                ;Option        ;CaptionML=[ENU=Type;
                                                              PTB=Tipo];
                                                   OptionCaptionML=[ENU=" ,Active,Passive,Result,Positive Total,Negative Total";
                                                                    PTB=" ,Ativo,Passivo,Resultado,Total Positivo,Total Negativo"];
                                                   OptionString=[ ,Ativo,Passivo,Resultado,TotalPositivo,TotalNegativo] }
    { 5   ;   ;Totaling            ;Text250       ;ValidateTableRelation=No;
                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=Totaling;
                                                              PTB=Intervalo Contas] }
    { 20  ;   ;DLPA/DMPL           ;Option        ;CaptionML=[ENU=DLPA/DMPL;
                                                              PTB=DLPA/DMPL];
                                                   OptionCaptionML=[ENU=" ,DLPA,DMPL";
                                                                    PTB=" ,DLPA,DMPL"];
                                                   OptionString=[ ,DLPA,DMPL] }
  }
  KEYS
  {
    {    ;Agglutination Code                      ;Clustered=Yes }
    {    ;Level                                    }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

