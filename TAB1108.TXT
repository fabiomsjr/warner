OBJECT Table 1108 Cost Accounting Setup
{
  OBJECT-PROPERTIES
  {
    Date=07/09/12;
    Time=12:00:00;
    Version List=NAVW17.00;
  }
  PROPERTIES
  {
    CaptionML=ENU=Cost Accounting Setup;
  }
  FIELDS
  {
    { 1   ;   ;Primary Key         ;Code10        ;CaptionML=ENU=Primary Key }
    { 2   ;   ;Starting Date for G/L Transfer;Date;OnValidate=BEGIN
                                                                CostRegister.SETRANGE(Source,CostRegister.Source::"Transfer from G/L");
                                                                IF CostRegister.FINDFIRST THEN
                                                                  ERROR(Text001,CostRegister."No.");
                                                              END;

                                                   CaptionML=[ENU=Starting Date for G/L Transfer;
                                                              PTB=Data Inicial para Transf. Contab.] }
    { 3   ;   ;Align G/L Account   ;Option        ;CaptionML=[ENU=Align G/L Account;
                                                              PTB=Alinhar Conta Contab.];
                                                   OptionCaptionML=ENU=No Alignment,Automatic,Prompt;
                                                   OptionString=No Alignment,Automatic,Prompt }
    { 4   ;   ;Align Cost Center Dimension;Option ;CaptionML=[ENU=Align Cost Center Dimension;
                                                              PTB=Alinhar Dimens�o Centro Custo];
                                                   OptionCaptionML=ENU=No Alignment,Automatic,Prompt;
                                                   OptionString=No Alignment,Automatic,Prompt }
    { 5   ;   ;Align Cost Object Dimension;Option ;CaptionML=[ENU=Align Cost Object Dimension;
                                                              PTB=Alinhar Dimens�o Objeto Custo];
                                                   OptionCaptionML=ENU=No Alignment,Automatic,Prompt;
                                                   OptionString=No Alignment,Automatic,Prompt }
    { 20  ;   ;Last Allocation ID  ;Code10        ;InitValue=001;
                                                   CaptionML=[ENU=Last Allocation ID;
                                                              PTB="�ltima Aloca��o "] }
    { 30  ;   ;Last Allocation Doc. No.;Code20    ;InitValue=AL0001;
                                                   CaptionML=[ENU=Last Allocation Doc. No.;
                                                              PTB=�ltimo N� Doc. Aloca��o] }
    { 100 ;   ;Auto Transfer from G/L;Boolean     ;OnValidate=VAR
                                                                TransferGlEntriesToCA@1000 : Codeunit 1105;
                                                              BEGIN
                                                                IF "Auto Transfer from G/L" THEN BEGIN
                                                                  MODIFY;
                                                                  TransferGlEntriesToCA.GetGLEntries;
                                                                END;
                                                              END;

                                                   CaptionML=[ENU=Auto Transfer from G/L;
                                                              PTB=Transf. Autom�tica do Contab.] }
    { 120 ;   ;Check G/L Postings  ;Boolean       ;CaptionML=[ENU=Check G/L Postings;
                                                              PTB=Verificar Registros Contab.] }
    { 205 ;   ;Cost Center Dimension;Code20       ;TableRelation=Dimension;
                                                   CaptionML=[ENU=Cost Center Dimension;
                                                              PTB=Dimens�o Centro Custo] }
    { 206 ;   ;Cost Object Dimension;Code20       ;TableRelation=Dimension;
                                                   CaptionML=[ENU=Cost Object Dimension;
                                                              PTB=Dimens�o Objeto Custo] }
  }
  KEYS
  {
    {    ;Primary Key                             ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      CostRegister@1000 : Record 1105;
      Text001@1001 : TextConst 'ENU=The starting date can no longer be defined. According to Register No. %1, general ledger entries have already been transferred to Cost Accounting.';

    BEGIN
    END.
  }
}

