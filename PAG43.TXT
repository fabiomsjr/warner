OBJECT Page 43 Sales Invoice
{
  OBJECT-PROPERTIES
  {
    Date=04/05/16;
    Time=15:32:52;
    Modified=Yes;
    Version List=NAVW18.00,NAVBR7,FNX068.00,WCMW1601_02_1;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Sales Invoice;
               PTB=Nota Fiscal Venda];
    SourceTable=Table36;
    SourceTableView=WHERE(Document Type=FILTER(Invoice));
    PageType=Document;
    RefreshOnActivate=Yes;
    OnInit=BEGIN
             SetExtDocNoMandatoryCondition;
           END;

    OnOpenPage=BEGIN
                 IF UserMgt.GetSalesFilter <> '' THEN BEGIN
                   FILTERGROUP(2);
                   SETRANGE("Responsibility Center",UserMgt.GetSalesFilter);
                   FILTERGROUP(0);
                 END;

                 SetDocNoVisible;
               END;

    OnAfterGetRecord=BEGIN
                       JobQueueVisible := "Job Queue Status" = "Job Queue Status"::"Scheduled for Posting";
                       SetExtDocNoMandatoryCondition;
                     END;

    OnNewRecord=BEGIN
                  "Responsibility Center" := UserMgt.GetSalesFilter;
                END;

    OnDeleteRecord=BEGIN
                     CurrPage.SAVERECORD;
                     EXIT(ConfirmDeletion);
                   END;

    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 57      ;1   ;ActionGroup;
                      CaptionML=[ENU=&Invoice;
                                 PTB=&Nota Fiscal];
                      Image=Invoice }
      { 59      ;2   ;Action    ;
                      Name=Statistics;
                      ShortCutKey=F7;
                      CaptionML=[ENU=Statistics;
                                 PTB=Estat�sticas];
                      Promoted=Yes;
                      Image=Statistics;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 CalcInvDiscForHeader;
                                 COMMIT;
                                 // > NAVBR
                                 //FORM.RUNMODAL(FORM::"Sales Statistics",Rec);
                                 PAGE.RUNMODAL(PAGE::"Sales Statistics",Rec);
                                 // < NAVBR
                                 SalesCalcDiscountByType.ResetRecalculateInvoiceDisc(Rec);
                               END;
                                }
      { 116     ;2   ;Action    ;
                      AccessByPermission=TableData 348=R;
                      ShortCutKey=Shift+Ctrl+D;
                      CaptionML=[ENU=Dimensions;
                                 PTB=Dimens�es];
                      Image=Dimensions;
                      OnAction=BEGIN
                                 ShowDocDim;
                                 CurrPage.SAVERECORD;
                               END;
                                }
      { 60      ;2   ;Action    ;
                      ShortCutKey=Shift+F7;
                      CaptionML=[ENU=Customer;
                                 PTB=Cart�o];
                      RunObject=Page 21;
                      RunPageLink=No.=FIELD(Sell-to Customer No.);
                      Image=Customer }
      { 162     ;2   ;Action    ;
                      CaptionML=[ENU=Approvals;
                                 PTB=Aprova��es];
                      Image=Approvals;
                      OnAction=VAR
                                 ApprovalEntries@1001 : Page 658;
                               BEGIN
                                 ApprovalEntries.Setfilters(DATABASE::"Sales Header","Document Type","No.");
                                 ApprovalEntries.RUN;
                               END;
                                }
      { 61      ;2   ;Action    ;
                      CaptionML=[ENU=Co&mments;
                                 PTB=Co&ment�rios];
                      RunObject=Page 67;
                      RunPageLink=Document Type=FIELD(Document Type),
                                  No.=FIELD(No.),
                                  Document Line No.=CONST(0);
                      Image=ViewComments }
      { 1102300037;2 ;Action    ;
                      CaptionML=[ENU=Additional Text;
                                 PTB=Textos Adicionais];
                      RunObject=Page 52112436;
                      RunPageView=SORTING(Type,Document Type,Document No.,Order No.);
                      RunPageLink=Type=CONST(Sale),
                                  Document Type=CONST(Invoice),
                                  Document No.=FIELD(No.);
                      Image=Text }
      { 35000012;2   ;Action    ;
                      CaptionML=[ENU=Document Charges;
                                 PTB=Encargos Documento];
                      RunObject=Page 52112437;
                      RunPageView=SORTING(Type,Document Type,Document No.,Item Charge Code)
                                  WHERE(Type=CONST(Sale),
                                        Document Type=CONST(Invoice));
                      RunPageLink=Document No.=FIELD(No.);
                      Image=ItemCosts }
      { 52006506;2   ;Action    ;
                      Name=OpenFiles;
                      CaptionML=[ENU=Files;
                                 PTB=Arquivos];
                      Image=ExternalDocument;
                      OnAction=VAR
                                 docFileMgt@52006500 : Codeunit 52112428;
                               BEGIN
                                 docFileMgt.OpenFiles(Rec);
                               END;
                                }
      { 171     ;2   ;Separator  }
      { 52006513;1   ;ActionGroup;
                      Name=AdditionalData;
                      CaptionML=[ENU=Additional Data;
                                 PTB=Dados Adicionais];
                      Image=ExtendedDataEntry }
      { 52006512;2   ;Action    ;
                      Name=ActionIndirectExportation;
                      CaptionML=[ENU=Indirect Exportation;
                                 PTB=Exporta��o Indireta];
                      RunObject=Page 52112514;
                      RunPageLink=Table ID=CONST(37),
                                  Document Type Filter=FIELD(Document Type),
                                  Document No. Filter=FIELD(No.);
                      Image=ExtendedDataEntry }
      { 7       ;1   ;ActionGroup;
                      CaptionML=ENU=Credit Card;
                      Image=CreditCardLog }
      { 172     ;2   ;Action    ;
                      CaptionML=[ENU=Credit Cards Transaction Lo&g Entries;
                                 PTB=Credit Cart�os Transaction Lo&g Entries];
                      RunObject=Page 829;
                      RunPageLink=Document Type=FIELD(Document Type),
                                  Document No.=FIELD(No.),
                                  Customer No.=FIELD(Bill-to Customer No.);
                      Image=CreditCardLog }
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 9       ;1   ;ActionGroup;
                      CaptionML=[ENU=Release;
                                 PTB=Liberar];
                      Image=ReleaseDoc }
      { 123     ;2   ;Action    ;
                      Name=Release;
                      ShortCutKey=Ctrl+F9;
                      CaptionML=[ENU=Re&lease;
                                 PTB=&Liberar];
                      Promoted=Yes;
                      Image=ReleaseDoc;
                      PromotedCategory=Process;
                      OnAction=VAR
                                 ReleaseSalesDoc@1000 : Codeunit 414;
                               BEGIN
                                 ReleaseSalesDoc.PerformManualRelease(Rec);
                               END;
                                }
      { 124     ;2   ;Action    ;
                      CaptionML=[ENU=Re&open;
                                 PTB=Re&abrir];
                      Image=ReOpen;
                      OnAction=VAR
                                 ReleaseSalesDoc@1001 : Codeunit 414;
                               BEGIN
                                 ReleaseSalesDoc.PerformManualReopen(Rec);
                               END;
                                }
      { 168     ;2   ;Separator  }
      { 62      ;1   ;ActionGroup;
                      CaptionML=[ENU=F&unctions;
                                 PTB=F&un��es];
                      Image=Action }
      { 35000011;2   ;Action    ;
                      CaptionML=[ENU=Assign Document Charges;
                                 PTB=Atribuir Encargos Documento];
                      RunObject=Codeunit 52112445;
                      Image=ItemCosts }
      { 63      ;2   ;Action    ;
                      Name=CalculateInvoiceDiscount;
                      AccessByPermission=TableData 19=R;
                      CaptionML=[ENU=Calculate &Invoice Discount;
                                 PTB=Calcular Desconto &Nota Fiscal];
                      Image=CalculateInvoiceDiscount;
                      OnAction=BEGIN
                                 ApproveCalcInvDisc;
                                 SalesCalcDiscountByType.ResetRecalculateInvoiceDisc(Rec);
                               END;
                                }
      { 142     ;2   ;Separator  }
      { 134     ;2   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[ENU=Get St&d. Cust. Sales Codes;
                                 PTB=Get St&d. Cust. Sales Codes];
                      Image=CustomerCode;
                      OnAction=VAR
                                 StdCustSalesCode@1000 : Record 172;
                               BEGIN
                                 StdCustSalesCode.InsertSalesLines(Rec);
                               END;
                                }
      { 139     ;2   ;Separator  }
      { 64      ;2   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[ENU=Copy Document;
                                 PTB=Copiar Documento];
                      Promoted=Yes;
                      Image=CopyDocument;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 CopySalesDoc.SetSalesHeader(Rec);
                                 CopySalesDoc.RUNMODAL;
                                 CLEAR(CopySalesDoc);
                               END;
                                }
      { 115     ;2   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[ENU=Move Negative Lines;
                                 PTB=Mover Linhas Negativas];
                      Image=MoveNegativeLines;
                      OnAction=BEGIN
                                 CLEAR(MoveNegSalesLines);
                                 MoveNegSalesLines.SetSalesHeader(Rec);
                                 MoveNegSalesLines.RUNMODAL;
                                 MoveNegSalesLines.ShowDocument;
                               END;
                                }
      { 141     ;2   ;Separator  }
      { 159     ;2   ;Action    ;
                      CaptionML=[ENU=Send A&pproval Request;
                                 PTB=Enviar Requisi��o A&prova��o];
                      Image=SendApprovalRequest;
                      OnAction=VAR
                                 ApprovalMgt@1001 : Codeunit 439;
                               BEGIN
                                 IF ApprovalMgt.SendSalesApprovalRequest(Rec) THEN;
                               END;
                                }
      { 160     ;2   ;Action    ;
                      CaptionML=[ENU=Cancel Approval Re&quest;
                                 PTB=Cancelar Requisi��o Aprova��o];
                      Image=Cancel;
                      OnAction=VAR
                                 ApprovalMgt@1001 : Codeunit 439;
                               BEGIN
                                 IF ApprovalMgt.CancelSalesApprovalRequest(Rec,TRUE,TRUE) THEN;
                               END;
                                }
      { 161     ;2   ;Separator  }
      { 52006504;1   ;ActionGroup;
                      Name=NFe;
                      CaptionML=[ENU=NF-e;
                                 PTB=NF-e] }
      { 52006503;2   ;Action    ;
                      Name=NFeValidate;
                      CaptionML=[ENU=Validate;
                                 PTB=Validar];
                      Image=CheckList;
                      OnAction=VAR
                                 nfeDocFunctions@52006500 : Codeunit 52112624;
                               BEGIN
                                 nfeDocFunctions.OpenSalesValidate(Rec);
                               END;
                                }
      { 52006502;2   ;Action    ;
                      Name=NFeOpenTestDANFe;
                      CaptionML=[ENU=View DANFe (Test);
                                 PTB=Visualizar DANFe (Teste)];
                      Image=Invoice;
                      OnAction=VAR
                                 nfeDocFunctions@52006500 : Codeunit 52112624;
                               BEGIN
                                 nfeDocFunctions.OpenSalesPreviewDANFe(Rec);
                               END;
                                }
      { 52006501;2   ;Action    ;
                      Name=NFeOpenTestXML;
                      CaptionML=[ENU=View XML (Test);
                                 PTB=Visualizar XML (Teste)];
                      Image=XMLFile;
                      OnAction=VAR
                                 nfeDocFunctions@52006500 : Codeunit 52112624;
                               BEGIN
                                 nfeDocFunctions.OpenSalesPreviewXML(Rec);
                               END;
                                }
      { 11      ;1   ;ActionGroup;
                      CaptionML=ENU=Credit Card;
                      Image=AuthorizeCreditCard }
      { 169     ;2   ;Action    ;
                      CaptionML=[ENU=Authorize;
                                 PTB=Autorizar];
                      Image=AuthorizeCreditCard;
                      OnAction=BEGIN
                                 Authorize;
                               END;
                                }
      { 170     ;2   ;Action    ;
                      CaptionML=[ENU=Void A&uthorize;
                                 PTB=Cancelar A&utoriza��o];
                      Image=VoidCreditCard;
                      OnAction=BEGIN
                                 Void;
                               END;
                                }
      { 69      ;1   ;ActionGroup;
                      CaptionML=[ENU=P&osting;
                                 PTB=Re&gistro];
                      Image=Post }
      { 1102300035;2 ;Action    ;
                      ShortCutKey=Ctrl+R;
                      CaptionML=[ENU=Posting Preview;
                                 PTB=Verificar Contabiliza��o];
                      RunObject=Codeunit 52112434;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=TestReport;
                      PromotedCategory=Report }
      { 71      ;2   ;Action    ;
                      Name=Post;
                      ShortCutKey=F9;
                      CaptionML=[ENU=P&ost;
                                 PTB=Registrar];
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=PostOrder;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 Post(CODEUNIT::"Sales-Post (Yes/No)");
                               END;
                                }
      { 70      ;2   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[ENU=Test Report;
                                 PTB=Relat�rio Verifica��o];
                      Image=TestReport;
                      OnAction=BEGIN
                                 ReportPrint.PrintSalesHeader(Rec);
                               END;
                                }
      { 72      ;2   ;Action    ;
                      ShortCutKey=Shift+F9;
                      CaptionML=[ENU=Post and &Print;
                                 PTB=Registrar e Im&primir];
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=PostPrint;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 Post(CODEUNIT::"Sales-Post + Print");
                               END;
                                }
      { 17      ;2   ;Action    ;
                      CaptionML=ENU=Post and Email;
                      Image=PostMail;
                      OnAction=VAR
                                 SalesPostPrint@1000 : Codeunit 82;
                               BEGIN
                                 SalesPostPrint.PostAndEmail(Rec);
                               END;
                                }
      { 73      ;2   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[ENU=Post &Batch;
                                 PTB=Registrar por Lote];
                      Image=PostBatch;
                      OnAction=BEGIN
                                 REPORT.RUNMODAL(REPORT::"Batch Post Sales Invoices",TRUE,TRUE,Rec);
                                 CurrPage.UPDATE(FALSE);
                               END;
                                }
      { 3       ;2   ;Action    ;
                      CaptionML=[ENU=Remove From Job Queue;
                                 PTB=Remover do Job Queue];
                      Visible=JobQueueVisible;
                      Image=RemoveLine;
                      OnAction=BEGIN
                                 CancelBackgroundPosting;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                CaptionML=[ENU=General;
                           PTB=Geral] }

    { 2   ;2   ;Field     ;
                SourceExpr="No.";
                Importance=Promoted;
                Visible=DocNoVisible;
                OnAssistEdit=BEGIN
                               IF AssistEdit(xRec) THEN
                                 CurrPage.UPDATE;
                             END;
                              }

    { 4   ;2   ;Field     ;
                SourceExpr="Sell-to Customer No.";
                Importance=Promoted;
                OnValidate=BEGIN
                             SelltoCustomerNoOnAfterValidat;
                           END;

                ShowMandatory=TRUE }

    { 128 ;2   ;Field     ;
                SourceExpr="Sell-to Contact No.";
                OnValidate=BEGIN
                             IF GETFILTER("Sell-to Contact No.") = xRec."Sell-to Contact No." THEN
                               IF "Sell-to Contact No." <> xRec."Sell-to Contact No." THEN
                                 SETRANGE("Sell-to Contact No.");
                           END;
                            }

    { 6   ;2   ;Field     ;
                SourceExpr="Sell-to Customer Name" }

    { 75  ;2   ;Field     ;
                SourceExpr="Sell-to Address";
                Importance=Additional }

    { 35000002;2;Field    ;
                SourceExpr="Sell-to Number";
                Importance=Additional }

    { 77  ;2   ;Field     ;
                SourceExpr="Sell-to Address 2";
                Importance=Additional }

    { 1102300030;2;Field  ;
                SourceExpr="Sell-to District";
                Importance=Additional }

    { 80  ;2   ;Field     ;
                SourceExpr="Sell-to Post Code";
                Importance=Additional }

    { 79  ;2   ;Field     ;
                SourceExpr="Sell-to City" }

    { 1102300029;2;Field  ;
                SourceExpr="Sell-to Territory Code" }

    { 8   ;2   ;Field     ;
                SourceExpr="Sell-to Contact" }

    { 12  ;2   ;Field     ;
                SourceExpr="Posting Date";
                Importance=Promoted }

    { 43  ;2   ;Field     ;
                SourceExpr="Document Date" }

    { 13  ;2   ;Field     ;
                SourceExpr="Incoming Document Entry No.";
                Visible=FALSE }

    { 126 ;2   ;Field     ;
                SourceExpr="External Document No.";
                Importance=Promoted;
                ShowMandatory=ExternalDocNoMandatory }

    { 10  ;2   ;Field     ;
                SourceExpr="Salesperson Code";
                OnValidate=BEGIN
                             SalespersonCodeOnAfterValidate;
                           END;
                            }

    { 129 ;2   ;Field     ;
                SourceExpr="Campaign No.";
                Importance=Additional }

    { 118 ;2   ;Field     ;
                SourceExpr="Responsibility Center";
                Importance=Additional }

    { 67  ;2   ;Field     ;
                SourceExpr="Assigned User ID";
                Importance=Additional }

    { 5   ;2   ;Field     ;
                SourceExpr="Job Queue Status";
                Importance=Additional }

    { 112 ;2   ;Field     ;
                SourceExpr=Status;
                Importance=Promoted }

    { 1102300000;1;Group  ;
                CaptionML=[ENU=National;
                           PTB=Nacional];
                GroupType=Group }

    { 1102300005;2;Field  ;
                SourceExpr="Branch Code";
                Importance=Promoted }

    { 52006507;2;Field    ;
                SourceExpr="Taxes Matrix Code" }

    { 1102300007;2;Field  ;
                SourceExpr="Tax Area Code";
                Importance=Promoted }

    { 35000017;2;Field    ;
                SourceExpr="Auto NFe";
                Importance=Additional }

    { 1102300008;2;Field  ;
                SourceExpr="Fiscal Document Type";
                Importance=Promoted }

    { 1102300006;2;Field  ;
                SourceExpr="Print Serie" }

    { 1102300009;2;Field  ;
                SourceExpr="Print Sub Serie";
                Importance=Additional }

    { 1102300010;2;Field  ;
                SourceExpr="CFOP Code" }

    { 1102300011;2;Field  ;
                SourceExpr="Operation Nature" }

    { 1102300012;2;Field  ;
                SourceExpr="Complementary Invoice Type";
                Importance=Additional }

    { 35000000;2;Field    ;
                SourceExpr="Invoice to Complement";
                Importance=Additional }

    { 1102300013;2;Field  ;
                SourceExpr="Operation Type" }

    { 1102300014;2;Field  ;
                SourceExpr="Fiscal Book Remarks";
                Importance=Additional }

    { 1102300038;2;Field  ;
                SourceExpr="NFe Chave Acesso" }

    { 1102300015;2;Field  ;
                SourceExpr="End User" }

    { 35000009;2;Field    ;
                SourceExpr="Posting Description";
                Importance=Additional }

    { 52006508;2;Field    ;
                SourceExpr="Additional Description";
                Importance=Additional }

    { 52006509;2;Field    ;
                SourceExpr="Buyer Presence" }

    { 56  ;1   ;Part      ;
                Name=SalesLines;
                SubPageLink=Document No.=FIELD(No.);
                PagePartID=Page47 }

    { 1905885101;1;Group  ;
                CaptionML=[ENU=Invoicing;
                           PTB=Faturamento] }

    { 14  ;2   ;Field     ;
                SourceExpr="Bill-to Customer No.";
                Importance=Promoted;
                OnValidate=BEGIN
                             BilltoCustomerNoOnAfterValidat;
                           END;
                            }

    { 132 ;2   ;Field     ;
                SourceExpr="Bill-to Contact No." }

    { 16  ;2   ;Field     ;
                SourceExpr="Bill-to Name" }

    { 18  ;2   ;Field     ;
                SourceExpr="Bill-to Address";
                Importance=Additional }

    { 35000003;2;Field    ;
                SourceExpr="Bill-to Number";
                Importance=Additional }

    { 20  ;2   ;Field     ;
                SourceExpr="Bill-to Address 2";
                Importance=Additional }

    { 35000004;2;Field    ;
                SourceExpr="Bill-to District";
                Importance=Additional }

    { 85  ;2   ;Field     ;
                SourceExpr="Bill-to Post Code";
                Importance=Additional }

    { 22  ;2   ;Field     ;
                SourceExpr="Bill-to City" }

    { 35000005;2;Field    ;
                SourceExpr="Bill-to Territory Code" }

    { 24  ;2   ;Field     ;
                SourceExpr="Bill-to Contact";
                Importance=Additional }

    { 84  ;2   ;Field     ;
                SourceExpr="Shortcut Dimension 1 Code";
                OnValidate=BEGIN
                             ShortcutDimension1CodeOnAfterV;
                           END;
                            }

    { 88  ;2   ;Field     ;
                SourceExpr="Shortcut Dimension 2 Code";
                OnValidate=BEGIN
                             ShortcutDimension2CodeOnAfterV;
                           END;
                            }

    { 26  ;2   ;Field     ;
                SourceExpr="Payment Terms Code";
                Importance=Promoted }

    { 28  ;2   ;Field     ;
                SourceExpr="Due Date";
                Importance=Promoted }

    { 30  ;2   ;Field     ;
                SourceExpr="Payment Discount %" }

    { 32  ;2   ;Field     ;
                SourceExpr="Pmt. Discount Date";
                Importance=Additional }

    { 101 ;2   ;Field     ;
                SourceExpr="Payment Method Code" }

    { 15  ;2   ;Field     ;
                SourceExpr="Direct Debit Mandate ID" }

    { 121 ;2   ;Field     ;
                SourceExpr="Prices Including VAT";
                OnValidate=BEGIN
                             PricesIncludingVATOnAfterValid;
                           END;
                            }

    { 156 ;2   ;Field     ;
                SourceExpr="VAT Bus. Posting Group";
                Visible=FALSE }

    { 166 ;2   ;Field     ;
                SourceExpr="Credit Card No." }

    { 164 ;2   ;Field     ;
                CaptionML=[ENU=Cr. Card Number (Last 4 Digits);
                           PTB=Cr. Cart�o Number (Last 4 Digits)];
                SourceExpr=GetCreditcardNumber }

    { 1906801201;1;Group  ;
                CaptionML=[ENU=Shipping;
                           PTB=Envio] }

    { 34  ;2   ;Field     ;
                SourceExpr="Ship-to Code";
                Importance=Promoted }

    { 36  ;2   ;Field     ;
                SourceExpr="Ship-to Name" }

    { 38  ;2   ;Field     ;
                SourceExpr="Ship-to Address" }

    { 35000006;2;Field    ;
                SourceExpr="Ship-to Number" }

    { 40  ;2   ;Field     ;
                SourceExpr="Ship-to Address 2" }

    { 35000007;2;Field    ;
                SourceExpr="Ship-to District" }

    { 93  ;2   ;Field     ;
                SourceExpr="Ship-to Post Code";
                Importance=Promoted }

    { 42  ;2   ;Field     ;
                SourceExpr="Ship-to City" }

    { 35000008;2;Field    ;
                SourceExpr="Ship-to Territory Code" }

    { 44  ;2   ;Field     ;
                SourceExpr="Ship-to Contact";
                Importance=Additional }

    { 90  ;2   ;Field     ;
                SourceExpr="Location Code" }

    { 46  ;2   ;Field     ;
                SourceExpr="Shipment Method Code" }

    { 103 ;2   ;Field     ;
                SourceExpr="Shipping Agent Code" }

    { 105 ;2   ;Field     ;
                SourceExpr="Package Tracking No.";
                Importance=Additional }

    { 48  ;2   ;Field     ;
                SourceExpr="Shipment Date";
                Importance=Promoted }

    { 1102300016;2;Field  ;
                SourceExpr="Freight Billed To" }

    { 35000016;2;Field    ;
                SourceExpr="Transported Quantity" }

    { 35000015;2;Field    ;
                SourceExpr=Species }

    { 35000014;2;Field    ;
                SourceExpr=Marks }

    { 35000013;2;Field    ;
                SourceExpr="Transport Number" }

    { 1102300021;2;Field  ;
                SourceExpr="Gross Weight" }

    { 1102300022;2;Field  ;
                SourceExpr="Net Weight" }

    { 1102300027;2;Field  ;
                SourceExpr="License Plate" }

    { 1102300028;2;Field  ;
                SourceExpr="UF Vehicle" }

    { 1907468901;1;Group  ;
                CaptionML=[ENU=Foreign Trade;
                           PTB=Internacional] }

    { 107 ;2   ;Field     ;
                SourceExpr="Currency Code";
                Importance=Promoted;
                OnValidate=BEGIN
                             CurrPage.UPDATE;
                             SalesCalcDiscountByType.ApplyDefaultInvoiceDiscount(0,Rec);
                           END;

                OnAssistEdit=BEGIN
                               CLEAR(ChangeExchangeRate);
                               IF "Posting Date" <> 0D THEN
                                 ChangeExchangeRate.SetParameter("Currency Code","Currency Factor","Posting Date")
                               ELSE
                                 ChangeExchangeRate.SetParameter("Currency Code","Currency Factor",WORKDATE);
                               IF ChangeExchangeRate.RUNMODAL = ACTION::OK THEN BEGIN
                                 VALIDATE("Currency Factor",ChangeExchangeRate.GetParameter);
                                 CurrPage.UPDATE;
                               END;
                               CLEAR(ChangeExchangeRate);
                             END;
                              }

    { 50  ;2   ;Field     ;
                SourceExpr="EU 3-Party Trade" }

    { 52  ;2   ;Field     ;
                SourceExpr="Transaction Type" }

    { 99  ;2   ;Field     ;
                SourceExpr="Transaction Specification" }

    { 54  ;2   ;Field     ;
                SourceExpr="Transport Method" }

    { 95  ;2   ;Field     ;
                SourceExpr="Exit Point" }

    { 97  ;2   ;Field     ;
                SourceExpr=Area }

    { 35000010;1;Group    ;
                CaptionML=[ENU=Exportation;
                           PTB=Exporta��o];
                GroupType=Group }

    { 1102300023;2;Field  ;
                SourceExpr="UF Boarding" }

    { 1102300024;2;Field  ;
                SourceExpr="Boarding Locate" }

    { 52006505;2;Part     ;
                SubPageLink=Invoice Type=CONST(Invoice),
                            Document No.=FIELD(No.);
                PagePartID=Page52112471;
                PartType=Page }

    { 1102300001;1;Group  ;
                CaptionML=PTB=NFS-e;
                GroupType=Group }

    { 1102300002;2;Field  ;
                SourceExpr="Service E-Invoice";
                Editable=FALSE }

    { 52006511;2;Field    ;
                SourceExpr="Service Delivery Date" }

    { 35000001;2;Field    ;
                SourceExpr="Service Delivery City" }

    { 52006500;2;Field    ;
                SourceExpr="NFS-e Tributation Code" }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 52006510;1;Part     ;
                SubPageLink=Type=CONST(Sale),
                            Document Type=CONST(Invoice),
                            Document No.=FIELD(No.);
                PagePartID=Page52112498;
                PartType=Page }

    { 1903720907;1;Part   ;
                SubPageLink=No.=FIELD(Sell-to Customer No.);
                PagePartID=Page9080;
                Visible=FALSE;
                PartType=Page }

    { 1907234507;1;Part   ;
                SubPageLink=No.=FIELD(Bill-to Customer No.);
                PagePartID=Page9081;
                Visible=FALSE;
                PartType=Page }

    { 1902018507;1;Part   ;
                SubPageLink=No.=FIELD(Bill-to Customer No.);
                PagePartID=Page9082;
                Visible=TRUE;
                PartType=Page }

    { 1900316107;1;Part   ;
                SubPageLink=No.=FIELD(Sell-to Customer No.);
                PagePartID=Page9084;
                Visible=TRUE;
                PartType=Page }

    { 1906127307;1;Part   ;
                SubPageLink=Document Type=FIELD(Document Type),
                            Document No.=FIELD(Document No.),
                            Line No.=FIELD(Line No.);
                PagePartID=Page9087;
                ProviderID=56;
                Visible=FALSE;
                PartType=Page }

    { 1901314507;1;Part   ;
                SubPageLink=No.=FIELD(No.);
                PagePartID=Page9089;
                ProviderID=56;
                Visible=TRUE;
                PartType=Page }

    { 1906354007;1;Part   ;
                SubPageLink=Table ID=CONST(36),
                            Document Type=FIELD(Document Type),
                            Document No.=FIELD(No.);
                PagePartID=Page9092;
                Visible=FALSE;
                PartType=Page }

    { 1907012907;1;Part   ;
                SubPageLink=No.=FIELD(No.);
                PagePartID=Page9108;
                ProviderID=56;
                Visible=FALSE;
                PartType=Page }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=TRUE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      ChangeExchangeRate@1001 : Page 511;
      CopySalesDoc@1002 : Report 292;
      MoveNegSalesLines@1006 : Report 6699;
      ReportPrint@1003 : Codeunit 228;
      UserMgt@1004 : Codeunit 5700;
      SalesCalcDiscountByType@1007 : Codeunit 56;
      JobQueueVisible@1000 : Boolean INDATASET;
      DocNoVisible@1005 : Boolean;
      ExternalDocNoMandatory@1008 : Boolean;

    LOCAL PROCEDURE Post@4(PostingCodeunitID@1000 : Integer);
    BEGIN
      SendToPosting(PostingCodeunitID);
      IF "Job Queue Status" = "Job Queue Status"::"Scheduled for Posting" THEN
        CurrPage.CLOSE;
      CurrPage.UPDATE(FALSE);
    END;

    LOCAL PROCEDURE ApproveCalcInvDisc@1();
    BEGIN
      CurrPage.SalesLines.PAGE.ApproveCalcInvDisc;
    END;

    LOCAL PROCEDURE SelltoCustomerNoOnAfterValidat@19034782();
    BEGIN
      IF GETFILTER("Sell-to Customer No.") = xRec."Sell-to Customer No." THEN
        IF "Sell-to Customer No." <> xRec."Sell-to Customer No." THEN
          SETRANGE("Sell-to Customer No.");
      CurrPage.UPDATE;
    END;

    LOCAL PROCEDURE SalespersonCodeOnAfterValidate@19011896();
    BEGIN
      CurrPage.SalesLines.PAGE.UpdateForm(TRUE);
    END;

    LOCAL PROCEDURE BilltoCustomerNoOnAfterValidat@19044114();
    BEGIN
      CurrPage.UPDATE;
    END;

    LOCAL PROCEDURE ShortcutDimension1CodeOnAfterV@19029405();
    BEGIN
      CurrPage.UPDATE;
    END;

    LOCAL PROCEDURE ShortcutDimension2CodeOnAfterV@19008725();
    BEGIN
      CurrPage.UPDATE;
    END;

    LOCAL PROCEDURE PricesIncludingVATOnAfterValid@19009096();
    BEGIN
      CurrPage.UPDATE;
    END;

    LOCAL PROCEDURE SetDocNoVisible@3();
    VAR
      DocumentNoVisibility@1000 : Codeunit 1400;
      DocType@1003 : 'Quote,Order,Invoice,Credit Memo,Blanket Order,Return Order,Reminder,FinChMemo';
    BEGIN
      DocNoVisible := DocumentNoVisibility.SalesDocumentNoIsVisible(DocType::Invoice,"No.");
    END;

    LOCAL PROCEDURE SetExtDocNoMandatoryCondition@2();
    VAR
      SalesReceivablesSetup@1000 : Record 311;
    BEGIN
      SalesReceivablesSetup.GET;
      ExternalDocNoMandatory := SalesReceivablesSetup."Ext. Doc. No. Mandatory"
    END;

    BEGIN
    {
      001  14/05/2010  JJB  WCMFSD019  Add field <Posting Description> onto General Group and promote it
    }
    END.
  }
}

