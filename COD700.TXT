OBJECT Codeunit 700 Page Management
{
  OBJECT-PROPERTIES
  {
    Date=13/03/15;
    Time=12:00:00;
    Version List=NAVW18.00.00.40262;
  }
  PROPERTIES
  {
    OnRun=BEGIN
          END;

  }
  CODE
  {
    VAR
      DataTypeManagement@1001 : Codeunit 701;

    PROCEDURE PageRun@7(RecRelatedVariant@1000 : Variant) : Boolean;
    VAR
      RecordRef@1002 : RecordRef;
      RecordRefVariant@1003 : Variant;
      PageID@1004 : Integer;
    BEGIN
      IF NOT GUIALLOWED THEN
        EXIT(FALSE);

      IF NOT DataTypeManagement.GetRecordRef(RecRelatedVariant,RecordRef) THEN
        EXIT(FALSE);

      PageID := GetPageID(RecordRef);

      IF PageID <> 0 THEN BEGIN
        RecordRefVariant := RecordRef;
        PAGE.RUN(PageID,RecordRefVariant);
        EXIT(TRUE);
      END;

      EXIT(FALSE);
    END;

    PROCEDURE PageRunModal@10(RecRelatedVariant@1000 : Variant) : Boolean;
    VAR
      RecordRef@1002 : RecordRef;
      RecordRefVariant@1003 : Variant;
      PageID@1004 : Integer;
    BEGIN
      IF NOT GUIALLOWED THEN
        EXIT(FALSE);

      IF NOT DataTypeManagement.GetRecordRef(RecRelatedVariant,RecordRef) THEN
        EXIT(FALSE);

      PageID := GetPageID(RecordRef);

      IF PageID <> 0 THEN BEGIN
        RecordRefVariant := RecordRef;
        PAGE.RUNMODAL(PageID,RecordRefVariant);
        EXIT(TRUE);
      END;

      EXIT(FALSE);
    END;

    PROCEDURE PageRunModalWithFieldFocus@1(RecRelatedVariant@1000 : Variant;FieldNumber@1001 : Integer) : Boolean;
    VAR
      RecordRef@1002 : RecordRef;
      RecordRefVariant@1003 : Variant;
      PageID@1004 : Integer;
    BEGIN
      IF NOT GUIALLOWED THEN
        EXIT(FALSE);

      IF NOT DataTypeManagement.GetRecordRef(RecRelatedVariant,RecordRef) THEN
        EXIT(FALSE);

      PageID := GetPageID(RecordRef);

      IF PageID <> 0 THEN BEGIN
        RecordRefVariant := RecordRef;
        PAGE.RUNMODAL(PageID,RecordRefVariant,FieldNumber);
        EXIT(TRUE);
      END;

      EXIT(FALSE);
    END;

    PROCEDURE GetPageID@4(RecRelatedVariant@1001 : Variant) : Integer;
    VAR
      MiniPagesMapping@1000 : Record 1305;
      RecordRef@1004 : RecordRef;
      PageID@1003 : Integer;
    BEGIN
      IF NOT DataTypeManagement.GetRecordRef(RecRelatedVariant,RecordRef) THEN
        EXIT;

      PageID := GetConditionalCardPageID(RecordRef);

      IF PageID = 0 THEN
        PageID := GetDefaultCardPageID(RecordRef.NUMBER);

      IF PageID = 0 THEN
        PageID := GetDefaultLookupPageID(RecordRef.NUMBER);

      IF MiniPagesMapping.READPERMISSION THEN
        IF MiniPagesMapping.GET(PageID) THEN
          PageID := MiniPagesMapping."Substitute Page ID";

      EXIT(PageID);
    END;

    PROCEDURE GetDefaultCardPageID@2(TableID@1000 : Integer) : Integer;
    VAR
      ObjectMetadata@1003 : Record 2000000071;
      ObjectXmlDoc@1002 : DotNet "'System.Xml, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.Xml.XmlDocument";
      XMLAttribute@1001 : DotNet "'System.Xml, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.Xml.XmlAttribute";
      XMLNsMgr@1004 : DotNet "'System.Xml, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.Xml.XmlNamespaceManager";
      XMLNode@1005 : DotNet "'System.Xml, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.Xml.XmlNode";
      LookupPageID@1006 : Integer;
      CardPageID@1007 : Integer;
    BEGIN
      IF TableID = 0 THEN
        EXIT(0);

      LookupPageID := GetDefaultLookupPageID(TableID);
      IF LookupPageID <> 0 THEN BEGIN
        IF NOT GetObjectMetadataXmlDoc(ObjectXmlDoc,ObjectMetadata."Object Type"::Page,LookupPageID) THEN
          EXIT(0);
        XMLNsMgr := XMLNsMgr.XmlNamespaceManager(ObjectXmlDoc.NameTable);
        XMLNsMgr.AddNamespace('ns','urn:schemas-microsoft-com:dynamics:NAV:MetaObjects');

        XMLNode := ObjectXmlDoc.SelectSingleNode('/ns:PageDefinition/ns:Properties',XMLNsMgr);
        XMLAttribute := XMLNode.Attributes.ItemOf('CardFormID');
        IF NOT ISNULL(XMLAttribute) THEN
          EVALUATE(CardPageID,XMLAttribute.Value);
        EXIT(CardPageID);
      END;
      EXIT(0);
    END;

    PROCEDURE GetDefaultLookupPageID@8(TableID@1000 : Integer) : Integer;
    VAR
      ObjectMetadata@1003 : Record 2000000071;
      ObjectXmlDoc@1002 : DotNet "'System.Xml, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.Xml.XmlDocument";
      XMLAttribute@1001 : DotNet "'System.Xml, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.Xml.XmlAttribute";
      LookupPageID@1004 : Integer;
    BEGIN
      IF TableID = 0 THEN
        EXIT(0);

      IF NOT GetObjectMetadataXmlDoc(ObjectXmlDoc,ObjectMetadata."Object Type"::Table,TableID) THEN
        EXIT(0);
      XMLAttribute := ObjectXmlDoc.DocumentElement.Attributes.ItemOf('LookupFormID');
      IF NOT ISNULL(XMLAttribute) THEN
        EVALUATE(LookupPageID,XMLAttribute.Value);
      EXIT(LookupPageID);
    END;

    PROCEDURE GetConditionalCardPageID@3(RecordRef@1001 : RecordRef) : Integer;
    BEGIN
      CASE RecordRef.NUMBER OF
        DATABASE::"Company Information":
          EXIT(PAGE::"Company Information");
        DATABASE::"Sales Header":
          EXIT(GetSalesHeaderPageID(RecordRef));
        DATABASE::"Purchase Header":
          EXIT(GetPurchaseHeaderPageID(RecordRef));
        DATABASE::"Service Header":
          EXIT(GetServiceHeaderPageID(RecordRef));
        DATABASE::"Gen. Journal Line":
          EXIT(GetGenJournalLinePageID(RecordRef));
        DATABASE::"Sales Header Archive":
          EXIT(GetSalesHeaderArchivePageID(RecordRef));
        DATABASE::"Purchase Header Archive":
          EXIT(GetPurchaseHeaderArchivePageID(RecordRef));
        DATABASE::"Res. Journal Line":
          EXIT(PAGE::"Resource Journal");
        DATABASE::"Job Journal Line":
          EXIT(PAGE::"Job Journal");
        DATABASE::"Item Analysis View":
          EXIT(GetAnalysisViewPageID(RecordRef));
      END;
      EXIT(0);
    END;

    LOCAL PROCEDURE GetSalesHeaderPageID@15(RecordRef@1000 : RecordRef) : Integer;
    VAR
      SalesHeader@1001 : Record 36;
    BEGIN
      RecordRef.SETTABLE(SalesHeader);
      CASE SalesHeader."Document Type" OF
        SalesHeader."Document Type"::Quote:
          EXIT(PAGE::"Sales Quote");
        SalesHeader."Document Type"::Order:
          EXIT(PAGE::"Sales Order");
        SalesHeader."Document Type"::Invoice:
          EXIT(PAGE::"Sales Invoice");
        SalesHeader."Document Type"::"Credit Memo":
          EXIT(PAGE::"Sales Credit Memo");
        SalesHeader."Document Type"::"Blanket Order":
          EXIT(PAGE::"Blanket Sales Order");
        SalesHeader."Document Type"::"Return Order":
          EXIT(PAGE::"Sales Return Order");
      END;
    END;

    LOCAL PROCEDURE GetPurchaseHeaderPageID@17(RecordRef@1000 : RecordRef) : Integer;
    VAR
      PurchaseHeader@1001 : Record 38;
    BEGIN
      RecordRef.SETTABLE(PurchaseHeader);
      CASE PurchaseHeader."Document Type" OF
        PurchaseHeader."Document Type"::Quote:
          EXIT(PAGE::"Purchase Quote");
        PurchaseHeader."Document Type"::Order:
          EXIT(PAGE::"Purchase Order");
        PurchaseHeader."Document Type"::Invoice:
          EXIT(PAGE::"Purchase Invoice");
        PurchaseHeader."Document Type"::"Credit Memo":
          EXIT(PAGE::"Purchase Credit Memo");
        PurchaseHeader."Document Type"::"Blanket Order":
          EXIT(PAGE::"Blanket Purchase Order");
        PurchaseHeader."Document Type"::"Return Order":
          EXIT(PAGE::"Purchase Return Order");
      END;
    END;

    LOCAL PROCEDURE GetServiceHeaderPageID@21(RecordRef@1000 : RecordRef) : Integer;
    VAR
      ServiceHeader@1001 : Record 5900;
    BEGIN
      RecordRef.SETTABLE(ServiceHeader);
      CASE ServiceHeader."Document Type" OF
        ServiceHeader."Document Type"::Quote:
          EXIT(PAGE::"Service Quote");
        ServiceHeader."Document Type"::Order:
          EXIT(PAGE::"Service Order");
        ServiceHeader."Document Type"::Invoice:
          EXIT(PAGE::"Service Invoice");
        ServiceHeader."Document Type"::"Credit Memo":
          EXIT(PAGE::"Service Credit Memo");
      END;
    END;

    LOCAL PROCEDURE GetGenJournalLinePageID@20(RecordRef@1000 : RecordRef) : Integer;
    VAR
      GenJournalLine@1003 : Record 81;
      GenJournalTemplate@1002 : Record 80;
    BEGIN
      RecordRef.SETTABLE(GenJournalLine);
      GenJournalTemplate.GET(GenJournalLine."Journal Template Name");
      IF GenJournalTemplate.Recurring THEN
        EXIT(PAGE::"Recurring General Journal");
      CASE GenJournalTemplate.Type OF
        GenJournalTemplate.Type::General:
          EXIT(PAGE::"General Journal");
        GenJournalTemplate.Type::Sales:
          EXIT(PAGE::"Sales Journal");
        GenJournalTemplate.Type::Purchases:
          EXIT(PAGE::"Purchase Journal");
        GenJournalTemplate.Type::"Cash Receipts":
          EXIT(PAGE::"Cash Receipt Journal");
        GenJournalTemplate.Type::Payments:
          EXIT(PAGE::"Payment Journal");
        GenJournalTemplate.Type::Assets:
          EXIT(PAGE::"Fixed Asset G/L Journal");
        GenJournalTemplate.Type::Intercompany:
          EXIT(PAGE::"IC General Journal");
        GenJournalTemplate.Type::Jobs:
          EXIT(PAGE::"Job G/L Journal");
      END;
    END;

    LOCAL PROCEDURE GetSalesHeaderArchivePageID@19(RecordRef@1000 : RecordRef) : Integer;
    VAR
      SalesHeaderArchive@1003 : Record 5107;
    BEGIN
      RecordRef.SETTABLE(SalesHeaderArchive);
      CASE SalesHeaderArchive."Document Type" OF
        SalesHeaderArchive."Document Type"::Quote:
          EXIT(PAGE::"Sales Quote Archive");
        SalesHeaderArchive."Document Type"::Order:
          EXIT(PAGE::"Sales Order Archive");
        SalesHeaderArchive."Document Type"::"Return Order":
          EXIT(PAGE::"Sales Return Order Archive");
      END;
    END;

    LOCAL PROCEDURE GetPurchaseHeaderArchivePageID@18(RecordRef@1000 : RecordRef) : Integer;
    VAR
      PurchaseHeaderArchive@1002 : Record 5109;
    BEGIN
      RecordRef.SETTABLE(PurchaseHeaderArchive);
      CASE PurchaseHeaderArchive."Document Type" OF
        PurchaseHeaderArchive."Document Type"::Quote:
          EXIT(PAGE::"Purchase Quote Archive");
        PurchaseHeaderArchive."Document Type"::Order:
          EXIT(PAGE::"Purchase Order Archive");
        PurchaseHeaderArchive."Document Type"::"Return Order":
          EXIT(PAGE::"Purchase Return Order Archive");
      END;
    END;

    LOCAL PROCEDURE GetAnalysisViewPageID@12(RecordRef@1000 : RecordRef) : Integer;
    VAR
      ItemAnalysisView@1002 : Record 7152;
    BEGIN
      RecordRef.SETTABLE(ItemAnalysisView);
      CASE ItemAnalysisView."Analysis Area" OF
        ItemAnalysisView."Analysis Area"::Sales:
          EXIT(PAGE::"Sales Analysis View Card");
        ItemAnalysisView."Analysis Area"::Purchase:
          EXIT(PAGE::"Purchase Analysis View Card");
        ItemAnalysisView."Analysis Area"::Inventory:
          EXIT(PAGE::"Invt. Analysis View Card");
      END;
    END;

    LOCAL PROCEDURE GetObjectMetadataXmlDoc@24(VAR ObjectXmlDoc@1000 : DotNet "'System.Xml, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.Xml.XmlDocument";ObjectType@1001 : Option;TableID@1002 : Integer) : Boolean;
    VAR
      ObjectMetadata@1003 : Record 2000000071;
      InStream@1004 : InStream;
    BEGIN
      IF NOT ObjectMetadata.GET(ObjectType,TableID) THEN
        EXIT(FALSE);
      ObjectMetadata.CALCFIELDS(Metadata);
      ObjectMetadata.Metadata.CREATEINSTREAM(InStream);
      ObjectXmlDoc := ObjectXmlDoc.XmlDocument;
      ObjectXmlDoc.Load(InStream);
      EXIT(TRUE);
    END;

    BEGIN
    END.
  }
}

