OBJECT Table 52112866 Company Dividend Paid
{
  OBJECT-PROPERTIES
  {
    Date=12/05/15;
    Time=15:24:58;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Company Dividend Paid;
               PTB=Rendimento Pago];
  }
  FIELDS
  {
    { 10  ;   ;Line No             ;Integer       ;CaptionML=[ENU=Line No.;
                                                              PTB=N� Linha] }
    { 20  ;   ;Country Code        ;Code10        ;TableRelation=Country/Region;
                                                   OnValidate=BEGIN
                                                                VALIDATE(CPFCNPJ, '');
                                                              END;

                                                   CaptionML=[ENU=Country Code;
                                                              PTB=C�d. Pa�s] }
    { 30  ;   ;Type                ;Option        ;OnValidate=BEGIN
                                                                VALIDATE(CPFCNPJ, '');
                                                              END;

                                                   CaptionML=[ENU=Type;
                                                              PTB=Tipo];
                                                   OptionCaptionML=[ENU=Person,Company;
                                                                    PTB=Pessoa F�sica,Pessoa Jur�dica];
                                                   OptionString=Person,Company }
    { 40  ;   ;CPFCNPJ             ;Text20        ;OnValidate=VAR
                                                                Country@1010000000 : Record 9;
                                                                FiscalFunc@1010000001 : Codeunit 52112424;
                                                                SiscomexCode@1010000002 : Code[10];
                                                              BEGIN
                                                                IF CPFCNPJ = '' THEN
                                                                  EXIT;

                                                                TESTFIELD("Country Code");
                                                                Country.GET("Country Code");
                                                                Country.TESTFIELD("BACEN Code");
                                                                SiscomexCode := FiscalFunc.GetCountrySiscomexCodeFromBACENCode(Country."BACEN Code");
                                                                IF SiscomexCode <> FiscalFunc.GetBrazilSiscomexCode THEN
                                                                  FIELDERROR("Country Code");

                                                                CASE Type OF
                                                                  Type::Person: FiscalFunc.CheckCPF(CPFCNPJ);
                                                                  Type::Company: FiscalFunc.CheckCNPJ(CPFCNPJ);
                                                                END;
                                                              END;

                                                   CaptionML=[ENU=CPF/CNPJ;
                                                              PTB=CPF/CNPJ] }
    { 50  ;   ;Name                ;Text250       ;CaptionML=[ENU=Name;
                                                              PTB=Nome] }
    { 60  ;   ;Qualification Code  ;Code10        ;TableRelation="ECD Qualification".Code WHERE (Type=CONST(CompAssociate));
                                                   CaptionML=[ENU=Qualification Code;
                                                              PTB=C�d. Qualifica��o] }
    { 70  ;   ;Work Salary         ;Decimal       ;CaptionML=[ENU=Work Salary;
                                                              PTB=Remunera��o Trabalho];
                                                   MinValue=0;
                                                   BlankNumbers=BlankZero }
    { 80  ;   ;Dividends           ;Decimal       ;CaptionML=[ENU=Dividends;
                                                              PTB=Dividendos];
                                                   MinValue=0;
                                                   BlankNumbers=BlankZero }
    { 90  ;   ;Interest Over Own Capital;Decimal  ;CaptionML=[ENU=Interest Over Own Capital;
                                                              PTB=Juros Sobre Capital Pr�prio];
                                                   MinValue=0;
                                                   BlankNumbers=BlankZero }
    { 100 ;   ;Other Income        ;Decimal       ;CaptionML=[ENU=Other Income;
                                                              PTB=Demais Rendimentos];
                                                   MinValue=0;
                                                   BlankNumbers=BlankZero }
    { 110 ;   ;IRRF                ;Decimal       ;CaptionML=[ENU=IRRF;
                                                              PTB=IRRF];
                                                   MinValue=0;
                                                   BlankNumbers=BlankZero }
    { 1000;   ;Date                ;Date          ;CaptionML=[ENU=Date;
                                                              PTB=Data] }
  }
  KEYS
  {
    {    ;Line No                                 ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

