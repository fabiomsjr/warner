OBJECT Table 5716 Substitution Condition
{
  OBJECT-PROPERTIES
  {
    Date=05/11/08;
    Time=12:00:00;
    Version List=NAVW16.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Substitution Condition;
               PTB=Condi��o Substitui��o];
  }
  FIELDS
  {
    { 1   ;   ;No.                 ;Code20        ;TableRelation="Item Substitution".No. WHERE (No.=FIELD(No.));
                                                   CaptionML=[ENU=No.;
                                                              PTB=N�] }
    { 2   ;   ;Variant Code        ;Code10        ;TableRelation="Item Substitution"."Variant Code" WHERE (No.=FIELD(No.),
                                                                                                           Variant Code=FIELD(Variant Code));
                                                   CaptionML=[ENU=Variant Code;
                                                              PTB=C�digo Variante] }
    { 3   ;   ;Substitute No.      ;Code20        ;TableRelation="Item Substitution"."Substitute No." WHERE (No.=FIELD(No.),
                                                                                                             Variant Code=FIELD(Variant Code),
                                                                                                             Substitute No.=FIELD(Substitute No.));
                                                   CaptionML=[ENU=Substitute No.;
                                                              PTB=N� Substituto] }
    { 4   ;   ;Substitute Variant Code;Code10     ;TableRelation="Item Substitution"."Substitute Variant Code" WHERE (No.=FIELD(No.),
                                                                                                                      Variant Code=FIELD(Variant Code),
                                                                                                                      Substitute No.=FIELD(Substitute No.),
                                                                                                                      Substitute Variant Code=FIELD(Substitute Variant Code));
                                                   CaptionML=[ENU=Substitute Variant Code;
                                                              PTB=Cod. Variante Substituto] }
    { 5   ;   ;Line No.            ;Integer       ;CaptionML=[ENU=Line No.;
                                                              PTB=N� Linha] }
    { 6   ;   ;Condition           ;Text80        ;CaptionML=[ENU=Condition;
                                                              PTB=Condi��o] }
    { 100 ;   ;Type                ;Option        ;CaptionML=[ENU=Type;
                                                              PTB=Tipo];
                                                   OptionCaptionML=[ENU=Item,Nonstock Item;
                                                                    PTB=Produto,Prod. n�o Invnt.];
                                                   OptionString=Item,Nonstock Item }
    { 101 ;   ;Substitute Type     ;Option        ;CaptionML=[ENU=Substitute Type;
                                                              PTB=Tipo Substituto];
                                                   OptionCaptionML=[ENU=Item,Nonstock Item;
                                                                    PTB=Produto,Prod. n�o Invnt.];
                                                   OptionString=Item,Nonstock Item }
  }
  KEYS
  {
    {    ;Type,No.,Variant Code,Substitute Type,Substitute No.,Substitute Variant Code,Line No.;
                                                   Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

