OBJECT Table 415 IC Outbox Jnl. Line
{
  OBJECT-PROPERTIES
  {
    Date=09/09/14;
    Time=12:00:00;
    Version List=NAVW18.00;
  }
  PROPERTIES
  {
    OnDelete=VAR
               DimMgt@1000 : Codeunit 408;
             BEGIN
               DimMgt.DeleteICJnlDim(
                 DATABASE::"IC Outbox Jnl. Line","Transaction No.","IC Partner Code","Transaction Source","Line No.");
             END;

    CaptionML=[ENU=IC Outbox Jnl. Line;
               PTB=IC Caixa Sa�da Linha Di�rio];
  }
  FIELDS
  {
    { 1   ;   ;Transaction No.     ;Integer       ;CaptionML=[ENU=Transaction No.;
                                                              PTB=N� Lan�amento];
                                                   Editable=No }
    { 2   ;   ;IC Partner Code     ;Code20        ;TableRelation="IC Partner".Code;
                                                   CaptionML=[ENU=IC Partner Code;
                                                              PTB=IC C�digo Parceiro];
                                                   Editable=No }
    { 3   ;   ;Line No.            ;Integer       ;CaptionML=[ENU=Line No.;
                                                              PTB=N� Linha];
                                                   Editable=No }
    { 4   ;   ;Account Type        ;Option        ;CaptionML=[ENU=Account Type;
                                                              PTB=Tipo Conta];
                                                   OptionCaptionML=[ENU=G/L Account,Customer,Vendor,IC Partner;
                                                                    PTB=Conta Cont�bil,Cliente,Fornecedor,Parceiro IC];
                                                   OptionString=G/L Account,Customer,Vendor,IC Partner }
    { 5   ;   ;Account No.         ;Code20        ;TableRelation=IF (Account Type=CONST(G/L Account)) "IC G/L Account"
                                                                 ELSE IF (Account Type=CONST(Customer)) Customer
                                                                 ELSE IF (Account Type=CONST(Vendor)) Vendor
                                                                 ELSE IF (Account Type=CONST(IC Partner)) "IC Partner";
                                                   OnValidate=VAR
                                                                Customer@1000 : Record 18;
                                                                Vendor@1001 : Record 23;
                                                              BEGIN
                                                                IF ("Account No." <> xRec."Account No.") AND ("Account No." <> '') THEN
                                                                  CASE "Account Type" OF
                                                                    "Account Type"::"IC Partner":
                                                                      TESTFIELD("Account No.","IC Partner Code");
                                                                    "Account Type"::Customer:
                                                                      BEGIN
                                                                        Customer.GET("Account No.");
                                                                        Customer.TESTFIELD("IC Partner Code","IC Partner Code");
                                                                      END;
                                                                    "Account Type"::Vendor:
                                                                      BEGIN
                                                                        Vendor.GET("Account No.");
                                                                        Vendor.TESTFIELD("IC Partner Code","IC Partner Code");
                                                                      END;
                                                                  END;
                                                              END;

                                                   CaptionML=[ENU=Account No.;
                                                              PTB=Conta No. 2] }
    { 6   ;   ;Amount              ;Decimal       ;CaptionML=[ENU=Amount;
                                                              PTB=Valor];
                                                   Editable=No }
    { 7   ;   ;Description         ;Text50        ;CaptionML=[ENU=Description;
                                                              PTB=Descri��o] }
    { 8   ;   ;VAT Amount          ;Decimal       ;CaptionML=[ENU=VAT Amount;
                                                              PTB=Valor IPI];
                                                   Editable=No }
    { 9   ;   ;Currency Code       ;Code10        ;AccessByPermission=TableData 4=R;
                                                   CaptionML=[ENU=Currency Code;
                                                              PTB=Cod. Moeda];
                                                   Editable=No }
    { 11  ;   ;Due Date            ;Date          ;CaptionML=[ENU=Due Date;
                                                              PTB=Data Vencimento] }
    { 12  ;   ;Payment Discount %  ;Decimal       ;CaptionML=[ENU=Payment Discount %;
                                                              PTB=% Desconto P.P.];
                                                   DecimalPlaces=0:5;
                                                   MinValue=0;
                                                   MaxValue=100 }
    { 13  ;   ;Payment Discount Date;Date         ;CaptionML=[ENU=Payment Discount Date;
                                                              PTB=Data Desconto Pagamento] }
    { 14  ;   ;Quantity            ;Decimal       ;CaptionML=[ENU=Quantity;
                                                              PTB=Quantidade];
                                                   Editable=No }
    { 15  ;   ;Transaction Source  ;Option        ;CaptionML=[ENU=Transaction Source;
                                                              PTB=Fonte Transa��o];
                                                   OptionCaptionML=[ENU=Rejected by Current Company,Created by Current Company;
                                                                    PTB=Rejeitado pela Empresa Atual,Criado pela Empresa Atual];
                                                   OptionString=Rejected by Current Company,Created by Current Company;
                                                   Editable=No }
    { 16  ;   ;Document No.        ;Code20        ;CaptionML=[ENU=Document No.;
                                                              PTB=No. Documento];
                                                   Editable=No }
  }
  KEYS
  {
    {    ;Transaction No.,IC Partner Code,Transaction Source,Line No.;
                                                   Clustered=Yes }
    {    ;IC Partner Code                          }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

