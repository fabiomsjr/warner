OBJECT Table 52112467 Posted Invoice Item Tracking
{
  OBJECT-PROPERTIES
  {
    Date=11/12/15;
    Time=16:50:40;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Posted Invoice Item Tracking;
               PTB=Rastreabilidade Produto NF Reg.];
  }
  FIELDS
  {
    { 10  ;   ;Document Type       ;Option        ;CaptionML=[ENU=Document Type;
                                                              PTB=Tipo Documento];
                                                   OptionCaptionML=[ENU=Sales Invoice,Purch. Invoice,Sales Cr. Memo,Purch. Cr. Memo,Service Invoice,Service Credit Memo;
                                                                    PTB=Venda,Compra,Cr�dito Venda,D�bito Compra,Nota Servi�o,Nota Cr�dito Servi�o];
                                                   OptionString=Sales Invoice,Purch. Invoice,Sales Cr. Memo,Purch. Cr. Memo,Service Inv.,Serv. Cr. Memo }
    { 20  ;   ;Document No.        ;Code20        ;TableRelation=IF (Document Type=CONST(Sales Invoice)) "Sales Invoice Header"
                                                                 ELSE IF (Document Type=CONST(Purch. Invoice)) "Purch. Inv. Header"
                                                                 ELSE IF (Document Type=CONST(Sales Cr. Memo)) "Sales Cr.Memo Header"
                                                                 ELSE IF (Document Type=CONST(Purch. Cr. Memo)) "Purch. Cr. Memo Hdr."
                                                                 ELSE IF (Document Type=CONST(Service Inv.)) "Service Invoice Header"
                                                                 ELSE IF (Document Type=CONST(Serv. Cr. Memo)) "Service Cr.Memo Header";
                                                   CaptionML=[ENU=Document No.;
                                                              PTB=N� Documento] }
    { 30  ;   ;Document Line No.   ;Integer       ;TableRelation=IF (Document Type=CONST(Sales Invoice)) "Sales Invoice Line"."Line No." WHERE (Document No.=FIELD(Document No.))
                                                                 ELSE IF (Document Type=CONST(Purch. Invoice)) "Purch. Inv. Line"."Line No." WHERE (Document No.=FIELD(Document No.))
                                                                 ELSE IF (Document Type=CONST(Sales Cr. Memo)) "Sales Cr.Memo Line"."Line No." WHERE (Document No.=FIELD(Document No.))
                                                                 ELSE IF (Document Type=CONST(Purch. Cr. Memo)) "Purch. Cr. Memo Line"."Line No." WHERE (Document No.=FIELD(Document No.))
                                                                 ELSE IF (Document Type=CONST(Service Inv.)) "Service Invoice Line"."Line No." WHERE (Document No.=FIELD(Document No.))
                                                                 ELSE IF (Document Type=CONST(Serv. Cr. Memo)) "Service Cr.Memo Line"."Line No." WHERE (Document No.=FIELD(Document No.));
                                                   CaptionML=[ENU=Document Line No.;
                                                              PTB=N� Linha Documento] }
    { 40  ;   ;Line No.            ;Integer       ;CaptionML=[ENU=Line No.;
                                                              PTB=N� Linha] }
    { 50  ;   ;Item No.            ;Code20        ;TableRelation=Item;
                                                   CaptionML=[ENU=Item No.;
                                                              PTB=N� Produto] }
    { 60  ;   ;Quantity            ;Decimal       ;CaptionML=[ENU=Quantity;
                                                              PTB=Quantidade];
                                                   DecimalPlaces=0:5 }
    { 70  ;   ;Description         ;Text50        ;CaptionML=[ENU=Description;
                                                              PTB=Descri��o] }
    { 80  ;   ;Serial No.          ;Code20        ;CaptionML=[ENU=Serial No.;
                                                              PTB=N� S�rie] }
    { 90  ;   ;Lot No.             ;Code20        ;CaptionML=[ENU=Lot No.;
                                                              PTB=N� Lote] }
    { 100 ;   ;Variant Code        ;Code10        ;TableRelation="Item Variant".Code WHERE (Item No.=FIELD(Item No.));
                                                   CaptionML=[ENU=Variant Code;
                                                              PTB=C�digo Variante] }
    { 110 ;   ;Warranty Date       ;Date          ;CaptionML=[ENU=Warranty Date;
                                                              PTB=Data Garantia] }
    { 120 ;   ;Expiration Date     ;Date          ;CaptionML=[ENU=Expiration Date;
                                                              PTB=Data T�rmino] }
  }
  KEYS
  {
    {    ;Document Type,Document No.,Document Line No.,Line No.;
                                                   Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      Text002@1102300001 : TextConst 'PTB=Arquivo DANFe n�o encontrado';
      Text003@1102300000 : TextConst 'PTB=Arquivo XML n�o encontrado';
      FileMgt@35000000 : Codeunit 419;
      Text004@35000001 : TextConst 'ENU=Download file;PTB=Baixar arquivo';
      Text005@35000002 : TextConst 'ENU=XML File (*.xml)|*.xml;PTB=Arquivo XML (*.xml)|*.xml';
      Text006@35000003 : TextConst 'ENU=PDF File (*.pdf)|*.pdf;PTB=Arquivo PDF (*.pdf)|*.pdf';

    PROCEDURE FilterByNFe@52006500(NFe@52006500 : Record 52112624);
    BEGIN
      CASE NFe."Document Type" OF
        NFe."Document Type"::"Sales Invoice": SETRANGE("Document Type", "Document Type"::"Sales Invoice");
        NFe."Document Type"::"Sales Cr. Memo": SETRANGE("Document Type", "Document Type"::"Sales Cr. Memo");
        NFe."Document Type"::"Purch. Invoice": SETRANGE("Document Type", "Document Type"::"Purch. Invoice");
        NFe."Document Type"::"Purch. Cr. Memo": SETRANGE("Document Type", "Document Type"::"Purch. Cr. Memo");
        NFe."Document Type"::"Service Invoice": SETRANGE("Document Type", "Document Type"::"Service Inv.");
        NFe."Document Type"::"Service Cr. Memo": SETRANGE("Document Type", "Document Type"::"Serv. Cr. Memo");
      ELSE
        EXIT;
      END;
      SETRANGE("Document No.", NFe."Document No.");
    END;

    BEGIN
    END.
  }
}

