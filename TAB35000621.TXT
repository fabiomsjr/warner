OBJECT Table 35000621 EFD File Line2
{
  OBJECT-PROPERTIES
  {
    Date=29/02/12;
    Time=14:06:25;
    Version List=FNX033.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Fiscal Lines Mov.;
               PTB=Linhas do Mov. Fiscal];
    LookupPageID=Page35000537;
    DrillDownPageID=Page35000537;
  }
  FIELDS
  {
    { 1   ;   ;Tax Settlement No.  ;Code20        ;TableRelation="Tax Settlement 20092";
                                                   CaptionML=[ENU=Settlement No.;
                                                              PTB=N� Apura��o] }
    { 2   ;   ;Level               ;Integer       ;CaptionML=[ENU=Level;
                                                              PTB=N�vel] }
    { 3   ;   ;Entry Code          ;Code10        ;CaptionML=[ENU=Entry Code;
                                                              PTB=C�d. Registro] }
    { 4   ;   ;File Type           ;Option        ;CaptionML=[ENU=File Type;
                                                              PTB=Tipo Arquivo];
                                                   OptionCaptionML=PTB=" ,Fiscal,PIS/COFINS,Cont�bil,FCont";
                                                   OptionString=[ ,Fiscal,PC,Contabil,FCont] }
    { 5   ;   ;Text                ;Text250       ;CaptionML=[ENU=Text;
                                                              PTB=Texto] }
    { 6   ;   ;Line No.            ;Integer       ;CaptionML=[ENU=Line No.;
                                                              PTB=N� Linha] }
  }
  KEYS
  {
    {    ;Tax Settlement No.,File Type,Line No.   ;Clustered=Yes }
    {    ;Tax Settlement No.,Entry Code            }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

