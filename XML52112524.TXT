OBJECT XMLport 52112524 Siscoserv RetificarRVS
{
  OBJECT-PROPERTIES
  {
    Date=21/07/15;
    Time=16:07:19;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    Direction=Export;
    Encoding=UTF-8;
    Format/Evaluate=XML Format/Evaluate;
  }
  ELEMENTS
  {
    { [{4789AA0B-8276-4CF3-B458-1A5FE61D42B9}];  ;RetificarRVS        ;Element ;Table   ;
                                                  ReqFilterFields=Field10;
                                                  VariableName=RVS;
                                                  SourceTable=Table52112673;
                                                  SourceTableView=SORTING(Field10);
                                                  Export::OnAfterGetRecord=BEGIN
                                                                             RVS.GetXMLData(Nif, CodigoPaisAdquirente, EnderecoAdquirente, CodigoMoeda, InfoComplementar);
                                                                           END;
                                                                            }

    { [{0DB84B5F-8C61-4A64-987E-09D2CDEB181A}];1 ;NumeroRVSEmpresa    ;Element ;Field   ;
                                                  DataType=Code;
                                                  SourceField=RVS::No. }

    { [{ADE58FCB-52CC-41C2-AAFA-02ED0DFA9001}];1 ;NomeAdquirente      ;Element ;Field   ;
                                                  DataType=Text;
                                                  SourceField=RVS::Customer Name }

    { [{53BB22D8-AFF3-422C-9C6E-FB751DE482D1}];1 ;EnderecoAdquirente  ;Element ;Text     }

    { [{49B221AF-F498-4077-8E38-452A0880D670}];1 ;CodigoPaisAdquirente;Element ;Text     }

    { [{C4A374C5-D44A-48D9-94E4-F3B56950A60A}];1 ;Nif                 ;Element ;Text     }

    { [{7C25AA47-E520-4F58-B29D-D7667FB5E47A}];1 ;Operacao            ;Element ;Table   ;
                                                  VariableName=RVSLine;
                                                  SourceTable=Table52112674;
                                                  SourceTableView=SORTING(Field10,Field20)
                                                                  WHERE(Field300=CONST(No));
                                                  LinkFields=Field10=FIELD(Field10);
                                                  LinkTable=RVS;
                                                  Export::OnAfterGetRecord=BEGIN
                                                                             RVSLine.GetXMLData(CodigoNbs, CodigoPaisDestino, ModoPrestacao);
                                                                           END;
                                                                            }

    { [{F3C4D920-170E-4713-B5EB-DA16306606EC}];2 ;NumeroOperacaoEmpresa;Element;Field   ;
                                                  DataType=Integer;
                                                  SourceField=RVSLine::Line No. }

    { [{4E9F78A8-5906-41A3-918B-3691CDF2BADB}];2 ;CodigoNbs           ;Element ;Text     }

    { [{66FC7BE9-31E0-4137-80BD-F56249EEF4CA}];2 ;CodigoPaisDestino   ;Element ;Text     }

    { [{EBC203A7-AF6F-405C-B53F-6AC101969D70}];2 ;ModoPrestacao       ;Element ;Text     }

    { [{1D9169B0-3B0A-4510-9B81-58D303181B44}];2 ;DataInicio          ;Element ;Field   ;
                                                  DataType=Date;
                                                  SourceField=RVSLine::Starting Date }

    { [{40827D63-32F5-4735-BF28-1800B649A1F8}];2 ;DataConclusao       ;Element ;Field   ;
                                                  DataType=Date;
                                                  SourceField=RVSLine::Ending Date }

    { [{39BF0B21-E058-49A9-9BF1-8A379CF4702B}];2 ;Valor               ;Element ;Field   ;
                                                  DataType=Decimal;
                                                  SourceField=RVSLine::Amount }

    { [{4E8FBA23-EC00-4F48-9431-CB3F4D16D3F6}];2 ;Enquadramento       ;Element ;Table   ;
                                                  VariableName=RVSLineLFramewrk;
                                                  SourceTable=Table52112676;
                                                  SourceTableView=SORTING(Field10,Field20,Field30);
                                                  LinkFields=Field10=FIELD(Field10),
                                                             Field20=FIELD(Field20);
                                                  LinkTable=RVSLine;
                                                  MinOccurs=Zero }

    { [{9353FB98-B80B-48A4-A661-D7346B013FB3}];3 ;CodigoEnquadramento ;Element ;Field   ;
                                                  DataType=Code;
                                                  SourceField=RVSLineLFramewrk::Statute Code }

    { [{016CD081-B642-4135-A0EB-6FF1457C0041}];3 ;NumeroRc            ;Element ;Field   ;
                                                  DataType=Code;
                                                  SourceField=RVSLineLFramewrk::Credit Reg. No.;
                                                  MinOccurs=Zero;
                                                  Export::OnBeforePassField=BEGIN
                                                                              IF RVSLineLFramewrk."Credit Reg. No." = '' THEN
                                                                                currXMLport.SKIP;
                                                                            END;
                                                                             }

    { [{F989497E-3108-4445-90EB-1FBDFDED87D5}];1 ;VincExportacaoBens  ;Element ;Table   ;
                                                  VariableName=RVSExportLink;
                                                  SourceTable=Table52112678;
                                                  SourceTableView=SORTING(Field10,Field30);
                                                  LinkFields=Field10=FIELD(Field10);
                                                  LinkTable=RVS }

    { [{27A9F881-D83B-4114-8822-A3034BCB8391}];2 ;NumeroRE            ;Element ;Field   ;
                                                  DataType=Code;
                                                  SourceField=RVSExportLink::Exportation Register No. }

    { [{B44BF938-AA37-41A0-841F-BE778B96F962}];1 ;InfoComplementar    ;Element ;Text     }

    { [{3CE85F88-D612-4FCC-8B2B-6BE30C4E511E}];1 ;CodigoMoeda         ;Element ;Text     }

  }
  EVENTS
  {
  }
  REQUESTPAGE
  {
    PROPERTIES
    {
    }
    CONTROLS
    {
    }
  }
  CODE
  {

    BEGIN
    END.
  }
}

