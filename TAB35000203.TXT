OBJECT Table 35000203 Tax Posting Account2
{
  OBJECT-PROPERTIES
  {
    Date=30/10/12;
    Time=16:50:17;
    Version List=FNX068.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Tax Posting Accounts;
               PTB=Contas Contabiliza��o Impostos];
  }
  FIELDS
  {
    { 10  ;   ;Tax Identification  ;Option        ;OnValidate=BEGIN
                                                                VALIDATE("Filter Type", 0);
                                                                VALIDATE("Payable Account Type", 0);
                                                                VALIDATE("Expense Account Type", 0);
                                                                VALIDATE("Tax Credit Account Type", 0);
                                                              END;

                                                   CaptionML=[ENU=Tax Identification;
                                                              PTB=Identifica��o do imposto];
                                                   OptionCaptionML=[ENU=" ,IPI,ICMS,PIS,COFINS,II,IRRF,INSS,ISS,CSL Ret.,PIS Ret.,COFINS Ret.,ISS Ret.,INSS Ret.,Dif. Aliq. ICMS,ST";
                                                                    PTB=" ,IPI,ICMS,PIS,COFINS,II,IRRF,INSS,ISS,CSL Ret.,PIS Ret.,COFINS Ret.,ISS Ret.,INSS Ret.,Dif. Aliq. ICMS,ST"];
                                                   OptionString=[ ,IPI,ICMS,PIS,COFINS,II,IRRF,INSS,ISS,CSL Ret.,PIS Ret.,COFINS Ret.,ISS Ret.,INSS Ret.,Dif. Aliq. ICMS,ST] }
    { 20  ;   ;Filter Type         ;Option        ;OnValidate=BEGIN
                                                                "Filter Code" := '';
                                                              END;

                                                   CaptionML=[ENU=Filter Type;
                                                              PTB=Tipo Filtro];
                                                   OptionCaptionML=[ENU=" ,Gen. Bus. Posting Group,Gen. Prod. Posting Group,Customer,Vendor,Jurisdiction";
                                                                    PTB=" ,Grupo Cont�bil Neg�cio,Grupo Cont�bil Produto,Cliente,Fornecedor,Jurisdi��o"];
                                                   OptionString=[ ,GenBus,GenProd,Cust,Vend,Jurisdiction] }
    { 30  ;   ;Filter Code         ;Code20        ;TableRelation=IF (Filter Type=CONST(GenBus)) "Gen. Business Posting Group"
                                                                 ELSE IF (Filter Type=CONST(GenProd)) "Gen. Product Posting Group"
                                                                 ELSE IF (Filter Type=CONST(Cust)) Customer
                                                                 ELSE IF (Filter Type=CONST(Vend)) Vendor
                                                                 ELSE IF (Filter Type=CONST(Jurisdiction)) "Tax Jurisdiction";
                                                   CaptionML=[ENU=Filter Code;
                                                              PTB=C�d. Filtro] }
    { 1010;   ;Payable Account Type;Option        ;OnValidate=BEGIN
                                                                "Payable Account No." := '';
                                                              END;

                                                   CaptionML=[ENU=Payable Account Type;
                                                              PTB=Tipo Conta a Pagar];
                                                   OptionCaptionML=[ENU=" ,G/L Account,Customer,Vendor,Bank Account,Fixed Asset,IC Partner";
                                                                    PTB=" ,Conta Cont�bil,Cliente,Fornecedor,Conta Banc�ria,Ativo Fixo,Parceiro IC"];
                                                   OptionString=[ ,G/L Account,Customer,Vendor,Bank Account,Fixed Asset,IC Partner] }
    { 1012;   ;Payable Account No. ;Code20        ;TableRelation=IF (Payable Account Type=CONST(G/L Account)) "G/L Account"
                                                                 ELSE IF (Payable Account Type=CONST(Customer)) Customer
                                                                 ELSE IF (Payable Account Type=CONST(Vendor)) Vendor
                                                                 ELSE IF (Payable Account Type=CONST(Bank Account)) "Bank Account"
                                                                 ELSE IF (Payable Account Type=CONST(Fixed Asset)) "Fixed Asset"
                                                                 ELSE IF (Payable Account Type=CONST(IC Partner)) "IC Partner";
                                                   CaptionML=[ENU=Payable Account No.;
                                                              PTB=N� Conta a Pagar] }
    { 1020;   ;Expense Account Type;Option        ;OnValidate=BEGIN
                                                                "Expense Account No." := '';
                                                              END;

                                                   CaptionML=[ENU=Expense Account Type;
                                                              PTB=Tipo Conta Despesa];
                                                   OptionCaptionML=[ENU=" ,G/L Account,Customer,Vendor,Bank Account,Fixed Asset,IC Partner";
                                                                    PTB=" ,Conta Cont�bil,Cliente,Fornecedor,Conta Banc�ria,Ativo Fixo,Parceiro IC"];
                                                   OptionString=[ ,G/L Account,Customer,Vendor,Bank Account,Fixed Asset,IC Partner] }
    { 1022;   ;Expense Account No. ;Code20        ;TableRelation=IF (Expense Account Type=CONST(G/L Account)) "G/L Account"
                                                                 ELSE IF (Expense Account Type=CONST(Customer)) Customer
                                                                 ELSE IF (Expense Account Type=CONST(Vendor)) Vendor
                                                                 ELSE IF (Expense Account Type=CONST(Bank Account)) "Bank Account"
                                                                 ELSE IF (Expense Account Type=CONST(Fixed Asset)) "Fixed Asset"
                                                                 ELSE IF (Expense Account Type=CONST(IC Partner)) "IC Partner";
                                                   CaptionML=[ENU=Expense Account No.;
                                                              PTB=N� Conta Despesa] }
    { 1030;   ;Tax Credit Account Type;Option     ;OnValidate=BEGIN
                                                                "Tax Credit Account No." := '';
                                                              END;

                                                   CaptionML=[ENU=Tax Credit Account Type;
                                                              PTB=Tipo Conta a Recuperar];
                                                   OptionCaptionML=[ENU=" ,G/L Account,Customer,Vendor,Bank Account,Fixed Asset,IC Partner";
                                                                    PTB=" ,Conta Cont�bil,Cliente,Fornecedor,Conta Banc�ria,Ativo Fixo,Parceiro IC"];
                                                   OptionString=[ ,G/L Account,Customer,Vendor,Bank Account,Fixed Asset,IC Partner] }
    { 1032;   ;Tax Credit Account No.;Code20      ;TableRelation=IF (Tax Credit Account Type=CONST(G/L Account)) "G/L Account"
                                                                 ELSE IF (Tax Credit Account Type=CONST(Customer)) Customer
                                                                 ELSE IF (Tax Credit Account Type=CONST(Vendor)) Vendor
                                                                 ELSE IF (Tax Credit Account Type=CONST(Bank Account)) "Bank Account"
                                                                 ELSE IF (Tax Credit Account Type=CONST(Fixed Asset)) "Fixed Asset"
                                                                 ELSE IF (Tax Credit Account Type=CONST(IC Partner)) "IC Partner";
                                                   CaptionML=[ENU=Tax Credit Account No.;
                                                              PTB=N� Conta a Recuperar] }
  }
  KEYS
  {
    {    ;Tax Identification,Filter Type,Filter Code;
                                                   Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      Text001@35000000 : TextConst 'ENU=A tax account setup for the tax %1 wasn''t found.;PTB=N�o foi poss�vel encontrar uma configura��o de contas para o imposto %1.';

    PROCEDURE FindAccounts@35000000(taxID@35000005 : Integer;jurisdictionCode@35000000 : Code[20];vendorNo@35000001 : Code[20];customerNo@35000002 : Code[20];genProdPostGrp@35000003 : Code[20];genBusPostGrp@35000004 : Code[20]);
    BEGIN
      IF jurisdictionCode <> '' THEN BEGIN
        RESET;
        SETRANGE("Tax Identification", taxID);
        SETRANGE("Filter Type", "Filter Type"::Jurisdiction);
        SETRANGE("Filter Code", jurisdictionCode);
        IF FINDFIRST THEN
          EXIT;
      END;
      IF vendorNo <> '' THEN BEGIN
        RESET;
        SETRANGE("Tax Identification", taxID);
        SETRANGE("Filter Type", "Filter Type"::Vend);
        SETRANGE("Filter Code", vendorNo);
        IF FINDFIRST THEN
          EXIT;
      END;
      IF customerNo <> '' THEN BEGIN
        RESET;
        SETRANGE("Tax Identification", taxID);
        SETRANGE("Filter Type", "Filter Type"::Cust);
        SETRANGE("Filter Code", customerNo);
        IF FINDFIRST THEN
          EXIT;
      END;
      IF genProdPostGrp <> '' THEN BEGIN
        RESET;
        SETRANGE("Tax Identification", taxID);
        SETRANGE("Filter Type", "Filter Type"::GenProd);
        SETRANGE("Filter Code", genProdPostGrp);
        IF FINDFIRST THEN
          EXIT;
      END;
      IF genBusPostGrp <> '' THEN BEGIN
        RESET;
        SETRANGE("Tax Identification", taxID);
        SETRANGE("Filter Type", "Filter Type"::GenBus);
        SETRANGE("Filter Code", genBusPostGrp);
        IF FINDFIRST THEN
          EXIT;
      END;

      RESET;
      SETRANGE("Tax Identification", taxID);
      SETRANGE("Filter Type", "Filter Type"::" ");
      SETRANGE("Filter Code", '');
      IF NOT FINDFIRST THEN
        ERROR(Text001, GETFILTER("Tax Identification"));
    END;

    BEGIN
    END.
  }
}

