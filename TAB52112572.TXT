OBJECT Table 52112572 Alternate Taxpayer
{
  OBJECT-PROPERTIES
  {
    Date=30/04/13;
    Time=18:14:18;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Alternate Taxpayer;
               PTB=Contribuinte Substituto];
  }
  FIELDS
  {
    { 1   ;   ;UF                  ;Code2         ;TableRelation=Territory;
                                                   CaptionML=PTB=Unidade Federa��o }
    { 2   ;   ;State Inscr.        ;Code20        ;CaptionML=PTB=Inscr. Estadual }
    { 44  ;   ;Tax Settlement Code ;Code20        ;CaptionML=[ENU=Tax Settlement Code;
                                                              PTB=C�d. Apura��o Imp.] }
  }
  KEYS
  {
    {    ;UF,State Inscr.                         ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

