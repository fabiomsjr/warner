OBJECT Table 52112585 Tax Credit Code
{
  OBJECT-PROPERTIES
  {
    Date=23/08/13;
    Time=13:21:04;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Tax Credit Code;
               PTB=C�d. Cr�dito Imposto];
    LookupPageID=Page52112568;
    DrillDownPageID=Page52112568;
  }
  FIELDS
  {
    { 10  ;   ;Tax Identification  ;Option        ;CaptionML=[ENU=Tax Identification;
                                                              PTB=Identifica��o Imposto];
                                                   OptionCaptionML=[ENU=" ,IPI,ICMS,PIS,COFINS,II,IRRF,INSS,ISS,CSL Ret.,PIS Ret.,COFINS Ret.,ISS Ret.,INSS Ret.,Dif. Aliq. ICMS,ST,PCC,CIDE,Funrural,Simples";
                                                                    PTB=" ,IPI,ICMS,PIS,COFINS,II,IRRF,INSS,ISS,CSL Ret.,PIS Ret.,COFINS Ret.,ISS Ret.,INSS Ret.,Dif. Aliq. ICMS,ST,PCC,CIDE,Funrural,Simples"];
                                                   OptionString=[ ,IPI,ICMS,PIS,COFINS,II,IRRF,INSS,ISS,CSL Ret.,PIS Ret.,COFINS Ret.,ISS Ret.,INSS Ret.,Dif. Aliq. ICMS,ST,PCC,CIDE,Funrural,Simples] }
    { 40  ;   ;Code                ;Code20        ;CaptionML=[ENU=Code;
                                                              PTB=C�digo] }
    { 50  ;   ;Description         ;Text100       ;CaptionML=[ENU=Description;
                                                              PTB=Descri��o] }
  }
  KEYS
  {
    {    ;Tax Identification,Code                 ;Clustered=Yes }
  }
  FIELDGROUPS
  {
    { 1   ;DropDown            ;Code,Description                         }
  }
  CODE
  {

    BEGIN
    END.
  }
}

