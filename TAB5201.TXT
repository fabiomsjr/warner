OBJECT Table 5201 Alternative Address
{
  OBJECT-PROPERTIES
  {
    Date=23/09/13;
    Time=12:00:00;
    Version List=NAVW17.10,NAVBR7;
  }
  PROPERTIES
  {
    DataCaptionFields=Employee No.,Name,Code;
    CaptionML=[ENU=Alternative Address;
               PTB=Endere�o Alternativo];
    LookupPageID=Page5204;
    DrillDownPageID=Page5204;
  }
  FIELDS
  {
    { 1   ;   ;Employee No.        ;Code20        ;TableRelation=Employee;
                                                   OnValidate=BEGIN
                                                                Employee.GET("Employee No.");
                                                                Name := Employee."Last Name";
                                                              END;

                                                   CaptionML=[ENU=Employee No.;
                                                              PTB=N� Empregado];
                                                   NotBlank=Yes }
    { 2   ;   ;Code                ;Code10        ;CaptionML=[ENU=Code;
                                                              PTB=C�digo];
                                                   NotBlank=Yes }
    { 3   ;   ;Name                ;Text50        ;CaptionML=[ENU=Name;
                                                              PTB=Nome] }
    { 4   ;   ;Name 2              ;Text50        ;CaptionML=[ENU=Name 2;
                                                              PTB=Nome Complementar] }
    { 5   ;   ;Address             ;Text50        ;CaptionML=[ENU=Address;
                                                              PTB=Endere�o] }
    { 6   ;   ;Address 2           ;Text50        ;CaptionML=[ENU=Address 2;
                                                              PTB=Endere�o Complementar] }
    { 7   ;   ;City                ;Text30        ;TableRelation=IF (Country/Region Code=CONST()) "Post Code".City
                                                                 ELSE IF (Country/Region Code=FILTER(<>'')) "Post Code".City WHERE (Country/Region Code=FIELD(Country/Region Code));
                                                   OnValidate=BEGIN
                                                                // > NAVBR
                                                                //PostCode.ValidateCity(City,"Post Code",County,"Country/Region Code",(CurrFieldNo <> 0) AND GUIALLOWED);
                                                                // < NAVBR
                                                              END;

                                                   ValidateTableRelation=No;
                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=City;
                                                              PTB=Cidade] }
    { 8   ;   ;Post Code           ;Code20        ;TableRelation="Post Code";
                                                   OnValidate=BEGIN
                                                                // > NAVBR
                                                                //PostCode.ValidatePostCode(City,"Post Code",County,"Country/Region Code",(CurrFieldNo <> 0) AND GUIALLOWED);
                                                                PostCode.ValidatePostCodeBR("Post Code", City, Address, County, District, "Territory Code", "Country/Region Code");
                                                                // < NAVBR
                                                              END;

                                                   CaptionML=[ENU=Post Code;
                                                              PTB=CEP] }
    { 9   ;   ;County              ;Text30        ;CaptionML=[ENU=County;
                                                              PTB=Regi�o] }
    { 10  ;   ;Phone No.           ;Text30        ;ExtendedDatatype=Phone No.;
                                                   CaptionML=[ENU=Phone No.;
                                                              PTB=Telefone] }
    { 11  ;   ;Fax No.             ;Text30        ;CaptionML=[ENU=Fax No.;
                                                              PTB=N� Fax] }
    { 12  ;   ;E-Mail              ;Text80        ;ExtendedDatatype=E-Mail;
                                                   CaptionML=[ENU=E-Mail;
                                                              PTB=E-Mail] }
    { 13  ;   ;Comment             ;Boolean       ;FieldClass=FlowField;
                                                   CalcFormula=Exist("Human Resource Comment Line" WHERE (Table Name=CONST(Alternative Address),
                                                                                                          No.=FIELD(Employee No.),
                                                                                                          Alternative Address Code=FIELD(Code)));
                                                   CaptionML=[ENU=Comment;
                                                              PTB=Coment�rio];
                                                   Editable=No }
    { 14  ;   ;Country/Region Code ;Code10        ;TableRelation=Country/Region;
                                                   CaptionML=[ENU=Country/Region Code;
                                                              PTB=C�d. Pa�s] }
    { 52112430;;District           ;Text30        ;CaptionML=[ENU=District;
                                                              PTB=Bairro] }
    { 52112440;;Territory Code     ;Code10        ;TableRelation=Territory;
                                                   CaptionML=[ENU=Territory Code;
                                                              PTB=C�d. Unidade Federal] }
  }
  KEYS
  {
    {    ;Employee No.,Code                       ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      PostCode@1000 : Record 225;
      Employee@1001 : Record 5200;

    BEGIN
    END.
  }
}

