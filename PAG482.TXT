OBJECT Page 482 Vendor Stats. by Currencies
{
  OBJECT-PROPERTIES
  {
    Date=30/04/13;
    Time=12:00:00;
    Version List=NAVW17.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Vendor Stats. by Currencies;
               PTB=Estat. Fornecedor por Moedas];
    InsertAllowed=No;
    DeleteAllowed=No;
    ModifyAllowed=No;
    LinksAllowed=No;
    SourceTable=Table23;
    PageType=ListPlus;
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Part      ;
                SubPageLink=Vendor Filter=FIELD(No.),
                            Global Dimension 1 Filter=FIELD(Global Dimension 1 Filter),
                            Global Dimension 2 Filter=FIELD(Global Dimension 2 Filter),
                            Date Filter=FIELD(Date Filter);
                PagePartID=Page487 }

  }
  CODE
  {

    BEGIN
    END.
  }
}

