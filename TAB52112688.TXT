OBJECT Table 52112688 Siscoserv RAS Import. Link
{
  OBJECT-PROPERTIES
  {
    Date=21/07/15;
    Time=15:05:40;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Goods Importation Link;
               PTB=V�ncula��o � Importa��o de Bens];
  }
  FIELDS
  {
    { 10  ;   ;RAS No.             ;Code20        ;TableRelation="Siscoserv RAS";
                                                   CaptionML=[ENU=RAS No.;
                                                              PTB=N� RAS] }
    { 30  ;   ;DI No.              ;Code20        ;CaptionML=[ENU=DI No.;
                                                              PTB=N� DI] }
  }
  KEYS
  {
    {    ;RAS No.,DI No.                          ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      RVSAlreadyExistsErr@52006500 : TextConst 'ENU=Siscoserv RVS %1 already exists.;PTB=RVS Siscoserv %1 j� existe.';

    BEGIN
    END.
  }
}

