OBJECT Table 80028 Post Code_UPG
{
  OBJECT-PROPERTIES
  {
    Date=13/05/16;
    Time=16:17:43;
    Modified=Yes;
    Version List=UPG;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Post Code;
               PTB=CEP];
    LookupPageID=Page367;
  }
  FIELDS
  {
    { 1   ;   ;Code                ;Code20        ;OnValidate=VAR
                                                                PostCode@1000 : Record 225;
                                                              BEGIN
                                                                PostCode.SETCURRENTKEY("Search City");
                                                                PostCode.SETRANGE("Search City","Search City");
                                                                PostCode.SETRANGE(Code,Code);
                                                                IF PostCode.FIND('-') THEN
                                                                  ERROR(Text000,FIELDCAPTION(Code),Code);
                                                              END;

                                                   CaptionML=[ENU=Code;
                                                              PTB=C�digo];
                                                   NotBlank=Yes }
    { 2   ;   ;City                ;Text30        ;OnValidate=VAR
                                                                PostCode@1000 : Record 225;
                                                              BEGIN
                                                                TESTFIELD(Code);
                                                                "Search City" := City;
                                                                IF xRec."Search City" <> "Search City" THEN BEGIN
                                                                  PostCode.SETCURRENTKEY("Search City");
                                                                  PostCode.SETRANGE("Search City","Search City");
                                                                  PostCode.SETRANGE(Code,Code);
                                                                  IF PostCode.FIND('-') THEN
                                                                    ERROR(Text000,FIELDCAPTION(City),City);
                                                                END;
                                                              END;

                                                   CaptionML=[ENU=City;
                                                              PTB=Cidade] }
    { 3   ;   ;Search City         ;Code30        ;CaptionML=[ENU=Search City;
                                                              PTB=Cidade Procura] }
    { 35000000;;District           ;Text30        ;CaptionML=[ENU=District;
                                                              PTB=Bairro];
                                                   Description=NAVBR5.01.0109 }
    { 35000001;;Territory Code     ;Code10        ;TableRelation=Territory;
                                                   CaptionML=PTB=C�d. Territ�rio;
                                                   Description=NAVBR5.01.0109 }
    { 35000002;;Address            ;Text50        ;CaptionML=PTB=Endere�o;
                                                   Description=NAVBR5.01.0109 }
    { 35000003;;Country Code       ;Code10        ;TableRelation=Country/Region;
                                                   CaptionML=PTB=C�d. Pa�s;
                                                   Description=NAVBR5.01.0109 }
    { 35000700;;IBGE City Code     ;Code20        ;TableRelation=Municipio2.Code WHERE (Territory Code=FIELD(Territory Code));
                                                   OnValidate=VAR
                                                                municipio@1102300000 : Record 35000712;
                                                              BEGIN
                                                                IF municipio.GET("IBGE City Code") THEN
                                                                  BEGIN
                                                                    City := municipio.City;
                                                                    "Country Code" := 'BR';
                                                                  END;
                                                              END;

                                                   ValidateTableRelation=No;
                                                   CaptionML=PTB=C�d. Municipio IBGE;
                                                   Description=NAVBR5.01.0109 }
  }
  KEYS
  {
    {    ;Code                                    ;Clustered=Yes }
    {    ;City,Code                                }
    {    ;Search City                              }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      Text000@1000 : TextConst 'ENU=%1 %2 already exists.;PTB=%1 %2 j� existem.';

    PROCEDURE ValidateCity@5(VAR City@1000 : Text[30];VAR PostCode@1001 : Code[20]);
    VAR
      PostCodeRec@1002 : Record 225;
      PostCodeRec2@1004 : Record 225;
      SearchCity@1003 : Code[30];
    BEGIN
    END;

    PROCEDURE ValidatePostCode@6(VAR City@1001 : Text[30];VAR PostCode@1000 : Code[20]);
    VAR
      PostCodeRec@1002 : Record 225;
      PostCodeRec2@1003 : Record 225;
    BEGIN
    END;

    PROCEDURE LookUpCity@7(VAR City@1001 : Text[30];VAR PostCode@1000 : Code[20];ReturnValues@1002 : Boolean);
    VAR
      PostCodeRec@1003 : Record 225;
    BEGIN
    END;

    PROCEDURE LookUpPostCode@8(VAR City@1001 : Text[30];VAR PostCode@1000 : Code[20];ReturnValues@1002 : Boolean);
    VAR
      PostCodeRec@1003 : Record 225;
    BEGIN
    END;

    BEGIN
    END.
  }
}

