OBJECT Table 377 Object Translation
{
  OBJECT-PROPERTIES
  {
    Date=07/09/12;
    Time=12:00:00;
    Version List=NAVW17.00;
  }
  PROPERTIES
  {
    DataPerCompany=No;
    CaptionML=[ENU=Object Translation;
               PTB=Tradu��o Objeto];
  }
  FIELDS
  {
    { 1   ;   ;Object Type         ;Option        ;CaptionML=[ENU=Object Type;
                                                              PTB=Tipo objeto];
                                                   OptionCaptionML=[ENU=" ,Table,Form,Report,,Codeunit,XMLPort,MenuSuite,Page";
                                                                    PTB=" ,Tabela,Formul�rio,Relat�rio,Dataport,Codeunit"];
                                                   OptionString=[ ,Table,Form,Report,,Codeunit,XMLPort,MenuSuite,Page] }
    { 2   ;   ;Object ID           ;Integer       ;TableRelation=IF (Object Type=FILTER(>' ')) AllObj."Object ID" WHERE (Object Type=FIELD(Object Type));
                                                   CaptionML=[ENU=Object ID;
                                                              PTB=Id. Objeto];
                                                   NotBlank=Yes }
    { 3   ;   ;Language ID         ;Integer       ;TableRelation="Windows Language";
                                                   OnValidate=BEGIN
                                                                CALCFIELDS("Language Name");
                                                              END;

                                                   CaptionML=[ENU=Language ID;
                                                              PTB=ID Idioma];
                                                   NotBlank=Yes;
                                                   BlankZero=Yes }
    { 4   ;   ;Language Name       ;Text80        ;FieldClass=FlowField;
                                                   CalcFormula=Lookup("Windows Language".Name WHERE (Language ID=FIELD(Language ID)));
                                                   CaptionML=[ENU=Language Name;
                                                              PTB=Nome Idioma];
                                                   Editable=No }
    { 5   ;   ;Description         ;Text250       ;CaptionML=[ENU=Description;
                                                              PTB=Descri��o] }
    { 6   ;   ;Object Name         ;Text30        ;FieldClass=FlowField;
                                                   CalcFormula=Lookup(Object.Name WHERE (Type=FIELD(Object Type),
                                                                                         ID=FIELD(Object ID)));
                                                   CaptionML=[ENU=Object Name;
                                                              PTB=Nome Objeto];
                                                   Editable=No }
  }
  KEYS
  {
    {    ;Object Type,Object ID,Language ID       ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    PROCEDURE TranslateObject@2(ObjectType@1000 : ' ,Table,Form,Report,,Codeunit,XMLPort,MenuSuite,Page';ObjectID@1001 : Integer) : Text[250];
    VAR
      Object@1002 : Record 2000000058;
    BEGIN
      IF Object.GET(ObjectType,ObjectID) THEN
        EXIT(Object."Object Caption");
    END;

    BEGIN
    END.
  }
}

