OBJECT Page 1215 Post Exch Line Def Part
{
  OBJECT-PROPERTIES
  {
    Date=09/09/14;
    Time=12:00:00;
    Version List=NAVW18.00;
  }
  PROPERTIES
  {
    CaptionML=ENU=Post Exch Line Def Part;
    SourceTable=Table1227;
    PageType=ListPart;
    CardPageID=Post Exch Mapping Card;
    OnAfterGetCurrRecord=VAR
                           PostingExchDef@1000 : Record 1222;
                         BEGIN
                           PostingExchDef.GET("Posting Exch. Def Code");
                           IsBankStatementImportType := PostingExchDef.CheckEnableDisableIsBankStatementImportType;
                           IsXMLFileType := NOT PostingExchDef.CheckEnableDisableIsNonXMLFileType;
                           IF (NOT IsXMLFileType) OR (NOT IsBankStatementImportType) THEN
                             Namespace := '';
                         END;

    ActionList=ACTIONS
    {
      { 7       ;    ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 9       ;1   ;Action    ;
                      Name=Field Mapping;
                      CaptionML=ENU=Field Mapping;
                      RunObject=Page 1214;
                      RunPageLink=Posting Exch. Def Code=FIELD(Posting Exch. Def Code),
                                  Posting Exch. Line Def Code=FIELD(Code);
                      Promoted=No;
                      PromotedIsBig=Yes;
                      Image=MapAccounts;
                      RunPageMode=Edit }
    }
  }
  CONTROLS
  {
    { 1   ;0   ;Container ;
                ContainerType=ContentArea }

    { 2   ;1   ;Group     ;
                Name=Group;
                CaptionML=ENU=Group;
                GroupType=Repeater }

    { 4   ;2   ;Field     ;
                SourceExpr=Code }

    { 5   ;2   ;Field     ;
                SourceExpr=Name }

    { 6   ;2   ;Field     ;
                SourceExpr="Column Count" }

    { 8   ;2   ;Field     ;
                SourceExpr="Data Line Tag" }

    { 3   ;2   ;Field     ;
                SourceExpr=Namespace;
                Editable=IsXMLFileType AND IsBankStatementImportType }

  }
  CODE
  {
    VAR
      IsBankStatementImportType@1001 : Boolean;
      IsXMLFileType@1000 : Boolean;

    BEGIN
    END.
  }
}

