OBJECT Page 146 Posted Purchase Invoices
{
  OBJECT-PROPERTIES
  {
    Date=13/10/14;
    Time=16:25:16;
    Modified=Yes;
    Version List=NAVW18.00,NAVBR7,FNX043.00,WAR003.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Posted Purchase Invoices;
               PTB=Hist�rico Notas Fiscais Compras];
    SourceTable=Table122;
    PageType=List;
    CardPageID=Posted Purchase Invoice;
    OnOpenPage=BEGIN
                 SetSecurityFilterOnRespCenter;
               END;

    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 21      ;1   ;ActionGroup;
                      CaptionML=[ENU=&Invoice;
                                 PTB=&Nota Fiscal];
                      Image=Invoice }
      { 29      ;2   ;Action    ;
                      ShortCutKey=F7;
                      CaptionML=[ENU=Statistics;
                                 PTB=Estat�sticas];
                      RunObject=Page 400;
                      RunPageLink=No.=FIELD(No.);
                      Promoted=Yes;
                      Image=Statistics;
                      PromotedCategory=Process }
      { 30      ;2   ;Action    ;
                      CaptionML=[ENU=Co&mments;
                                 PTB=Co&ment�rios];
                      RunObject=Page 66;
                      RunPageLink=Document Type=CONST(Posted Invoice),
                                  No.=FIELD(No.);
                      Image=ViewComments }
      { 1102601000;2 ;Action    ;
                      AccessByPermission=TableData 348=R;
                      ShortCutKey=Shift+Ctrl+D;
                      CaptionML=[ENU=Dimensions;
                                 PTB=Dimens�es];
                      Image=Dimensions;
                      OnAction=BEGIN
                                 ShowDimensions;
                               END;
                                }
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 22      ;1   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=ENU=&Print;
                      Promoted=Yes;
                      Image=Print;
                      PromotedCategory=Process;
                      OnAction=VAR
                                 PurchInvHeader@1102 : Record 122;
                               BEGIN
                                 CurrPage.SETSELECTIONFILTER(PurchInvHeader);
                                 PurchInvHeader.PrintRecords(TRUE);
                               END;
                                }
      { 27      ;1   ;Action    ;
                      CaptionML=ENU=&Navigate;
                      Promoted=Yes;
                      Image=Navigate;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 Navigate;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 4   ;2   ;Field     ;
                SourceExpr="No." }

    { 2   ;2   ;Field     ;
                SourceExpr="Buy-from Vendor No." }

    { 15  ;2   ;Field     ;
                SourceExpr="Order Address Code";
                Visible=FALSE }

    { 6   ;2   ;Field     ;
                SourceExpr="Buy-from Vendor Name" }

    { 35100000;2;Field    ;
                SourceExpr="Posting Text" }

    { 52006503;2;Field    ;
                SourceExpr="Branch Code" }

    { 52006501;2;Field    ;
                SourceExpr="Fiscal Document Type" }

    { 1102300000;2;Field  ;
                SourceExpr="Vendor Invoice No." }

    { 35000004;2;Field    ;
                SourceExpr="Quote No." }

    { 35000005;2;Field    ;
                SourceExpr="Order No." }

    { 52006500;2;Field    ;
                SourceExpr="Order Date";
                Visible=FALSE }

    { 35  ;2   ;Field     ;
                SourceExpr="Currency Code" }

    { 13  ;2   ;Field     ;
                SourceExpr=Amount;
                OnDrillDown=BEGIN
                              SETRANGE("No.");
                              PAGE.RUNMODAL(PAGE::"Posted Purchase Invoice",Rec)
                            END;
                             }

    { 17  ;2   ;Field     ;
                SourceExpr="Amount Including VAT";
                OnDrillDown=BEGIN
                              SETRANGE("No.");
                              PAGE.RUNMODAL(PAGE::"Posted Purchase Invoice",Rec)
                            END;
                             }

    { 31  ;2   ;Field     ;
                SourceExpr="Buy-from Post Code";
                Visible=FALSE }

    { 25  ;2   ;Field     ;
                SourceExpr="Buy-from Country/Region Code";
                Visible=FALSE }

    { 39  ;2   ;Field     ;
                SourceExpr="Buy-from Contact";
                Visible=FALSE }

    { 137 ;2   ;Field     ;
                SourceExpr="Pay-to Vendor No.";
                Visible=FALSE }

    { 135 ;2   ;Field     ;
                SourceExpr="Pay-to Name";
                Visible=FALSE }

    { 37  ;2   ;Field     ;
                SourceExpr="Pay-to Post Code";
                Visible=FALSE }

    { 33  ;2   ;Field     ;
                SourceExpr="Pay-to Country/Region Code";
                Visible=FALSE }

    { 125 ;2   ;Field     ;
                SourceExpr="Pay-to Contact";
                Visible=FALSE }

    { 121 ;2   ;Field     ;
                SourceExpr="Ship-to Code";
                Visible=FALSE }

    { 119 ;2   ;Field     ;
                SourceExpr="Ship-to Name";
                Visible=FALSE }

    { 23  ;2   ;Field     ;
                SourceExpr="Ship-to Post Code";
                Visible=FALSE }

    { 19  ;2   ;Field     ;
                SourceExpr="Ship-to Country/Region Code";
                Visible=FALSE }

    { 109 ;2   ;Field     ;
                SourceExpr="Ship-to Contact";
                Visible=FALSE }

    { 105 ;2   ;Field     ;
                SourceExpr="Posting Date";
                Visible=FALSE }

    { 73  ;2   ;Field     ;
                SourceExpr="Purchaser Code";
                Visible=FALSE }

    { 87  ;2   ;Field     ;
                SourceExpr="Shortcut Dimension 1 Code";
                Visible=FALSE }

    { 85  ;2   ;Field     ;
                SourceExpr="Shortcut Dimension 2 Code";
                Visible=FALSE }

    { 89  ;2   ;Field     ;
                SourceExpr="Location Code";
                Visible=TRUE }

    { 8   ;2   ;Field     ;
                SourceExpr="No. Printed";
                Visible=FALSE }

    { 1102601001;2;Field  ;
                SourceExpr="Document Date";
                Visible=FALSE }

    { 1102601003;2;Field  ;
                SourceExpr="Payment Terms Code";
                Visible=FALSE }

    { 1102601005;2;Field  ;
                SourceExpr="Due Date";
                Visible=FALSE }

    { 1102601007;2;Field  ;
                SourceExpr="Payment Discount %";
                Visible=FALSE }

    { 1102601009;2;Field  ;
                SourceExpr="Payment Method Code";
                Visible=FALSE }

    { 1102601011;2;Field  ;
                SourceExpr="Shipment Method Code";
                Visible=FALSE }

    { 35000003;2;Field    ;
                DrillDown=No;
                SourceExpr="Credit Memos" }

    { 52006504;2;Field    ;
                SourceExpr="Return Invoices" }

    { 52006505;2;Field    ;
                SourceExpr="Issue Date/Time" }

    { 35000002;2;Field    ;
                SourceExpr="NFe cStat" }

    { 35000001;2;Field    ;
                SourceExpr="NFe xMotivo" }

    { 35000000;2;Field    ;
                SourceExpr="NFe Chave Acesso" }

    { 52006502;2;Field    ;
                SourceExpr="Beneficiary CPFCNPJ";
                Importance=Additional;
                Visible=FALSE;
                Editable=FALSE }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=TRUE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

