OBJECT Codeunit 52113164 Time Sheet Management Hook
{
  OBJECT-PROPERTIES
  {
    Date=14/11/14;
    Time=13:12:58;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    OnRun=BEGIN
          END;

  }
  CODE
  {
    VAR
      TSHasAssocServLines@52006500 : TextConst 'ENU=Time sheet line cannot be reopened because there are linked service lines.;PTB=A linha de apontamento n�o pode ser reaberta pois existem linhas de servi�o associadas.';

    PROCEDURE ShouldCreateServDocLinesFromTS@27(ServiceHeader@1000 : Record 5900) : Boolean;
    VAR
      resSetup@52006502 : Record 314;
      servItemNo@52006500 : Code[20];
      timeSheetHeader@1002 : Record 950;
      timeSheetLine@1001 : Record 951;
      timeSheetDetail@1007 : Record 952;
      serviceLine@1003 : Record 5902;
      lineNo@1004 : Integer;
      qtyToPost@1005 : Decimal;
      serviceLineTS@52006501 : Record 52112485;
      tmpServLine@52006503 : TEMPORARY Record 5902;
    BEGIN
      resSetup.GET;
      IF NOT resSetup."Group TS Service Lines" THEN
        EXIT(TRUE);

      servItemNo := GetFirstServiceItemNo(ServiceHeader);

      serviceLine.SETRANGE("Document Type",ServiceHeader."Document Type");
      serviceLine.SETRANGE("Document No.",ServiceHeader."No.");
      IF serviceLine.FINDLAST THEN
        lineNo := serviceLine."Line No.";

      timeSheetDetail.SETRANGE("Service Order No.", ServiceHeader."No.");
      timeSheetDetail.SETRANGE(Status, timeSheetDetail.Status::Approved);
      timeSheetDetail.SETRANGE(Posted, FALSE);
      IF timeSheetDetail.FINDSET THEN
        REPEAT
          serviceLineTS.RESET;
          serviceLineTS.SETRANGE("Time Sheet No.", timeSheetDetail."Time Sheet No.");
          serviceLineTS.SETRANGE("Time Sheet Line No.", timeSheetDetail."Time Sheet Line No.");
          serviceLineTS.SETRANGE("Time Sheet Date", timeSheetDetail.Date);
          IF serviceLineTS.ISEMPTY THEN BEGIN
            qtyToPost := timeSheetDetail.GetMaxQtyToPost;
            IF qtyToPost <> 0 THEN BEGIN
              timeSheetHeader.GET(timeSheetDetail."Time Sheet No.");
              timeSheetLine.GET(timeSheetDetail."Time Sheet No.", timeSheetDetail."Time Sheet Line No.");

              tmpServLine.RESET;
              tmpServLine.SETRANGE("No.", timeSheetHeader."Resource No.");
              tmpServLine.SETRANGE("Work Type Code", timeSheetLine."Work Type Code");
              IF NOT tmpServLine.FINDFIRST THEN BEGIN
                lineNo += 10000;
                tmpServLine.INIT;
                tmpServLine."No." := timeSheetHeader."Resource No.";
                tmpServLine."Work Type Code" := timeSheetLine."Work Type Code";
                tmpServLine."Line No." := lineNo;
                tmpServLine.INSERT;
              END;

              tmpServLine.Quantity += timeSheetDetail.Quantity;
              IF NOT timeSheetLine.Chargeable THEN
                tmpServLine."Qty. to Consume" += qtyToPost;
              tmpServLine.MODIFY;

              serviceLineTS.INIT;
              serviceLineTS."Document Type" := ServiceHeader."Document Type";
              serviceLineTS."Document No." := ServiceHeader."No.";
              serviceLineTS."Document Line No." := tmpServLine."Line No.";
              serviceLineTS."Time Sheet No." := timeSheetDetail."Time Sheet No.";
              serviceLineTS."Time Sheet Line No." := timeSheetDetail."Time Sheet Line No.";
              serviceLineTS."Time Sheet Date" := timeSheetDetail.Date;
              serviceLineTS.INSERT;
            END;
          END;
        UNTIL timeSheetDetail.NEXT = 0;

      serviceLine.RESET;
      tmpServLine.RESET;
      IF tmpServLine.FINDSET THEN
        REPEAT
          serviceLine.INIT;
          serviceLine."Document Type" := ServiceHeader."Document Type";
          serviceLine."Document No." := ServiceHeader."No.";
          serviceLine."Line No." := tmpServLine."Line No.";
          serviceLine.VALIDATE("Service Item No.", servItemNo);
          serviceLine.Type := serviceLine.Type::Resource;
          serviceLine.VALIDATE("No.", tmpServLine."No.");
          serviceLine.VALIDATE(Quantity, tmpServLine.Quantity);
          IF tmpServLine."Qty. to Consume" <> 0 THEN
            serviceLine.VALIDATE("Qty. to Consume", tmpServLine."Qty. to Consume");
          serviceLine.VALIDATE("Work Type Code", tmpServLine."Work Type Code");
          serviceLine.INSERT;
        UNTIL tmpServLine.NEXT = 0;

      EXIT(FALSE);
    END;

    PROCEDURE ShouldCreateTSLineFromServiceLine@29(serviceLine@1000 : Record 5902;documentNo@1001 : Code[20];chargeable@1002 : Boolean) : Boolean;
    VAR
      servLineTS@52006501 : Record 52112485;
    BEGIN
      servLineTS.SETRANGE("Document Type", serviceLine."Document Type");
      servLineTS.SETRANGE("Document No.", serviceLine."Document No.");
      servLineTS.SETRANGE("Document Line No.", serviceLine."Line No.");
      EXIT(servLineTS.ISEMPTY);
    END;

    PROCEDURE ApprovalCheckLinkedServiceDoc@8(TimeSheetLine@1000 : Record 951);
    VAR
      servLineTS@52006500 : Record 52112485;
    BEGIN
      servLineTS.SETRANGE("Document Type", servLineTS."Document Type"::Order);
      servLineTS.SETRANGE("Document No.", TimeSheetLine."Service Order No.");
      servLineTS.SETRANGE("Time Sheet No.", TimeSheetLine."Time Sheet No.");
      servLineTS.SETRANGE("Time Sheet Line No.", TimeSheetLine."Line No.");
      IF NOT servLineTS.ISEMPTY THEN
        ERROR(TSHasAssocServLines);
    END;

    LOCAL PROCEDURE _@52006501();
    BEGIN
    END;

    LOCAL PROCEDURE GetFirstServiceItemNo@28(ServiceHeader@1000 : Record 5900) : Code[20];
    VAR
      ServiceItemLine@1001 : Record 5901;
    BEGIN
      ServiceItemLine.SETRANGE("Document Type", ServiceHeader."Document Type");
      ServiceItemLine.SETRANGE("Document No.", ServiceHeader."No.");
      ServiceItemLine.FINDFIRST;
      EXIT(ServiceItemLine."Service Item No.");
    END;

    BEGIN
    END.
  }
}

