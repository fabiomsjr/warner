OBJECT Page 9171 Profile List
{
  OBJECT-PROPERTIES
  {
    Date=09/09/14;
    Time=12:00:00;
    Version List=NAVW18.00.00.41779,NAVBR7;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Profile List;
               PTB=Lista Perfil];
    SourceTable=Table2000000072;
    PageType=List;
    CardPageID=Profile Card;
    PromotedActionCategoriesML=ENU=New,Process,Report,Resource Translation;
    OnOpenPage=VAR
                 FileManagement@1000 : Codeunit 419;
               BEGIN
                 CanRunDotNetOnClient := FileManagement.CanRunDotNetOnClient
               END;

    ActionList=ACTIONS
    {
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 1102601007;1 ;ActionGroup;
                      CaptionML=[ENU=F&unctions;
                                 PTB=F&un��es];
                      Image=Action }
      { 1102601008;2 ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[ENU=Copy Profile;
                                 PTB=Copiar Perfil];
                      Promoted=Yes;
                      Image=Copy;
                      OnAction=VAR
                                 Profile@1035 : Record 2000000072;
                                 CopyProfile@1034 : Report 9170;
                               BEGIN
                                 Profile.SETRANGE("Profile ID","Profile ID");
                                 CopyProfile.SETTABLEVIEW(Profile);
                                 CopyProfile.RUNMODAL;

                                 IF GET(CopyProfile.GetProfileID) THEN;
                               END;
                                }
      { 52006500;2   ;Action    ;
                      Name=ActionConfigure;
                      CaptionML=[ENU=Configure;
                                 PTB=Configurar];
                      Promoted=Yes;
                      Image=UserSetup;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 ActionConfigure;
                               END;
                                }
      { 52006501;2   ;Action    ;
                      Name=ActionTest;
                      CaptionML=[ENU=Test;
                                 PTB=Testar];
                      Promoted=Yes;
                      Image=UserInterface;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 ActionTest;
                               END;
                                }
      { 4       ;2   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=ENU=Import Profile;
                      Promoted=Yes;
                      Image=Import;
                      OnAction=BEGIN
                                 COMMIT;
                                 REPORT.RUNMODAL(REPORT::"Import Profiles",FALSE);
                                 COMMIT;
                               END;
                                }
      { 14      ;1   ;ActionGroup;
                      CaptionML=ENU=Resource Translation }
      { 6       ;2   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=ENU=Import Translated Profile Resources From Folder;
                      Promoted=Yes;
                      Visible=CanRunDotNetOnClient;
                      Image=Language;
                      PromotedCategory=Category4;
                      OnAction=VAR
                                 ProfileRec@1002 : Record 2000000072;
                                 ConfPersonalizationMgt@1001 : Codeunit 9170;
                               BEGIN
                                 CurrPage.SETSELECTIONFILTER(ProfileRec);
                                 ConfPersonalizationMgt.ImportTranslatedResourcesWithFolderSelection(ProfileRec);
                               END;
                                }
      { 13      ;2   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=ENU=Import Translated Profile Resources From Zip File;
                      Promoted=Yes;
                      Image=Language;
                      PromotedCategory=Category4;
                      OnAction=VAR
                                 ProfileRec@1002 : Record 2000000072;
                                 ConfPersonalizationMgt@1001 : Codeunit 9170;
                               BEGIN
                                 CurrPage.SETSELECTIONFILTER(ProfileRec);
                                 ConfPersonalizationMgt.ImportTranslatedResources(ProfileRec,'',TRUE);
                               END;
                                }
      { 11      ;2   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=ENU=Export Translated Profile Resources;
                      Promoted=Yes;
                      Image=ExportAttachment;
                      PromotedCategory=Category4;
                      OnAction=VAR
                                 ProfileRec@1002 : Record 2000000072;
                                 ConfPersonalizationMgt@1001 : Codeunit 9170;
                               BEGIN
                                 CurrPage.SETSELECTIONFILTER(ProfileRec);
                                 ConfPersonalizationMgt.ExportTranslatedResourcesWithFolderSelection(ProfileRec);
                               END;
                                }
      { 12      ;2   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=ENU=Remove Translated Profile Resources;
                      Promoted=Yes;
                      Image=RemoveLine;
                      PromotedCategory=Category4;
                      OnAction=VAR
                                 ProfileRec@1002 : Record 2000000072;
                                 ConfPersonalizationMgt@1001 : Codeunit 9170;
                               BEGIN
                                 CurrPage.SETSELECTIONFILTER(ProfileRec);
                                 ConfPersonalizationMgt.RemoveTranslatedResourcesWithLanguageSelection(ProfileRec);
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                CaptionML=[ENU=Profile ID;
                           PTB=Perfil ID];
                SourceExpr="Profile ID" }

    { 8   ;2   ;Field     ;
                CaptionML=[ENU=Description;
                           PTB=Descri��o];
                SourceExpr=Description }

    { 15  ;2   ;Field     ;
                Lookup=No;
                CaptionML=[ENU=Role Center ID;
                           PTB=Centro de Fun��es ID];
                BlankZero=Yes;
                SourceExpr="Role Center ID" }

    { 1102601000;2;Field  ;
                CaptionML=[ENU=Default Role Center;
                           PTB=Centro de Fun��es Padr�o];
                SourceExpr="Default Role Center" }

    { 3   ;2   ;Field     ;
                SourceExpr="Disable Personalization" }

    { 5   ;2   ;Field     ;
                SourceExpr="Use Record Notes" }

    { 7   ;2   ;Field     ;
                SourceExpr="Record Notebook" }

    { 9   ;2   ;Field     ;
                SourceExpr="Use Page Notes" }

    { 10  ;2   ;Field     ;
                SourceExpr="Page Notebook" }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      CanRunDotNetOnClient@1001 : Boolean;

    LOCAL PROCEDURE ">NAVBR func"@52006501();
    BEGIN
    END;

    LOCAL PROCEDURE ActionConfigure@52006500();
    BEGIN
      IF "Profile ID" = '' THEN
        EXIT;
      StartRTC(STRSUBSTNO('-profile:"%1" -configure', "Profile ID"));
    END;

    LOCAL PROCEDURE ActionTest@52006521();
    BEGIN
      IF "Profile ID" = '' THEN
        EXIT;
      StartRTC(STRSUBSTNO('-profile:"%1"', "Profile ID"));
    END;

    LOCAL PROCEDURE StartRTC@52006524(Arguments@52006500 : Text);
    VAR
      ActiveSession@52006506 : Record 2000000110;
      ServerFile@52006507 : DotNet "'mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.IO.File";
      XmlDoc@52006513 : DotNet "'System.Xml, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.Xml.XmlDocument";
      XmlNode@52006512 : DotNet "'System.Xml, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.Xml.XmlNode";
      AppDomain@52006504 : DotNet "'mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.AppDomain" RUNONCLIENT;
      ProcessStartInfo@52006503 : DotNet "'System, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.Diagnostics.ProcessStartInfo" RUNONCLIENT;
      Process@52006502 : DotNet "'System, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.Diagnostics.Process" RUNONCLIENT;
      ServerInstance@52006515 : Text;
      ServerPort@52006516 : Text;
      RTCPath@52006501 : Text;
      URL@52006505 : Text;
    BEGIN
      RTCPath := AppDomain.CurrentDomain.BaseDirectory + AppDomain.CurrentDomain.FriendlyName;
      ActiveSession.SETRANGE("Session ID", SESSIONID);
      ActiveSession.FINDFIRST;
      XmlDoc := XmlDoc.XmlDocument;
      IF ServerFile.Exists(APPLICATIONPATH + 'Instances\' + ActiveSession."Server Instance Name" + '\CustomSettings.config') THEN
        XmlDoc.Load(APPLICATIONPATH + 'Instances\' + ActiveSession."Server Instance Name" + '\CustomSettings.config')
      ELSE
        XmlDoc.Load(APPLICATIONPATH + 'CustomSettings.config');
      XmlNode := XmlDoc.SelectSingleNode('//appSettings/add[@key=''ServerInstance'']');
      ServerInstance := XmlNode.Attributes.Item(1).InnerText;
      XmlNode := XmlDoc.SelectSingleNode('//appSettings/add[@key=''ClientServicesPort'']');
      ServerPort := XmlNode.Attributes.Item(1).InnerText;
      URL := STRSUBSTNO('DynamicsNAV://%1:%2/%3/%4/',
                        ActiveSession."Server Computer Name", ServerPort, ServerInstance, COMPANYNAME);
      ProcessStartInfo := ProcessStartInfo.ProcessStartInfo(RTCPath);
      ProcessStartInfo.Arguments := STRSUBSTNO('%1 "%2"', Arguments, URL);
      Process := Process.Process;
      Process.Start(ProcessStartInfo);
    END;

    BEGIN
    END.
  }
}

