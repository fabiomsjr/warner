OBJECT Page 52113088 Item Journal Approval Setup
{
  OBJECT-PROPERTIES
  {
    Date=05/05/15;
    Time=13:50:47;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Item Journal Approval Flow;
               PTB=Conf. Aprova��o Di�rio Produto];
    SourceTable=Table52113080;
    PageType=List;
  }
  CONTROLS
  {
    { 52006500;0;Container;
                ContainerType=ContentArea }

    { 52006501;1;Group    ;
                Name=Group;
                GroupType=Repeater }

    { 52006502;2;Field    ;
                SourceExpr="Journal Template Name" }

    { 52006503;2;Field    ;
                SourceExpr="Approval Flow Rule Code" }

    { 52006504;2;Field    ;
                SourceExpr="Approval Batch Serial No." }

    { 52006505;2;Field    ;
                SourceExpr="Addit. Approvers List Code" }

  }
  CODE
  {

    BEGIN
    END.
  }
}

