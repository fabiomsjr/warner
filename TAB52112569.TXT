OBJECT Table 52112569 Contribution Details
{
  OBJECT-PROPERTIES
  {
    Date=25/04/14;
    Time=08:33:45;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Contribuition Details;
               PTB=Detalhes da Contribui��o];
  }
  FIELDS
  {
    { 1   ;   ;Tax Settlement Code ;Code20        ;CaptionML=PTB=N� }
    { 2   ;   ;User ID             ;Code20        ;CaptionML=PTB=Id Usu�rio }
    { 3   ;   ;Branch Code         ;Code20        ;CaptionML=PTB=C�d. Filial }
    { 4   ;   ;Code Social Contribuition;Code10   ;CaptionML=PTB=C�d. Contribui��o Social }
    { 5   ;   ;Recipe Amount       ;Decimal       ;CaptionML=PTB=Receita Bruta }
    { 6   ;   ;Basis Amount        ;Decimal       ;CaptionML=PTB=Valor Base }
    { 7   ;   ;Aliquot Amount      ;Decimal       ;CaptionML=PTB=Valor Al�quota }
    { 8   ;   ;Contribuition Amount;Decimal       ;CaptionML=PTB=Valor Contribui��o }
    { 9   ;   ;Increased Amount    ;Decimal        }
    { 10  ;   ;Reduction Amount    ;Decimal       ;CaptionML=PTB=Valor Redu��o }
    { 11  ;   ;Deferred Amount     ;Decimal        }
    { 12  ;   ;Contribuition Totals;Decimal        }
    { 13  ;   ;Previous Deferred Amount;Decimal    }
    { 14  ;   ;Tax Identification  ;Option        ;CaptionML=[ENU=Tax Identification;
                                                              PTB=Identifica��o do imposto];
                                                   OptionCaptionML=[ENU=" ,IPI,ICMS,PIS,COFINS,II,IRRF,INSS,ISS,CSL Ret.,PIS Ret.,COFINS Ret.,ISS Ret.,INSS Ret.,Dif. Aliq. ICMS,ST,PCC,CIDE,Funrural,Simples";
                                                                    PTB=" ,IPI,ICMS,PIS,COFINS,II,IRRF,INSS,ISS,CSL Ret.,PIS Ret.,COFINS Ret.,ISS Ret.,INSS Ret.,Dif. Aliq. ICMS,ST,PCC,CIDE,Funrural,Simples"];
                                                   OptionString=[ ,IPI,ICMS,PIS,COFINS,II,IRRF,INSS,ISS,CSL Ret.,PIS Ret.,COFINS Ret.,ISS Ret.,INSS Ret.,Dif. Aliq. ICMS,ST,PCC,CIDE,Funrural,Simples];
                                                   Editable=Yes }
  }
  KEYS
  {
    {    ;Tax Settlement Code,User ID,Tax Identification;
                                                   Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

