OBJECT Table 5958 Resource Service Zone
{
  OBJECT-PROPERTIES
  {
    Date=07/09/12;
    Time=12:00:00;
    Version List=NAVW17.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Resource Service Zone;
               PTB=Zona Recurso Servi�o];
    LookupPageID=Page6021;
  }
  FIELDS
  {
    { 1   ;   ;Resource No.        ;Code20        ;TableRelation=Resource;
                                                   CaptionML=[ENU=Resource No.;
                                                              PTB=N� Recurso];
                                                   NotBlank=Yes }
    { 2   ;   ;Service Zone Code   ;Code10        ;TableRelation="Service Zone";
                                                   CaptionML=[ENU=Service Zone Code;
                                                              PTB=Cod. Zona Servi�o];
                                                   NotBlank=Yes }
    { 3   ;   ;Starting Date       ;Date          ;CaptionML=[ENU=Starting Date;
                                                              PTB=Data Inicial] }
    { 4   ;   ;Description         ;Text50        ;CaptionML=[ENU=Description;
                                                              PTB=Descri��o] }
  }
  KEYS
  {
    {    ;Resource No.,Service Zone Code,Starting Date;
                                                   Clustered=Yes }
    {    ;Service Zone Code,Starting Date,Resource No. }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

