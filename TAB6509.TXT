OBJECT Table 6509 Whse. Item Entry Relation
{
  OBJECT-PROPERTIES
  {
    Date=05/11/08;
    Time=12:00:00;
    Version List=NAVW16.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Whse. Item Entry Relation;
               PTB=Rela��o Mov. Produto Dep�sito];
  }
  FIELDS
  {
    { 1   ;   ;Item Entry No.      ;Integer       ;TableRelation="Item Ledger Entry";
                                                   CaptionML=[ENU=Item Entry No.;
                                                              PTB=N� Mov. Produto] }
    { 10  ;   ;Source Type         ;Integer       ;CaptionML=[ENU=Source Type;
                                                              PTB=Tipo Origem] }
    { 11  ;   ;Source Subtype      ;Option        ;CaptionML=[ENU=Source Subtype;
                                                              PTB=Subtipo Origem];
                                                   OptionCaptionML=[ENU=0,1,2,3,4,5,6,7,8,9,10;
                                                                    PTB=0,1,2,3,4,5,6,7,8,9,10];
                                                   OptionString=0,1,2,3,4,5,6,7,8,9,10 }
    { 12  ;   ;Source ID           ;Code20        ;CaptionML=[ENU=Source ID;
                                                              PTB=ID Origem] }
    { 13  ;   ;Source Batch Name   ;Code10        ;CaptionML=[ENU=Source Batch Name;
                                                              PTB=Nome Se��o Origem] }
    { 14  ;   ;Source Prod. Order Line;Integer    ;CaptionML=[ENU=Source Prod. Order Line;
                                                              PTB=Linha Ord. Prod. Origem] }
    { 15  ;   ;Source Ref. No.     ;Integer       ;CaptionML=[ENU=Source Ref. No.;
                                                              PTB=N� Ref. Origem] }
    { 20  ;   ;Serial No.          ;Code20        ;CaptionML=[ENU=Serial No.;
                                                              PTB=No. S�rie] }
    { 21  ;   ;Lot No.             ;Code20        ;CaptionML=[ENU=Lot No.;
                                                              PTB=No. Lote] }
    { 30  ;   ;Order No.           ;Code20        ;CaptionML=[ENU=Order No.;
                                                              PTB=N� Pedido] }
    { 31  ;   ;Order Line No.      ;Integer       ;CaptionML=[ENU=Order Line No.;
                                                              PTB=N� Linha Pedido] }
  }
  KEYS
  {
    {    ;Item Entry No.                          ;Clustered=Yes }
    {    ;Source ID,Source Type,Source Subtype,Source Ref. No.,Source Prod. Order Line,Source Batch Name }
    {    ;Order No.,Order Line No.                 }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

