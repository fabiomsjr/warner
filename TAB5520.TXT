OBJECT Table 5520 Unplanned Demand
{
  OBJECT-PROPERTIES
  {
    Date=07/09/12;
    Time=12:00:00;
    Version List=NAVW17.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Unplanned Demand;
               PTB=Demanda n�o Planejada];
  }
  FIELDS
  {
    { 1   ;   ;Demand Type         ;Option        ;CaptionML=[ENU=Demand Type;
                                                              PTB=Tipo Demanda];
                                                   OptionCaptionML=[ENU=" ,Production,Sales,Service,Job,Assembly";
                                                                    PTB=,Produ��o,Vendas];
                                                   OptionString=[ ,Production,Sales,Service,Job,Assembly];
                                                   Editable=No }
    { 2   ;   ;Demand SubType      ;Option        ;CaptionML=[ENU=Demand SubType;
                                                              PTB=Subtipo Demanda];
                                                   OptionCaptionML=[ENU=0,1,2,3,4,5,6,7,8,9;
                                                                    PTB=0,1,2,3,4,5,6,7,8,9];
                                                   OptionString=0,1,2,3,4,5,6,7,8,9;
                                                   Editable=No }
    { 4   ;   ;Demand Order No.    ;Code20        ;OnValidate=BEGIN
                                                                "Sell-to Customer No." := '';
                                                                Description := '';

                                                                IF "Demand Order No." = '' THEN
                                                                  EXIT;

                                                                CASE "Demand Type" OF
                                                                  "Demand Type"::Sales:
                                                                    BEGIN
                                                                      SalesHeader.GET("Demand SubType","Demand Order No.");
                                                                      "Sell-to Customer No." := SalesHeader."Sell-to Customer No.";
                                                                      Description := SalesHeader."Sell-to Customer Name";
                                                                    END;
                                                                  "Demand Type"::Production:
                                                                    BEGIN
                                                                      ProdOrder.GET("Demand SubType","Demand Order No.");
                                                                      Description := ProdOrder.Description;
                                                                    END;
                                                                  "Demand Type"::Assembly:
                                                                    BEGIN
                                                                      AsmHeader.GET("Demand SubType","Demand Order No.");
                                                                      Description := AsmHeader.Description;
                                                                    END;
                                                                  "Demand Type"::Service:
                                                                    BEGIN
                                                                      ServHeader.GET("Demand SubType","Demand Order No.");
                                                                      "Sell-to Customer No." := ServHeader."Customer No.";
                                                                      Description := ServHeader.Name;
                                                                    END;
                                                                  "Demand Type"::Job:
                                                                    BEGIN
                                                                      Job.GET("Demand Order No.");
                                                                      "Sell-to Customer No." := Job."Bill-to Customer No.";
                                                                      Description := Job.Description;
                                                                    END;
                                                                END;
                                                                "Demand Line No." := 0;
                                                                "Demand Ref. No." := 0;
                                                              END;

                                                   CaptionML=[ENU=Demand Order No.;
                                                              PTB=No. Ordem de Demanda];
                                                   Editable=No }
    { 5   ;   ;Demand Line No.     ;Integer       ;CaptionML=[ENU=Demand Line No.;
                                                              PTB=No. Linha de Demanda] }
    { 6   ;   ;Demand Ref. No.     ;Integer       ;CaptionML=[ENU=Demand Ref. No.;
                                                              PTB=No. Ref. Demanda] }
    { 7   ;   ;Sell-to Customer No.;Code20        ;CaptionML=[ENU=Sell-to Customer No.;
                                                              PTB=Venda-a N� Cliente];
                                                   Editable=No }
    { 8   ;   ;Description         ;Text50        ;CaptionML=[ENU=Description;
                                                              PTB=Descri��o];
                                                   Editable=No }
    { 9   ;   ;Demand Date         ;Date          ;CaptionML=[ENU=Demand Date;
                                                              PTB=Data da Procura];
                                                   Editable=No }
    { 10  ;   ;Status              ;Option        ;CaptionML=[ENU=Status;
                                                              PTB=Status];
                                                   OptionCaptionML=[ENU=0,1,2,3,4,5,6,7,8,9,10;
                                                                    PTB=0,1,2,3,4,5,6,7,8,9,10];
                                                   OptionString=0,1,2,3,4,5,6,7,8,9,10;
                                                   Editable=No }
    { 13  ;   ;Item No.            ;Code20        ;CaptionML=[ENU=Item No.;
                                                              PTB=No. Produto] }
    { 14  ;   ;Variant Code        ;Code10        ;CaptionML=[ENU=Variant Code;
                                                              PTB=C�digo Variante] }
    { 15  ;   ;Location Code       ;Code10        ;CaptionML=[ENU=Location Code;
                                                              PTB=Cod. Dep�sito] }
    { 16  ;   ;Quantity (Base)     ;Decimal       ;CaptionML=[ENU=Quantity (Base);
                                                              PTB=Qtd. (Base)] }
    { 17  ;   ;Level               ;Integer       ;CaptionML=[ENU=Level;
                                                              PTB=N�vel] }
    { 18  ;   ;Bin Code            ;Code20        ;TableRelation=Bin.Code WHERE (Location Code=FIELD(Location Code),
                                                                                 Item Filter=FIELD(Item No.),
                                                                                 Variant Filter=FIELD(Variant Code));
                                                   CaptionML=[ENU=Bin Code;
                                                              PTB=C�d. Prateleira] }
    { 19  ;   ;Qty. per Unit of Measure;Decimal   ;InitValue=1;
                                                   CaptionML=[ENU=Qty. per Unit of Measure;
                                                              PTB=Qtd. por Unidade Medida];
                                                   DecimalPlaces=0:5;
                                                   Editable=No }
    { 20  ;   ;Unit of Measure Code;Code10        ;TableRelation="Item Unit of Measure".Code WHERE (Item No.=FIELD(Item No.));
                                                   CaptionML=[ENU=Unit of Measure Code;
                                                              PTB=Cod. Unidade Medida] }
    { 21  ;   ;Reserve             ;Boolean       ;CaptionML=[ENU=Reserve;
                                                              PTB=Reserva] }
    { 22  ;   ;Needed Qty. (Base)  ;Decimal       ;CaptionML=[ENU=Needed Qty. (Base);
                                                              PTB=Qtd. Necess�ria (Base)] }
    { 23  ;   ;Special Order       ;Boolean       ;CaptionML=ENU=Special Order }
    { 24  ;   ;Purchasing Code     ;Code10        ;CaptionML=ENU=Purchasing Code }
  }
  KEYS
  {
    {    ;Demand Type,Demand SubType,Demand Order No.,Demand Line No.,Demand Ref. No.;
                                                   Clustered=Yes }
    {    ;Demand Date,Level                        }
    {    ;Item No.,Variant Code,Location Code,Demand Date;
                                                   SumIndexFields=Quantity (Base),Needed Qty. (Base) }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      SalesHeader@1000 : Record 36;
      ProdOrder@1001 : Record 5405;
      ServHeader@1002 : Record 5900;
      Job@1003 : Record 167;
      AsmHeader@1004 : Record 900;

    BEGIN
    END.
  }
}

