OBJECT Table 342 Acc. Sched. Cell Value
{
  OBJECT-PROPERTIES
  {
    Date=07/09/12;
    Time=12:00:00;
    Version List=NAVW17.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Acc. Sched. Cell Value;
               PTB=Esquema conta valor c�lula];
  }
  FIELDS
  {
    { 1   ;   ;Row No.             ;Integer       ;CaptionML=[ENU=Row No.;
                                                              PTB=N� Fila] }
    { 2   ;   ;Column No.          ;Integer       ;CaptionML=[ENU=Column No.;
                                                              PTB=N� coluna] }
    { 3   ;   ;Value               ;Decimal       ;CaptionML=[ENU=Value;
                                                              PTB=VALOR];
                                                   AutoFormatType=1 }
    { 4   ;   ;Has Error           ;Boolean       ;CaptionML=[ENU=Has Error;
                                                              PTB=Erro!] }
    { 5   ;   ;Period Error        ;Boolean       ;CaptionML=[ENU=Period Error;
                                                              PTB=Erro de Per�odo] }
  }
  KEYS
  {
    {    ;Row No.,Column No.                      ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

