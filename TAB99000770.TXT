OBJECT Table 99000770 Manufacturing Comment Line
{
  OBJECT-PROPERTIES
  {
    Date=07/09/12;
    Time=12:00:00;
    Version List=NAVW17.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Manufacturing Comment Line;
               PTB=Linha de Coment�rio Produ��o];
    LookupPageID=Page99000785;
    DrillDownPageID=Page99000785;
  }
  FIELDS
  {
    { 1   ;   ;Table Name          ;Option        ;CaptionML=[ENU=Table Name;
                                                              PTB=Nome Tabela];
                                                   OptionCaptionML=[ENU=Work Center,Machine Center,Routing Header,Production BOM Header;
                                                                    PTB=Centro Trabalho,Centro M�quina,Cabe�alho de Roteiro,Cabe�alho de L.M. Produ��o];
                                                   OptionString=Work Center,Machine Center,Routing Header,Production BOM Header }
    { 2   ;   ;No.                 ;Code20        ;TableRelation=IF (Table Name=CONST(Work Center)) "Work Center"
                                                                 ELSE IF (Table Name=CONST(Machine Center)) "Machine Center"
                                                                 ELSE IF (Table Name=CONST(Routing Header)) "Routing Header"
                                                                 ELSE IF (Table Name=CONST(Production BOM Header)) "Production BOM Header";
                                                   CaptionML=[ENU=No.;
                                                              PTB=N�];
                                                   NotBlank=Yes }
    { 3   ;   ;Line No.            ;Integer       ;CaptionML=[ENU=Line No.;
                                                              PTB=N� Linha] }
    { 4   ;   ;Date                ;Date          ;CaptionML=[ENU=Date;
                                                              PTB=Data] }
    { 5   ;   ;Code                ;Code10        ;CaptionML=[ENU=Code;
                                                              PTB=C�digo] }
    { 6   ;   ;Comment             ;Text80        ;CaptionML=[ENU=Comment;
                                                              PTB=Coment�rio] }
  }
  KEYS
  {
    {    ;Table Name,No.,Line No.                 ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    PROCEDURE SetUpNewLine@1();
    VAR
      CommentLine@1000 : Record 99000770;
    BEGIN
      CommentLine.SETRANGE("Table Name","Table Name");
      CommentLine.SETRANGE("No.","No.");
      CommentLine.SETRANGE(Date,WORKDATE);
      IF NOT CommentLine.FINDFIRST THEN
        Date := WORKDATE;
    END;

    BEGIN
    END.
  }
}

