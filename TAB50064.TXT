OBJECT Table 50064 Post. Net Sales Header
{
  OBJECT-PROPERTIES
  {
    Date=29/04/11;
    Time=11:51:46;
    Version List=NetSales;
  }
  PROPERTIES
  {
    OnDelete=BEGIN
               NetSalesLine.RESET;
               NetSalesLine.SETCURRENTKEY(NetSalesLine."Net Sales No.");
               NetSalesLine.SETRANGE(NetSalesLine."Net Sales No.","No.");
               IF NetSalesLine.FIND('-') THEN
                 NetSalesLine.DELETEALL;

               NetSalesSintetic.RESET;
               NetSalesSintetic.SETCURRENTKEY(NetSalesSintetic."Net Sales No.");
               NetSalesSintetic.SETRANGE(NetSalesSintetic."Net Sales No.","No.");
               IF NetSalesSintetic.FIND('-') THEN
                 NetSalesSintetic.DELETEALL;
             END;

    OnRename=BEGIN
               ERROR(Text003,TABLECAPTION);
             END;

    CaptionML=[ENU=Post. Net Sales Header;
               PTB=Hist. Cab. Valida��o Margem];
    LookupPageID=Page50079;
    DrillDownPageID=Page50079;
  }
  FIELDS
  {
    { 1   ;   ;No.                 ;Code10        ;CaptionML=[ENU=No.;
                                                              PTB=N�] }
    { 2   ;   ;User ID             ;Code20        ;TableRelation="User Setup";
                                                   CaptionML=[ENU=User ID;
                                                              PTB=ID Usu�rio] }
    { 3   ;   ;Creation Date       ;Date          ;CaptionML=[ENU=Creation Date;
                                                              PTB=Data Cria��o] }
    { 4   ;   ;Time Creation       ;Time          ;CaptionML=[ENU=Time Creation;
                                                              PTB=Hora Cria��o] }
    { 5   ;   ;No. Series          ;Code10        ;TableRelation="No. Series";
                                                   CaptionML=[ENU=No. Series;
                                                              PTB=N� S�ries];
                                                   Editable=No }
    { 6   ;   ;Posting Date        ;Date          ;OnValidate=BEGIN
                                                                IF ("Posting Date" <> xRec."Posting Date") AND (CheckEntries) THEN BEGIN
                                                                  ValidateDelete(FIELDCAPTION("Posting Date"));
                                                                  ResetRecords;
                                                                END;
                                                              END;

                                                   CaptionML=[ENU=Posting Date;
                                                              PTB=Data Registro] }
    { 7   ;   ;Description         ;Text50        ;OnValidate=BEGIN
                                                                IF (Description <> xRec.Description) AND (CheckEntries) THEN BEGIN
                                                                  ValidateDelete(FIELDCAPTION(Description));
                                                                  ResetRecords;
                                                                END;
                                                              END;

                                                   CaptionML=[ENU=Description;
                                                              PTB=Descri��o] }
    { 8   ;   ;Start Date          ;Date          ;OnValidate=BEGIN
                                                                IF ("Start Date" <> xRec."Start Date") AND (CheckEntries) THEN BEGIN
                                                                  ValidateDelete(FIELDCAPTION("Start Date"));
                                                                  ResetRecords;
                                                                END;
                                                              END;

                                                   CaptionML=[ENU=Start Date;
                                                              PTB=Data Inicial] }
    { 9   ;   ;End Date            ;Date          ;OnValidate=BEGIN
                                                                IF ("End Date" <> xRec."End Date") AND (CheckEntries) THEN BEGIN
                                                                  ValidateDelete(FIELDCAPTION("End Date"));
                                                                  ResetRecords;
                                                                END;
                                                              END;

                                                   CaptionML=[ENU=End Date;
                                                              PTB=Data Final] }
    { 10  ;   ;Filters             ;Text250       ;CaptionML=PTB=Filtros;
                                                   Editable=No }
    { 11  ;   ;Filters 2           ;Text250       ;CaptionML=PTB=Complemento Filtros;
                                                   Editable=No }
    { 50000;  ;User ID Conv.       ;Code20        ;TableRelation="User Setup";
                                                   CaptionML=[ENU=User ID;
                                                              PTB=ID Usu�rio Conv.] }
    { 50001;  ;Conv. Date          ;Date          ;CaptionML=[ENU=Creation Date;
                                                              PTB=Data Convers�o] }
    { 50002;  ;Time Conv.          ;Time          ;CaptionML=[ENU=Time Creation;
                                                              PTB=Hora Convers�o] }
  }
  KEYS
  {
    {    ;No.                                     ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      NoSeriesMgt@1102300004 : Codeunit 396;
      NetSalesLine@1102300009 : Record 50056;
      Text003@1102300001 : TextConst 'ENU=You cannot rename a %1.;PTB=N�o pode modificar o nome de %1';
      Text011@1102300002 : TextConst 'ENU=If you change the contents of the %1 field, the analysis view entries will be deleted.;PTB=Se alterar o conte�do do campo %1, os movimentos existentes ser�o eliminados.';
      Text012@1102300005 : TextConst 'ENU=\You will have to update again.\\Do you want to enter a new value in the %1 field?;PTB=\O c�lculo dever� ser realizado novamente.\\Deseja modificar o valor do campo %1?';
      Text013@1102300006 : TextConst 'ENU=The update has been interrupted in response to the warning.;PTB=A atualiza��o foi interrompida em resposta ao aviso.';
      NetSalesSintetic@1102300000 : Record 50057;

    PROCEDURE AssistEdit@1(OldNetSalesHeader@1000 : Record 50055) : Boolean;
    BEGIN
    END;

    PROCEDURE CheckEntries@1102300000() : Boolean;
    BEGIN
      NetSalesLine.RESET;
      NetSalesLine.SETCURRENTKEY(NetSalesLine."Net Sales No.");
      NetSalesLine.SETRANGE(NetSalesLine."Net Sales No.","No.");
      IF NetSalesLine.FIND('-') THEN
        EXIT(TRUE);
    END;

    PROCEDURE ResetRecords@5();
    BEGIN
      NetSalesLine.RESET;
      NetSalesLine.SETCURRENTKEY(NetSalesLine."Net Sales No.");
      NetSalesLine.SETRANGE(NetSalesLine."Net Sales No.","No.");
      IF NetSalesLine.FIND('-') THEN
        NetSalesLine.DELETEALL;

      NetSalesSintetic.RESET;
      NetSalesSintetic.SETCURRENTKEY(NetSalesSintetic."Net Sales No.");
      NetSalesSintetic.SETRANGE(NetSalesSintetic."Net Sales No.","No.");
      IF NetSalesSintetic.FIND('-') THEN
        NetSalesSintetic.DELETEALL;
    END;

    PROCEDURE ValidateDelete@10(FieldName@1000 : Text[100]);
    VAR
      Question@1001 : Text[250];
    BEGIN
      Question := STRSUBSTNO(
        Text011 +
        Text012,FieldName);
      IF NOT DIALOG.CONFIRM(Question, TRUE) THEN
        ERROR(Text013);
    END;

    BEGIN
    END.
  }
}

