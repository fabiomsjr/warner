OBJECT Page 52113089 AF Additional Approvers Lists
{
  OBJECT-PROPERTIES
  {
    Date=05/05/15;
    Time=13:37:30;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Additional Approvers Lists;
               PTB=Listas Aprovadores Adicionais];
    SourceTable=Table52113085;
    PageType=Card;
    CardPageID=AF Addit. Approvers List Card;
  }
  CONTROLS
  {
    { 52006500;0;Container;
                ContainerType=ContentArea }

    { 52006501;1;Group    ;
                Name=Geral;
                GroupType=Repeater }

    { 52006502;2;Field    ;
                SourceExpr=Code }

    { 52006503;2;Field    ;
                SourceExpr=Description }

  }
  CODE
  {

    BEGIN
    END.
  }
}

