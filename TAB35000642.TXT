OBJECT Table 35000642 Unit Measure.2
{
  OBJECT-PROPERTIES
  {
    Date=29/02/12;
    Time=18:14:19;
    Version List=EFD5.08.09;
  }
  PROPERTIES
  {
    DataCaptionFields=Code,Description;
    OnDelete=VAR
               USText001@1240470002 : TextConst 'ENU=The Unit of Measure - %1  is attached to a Payroll Rate record and therefore cannot be deleted.;ESM=La unidad de medida - %1  est� asociada a un registro de retribuci�n y no puede eliminarse.;FRC=L''unit� de mesure - %1 est associ�e � un enregistrement de type Taux salarial et ne peut pas �tre supprim�e.;ENC=The Unit of Measure - %1  is attached to a Payroll Rate record and therefore cannot be deleted.';
             BEGIN
               UnitOfMeasureTranslation.SETRANGE(Code,"Tax Settlement Code");
               UnitOfMeasureTranslation.DELETEALL;
             END;

    CaptionML=[ENU=Measure Unit;
               PTB=Unidade de Medida;
               ESM=Unidad Medida;
               FRC=Unit� de Mesure];
    LookupPageID=Page209;
    DrillDownPageID=Page209;
  }
  FIELDS
  {
    { 1   ;   ;Tax Settlement Code ;Code20        ;CaptionML=[ENU=No.;
                                                              PTB=No.];
                                                   Description=Geral }
    { 2   ;   ;Code                ;Code10        ;CaptionML=[ENU=Code;
                                                              PTB=C�digo;
                                                              ESM=C�digo;
                                                              FRC="Code  ";
                                                              ENC=Code];
                                                   NotBlank=Yes }
    { 3   ;   ;Description         ;Text10        ;CaptionML=[ENU=Description;
                                                              PTB=Descri��o;
                                                              ESM=Descripci�n;
                                                              FRC=Description;
                                                              ENC=Description] }
  }
  KEYS
  {
    {    ;Tax Settlement Code,Code                ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      UnitOfMeasureTranslation@1000 : Record 5402;

    BEGIN
    {
      MBS/BR

      No.   dd.mm.yy   Developer   Company   DocNo.   Description
      -----------------------------------------------------------------------------------------------------------------
      00    01.12.06   MK          MICROSOFT LOC001   The reference Documentation file LOCALIZATIONBR.DOC
    }
    END.
  }
}

