OBJECT Codeunit 52113139 Item Jnl.-Post Line Hook
{
  OBJECT-PROPERTIES
  {
    Date=23/07/15;
    Time=16:49:23;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    Permissions=TableData 52112493=rimd;
    OnRun=BEGIN
          END;

  }
  CODE
  {
    VAR
      ItemJnlPostLineCHook@52006500 : Codeunit 52113140;
      Text001@52006501 : TextConst 'ENU=Location %1 only allows postings on branch %2.;PTB=Dep�sito %1 s� permite lan�amentos na filial %2.';

    PROCEDURE OnBeforePost@52006502(VAR itemJnlLine@52006500 : Record 83);
    BEGIN
      UpdatePurchReq(itemJnlLine);

      ItemJnlPostLineCHook.OnBeforePost(itemJnlLine);
    END;

    PROCEDURE OnInitItemLedgEntry@52006506(VAR itemJnlLine@52006500 : Record 83;VAR itemLedgEntry@52006501 : Record 32);
    BEGIN
      itemLedgEntry.Description := itemJnlLine.Description;

      ItemJnlPostLineCHook.OnInitItemLedgEntry(itemJnlLine, itemLedgEntry);
    END;

    PROCEDURE OnBeforeInsertItemLedgEntry@52006505(itemJnlLine@52006501 : Record 83;VAR itemLedgEntry@52006500 : Record 32);
    VAR
      dimMgt@52006502 : Codeunit 408;
      location@52006503 : Record 14;
    BEGIN
      WITH itemLedgEntry DO BEGIN
        "Branch Code" := dimMgt.GetBranchCodeFromDimSetID("Dimension Set ID");
        "Third-Party Stock Entry" := itemJnlLine."Third-Party Stock Entry";

        //Checks Location Branch Code against journal Branch Code
        IF "Location Code" <> '' THEN BEGIN
          location.GET("Location Code");
          IF location."Branch Code" <> '' THEN BEGIN
            IF "Branch Code" <> location."Branch Code" THEN
              ERROR(Text001, "Location Code", location."Branch Code");
          END;
        END;

        IF itemJnlLine."Create Third-Party In. Entry" THEN
          CreateThirdPartyItemEntry(itemJnlLine, itemLedgEntry);
      END;

      ItemJnlPostLineCHook.OnBeforeInsertItemLedgEntry(itemJnlLine, itemLedgEntry);
    END;

    PROCEDURE OnAfterInitValueEntry@52006504(itemJnlLine@52006502 : Record 83;itemLedgEntry@52006500 : Record 32;VAR valueEntry@52006501 : Record 5802);
    VAR
      operationType@52006503 : Record 52112435;
    BEGIN
      WITH valueEntry DO BEGIN
        Description := itemJnlLine.Description;
        "Third-Party Stock Entry" := itemJnlLine."Third-Party Stock Entry";
      END;

      ItemJnlPostLineCHook.OnAfterInitValueEntry(itemJnlLine, itemLedgEntry, valueEntry);
    END;

    PROCEDURE OnBeforeInsertValueEntry@52006503(itemJnlLine@52006501 : Record 83;itemLedgEntry@52006500 : Record 32;VAR valueEntry@52006502 : Record 5802);
    VAR
      dimMgt@52006503 : Codeunit 408;
    BEGIN
      WITH valueEntry DO BEGIN
        "Branch Code" := dimMgt.GetBranchCodeFromDimSetID("Dimension Set ID");
      END;

      IF valueEntry."Third-Party Stock Entry" THEN
        UpdateThirdPartyItemEntry(itemJnlLine, itemLedgEntry, valueEntry);

      ItemJnlPostLineCHook.OnBeforeInsertValueEntry(itemJnlLine, itemLedgEntry, valueEntry);
    END;

    PROCEDURE OnBeforeInventoryPostingToGL@52006500(VAR genJnlLine@52006500 : Record 81;valueEntry@52006501 : Record 5802);
    BEGIN
      //Need to be moved to different Hook codeunit
      ItemJnlPostLineCHook.OnBeforeInventoryPostingToGL(genJnlLine, valueEntry);
    END;

    PROCEDURE _@52006507();
    BEGIN
    END;

    LOCAL PROCEDURE UpdatePurchReq@52006501(VAR itemJnlLine@52006500 : Record 83);
    VAR
      purchReqLineOpenDoc@52006501 : Record 52112729;
      purchReqLine@52006502 : Record 52112724;
    BEGIN
      WITH itemJnlLine DO BEGIN
        IF ("Journal Template Name" = '') OR ("Journal Batch Name" = '') OR (itemJnlLine."Line No." = 0) THEN
          EXIT;
        IF Quantity >= 0 THEN
          EXIT;

        purchReqLineOpenDoc.SETCURRENTKEY("Item Jnl. Template", "Item Jnl. Batch", "Item Jnl. Line");
        purchReqLineOpenDoc.SETRANGE("Item Jnl. Template", "Journal Template Name");
        purchReqLineOpenDoc.SETRANGE("Item Jnl. Batch", "Journal Batch Name");
        purchReqLineOpenDoc.SETRANGE("Item Jnl. Line", "Line No.");
        IF purchReqLineOpenDoc.FINDFIRST THEN
          REPEAT
            purchReqLine.GET(purchReqLineOpenDoc."Request No.", purchReqLineOpenDoc."Request Line No.");
            purchReqLine."Open Quantity" -= ABS(itemJnlLine.Quantity);
            purchReqLine.MODIFY;
          UNTIL purchReqLineOpenDoc.NEXT = 0;
        purchReqLineOpenDoc.DELETEALL;
      END;
    END;

    PROCEDURE CreateThirdPartyItemEntry@52006508(itemJnlLine@52006501 : Record 83;itemLedgEntry@52006500 : Record 32);
    VAR
      operationType@52006502 : Record 52112435;
      thirdPartItemEntry@52006503 : Record 52112493;
      entryNo@52006504 : Integer;
      locationCode@52006505 : Code[20];
      cust@52006506 : Record 18;
    BEGIN
      itemJnlLine.TESTFIELD("Operation Type");
      operationType.GET(itemJnlLine."Operation Type");
      locationCode := operationType."Third-Party Location Code";
      IF locationCode = '' THEN BEGIN
        itemLedgEntry.TESTFIELD("Source No.");
        cust.GET(itemLedgEntry."Source No.");
        cust.TESTFIELD("Third-Party Location Code");
        locationCode := cust."Third-Party Location Code";
      END;

      thirdPartItemEntry.SETCURRENTKEY("Source Item Ledger Entry No.");
      thirdPartItemEntry.SETRANGE("Source Item Ledger Entry No.", itemLedgEntry."Entry No.");
      IF NOT thirdPartItemEntry.ISEMPTY THEN
        EXIT;

      thirdPartItemEntry.RESET;
      IF thirdPartItemEntry.FINDLAST THEN
        entryNo := thirdPartItemEntry."Entry No.";
      entryNo += 1;

      WITH itemLedgEntry DO BEGIN
        thirdPartItemEntry.INIT;
        thirdPartItemEntry."Entry No." := entryNo;
        thirdPartItemEntry."Source Item Ledger Entry No." := itemLedgEntry."Entry No.";
        thirdPartItemEntry."Item No." := "Item No.";
        thirdPartItemEntry.Quantity := ABS(Quantity);
        thirdPartItemEntry."Remaining Quantity" := ABS(Quantity);
        thirdPartItemEntry."Posting Date" := "Posting Date";
        thirdPartItemEntry."Document No." := "Document No.";
        thirdPartItemEntry.Description := Description;
        thirdPartItemEntry."External Document No." := "External Document No.";
        thirdPartItemEntry."No. Series" := itemLedgEntry."No. Series";
        thirdPartItemEntry."Document Date" := "Document Date";
        thirdPartItemEntry."Serial No." := "Serial No.";
        thirdPartItemEntry."Lot No." := "Lot No.";
        thirdPartItemEntry."Location Code" := locationCode;
        thirdPartItemEntry."Gen. Prod. Posting Group" := itemJnlLine."Gen. Prod. Posting Group";
        thirdPartItemEntry.INSERT;
      END;
    END;

    PROCEDURE UpdateThirdPartyItemEntry@52006512(ItemJnlLine@52006501 : Record 83;ItemLedgEntry@52006500 : Record 32;VAR ValueEntry@52006505 : Record 5802);
    VAR
      ThirdPartItemEntry@52006503 : Record 52112493;
    BEGIN
      ValueEntry."Sales Amount (Expected)" := 0;
      ValueEntry."Sales Amount (Actual)" := 0;

      ItemLedgEntry.CALCFIELDS("Cost Amount (Actual)");
      IF (ItemLedgEntry."Cost Amount (Actual)" <> 0) OR (ValueEntry."Cost Amount (Actual)" = 0) THEN
        EXIT;

      WITH ThirdPartItemEntry DO BEGIN
        SETCURRENTKEY("Source Item Ledger Entry No.");
        SETRANGE("Source Item Ledger Entry No.", ItemLedgEntry."Entry No.");
        IF FINDFIRST THEN BEGIN
          "Cost to Post" += ValueEntry."Cost Amount (Actual)";
          Posted := FALSE;
          MODIFY;
        END;
      END;
    END;

    BEGIN
    END.
  }
}

