OBJECT Table 52112863 ECF Setup
{
  OBJECT-PROPERTIES
  {
    Date=24/05/17;
    Time=14:52:30;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=ECF Setup;
               PTB=Configura��o ECF];
  }
  FIELDS
  {
    { 1   ;   ;Primary Key         ;Code1         ;CaptionML=[ENU=Primary Key;
                                                              PTB=Chave Prim�ria] }
    { 15  ;   ;Version Code Layout ;Text4         ;CaptionML=PTB=Cod. Vers�o Layout }
    { 16  ;   ;ECF Type            ;Option        ;CaptionML=PTB=Tipo ECF;
                                                   OptionCaptionML=PTB=0-ECF de empresa n�o participante de SCP como s�cio ostensivo,1-ECF de empresa participante de SCP como s�cio ostensivo,2-ECF da SCP;
                                                   OptionString=0-ECF de empresa n�o participante de SCP como s�cio ostensivo,1-ECF de empresa participante de SCP como s�cio ostensivo,2-ECF da SCP }
    { 17  ;   ;Code SPC            ;Code14        ;CaptionML=PTB=Identifica��o SPC }
    { 18  ;   ;Tribute Code        ;Option        ;CaptionML=PTB=Tributa��o do Lucro;
                                                   OptionCaptionML=PTB=" ,Lucro Real,Lucro Real/Arbitrado,Lucro Presumido/Real,Lucro Presumido/Real/Arbitrado,Lucro Presumido,Lucro Arbitrado,Lucro Presumido/Arbitrado,Imune do IRPJ,Isenta do IRPJ";
                                                   OptionString=[ ,Lucro Real,Lucro Real/Arbitrado,Lucro Presumido/Real,Lucro Presumido/Real/Arbitrado,Lucro Presumido,Lucro Arbitrado,Lucro Presumido/Arbitrado,Imune do IRPJ,Isenta do IRPJ] }
    { 19  ;   ;Calculation Form    ;Option        ;CaptionML=PTB=Forma Apura��o;
                                                   OptionCaptionML=PTB=T-Trimestral,A-Anual;
                                                   OptionString=T-Trimestral,A-Anual }
    { 20  ;   ;Qualification Juridical Person;Option;
                                                   CaptionML=[ENU=Qualification Juridical Person;
                                                              PTB=Qualifica��o Pessoa Juridica];
                                                   OptionCaptionML=[ENU=01 - Juridical Person in General, 02 - PJ Component of the Financial System, 03 - Insurance Companies, Capitalization or Open Supplementary Pension Entity;
                                                                    PTB=01 - PJ em Geral,02 - PJ Componente do Sistema Financeiro,03 - Sociedades Seguradoras, de Capitaliza��o ou Entidade Aberta de Previd�ncia Complementar];
                                                   OptionString=01 - PJ em Geral,02 - PJ Componente do Sistema Financeiro,03 - Sociedades Seguradoras, de Capitaliza��o ou Entidade Aberta de Previd�ncia Complementar }
    { 22  ;   ;Estimated Determination Form;Option;CaptionML=PTB=Forma de Apura��o da Estimativa;
                                                   OptionCaptionML=[ENU=0 - Out of Time, E - Gross Revenue, B - Balance or Balance Sheet;
                                                                    PTB=0 -  Fora do Per�odo,E - Receita Bruta,B - Balan�o ou Balancete];
                                                   OptionString=0 -  Fora do Per�odo,E - Receita Bruta,B - Balan�o ou Balancete }
    { 23  ;   ;Type of Bookkeeping ;Option        ;CaptionML=PTB=Tipo de Escritura��o;
                                                   OptionCaptionML=[ENU=L - Cash Book, C - Accounting;
                                                                    PTB=L - Livro Caixa,C - Cont�bil];
                                                   OptionString=L - Livro Caixa,C - Cont�bil }
    { 24  ;   ;PJ Subject to CSLL rate 15%;Boolean;CaptionML=PTB=PJ Sujeita � Al�quota da CSLL de 15% }
    { 25  ;   ;Quantity of SCP then PJ;Integer    ;CaptionML=PTB=Quantidade de SCP da PJ;
                                                   BlankNumbers=BlankZero }
    { 26  ;   ;Adm Funds and Investment Clubs;Boolean;
                                                   CaptionML=PTB=Administradora de Fundos e Clubes de Investimento }
    { 27  ;   ;Interests in Trusts Business;Boolean;
                                                   CaptionML=PTB=Participa��es em Cons�rcios de Empresas }
    { 30  ;   ;PJ Framed in Articles 48 or 49;Boolean;
                                                   CaptionML=PTB=PJ Enquadrada nos artigos 48 ou 49 da IN RFB no 1.312/2012 }
    { 31  ;   ;Interests abroad    ;Boolean       ;CaptionML=PTB=Participa��es no Exterior }
    { 32  ;   ;Rural activity      ;Boolean       ;CaptionML=PTB=Atividade Rural }
    { 33  ;   ;Profit Exploration  ;Boolean       ;CaptionML=PTB=Lucro da Explora��o }
    { 34  ;   ;Tax exemp and reduction presum;Boolean;
                                                   CaptionML=PTB=Isen��o e Redu��o do Imp Luc.Presumido }
    { 35  ;   ;FINOR / FINAM / FUNRES;Boolean      }
    { 51  ;   ;Industrial Hub of Manaus;Boolean   ;CaptionML=PTB=P�lo Industrial de Manaus e Amaz�nia Ocidental }
    { 52  ;   ;Export Processing Zones;Boolean    ;CaptionML=PTB=Zonas de Processamento de Exporta��o }
    { 53  ;   ;Free Areas          ;Boolean       ;CaptionML=PTB=�reas de Livre Com�rcio }
    { 54  ;   ;Form of Taxation    ;Option        ;CaptionML=[ENU=Form of Taxation;
                                                              PTB=Forma de Tributa��o];
                                                   OptionCaptionML=[ENU=0,R,P,A,E;
                                                                    PTB=0-Zero,R-Real,P-Presumido,A-Arbitrado,E-Real Estimativa];
                                                   OptionString=0-Zero,R-Real,P-Presumido,A-Arbitrado,E-Real Estimativa }
    { 55  ;   ;Legal Nature Code   ;Code10        ;TableRelation="ECF Legal Nature Code";
                                                   CaptionML=[ENU=Legal Nature Code;
                                                              PTB=C�d. Natureza Jur�dica] }
    { 58  ;   ;Method Evaluation Stock;Option     ;CaptionML=PTB=M�todo avalia��o estoque;
                                                   OptionCaptionML=PTB=1 - Custo M�dio Ponderado,2 - PEPS,3 - Arbitramento,4 - Custo Espec�fico,5 - Valor Realiz�vel L�quido,6 - Invent�rio Periodico,7 - Outros;
                                                   OptionString=1 - Custo M�dio Ponderado,2 - PEPS,3 - Arbitramento,4 - Custo Espec�fico,5 - Valor Realiz�vel L�quido,6 - Invent�rio Periodico,7 - Outros }
    { 59  ;   ;Refis Option        ;Boolean       ;CaptionML=PTB=Optante Refis }
    { 60  ;   ;PAES Option         ;Boolean       ;CaptionML=PTB=Optante Paes }
    { 61  ;   ;RTT Exit Option     ;Boolean       ;CaptionML=PTB=Optante Extin��o RTT }
    { 62  ;   ;Diference Contab and Fcont;Boolean ;CaptionML=PTB=Dif Contab.Societ�ria e Fcont }
    { 80  ;   ;ECF Dimension 1 Code;Code20        ;TableRelation=Dimension;
                                                   CaptionML=[ENU=ECF Dimension 1 Code;
                                                              PTB=C�d. Dimens�o ECF 1] }
    { 90  ;   ;ECF Dimension 2 Code;Code20        ;TableRelation=Dimension;
                                                   CaptionML=[ENU=ECF Dimension 2 Code;
                                                              PTB=C�d. Dimens�o ECF 2] }
    { 100 ;   ;PJ Subject to CSLL Rate;Option     ;CaptionML=[ENU=PJ Subject to CSLL Rate;
                                                              PTB=PJ Sujeita � Al�quota CSLL];
                                                   OptionCaptionML=[ENU=" ,9%,17%,20%";
                                                                    PTB=" ,9%,17%,20%"];
                                                   OptionString=[ ,9,17,20] }
    { 110 ;   ;IND_REC_RECEITA     ;Option        ;CaptionML=[ENU=Income Recognition Criteria;
                                                              PTB=Crit�rio de Reconhecimento de Receitas];
                                                   OptionCaptionML=[ENU=Regime de Caixa,Regime de Compet�ncia;
                                                                    PTB=Regime de Caixa,Regime de Compet�ncia];
                                                   OptionString=RegimeCaixa,RegimeCompetencia }
    { 120 ;   ;IND_PAIS_A_PAIS     ;Boolean       ;CaptionML=[ENU=A pessoa jur�dica � entidade de grupo multinacional, nos termos a IN RFB n� 1.681/2016;
                                                              PTB=A pessoa jur�dica � entidade de grupo multinacional, nos termos a IN RFB n� 1.681/2016] }
  }
  KEYS
  {
    {    ;Primary Key                             ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      Text001@1102300000 : TextConst 'ENU=Font size must be between 3 and 13.;PTB=Tamanho da fonte deve estar entre 3 e 13.';

    BEGIN
    END.
  }
}

