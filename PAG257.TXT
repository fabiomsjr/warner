OBJECT Page 257 Source Codes
{
  OBJECT-PROPERTIES
  {
    Date=07/09/12;
    Time=12:00:00;
    Version List=NAVW17.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Source Codes;
               PTB=C�digos Origem];
    SourceTable=Table230;
    PageType=List;
    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 8       ;1   ;ActionGroup;
                      CaptionML=[ENU=&Source;
                                 PTB=&Origem];
                      Image=CodesList }
      { 9       ;2   ;Action    ;
                      CaptionML=[ENU=G/L Registers;
                                 PTB=Registros Cont�beis];
                      RunObject=Page 116;
                      RunPageView=SORTING(Source Code);
                      RunPageLink=Source Code=FIELD(Code);
                      Image=GLRegisters }
      { 10      ;2   ;Action    ;
                      CaptionML=[ENU=Item Registers;
                                 PTB=Registros de Item];
                      RunObject=Page 117;
                      RunPageView=SORTING(Source Code);
                      RunPageLink=Source Code=FIELD(Code);
                      Image=ItemRegisters }
      { 13      ;2   ;Action    ;
                      CaptionML=[ENU=Resource Registers;
                                 PTB=Registros Recurso];
                      RunObject=Page 274;
                      RunPageView=SORTING(Source Code);
                      RunPageLink=Source Code=FIELD(Code);
                      Image=ResourceRegisters }
      { 14      ;2   ;Action    ;
                      CaptionML=[ENU=Job Registers;
                                 PTB=Registros Projeto];
                      RunObject=Page 278;
                      RunPageView=SORTING(Source Code);
                      RunPageLink=Source Code=FIELD(Code);
                      Image=JobRegisters }
      { 11      ;2   ;Action    ;
                      CaptionML=[ENU=FA Registers;
                                 PTB=Registros Ativo FIxo];
                      RunObject=Page 5627;
                      RunPageView=SORTING(Source Code);
                      RunPageLink=Source Code=FIELD(Code);
                      Image=FARegisters }
      { 16      ;2   ;Action    ;
                      CaptionML=[ENU=I&nsurance Registers;
                                 PTB=Registros Seguros];
                      RunObject=Page 5656;
                      RunPageView=SORTING(Source Code);
                      RunPageLink=Source Code=FIELD(Code);
                      Image=InsuranceRegisters }
      { 7300    ;2   ;Action    ;
                      CaptionML=[ENU=Warehouse Registers;
                                 PTB=Registros Dep�sito];
                      RunObject=Page 7325;
                      RunPageView=SORTING(Source Code);
                      RunPageLink=Source Code=FIELD(Code);
                      Image=WarehouseRegisters }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                SourceExpr=Code }

    { 4   ;2   ;Field     ;
                SourceExpr=Description }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

