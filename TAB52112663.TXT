OBJECT Table 52112663 FCI Worksheet
{
  OBJECT-PROPERTIES
  {
    Date=20/07/15;
    Time=13:05:53;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=FCI Worksheet;
               PTB=Planilha FCI];
    LookupPageID=Page52112663;
  }
  FIELDS
  {
    { 10  ;   ;Calculation Date    ;Date          ;CaptionML=[ENU=Calculation Date;
                                                              PTB=Data C�lculo] }
    { 20  ;   ;Branch Code         ;Code20        ;TableRelation="Branch Information";
                                                   CaptionML=[ENU=Branch Code;
                                                              PTB=C�d. Filial] }
    { 30  ;   ;Calculated Item No. ;Code20        ;TableRelation=Item;
                                                   CaptionML=[ENU=Calculated Item No.;
                                                              PTB=N� Produto Calculado] }
    { 40  ;   ;Item Level          ;Integer       ;CaptionML=[ENU=Item Level;
                                                              PTB=N�vel Produto];
                                                   MinValue=0;
                                                   BlankNumbers=BlankZero }
    { 50  ;   ;Item No.            ;Code20        ;TableRelation=Item;
                                                   CaptionML=[ENU=Item No.;
                                                              PTB=N� Produto] }
    { 55  ;   ;Item Description    ;Text50        ;FieldClass=FlowField;
                                                   CalcFormula=Lookup(Item.Description WHERE (No.=FIELD(Item No.)));
                                                   CaptionML=[ENU=Item Description;
                                                              PTB=Descri��o Produto];
                                                   Editable=No }
    { 70  ;   ;FCI No.             ;Text36        ;CaptionML=[ENU=FCI No.;
                                                              PTB=N� FCI] }
    { 80  ;   ;VI Amount           ;Decimal       ;CaptionML=[ENU=VI Amount;
                                                              PTB=VI - Valor Parcela Importada];
                                                   BlankNumbers=BlankZero }
    { 90  ;   ;VO Amount           ;Decimal       ;CaptionML=[ENU=VO Amount;
                                                              PTB=VO - Valor Opera��o Sa�da Interestadual];
                                                   BlankNumbers=BlankZero }
    { 100 ;   ;CI Percentage       ;Decimal       ;CaptionML=[ENU=CI Percentage;
                                                              PTB=CI - Conte�do Importa��o];
                                                   BlankNumbers=BlankZero }
    { 110 ;   ;CI Range            ;Decimal       ;TableRelation="CI Range";
                                                   CaptionML=[ENU=CI Range;
                                                              PTB=Faixa CI];
                                                   DecimalPlaces=0:2;
                                                   MinValue=0;
                                                   MaxValue=100;
                                                   BlankNumbers=BlankZero }
    { 120 ;   ;New FCI Pending     ;Boolean       ;CaptionML=[ENU=New FCI Pending;
                                                              PTB=Nova FCI Pendente] }
    { 130 ;   ;FCI File No.        ;Code20        ;TableRelation="FCI File";
                                                   CaptionML=[ENU=FCI File No.;
                                                              PTB=N� Arquivo FCI] }
  }
  KEYS
  {
    {    ;Calculation Date,Branch Code,Calculated Item No.,Item Level,Item No.;
                                                   Clustered=Yes }
    {    ;New FCI Pending,Calculation Date         }
    {    ;FCI File No.                             }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

