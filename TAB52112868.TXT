OBJECT Table 52112868 ECF Posting Code
{
  OBJECT-PROPERTIES
  {
    Date=29/06/15;
    Time=13:59:47;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    OnDelete=VAR
               APartEntriesSetup@52006500 : Record 52112870;
             BEGIN
               APartEntriesSetup.SETRANGE("Posting Code", Code);
               APartEntriesSetup.DELETEALL(TRUE);
             END;

    CaptionML=[ENU=ECF Posting Code;
               PTB=C�digo Lan�amento ECF];
    LookupPageID=Page52112880;
  }
  FIELDS
  {
    { 10  ;   ;Code                ;Code10        ;OnValidate=VAR
                                                                Pos@52006501 : Integer;
                                                                Code1@52006500 : Integer;
                                                                Code2@52006502 : Integer;
                                                              BEGIN
                                                                Pos := STRPOS(Code, '.');
                                                                IF Pos = 0 THEN BEGIN
                                                                  IF EVALUATE("Internal Code", Code) THEN;
                                                                  EXIT;
                                                                END;

                                                                IF EVALUATE(Code1, COPYSTR(Code, 1, Pos - 1)) THEN;
                                                                IF EVALUATE(Code2, COPYSTR(Code, Pos + 1, STRLEN(Code))) THEN;

                                                                "Internal Code" := Code1 + (Code2 / 1000);
                                                              END;

                                                   CaptionML=[ENU=Code;
                                                              PTB=C�digo];
                                                   NotBlank=Yes }
    { 20  ;   ;Description         ;Text250       ;CaptionML=[ENU=Description;
                                                              PTB=Descri��o] }
    { 30  ;   ;Type                ;Option        ;CaptionML=[ENU=Type;
                                                              PTB=Tipo];
                                                   OptionCaptionML=[ENU=" ,Addition,Exclusion,Loss Compensation,Profit";
                                                                    PTB=" ,Adi��o,Exclus�o,Compensa��o de Preju�zo,Lucro"];
                                                   OptionString=[ ,Addition,Exclusion,LossComp,Profit] }
    { 40  ;   ;Accounts            ;Boolean       ;FieldClass=FlowField;
                                                   CalcFormula=Exist("ECF A-Part Entries Setup" WHERE (Posting Code=FIELD(Code)));
                                                   CaptionML=[ENU=Accounts;
                                                              PTB=Contas];
                                                   Editable=No }
    { 1000;   ;Internal Code       ;Decimal       ;CaptionML=[ENU=Internal Code;
                                                              PTB=C�d. Interno];
                                                   DecimalPlaces=2:4 }
  }
  KEYS
  {
    {    ;Internal Code,Code                      ;Clustered=Yes }
  }
  FIELDGROUPS
  {
    { 1   ;DropDown            ;Code,Description,Type                    }
  }
  CODE
  {

    BEGIN
    END.
  }
}

