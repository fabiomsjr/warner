OBJECT Table 99008517 BizTalk Request
{
  OBJECT-PROPERTIES
  {
    Date=29/02/12;
    Time=18:14:20;
    Version List=NAVW16.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=BizTalk Request;
               PTB=Pedido BizTalk];
  }
  FIELDS
  {
    { 1   ;   ;Entry No.           ;Integer       ;CaptionML=[ENU=Entry No.;
                                                              PTB=N� Mov.] }
    { 2   ;   ;Request Type        ;Option        ;CaptionML=[ENU=Request Type;
                                                              PTB=Encomenda de Tipo];
                                                   OptionCaptionML=[ENU=Create Partner Organization,Modify Partner Organization,Add Identifier,Modify Identifier,Remove Identifier;
                                                                    PTB=Criar Organiza��o Parceiro,Modificar Organiza��o Parceiro,Adicionar Identificador,Modificar Identificador,Remover Identificador];
                                                   OptionString=Create Partner Organization,Modify Partner Organization,Add Identifier,Modify Identifier,Remove Identifier }
    { 3   ;   ;BizTalk Organization Name;Text50   ;CaptionML=[ENU=BizTalk Organization Name;
                                                              PTB=Nome Organiza��o BizTalk] }
    { 4   ;   ;BizTalk Partner No. ;Integer       ;CaptionML=[ENU=BizTalk Partner No.;
                                                              PTB=N� Parceiro BizTalk] }
    { 5   ;   ;BizTalk Organization ID;Integer    ;CaptionML=[ENU=BizTalk Organization ID;
                                                              PTB=ID Organiza��o BizTalk] }
    { 6   ;   ;Original BizTalk ID Name;Text50    ;CaptionML=[ENU=Original BizTalk ID Name;
                                                              PTB=Nome ID BizTalk Original] }
    { 7   ;   ;Original BizTalk ID Qualifier;Text50;
                                                   CaptionML=[ENU=Original BizTalk ID Qualifier;
                                                              PTB=Qualif. ID BizTalk Original] }
    { 8   ;   ;Original BizTalk ID Value;Text50   ;CaptionML=[ENU=Original BizTalk ID Value;
                                                              PTB=Valor ID Biztalk Original] }
    { 9   ;   ;New BizTalk ID Name ;Text50        ;CaptionML=[ENU=New BizTalk ID Name;
                                                              PTB=Novo Nome ID BizTalk] }
    { 10  ;   ;New BizTalk ID Qualifier;Text50    ;CaptionML=[ENU=New BizTalk ID Qualifier;
                                                              PTB=Novo Qualif. ID New BizTalk] }
    { 11  ;   ;New BizTalk ID Value;Text50        ;CaptionML=[ENU=New BizTalk ID Value;
                                                              PTB=Novo Valor ID BizTalk] }
    { 12  ;   ;BizTalk Application Name;Text50    ;CaptionML=[ENU=BizTalk Application Name;
                                                              PTB=Nome Aplica��o BizTalk] }
    { 13  ;   ;BizTalk Application ID;Integer     ;CaptionML=[ENU=BizTalk Application ID;
                                                              PTB=ID Aplica��o BizTalk] }
  }
  KEYS
  {
    {    ;Entry No.                               ;Clustered=Yes }
    {    ;BizTalk Partner No.,Request Type         }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

