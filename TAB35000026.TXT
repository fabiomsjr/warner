OBJECT Table 35000026 DIPJ -Entrada/Sa�da Insumos2
{
  OBJECT-PROPERTIES
  {
    Date=29/02/12;
    Time=18:14:18;
    Version List=NAVBR5.00,NAVBR5.00.02,NAVBR5.01.00,NAVBR5.01.0909;
  }
  PROPERTIES
  {
    CaptionML=[ENU=CFOP Abstract;
               PTB=DIPJ -Entrada/Sa�da Insumos];
  }
  FIELDS
  {
    { 1   ;   ;CFOP Code           ;Code10        ;CaptionML=[ENU=CFOP Code;
                                                              PTB=C�d. CFOP] }
    { 2   ;   ;G/L Amount          ;Decimal       ;CaptionML=[ENU=G/L Amount;
                                                              PTB=Valor Cont�bil] }
    { 3   ;   ;ICMS Basis Amount   ;Decimal       ;CaptionML=[ENU=ICMS Basis Amount;
                                                              PTB=Valor Base ICMS] }
    { 4   ;   ;ICMS Exempt Amount  ;Decimal       ;CaptionML=[ENU=ICMS Exempt Amount;
                                                              PTB=Valor Isentas ICMS] }
    { 5   ;   ;ICMS Others Amount  ;Decimal       ;CaptionML=[ENU=ICMS Others Amount;
                                                              PTB=Valor ICMS Outros] }
    { 6   ;   ;IPI Basis Amount    ;Decimal       ;CaptionML=[ENU=IPI Basis Amount;
                                                              PTB=Valor Base IPI] }
    { 7   ;   ;IPI Exempt Amount   ;Decimal       ;CaptionML=[ENU=IPI Exempt Amount;
                                                              PTB=Valor IPI Isento] }
    { 8   ;   ;IPI Others Amount   ;Decimal       ;CaptionML=[ENU=IPI Others Amount;
                                                              PTB=Valor IPI Outros] }
    { 9   ;   ;ICMS Amount         ;Decimal       ;CaptionML=[ENU=ICMS Amount;
                                                              PTB=Valor ICMS] }
    { 10  ;   ;IPI Amount          ;Decimal       ;CaptionML=[ENU=IPI Amount;
                                                              PTB=Valor IPI] }
    { 11  ;   ;CFOP 1st. Dig.      ;Integer       ;CaptionML=[ENU=CFOP 1st. Dig.;
                                                              PTB=CFOP 1a. Dig.] }
    { 12  ;   ;CFOP Description    ;Text50        ;CaptionML=[ENU=CFOP Description;
                                                              PTB=Descr. CFOP] }
    { 13  ;   ;G/L Amount 1        ;Decimal       ;CaptionML=[ENU=G/L Amount 1;
                                                              PTB=Valor Cont�bil 1] }
    { 14  ;   ;G/L Amount 2        ;Decimal       ;CaptionML=[ENU=G/L Amount 2;
                                                              PTB=Valor Cont�bil 2] }
    { 15  ;   ;G/L Amount 3        ;Decimal       ;CaptionML=[ENU=G/L Amount 3;
                                                              PTB=Valor Cont�bil 3] }
    { 31  ;   ;Fiscal Type         ;Option        ;CaptionML=[ENU=Fiscal Type;
                                                              PTB=Tipo Fiscal];
                                                   OptionCaptionML=[ENU=" ,Input,Output";
                                                                    PTB=" ,Entrada,Sa�da"];
                                                   OptionString=[ ,Input,Output] }
    { 32  ;   ;User ID             ;Code20        ;CaptionML=[ENU=User ID;
                                                              PTB=ID Usu�rio] }
    { 33  ;   ;Tax Settlement Code ;Code20        ;CaptionML=[ENU=Tax Settlement Code;
                                                              PTB=C�d. Apura��o Imp.] }
    { 34  ;   ;NCM Code            ;Code20        ;TableRelation="NCM Codes2";
                                                   CaptionML=[ENU=NCM Code;
                                                              PTB=C�digo NCM];
                                                   Description=MBS/BR.01 }
    { 35  ;   ;Vendor / Customer No.;Code10        }
  }
  KEYS
  {
    {    ;CFOP Code,User ID,Tax Settlement Code,NCM Code;
                                                   Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    {
      MBS/BR

      No.   dd.mm.yy   Developer   Company   DocNo.   Description
      -----------------------------------------------------------------------------------------------------------------
      00    01.12.06   MK          MICROSOFT LOC001   The reference Documentation file LOCALIZATIONBR.DOC
    }
    END.
  }
}

