OBJECT Table 52112454 Retained Tax Buffer
{
  OBJECT-PROPERTIES
  {
    Date=25/04/14;
    Time=08:31:03;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
  }
  FIELDS
  {
    { 10  ;   ;Tax Identification  ;Option        ;OptionCaptionML=[ENU=" ,IPI,ICMS,PIS,COFINS,II,IRRF,INSS,ISS,CSL Ret.,PIS Ret.,COFINS Ret.,ISS Ret.,INSS Ret.,Dif. Aliq. ICMS,ST,PCC,CIDE,Funrural,Simples";
                                                                    PTB=" ,IPI,ICMS,PIS,COFINS,II,IRRF,INSS,ISS,CSL Ret.,PIS Ret.,COFINS Ret.,ISS Ret.,INSS Ret.,Dif. Aliq. ICMS,ST,PCC,CIDE,Funrural,Simples"];
                                                   OptionString=[ ,IPI,ICMS,PIS,COFINS,II,IRRF,INSS,ISS,CSL Ret.,PIS Ret.,COFINS Ret.,ISS Ret.,INSS Ret.,Dif. Aliq. ICMS,ST,PCC,CIDE,Funrural,Simples] }
    { 18  ;   ;DocBase             ;Decimal        }
    { 20  ;   ;AcumBase            ;Decimal        }
    { 21  ;   ;CurrTax             ;Decimal        }
    { 22  ;   ;TaxAccount          ;Code20         }
    { 23  ;   ;Percent             ;Decimal        }
    { 27  ;   ;MinAmount           ;Decimal        }
    { 28  ;   ;MonthTotalBase      ;Decimal        }
    { 29  ;   ;MonthTotalTax       ;Decimal        }
    { 30  ;   ;VatEntryNo          ;Integer        }
    { 40  ;   ;DueDate             ;Date           }
  }
  KEYS
  {
    {    ;Tax Identification                      ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

