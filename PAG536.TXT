OBJECT Page 536 Dimensions
{
  OBJECT-PROPERTIES
  {
    Date=21/03/12;
    Time=13:04:27;
    Modified=Yes;
    Version List=NAVW18.00,WCMW1601_02_2NAVW16.00.01;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Dimensions;
               PTB=Dimens�es];
    SourceTable=Table348;
    PageType=List;
    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 19      ;1   ;ActionGroup;
                      CaptionML=[ENU=&Dimension;
                                 PTB=&Dimension];
                      Image=Dimensions }
      { 20      ;2   ;Action    ;
                      CaptionML=[ENU=Dimension &Values;
                                 PTB=Dimension &Values];
                      RunObject=Page 537;
                      RunPageLink=Dimension Code=FIELD(Code);
                      Promoted=Yes;
                      Image=Dimensions;
                      PromotedCategory=Process }
      { 21      ;2   ;Action    ;
                      CaptionML=[ENU=Account Type De&fault Dim.;
                                 PTB=Account T�pe De&fault Dim.];
                      RunObject=Page 541;
                      RunPageLink=Dimension Code=FIELD(Code),
                                  No.=CONST();
                      Image=DefaultDimension }
      { 22      ;2   ;Action    ;
                      CaptionML=[ENU=Translations;
                                 PTB=Translations];
                      RunObject=Page 580;
                      RunPageLink=Code=FIELD(Code);
                      Image=Translations }
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 25      ;1   ;ActionGroup;
                      CaptionML=[ENU=F&unctions;
                                 PTB=F&un��es];
                      Image=Action }
      { 29      ;2   ;Action    ;
                      Name=MapToICDimWithSameCode;
                      CaptionML=[ENU=Map to IC Dim. with Same Code;
                                 PTB=Map to IC Dim. with Same Code];
                      Image=MapDimensions;
                      OnAction=VAR
                                 Dimension@1000 : Record 348;
                                 ICMapping@1001 : Codeunit 428;
                               BEGIN
                                 CurrPage.SETSELECTIONFILTER(Dimension);
                                 IF Dimension.FIND('-') AND CONFIRM(Text000) THEN
                                   REPEAT
                                     ICMapping.MapOutgoingICDimensions(Dimension);
                                   UNTIL Dimension.NEXT = 0;
                               END;
                                }
      { 1102300007;2 ;Action    ;
                      Name=<Action1000000000>;
                      CaptionML=ENU=Import Group Dimensions;
                      Promoted=Yes;
                      Image=Import;
                      PromotedCategory=Category4;
                      OnAction=BEGIN
                                 Utils.ImportGroupDimensionValues;
                               END;
                                }
      { 1102300006;2 ;Action    ;
                      Name=<Action1000000001>;
                      CaptionML=ENU=Export Group Dimensions;
                      Promoted=Yes;
                      Image=Export;
                      PromotedCategory=Category4;
                      OnAction=BEGIN
                                 Utils.ExportGroupDimensionValues;
                               END;
                                }
      { 1102300005;2 ;Action    ;
                      Name=<Action1000000003>;
                      CaptionML=ENU=Selected Local Dim and Values;
                      ToolTipML=ENU=Sync Selected Local Dim and Values;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=GL;
                      PromotedCategory=Category5;
                      OnAction=VAR
                                 Var_Dim@1000000002 : Record 348;
                                 Var_Utilities@1000000000 : Codeunit 60000;
                               BEGIN
                                 // -002
                                 CurrPage.SETSELECTIONFILTER(Var_Dim);
                                 Var_Utilities.UpdateSelectedDimAndDimValue(Var_Dim);
                                 // +002
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 1102300000;2;Field  ;
                SourceExpr="Group Code" }

    { 2   ;2   ;Field     ;
                SourceExpr=Code }

    { 4   ;2   ;Field     ;
                SourceExpr=Name }

    { 6   ;2   ;Field     ;
                SourceExpr="Code Caption" }

    { 8   ;2   ;Field     ;
                SourceExpr="Filter Caption" }

    { 10  ;2   ;Field     ;
                SourceExpr=Description }

    { 15  ;2   ;Field     ;
                SourceExpr=Blocked }

    { 23  ;2   ;Field     ;
                SourceExpr="Map-to IC Dimension Code";
                Visible=FALSE }

    { 17  ;2   ;Field     ;
                SourceExpr="Consolidation Code";
                Visible=FALSE }

    { 1102300004;2;Group  ;
                CaptionML=ENU=WCM;
                GroupType=Group }

    { 1102300003;3;Field  ;
                SourceExpr="Source Company";
                Editable=False }

    { 1102300002;3;Field  ;
                SourceExpr="Sync Date";
                Editable=False }

    { 1102300001;3;Field  ;
                SourceExpr="Sync User";
                Editable=False }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      Text000@1000 : TextConst 'ENU=Are you sure you want to map the selected lines?;PTB=Voc� tem certeza que deseja mapear as linhas selecionadas?';
      Utils@1102300000 : Codeunit 60000;

    BEGIN
    {
      001  08/09/09  CC  WCMFSD002   Added Import and Export options to Actions
                                     Added Group Code to form
      002  27/05/10  JJB  WCMFSD020  Add Action for Sync Local Dim
      003  27/05/10  JJB  WCMFSD020  Add Group called - WCM and added some fields onto it.
    }
    END.
  }
}

