OBJECT Page 14 Salespeople/Purchasers
{
  OBJECT-PROPERTIES
  {
    Date=09/09/14;
    Time=12:00:00;
    Version List=NAVW18.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Salespeople/Purchasers;
               PTB=Vendedores/Compradores];
    SourceTable=Table13;
    PageType=List;
    CardPageID=Salesperson/Purchaser Card;
    OnInit=VAR
             SegmentLine@1000 : Record 5077;
           BEGIN
             CreateInteractionVisible := SegmentLine.READPERMISSION;
           END;

    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 11      ;1   ;ActionGroup;
                      CaptionML=[ENU=&Salesperson;
                                 PTB=&Vendedores];
                      Image=SalesPerson }
      { 14      ;2   ;Action    ;
                      CaptionML=[ENU=Tea&ms;
                                 PTB=Equi&pes];
                      RunObject=Page 5107;
                      RunPageView=SORTING(Salesperson Code);
                      RunPageLink=Salesperson Code=FIELD(Code);
                      Image=TeamSales }
      { 15      ;2   ;Action    ;
                      CaptionML=[ENU=Con&tacts;
                                 PTB=Con&tatos];
                      RunObject=Page 5052;
                      RunPageView=SORTING(Salesperson Code);
                      RunPageLink=Salesperson Code=FIELD(Code);
                      Image=CustomerContact }
      { 26      ;2   ;ActionGroup;
                      CaptionML=[ENU=Dimensions;
                                 PTB=Dimens�es];
                      Image=Dimensions }
      { 18      ;3   ;Action    ;
                      ShortCutKey=Shift+Ctrl+D;
                      CaptionML=[ENU=Dimensions-Single;
                                 PTB=Dimens�es-Simples];
                      RunObject=Page 540;
                      RunPageLink=Table ID=CONST(13),
                                  No.=FIELD(Code);
                      Image=Dimensions }
      { 27      ;3   ;Action    ;
                      AccessByPermission=TableData 348=R;
                      CaptionML=[ENU=Dimensions-&Multiple;
                                 PTB=Dimens�es-&Multiplas];
                      Image=DimensionSets;
                      OnAction=VAR
                                 SalespersonPurchaser@1001 : Record 13;
                                 DefaultDimMultiple@1002 : Page 542;
                               BEGIN
                                 CurrPage.SETSELECTIONFILTER(SalespersonPurchaser);
                                 DefaultDimMultiple.SetMultiSalesperson(SalespersonPurchaser);
                                 DefaultDimMultiple.RUNMODAL;
                               END;
                                }
      { 19      ;2   ;Action    ;
                      ShortCutKey=F7;
                      CaptionML=[ENU=Statistics;
                                 PTB=Estatisticas];
                      RunObject=Page 5117;
                      RunPageLink=Code=FIELD(Code);
                      Promoted=Yes;
                      Image=Statistics;
                      PromotedCategory=Process }
      { 20      ;2   ;Action    ;
                      CaptionML=[ENU=C&ampaigns;
                                 PTB=C&ampanhas];
                      RunObject=Page 5087;
                      RunPageView=SORTING(Salesperson Code);
                      RunPageLink=Salesperson Code=FIELD(Code);
                      Image=Campaign }
      { 21      ;2   ;Action    ;
                      CaptionML=[ENU=S&egments;
                                 PTB=S&egmentos];
                      RunObject=Page 5093;
                      RunPageView=SORTING(Salesperson Code);
                      RunPageLink=Salesperson Code=FIELD(Code);
                      Image=Segment }
      { 22      ;2   ;Separator ;
                      CaptionML=[ENU="";
                                 PTB=""] }
      { 23      ;2   ;Action    ;
                      ShortCutKey=Ctrl+F7;
                      CaptionML=[ENU=Interaction Log E&ntries;
                                 PTB=Movs. I&ntera��o];
                      RunObject=Page 5076;
                      RunPageView=SORTING(Salesperson Code);
                      RunPageLink=Salesperson Code=FIELD(Code);
                      Image=InteractionLog }
      { 29      ;2   ;Action    ;
                      CaptionML=[ENU=Postponed &Interactions;
                                 PTB=&Intera��es Adiadas];
                      RunObject=Page 5082;
                      RunPageView=SORTING(Salesperson Code);
                      RunPageLink=Salesperson Code=FIELD(Code);
                      Image=PostponedInteractions }
      { 24      ;2   ;Action    ;
                      CaptionML=[ENU=T&o-dos;
                                 PTB=&A��es a Efetuar];
                      RunObject=Page 5096;
                      RunPageView=SORTING(Salesperson Code);
                      RunPageLink=Salesperson Code=FIELD(Code),
                                  System To-do Type=FILTER(Organizer|Salesperson Attendee);
                      Image=TaskList }
      { 30      ;2   ;ActionGroup;
                      CaptionML=[ENU=Oppo&rtunities;
                                 PTB=Opo&rtunidades];
                      Image=OpportunityList }
      { 25      ;3   ;Action    ;
                      CaptionML=[ENU=List;
                                 PTB=Lista];
                      RunObject=Page 5123;
                      RunPageView=SORTING(Salesperson Code);
                      RunPageLink=Salesperson Code=FIELD(Code);
                      Image=OpportunitiesList }
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 17      ;1   ;Action    ;
                      Name=CreateInteraction;
                      AccessByPermission=TableData 5062=R;
                      Ellipsis=Yes;
                      CaptionML=[ENU=Create &Interaction;
                                 PTB=Criar &Intera��o];
                      Promoted=Yes;
                      Visible=CreateInteractionVisible;
                      Image=CreateInteraction;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 CreateInteraction;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                SourceExpr=Code }

    { 4   ;2   ;Field     ;
                SourceExpr=Name }

    { 6   ;2   ;Field     ;
                SourceExpr="Commission %" }

    { 12  ;2   ;Field     ;
                SourceExpr="Phone No." }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      CreateInteractionVisible@1000 : Boolean INDATASET;

    BEGIN
    END.
  }
}

