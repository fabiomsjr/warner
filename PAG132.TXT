OBJECT Page 132 Posted Sales Invoice
{
  OBJECT-PROPERTIES
  {
    Date=09/09/14;
    Time=12:00:00;
    Version List=NAVW18.00,NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Posted Sales Invoice;
               PTB=Hist. Nota Fiscal Venda];
    InsertAllowed=No;
    SourceTable=Table112;
    PageType=Document;
    RefreshOnActivate=Yes;
    OnOpenPage=BEGIN
                 SetSecurityFilterOnRespCenter;
               END;

    OnAfterGetCurrRecord=BEGIN
                           // > NAVBR
                           ShowNFSeLink := "E-Invoice Verification Code" <> '';
                           // < NAVBR
                         END;

    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 55      ;1   ;ActionGroup;
                      CaptionML=[ENU=&Invoice;
                                 PTB=&Nota Fiscal];
                      Image=Invoice }
      { 8       ;2   ;Action    ;
                      Name=Statistics;
                      ShortCutKey=F7;
                      CaptionML=[ENU=Statistics;
                                 PTB=Estat�sticas];
                      RunObject=Page 397;
                      RunPageLink=No.=FIELD(No.);
                      Promoted=Yes;
                      Image=Statistics;
                      PromotedCategory=Process }
      { 57      ;2   ;Action    ;
                      CaptionML=[ENU=Co&mments;
                                 PTB=Co&ment�rios];
                      RunObject=Page 67;
                      RunPageLink=Document Type=CONST(Posted Invoice),
                                  No.=FIELD(No.),
                                  Document Line No.=CONST(0);
                      Image=ViewComments }
      { 89      ;2   ;Action    ;
                      AccessByPermission=TableData 348=R;
                      ShortCutKey=Shift+Ctrl+D;
                      CaptionML=[ENU=Dimensions;
                                 PTB=Dimens�es];
                      Image=Dimensions;
                      OnAction=BEGIN
                                 ShowDimensions;
                               END;
                                }
      { 112     ;2   ;Action    ;
                      CaptionML=[ENU=Approvals;
                                 PTB=Aprova��es];
                      Image=Approvals;
                      OnAction=VAR
                                 PostedApprovalEntries@1001 : Page 659;
                               BEGIN
                                 PostedApprovalEntries.Setfilters(DATABASE::"Sales Invoice Header","No.");
                                 PostedApprovalEntries.RUN;
                               END;
                                }
      { 35000027;2   ;Action    ;
                      CaptionML=[ENU=Additional Text;
                                 PTB=Textos Adicionais];
                      RunObject=Page 52112436;
                      RunPageView=SORTING(Type,Document Type,Document No.,Order No.);
                      RunPageLink=Type=CONST(Sale),
                                  Document Type=CONST(Post. Invoice),
                                  Document No.=FIELD(No.);
                      Image=Text }
      { 52006518;2   ;Action    ;
                      Name=OpenFiles;
                      CaptionML=[ENU=Files;
                                 PTB=Arquivos];
                      Image=ExternalDocument;
                      OnAction=VAR
                                 docFileMgt@52006500 : Codeunit 52112428;
                               BEGIN
                                 docFileMgt.OpenFiles(Rec);
                               END;
                                }
      { 171     ;2   ;Separator  }
      { 172     ;2   ;Action    ;
                      CaptionML=[ENU=Credit Cards Transaction Lo&g Entries;
                                 PTB=Log Transa��es Cr�dito];
                      RunObject=Page 829;
                      RunPageLink=Document Type=CONST(Payment),
                                  Document No.=FIELD(No.),
                                  Customer No.=FIELD(Bill-to Customer No.);
                      Image=CreditCardLog }
      { 52006532;1   ;ActionGroup;
                      Name=AdditionalData;
                      CaptionML=[ENU=Additional Data;
                                 PTB=Dados Adicionais];
                      Image=ExtendedDataEntry }
      { 52006531;2   ;Action    ;
                      Name=ActionIndirectExportation;
                      CaptionML=[ENU=Indirect Exportation;
                                 PTB=Exporta��o Indireta];
                      RunObject=Page 52112514;
                      RunPageLink=Table ID=CONST(113),
                                  Document No. Filter=FIELD(No.);
                      Image=ExtendedDataEntry }
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 58      ;1   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[ENU=&Print;
                                 PTB=Im&primir];
                      Promoted=Yes;
                      Image=Print;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 CurrPage.SETSELECTIONFILTER(SalesInvHeader);
                                 SalesInvHeader.PrintRecords(TRUE);
                               END;
                                }
      { 9       ;1   ;Action    ;
                      CaptionML=ENU=&Email;
                      Promoted=Yes;
                      Image=Email;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 SalesInvHeader := Rec;
                                 CurrPage.SETSELECTIONFILTER(SalesInvHeader);
                                 SalesInvHeader.EmailRecords(FALSE);
                               END;
                                }
      { 59      ;1   ;Action    ;
                      CaptionML=[ENU=&Navigate;
                                 PTB=&Navegar];
                      Promoted=Yes;
                      Image=Navigate;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 Navigate;
                               END;
                                }
      { 35000012;1   ;ActionGroup;
                      CaptionML=[ENU=NF-e;
                                 PTB=NF-e];
                      Image=Sales }
      { 35000013;2   ;Action    ;
                      CaptionML=[ENU=Transmit;
                                 PTB=Transmitir];
                      Promoted=Yes;
                      Image=SendElectronicDocument;
                      PromotedCategory=Process;
                      OnAction=VAR
                                 navNFeDocFunctions@35000000 : Codeunit 52112624;
                               BEGIN
                                 navNFeDocFunctions.SalesReception(Rec);
                               END;
                                }
      { 35000014;2   ;Action    ;
                      CaptionML=[ENU=Verify;
                                 PTB=Consultar];
                      Image=Import;
                      OnAction=VAR
                                 navNFeDocFunctions@35000000 : Codeunit 52112624;
                               BEGIN
                                 navNFeDocFunctions.SalesVerify(Rec);
                               END;
                                }
      { 35000015;2   ;Separator  }
      { 35000016;2   ;Action    ;
                      CaptionML=[ENU=View DANFe;
                                 PTB=Visualizar DANFe];
                      Promoted=Yes;
                      Image=Invoice;
                      PromotedCategory=Process;
                      OnAction=VAR
                                 navNFeDocFunctions@35000000 : Codeunit 52112624;
                               BEGIN
                                 navNFeDocFunctions.SalesViewDANFe(Rec);
                               END;
                                }
      { 35000017;2   ;Action    ;
                      CaptionML=[ENU=View XML;
                                 PTB=Visualizar XML];
                      Promoted=Yes;
                      Image=XMLFile;
                      PromotedCategory=Process;
                      OnAction=VAR
                                 navNFeDocFunctions@35000000 : Codeunit 52112624;
                               BEGIN
                                 navNFeDocFunctions.SalesViewXML(Rec);
                               END;
                                }
      { 35000018;2   ;Separator  }
      { 35000019;2   ;Action    ;
                      CaptionML=[ENU=View DANFe (Test);
                                 PTB=Visualizar DANFe (Teste)];
                      Promoted=Yes;
                      Image=Invoice;
                      PromotedCategory=Process;
                      OnAction=VAR
                                 navNFeDocFunctions@35000000 : Codeunit 52112624;
                               BEGIN
                                 navNFeDocFunctions.SalesPreviewDANFe(Rec);
                               END;
                                }
      { 35000020;2   ;Action    ;
                      CaptionML=[ENU=View XML (Test);
                                 PTB=Visualizar XML (Teste)];
                      Promoted=Yes;
                      Image=XMLFile;
                      PromotedCategory=Process;
                      OnAction=VAR
                                 navNFeDocFunctions@35000000 : Codeunit 52112624;
                               BEGIN
                                 navNFeDocFunctions.SalesPreviewXML(Rec);
                               END;
                                }
      { 35000021;2   ;Separator  }
      { 35000022;2   ;Action    ;
                      CaptionML=[ENU=Discard Number;
                                 PTB=Inutilizar N�mero];
                      Image=Reject;
                      OnAction=VAR
                                 navNFeDocFunctions@35000000 : Codeunit 52112624;
                               BEGIN
                                 navNFeDocFunctions.SalesDiscardNo(Rec);
                               END;
                                }
      { 35000023;2   ;Separator  }
      { 35000024;2   ;Action    ;
                      CaptionML=[ENU=Export to Signer;
                                 PTB=Exportar para Assinador];
                      Image=Export;
                      OnAction=VAR
                                 navNFeDocFunctions@35000000 : Codeunit 52112624;
                               BEGIN
                                 navNFeDocFunctions.SalesExport(Rec);
                               END;
                                }
      { 35000025;1   ;ActionGroup;
                      CaptionML=[ENU=NFS-e;
                                 PTB=NFS-e] }
      { 35000026;2   ;Action    ;
                      CaptionML=[ENU=Open Link;
                                 PTB=Abrir Link];
                      Visible=ShowNFSeLink;
                      Image=Link;
                      OnAction=VAR
                                 nfse@35000000 : Codeunit 52112625;
                               BEGIN
                                 nfse.AbrirLinkSales(Rec);
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                CaptionML=[ENU=General;
                           PTB=Geral] }

    { 2   ;2   ;Field     ;
                SourceExpr="No.";
                Importance=Promoted;
                Editable=FALSE }

    { 4   ;2   ;Field     ;
                SourceExpr="Sell-to Customer No.";
                Importance=Promoted;
                Editable=FALSE }

    { 96  ;2   ;Field     ;
                SourceExpr="Sell-to Contact No.";
                Editable=FALSE }

    { 61  ;2   ;Field     ;
                SourceExpr="Sell-to Customer Name";
                Editable=FALSE }

    { 63  ;2   ;Field     ;
                SourceExpr="Sell-to Address";
                Editable=FALSE }

    { 52006523;2;Field    ;
                SourceExpr="Sell-to Number";
                Editable=FALSE }

    { 65  ;2   ;Field     ;
                SourceExpr="Sell-to Address 2";
                Editable=FALSE }

    { 6   ;2   ;Field     ;
                SourceExpr="Sell-to Post Code";
                Editable=FALSE }

    { 1102300001;2;Field  ;
                SourceExpr="Sell-to District";
                Editable=FALSE }

    { 67  ;2   ;Field     ;
                SourceExpr="Sell-to City";
                Editable=FALSE }

    { 1102300000;2;Field  ;
                SourceExpr="Sell-to Territory Code";
                Editable=FALSE }

    { 69  ;2   ;Field     ;
                SourceExpr="Sell-to Contact";
                Editable=FALSE }

    { 12  ;2   ;Field     ;
                SourceExpr="Posting Date";
                Importance=Promoted;
                Editable=FALSE }

    { 25  ;2   ;Field     ;
                SourceExpr="Document Date";
                Editable=FALSE }

    { 52006524;2;Field    ;
                SourceExpr="Your Reference";
                Editable=FALSE }

    { 114 ;2   ;Field     ;
                SourceExpr="Quote No." }

    { 86  ;2   ;Field     ;
                SourceExpr="Order No.";
                Importance=Promoted;
                Editable=FALSE }

    { 73  ;2   ;Field     ;
                SourceExpr="Pre-Assigned No.";
                Editable=FALSE }

    { 94  ;2   ;Field     ;
                SourceExpr="External Document No.";
                Importance=Promoted;
                Editable=FALSE }

    { 10  ;2   ;Field     ;
                SourceExpr="Salesperson Code";
                Editable=FALSE }

    { 92  ;2   ;Field     ;
                SourceExpr="Responsibility Center";
                Editable=FALSE }

    { 14  ;2   ;Field     ;
                SourceExpr="No. Printed";
                Editable=FALSE }

    { 52006517;1;Group    ;
                CaptionML=[ENU=National;
                           PTB=Nacional];
                Editable=FALSE;
                GroupType=Group }

    { 52006516;2;Field    ;
                SourceExpr="Branch Code";
                Importance=Promoted }

    { 52006519;2;Field    ;
                SourceExpr="Taxes Matrix Code" }

    { 52006515;2;Field    ;
                SourceExpr="Tax Area Code";
                Importance=Promoted }

    { 52006514;2;Field    ;
                SourceExpr="Fiscal Document Type";
                Importance=Promoted }

    { 52006513;2;Field    ;
                SourceExpr="Print Serie" }

    { 52006512;2;Field    ;
                SourceExpr="Print Sub Serie" }

    { 52006511;2;Field    ;
                SourceExpr="CFOP Code" }

    { 52006510;2;Field    ;
                SourceExpr="Operation Nature" }

    { 52006509;2;Field    ;
                SourceExpr="Complementary Invoice Type" }

    { 52006508;2;Field    ;
                SourceExpr="Invoice to Complement";
                Editable=FALSE }

    { 52006507;2;Field    ;
                SourceExpr="Operation Type" }

    { 52006506;2;Field    ;
                SourceExpr="Fiscal Book Remarks" }

    { 52006505;2;Field    ;
                SourceExpr="End User" }

    { 52006504;2;Field    ;
                SourceExpr="Credit Memos" }

    { 52006522;2;Field    ;
                SourceExpr="Return Invoices" }

    { 52006520;2;Field    ;
                SourceExpr="Posting Description";
                Importance=Additional }

    { 52006521;2;Field    ;
                SourceExpr="Additional Description";
                Importance=Additional }

    { 52006528;2;Field    ;
                SourceExpr="Buyer Presence" }

    { 54  ;1   ;Part      ;
                Name=SalesInvLines;
                SubPageLink=Document No.=FIELD(No.);
                PagePartID=Page133 }

    { 1905885101;1;Group  ;
                CaptionML=[ENU=Invoicing;
                           PTB=Faturamento] }

    { 16  ;2   ;Field     ;
                SourceExpr="Bill-to Customer No.";
                Importance=Promoted;
                Editable=FALSE }

    { 98  ;2   ;Field     ;
                SourceExpr="Bill-to Contact No.";
                Editable=FALSE }

    { 18  ;2   ;Field     ;
                SourceExpr="Bill-to Name";
                Editable=FALSE }

    { 20  ;2   ;Field     ;
                SourceExpr="Bill-to Address";
                Editable=FALSE }

    { 52006527;2;Field    ;
                SourceExpr="Bill-to Number";
                Editable=FALSE }

    { 22  ;2   ;Field     ;
                SourceExpr="Bill-to Address 2";
                Editable=FALSE }

    { 78  ;2   ;Field     ;
                SourceExpr="Bill-to Post Code";
                Editable=FALSE }

    { 24  ;2   ;Field     ;
                SourceExpr="Bill-to City";
                Editable=FALSE }

    { 26  ;2   ;Field     ;
                SourceExpr="Bill-to Contact";
                Editable=FALSE }

    { 7   ;2   ;Field     ;
                SourceExpr="Shortcut Dimension 1 Code";
                Editable=FALSE }

    { 74  ;2   ;Field     ;
                SourceExpr="Shortcut Dimension 2 Code";
                Editable=FALSE }

    { 28  ;2   ;Field     ;
                SourceExpr="Payment Terms Code";
                Importance=Promoted;
                Editable=FALSE }

    { 30  ;2   ;Field     ;
                SourceExpr="Due Date";
                Importance=Promoted;
                Editable=FALSE }

    { 32  ;2   ;Field     ;
                SourceExpr="Payment Discount %";
                Editable=FALSE }

    { 34  ;2   ;Field     ;
                SourceExpr="Pmt. Discount Date";
                Editable=FALSE }

    { 82  ;2   ;Field     ;
                SourceExpr="Payment Method Code";
                Editable=FALSE }

    { 3   ;2   ;Field     ;
                SourceExpr="Direct Debit Mandate ID";
                Editable=FALSE }

    { 127 ;2   ;Field     ;
                SourceExpr="Credit Card No.";
                Editable=FALSE }

    { 126 ;2   ;Field     ;
                CaptionML=[ENU=Cr. Card Number (Last 4 Digits);
                           PTB=Cr. Cart�o Number (Last 4 Digits)];
                SourceExpr=GetCreditcardNumber;
                Editable=FALSE }

    { 1906801201;1;Group  ;
                CaptionML=[ENU=Shipping;
                           PTB=Envio] }

    { 36  ;2   ;Field     ;
                SourceExpr="Ship-to Code";
                Importance=Promoted;
                Editable=FALSE }

    { 38  ;2   ;Field     ;
                SourceExpr="Ship-to Name";
                Editable=FALSE }

    { 40  ;2   ;Field     ;
                SourceExpr="Ship-to Address";
                Editable=FALSE }

    { 52006526;2;Field    ;
                SourceExpr="Ship-to Number";
                Editable=FALSE }

    { 42  ;2   ;Field     ;
                SourceExpr="Ship-to Address 2";
                Editable=FALSE }

    { 80  ;2   ;Field     ;
                CaptionML=PTB=Ship-to CEP/Cidade;
                SourceExpr="Ship-to Post Code";
                Importance=Promoted;
                Editable=FALSE }

    { 44  ;2   ;Field     ;
                SourceExpr="Ship-to City";
                Editable=FALSE }

    { 46  ;2   ;Field     ;
                SourceExpr="Ship-to Contact";
                Editable=FALSE }

    { 76  ;2   ;Field     ;
                SourceExpr="Location Code";
                Editable=FALSE }

    { 48  ;2   ;Field     ;
                SourceExpr="Shipment Method Code";
                Editable=FALSE }

    { 50  ;2   ;Field     ;
                SourceExpr="Shipment Date";
                Importance=Promoted;
                Editable=FALSE }

    { 1102300010;2;Field  ;
                SourceExpr="Freight Billed To";
                Editable=FALSE }

    { 1102300009;2;Field  ;
                SourceExpr="Shipping Agent Code";
                Editable=FALSE }

    { 1102300008;2;Field  ;
                SourceExpr=Marks;
                Editable=FALSE }

    { 1102300007;2;Field  ;
                SourceExpr="Transported Quantity";
                Editable=FALSE }

    { 1102300006;2;Field  ;
                SourceExpr="Transport Number";
                Editable=FALSE }

    { 1102300005;2;Field  ;
                SourceExpr="Gross Weight";
                Editable=FALSE }

    { 1102300004;2;Field  ;
                SourceExpr="Net Weight";
                Editable=FALSE }

    { 1102300003;2;Field  ;
                SourceExpr="License Plate";
                Editable=FALSE }

    { 1102300002;2;Field  ;
                SourceExpr="UF Vehicle";
                Editable=FALSE }

    { 1907468901;1;Group  ;
                CaptionML=[ENU=Foreign Trade;
                           PTB=Internacional] }

    { 87  ;2   ;Field     ;
                SourceExpr="Currency Code";
                Importance=Promoted;
                OnAssistEdit=BEGIN
                               ChangeExchangeRate.SetParameter("Currency Code","Currency Factor","Posting Date");
                               ChangeExchangeRate.EDITABLE(FALSE);
                               IF ChangeExchangeRate.RUNMODAL = ACTION::OK THEN BEGIN
                                 "Currency Factor" := ChangeExchangeRate.GetParameter;
                                 MODIFY;
                               END;
                               CLEAR(ChangeExchangeRate);
                             END;
                              }

    { 52  ;2   ;Field     ;
                SourceExpr="EU 3-Party Trade";
                Editable=FALSE }

    { 52006503;1;Group    ;
                CaptionML=[ENU=Exportation;
                           PTB=Exporta��o];
                Editable=FALSE;
                GroupType=Group }

    { 52006502;2;Field    ;
                SourceExpr="UF Boarding" }

    { 52006501;2;Field    ;
                SourceExpr="Boarding Locate" }

    { 52006500;2;Part     ;
                SubPageLink=Invoice Type=CONST(Posted Invoice),
                            Document No.=FIELD(No.);
                PagePartID=Page52112471;
                PartType=Page }

    { 1102300027;1;Group  ;
                CaptionML=[ENU=NF-e;
                           PTB=NF-e];
                Editable=FALSE;
                GroupType=Group }

    { 52006525;2;Field    ;
                SourceExpr="Issue Date/Time";
                Editable=FALSE }

    { 1102300028;2;Field  ;
                SourceExpr="NFe Chave Acesso" }

    { 1102300029;2;Field  ;
                SourceExpr="NFe Data Envio" }

    { 1102300030;2;Field  ;
                SourceExpr="NFe Ambiente" }

    { 1102300031;2;Field  ;
                SourceExpr="NFe Protocolo" }

    { 1102300032;2;Field  ;
                SourceExpr="NFe cStat" }

    { 1102300033;2;Field  ;
                SourceExpr="NFe xMotivo" }

    { 35000004;1;Group    ;
                CaptionML=[ENU=NFS-e;
                           PTB=NFS-e];
                Editable=FALSE;
                GroupType=Group }

    { 35000003;2;Field    ;
                SourceExpr="Service E-Invoice";
                Editable=FALSE }

    { 52006530;2;Field    ;
                SourceExpr="Service Delivery Date";
                Editable=FALSE }

    { 35000000;2;Field    ;
                SourceExpr="Service Delivery City";
                Editable=FALSE }

    { 35000001;2;Field    ;
                SourceExpr="NFS-e Tributation Code";
                Editable=FALSE }

    { 35000005;2;Field    ;
                SourceExpr="E-Invoice File Generated" }

    { 35000006;2;Field    ;
                SourceExpr="E-Invoice No." }

    { 35000007;2;Field    ;
                SourceExpr="E-Invoice Date" }

    { 35000008;2;Field    ;
                SourceExpr="E-Invoice Verification Code" }

    { 35000009;2;Field    ;
                SourceExpr="E-Invoice RPS No." }

    { 35000010;2;Field    ;
                SourceExpr="E-Invoice RPS Date" }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 52006529;1;Part     ;
                SubPageLink=Type=CONST(Sale),
                            Document Type=CONST(Post. Invoice),
                            Document No.=FIELD(No.);
                PagePartID=Page52112499;
                PartType=Page }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=TRUE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      SalesInvHeader@1000 : Record 112;
      ChangeExchangeRate@1001 : Page 511;
      "> NAVBR"@35000001 : Integer;
      ShowNFSeLink@35000000 : Boolean;

    BEGIN
    END.
  }
}

