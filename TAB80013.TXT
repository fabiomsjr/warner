OBJECT Table 80013 Customer Posting Group_UPG
{
  OBJECT-PROPERTIES
  {
    Date=13/05/16;
    Time=12:20:48;
    Modified=Yes;
    Version List=UPG;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Customer Posting Group;
               PTB=Gr. Cont�bil Cliente];
    LookupPageID=Page110;
  }
  FIELDS
  {
    { 1   ;   ;Code                ;Code10        ;CaptionML=[ENU=Code;
                                                              PTB=C�digo];
                                                   NotBlank=Yes }
    { 2   ;   ;Receivables Account ;Code20        ;TableRelation="G/L Account";
                                                   OnValidate=BEGIN
                                                                CheckGLAcc("Receivables Account",FALSE,FALSE);
                                                              END;

                                                   CaptionML=[ENU=Receivables Account;
                                                              PTB=Conta Clientes] }
    { 7   ;   ;Service Charge Acc. ;Code20        ;TableRelation="G/L Account";
                                                   OnValidate=BEGIN
                                                                CheckGLAcc("Service Charge Acc.",TRUE,TRUE);
                                                              END;

                                                   CaptionML=[ENU=Service Charge Acc.;
                                                              PTB=Conta Despesas] }
    { 8   ;   ;Payment Disc. Debit Acc.;Code20    ;TableRelation="G/L Account";
                                                   OnValidate=BEGIN
                                                                CheckGLAcc("Payment Disc. Debit Acc.",FALSE,FALSE);
                                                              END;

                                                   CaptionML=[ENU=Payment Disc. Debit Acc.;
                                                              PTB=Conta D�bito Desc. Pag.] }
    { 9   ;   ;Invoice Rounding Account;Code20    ;TableRelation="G/L Account";
                                                   OnValidate=BEGIN
                                                                CheckGLAcc("Invoice Rounding Account",TRUE,FALSE);
                                                              END;

                                                   CaptionML=[ENU=Invoice Rounding Account;
                                                              PTB=Conta Arredond. N.Fiscal] }
    { 10  ;   ;Additional Fee Account;Code20      ;TableRelation="G/L Account";
                                                   OnValidate=BEGIN
                                                                CheckGLAcc("Additional Fee Account",TRUE,TRUE);
                                                              END;

                                                   CaptionML=[ENU=Additional Fee Account;
                                                              PTB=Conta Taxa Adicional] }
    { 11  ;   ;Interest Account    ;Code20        ;TableRelation="G/L Account";
                                                   OnValidate=BEGIN
                                                                CheckGLAcc("Interest Account",TRUE,FALSE);
                                                              END;

                                                   CaptionML=[ENU=Interest Account;
                                                              PTB=Conta Juros] }
    { 12  ;   ;Debit Curr. Appln. Rndg. Acc.;Code20;
                                                   TableRelation="G/L Account";
                                                   CaptionML=[ENU=Debit Curr. Appln. Rndg. Acc.;
                                                              PTB=Conta D�bito Arredond. Liq. Moeda] }
    { 13  ;   ;Credit Curr. Appln. Rndg. Acc.;Code20;
                                                   TableRelation="G/L Account";
                                                   CaptionML=[ENU=Credit Curr. Appln. Rndg. Acc.;
                                                              PTB=Conta Cr�dito Arredond. Liq. Moeda] }
    { 14  ;   ;Debit Rounding Account;Code20      ;TableRelation="G/L Account";
                                                   CaptionML=[ENU=Debit Rounding Account;
                                                              PTB=Conta Arredond. D�bito] }
    { 15  ;   ;Credit Rounding Account;Code20     ;TableRelation="G/L Account";
                                                   CaptionML=[ENU=Credit Rounding Account;
                                                              PTB=Conta Arredond. Cr�dito] }
    { 16  ;   ;Payment Disc. Credit Acc.;Code20   ;TableRelation="G/L Account";
                                                   OnValidate=BEGIN
                                                                CheckGLAcc("Payment Disc. Credit Acc.",FALSE,FALSE);
                                                              END;

                                                   CaptionML=[ENU=Payment Disc. Credit Acc.;
                                                              PTB=Conta Cr�dito Desc. Pag.] }
    { 17  ;   ;Payment Tolerance Debit Acc.;Code20;TableRelation="G/L Account";
                                                   CaptionML=[ENU=Payment Tolerance Debit Acc.;
                                                              PTB=Conta D�bito Toler�ncia Pag.] }
    { 18  ;   ;Payment Tolerance Credit Acc.;Code20;
                                                   TableRelation="G/L Account";
                                                   CaptionML=[ENU=Payment Tolerance Credit Acc.;
                                                              PTB=Conta Cr�dito Toler�ncia Pag.] }
    { 35000001;;Prepayment Account ;Code20        ;TableRelation="G/L Account";
                                                   CaptionML=[ENU=Prepayment Account;
                                                              PTB=Conta Adiantamento];
                                                   Description=NAVBR5.01.0109 }
  }
  KEYS
  {
    {    ;Code                                    ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    LOCAL PROCEDURE CheckGLAcc@2(AccNo@1000 : Code[20];CheckProdPostingGroup@1001 : Boolean;CheckDirectPosting@1002 : Boolean);
    VAR
      GLAcc@1003 : Record 15;
    BEGIN
      IF AccNo <> '' THEN BEGIN
        GLAcc.GET(AccNo);
        GLAcc.CheckGLAcc;
        IF CheckProdPostingGroup THEN
          GLAcc.TESTFIELD("Gen. Prod. Posting Group");
        IF CheckDirectPosting THEN
          GLAcc.TESTFIELD("Direct Posting",TRUE);
      END;
    END;

    BEGIN
    END.
  }
}

