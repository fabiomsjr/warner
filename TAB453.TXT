OBJECT Table 453 Approval Code
{
  OBJECT-PROPERTIES
  {
    Date=07/09/12;
    Time=12:00:00;
    Version List=NAVW17.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Approval Code;
               PTB=C�digo de Aprova��o];
    LookupPageID=Page657;
    DrillDownPageID=Page657;
  }
  FIELDS
  {
    { 1   ;   ;Code                ;Code20        ;CaptionML=[ENU=Code;
                                                              PTB=C�digo];
                                                   NotBlank=Yes }
    { 2   ;   ;Description         ;Text100       ;CaptionML=[ENU=Description;
                                                              PTB=Descri��o] }
    { 3   ;   ;Linked To Table Name;Text50        ;CaptionML=[ENU=Linked To Table Name;
                                                              PTB=Relacionado a Nome Tabela] }
    { 4   ;   ;Linked To Table No. ;Integer       ;TableRelation=Object.ID WHERE (Type=CONST(Table));
                                                   OnValidate=BEGIN
                                                                Objects.SETRANGE(Type,Objects.Type::Table);
                                                                Objects.SETRANGE(ID,"Linked To Table No.");
                                                                IF Objects.FINDFIRST THEN
                                                                  "Linked To Table Name" := Objects.Name
                                                                ELSE
                                                                  "Linked To Table Name" := '';
                                                              END;

                                                   CaptionML=[ENU=Linked To Table No.;
                                                              PTB=Relacionado a No. Tabela] }
  }
  KEYS
  {
    {    ;Code                                    ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      Objects@1000 : Record 2000000001;

    BEGIN
    END.
  }
}

