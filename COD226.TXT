OBJECT Codeunit 226 CustEntry-Apply Posted Entries
{
  OBJECT-PROPERTIES
  {
    Date=15/06/15;
    Time=13:53:12;
    Version List=NAVW17.10;
  }
  PROPERTIES
  {
    TableNo=21;
    Permissions=TableData 21=rimd;
    OnRun=BEGIN
            Apply(Rec,"Document No.",0D);
          END;

  }
  CODE
  {
    VAR
      PostingApplicationMsg@1000 : TextConst 'ENU=Posting application...;PTB=Registrando aplica��o...';
      MustNotBeBeforeErr@1001 : TextConst 'ENU=The Posting Date entered must not be before the Posting Date on the Cust. Ledger Entry.;PTB=A aplica��o registrou-se corretamente.';
      NoEntriesAppliedErr@1003 : TextConst 'ENU=The application was successfully posted though no entries have been applied.;PTB=A aplica��o foi registrada com sucesso embora n�o tenham sido aplicados movimentos.';
      UnapplyPostedAfterThisEntryErr@1018 : TextConst 'ENU=Before you can unapply this entry, you must first unapply all application entries that were posted after this entry.;PTB=Antes de desaplicar esse lan�amento, voc� deve primeiro desaplicar todos as aplica��es de lan�amento que foram registrados depois desse lan�amento.';
      NoApplicationEntryErr@1017 : TextConst 'ENU=Cust. Ledger Entry No. %1 does not have an application entry.;PTB=No. %1 %2 n�o tem um lan�amento de aplica��o.';
      UnapplyingMsg@1015 : TextConst 'ENU=Unapplying and posting...;PTB=N�o aplicados e registrando.';
      UnapplyAllPostedAfterThisEntryErr@1019 : TextConst 'ENU=Before you can unapply this entry, you must first unapply all application entries in Cust. Ledger Entry No. %1 that were posted after this entry.;PTB=Antes de desaplicar esse movimento, voc� deve desaplicar todos os movs. aplica��o em Mov. Cliente n� %1 que foram registrados depois desse movimento.';
      NotAllowedPostingDatesErr@1021 : TextConst 'ENU=Posting date is not within the range of allowed posting dates.;PTB=%1 n�o se encontra no intervalo de datas de registro permitido.';
      LatestEntryMustBeAnApplicationErr@1024 : TextConst 'ENU=The latest Transaction No. must be an application in Cust. Ledger Entry No. %1.;PTB=O ultimo %3 deve ser uma aplica��o em %1 No. %2.';
      CannotUnapplyExchRateErr@1023 : TextConst 'ENU=You cannot unapply the entry with the posting date %1, because the exchange rate for the additional reporting currency has been changed.;PTB=Voc� n�o pode desaplicar o movimento com a data de registro %1, porque a taxa de convers�o para a moeda adicional de relat�rio foi alterada.';
      CannotUnapplyInReversalErr@1026 : TextConst 'ENU=You cannot unapply Cust. Ledger Entry No. %1 because the entry is part of a reversal.;PTB=Voc� n�o pode desaplicar %1 No. %2 porque os movimentos foram envolvidos em uma revers�o.';
      CannotApplyClosedEntriesErr@1102601000 : TextConst 'ENU=One or more of the entries that you selected is closed. You cannot apply closed entries.;PTB=Um ou mais movimentos que voc� selecionou est�o fechados. Voc� n�o pode aplicar movimentos fechados.';

    PROCEDURE Apply@17(CustLedgEntry@1000 : Record 21;DocumentNo@1001 : Code[20];ApplicationDate@1002 : Date);
    VAR
      PaymentToleranceMgt@1003 : Codeunit 426;
    BEGIN
      WITH CustLedgEntry DO BEGIN
        PaymentToleranceMgt.ApplyPostedEntriesMode;
        IF NOT PaymentToleranceMgt.PmtTolCust(CustLedgEntry) THEN
          EXIT;
        GET("Entry No.");

        IF ApplicationDate = 0D THEN
          ApplicationDate := GetApplicationDate(CustLedgEntry)
        ELSE
          IF ApplicationDate < GetApplicationDate(CustLedgEntry) THEN
            ERROR(MustNotBeBeforeErr);

        IF DocumentNo = '' THEN
          DocumentNo := "Document No.";

        CustPostApplyCustLedgEntry(CustLedgEntry,DocumentNo,ApplicationDate);
      END;
    END;

    PROCEDURE GetApplicationDate@14(CustLedgEntry@1000 : Record 21) ApplicationDate : Date;
    VAR
      ApplyToCustLedgEntry@1001 : Record 21;
    BEGIN
      WITH CustLedgEntry DO BEGIN
        ApplicationDate := 0D;
        ApplyToCustLedgEntry.SETCURRENTKEY("Customer No.","Applies-to ID");
        ApplyToCustLedgEntry.SETRANGE("Customer No.","Customer No.");
        ApplyToCustLedgEntry.SETRANGE("Applies-to ID","Applies-to ID");
        ApplyToCustLedgEntry.FINDSET;
        REPEAT
          IF ApplyToCustLedgEntry."Posting Date" > ApplicationDate THEN
            ApplicationDate := ApplyToCustLedgEntry."Posting Date";
        UNTIL ApplyToCustLedgEntry.NEXT = 0;
      END;
    END;

    LOCAL PROCEDURE CustPostApplyCustLedgEntry@12(CustLedgEntry@1000 : Record 21;DocumentNo@1002 : Code[20];ApplicationDate@1003 : Date);
    VAR
      SourceCodeSetup@1004 : Record 242;
      GenJnlLine@1005 : Record 81;
      UpdateAnalysisView@1001 : Codeunit 410;
      GenJnlPostLine@1006 : Codeunit 12;
      Window@1007 : Dialog;
      EntryNoBeforeApplication@1008 : Integer;
      EntryNoAfterApplication@1009 : Integer;
    BEGIN
      WITH CustLedgEntry DO BEGIN
        Window.OPEN(PostingApplicationMsg);

        SourceCodeSetup.GET;

        GenJnlLine.INIT;
        GenJnlLine."Document No." := DocumentNo;
        GenJnlLine."Posting Date" := ApplicationDate;
        GenJnlLine."Document Date" := GenJnlLine."Posting Date";
        GenJnlLine."Account Type" := GenJnlLine."Account Type"::Customer;
        GenJnlLine."Account No." := "Customer No.";
        CALCFIELDS("Debit Amount","Credit Amount","Debit Amount (LCY)","Credit Amount (LCY)");
        GenJnlLine.Correction :=
          ("Debit Amount" < 0) OR ("Credit Amount" < 0) OR
          ("Debit Amount (LCY)" < 0) OR ("Credit Amount (LCY)" < 0);
        GenJnlLine."Document Type" := "Document Type";
        GenJnlLine.Description := Description;
        GenJnlLine."Shortcut Dimension 1 Code" := "Global Dimension 1 Code";
        GenJnlLine."Shortcut Dimension 2 Code" := "Global Dimension 2 Code";
        GenJnlLine."Dimension Set ID" := "Dimension Set ID";
        GenJnlLine."Posting Group" := "Customer Posting Group";
        GenJnlLine."Source Type" := GenJnlLine."Source Type"::Customer;
        GenJnlLine."Source No." := "Customer No.";
        GenJnlLine."Source Code" := SourceCodeSetup."Sales Entry Application";
        GenJnlLine."System-Created Entry" := TRUE;

        EntryNoBeforeApplication := FindLastApplDtldCustLedgEntry;

        GenJnlPostLine.CustPostApplyCustLedgEntry(GenJnlLine,CustLedgEntry);

        EntryNoAfterApplication := FindLastApplDtldCustLedgEntry;
        IF EntryNoAfterApplication = EntryNoBeforeApplication THEN
          ERROR(NoEntriesAppliedErr);

        COMMIT;
        Window.CLOSE;
        UpdateAnalysisView.UpdateAll(0,TRUE);
      END;
    END;

    LOCAL PROCEDURE FindLastApplDtldCustLedgEntry@1() : Integer;
    VAR
      DtldCustLedgEntry@1000 : Record 379;
    BEGIN
      DtldCustLedgEntry.LOCKTABLE;
      IF DtldCustLedgEntry.FINDLAST THEN
        EXIT(DtldCustLedgEntry."Entry No.");

      EXIT(0);
    END;

    LOCAL PROCEDURE FindLastApplEntry@2(CustLedgEntryNo@1002 : Integer) : Integer;
    VAR
      DtldCustLedgEntry@1001 : Record 379;
      ApplicationEntryNo@1000 : Integer;
    BEGIN
      WITH DtldCustLedgEntry DO BEGIN
        SETCURRENTKEY("Cust. Ledger Entry No.","Entry Type");
        SETRANGE("Cust. Ledger Entry No.",CustLedgEntryNo);
        SETRANGE("Entry Type","Entry Type"::Application);
        SETRANGE(Unapplied,FALSE);
        ApplicationEntryNo := 0;
        IF FIND('-') THEN
          REPEAT
            IF "Entry No." > ApplicationEntryNo THEN
              ApplicationEntryNo := "Entry No.";
          UNTIL NEXT = 0;
      END;
      EXIT(ApplicationEntryNo);
    END;

    LOCAL PROCEDURE FindLastTransactionNo@6(CustLedgEntryNo@1002 : Integer) : Integer;
    VAR
      DtldCustLedgEntry@1001 : Record 379;
      LastTransactionNo@1000 : Integer;
    BEGIN
      WITH DtldCustLedgEntry DO BEGIN
        SETCURRENTKEY("Cust. Ledger Entry No.","Entry Type");
        SETRANGE("Cust. Ledger Entry No.",CustLedgEntryNo);
        SETRANGE(Unapplied,FALSE);
        SETFILTER("Entry Type",'<>%1&<>%2',"Entry Type"::"Unrealized Loss","Entry Type"::"Unrealized Gain");
        LastTransactionNo := 0;
        IF FINDSET THEN
          REPEAT
            IF LastTransactionNo < "Transaction No." THEN
              LastTransactionNo := "Transaction No.";
          UNTIL NEXT = 0;
      END;
      EXIT(LastTransactionNo);
    END;

    PROCEDURE UnApplyDtldCustLedgEntry@3(DtldCustLedgEntry@1000 : Record 379);
    VAR
      ApplicationEntryNo@1001 : Integer;
    BEGIN
      DtldCustLedgEntry.TESTFIELD("Entry Type",DtldCustLedgEntry."Entry Type"::Application);
      DtldCustLedgEntry.TESTFIELD(Unapplied,FALSE);
      ApplicationEntryNo := FindLastApplEntry(DtldCustLedgEntry."Cust. Ledger Entry No.");

      IF DtldCustLedgEntry."Entry No." <> ApplicationEntryNo THEN
        ERROR(UnapplyPostedAfterThisEntryErr);
      CheckReversal(DtldCustLedgEntry."Cust. Ledger Entry No.");
      UnApplyCustomer(DtldCustLedgEntry);
    END;

    PROCEDURE UnApplyCustLedgEntry@7(CustLedgEntryNo@1000 : Integer);
    VAR
      DtldCustLedgEntry@1002 : Record 379;
      ApplicationEntryNo@1001 : Integer;
    BEGIN
      CheckReversal(CustLedgEntryNo);
      ApplicationEntryNo := FindLastApplEntry(CustLedgEntryNo);
      IF ApplicationEntryNo = 0 THEN
        ERROR(NoApplicationEntryErr,CustLedgEntryNo);
      DtldCustLedgEntry.GET(ApplicationEntryNo);
      UnApplyCustomer(DtldCustLedgEntry);
    END;

    LOCAL PROCEDURE UnApplyCustomer@19(DtldCustLedgEntry@1000000000 : Record 379);
    VAR
      UnapplyCustEntries@1000 : Page 623;
    BEGIN
      WITH DtldCustLedgEntry DO BEGIN
        TESTFIELD("Entry Type","Entry Type"::Application);
        TESTFIELD(Unapplied,FALSE);
        UnapplyCustEntries.SetDtldCustLedgEntry("Entry No.");
        UnapplyCustEntries.LOOKUPMODE(TRUE);
        UnapplyCustEntries.RUNMODAL;
      END;
    END;

    PROCEDURE PostUnApplyCustomer@4(DtldCustLedgEntry2@1007 : Record 379;DocNo@1000 : Code[20];PostingDate@1001 : Date);
    VAR
      GLEntry@1019 : Record 17;
      CustLedgEntry@1018 : Record 21;
      SourceCodeSetup@1017 : Record 242;
      GenJnlLine@1016 : Record 81;
      DtldCustLedgEntry@1014 : Record 379;
      DateComprReg@1005 : Record 87;
      GenJnlPostLine@1002 : Codeunit 12;
      Window@1012 : Dialog;
      LastTransactionNo@1003 : Integer;
      AddCurrChecked@1004 : Boolean;
      MaxPostingDate@1006 : Date;
    BEGIN
      MaxPostingDate := 0D;
      GLEntry.LOCKTABLE;
      DtldCustLedgEntry.LOCKTABLE;
      CustLedgEntry.LOCKTABLE;
      CustLedgEntry.GET(DtldCustLedgEntry2."Cust. Ledger Entry No.");
      CheckPostingDate(PostingDate,MaxPostingDate);
      IF PostingDate < DtldCustLedgEntry2."Posting Date" THEN
        ERROR(MustNotBeBeforeErr);
      IF DtldCustLedgEntry2."Transaction No." = 0 THEN BEGIN
        DtldCustLedgEntry.SETCURRENTKEY("Application No.","Customer No.","Entry Type");
        DtldCustLedgEntry.SETRANGE("Application No.",DtldCustLedgEntry2."Application No.");
      END ELSE BEGIN
        DtldCustLedgEntry.SETCURRENTKEY("Transaction No.","Customer No.","Entry Type");
        DtldCustLedgEntry.SETRANGE("Transaction No.",DtldCustLedgEntry2."Transaction No.");
      END;
      DtldCustLedgEntry.SETRANGE("Customer No.",DtldCustLedgEntry2."Customer No.");
      DtldCustLedgEntry.SETFILTER("Entry Type",'<>%1',DtldCustLedgEntry."Entry Type"::"Initial Entry");
      DtldCustLedgEntry.SETRANGE(Unapplied,FALSE);
      IF DtldCustLedgEntry.FIND('-') THEN
        REPEAT
          IF NOT AddCurrChecked THEN BEGIN
            CheckAdditionalCurrency(PostingDate,DtldCustLedgEntry."Posting Date");
            AddCurrChecked := TRUE;
          END;
          CheckReversal(DtldCustLedgEntry."Cust. Ledger Entry No.");
          IF DtldCustLedgEntry."Transaction No." <> 0 THEN BEGIN
            IF DtldCustLedgEntry."Entry Type" = DtldCustLedgEntry."Entry Type"::Application THEN BEGIN
              LastTransactionNo :=
                FindLastApplTransactionEntry(DtldCustLedgEntry."Cust. Ledger Entry No.");
              IF (LastTransactionNo <> 0) AND (LastTransactionNo <> DtldCustLedgEntry."Transaction No.") THEN
                ERROR(UnapplyAllPostedAfterThisEntryErr,DtldCustLedgEntry."Cust. Ledger Entry No.");
            END;
            LastTransactionNo := FindLastTransactionNo(DtldCustLedgEntry."Cust. Ledger Entry No.");
            IF (LastTransactionNo <> 0) AND (LastTransactionNo <> DtldCustLedgEntry."Transaction No.") THEN
              ERROR(LatestEntryMustBeAnApplicationErr,DtldCustLedgEntry."Cust. Ledger Entry No.");
          END;
        UNTIL DtldCustLedgEntry.NEXT = 0;

      DateComprReg.CheckMaxDateCompressed(MaxPostingDate,0);

      WITH DtldCustLedgEntry2 DO BEGIN
        SourceCodeSetup.GET;
        CustLedgEntry.GET("Cust. Ledger Entry No.");
        GenJnlLine."Document No." := DocNo;
        GenJnlLine."Posting Date" := PostingDate;
        GenJnlLine."Account Type" := GenJnlLine."Account Type"::Customer;
        GenJnlLine."Account No." := "Customer No.";
        GenJnlLine.Correction := TRUE;
        GenJnlLine."Document Type" := "Document Type";
        GenJnlLine.Description := CustLedgEntry.Description;
        GenJnlLine."Shortcut Dimension 1 Code" := CustLedgEntry."Global Dimension 1 Code";
        GenJnlLine."Shortcut Dimension 2 Code" := CustLedgEntry."Global Dimension 2 Code";
        GenJnlLine."Dimension Set ID" := CustLedgEntry."Dimension Set ID";
        GenJnlLine."Posting Group" := CustLedgEntry."Customer Posting Group";
        GenJnlLine."Source Type" := GenJnlLine."Source Type"::Customer;
        GenJnlLine."Source No." := "Customer No.";
        GenJnlLine."Source Code" := SourceCodeSetup."Unapplied Sales Entry Appln.";
        GenJnlLine."Source Currency Code" := "Currency Code";
        GenJnlLine."System-Created Entry" := TRUE;
        Window.OPEN(UnapplyingMsg);
        GenJnlPostLine.UnapplyCustLedgEntry(GenJnlLine,DtldCustLedgEntry2);
        COMMIT;
        Window.CLOSE;
      END;
    END;

    LOCAL PROCEDURE CheckPostingDate@5(PostingDate@1001 : Date;VAR MaxPostingDate@1005 : Date);
    VAR
      GenJnlCheckLine@1000 : Codeunit 11;
    BEGIN
      IF GenJnlCheckLine.DateNotAllowed(PostingDate) THEN
        ERROR(NotAllowedPostingDatesErr);

      IF PostingDate > MaxPostingDate THEN
        MaxPostingDate := PostingDate;
    END;

    LOCAL PROCEDURE CheckAdditionalCurrency@8(OldPostingDate@1000 : Date;NewPostingDate@1001 : Date);
    VAR
      GLSetup@1002 : Record 98;
      CurrExchRate@1003 : Record 330;
    BEGIN
      IF OldPostingDate = NewPostingDate THEN
        EXIT;
      GLSetup.GET;
      IF GLSetup."Additional Reporting Currency" <> '' THEN
        IF CurrExchRate.ExchangeRate(OldPostingDate,GLSetup."Additional Reporting Currency") <>
           CurrExchRate.ExchangeRate(NewPostingDate,GLSetup."Additional Reporting Currency")
        THEN
          ERROR(CannotUnapplyExchRateErr,NewPostingDate);
    END;

    PROCEDURE CheckReversal@9(CustLedgEntryNo@1000 : Integer);
    VAR
      CustLedgEntry@1001 : Record 21;
    BEGIN
      CustLedgEntry.GET(CustLedgEntryNo);
      IF CustLedgEntry.Reversed THEN
        ERROR(CannotUnapplyInReversalErr,CustLedgEntryNo);
    END;

    PROCEDURE ApplyCustEntryFormEntry@10(VAR ApplyingCustLedgEntry@1000 : Record 21);
    VAR
      CustLedgEntry@1002 : Record 21;
      ApplyCustEntries@1001 : Page 232;
      CustEntryApplID@1004 : Code[50];
    BEGIN
      IF NOT ApplyingCustLedgEntry.Open THEN
        ERROR(CannotApplyClosedEntriesErr);

      CustEntryApplID := USERID;
      IF CustEntryApplID = '' THEN
        CustEntryApplID := '***';
      IF ApplyingCustLedgEntry."Remaining Amount" = 0 THEN
        ApplyingCustLedgEntry.CALCFIELDS("Remaining Amount");

      ApplyingCustLedgEntry."Applying Entry" := TRUE;
      ApplyingCustLedgEntry."Applies-to ID" := CustEntryApplID;
      ApplyingCustLedgEntry."Amount to Apply" := ApplyingCustLedgEntry."Remaining Amount";
      CODEUNIT.RUN(CODEUNIT::"Cust. Entry-Edit",ApplyingCustLedgEntry);
      COMMIT;

      CustLedgEntry.SETCURRENTKEY("Customer No.",Open,Positive);
      CustLedgEntry.SETRANGE("Customer No.",ApplyingCustLedgEntry."Customer No.");
      CustLedgEntry.SETRANGE(Open,TRUE);
      IF CustLedgEntry.FINDFIRST THEN BEGIN
        ApplyCustEntries.SetCustLedgEntry(ApplyingCustLedgEntry);
        ApplyCustEntries.SETRECORD(CustLedgEntry);
        ApplyCustEntries.SETTABLEVIEW(CustLedgEntry);
        ApplyCustEntries.RUNMODAL;
        CLEAR(ApplyCustEntries);
        ApplyingCustLedgEntry."Applying Entry" := FALSE;
        ApplyingCustLedgEntry."Applies-to ID" := '';
        ApplyingCustLedgEntry."Amount to Apply" := 0;
      END;
    END;

    PROCEDURE FindLastApplTransactionEntry@11(CustLedgEntryNo@1000 : Integer) : Integer;
    VAR
      DtldCustLedgEntry@1001 : Record 379;
      LastTransactionNo@1002 : Integer;
    BEGIN
      DtldCustLedgEntry.SETCURRENTKEY("Cust. Ledger Entry No.","Entry Type");
      DtldCustLedgEntry.SETRANGE("Cust. Ledger Entry No.",CustLedgEntryNo);
      DtldCustLedgEntry.SETRANGE("Entry Type",DtldCustLedgEntry."Entry Type"::Application);
      LastTransactionNo := 0;
      IF DtldCustLedgEntry.FIND('-') THEN
        REPEAT
          IF (DtldCustLedgEntry."Transaction No." > LastTransactionNo) AND NOT DtldCustLedgEntry.Unapplied THEN
            LastTransactionNo := DtldCustLedgEntry."Transaction No.";
        UNTIL DtldCustLedgEntry.NEXT = 0;
      EXIT(LastTransactionNo);
    END;

    BEGIN
    END.
  }
}

