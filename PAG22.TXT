OBJECT Page 22 Customer List
{
  OBJECT-PROPERTIES
  {
    Date=07/01/15;
    Time=12:00:00;
    Version List=NAVW18.00.00.39368,NAVBR7;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Customer List;
               PTB=Lista Clientes];
    SourceTable=Table18;
    PageType=List;
    CardPageID=Customer Card;
    OnAfterGetRecord=BEGIN
                       SetSocialListeningFactboxVisibility
                     END;

    OnAfterGetCurrRecord=BEGIN
                           SetSocialListeningFactboxVisibility
                         END;

    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 16      ;1   ;ActionGroup;
                      CaptionML=[ENU=&Customer;
                                 PTB=&Cliente];
                      Image=Customer }
      { 20      ;2   ;Action    ;
                      CaptionML=[ENU=Co&mments;
                                 PTB=Co&ment�rios];
                      RunObject=Page 124;
                      RunPageLink=Table Name=CONST(Customer),
                                  No.=FIELD(No.);
                      Image=ViewComments }
      { 66      ;2   ;ActionGroup;
                      CaptionML=[ENU=Dimensions;
                                 PTB=Dimens�es];
                      Image=Dimensions }
      { 84      ;3   ;Action    ;
                      ShortCutKey=Shift+Ctrl+D;
                      CaptionML=[ENU=Dimensions-Single;
                                 PTB=Dimens�es-Simples];
                      RunObject=Page 540;
                      RunPageLink=Table ID=CONST(18),
                                  No.=FIELD(No.);
                      Image=Dimensions }
      { 42      ;3   ;Action    ;
                      AccessByPermission=TableData 348=R;
                      CaptionML=[ENU=Dimensions-&Multiple;
                                 PTB=Dimens�es-&Multiplas];
                      Image=DimensionSets;
                      OnAction=VAR
                                 Cust@1001 : Record 18;
                                 DefaultDimMultiple@1002 : Page 542;
                               BEGIN
                                 CurrPage.SETSELECTIONFILTER(Cust);
                                 DefaultDimMultiple.SetMultiCust(Cust);
                                 DefaultDimMultiple.RUNMODAL;
                               END;
                                }
      { 58      ;2   ;Action    ;
                      CaptionML=[ENU=Bank Accounts;
                                 PTB=Conta Banco];
                      RunObject=Page 424;
                      RunPageLink=Customer No.=FIELD(No.);
                      Image=BankAccount }
      { 29      ;2   ;Action    ;
                      CaptionML=ENU=Direct Debit Mandates;
                      RunObject=Page 1230;
                      RunPageLink=Customer No.=FIELD(No.);
                      Image=MakeAgreement }
      { 23      ;2   ;Action    ;
                      CaptionML=[ENU=Ship-&to Addresses;
                                 PTB=Endere�o Env&io];
                      RunObject=Page 301;
                      RunPageLink=Customer No.=FIELD(No.);
                      Image=ShipAddress }
      { 60      ;2   ;Action    ;
                      AccessByPermission=TableData 5050=R;
                      CaptionML=[ENU=C&ontact;
                                 PTB=C&ontato];
                      Image=ContactPerson;
                      OnAction=BEGIN
                                 ShowContact;
                               END;
                                }
      { 45      ;2   ;Action    ;
                      CaptionML=[ENU=Cross Re&ferences;
                                 PTB=Re&ferencia Cruzada];
                      RunObject=Page 5723;
                      RunPageView=SORTING(Cross-Reference Type,Cross-Reference Type No.);
                      RunPageLink=Cross-Reference Type=CONST(Customer),
                                  Cross-Reference Type No.=FIELD(No.);
                      Image=Change }
      { 17      ;2   ;Separator  }
      { 9       ;1   ;ActionGroup;
                      CaptionML=[ENU=History;
                                 PTB=Hist�rico];
                      Image=History }
      { 22      ;2   ;Action    ;
                      ShortCutKey=Ctrl+F7;
                      CaptionML=[ENU=Ledger E&ntries;
                                 PTB=Movim&entos];
                      RunObject=Page 25;
                      RunPageView=SORTING(Customer No.);
                      RunPageLink=Customer No.=FIELD(No.);
                      Promoted=Yes;
                      Image=CustomerLedger;
                      PromotedCategory=Process }
      { 18      ;2   ;Action    ;
                      ShortCutKey=F7;
                      CaptionML=[ENU=Statistics;
                                 PTB=Estat�sticas];
                      RunObject=Page 151;
                      RunPageLink=No.=FIELD(No.),
                                  Date Filter=FIELD(Date Filter),
                                  Global Dimension 1 Filter=FIELD(Global Dimension 1 Filter),
                                  Global Dimension 2 Filter=FIELD(Global Dimension 2 Filter);
                      Promoted=Yes;
                      Image=Statistics;
                      PromotedCategory=Process }
      { 21      ;2   ;Action    ;
                      CaptionML=[ENU=S&ales;
                                 PTB=Vend&as];
                      RunObject=Page 155;
                      RunPageLink=No.=FIELD(No.),
                                  Global Dimension 1 Filter=FIELD(Global Dimension 1 Filter),
                                  Global Dimension 2 Filter=FIELD(Global Dimension 2 Filter);
                      Image=Sales }
      { 19      ;2   ;Action    ;
                      CaptionML=[ENU=Entry Statistics;
                                 PTB=Estat�stica Documentos];
                      RunObject=Page 302;
                      RunPageLink=No.=FIELD(No.),
                                  Date Filter=FIELD(Date Filter),
                                  Global Dimension 1 Filter=FIELD(Global Dimension 1 Filter),
                                  Global Dimension 2 Filter=FIELD(Global Dimension 2 Filter);
                      Image=EntryStatistics }
      { 63      ;2   ;Action    ;
                      CaptionML=[ENU=Statistics by C&urrencies;
                                 PTB=Estat�sticas por M&oeda];
                      RunObject=Page 486;
                      RunPageLink=Customer Filter=FIELD(No.),
                                  Global Dimension 1 Filter=FIELD(Global Dimension 1 Filter),
                                  Global Dimension 2 Filter=FIELD(Global Dimension 2 Filter),
                                  Date Filter=FIELD(Date Filter);
                      Image=Currencies }
      { 6500    ;2   ;Action    ;
                      CaptionML=[ENU=Item &Tracking Entries;
                                 PTB=Movimento Item &Rastreabilidade];
                      Image=ItemTrackingLedger;
                      OnAction=VAR
                                 ItemTrackingMgt@1001 : Codeunit 6500;
                               BEGIN
                                 ItemTrackingMgt.CallItemTrackingEntryForm(1,"No.",'','','','','');
                               END;
                                }
      { 24      ;1   ;ActionGroup;
                      CaptionML=[ENU=S&ales;
                                 PTB=Vend&as];
                      Image=Sales }
      { 25      ;2   ;Action    ;
                      CaptionML=[ENU=Invoice &Discounts;
                                 PTB=Nota Fiscal &Descontos];
                      RunObject=Page 23;
                      RunPageLink=Code=FIELD(Invoice Disc. Code);
                      Image=CalculateInvoiceDiscount }
      { 26      ;2   ;Action    ;
                      CaptionML=[ENU=Prices;
                                 PTB=Pre�os];
                      RunObject=Page 7002;
                      RunPageView=SORTING(Sales Type,Sales Code);
                      RunPageLink=Sales Type=CONST(Customer),
                                  Sales Code=FIELD(No.);
                      Image=Price }
      { 71      ;2   ;Action    ;
                      CaptionML=[ENU=Line Discounts;
                                 PTB=Descontos Linha];
                      RunObject=Page 7004;
                      RunPageView=SORTING(Sales Type,Sales Code);
                      RunPageLink=Sales Type=CONST(Customer),
                                  Sales Code=FIELD(No.);
                      Image=LineDiscount }
      { 82      ;2   ;Action    ;
                      CaptionML=[ENU=Prepa&yment Percentages;
                                 PTB=Percentual Adiantamento];
                      RunObject=Page 664;
                      RunPageView=SORTING(Sales Type,Sales Code);
                      RunPageLink=Sales Type=CONST(Customer),
                                  Sales Code=FIELD(No.);
                      Image=PrepaymentPercentages }
      { 75      ;2   ;Action    ;
                      CaptionML=[ENU=S&td. Cust. Sales Codes;
                                 PTB=C�digos Venda Cliente Padr�o];
                      RunObject=Page 173;
                      RunPageLink=Customer No.=FIELD(No.);
                      Image=CodesList }
      { 11      ;1   ;ActionGroup;
                      CaptionML=[ENU=Documents;
                                 PTB=Documentos];
                      Image=Documents }
      { 27      ;2   ;Action    ;
                      CaptionML=[ENU=Quotes;
                                 PTB=Cota��es];
                      RunObject=Page 9300;
                      RunPageView=SORTING(Sell-to Customer No.);
                      RunPageLink=Sell-to Customer No.=FIELD(No.);
                      Image=Quote }
      { 28      ;2   ;Action    ;
                      CaptionML=[ENU=Orders;
                                 PTB=Pedidos];
                      RunObject=Page 9305;
                      RunPageView=SORTING(Sell-to Customer No.);
                      RunPageLink=Sell-to Customer No.=FIELD(No.);
                      Image=Document }
      { 70      ;2   ;Action    ;
                      CaptionML=[ENU=Return Orders;
                                 PTB=Devolu��es];
                      RunObject=Page 9304;
                      RunPageView=SORTING(Sell-to Customer No.);
                      RunPageLink=Sell-to Customer No.=FIELD(No.);
                      Image=ReturnOrder }
      { 76      ;2   ;ActionGroup;
                      CaptionML=[ENU=Issued Documents;
                                 PTB=Documentos Emitidos];
                      Image=Documents }
      { 77      ;3   ;Action    ;
                      CaptionML=[ENU=Issued &Reminders;
                                 PTB=&Carta Avisos Emitidas];
                      RunObject=Page 440;
                      RunPageView=SORTING(Customer No.,Posting Date);
                      RunPageLink=Customer No.=FIELD(No.);
                      Image=OrderReminder }
      { 78      ;3   ;Action    ;
                      CaptionML=[ENU=Issued &Finance Charge Memos;
                                 PTB=Notas de &Juros Emitidas];
                      RunObject=Page 452;
                      RunPageView=SORTING(Customer No.,Posting Date);
                      RunPageLink=Customer No.=FIELD(No.);
                      Image=FinChargeMemo }
      { 65      ;2   ;Action    ;
                      CaptionML=[ENU=Blanket Orders;
                                 PTB=Pedidos em Aberto];
                      RunObject=Page 9303;
                      RunPageView=SORTING(Document Type,Sell-to Customer No.);
                      RunPageLink=Sell-to Customer No.=FIELD(No.);
                      Image=BlanketOrder }
      { 13      ;1   ;ActionGroup;
                      CaptionML=[ENU=Credit Card;
                                 PTB=Cart�o Cr�dito];
                      Image=CreditCard }
      { 15      ;2   ;ActionGroup;
                      CaptionML=[ENU=Credit Cards;
                                 PTB=Cart�es Cr�dito];
                      Image=CreditCard }
      { 85      ;3   ;Action    ;
                      CaptionML=[ENU=C&redit Cards;
                                 PTB=Cart�o C&redito];
                      RunObject=Page 828;
                      RunPageLink=Customer No.=FIELD(No.);
                      Image=CreditCard }
      { 86      ;3   ;Action    ;
                      CaptionML=[ENU=Credit Cards Transaction Lo&g Entries;
                                 PTB=Log de Transa��es com Cart�o de Cr�dito];
                      RunObject=Page 829;
                      RunPageLink=Customer No.=FIELD(No.);
                      Image=CreditCardLog }
      { 14      ;1   ;ActionGroup;
                      CaptionML=[ENU=Service;
                                 PTB=Servi�o];
                      Image=ServiceItem }
      { 81      ;2   ;Action    ;
                      CaptionML=[ENU=Service Orders;
                                 PTB=Pedido Servi�o];
                      RunObject=Page 9318;
                      RunPageView=SORTING(Document Type,Customer No.);
                      RunPageLink=Customer No.=FIELD(No.);
                      Image=Document }
      { 68      ;2   ;Action    ;
                      CaptionML=[ENU=Ser&vice Contracts;
                                 PTB=Contratos Ser&vi�o];
                      RunObject=Page 6065;
                      RunPageView=SORTING(Customer No.,Ship-to Code);
                      RunPageLink=Customer No.=FIELD(No.);
                      Image=ServiceAgreement }
      { 69      ;2   ;Action    ;
                      CaptionML=[ENU=Service &Items;
                                 PTB=&Itens Servi�o];
                      RunObject=Page 5988;
                      RunPageView=SORTING(Customer No.,Ship-to Code,Item No.,Serial No.);
                      RunPageLink=Customer No.=FIELD(No.);
                      Image=ServiceItem }
      { 1900000005;0 ;ActionContainer;
                      ActionContainerType=NewDocumentItems }
      { 1902575205;1 ;Action    ;
                      CaptionML=[ENU=Blanket Sales Order;
                                 PTB=Pedidos de Venda em Aberdo];
                      RunObject=Page 507;
                      RunPageLink=Sell-to Customer No.=FIELD(No.);
                      Promoted=No;
                      Image=BlanketOrder;
                      PromotedCategory=New;
                      RunPageMode=Create }
      { 1900246505;1 ;Action    ;
                      CaptionML=[ENU=Sales Quote;
                                 PTB=Cota��o de Venda];
                      RunObject=Page 41;
                      RunPageLink=Sell-to Customer No.=FIELD(No.);
                      Promoted=Yes;
                      Image=Quote;
                      PromotedCategory=New;
                      RunPageMode=Create }
      { 1906384905;1 ;Action    ;
                      CaptionML=[ENU=Sales Invoice;
                                 PTB=Nota Fiscal de Venda];
                      RunObject=Page 43;
                      RunPageLink=Sell-to Customer No.=FIELD(No.);
                      Promoted=Yes;
                      Image=Invoice;
                      PromotedCategory=New;
                      RunPageMode=Create }
      { 1904747105;1 ;Action    ;
                      CaptionML=[ENU=Sales Order;
                                 PTB=Pedido de Venda];
                      RunObject=Page 42;
                      RunPageLink=Sell-to Customer No.=FIELD(No.);
                      Promoted=Yes;
                      Image=Document;
                      PromotedCategory=New;
                      RunPageMode=Create }
      { 1902583505;1 ;Action    ;
                      CaptionML=[ENU=Sales Credit Memo;
                                 PTB=Nota Cr�dito Venda];
                      RunObject=Page 44;
                      RunPageLink=Sell-to Customer No.=FIELD(No.);
                      Promoted=No;
                      Image=CreditMemo;
                      PromotedCategory=New;
                      RunPageMode=Create }
      { 1905163705;1 ;Action    ;
                      CaptionML=[ENU=Sales Return Order;
                                 PTB=Devolu��o de Pedido de Venda];
                      RunObject=Page 6630;
                      RunPageLink=Sell-to Customer No.=FIELD(No.);
                      Promoted=No;
                      Image=ReturnOrder;
                      PromotedCategory=New;
                      RunPageMode=Create }
      { 1905185205;1 ;Action    ;
                      CaptionML=[ENU=Service Quote;
                                 PTB=Cota��o de Servi�o];
                      RunObject=Page 5964;
                      RunPageLink=Customer No.=FIELD(No.);
                      Promoted=No;
                      Image=Quote;
                      PromotedCategory=New;
                      RunPageMode=Create }
      { 1902079405;1 ;Action    ;
                      CaptionML=[ENU=Service Invoice;
                                 PTB=Nota Fiscal de Servi�o];
                      RunObject=Page 5933;
                      RunPageLink=Customer No.=FIELD(No.);
                      Promoted=No;
                      Image=Invoice;
                      PromotedCategory=New;
                      RunPageMode=Create }
      { 1907102005;1 ;Action    ;
                      CaptionML=[ENU=Service Order;
                                 PTB=Pedido de Servi�o];
                      RunObject=Page 5900;
                      RunPageLink=Customer No.=FIELD(No.);
                      Promoted=No;
                      Image=Document;
                      PromotedCategory=New;
                      RunPageMode=Create }
      { 1901662105;1 ;Action    ;
                      CaptionML=[ENU=Service Credit Memo;
                                 PTB=Nota Cr�dito Servi�o];
                      RunObject=Page 5935;
                      RunPageLink=Customer No.=FIELD(No.);
                      Promoted=No;
                      Image=CreditMemo;
                      PromotedCategory=New;
                      RunPageMode=Create }
      { 1903839805;1 ;Action    ;
                      CaptionML=[ENU=Reminder;
                                 PTB=Lembrete];
                      RunObject=Page 434;
                      RunPageLink=Customer No.=FIELD(No.);
                      Promoted=Yes;
                      Image=Reminder;
                      PromotedCategory=New;
                      RunPageMode=Create }
      { 1901102005;1 ;Action    ;
                      CaptionML=[ENU=Finance Charge Memo;
                                 PTB=Encargo Financeiro];
                      RunObject=Page 446;
                      RunPageLink=Customer No.=FIELD(No.);
                      Promoted=No;
                      Image=FinChargeMemo;
                      PromotedCategory=New;
                      RunPageMode=Create }
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 1900839804;1 ;Action    ;
                      CaptionML=[ENU=Cash Receipt Journal;
                                 PTB=Di�rio de Recebimentos];
                      RunObject=Page 255;
                      Promoted=Yes;
                      Image=CashReceiptJournal;
                      PromotedCategory=Process }
      { 1905171704;1 ;Action    ;
                      CaptionML=[ENU=Sales Journal;
                                 PTB=Di�rio de Vendas];
                      RunObject=Page 253;
                      Promoted=Yes;
                      Image=Journals;
                      PromotedCategory=Process }
      { 1900000006;0 ;ActionContainer;
                      ActionContainerType=Reports }
      { 3       ;1   ;ActionGroup;
                      CaptionML=[ENU=General;
                                 PTB=Geral] }
      { 1905562606;2 ;Action    ;
                      CaptionML=[ENU=Customer List;
                                 PTB=Cliente - Listagem];
                      RunObject=Report 101;
                      Promoted=No;
                      Image=Report;
                      PromotedCategory=Report }
      { 1901007206;2 ;Action    ;
                      CaptionML=[ENU=Customer Register;
                                 PTB=Reg. Mov. Cliente];
                      RunObject=Report 103;
                      Promoted=No;
                      Image=Report;
                      PromotedCategory=Report }
      { 1907152806;2 ;Action    ;
                      CaptionML=[ENU=Customer - Top 10 List;
                                 PTB=Cliente - Listagem dos 10 Melhores];
                      RunObject=Report 111;
                      Promoted=Yes;
                      Image=Report;
                      PromotedCategory=Report }
      { 5       ;1   ;ActionGroup;
                      CaptionML=[ENU=Sales;
                                 PTB=Vendas];
                      Image=Sales }
      { 1905727106;2 ;Action    ;
                      CaptionML=[ENU=Customer - Order Summary;
                                 PTB=Cliente - Resumo de Pedidos];
                      RunObject=Report 107;
                      Promoted=Yes;
                      Image=Report;
                      PromotedCategory=Report }
      { 1900172506;2 ;Action    ;
                      CaptionML=[ENU=Customer - Order Detail;
                                 PTB=Cliente - Detalhe Pedido];
                      RunObject=Report 108;
                      Promoted=No;
                      Image=Report;
                      PromotedCategory=Report }
      { 1906073506;2 ;Action    ;
                      CaptionML=[ENU=Customer - Sales List;
                                 PTB=Cliente - Lista de Venda];
                      RunObject=Report 119;
                      Promoted=Yes;
                      Image=Report;
                      PromotedCategory=Report }
      { 1904190506;2 ;Action    ;
                      CaptionML=[ENU=Sales Statistics;
                                 PTB=Estat�sticas de Venda];
                      RunObject=Report 112;
                      Promoted=No;
                      Image=Report;
                      PromotedCategory=Report }
      { 1900760706;2 ;Action    ;
                      CaptionML=[ENU=Customer/Item Sales;
                                 PTB=Clientes/ Itens de Venda];
                      RunObject=Report 113;
                      Promoted=No;
                      Image=Report;
                      PromotedCategory=Report }
      { 7       ;1   ;ActionGroup;
                      CaptionML=[ENU=Financial Management;
                                 PTB=Gerenciamento Financeiro];
                      Image=Report }
      { 1906871306;2 ;Action    ;
                      CaptionML=[ENU=Customer - Detail Trial Bal.;
                                 PTB=Cliente - Balancete];
                      RunObject=Report 104;
                      Promoted=No;
                      Image=Report;
                      PromotedCategory=Report }
      { 1907944606;2 ;Action    ;
                      CaptionML=[ENU=Customer - Summary Aging;
                                 PTB=Cliente - Resumo por Vencimento];
                      RunObject=Report 105;
                      Promoted=No;
                      Image=Report;
                      PromotedCategory=Report }
      { 1906813206;2 ;Action    ;
                      CaptionML=[ENU=Customer Detailed Aging;
                                 PTB=Cliente - Vencimento Detalhado];
                      RunObject=Report 106;
                      Promoted=No;
                      Image=Report;
                      PromotedCategory=Report }
      { 1906768606;2 ;Action    ;
                      CaptionML=ENU=Statement;
                      RunObject=Report 116;
                      Promoted=Yes;
                      Image=Report;
                      PromotedCategory=Report }
      { 1903839806;2 ;Action    ;
                      CaptionML=[ENU=Reminder;
                                 PTB=Lembrete];
                      RunObject=Report 117;
                      Promoted=No;
                      Image=Reminder;
                      PromotedCategory=Report }
      { 1900711606;2 ;Action    ;
                      CaptionML=[ENU=Aged Accounts Receivable;
                                 PTB=Recebimentos Cont�beis Atrasados];
                      RunObject=Report 120;
                      Promoted=Yes;
                      Image=Report;
                      PromotedCategory=Report }
      { 1902299006;2 ;Action    ;
                      CaptionML=[ENU=Customer - Balance to Date;
                                 PTB=Cliente - Saldo por Data];
                      RunObject=Report 121;
                      Promoted=Yes;
                      Image=Report;
                      PromotedCategory=Report }
      { 1906359306;2 ;Action    ;
                      CaptionML=[ENU=Customer - Trial Balance;
                                 PTB=Cliente - Saldo Parcial];
                      RunObject=Report 129;
                      Promoted=No;
                      Image=Report;
                      PromotedCategory=Report }
      { 1904039606;2 ;Action    ;
                      CaptionML=[ENU=Customer - Payment Receipt;
                                 PTB=Cliente - Pagamentos Recebidos];
                      RunObject=Report 211;
                      Promoted=Yes;
                      Image=Report;
                      PromotedCategory=Report }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                SourceExpr="No." }

    { 4   ;2   ;Field     ;
                SourceExpr=Name }

    { 1102300002;2;Field  ;
                SourceExpr=Category;
                Visible=false }

    { 1102300000;2;Field  ;
                SourceExpr="C.N.P.J./C.P.F." }

    { 1102300001;2;Field  ;
                SourceExpr="I.E." }

    { 35000001;2;Field    ;
                SourceExpr="Balance (LCY)" }

    { 40  ;2   ;Field     ;
                SourceExpr="Responsibility Center" }

    { 43  ;2   ;Field     ;
                SourceExpr="Location Code" }

    { 54  ;2   ;Field     ;
                SourceExpr="Post Code";
                Visible=FALSE }

    { 30  ;2   ;Field     ;
                SourceExpr="Country/Region Code";
                Visible=FALSE }

    { 6   ;2   ;Field     ;
                SourceExpr="Phone No." }

    { 32  ;2   ;Field     ;
                SourceExpr="Fax No.";
                Visible=FALSE }

    { 79  ;2   ;Field     ;
                SourceExpr="IC Partner Code";
                Visible=FALSE }

    { 34  ;2   ;Field     ;
                SourceExpr=Contact }

    { 36  ;2   ;Field     ;
                SourceExpr="Salesperson Code";
                Visible=FALSE }

    { 38  ;2   ;Field     ;
                SourceExpr="Customer Posting Group";
                Visible=FALSE }

    { 56  ;2   ;Field     ;
                SourceExpr="Gen. Bus. Posting Group";
                Visible=FALSE }

    { 52  ;2   ;Field     ;
                SourceExpr="VAT Bus. Posting Group";
                Visible=FALSE }

    { 46  ;2   ;Field     ;
                SourceExpr="Customer Price Group";
                Visible=FALSE }

    { 72  ;2   ;Field     ;
                SourceExpr="Customer Disc. Group";
                Visible=FALSE }

    { 8   ;2   ;Field     ;
                SourceExpr="Payment Terms Code";
                Visible=FALSE }

    { 61  ;2   ;Field     ;
                SourceExpr="Reminder Terms Code";
                Visible=FALSE }

    { 48  ;2   ;Field     ;
                SourceExpr="Fin. Charge Terms Code";
                Visible=FALSE }

    { 10  ;2   ;Field     ;
                SourceExpr="Currency Code";
                Visible=FALSE }

    { 50  ;2   ;Field     ;
                SourceExpr="Language Code";
                Visible=FALSE }

    { 12  ;2   ;Field     ;
                SourceExpr="Search Name" }

    { 1102601000;2;Field  ;
                SourceExpr="Credit Limit (LCY)";
                Visible=FALSE }

    { 1102601002;2;Field  ;
                SourceExpr=Blocked;
                Visible=FALSE }

    { 1102601004;2;Field  ;
                SourceExpr="Last Date Modified";
                Visible=FALSE }

    { 1102601006;2;Field  ;
                SourceExpr="Application Method";
                Visible=FALSE }

    { 1102601008;2;Field  ;
                SourceExpr="Combine Shipments";
                Visible=FALSE }

    { 1102601010;2;Field  ;
                SourceExpr=Reserve;
                Visible=FALSE }

    { 1102601012;2;Field  ;
                SourceExpr="Shipping Advice";
                Visible=FALSE }

    { 1102601014;2;Field  ;
                SourceExpr="Shipping Agent Code";
                Visible=FALSE }

    { 1102601016;2;Field  ;
                SourceExpr="Base Calendar Code";
                Visible=FALSE }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 33  ;1   ;Part      ;
                SubPageLink=Source Type=CONST(Customer),
                            Source No.=FIELD(No.);
                PagePartID=Page875;
                Visible=SocialListeningVisible;
                PartType=Page }

    { 31  ;1   ;Part      ;
                SubPageLink=Source Type=CONST(Customer),
                            Source No.=FIELD(No.);
                PagePartID=Page876;
                Visible=SocialListeningSetupVisible;
                PartType=Page;
                UpdatePropagation=Both }

    { 1903720907;1;Part   ;
                SubPageLink=No.=FIELD(No.),
                            Currency Filter=FIELD(Currency Filter),
                            Date Filter=FIELD(Date Filter),
                            Global Dimension 1 Filter=FIELD(Global Dimension 1 Filter),
                            Global Dimension 2 Filter=FIELD(Global Dimension 2 Filter);
                PagePartID=Page9080;
                Visible=TRUE;
                PartType=Page }

    { 1907234507;1;Part   ;
                SubPageLink=No.=FIELD(No.),
                            Currency Filter=FIELD(Currency Filter),
                            Date Filter=FIELD(Date Filter),
                            Global Dimension 1 Filter=FIELD(Global Dimension 1 Filter),
                            Global Dimension 2 Filter=FIELD(Global Dimension 2 Filter);
                PagePartID=Page9081;
                Visible=FALSE;
                PartType=Page }

    { 1902018507;1;Part   ;
                SubPageLink=No.=FIELD(No.),
                            Currency Filter=FIELD(Currency Filter),
                            Date Filter=FIELD(Date Filter),
                            Global Dimension 1 Filter=FIELD(Global Dimension 1 Filter),
                            Global Dimension 2 Filter=FIELD(Global Dimension 2 Filter);
                PagePartID=Page9082;
                Visible=TRUE;
                PartType=Page }

    { 1900316107;1;Part   ;
                SubPageLink=No.=FIELD(No.),
                            Currency Filter=FIELD(Currency Filter),
                            Date Filter=FIELD(Date Filter),
                            Global Dimension 1 Filter=FIELD(Global Dimension 1 Filter),
                            Global Dimension 2 Filter=FIELD(Global Dimension 2 Filter);
                PagePartID=Page9084;
                Visible=FALSE;
                PartType=Page }

    { 1907829707;1;Part   ;
                SubPageLink=No.=FIELD(No.),
                            Currency Filter=FIELD(Currency Filter),
                            Date Filter=FIELD(Date Filter),
                            Global Dimension 1 Filter=FIELD(Global Dimension 1 Filter),
                            Global Dimension 2 Filter=FIELD(Global Dimension 2 Filter);
                PagePartID=Page9085;
                Visible=FALSE;
                PartType=Page }

    { 1902613707;1;Part   ;
                SubPageLink=No.=FIELD(No.),
                            Currency Filter=FIELD(Currency Filter),
                            Date Filter=FIELD(Date Filter),
                            Global Dimension 1 Filter=FIELD(Global Dimension 1 Filter),
                            Global Dimension 2 Filter=FIELD(Global Dimension 2 Filter);
                PagePartID=Page9086;
                Visible=FALSE;
                PartType=Page }

    { 1900383207;1;Part   ;
                Visible=TRUE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=TRUE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      SocialListeningSetupVisible@1001 : Boolean;
      SocialListeningVisible@1000 : Boolean;

    PROCEDURE GetSelectionFilter@2() : Text;
    VAR
      Cust@1001 : Record 18;
      SelectionFilterManagement@1002 : Codeunit 46;
    BEGIN
      CurrPage.SETSELECTIONFILTER(Cust);
      EXIT(SelectionFilterManagement.GetSelectionFilterForCustomer(Cust));
    END;

    PROCEDURE SetSelection@1(VAR Cust@1000 : Record 18);
    BEGIN
      CurrPage.SETSELECTIONFILTER(Cust);
    END;

    LOCAL PROCEDURE SetSocialListeningFactboxVisibility@3();
    VAR
      SocialListeningMgt@1000 : Codeunit 871;
    BEGIN
      SocialListeningMgt.GetCustFactboxVisibility(Rec,SocialListeningSetupVisible,SocialListeningVisible);
    END;

    BEGIN
    END.
  }
}

