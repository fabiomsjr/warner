OBJECT Report 52112634 NFS-e Rio Grande
{
  OBJECT-PROPERTIES
  {
    Date=05/08/14;
    Time=08:00:34;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    Permissions=TableData 112=rm;
    CaptionML=[ENU=NFS-e;
               PTB=NFS-e];
    ProcessingOnly=Yes;
    OnPreReport=BEGIN
                  FileExportMgt.Create;
                END;

    OnPostReport=BEGIN
                   FileExportMgt.Download(FORMAT(CURRENTDATETIME, 0, '<Year4><Month,2><Day,2><Hours,2><Minutes,2><Seconds,2>.txt'));
                 END;

  }
  DATASET
  {
    { 1000000006;0;DataItem;SalesInvoiceHeader;
               DataItemTable=Table112;
               DataItemTableView=SORTING(No.)
                                 WHERE(Service E-Invoice=CONST(Yes));
               OnPreDataItem=BEGIN
                               IF COUNT = 0 THEN
                                 ERROR(Text002);

                               CurrReport.BREAK;
                             END;

               OnAfterGetRecord=VAR
                                  nfseBatch@52006500 : Record 52112633;
                                  salesInvLine@1000000003 : Record 113;
                                  additInvLineText@1000000002 : Record 52112436;
                                  varExit@1000000001 : Code[10];
                                  salesInvHeader@1000000000 : Record 112;
                                BEGIN
                                END;

               ReqFilterFields=Posting Date,No.,Bill-to Customer No. }

    { 1000000026;;DataItem;Header            ;
               DataItemTable=Table2000000026;
               DataItemTableView=SORTING(Number)
                                 WHERE(Number=FILTER(1));
               OnAfterGetRecord=VAR
                                  compInfo@1000000000 : Record 79;
                                BEGIN
                                  SalesInvoiceHeader.FINDFIRST;
                                  compInfo.GetCompanyBranchInfo(SalesInvoiceHeader."Branch Code");
                                  compInfo.TESTFIELD("C.C.M.");
                                  T1 := '1' + 'NFE_LOTE    ' + TextFunctions.FormatText(DELCHR(compInfo."C.C.M.", '<=>', ',./-'),8,TRUE) + '010' + FORMAT(TODAY, 0, '<Year4><Month,2><Day,2>');

                                  FileExportMgt.WriteLine(T1);
                                END;
                                 }

    { 1000000001;;DataItem;Line              ;
               DataItemTable=Table2000000026;
               OnPreDataItem=BEGIN
                               SETRANGE(Number, 1, SalesInvoiceHeader.COUNT);
                             END;

               OnAfterGetRecord=VAR
                                  salesInvLine@1000000000 : Record 113;
                                  nfeSetup@1000000004 : Record 52112625;
                                  noSeriesMgt@1000000003 : Codeunit 396;
                                  nfseBatch@1000000002 : Record 52112633;
                                  nfseTributationCode@52006500 : Record 52112635;
                                  item@1000000005 : Record 27;
                                  nfsServiceCode@1000000006 : Record 52112623;
                                  branchServiceCode@1000000007 : Record 52112637;
                                  serviceDescription@52006501 : Text;
                                  serviceCode@52006502 : Text;
                                BEGIN
                                  IF Number = 1 THEN
                                    SalesInvoiceHeader.FINDFIRST
                                  ELSE
                                    SalesInvoiceHeader.NEXT;

                                  nfeSetup.GET(SalesInvoiceHeader."Branch Code");
                                  IF nfeSetup."Use DateTime As Lot No." THEN
                                    BatchNo := FORMAT(CURRENTDATETIME, 0, '<Year,2><Month,2><Day,2><Hours24,2><Minutes,2><Seconds,2>')
                                  ELSE
                                    BatchNo := noSeriesMgt.GetNextNo(nfeSetup."Batch Nos.", WORKDATE, TRUE);
                                  nfseBatch.INIT;
                                  nfseBatch."Batch No."          := BatchNo;
                                  nfseBatch."Branch Code"        := SalesInvoiceHeader."Branch Code";
                                  nfseBatch.Status               := nfseBatch.Status::Enviado;
                                  nfseBatch.INSERT;

                                  Customer.GET(SalesInvoiceHeader."Bill-to Customer No.");

                                  CustomerName := DELCHR(Customer.Name, '<>', ' ');
                                  IF STRLEN(Customer."Name 2") > 0 THEN
                                    CustomerName := ' ' + DELCHR(Customer."Name 2", '<>', ' ');

                                  CNPJCPF := DELCHR(Customer."C.N.P.J./C.P.F.", '<=>', ',.-/\');
                                  IF ((Customer.Category = Customer.Category::"1.- Person") AND (CNPJCPF = '') ) OR (Customer.Category = Customer.Category::"3.- Foreign") THEN
                                    CNPJCPF := 'PFNI';

                                  IM := DELCHR(Customer."C.C.M.", '<=>', ',.-/\');

                                  TipoCodificacao := '1'; // Lei 116, se for CNAE deve ser '2';

                                  //IF IE = 'ISENTO' THEN
                                    IE := '        ';

                                  //IF IM = '' THEN
                                    IM := '        ';

                                  SalesInvoiceHeader.CALCFIELDS(Amount);
                                  ValorServicos := SalesInvoiceHeader.Amount;
                                  ValorBase     := SalesInvoiceHeader.Amount;

                                  TotalServicos += ValorServicos;
                                  TotalBase     += ValorBase;

                                  SalesInvoiceHeader.TESTFIELD("NFS-e Tributation Code");
                                  nfseTributationCode.GET(SalesInvoiceHeader."Branch Code", SalesInvoiceHeader."NFS-e Tributation Code");
                                  nfseTributationCode.TESTFIELD("Tributation Code");
                                  Situacao := nfseTributationCode."Tributation Code";

                                  SalesInvoiceHeader.CALCFIELDS("Credit Memos");
                                  IF SalesInvoiceHeader."Credit Memos" THEN
                                    Situacao := 'C';

                                  salesInvLine.SETRANGE("Document No.", SalesInvoiceHeader."No.");
                                  salesInvLine.SETRANGE(Type, salesInvLine.Type::Item);
                                  IF salesInvLine.FINDFIRST THEN BEGIN
                                    REPEAT
                                      item.GET(salesInvLine."No.");
                                      IF item.Service THEN BEGIN
                                        item.TESTFIELD("Service Code");
                                        nfsServiceCode.GET(item."Service Code");

                                        CLEAR(branchServiceCode);
                                        IF branchServiceCode.GET(nfsServiceCode.Code, SalesInvoiceHeader."Branch Code") THEN;

                                        IF (SalesInvoiceHeader."Branch Code" = '') OR (branchServiceCode."Branch Code" = '')  THEN BEGIN
                                          nfsServiceCode.TESTFIELD("Tributation Code");
                                          serviceCode := nfsServiceCode."Tributation Code";
                                        END ELSE BEGIN
                                          branchServiceCode.TESTFIELD("Tributation Code");
                                          serviceCode := branchServiceCode."Tributation Code";
                                        END;
                                      END;
                                    UNTIL (salesInvLine.NEXT = 0) OR (serviceCode <> '');
                                  END ELSE
                                    ERROR(Text004);

                                  salesInvLine.RESET;
                                  salesInvLine.SETRANGE("Document No.", SalesInvoiceHeader."No.");
                                  IF salesInvLine.FINDFIRST THEN
                                    REPEAT
                                      serviceDescription += salesInvLine.Description;
                                      IF salesInvLine."Description 2" <> '' THEN
                                        serviceDescription += ' ' + salesInvLine."Description 2";
                                      serviceDescription += '|';
                                    UNTIL salesInvLine.NEXT = 0;
                                  serviceDescription := DELCHR(serviceDescription, '>', '|');
                                  serviceDescription := COPYSTR(serviceDescription, 1, 1000);

                                  IF nfseBatch.GET(SalesInvoiceHeader."E-Invoice Batch No.", SalesInvoiceHeader."Branch Code") THEN
                                    IF (nfseBatch.Status = nfseBatch.Status::Enviado) OR (nfseBatch.Status = nfseBatch.Status::Sucesso) THEN
                                      ERROR(Text006, nfseBatch."Batch No.");

                                  SalesInvoiceHeader."E-Invoice Batch No."      := BatchNo;
                                  SalesInvoiceHeader."E-Invoice File Generated" := TRUE;
                                  SalesInvoiceHeader.MODIFY;

                                  T2 := '2' + TextFunctions.FormatText(TextFunctions.PadLeft(SalesInvoiceHeader."E-Invoice RPS No.", 12, '0'),12,FALSE)
                                            + TipoCodificacao
                                            + TextFunctions.PadLeft(serviceCode, 7, ' ')
                                            + Situacao
                                            + FormatDec(ValorServicos)
                                            + FormatDec(ValorBase)
                                            + TextFunctions.FormatText(CNPJCPF,15,FALSE)
                                            + TextFunctions.FormatText(FormatText(IM),8,FALSE)
                                            + TextFunctions.FormatText(FormatText(IE),8,FALSE)
                                            + TextFunctions.FormatText(CustomerName,100,FALSE)
                                            + TextFunctions.FormatText(Customer.Address,50,FALSE)
                                            + TextFunctions.FormatText(Customer.Number,10,FALSE)
                                            + TextFunctions.FormatText(Customer."Address 2",30,FALSE)
                                            + TextFunctions.FormatText(Customer.District,30,FALSE)
                                            + TextFunctions.FormatText(Customer.City,50,FALSE)
                                            + Customer."Territory Code"
                                            + TextFunctions.FormatText(DELCHR(Customer."Post Code", '<=>', '.,-/'),8,FALSE)
                                            + TextFunctions.FormatText(Customer."E-Mail",100,FALSE)
                                            + serviceDescription;

                                  FileExportMgt.WriteLine(T2);
                                END;
                                 }

    { 1000000002;;DataItem;Footer            ;
               DataItemTable=Table2000000026;
               DataItemTableView=SORTING(Number)
                                 WHERE(Number=FILTER(1));
               OnAfterGetRecord=BEGIN
                                  T9 := '9' + FormatInt(SalesInvoiceHeader.COUNT) + FormatDec(TotalServicos) + FormatDec(TotalBase);

                                  FileExportMgt.WriteLine(T9);
                                END;
                                 }

  }
  REQUESTPAGE
  {
    PROPERTIES
    {
    }
    CONTROLS
    {
    }
  }
  LABELS
  {
  }
  CODE
  {
    VAR
      FileExportMgt@1000000088 : Codeunit 52112429;
      Customer@1000000011 : Record 18;
      TipoCodificacao@1000000010 : Text[30];
      Situacao@1000000008 : Text[30];
      ValorServicos@1000000007 : Decimal;
      ValorBase@1000000006 : Decimal;
      TotalServicos@1000000005 : Decimal;
      TotalBase@1000000004 : Decimal;
      CNPJCPF@1000000003 : Text[30];
      IE@1000000002 : Text[30];
      IM@1000000001 : Text[30];
      CustomerName@1000000000 : Text[100];
      T1@1000000012 : Text;
      T2@1000000013 : Text;
      T9@1000000014 : Text;
      Text001@1000000017 : TextConst 'PTB=Um filtro de data deve ser especificado.';
      Text002@1000000016 : TextConst 'PTB=Nenhuma nota fiscal foi encontrada.';
      Text006@1000000023 : TextConst 'PTB=Lote %1 dever� ser consultado antes do envio.';
      TextFunctions@1000000018 : Codeunit 52112453;
      GenFunc@1000000020 : Codeunit 52112423;
      BatchNo@1000000022 : Code[20];
      Text004@1000000015 : TextConst 'PTB=Nenhuma linha da nota fiscal possui c�digo de servi�o';

    PROCEDURE FormatText@1102300085(_text@1102300000 : Text[1024]) text : Text[1024];
    BEGIN
      text := TextFunctions.Ascii2Ansi(_text);
    END;

    PROCEDURE FormatInt@1102300038(int@1102300000 : Integer) text : Text[1024];
    BEGIN
      text := FORMAT(int);
      text := PADSTR('', 10 - STRLEN(text), '0') + text;
    END;

    PROCEDURE FormatDec@1102300039(dec@1102300000 : Decimal) text : Text[1024];
    BEGIN
      text := FORMAT(dec, 0, '<Precision,2:2><Standard Format,0>');
      text := DELCHR(text, '<=>', ',.');
      text := PADSTR('', 15 - STRLEN(text), '0') + text;
    END;

    BEGIN
    END.
  }
  RDLDATA
  {
  }
}

