OBJECT Table 6506 Item Tracking Comment
{
  OBJECT-PROPERTIES
  {
    Date=05/11/08;
    Time=12:00:00;
    Version List=NAVW16.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Item Tracking Comment;
               PTB=Coment. Rastreabilidade Produto];
  }
  FIELDS
  {
    { 1   ;   ;Type                ;Option        ;CaptionML=[ENU=Type;
                                                              PTB=Tipo];
                                                   OptionCaptionML=[ENU=" ,Serial No.,Lot No.";
                                                                    PTB=" ,No S�rie,No. Lote"];
                                                   OptionString=[ ,Serial No.,Lot No.] }
    { 2   ;   ;Item No.            ;Code20        ;TableRelation=Item;
                                                   CaptionML=[ENU=Item No.;
                                                              PTB=No. Produto];
                                                   NotBlank=Yes }
    { 3   ;   ;Variant Code        ;Code10        ;TableRelation="Item Variant".Code WHERE (Item No.=FIELD(Item No.));
                                                   CaptionML=[ENU=Variant Code;
                                                              PTB=C�digo Variante] }
    { 4   ;   ;Serial/Lot No.      ;Code20        ;CaptionML=[ENU=Serial/Lot No.;
                                                              PTB=N� S�rie/Lote];
                                                   NotBlank=Yes }
    { 5   ;   ;Line No.            ;Integer       ;CaptionML=[ENU=Line No.;
                                                              PTB=N� Linha] }
    { 11  ;   ;Date                ;Date          ;CaptionML=[ENU=Date;
                                                              PTB=Data] }
    { 13  ;   ;Comment             ;Text80        ;CaptionML=[ENU=Comment;
                                                              PTB=Coment�rio] }
  }
  KEYS
  {
    {    ;Type,Item No.,Variant Code,Serial/Lot No.,Line No.;
                                                   Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

