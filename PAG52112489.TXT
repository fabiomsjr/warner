OBJECT Page 52112489 Tax Payment Bill Subform
{
  OBJECT-PROPERTIES
  {
    Date=16/04/14;
    Time=14:40:32;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Tax Vendor Ledger Entries;
               PTB=Movimentos Fornecedor Imposto];
    SourceTable=Table25;
    SourceTableView=WHERE(Tax Payment Bill No.=FILTER(<>''));
    PageType=ListPart;
    OnAfterGetRecord=BEGIN
                       StyleTxt := SetStyle;
                     END;

    ActionList=ACTIONS
    {
      { 52006500;    ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 52006501;1   ;Action    ;
                      CaptionML=[ENU=Remove;
                                 PTB=Remover];
                      Image=RemoveLine;
                      OnAction=BEGIN
                                 Action_Remove;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 52006546;0;Container;
                ContainerType=ContentArea }

    { 52006545;1;Group    ;
                GroupType=Repeater }

    { 52006544;2;Field    ;
                SourceExpr="Posting Date";
                Editable=FALSE }

    { 52006543;2;Field    ;
                SourceExpr="Document Type";
                Editable=FALSE;
                StyleExpr=StyleTxt }

    { 52006542;2;Field    ;
                SourceExpr="BR Prepayment";
                Editable=FALSE }

    { 52006541;2;Field    ;
                SourceExpr="Document No.";
                Editable=FALSE;
                StyleExpr=StyleTxt }

    { 52006540;2;Field    ;
                SourceExpr="External Document No.";
                Editable=FALSE }

    { 52006539;2;Field    ;
                SourceExpr="DI Invoice No.";
                Visible=FALSE;
                Editable=FALSE }

    { 52006538;2;Field    ;
                SourceExpr="Quote No.";
                Visible=FALSE }

    { 52006537;2;Field    ;
                SourceExpr="Order No.";
                Visible=FALSE }

    { 52006536;2;Field    ;
                SourceExpr="Vendor No.";
                Editable=FALSE }

    { 52006535;2;Field    ;
                SourceExpr="Vendor Posting Group";
                Visible=FALSE }

    { 52006534;2;Field    ;
                SourceExpr=Description;
                Editable=FALSE }

    { 52006533;2;Field    ;
                SourceExpr="Branch Code" }

    { 52006532;2;Field    ;
                SourceExpr="Tax Jurisdiction Code" }

    { 52006530;2;Field    ;
                SourceExpr="Global Dimension 1 Code";
                Visible=FALSE;
                Editable=FALSE }

    { 52006529;2;Field    ;
                SourceExpr="Global Dimension 2 Code";
                Visible=FALSE;
                Editable=FALSE }

    { 52006528;2;Field    ;
                SourceExpr="IC Partner Code";
                Visible=FALSE;
                Editable=FALSE }

    { 52006527;2;Field    ;
                SourceExpr="Purchaser Code";
                Visible=FALSE;
                Editable=FALSE }

    { 52006526;2;Field    ;
                SourceExpr="Currency Code";
                Editable=FALSE }

    { 52006525;2;Field    ;
                SourceExpr="Original Amount";
                Editable=FALSE }

    { 52006524;2;Field    ;
                SourceExpr="Original Amt. (LCY)";
                Visible=FALSE;
                Editable=FALSE }

    { 52006523;2;Field    ;
                SourceExpr=Amount;
                Editable=FALSE }

    { 52006522;2;Field    ;
                SourceExpr="Amount (LCY)";
                Visible=FALSE;
                Editable=FALSE }

    { 52006521;2;Field    ;
                SourceExpr="Remaining Amount";
                Editable=FALSE }

    { 52006520;2;Field    ;
                SourceExpr="Remaining Amt. (LCY)";
                Visible=FALSE;
                Editable=FALSE }

    { 52006519;2;Field    ;
                SourceExpr="Bal. Account Type";
                Visible=FALSE;
                Editable=FALSE }

    { 52006518;2;Field    ;
                SourceExpr="Bal. Account No.";
                Visible=FALSE;
                Editable=FALSE }

    { 52006517;2;Field    ;
                SourceExpr="Due Date";
                StyleExpr=StyleTxt }

    { 52006516;2;Field    ;
                SourceExpr="Pmt. Discount Date" }

    { 52006515;2;Field    ;
                SourceExpr="Pmt. Disc. Tolerance Date" }

    { 52006514;2;Field    ;
                SourceExpr="Original Pmt. Disc. Possible" }

    { 52006513;2;Field    ;
                SourceExpr="Remaining Pmt. Disc. Possible" }

    { 52006512;2;Field    ;
                SourceExpr="Max. Payment Tolerance" }

    { 52006511;2;Field    ;
                SourceExpr=Open;
                Editable=FALSE }

    { 52006510;2;Field    ;
                SourceExpr="On Hold" }

    { 52006509;2;Field    ;
                SourceExpr="User ID";
                Visible=FALSE;
                Editable=FALSE }

    { 52006508;2;Field    ;
                SourceExpr="Source Code";
                Visible=FALSE;
                Editable=FALSE }

    { 52006507;2;Field    ;
                SourceExpr="Reason Code";
                Visible=FALSE;
                Editable=FALSE }

    { 52006506;2;Field    ;
                SourceExpr=Reversed;
                Visible=FALSE }

    { 52006505;2;Field    ;
                SourceExpr="Reversed by Entry No.";
                Visible=FALSE;
                Editable=FALSE }

    { 52006504;2;Field    ;
                SourceExpr="Reversed Entry No.";
                Visible=FALSE;
                Editable=FALSE }

    { 52006503;2;Field    ;
                SourceExpr="Entry No.";
                Editable=FALSE }

  }
  CODE
  {
    VAR
      StyleTxt@52006500 : Text;
      Text001@52006501 : TextConst 'ENU=Are you sure you wish to remove document %1?;PTB=Tem certeza que deseja remover o documento %1?';

    PROCEDURE Action_Remove@52006503();
    VAR
      taxPaymentBill@52006500 : Record 52112469;
    BEGIN
      IF CONFIRM(STRSUBSTNO(Text001, "Document No.")) THEN BEGIN
        taxPaymentBill.RemoveEntry(Rec);
        CurrPage.UPDATE(FALSE);
      END;
    END;

    BEGIN
    END.
  }
}

