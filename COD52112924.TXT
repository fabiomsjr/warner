OBJECT Codeunit 52112924 XSL Transform
{
  OBJECT-PROPERTIES
  {
    Date=05/03/15;
    Time=10:54:21;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    OnRun=BEGIN
            MESSAGE(Transform(
              '<?xml version="1.0" encoding="UTF-8"?>'+
              '<catalog>'+
              '  <cd>'+
              '    <title>First Title</title>'+
              '    <artist>Bob Dylan</artist>'+
              '    <country>USA</country>'+
              '    <company>Columbia</company>'+
              '    <price>100000000.90</price>'+
              '    <year>1985</year>'+
              '  </cd>'+
              '  <cd>'+
              '    <title>Hide your heart</title>'+
              '    <artist>Bonnie Tyler</artist>'+
              '    <country>UK</country>'+
              '    <company>CBS Records</company>'+
              '    <price>9.90</price>'+
              '    <year>1988</year>'+
              '  </cd>'+
              '</catalog>'
            ,
              '<?xml version="1.0" encoding="UTF-8"?>'+
              '<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">'+
              '<xsl:decimal-format name="pt-br" decimal-separator="," grouping-separator="."/>'+
              '<xsl:template match="/">'+
              '  <html>'+
              '  <body>                    '+
              '    <h2>My CD Collection</h2>'+
              '    <h3><xsl:value-of select="catalog/cd/title[1]"/></h3>'+
              '    <table border="1">'+
              '      <tr bgcolor="#9acd32">'+
              '        <th>Title</th>'+
              '        <th>Artist</th>'+
              '        <th>Price</th>   '+
              '      </tr>                '+
              '      <xsl:for-each select="catalog/cd">'+
              '      <tr>                                '+
              '        <td><xsl:value-of select="title" /></td>'+
              '        <td><xsl:value-of select="artist" /></td>'+
              '        <td>R$ <xsl:value-of select="format-number(price, ''#.###,00'', ''pt-br'')" /></td>'+
              '      </tr>'+
              '      </xsl:for-each>'+
              '    </table>'+
              '  </body>'+
              '  </html>  '+
              '</xsl:template>'+
              '</xsl:stylesheet>'

            ));
          END;

  }
  CODE
  {

    PROCEDURE Transform@52006501(XML@52006500 : Text;XSL@52006502 : Text) : Text;
    VAR
      StringReaderXml@52006501 : DotNet "'mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.IO.StringReader";
      StringReaderXsl@52006503 : DotNet "'mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.IO.StringReader";
      XmlReaderXml@52006504 : DotNet "'System.Xml, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.Xml.XmlReader";
      XmlReaderXsl@52006505 : DotNet "'System.Xml, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.Xml.XmlReader";
      XslCompiledTransform@52006506 : DotNet "'System.Xml, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.Xml.Xsl.XslCompiledTransform";
      StringWriter@52006507 : DotNet "'mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.IO.StringWriter";
      XmlWriter@52006508 : DotNet "'System.Xml, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.Xml.XmlWriter";
    BEGIN
      StringReaderXml := StringReaderXml.StringReader(XML);
      StringReaderXsl := StringReaderXsl.StringReader(XSL);
      XmlReaderXml := XmlReaderXml.Create(StringReaderXml);
      XmlReaderXsl := XmlReaderXsl.Create(StringReaderXsl);
      XslCompiledTransform := XslCompiledTransform.XslCompiledTransform;
      XslCompiledTransform.Load(XmlReaderXsl);
      StringWriter := StringWriter.StringWriter;
      XmlWriter := XmlWriter.Create(StringWriter, XslCompiledTransform.OutputSettings);
      XslCompiledTransform.Transform(XmlReaderXml, XmlWriter);
      EXIT(StringWriter.ToString);
    END;

    BEGIN
    END.
  }
}

