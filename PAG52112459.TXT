OBJECT Page 52112459 Fiscal Data Update Subform
{
  OBJECT-PROPERTIES
  {
    Date=04/12/15;
    Time=10:37:57;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Lines;
               PTB=Linhas];
    InsertAllowed=No;
    DeleteAllowed=No;
    SourceTable=Table52112452;
    PageType=ListPart;
    SourceTableTemporary=Yes;
    ShowFilter=No;
  }
  CONTROLS
  {
    { 35000000;0;Container;
                ContainerType=ContentArea }

    { 35000001;1;Group    ;
                Name=Group;
                GroupType=Repeater;
                FreezeColumnID=Line Amount }

    { 35000002;2;Field    ;
                SourceExpr="No." }

    { 35000003;2;Field    ;
                SourceExpr=Description }

    { 35000004;2;Field    ;
                SourceExpr=Quantity }

    { 35000005;2;Field    ;
                SourceExpr="Direct Unit Cost" }

    { 35000006;2;Field    ;
                SourceExpr="Line Amount" }

    { 35000007;2;Field    ;
                SourceExpr="CFOP Code" }

    { 35000008;2;Field    ;
                SourceExpr="NCM Code" }

    { 52006500;2;Field    ;
                SourceExpr="Origin Code" }

    { 35000009;2;Field    ;
                SourceExpr="ICMS CST Code" }

    { 35000010;2;Field    ;
                SourceExpr="ICMS Base Isenta" }

    { 35000011;2;Field    ;
                SourceExpr="ICMS Base Outras" }

    { 35000012;2;Field    ;
                SourceExpr="IPI CST Code" }

    { 52006501;2;Field    ;
                SourceExpr="IPI Statute Code" }

    { 35000013;2;Field    ;
                SourceExpr="IPI Base Isenta" }

    { 35000014;2;Field    ;
                SourceExpr="IPI Base Outras" }

    { 35000015;2;Field    ;
                SourceExpr="PIS CST Code" }

    { 35000016;2;Field    ;
                SourceExpr="Perc. PIS" }

    { 35000017;2;Field    ;
                SourceExpr="Valor PIS" }

    { 35000018;2;Field    ;
                SourceExpr="COFINS CST Code" }

    { 35000019;2;Field    ;
                SourceExpr="Perc. COFINS" }

    { 35000020;2;Field    ;
                SourceExpr="Valor COFINS" }

    { 35000021;2;Field    ;
                SourceExpr="Natureza Receita PIS" }

    { 35000022;2;Field    ;
                SourceExpr="Natureza Receita COFINS" }

    { 35000023;2;Field    ;
                SourceExpr="Base Calculation Credit Code" }

    { 35000024;2;Field    ;
                SourceExpr="Addition Number" }

  }
  CODE
  {

    PROCEDURE SetLines@35000000(VAR updLine@35000000 : TEMPORARY Record 52112452);
    BEGIN
      RESET;
      DELETEALL;
      IF updLine.FINDSET THEN
        REPEAT
          INIT;
          TRANSFERFIELDS(updLine);
          INSERT;
        UNTIL updLine.NEXT = 0;
      IF FINDFIRST THEN;
    END;

    PROCEDURE GetLines@35000001(VAR updLine@35000000 : TEMPORARY Record 52112452);
    BEGIN
      RESET;
      updLine.RESET;
      updLine.DELETEALL;
      IF FINDSET THEN
        REPEAT
          updLine.INIT;
          updLine.TRANSFERFIELDS(Rec);
          updLine.INSERT;
        UNTIL NEXT = 0;
      IF FINDFIRST THEN;
    END;

    BEGIN
    END.
  }
}

