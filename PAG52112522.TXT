OBJECT Page 52112522 License Permission
{
  OBJECT-PROPERTIES
  {
    Date=26/02/14;
    Time=15:01:59;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    Editable=No;
    SourceTable=Table2000000043;
    PageType=List;
  }
  CONTROLS
  {
    { 52006500;0;Container;
                ContainerType=ContentArea }

    { 52006501;1;Group    ;
                Name=Group;
                GroupType=Repeater }

    { 52006502;2;Field    ;
                SourceExpr="Object Type" }

    { 52006503;2;Field    ;
                SourceExpr="Object Number" }

    { 52006504;2;Field    ;
                SourceExpr="Read Permission" }

    { 52006505;2;Field    ;
                SourceExpr="Insert Permission" }

    { 52006506;2;Field    ;
                SourceExpr="Modify Permission" }

    { 52006507;2;Field    ;
                SourceExpr="Delete Permission" }

    { 52006508;2;Field    ;
                SourceExpr="Execute Permission" }

  }
  CODE
  {

    BEGIN
    END.
  }
}

