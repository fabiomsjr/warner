OBJECT Table 5845 Inventory Report Header
{
  OBJECT-PROPERTIES
  {
    Date=05/11/08;
    Time=12:00:00;
    Version List=NAVW16.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Inventory Report Header;
               PTB=Cabe�alho Relat�rio Invent�rio];
  }
  FIELDS
  {
    { 1   ;   ;Code                ;Code10        ;CaptionML=[ENU=Code;
                                                              PTB=C�digo] }
    { 3   ;   ;Item Filter         ;Code20        ;FieldClass=FlowFilter;
                                                   CaptionML=[ENU=Item Filter;
                                                              PTB=Filtros Produto] }
    { 5   ;   ;Location Filter     ;Code10        ;FieldClass=FlowFilter;
                                                   CaptionML=[ENU=Location Filter;
                                                              PTB=Filtro Dep�sito] }
    { 6   ;   ;Posting Date Filter ;Date          ;FieldClass=FlowFilter;
                                                   CaptionML=[ENU=Posting Date Filter;
                                                              PTB=Data Filtro Registro] }
    { 7   ;   ;Calculated          ;Boolean       ;CaptionML=[ENU=Calculated;
                                                              PTB=Calculado] }
    { 9   ;   ;Line Option         ;Option        ;CaptionML=[ENU=Line Option;
                                                              PTB=Op��o Linha];
                                                   OptionCaptionML=[ENU=Balance Sheet,Income Statement;
                                                                    PTB=Folha Balan�o,Mensagem de Entrada];
                                                   OptionString=Balance Sheet,Income Statement }
    { 10  ;   ;Column Option       ;Option        ;CaptionML=[ENU=Column Option;
                                                              PTB=Op��o Coluna];
                                                   OptionCaptionML=[ENU=Balance Sheet,Income Statement;
                                                                    PTB=Folha Balan�o,Mensagem de Entrada];
                                                   OptionString=Balance Sheet,Income Statement }
    { 11  ;   ;Show Warning        ;Boolean       ;CaptionML=[ENU=Show Warning;
                                                              PTB=Mostrar Aviso] }
  }
  KEYS
  {
    {    ;Code                                    ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

