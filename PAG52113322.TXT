OBJECT Page 52113322 Change G/L Entry Branch Dim.
{
  OBJECT-PROPERTIES
  {
    Date=25/03/14;
    Time=10:27:47;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Change G/L Entry Branch Dim.;
               PTB=Modificar Dim. Filial Mov. Cont�ibl];
    InsertAllowed=No;
    DeleteAllowed=No;
    SourceTable=Table17;
    PageType=Worksheet;
    ActionList=ACTIONS
    {
      { 52006514;0   ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 52006513;1   ;Action    ;
                      ShortCutKey=Shift+Ctrl+D;
                      CaptionML=[ENU=Dimensions;
                                 PTB=Dimens�es];
                      Image=Dimensions;
                      OnAction=BEGIN
                                 ShowDimensions;
                                 CurrPage.SAVERECORD;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 52006500;0;Container;
                ContainerType=ContentArea }

    { 52006501;1;Group    ;
                Name=Group;
                GroupType=Repeater }

    { 52006502;2;Field    ;
                SourceExpr="Entry No.";
                Editable=FALSE }

    { 52006503;2;Field    ;
                SourceExpr="G/L Account No.";
                Editable=FALSE }

    { 52006504;2;Field    ;
                SourceExpr="Posting Date";
                Editable=FALSE }

    { 52006505;2;Field    ;
                SourceExpr="Document Type";
                Editable=FALSE }

    { 52006506;2;Field    ;
                SourceExpr="Document No.";
                Editable=FALSE }

    { 52006507;2;Field    ;
                SourceExpr=Description;
                Editable=FALSE }

    { 52006508;2;Field    ;
                SourceExpr=Amount;
                Editable=FALSE }

    { 52006509;2;Field    ;
                SourceExpr="Global Dimension 1 Code";
                Editable=FALSE }

    { 52006510;2;Field    ;
                SourceExpr="Global Dimension 2 Code";
                Editable=FALSE }

    { 52006511;2;Field    ;
                SourceExpr="Branch Code";
                Editable=FALSE }

    { 52006515;2;Field    ;
                SourceExpr="Dimension Set ID";
                Editable=FALSE }

    { 52006512;2;Field    ;
                Name=NewBranchCode_;
                CaptionML=[ENU=New Branch Code;
                           PTB=Novo C�d. Filial];
                SourceExpr=NewBranchCode;
                TableRelation="Branch Information";
                OnValidate=VAR
                             branchInfo@52006500 : Record 52112423;
                             tmpDimSetEntry@52006501 : TEMPORARY Record 480;
                             dimMgt@52006502 : Codeunit 408;
                             glSetup@52006503 : Record 98;
                           BEGIN
                             glSetup.GET;
                             branchInfo.GET(NewBranchCode);
                             branchInfo.TESTFIELD("Dimension Code");
                             branchInfo.TESTFIELD("Dimension Value Code");
                             dimMgt.GetDimensionSet(tmpDimSetEntry, "Dimension Set ID");
                             tmpDimSetEntry."Dimension Code" := branchInfo."Dimension Code";
                             tmpDimSetEntry."Dimension Value Code" := branchInfo."Dimension Value Code";
                             IF NOT tmpDimSetEntry.FIND THEN
                               tmpDimSetEntry.INSERT;
                             tmpDimSetEntry.VALIDATE("Dimension Value Code", branchInfo."Dimension Value Code");
                             tmpDimSetEntry.MODIFY;
                             "Dimension Set ID" := dimMgt.GetDimensionSetID(tmpDimSetEntry);

                             IF glSetup."Global Dimension 1 Code" = tmpDimSetEntry."Dimension Code" THEN
                               "Global Dimension 1 Code" := tmpDimSetEntry."Dimension Value Code";
                             IF glSetup."Global Dimension 2 Code" = tmpDimSetEntry."Dimension Code" THEN
                               "Global Dimension 2 Code" := tmpDimSetEntry."Dimension Value Code";

                             "Branch Code" := branchInfo.Code;

                             MODIFY;

                             NewBranchCode := '';
                           END;
                            }

  }
  CODE
  {
    VAR
      NewBranchCode@52006500 : Code[20];

    BEGIN
    END.
  }
}

