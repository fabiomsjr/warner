OBJECT Table 35000038 Periodic Tax Setup2
{
  OBJECT-PROPERTIES
  {
    Date=29/02/12;
    Time=18:14:18;
    Version List=NAVBR5.01.0109;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Periodic Tax Setup;
               PTB=Conf. de IMpostos Peri�dicos];
  }
  FIELDS
  {
    { 1   ;   ;Tax Identification  ;Option        ;CaptionML=[ENU=Tax Identification;
                                                              PTB=Identifica��o do imposto];
                                                   OptionCaptionML=[ENU=" ,IPI,ICMS,PIS,COFINS,II,IRRF,INSS,ISS,CSL Ret.,PIS Ret.,COFINS Ret.,ISS Ret.,INSS Ret.,Dif. Aliq. ICMS";
                                                                    PTB=" ,IPI,ICMS,PIS,COFINS,II,IRRF,INSS,ISS,CSL Ret.,PIS Ret.,COFINS Ret.,ISS Ret.,INSS Ret.,Dif. Aliq. ICMS"];
                                                   OptionString=[ ,IPI,ICMS,PIS,COFINS,II,IRRF,INSS,ISS,CSL Ret.,PIS Ret.,COFINS Ret.,ISS Ret.,INSS Ret.,Dif. Aliq. ICMS] }
    { 2   ;   ;Tax Installment Times;Integer      ;Description=NAVBR5.01.0109.02 }
    { 3   ;   ;Periodic Tax Instalment;DateFormula;Description=NAVBR5.01.0109.02 }
  }
  KEYS
  {
    {    ;Tax Identification                      ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

