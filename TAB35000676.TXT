OBJECT Table 35000676 EFD Operation Complement2
{
  OBJECT-PROPERTIES
  {
    Date=29/02/12;
    Time=09:36:52;
    Version List=FNX039.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Social Contribution Calculated";
               PTB=Contribui��o Social Apurada];
    LookupPageID=Page35000676;
    DrillDownPageID=Page35000676;
  }
  FIELDS
  {
    { 1   ;   ;Tax Settlement Code ;Code20        ;CaptionML=PTB=C�digo Tribut�rio Liquida��o }
    { 2   ;   ;Document ID         ;Integer        }
    { 3   ;   ;Company ID          ;Text30         }
    { 10  ;   ;Tax Identification  ;Option        ;CaptionML=PTB=Identifica��o do Imposto;
                                                   OptionCaptionML=PTB=Pis,Cofins;
                                                   OptionString=Pis,Cofins }
    { 11  ;   ;CST Code            ;Code10        ;CaptionML=PTB=C�digo CST }
    { 12  ;   ;Code Nature Basis   ;Code10        ;CaptionML=PTB=C�digo da Natureza da Base }
    { 13  ;   ;Line Amount         ;Decimal        }
    { 14  ;   ;Base                ;Decimal       ;CaptionML=PTB=Valor da Base }
    { 15  ;   ;Tax %               ;Decimal       ;CaptionML=PTB=Taxa % }
    { 16  ;   ;Tax Amount          ;Decimal       ;CaptionML=PTB=Valor do Imposto }
    { 17  ;   ;Account No.         ;Code20        ;CaptionML=PTB=No. da Conta }
  }
  KEYS
  {
    {    ;Tax Settlement Code,Document ID,Tax Identification,CST Code,Code Nature Basis;
                                                   Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    PROCEDURE Add@1102300015(docLine@1102300000 : Record 35000627);
    VAR
      taxId@1102300001 : Integer;
      cfop@1102300004 : Record 35000004;
    BEGIN
      RESET;
      //FiscalDocumentType.GET(VatEntry."Fiscal Document Type");
      //IF NOT (FiscalDocumentType."Electronic File Code" IN ['06','28','29','21','22','07','08','8B','09','10','11','26','27','57']) THEN
      //  EXIT;

      FOR taxId := "Tax Identification"::Pis TO "Tax Identification"::Cofins DO BEGIN
        INIT;
        "Tax Settlement Code" := docLine."Tax Settlement No.";
        "Document ID" := docLine."Document ID";

        "Code Nature Basis" := docLine."Base Calculation Credit Code";
        IF "Code Nature Basis" = '' THEN
          IF cfop.GET(docLine."CFOP Code") THEN
            "Code Nature Basis" := cfop."Basis Calculation Credit Code";

        "Tax Identification" := taxId;

        CASE taxId OF
          "Tax Identification"::Pis: BEGIN
            "CST Code" := docLine."PIS CST Code";
          END;
          "Tax Identification"::Cofins: BEGIN
            "CST Code" := docLine."COFINS CST Code";
          END;
        END;

        IF NOT FIND THEN
          INSERT;

        CASE taxId OF
          "Tax Identification"::Pis: BEGIN
            "Tax %"      := docLine."PIS %";
            Base         += docLine."PIS Base";
            "Tax Amount" += docLine."PIS Amount";
          END;
          "Tax Identification"::Cofins: BEGIN
            "Tax %"      := docLine."COFINS %";
            Base         += docLine."COFINS Base";
            "Tax Amount" += docLine."COFINS Amount";
          END;
        END;

        "Company ID" := docLine."Company ID";
        "Line Amount" += docLine."Line Amount";
        MODIFY;
      END;
    END;

    BEGIN
    END.
  }
}

