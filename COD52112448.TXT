OBJECT Codeunit 52112448 Import from Excel
{
  OBJECT-PROPERTIES
  {
    Date=30/04/13;
    Time=16:07:35;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    OnRun=BEGIN
          END;

  }
  CODE
  {

    PROCEDURE Import@1102300001(VAR masterRecRef@1102300000 : RecordRef;lineNoFieldID@1102300017 : Integer);
    VAR
      filename@1102300015 : Text[1024];
      fileMgt@1102300014 : Codeunit 419;
      recRef@1102300012 : RecordRef;
      fieldRef@1102300011 : FieldRef;
      field@1102300010 : Record 2000000041;
      masterFieldRef@1102300013 : FieldRef;
      excelBuf@1102300009 : TEMPORARY Record 370;
      col@1102300008 : Integer;
      lastCol@1102300007 : Integer;
      columns@1102300006 : ARRAY [1000] OF Text[100];
      row@1102300005 : Integer;
      lastRow@1102300016 : Integer;
      doExit@1102300004 : Boolean;
      i@1102300003 : Integer;
      date@1102300002 : Date;
      decimal@1102300001 : Decimal;
      lineNo@1102300018 : Integer;
    BEGIN
      filename := fileMgt.OpenFileDialog('Selecionar arquivo Excel', '', '');
      IF filename = '' THEN
        EXIT;

      excelBuf.OpenBook(filename, excelBuf.SelectSheetsName(filename));
      excelBuf.ReadSheet;
      excelBuf.FINDLAST;
      lastRow := excelBuf."Row No.";

      row := 1;
      excelBuf.SETRANGE("Row No.", row);
      excelBuf.FINDFIRST;
      REPEAT
         lastCol := excelBuf."Column No.";
         columns[lastCol] := excelBuf."Cell Value as Text";
      UNTIL excelBuf.NEXT = 0;

      recRef.OPEN(masterRecRef.NUMBER);
      REPEAT
        row += 1;
        excelBuf.SETRANGE("Row No.", row);

        recRef.INIT;
        field.RESET;
        field.SETRANGE(Class, field.Class::Normal);
        field.SETRANGE(TableNo, recRef.NUMBER);
        IF field.FINDFIRST THEN
          REPEAT
            fieldRef := recRef.FIELD(field."No.");
            masterFieldRef := masterRecRef.FIELD(field."No.");
            fieldRef.VALUE(masterFieldRef.VALUE);
          UNTIL field.NEXT = 0;

        IF lineNoFieldID <> 0 THEN BEGIN
          lineNo += 10000;
          fieldRef := recRef.FIELD(lineNoFieldID);
          fieldRef.VALUE(lineNo);
        END;

        FOR col := 1 TO lastCol DO BEGIN
          excelBuf.SETRANGE("Column No.", col);
          IF excelBuf.FINDFIRST THEN BEGIN
            field.SETRANGE("Field Caption", columns[col]);
            IF field.FINDFIRST THEN BEGIN
              fieldRef := recRef.FIELD(field."No.");
              CASE field.Type OF
                field.Type::Option: BEGIN
                  FOR i := 1 TO GetOptionLength(fieldRef.OPTIONCAPTION) DO
                    IF SELECTSTR(i, fieldRef.OPTIONCAPTION) = excelBuf."Cell Value as Text" THEN
                      fieldRef.VALUE(i - 1);
                END;
                field.Type::Date: BEGIN
                  EVALUATE(date, excelBuf."Cell Value as Text");
                  fieldRef.VALUE(date);
                END;
                field.Type::Decimal, field.Type::Integer: BEGIN
                  EVALUATE(decimal, excelBuf."Cell Value as Text");
                  fieldRef.VALUE(decimal);
                END;
                field.Type::OemText, field.Type::OemCode:
                  fieldRef.VALUE(excelBuf."Cell Value as Text");
              END;
            END;
          END;
        END;
        recRef.INSERT;
      UNTIL row = lastRow;
    END;

    LOCAL PROCEDURE GetOptionLength@1102300003(optionStr@1102300000 : Text[1024]) : Integer;
    VAR
      i@1102300001 : Integer;
      count@1102300002 : Integer;
    BEGIN
      FOR i := 1 TO STRLEN(optionStr) DO
        IF optionStr[i] = ',' THEN
          count += 1;
      EXIT(count + 1);
    END;

    BEGIN
    END.
  }
}

