OBJECT Table 52112472 NBS Service Code
{
  OBJECT-PROPERTIES
  {
    Date=20/07/15;
    Time=09:42:56;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    DataPerCompany=No;
    OnDelete=VAR
               TaxExceptionPerc@52006500 : Record 52112474;
             BEGIN
               TaxExceptionPerc.SETRANGE("Exception Code", Code);
               TaxExceptionPerc.DELETEALL(TRUE);
             END;

    CaptionML=[ENU=NBS Service Code;
               PTB=C�digo Servi�o NBS];
    LookupPageID=Page52112507;
  }
  FIELDS
  {
    { 10  ;   ;Code                ;Code20        ;CaptionML=[ENU=Code;
                                                              PTB=C�digo];
                                                   NotBlank=Yes }
    { 20  ;   ;Description         ;Text250       ;CaptionML=[ENU=Description;
                                                              PTB=Descri��o] }
  }
  KEYS
  {
    {    ;Code                                    ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

