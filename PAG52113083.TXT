OBJECT Page 52113083 Approval Flow Entries Log
{
  OBJECT-PROPERTIES
  {
    Date=20/03/15;
    Time=10:28:04;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Approval Entries Log;
               PTB=Log Movimentos Aprova��o];
    SourceTable=Table52113074;
    PageType=List;
    PromotedActionCategoriesML=[ENU=New,Approval,Navigate;
                                PTB=Novo,Aprova��o,Navegar];
    ActionList=ACTIONS
    {
      { 52006518;0   ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 52006519;1   ;Action    ;
                      Name=Document;
                      CaptionML=[ENU=Document;
                                 PTB=Documento];
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=Document;
                      PromotedCategory=Report;
                      OnAction=BEGIN
                                 ActionDocument;
                               END;
                                }
      { 52006520;1   ;Action    ;
                      Name=Flow;
                      CaptionML=[ENU=Flow;
                                 PTB=Fluxo];
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=Approvals;
                      PromotedCategory=Report;
                      OnAction=BEGIN
                                 ActionFlow;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 52006500;0;Container;
                ContainerType=ContentArea }

    { 52006501;1;Group    ;
                Name=Group;
                Editable=FALSE;
                GroupType=Repeater }

    { 52006502;2;Field    ;
                SourceExpr="Approval Flow No." }

    { 52006503;2;Field    ;
                SourceExpr="Sequence No." }

    { 52006504;2;Field    ;
                SourceExpr=Description }

    { 52006505;2;Field    ;
                SourceExpr=Amount }

    { 52006506;2;Field    ;
                SourceExpr="Approver ID" }

    { 52006507;2;Field    ;
                SourceExpr=Status }

    { 52006508;2;Field    ;
                SourceExpr="Due Date" }

    { 52006509;2;Field    ;
                SourceExpr="Last Date-Time Modified" }

    { 52006510;2;Field    ;
                SourceExpr="Last Modified By ID" }

    { 52006513;2;Field    ;
                SourceExpr="Last E-Mail Date";
                Visible=FALSE }

    { 52006514;2;Field    ;
                SourceExpr=Comments }

    { 52006512;2;Field    ;
                SourceExpr=Reason }

  }
  CODE
  {

    LOCAL PROCEDURE ActionDocument@52006506();
    BEGIN
      OpenDocument;
    END;

    LOCAL PROCEDURE ActionFlow@52006507();
    BEGIN
      OpenFlow;
    END;

    BEGIN
    END.
  }
}

