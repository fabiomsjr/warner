OBJECT Page 826 DO Payment Setup
{
  OBJECT-PROPERTIES
  {
    Date=09/09/14;
    Time=12:00:00;
    Version List=NAVW18.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Microsoft Dynamics ERP Payment Services Setup;
               PTB=Microsoft D�namics ERP Payment Services Setup];
    InsertAllowed=No;
    DeleteAllowed=No;
    SourceTable=Table826;
    PageType=Card;
    OnInit=BEGIN
             MCAIsEnabled := TRUE;
           END;

    OnOpenPage=BEGIN
                 RESET;
                 IF NOT GET THEN BEGIN
                   INIT;
                   INSERT;
                 END;
               END;

    OnAfterGetRecord=BEGIN
                       MCAIsEnabled := "Charge Type" = "Charge Type"::Percent;
                     END;

  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                CaptionML=[ENU=General;
                           PTB=Geral] }

    { 2   ;2   ;Field     ;
                CaptionML=PTB=Autoriza��o Obrigat�ria;
                SourceExpr="Authorization Required" }

    { 4   ;2   ;Field     ;
                CaptionML=PTB=Dias Antes de Autoriza��o;
                SourceExpr="Days Before Auth. Expiry" }

    { 1905443501;1;Group  ;
                CaptionML=[ENU=Additional Charges;
                           PTB=Despesas Adicionais] }

    { 6   ;2   ;Field     ;
                CaptionML=PTB=Tipos Despesas;
                SourceExpr="Charge Type";
                OnValidate=BEGIN
                             MCAIsEnabled := "Charge Type" = "Charge Type"::Percent;
                           END;
                            }

    { 8   ;2   ;Field     ;
                CaptionML=PTB=Valor Despesas;
                SourceExpr="Charge Value" }

    { 10  ;2   ;Field     ;
                CaptionML=PTB=Max. Quantidade de carga (LCY);
                SourceExpr="Max. Charge Amount (LCY)";
                Enabled=MCAIsEnabled }

    { 1904569201;1;Group  ;
                CaptionML=[ENU=Numbering;
                           PTB=Numera��o] }

    { 12  ;2   ;Field     ;
                CaptionML=PTB=Cart�o de Cr�dito n � s;
                SourceExpr="Credit Card Nos." }

  }
  CODE
  {
    VAR
      MCAIsEnabled@1001 : Boolean INDATASET;

    BEGIN
    END.
  }
}

