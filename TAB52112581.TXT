OBJECT Table 52112581 Tax Usage Code
{
  OBJECT-PROPERTIES
  {
    Date=21/08/13;
    Time=09:56:50;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Tax Usage Code;
               PTB=C�d. Utiliza��o Imposto];
    LookupPageID=Page52112566;
    DrillDownPageID=Page52112566;
  }
  FIELDS
  {
    { 10  ;   ;Tax Identification  ;Option        ;CaptionML=[ENU=Tax Identification;
                                                              PTB=Identifica��o Imposto];
                                                   OptionCaptionML=[ENU=" ,IPI,ICMS,PIS,COFINS,II,IRRF,INSS,ISS,CSL Ret.,PIS Ret.,COFINS Ret.,ISS Ret.,INSS Ret.,Dif. Aliq. ICMS,ST,PCC,CIDE,Funrural,Simples";
                                                                    PTB=" ,IPI,ICMS,PIS,COFINS,II,IRRF,INSS,ISS,CSL Ret.,PIS Ret.,COFINS Ret.,ISS Ret.,INSS Ret.,Dif. Aliq. ICMS,ST,PCC,CIDE,Funrural,Simples"];
                                                   OptionString=[ ,IPI,ICMS,PIS,COFINS,II,IRRF,INSS,ISS,CSL Ret.,PIS Ret.,COFINS Ret.,ISS Ret.,INSS Ret.,Dif. Aliq. ICMS,ST,PCC,CIDE,Funrural,Simples] }
    { 40  ;   ;Code                ;Code20        ;CaptionML=[ENU=Code;
                                                              PTB=C�digo] }
    { 50  ;   ;Description         ;Text100       ;CaptionML=[ENU=Description;
                                                              PTB=Descri��o] }
  }
  KEYS
  {
    {    ;Tax Identification,Code                 ;Clustered=Yes }
  }
  FIELDGROUPS
  {
    { 1   ;DropDown            ;Code,Description                         }
  }
  CODE
  {

    BEGIN
    {
      NAVBR5.01.0109

      No.   dd.mm.yy   Developer   Company   DocNo.   Description
      -----------------------------------------------------------------------------------------------------------------
      00    01.12.06   MK          MICROSOFT LOC001   The reference Documentation file LOCALIZATIONBR.DOC
    }
    END.
  }
}

