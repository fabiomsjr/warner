OBJECT Page 52113075 Approval Flow Comments
{
  OBJECT-PROPERTIES
  {
    Date=09/01/15;
    Time=16:10:22;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Comments;
               PTB=Coment rios];
    SourceTable=Table52113075;
    DelayedInsert=Yes;
    PageType=ListPart;
    AutoSplitKey=Yes;
  }
  CONTROLS
  {
    { 52006500;0;Container;
                ContainerType=ContentArea }

    { 52006501;1;Group    ;
                Name=Group;
                GroupType=Repeater }

    { 52006502;2;Field    ;
                SourceExpr=Comment }

    { 52006503;2;Field    ;
                SourceExpr="User ID";
                Editable=FALSE }

    { 52006504;2;Field    ;
                SourceExpr="Date-Time";
                Editable=FALSE }

  }
  CODE
  {

    BEGIN
    END.
  }
}

