OBJECT Table 471 Job Queue Category
{
  OBJECT-PROPERTIES
  {
    Date=07/09/12;
    Time=12:00:00;
    Version List=NAVW17.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Job Queue Category;
               PTB=Categoria Job Queue];
  }
  FIELDS
  {
    { 1   ;   ;Code                ;Code10        ;CaptionML=[ENU=Code;
                                                              PTB=ID Sess�o];
                                                   NotBlank=Yes }
    { 2   ;   ;Description         ;Text30        ;CaptionML=[ENU=Description;
                                                              PTB=ID Usu�rio] }
  }
  KEYS
  {
    {    ;Code                                    ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

