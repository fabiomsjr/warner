OBJECT Table 6521 Item Tracing History Buffer
{
  OBJECT-PROPERTIES
  {
    Date=05/11/08;
    Time=12:00:00;
    Version List=NAVW16.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Item Tracing History Buffer;
               PTB=Hist�rico Acompanhamento Buffer Produto];
  }
  FIELDS
  {
    { 1   ;   ;Entry No.           ;Integer       ;CaptionML=[ENU=Entry No.;
                                                              PTB=N� Mov.] }
    { 2   ;   ;Level               ;Integer       ;CaptionML=[ENU=Level;
                                                              PTB=N�vel] }
    { 10  ;   ;Serial No. Filter   ;Code250       ;CaptionML=[ENU=Serial No. Filter;
                                                              PTB=Filtro N� S�rie] }
    { 11  ;   ;Lot No. Filter      ;Code250       ;CaptionML=[ENU=Lot No. Filter;
                                                              PTB=Filtro N� Lote] }
    { 12  ;   ;Item No. Filter     ;Code250       ;CaptionML=[ENU=Item No. Filter;
                                                              PTB=Filtro N� Produto] }
    { 13  ;   ;Variant Filter      ;Code250       ;CaptionML=[ENU=Variant Filter;
                                                              PTB=Filtro Variante] }
    { 14  ;   ;Trace Method        ;Option        ;CaptionML=[ENU=Trace Method;
                                                              PTB=M�todo Seguimento];
                                                   OptionCaptionML=[ENU=Origin->Usage,Usage->Origin;
                                                                    PTB=Origem->Uso,Uso->Origem];
                                                   OptionString=Origin->Usage,Usage->Origin }
    { 15  ;   ;Show Components     ;Option        ;CaptionML=[ENU=Show Components;
                                                              PTB=Mostrar Componentes];
                                                   OptionCaptionML=[ENU=No,Item-tracked only,All;
                                                                    PTB=No,Apenas produto rastreado,Todos];
                                                   OptionString=No,Item-tracked only,All }
  }
  KEYS
  {
    {    ;Entry No.,Level                         ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

