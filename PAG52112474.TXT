OBJECT Page 52112474 Accountant by Branch Code
{
  OBJECT-PROPERTIES
  {
    Date=02/07/13;
    Time=14:33:24;
    Version List=NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Responsible by... (EFD);
               PTB=Respons vel por... (EFD)];
    SourceTable=Table52112462;
    DelayedInsert=Yes;
    PageType=ListPart;
    ShowFilter=No;
  }
  CONTROLS
  {
    { 52006500;0;Container;
                ContainerType=ContentArea }

    { 52006501;1;Group    ;
                Name=Group;
                GroupType=Repeater }

    { 52006502;2;Field    ;
                SourceExpr="Branch Code";
                OnValidate=BEGIN
                             CALCFIELDS("Branch Name");
                           END;
                            }

    { 52006503;2;Field    ;
                SourceExpr="Branch Name" }

  }
  CODE
  {

    BEGIN
    END.
  }
}

