OBJECT Table 99003604 Variable
{
  OBJECT-PROPERTIES
  {
    Date=22/09/04;
    Time=11:50:43;
    Version List=NDT2.00.01;
  }
  PROPERTIES
  {
  }
  FIELDS
  {
    { 1   ;   ;Entry No.           ;Integer        }
    { 2   ;   ;Version No.         ;Integer        }
    { 3   ;   ;Reference No.       ;Integer        }
    { 4   ;   ;Object Type         ;Option        ;OptionString=Table,Form,Report,Dataport,Codeunit,XMLport,MenuSuite }
    { 5   ;   ;Object ID           ;Integer        }
    { 6   ;   ;Local               ;Boolean        }
    { 7   ;   ;Array               ;Boolean        }
    { 8   ;   ;Temporarily         ;Boolean        }
    { 9   ;   ;Var                 ;Boolean        }
    { 10  ;   ;Function Parameter  ;Boolean        }
    { 11  ;   ;Function Return Type;Boolean        }
    { 12  ;   ;Type                ;Option        ;OptionString=Record,Form,Report,Dataport,Codeunit,System,Code,Text,Integer,Boolean,Decimal,Date,DateFormula,TextConst,Time,Option,Dialog,File,Action,OCX,Char,Binary,Automation,TransactionType,InStream,OutStream,Variant,RecordRef,FieldRef,KeyRef,RowID,GUID,DateTime,Duration,BigInteger,XMLport,MenuSuite,BigText,RecordID }
    { 13  ;   ;SubType             ;Text250        }
    { 14  ;   ;Name                ;Text50         }
    { 15  ;   ;Size                ;Integer        }
    { 16  ;   ;Dimension           ;Text10         }
    { 17  ;   ;SubType ID          ;Integer        }
    { 20  ;   ;SubType Name        ;Text30        ;FieldClass=FlowField;
                                                   CalcFormula=Lookup("Object Imported".Name WHERE (Version No.=FIELD(Version No.),
                                                                                                    Type=FIELD(Type),
                                                                                                    ID=FIELD(SubType ID)));
                                                   Editable=No }
    { 21  ;   ;Property Reference No.;Integer      }
    { 22  ;   ;Object Consecutive No.;Integer      }
  }
  KEYS
  {
    {    ;Entry No.                               ;MaintainSIFTIndex=No;
                                                   Clustered=Yes }
    {    ;Reference No.,Local,Function Parameter,Name,Type,SubType ID;
                                                   MaintainSIFTIndex=No }
    {    ;Version No.,Object Type,Object ID,Name  ;MaintainSIFTIndex=No }
    {    ;Version No.,Type,SubType ID             ;MaintainSIFTIndex=No }
    {    ;Version No.,Property Reference No.      ;MaintainSIFTIndex=No }
    {    ;Version No.,Reference No.,Object Type,Object ID,Entry No.,Local,Function Parameter,Name,Function Return Type;
                                                   MaintainSIFTIndex=No }
    {    ;Reference No.,Entry No.                 ;MaintainSIFTIndex=No }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

