OBJECT Table 45 G/L Register
{
  OBJECT-PROPERTIES
  {
    Date=07/09/12;
    Time=12:00:00;
    Version List=NAVW17.00,NAVBR7;
  }
  PROPERTIES
  {
    CaptionML=[ENU=G/L Register;
               PTB=Reg. Mov. Cont�beis];
    LookupPageID=Page116;
  }
  FIELDS
  {
    { 1   ;   ;No.                 ;Integer       ;CaptionML=[ENU=No.;
                                                              PTB=N�] }
    { 2   ;   ;From Entry No.      ;Integer       ;TableRelation="G/L Entry";
                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=From Entry No.;
                                                              PTB=Do N� Mov.] }
    { 3   ;   ;To Entry No.        ;Integer       ;TableRelation="G/L Entry";
                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=To Entry No.;
                                                              PTB=At� N� Mov.] }
    { 4   ;   ;Creation Date       ;Date          ;CaptionML=[ENU=Creation Date;
                                                              PTB=Data Cria��o] }
    { 5   ;   ;Source Code         ;Code10        ;TableRelation="Source Code";
                                                   CaptionML=[ENU=Source Code;
                                                              PTB=Cod. Origem] }
    { 6   ;   ;User ID             ;Code50        ;TableRelation=User."User Name";
                                                   OnLookup=VAR
                                                              UserMgt@1000 : Codeunit 418;
                                                            BEGIN
                                                              UserMgt.LookupUserID("User ID");
                                                            END;

                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=User ID;
                                                              PTB=ID Usu�rio] }
    { 7   ;   ;Journal Batch Name  ;Code10        ;CaptionML=[ENU=Journal Batch Name;
                                                              PTB=Nome Se��o Di�rio] }
    { 8   ;   ;From VAT Entry No.  ;Integer       ;TableRelation="VAT Entry";
                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=From VAT Entry No.;
                                                              PTB=A partir do N� Mov. IPI] }
    { 9   ;   ;To VAT Entry No.    ;Integer       ;TableRelation="VAT Entry";
                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=To VAT Entry No.;
                                                              PTB=At� N� Mov. IPI] }
    { 10  ;   ;Reversed            ;Boolean       ;CaptionML=[ENU=Reversed;
                                                              PTB=Reservado] }
    { 52112430;;Approval Flow No.  ;Code20        ;TableRelation="Approval Flow";
                                                   CaptionML=[ENU=Approval Flow No.;
                                                              PTB=N� Fluxo Aprova��o] }
  }
  KEYS
  {
    {    ;No.                                     ;Clustered=Yes }
    {    ;Creation Date                            }
    {    ;Source Code,Journal Batch Name,Creation Date }
    {    ;From Entry No.,To Entry No.              }
  }
  FIELDGROUPS
  {
    { 1   ;DropDown            ;No.,From Entry No.,To Entry No.,Creation Date,Source Code }
  }
  CODE
  {

    BEGIN
    END.
  }
}

